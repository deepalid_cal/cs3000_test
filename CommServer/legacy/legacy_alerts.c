/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#include	"legacy_alerts.h"

#include	"legacy_common.h"

#include	"alert_parsing.h"

// Required for SQL functions
#include	"sql_merge.h"

#include	"legacy_change_lines_600.h"

#include	"legacy_change_lines_500.h"




/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

#define	MAX_ALERT_LENGTH					(ALERTS_MAX_TOTAL_PARSED_LENGTH)

// ----------

#define MAX_CHANGE_LINE_VARIABLE_SIZE (6)

// ----------

#define	FOAL_WeArePOAFS() 			(TRUE)

// ----------

#define PERCENT_ADJUST_NO_EXPIRE	0xFFFF

// ----------

#define LIGHTS_REASON__NO_ALERT_LINE					0x00
#define LIGHTS_REASON__ON_DUE_TO_ON_TIME				0x10
#define LIGHTS_REASON__ON_DUE_TO_MANUALLY_STARTED		0x20
#define LIGHTS_REASON__ON_DUE_TO_COMMUNICATIONS			0x30
#define LIGHTS_REASON__OFF_DUE_TO_OFF_TIME				0x40
#define nlu_LIGHTS_REASON__OFF_DUE_TO_TIME_EXPIRED		0x50
#define LIGHTS_REASON__OFF_DUE_TO_STOP_KEY				0x60
#define LIGHTS_REASON__OFF_DUE_TO_USER_TURNED_OFF		0x70
#define nlu_LIGHTS_REASON__OFF_DUE_TO_PROGRAM_RESTART	0x80

// ----------

#define LEGACY_IN_LIST_FOR_SMS_HO					0x00
#define LEGACY_IN_LIST_FOR_PROGRAMMED_IRRIGATION	0x20
#define LEGACY_IN_LIST_FOR_SMS_2					0x40
#define LEGACY_IN_LIST_FOR_SMS_1					0x60
#define LEGACY_IN_LIST_FOR_MANUAL					0x80
#define LEGACY_IN_LIST_FOR_SMS_SEQUENCE				0xA0
#define LEGACY_IN_LIST_FOR_TEST						0xC0
#define LEGACY_IN_LIST_FOR_DTMF						0xE0

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef enum
{
	LEGACY_INITIATED_VIA_KEYPAD,

	LEGACY_INITIATED_VIA_COMM,

	LEGACY_INITIATED_VIA_DIRECT_ACCESS,

	LEGACY_INITIATED_VIA_MVSHORT,

	LEGACY_INITIATED_VIA_MVOR_SCHEDULE,

	LEGACY_INITIATED_VIA_MLB,

	LEGACY_INITIATED_VIA_SYSTEM_TIMEOUT,   

	LEGACY_INITIATED_VIA_LAST_TYPE	// for indexing purposes

} LEGACY_INITIATED_VIA_ENUM;

typedef unsigned char LEGACY_INITIATED_VIA_ENUM_SIZE;

// ----------

typedef enum
{
	LEGACY_SKIPPED_DUE_TO_CONTROLLER_OFF,

	LEGACY_SKIPPED_DUE_TO_MLB,

	LEGACY_SKIPPED_DUE_TO_RAIN_SWITCH,

	LEGACY_SKIPPED_DUE_TO_MVOR_CLOSED,

	LEGACY_SKIPPED_DUE_TO_RAIN_R_OR_RAIN_BUILDUP,

	LEGACY_SKIPPED_DUE_TO_RAIN_S,

	LEGACY_SKIPPED_DUE_TO_RAIN_P,

	LEGACY_SKIPPED_DUE_TO_STOP_TIME,

	LEGACY_SKIPPED_DUE_TO_FREEZE_SWITCH,

	LEGACY_SKIPPED_DUE_TO_STOP_SWITCH,

	LEGACY_SKIPPED_LAST_REASON

} LEGACY_REASONS_TO_SKIP_IRRIGATION;

typedef unsigned char LEGACY_REASONS_TO_SKIP_IRRIGATION_SIZE;

// ----------

typedef enum
{
	LEGACY_CLEAR_MLB_FROM_KEYPAD,

	LEGACY_CLEAR_MLB_FROM_COMMLINK,

	LEGACY_CLEAR_MLB_SHORT

} LEGACY_CLEARED_MLB_HOW;

typedef unsigned char LEGACY_CLEARED_MLB_HOW_SIZE;

// ----------

typedef enum
{
	LEGACY_PWRESET_UPDATE_FROM_UNKNOWN_ROM_VERSION,

	LEGACY_PWRESET_CMOS_UNITIALIZED,

	LEGACY_PWRESET_COMMLINK

} LEGACY_PASSWORD_RESET_REASON;

typedef unsigned char LEGACY_PASSWORD_RESET_REASON_SIZE;

// ----------

typedef enum
{
	LEGACY_PW_CANCEL_UNLOCK_KEYPAD_TIMEOUT,

	LEGACY_PW_CANCEL_UNLOCK_USER_CANCELLED

} LEGACY_PASSWORD_CANCEL_UNLOCK_REASON;

typedef unsigned char LEGACY_PASSWORD_CANCEL_UNLOCK_REASON_SIZE;

// ----------

typedef enum
{
	LEGACY_PW_LOGOUT_KEYPAD_TIMEOUT,

	LEGACY_PW_LOGOUT_USER_LOGGEDOUT

} LEGACY_PASSWORD_LOGOUT_REASON;

typedef unsigned char LEGACY_PASSWORD_LOGOUT_REASON_SIZE;

// ----------

typedef enum
{
	LEGACY_PR_DUE_TO_WIND,	 // pause or resume due to wind

	LEGACY_PR_DUE_TO_KEYPAD,

	LEGACY_PR_DUE_TO_CENTRAL_COMMLINK

} LEGACY_REASONS_TO_PAUSE_RESUME_IRRIGATION;

typedef unsigned char LEGACY_REASONS_TO_PAUSE_RESUME_IRRIGATION_SIZE;

// ----------

typedef enum
{
	X_WDT,
	X_DTMF,

	X_BUS_ERROR,
	X_ADDR_ERROR,
	X_ILLEGAL_INSTR,
	X_DIV_BY_0,
	X_CHK_INSTR,
	X_TRAPV_INSTR,
	X_PRIV_VIOLATION,
	X_TRACE,
	X_1010_EMULATOR,
	X_1111_EMULATOR,
	X_UNASSIGNED_1,
	X_UNASSIGNED_2,
	X_UNINITIALIZED_INT,
	X_SPURIOUS_INT,

	X_RETURN_FROM_MAIN,

	LAST_X_TYPE	  // to set array length in diagdply.c

} LEGACY_EXCEPTION_LINE_TYPE;

typedef unsigned char LEGACY_EXCEPTION_LINE_TYPE_SIZE;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

char LEGACY_who_initiated_string[ LEGACY_INITIATED_VIA_LAST_TYPE ][ 24 ] = {

	" (keypad)",
	" (comm)",
	" (direct access)",
	" (mv short)",
	" (scheduled)",
	" (mainline break)",
	" (system timeout)"
};

char LEGACY_s_who_initiated_string[ LEGACY_INITIATED_VIA_LAST_TYPE ][ 24 ] = {

	" (keypad)",
	" (comm)",
	" (direct access)",
	" (mv short)",
	" (scheduled)",
	" (mainline break)",
	" (system timeout)"
};

// ----------

char LEGACY_SkippedReasonsText[ LEGACY_SKIPPED_LAST_REASON ][ 48 ] = {

	" (Controller OFF)",
	" (MAINLINE BREAK)",
	" (Rain Switch)",
	" (MV CLOSED)",
	" (Rain)",
	" (Poll S in Table)",
	" (Poll P in Table)",
	" (Stop Time SAME AS Start Time!)",
	" (Freeze Switch)",
	" (Stop Switch)"

};

char LEGACY_S_SkippedReasonsText[ LEGACY_SKIPPED_LAST_REASON ][ 48 ] = {

	" (Controller OFF)",
	" (MAINLINE BREAK)",
	" (Rain Switch)",
	" (MV CLOSED)",
	" (Rain)",
	" (Poll S in Table)",
	" (Poll P in Table)",
	" (Stop Time SAME AS Start Time!)",
	" (Freeze Switch)",
	" (Stop Switch)"

};

// ----------

const char E_ProgStrWithoutColon[8][7] = { "PROG A\0",
	"PROG B\0",
	"PROG C\0",
	"PROG D\0",
	"PROG E\0",
	"PROG F\0",
	"PROG G\0",
	" ALL  \0" };

const char S_ProgStrWithoutColon[8][7] = { "PROG A\0",
	"PROG B\0",
	"PROG C\0",
	"PROG D\0",
	"PROG E\0",
	"PROG F\0",
	"PROG G\0",
	" ALL  \0" };

// ----------

char A_NoYes[ 2 ][ 4 ] = { "NO", "YES" };

char S_A_NoYes[ 2 ][ 4 ] = { "NO", "SI" };

// ----------

char A_OffOn[ 2 ][ 4 ] = { "OFF", "ON" };

char S_A_OffOn[ 2 ][ 9 ] = { "APAGADO", "PRENDIDO" };

// ----------

char A_PlusMinus[ 2 ][ 8 ] = { "+", "-" };

// ----------

char lights_time_str16[ 4 ][ 16 ] =
{
	"first ON time",
	"second ON time",
	"first OFF time",
	"second OFF time"
};

char s_lights_time_str16[ 4 ][ 28 ] =  {  
	"primero tiempo de PRENDER",
	"segundo tiempo de PRENDER",
	"primero tiempo de APAGAR",
	"segundo tiempo de APAGAR"
};

// ----------

char inuse_str[ 2 ][ 12 ] = { "NOT IN USE", "IN USE" };

char s_inuse_str[ 2 ][ 12 ] = { "NO EN USO", "EN USO" };

// ----------

char referenceET_str[ 2 ][ 24 ] = { "CHOOSE FROM SUPPLIED", "ENTER YOUR OWN" };

char s_referenceET_str[ 2 ][ 24 ] = { "ESCOJA DE PROPORCIONADO", "INCORPROE SUS PROPIOS" };

// ----------

char budgetoption_str[ 3 ][ 24 ] = { "ENTER MONTHLY", "ENTER YEARLY", "CALCULATED" };

char s_budgetoption_str[ 3 ][ 24 ] = { "ENTER MONTHLY", "ENTER YEARLY", "CALCULATED" };

// ----------

char prioritylevels_str[ 3 ][ 7 ] = { "HIGH\0", "MEDIUM\0", "LOW\0" };

char s_prioritylevels_str[ 3 ][ 7 ] = { "ALTO\0", "MEDIO\0", "BAJO\0" };

// ----------

const char E_ActionStrs[ 3 ][ 16 ] =
{ 
	"No Alerts      \0",
	"Alert/No Action\0",
	"Alert/Shut-Off \0"
};

const char S_ActionStrs[ 3 ][ 16 ] =
{
	"No Alertas     \0",
	"Alerta/Corre   \0",
	"Alerta/Apague  \0"
};

// ----------

const char E_CapDayStringsShort[7][4] = { "SUN\0","MON\0","TUE\0","WED\0","THU\0","FRI\0","SAT\0"};

const char S_CapDayStringsShort[7][4] = { "DOM\0","LUN\0","MAR\0","MIE\0","JUE\0","VIE\0","SAB\0"};

// ----------

const char FlowMeterStrings[ 13 ][ 9 ] =
{
	"NOT USED\0",	//  0
	"FM-1    \0",	//  1
	"FM-1B   \0",	//  2
	"FM-1.25B\0",	//  3
	"FM-1.5  \0",	//  4
	"FM-2    \0",	//  5
	"FM-3    \0",	//  6
	"FM-1.5B \0",	//  7
	"FM-2B   \0",	//  8
	"FMBX    \0",	//  9
	"        \0",	// 10
	"        \0",	// 11
	"        \0"	// 12
};

// ----------

char MVType_Str_20[ 4 ][ 24 ] =
{
	"NORMALLY OPEN  ",
	"NORMALLY CLOSED",
	"NORMALMENTE ABIERTO",
	"NORMALMENTE CERRADA"
};

// ----------

const char  E_MonthStringsShort[13][4] =
{
	"NO!\0",
	"Jan\0",
	"Feb\0",
	"Mar\0",
	"Apr\0",
	"May\0",
	"Jun\0",
	"Jul\0",
	"Aug\0",
	"Sep\0",
	"Oct\0",
	"Nov\0",
	"Dec\0"
};

const char  S_MonthStringsShort[13][4] =
{
	"NO!\0",
	"Ene\0",
	"Feb\0",
	"Mar\0",
	"Abr\0",
	"May\0",
	"Jun\0",
	"Jul\0",
	"Ago\0",
	"Sep\0",
	"Oct\0",
	"Nov\0",
	"Dic\0"
};

// ----------

const char  E_MonthStringsShortCAPS[13][4] =
{
	"NO!\0",
	"JAN\0",
	"FEB\0",
	"MAR\0",
	"APR\0",
	"MAY\0",
	"JUN\0",
	"JUL\0",
	"AUG\0",
	"SEP\0",
	"OCT\0",
	"NOV\0",
	"DEC\0"
};

const char  S_MonthStringsShortCAPS[13][4] =
{
	"NO!\0",
	"ENE\0",
	"FEB\0",
	"MAR\0",
	"ABR\0",
	"MAY\0",
	"JUN\0",
	"JUL\0",
	"AGO\0",
	"SEP\0",
	"OCT\0",
	"NOV\0",
	"DIC\0"
};

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
extern char *valveson_str( char *pstr, const UNS_32 pstr_size, const unsigned short pvalveson )
{

	if( pvalveson == 0 )
	{

		strlcpy( pstr, "X", pstr_size );

	}
	else
	{

		snprintf( pstr, pstr_size, "%d", pvalveson );

	}

	return( pstr );

}

/* ---------------------------------------------------------- */
extern char *Change_MonthString_16( char *pstr, const UNS_32 pstr_size, const unsigned short preason, const unsigned short index0_11 )
{

	// If preason has its high order bit set we return the month string
	// else return a NULL string. pstr is provided as a
	// place to build the NULL string.
	//

	// make the null string now
	//
	pstr[ 0 ] = 0;

	if( (preason & EEWRITE_REASON_12MONTH_MASK) == EEWRITE_REASON_12MONTH_MASK )
	{

		strlcat( pstr, " ", pstr_size );

		if( Language == ENGLISH )
		{

			strlcat( pstr, E_MonthStringsShort[ index0_11 + 1 ], pstr_size );

		}
		else
		{

			strlcat( pstr, S_MonthStringsShort[ index0_11 + 1 ], pstr_size );

		}

	}
	else
	{

		// nothing to do the null string is already made

	}


	return( pstr );

}

/* ---------------------------------------------------------- */
extern char *Change_ReasonString_32( char *pstr, const UNS_32 pstr_size, unsigned short preason )
{

	// The high order byte of preason is used for special flags. MASK it out.
	//
	preason &= 0x0FFF;

	// make the null string now
	//
	pstr[ 0 ] = 0;

	if( Language == ENGLISH )
	{

		switch( preason )
		{
			
			case EEWRITE_REASON_COMMLINK:
				strlcat( pstr, " (comm)", pstr_size );
				break;

			case EEWRITE_REASON_KEYPAD:
				strlcat( pstr, " (keypad)", pstr_size );
				break;  

			case EEWRITE_REASON_ZEROED_DERATE_TABLE:
				strlcat( pstr, " (table reset)", pstr_size );
				break;  

			case EEWRITE_REASON_NEWROMS:
				strlcat( pstr, " (new roms)", pstr_size );
				break;  

			case EEWRITE_REASON_CHECKSUM_UPDATE:
				// there's really nothing to add to this
				break;  

			case EEWRITE_REASON_NORMAL_DERATE_UPDATE:
				strlcat( pstr, " (normal derate update)", pstr_size );
				break;  

			case EEWRITE_REASON_AUTO_ADJUSTED:
				strlcat( pstr, " (auto adjust)", pstr_size );
				break;  

			case EEWRITE_REASON_EXPECTED_GPM_SET:
				strlcat( pstr, " (set expected)", pstr_size );
				break;  

			case EEWRITE_REASON_HIT_A_STARTTIME:
				strlcat( pstr, " (start time)", pstr_size );
				break;  

			case EEWRITE_REASON_MOISTURE_SENSOR_READ:
				strlcat( pstr, " (sensor read)", pstr_size );
				break;  

			case EEWRITE_REASON_SHARING_WEATHER:
				strlcat( pstr, " (shared)", pstr_size );
				break;         

			case EEWRITE_REASON_ITS_RAINING:
				strlcat( pstr, " (it's raining)", pstr_size );
				break;

			case EEWRITE_REASON_DAY_CHANGE:
				strlcat( pstr, " (day change)", pstr_size );
				break;              

			case EEWRITE_REASON_MONTH_CHANGE:
				strlcat( pstr, " (month change)", pstr_size );
				break;              

			case EEWRITE_REASON_NORMAL_STARTUP:
				strlcat( pstr, " (startup housekeeping)", pstr_size );
				break;              

			case EEWRITE_REASON_START_OF_ET_DAY:
				strlcat( pstr, " (new ET day)", pstr_size );
				break;              

			case EEWRITE_REASON_NEW_CITY:
				strlcat( pstr, " (new city)", pstr_size );
				break;              

			case EEWRITE_REASON_SET_BY_CMOS:
				strlcat( pstr, " (set by cmos)", pstr_size );
				break;              

			case EEWRITE_REASON_FCC_EXPECTED_CHANGED:
				strlcat( pstr, " (expected changed)", pstr_size );
				break;              

			case EEWRITE_REASON_FCC_RAN_A_CYCLE:
				strlcat( pstr, " (new cycle)", pstr_size );
				break;              

			case EEWRITE_REASON_DONT_SHOW_A_REASON:
				// nothing to do
				break;              

			case EEWRITE_REASON_CMOS_VERSION_UPDATE:
				strlcat( pstr, " (cmos version update)", pstr_size );
				break;              

			case EEWRITE_REASON_CMOS_FAILED_OR_NEW:
				strlcat( pstr, " (cmos new or failed)", pstr_size );
				break;              

			case EEWRITE_REASON_RRE_REMOTE:
				strlcat( pstr, " (radio remote)", pstr_size );
				break;              

		}

	}
	else
	{

		switch( preason )
		{
			
			case EEWRITE_REASON_COMMLINK:
				strlcat( pstr, " (comm)", pstr_size );
				break;

			case EEWRITE_REASON_KEYPAD:
				strlcat( pstr, " (teclado)", pstr_size );
				break;  

			case EEWRITE_REASON_ZEROED_DERATE_TABLE:
				strlcat( pstr, " (tabla repone)", pstr_size );
				break;  

			case EEWRITE_REASON_NEWROMS:
				strlcat( pstr, " (nuevas roms)", pstr_size );
				break;  

			case EEWRITE_REASON_CHECKSUM_UPDATE:
				// there's really nothing to add to this
				break;  

			case EEWRITE_REASON_NORMAL_DERATE_UPDATE:
				strlcat( pstr, " (derate normal actualiza)", pstr_size );
				break;  

			case EEWRITE_REASON_AUTO_ADJUSTED:
				strlcat( pstr, " (auto ajusta)", pstr_size );
				break;  

			case EEWRITE_REASON_EXPECTED_GPM_SET:
				strlcat( pstr, " (conjunto previsto)", pstr_size );
				break;  

			case EEWRITE_REASON_HIT_A_STARTTIME:
				strlcat( pstr, " (empiece tiempo)", pstr_size );
				break;  

			case EEWRITE_REASON_MOISTURE_SENSOR_READ:
				strlcat( pstr, " (sensor leyo)", pstr_size );
				break;  

			case EEWRITE_REASON_SHARING_WEATHER:
				strlcat( pstr, " (compartido)", pstr_size );
				break;         

			case EEWRITE_REASON_ITS_RAINING:
				strlcat( pstr, " (llueve)", pstr_size );
				break;

			case EEWRITE_REASON_DAY_CHANGE:
				strlcat( pstr, " (cambio de dia)", pstr_size );
				break;              

			case EEWRITE_REASON_MONTH_CHANGE:
				strlcat( pstr, " (cambio de mes)", pstr_size );
				break;              

			case EEWRITE_REASON_NORMAL_STARTUP:
				strlcat( pstr, " (inicio)", pstr_size );
				break;              

			case EEWRITE_REASON_START_OF_ET_DAY:
				strlcat( pstr, " (nuevo ET dia)", pstr_size );
				break;              

			case EEWRITE_REASON_NEW_CITY:
				strlcat( pstr, " (nueva ciudad)", pstr_size );
				break;              

			case EEWRITE_REASON_SET_BY_CMOS:
				strlcat( pstr, " (ponga por cmos)", pstr_size );
				break;              

			case EEWRITE_REASON_FCC_EXPECTED_CHANGED:
				strlcat( pstr, " (previsto cambiado)", pstr_size );
				break;              

			case EEWRITE_REASON_FCC_RAN_A_CYCLE:
				strlcat( pstr, " (nuevo ciclo)", pstr_size );
				break;              

			case EEWRITE_REASON_DONT_SHOW_A_REASON:
				// nothing to do
				break;              

			case EEWRITE_REASON_CMOS_VERSION_UPDATE:
				strlcat( pstr, " (version de cmos actualiza)", pstr_size );
				break;              

			case EEWRITE_REASON_CMOS_FAILED_OR_NEW:
				strlcat( pstr, " (cmos nuevo o fallado)", pstr_size );
				break;              

			case EEWRITE_REASON_RRE_REMOTE:
				strlcat( pstr, " (radio remote)", pstr_size );
				break;              

		}

	}


	return( pstr );

}

/* ---------------------------------------------------------- */
extern char *ShaveRightPad_63max( char *pbuf, const UNS_32 pbuf_size, char *pstr )
{

	int i;	// must be an int for the for loop to end

// use a local string buffer to perform length adjustment - use of the local buffer allows
// calls such as ShaveRightPad_63max( mystr, mystr ) to work.
//
	char str_64[ 64 ];


	// put pstr into pbuf, starting at the end remove any spaces by moving the null char, return pbuf
	//
	strlcpy( str_64, pstr, sizeof(str_64) );

	for (i = (INT_32)strlen(str_64) - 1; i >= 0; i--)
	{

		// if we've bumped into a character we're done.
		//
		if( str_64[ i ] != 0x20 )
		{

			break;

		}
		else
		{

			// move the end of the string in one
			//
			str_64[ i ] = 0x00;

		}
	}

	strlcpy( pbuf, str_64, pbuf_size );

	return( pbuf );

}

/* ---------------------------------------------------------- */
static char *ReasonONString( char *pbuf, const UNS_32 pbuf_size, unsigned char preason )
{

	switch( preason )
	{
		
		case LEGACY_IN_LIST_FOR_DTMF:
			if( Language == ENGLISH )
			{
				strlcpy( pbuf, "Radio Remote", pbuf_size );
			}
			else
			{
				strlcpy( pbuf, "Radio Remoto", pbuf_size );
			}
			break;
		case LEGACY_IN_LIST_FOR_TEST:
			if( Language == ENGLISH )
			{
				strlcpy( pbuf, "Test Irrigation", pbuf_size );
			}
			else
			{
				strlcpy( pbuf, "Pruebe", pbuf_size );
			}
			break;
		case LEGACY_IN_LIST_FOR_SMS_SEQUENCE:
			if( Language == ENGLISH )
			{
				strlcpy( pbuf, "Walk-Thru", pbuf_size );
			}
			else
			{
				strlcpy( pbuf, "Inspeccion", pbuf_size );
			}
			break;
		case LEGACY_IN_LIST_FOR_MANUAL:
			if( Language == ENGLISH )
			{
				strlcpy( pbuf, "Manually Watering", pbuf_size );
			}
			else
			{
				strlcpy( pbuf, "Manual Riego", pbuf_size );
			}
			break;
		case LEGACY_IN_LIST_FOR_SMS_1:
		case LEGACY_IN_LIST_FOR_SMS_2:
			if( Language == ENGLISH )
			{
				strlcpy( pbuf, "Manual Program", pbuf_size );
			}
			else
			{
				strlcpy( pbuf, "Programa Manual", pbuf_size );
			}
			break;
		case LEGACY_IN_LIST_FOR_PROGRAMMED_IRRIGATION:
			if( Language == ENGLISH )
			{
				strlcpy( pbuf, "Programmed Irrigation", pbuf_size );
			}
			else
			{
				strlcpy( pbuf, "Irrigacion Programada", pbuf_size );
			}
			break;
		case LEGACY_IN_LIST_FOR_SMS_HO:
			if( Language == ENGLISH )
			{
				strlcpy( pbuf, "Manual Hold-Over", pbuf_size );
			}
			else
			{
				strlcpy( pbuf, "Manual Remanente", pbuf_size );
			}
			break;
	}


	return( pbuf );

}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static unsigned short Pull_StartTime_Skipped_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{
	unsigned short rv;

	PROG_ASS_SIZE p;

	LEGACY_REASONS_TO_SKIP_IRRIGATION_SIZE reason;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &reason, pindex, sizeof( LEGACY_REASONS_TO_SKIP_IRRIGATION_SIZE ) );
	rv += ALERTS_pull_legacy_object_off_pile(ppile, &p, pindex, sizeof( PROG_ASS_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "%s Not Watering%s", E_ProgStrWithoutColon[ p ], LEGACY_SkippedReasonsText[ reason ] );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static unsigned short Pull_Exception_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{
	LEGACY_EXCEPTION_LINE_TYPE_SIZE xlt;

	unsigned short rv;

	char ExceptionText[ LAST_X_TYPE ][ 16 ] =   {

		"WDT           ",
		"DTMF          ",
		"Bus Error     ",
		"Address Error ",
		"Illegal Instr ",
		"Divide By Zero",
		"CHK Instr     ",
		"TRAPV Instr   ",
		"Priv Violation",
		"Trace         ",
		"1010 Emulator ",
		"1111 Emulator ",
		"Unassigned 1  ",
		"Unassigned 2  ",
		"Uninitialized ",
		"Spurious Int  ",
		"Returned Main "
	};

	rv = ALERTS_pull_legacy_object_off_pile( ppile, &xlt, pindex, sizeof( LEGACY_EXCEPTION_LINE_TYPE_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		strlcat( pdest, ExceptionText[ xlt ], pdest_size );
	}

	return( rv );
}

/* ---------------------------------------------------------- */
static unsigned short Pull_MainlineBreak_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short measured, allowed;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &measured, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &allowed, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		snprintf( pdest, pdest_size, "MAINLINE BREAK: measured %d gpm / allowed %d gpm", measured, allowed );
	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_flow_not_checked_no_reasons_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short station;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		// because this alert shows on each irri machine there is only the station number as
		// opposed to address cahr plu station number
		//
		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Flow Not Checked: Sta %d", station + 1 );

		}
		else
		{

			snprintf( pdest, pdest_size, "Flujo No Verificado: Sta %d", station + 1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_flow_not_checked_with_reasons_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short station;

	BOOLEAN vit, mvjo, all_linefill_elapsed, all_valveclose_elapsed, my_linefill, my_valveclose, stable;  

	char str_16[ 16 ];


	rv = 0;

	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_16, sizeof(str_16), pindex );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &vit, pindex, sizeof( BOOLEAN ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &mvjo, pindex, sizeof( BOOLEAN ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &all_linefill_elapsed, pindex, sizeof( BOOLEAN ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &all_valveclose_elapsed, pindex, sizeof( BOOLEAN ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &my_linefill, pindex, sizeof( BOOLEAN ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &my_valveclose, pindex, sizeof( BOOLEAN ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &stable, pindex, sizeof( BOOLEAN ) );


	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Flow Not Checked: Sta %d", station + 1 );

		}
		else
		{

			snprintf( pdest, pdest_size, "Flujo No Verificado: Sta %d", station + 1 );

		}

		/*
		this used to be the full blown display of all the data for debug...the central will still
		show it this way in field service mode
		
		snprintf( pdest, pdest_size, 
				 "FLOW NOT CHECKED: Sta %d, vit=%s mvjo=%s all_lf=%s all_vc=%s my_lf=%s my_vc=%s stable=%s",
				 station + 1,
				 false_true[ vit ],
				 false_true[ mvjo ],
				 false_true[ all_linefill_elapsed ],
				 false_true[ all_valveclose_elapsed ],
				 false_true[ my_linefill ],
				 false_true[ my_valveclose ],
				 false_true[ stable ]
			   );
		*/

	}


	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_Flow_Error_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, ALERT_ID paid, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	// This function is responsible to build the string for the high flow alert in pdest.
	// It needs to keep pindex current to the next place the data comes from.
	// Must return how many bytes it pulls off of the pile for this alert.
	//
	// pdest points to a string and can be concatenated to as a string
	//

	unsigned short rv;

	unsigned char in_list_for;

	unsigned short station_number;

	unsigned short system_expected_us;
	unsigned short system_derated_expected_us;
	unsigned short system_limit_us;
	unsigned short system_actual_flow_us;

	unsigned short station_derated_expected_us;
	unsigned short station_share_of_actual_us;
	unsigned short stations_on_us;

	char str_32[ 32 ];


	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &in_list_for, pindex, sizeof( unsigned char ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station_number, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &system_expected_us, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &system_derated_expected_us, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &system_limit_us, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &system_actual_flow_us, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station_derated_expected_us, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station_share_of_actual_us, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &stations_on_us, pindex, sizeof( unsigned short ) );


	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( paid == LEGACY_AID_FIRST_LOWFLOW )
		{

			if( system_actual_flow_us == 0 )
			{

				if( Language == ENGLISH )
				{

					strlcpy( str_32, "1st NO FLOW", sizeof(str_32) );

				}
				else
				{

					strlcpy( str_32, "PRIMERO NO FLUJO", sizeof(str_32) );

				}

			}
			else
			{

				if( Language == ENGLISH )
				{

					strlcpy( str_32, "1st LOW FLOW", sizeof(str_32) );

				}
				else
				{

					strlcpy( str_32, "PRIMERO BAJO FLUJO", sizeof(str_32) );

				}

			}

		}
		else if( paid == LEGACY_AID_FINAL_LOWFLOW )
		{

			if( system_actual_flow_us == 0 )
			{

				if( Language == ENGLISH )
				{

					strlcpy( str_32, "NO FLOW", sizeof(str_32) );

				}
				else
				{

					strlcpy( str_32, "NO FLUJO", sizeof(str_32) );

				}

			}
			else
			{

				if( Language == ENGLISH )
				{

					strlcpy( str_32, "LOW FLOW", sizeof(str_32) );

				}
				else
				{

					strlcpy( str_32, "BAJO FLUJO", sizeof(str_32) );

				}

			}

		}
		else if( paid == LEGACY_AID_FIRST_HIGHFLOW )
		{

			if( Language == ENGLISH )
			{

				strlcpy( str_32, "1st HIGH FLOW", sizeof(str_32) );

			}
			else
			{

				strlcpy( str_32, "PRIMERO ALTO FLUJO", sizeof(str_32) );

			}

		}
		else if( paid == LEGACY_AID_FINAL_HIGHFLOW )
		{

			if( Language == ENGLISH )
			{

				strlcpy( str_32, "HIGH FLOW", sizeof(str_32) );

			}
			else
			{

				strlcpy( str_32, "ALTO FLUJO", sizeof(str_32) );

			}

		}

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, 
					  "%s Sta %d, Expected %d Saw %d gpm",
					  str_32,
					  station_number + 1,
					  station_derated_expected_us,
					  station_share_of_actual_us );

		}
		else
		{

			snprintf( pdest, pdest_size, 
					  "%s Sta %d, Previsto %d Vio %d gpm",
					  str_32,
					  station_number + 1,
					  station_derated_expected_us,
					  station_share_of_actual_us );

		}



		/*
		
		this is the code for the VERY detailed line which we are no longer showing
		
		if ( paid == LEGACY_AID_FIRST_LOWFLOW ) {
		
			if ( system_actual_flow_us == 0 ) {
				
				snprintf( str_32, "1st No Flow Sta %d:", station_number + 1 );

			} else {
				
				snprintf( str_32, "1st Low Flow Sta %d:", station_number + 1 );

			}
		
		} else
		if ( paid == LEGACY_AID_FINAL_LOWFLOW ) {
			
			if ( system_actual_flow_us == 0 ) {
				
				snprintf( str_32, "NO FLOW Sta %d:", station_number + 1 );

			} else {
				
				snprintf( str_32, "LOW FLOW Sta %d:", station_number + 1 );

			}

		} else
		if ( paid == LEGACY_AID_FIRST_HIGHFLOW ) {
			
			snprintf( str_32, "1st High Flow %d:", station_number + 1 );

		} else
		if ( paid == LEGACY_AID_FINAL_HIGHFLOW ) {
			
			snprintf( str_32, "HIGH FLOW %d:", station_number + 1 );

		}
		

		snprintf( pdest, pdest_size, 
				 "%s (%s), SYSTEM: expected=%d, learned exp=%d, limit=%d, actual=%d, STATION: expected=%d, rate=%d, #on=%d",
				 str_32,
				 ReasonONString( str_24, in_list_for ),
				 
				 system_expected_us,
				 system_derated_expected_us,
				 system_limit_us,
				 system_actual_flow_us,
				 
				 station_derated_expected_us,
				 station_share_of_actual_us,

				 stations_on_us );
				 
		*/

	}


	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_Short_STATION_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;
	unsigned short station;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "SHORT: Station %d", station+1 );

		}
		else
		{

			snprintf( pdest, pdest_size, "CORTO: Estacion %d", station+1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_Short_MV_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;
	unsigned char laddr;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &laddr, pindex, sizeof( unsigned char ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( FOAL_WeArePOAFS() == TRUE )
		{

			if( Language == ENGLISH )
			{

				snprintf( pdest, pdest_size, "SHORT: Master Valve, reported by \"%c\"", laddr );

			}
			else
			{

				snprintf( pdest, pdest_size, "SHORT: Valvula Maestra, reported informado por \"%c\"", laddr );

			}

		}
		else
		{

			if( Language == ENGLISH )
			{

				snprintf( pdest, pdest_size, "SHORT: Master Valve" );

			}
			else
			{

				snprintf( pdest, pdest_size, "CORTO: Valvula Maestra" );

			}

		}   

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_Short_PUMP_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;
	unsigned char laddr;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &laddr, pindex, sizeof( unsigned char ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( FOAL_WeArePOAFS() == TRUE )
		{

			if( Language == ENGLISH )
			{

				snprintf( pdest, pdest_size, "SHORT: Pump, reported by \"%c\"", laddr );

			}
			else
			{

				snprintf( pdest, pdest_size, "SHORT: Bomba Salida, reported informado por \"%c\"", laddr );

			}

		}
		else
		{

			if( Language == ENGLISH )
			{

				snprintf( pdest, pdest_size, "SHORT: Pump" );

			}
			else
			{

				snprintf( pdest, pdest_size, "CORTO: Bomba Salida" );

			}

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_Short_COMBINATION_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;
	unsigned short total_on;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &total_on, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "SHORT: Combination suspected, %d outputs on", total_on );

		}
		else
		{

			snprintf( pdest, pdest_size, "CORTO: Combinacion sospecho, %d producciones en", total_on );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_Short_LIGHT_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;
	unsigned short station;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "SHORT: Lights %d suspected", station+1 );

		}
		else
		{

			snprintf( pdest, pdest_size, "CORTO: Luces %d sospecho", station+1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_OpenStation_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;
	unsigned short station;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "NO CURRENT: Station %d suspected", station+1 );

		}
		else
		{

			snprintf( pdest, pdest_size, "NO CORRIENTE: Estacion %d sospecho", station+1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_ClockSet_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, unsigned short paid, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	DATE_TIME legacy_fromtime, legacy_totime;

	DATE_TIME fromtime, totime;


	char creason[ 3 ][ 24 ] = { " (keypad)",
		" (comm)",
		" (daylight saving)"  };

	char s_creason[ 3 ][ 24 ] = {   " (keypad)",
		" (comm)",
		" (hora de verano)"  };

	char from_str_32[ 32 ], to_str_32[ 32 ];


	rv = 0;
	rv += ALERTS_pull_object_off_pile(ppile, &legacy_fromtime, pindex, sizeof(DATE_TIME));
	rv += ALERTS_pull_object_off_pile(ppile, &legacy_totime, pindex, sizeof(DATE_TIME));

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		fromtime = LEGACY_COMMON_swap_endianness_datetime( legacy_fromtime );

		totime = LEGACY_COMMON_swap_endianness_datetime( legacy_totime );


		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Clock Set from %s to %s%s", DATE_TIME_to_DateTimeStr_32( from_str_32, sizeof(from_str_32), fromtime ), DATE_TIME_to_DateTimeStr_32( to_str_32, sizeof(to_str_32), totime ), creason[ paid - LEGACY_AID_CLOCKSET_KEYPAD ] );

		}
		else
		{

			snprintf( pdest, pdest_size, "Reloj Cambio de %s a %s%s", DATE_TIME_to_DateTimeStr_32( from_str_32, sizeof(from_str_32), fromtime ), DATE_TIME_to_DateTimeStr_32( to_str_32, sizeof(to_str_32), totime ), s_creason[ paid - LEGACY_AID_CLOCKSET_KEYPAD ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_OverBudget_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv, us_pob;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &us_pob, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "USE EXPECTED TO EXCEED BUDGET BY %d%%", us_pob );

		}
		else
		{

			snprintf( pdest, pdest_size, "USO VA EXCEDER PRESUPUESTO POR %d%%", us_pob );

		}

	}


	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_HitStopTime_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;
	PROG_ASS_SIZE prog;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile(ppile, &prog, pindex, sizeof( PROG_ASS_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{


		if( Language == ENGLISH )
		{

			strlcat( pdest, E_ProgStrWithoutColon[ prog ], pdest_size );
			strlcat( pdest, " Hit Stop Time while irrigating", pdest_size );

		}
		else
		{

			strlcat( pdest, S_ProgStrWithoutColon[ prog ], pdest_size );
			strlcat( pdest, " Golpee la Tiempo Detenido al irrigar", pdest_size );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_SetHOByStation_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv, station;
	long ho;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &ho, pindex, sizeof( long ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Hold-Over Set: Station %d to %.1f mins", station+1, ho/60.0 ); 

		}
		else
		{

			snprintf( pdest, pdest_size, "Conjunto Remanente: Estacion %d a %.1f min", station+1, ho/60.0 ); 

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_SetHOByProg_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	PROG_ASS_SIZE lprog;
	unsigned short rv;
	long ho;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &lprog, pindex, sizeof( PROG_ASS_SIZE ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &ho, pindex, sizeof( long ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Hold-Over Set: %s to %.1f mins", E_ProgStrWithoutColon[ lprog ], ho/60.0 ); 

		}
		else
		{

			snprintf( pdest, pdest_size, "Conjunto Remanente: %s a %.1f min", S_ProgStrWithoutColon[ lprog ], ho/60.0 ); 

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_SetHOAllStations_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;
	long ho;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &ho, pindex, sizeof( long ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Hold-Over Set: All Stations to %.1f mins", ho/60.0 ); 

		}
		else
		{

			snprintf( pdest, pdest_size, "Conjunto Remanente: Todas Estaciones a %.1f min", ho/60.0 ); 

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_SetNOWDaysByStation_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv, station, nowdays;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &nowdays, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "No Water Days Set: Station %d to %d days", station+1, nowdays ); 

		}
		else
		{

			snprintf( pdest, pdest_size, "Conjunto Dias Sin Agua: Estacion %d a %d dias", station+1, nowdays ); 

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_SetNOWDaysByProg_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	PROG_ASS_SIZE lprog;
	unsigned short rv, nowdays;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile(ppile, &lprog, pindex, sizeof( PROG_ASS_SIZE ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &nowdays, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "No Water Days Set: %s to %d days", E_ProgStrWithoutColon[ lprog ], nowdays ); 

		}
		else
		{

			snprintf( pdest, pdest_size, "Conjunto Dias Sin Agua: %s a %d dias", S_ProgStrWithoutColon[ lprog ], nowdays ); 

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_SetNOWDaysAllStations_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv, nowdays;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &nowdays, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "No Water Days Set: All Stations to %d days", nowdays ); 

		}
		else
		{

			snprintf( pdest, pdest_size, "Conjunto Dias Sin Agua: Todas Estaciones %d dias", nowdays ); 

		}


	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_PasswordLogin_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	char lstr[ 16 ];
	unsigned short level, rv;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &level, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lstr, sizeof(lstr), pindex );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		// snprintf( pdest, pdest_size, "Level %d logged in: %s", level, lstr );
		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Level %d logged in: ***", level );

		}
		else
		{

			snprintf( pdest, pdest_size, "Nivel %d Entro: ***", level );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_RRE_PasswordLogin_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	char lstr_16[ 16 ], lstr_64[ 64 ];
	unsigned char level_uc;
	unsigned short rv;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &level_uc, pindex, sizeof( unsigned char ) );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lstr_16, sizeof(lstr_16), pindex );
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lstr_64, sizeof(lstr_64), pindex );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( (level_uc & 0x80) == 0x80 )
		{

			// fix up the level ie clear the high order bit
			//
			level_uc &= 0x7F;

			// if the high order bit is set the guy is actually logged in - if not he didn't need to
			// log in to make the changes
			//
			// snprintf( pdest, pdest_size, "Level %d logged in: %s", level, lstr );
			if( Language == ENGLISH )
			{

				snprintf( pdest, pdest_size, "Changes by RRe %s Level %d Logged In: ***", lstr_64, level_uc );

			}
			else
			{

				snprintf( pdest, pdest_size, "Cambios por RRe %s Nivel %d Conectados: ***", lstr_64, level_uc );

			}

		}
		else
		{

			// snprintf( pdest, pdest_size, "Level %d logged in: %s", level, lstr );
			if( Language == ENGLISH )
			{

				snprintf( pdest, pdest_size, "Changes by RRe %s - No login needed", lstr_64, level_uc );

			}
			else
			{

				snprintf( pdest, pdest_size, "Cambios por RRe %s - No Login necesario", lstr_64, level_uc );

			}

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_PasswordLogout_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, ALERT_ID aid, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short level, rv;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &level, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( aid == LEGACY_AID_PASSWORDS_LOGOUT_KEYPAD_TIMEOUT )
		{

			if( Language == ENGLISH )
			{

				snprintf( pdest, pdest_size, "Level %d Logged Out (keypad timeout)", level ); 

			}
			else
			{

				snprintf( pdest, pdest_size, "Nivel %d Apuntado Fuera (teclado descanso)", level ); 

			}

		}
		else
			if( aid == LEGACY_AID_PASSWORDS_LOGOUT_USER_LOGGED_OUT )
		{

			if( Language == ENGLISH )
			{

				snprintf( pdest, pdest_size, "Level %d Logged Out (user logged out)", level ); 

			}
			else
			{

				snprintf( pdest, pdest_size, "Nivel %d Apuntado Fuera (teclado descanso)", level ); 

			}

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_SystemCapacityAutomaticallyAdjusted_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	char lstr[ 16 ];
	unsigned short from, to, rv;

	rv = 0;
	rv += ALERTS_pull_string_off_pile( ppile, (char*)&lstr, sizeof(lstr), pindex );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &from, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &to, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "System Capacity Adjusted From %d To %d gpm for Station %s", from, to, lstr );

		}
		else
		{

			snprintf( pdest, pdest_size, "Numeros Max Corriente Ajustado de %d a %d gpm para Estacion %s", from, to, lstr );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_MVOR_Alert_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, ALERT_ID aid, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	LEGACY_INITIATED_VIA_ENUM_SIZE who_initiated;

	unsigned short closed;

	unsigned long seconds;


	char closed_text[ 2 ][ 8 ] = {

		"OPEN",
		"CLOSE"
	};

	char s_closed_text[ 2 ][ 8 ] = {

		"ABRIO",
		"CERRO"
	};

	rv = 0;

/*
	case AID_MVOR_REQUESTED:	HOW, CLOSED, SECONDS
	case AID_MVOR_STARTED:		CLOSED, SECONDS
	case AID_MVOR_CANCELLED:	HOW, CLOSED
	case AID_MVOR_STOPPED:		CLOSED

*/

	if( (aid == LEGACY_AID_MVOR_REQUESTED) || (aid == LEGACY_AID_MVOR_CANCELLED) )
	{

		rv += ALERTS_pull_legacy_object_off_pile( ppile, &who_initiated, pindex, sizeof( LEGACY_INITIATED_VIA_ENUM_SIZE ) );

	}

	// they all have the closed term
	//
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &closed, pindex, sizeof( unsigned short ) );


	if( (aid == LEGACY_AID_MVOR_REQUESTED) || (aid == LEGACY_AID_MVOR_STARTED) )
	{

		rv += ALERTS_pull_legacy_object_off_pile( ppile, &seconds, pindex, sizeof( unsigned long ) );

	}


	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		// now make the text based on the aid and what we've pulled
		//
		if( aid == LEGACY_AID_MVOR_REQUESTED )
		{

			if( Language == ENGLISH )
			{

				snprintf( pdest, pdest_size, "MVOR %s Requested for %.2f hours%s", closed_text[ closed ], seconds/3600.0, LEGACY_who_initiated_string[ who_initiated ] ); 

			}
			else
			{

				snprintf( pdest, pdest_size, "MVOR %s Solicito por %.2f horas%s", s_closed_text[ closed ], seconds/3600.0, LEGACY_s_who_initiated_string[ who_initiated ] ); 

			}

		}
		else
			if( aid == LEGACY_AID_MVOR_STARTED )
		{

			if( Language == ENGLISH )
			{

				snprintf( pdest, pdest_size, "MVOR %s Started for %.2f hours", closed_text[ closed ], seconds/3600.0 ); 

			}
			else
			{

				snprintf( pdest, pdest_size, "MVOR %s Empezo por %.2f horas", s_closed_text[ closed ], seconds/3600.0 ); 

			}

		}
		else
			if( aid == LEGACY_AID_MVOR_CANCELLED )
		{

			if( Language == ENGLISH )
			{

				snprintf( pdest, pdest_size, "MVOR %s Cancelled%s", closed_text[ closed ], LEGACY_who_initiated_string[ who_initiated ] ); 

			}
			else
			{

				snprintf( pdest, pdest_size, "MVOR %s Cancelado%s", s_closed_text[ closed ], LEGACY_s_who_initiated_string[ who_initiated ] ); 

			}

		}
		else
			if( aid == LEGACY_AID_MVOR_STOPPED )
		{

			if( Language == ENGLISH )
			{

				snprintf( pdest, pdest_size, "MVOR %s Ended", closed_text[ closed ] ); 

			}
			else
			{

				snprintf( pdest, pdest_size, "MVOR %s Termino", s_closed_text[ closed ] ); 

			}

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_ManualHOorPROG_Skipped_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, ALERT_ID paid, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{


	unsigned short rv;

	char lstr_16[ 16 ];

	LEGACY_REASONS_TO_SKIP_IRRIGATION_SIZE reason;


	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &reason, pindex, sizeof( LEGACY_REASONS_TO_SKIP_IRRIGATION_SIZE ) );


	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		lstr_16[ 0 ] = 0;

		if( Language == ENGLISH )
		{

			if( paid == LEGACY_AID_MANUALHO_SKIPPED )
			{

				strlcat( lstr_16, "HO", sizeof(lstr_16) );

			}
			else
			{

				strlcat( lstr_16, "PROGRAM", sizeof(lstr_16) );

			}

			snprintf( pdest, pdest_size, "Manual %s Skipped: %s", lstr_16, LEGACY_SkippedReasonsText[ reason ] );

		}
		else
		{

			if( paid == LEGACY_AID_MANUALHO_SKIPPED )
			{

				strlcat( lstr_16, "HO", sizeof(lstr_16) );

			}
			else
			{

				strlcat( lstr_16, "PROGRAMA", sizeof(lstr_16) );

			}

			snprintf( pdest, pdest_size, "Manual %s Saltado: %s", lstr_16, LEGACY_S_SkippedReasonsText[ reason ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_EEWriteInProcess_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short item, rv;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &item, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "EE WRITE IN PROCESS : %d items remaining", item );

		}
		else
		{

			snprintf( pdest, pdest_size, "EE ESCRIBA EN PROCESO : %d articulos que quedan", item );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
unsigned short GAGE_percentfull_us( unsigned short pamount )
{
#define	PULSES_IN_GAGE 1100

	unsigned short rv;

	rv = (unsigned short)( ( (float)pamount / PULSES_IN_GAGE ) * 100.0 );

	return( rv );
}

/* ---------------------------------------------------------- */
static unsigned short Pull_ETGAGE_percent_full_edited( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex)
{

	unsigned short from, to, rv;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &from, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &to, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Change: ET Gage Percent Full from %2d%% to %2d%% (keypad)", GAGE_percentfull_us( from ), GAGE_percentfull_us( to ) );

		}
		else
		{

			snprintf( pdest, pdest_size, "Cambio: Medidor de ET Por Ciento Repleto de %2d%% a %2d%% (teclado)", GAGE_percentfull_us( from ), GAGE_percentfull_us( to ) );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_ETGAGE_pulse( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short pulse, rv;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &pulse, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "ET GAGE PULSE %d", pulse );

		}
		else
		{

			snprintf( pdest, pdest_size, "MEDIDOR DE ET PULSO %d", pulse );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_RAIN_24HourTotal_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;
	float rain;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &rain, pindex, sizeof( float ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Midnight-to-Midnight Rainfall is %.2f inches", rain );

		}
		else
		{

			snprintf( pdest, pdest_size, "La Lluvia de la Medianoche-a-Medianoche es %.2f pulgadas", rain );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_ETGAGE_PercentFull_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;
	unsigned short percent_full;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &percent_full, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "ATTENTION Gage only %2d%% full", percent_full );

		}
		else
		{

			snprintf( pdest, pdest_size, "ATENCION Medidor de Et solo %2d%% repleto", percent_full );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_StopKey_withreason_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned char ljust_stopped;

	LEGACY_INITIATED_VIA_ENUM_SIZE who_initiated;

	char str_32[ 32 ];


	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &who_initiated, pindex, sizeof( LEGACY_INITIATED_VIA_ENUM_SIZE ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &ljust_stopped, pindex, sizeof( unsigned char ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "STOPPED %s%s", ReasonONString( str_32, sizeof(str_32), ljust_stopped ), LEGACY_who_initiated_string[ who_initiated ] );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "PARADO %s%s", ReasonONString( str_32, sizeof(str_32), ljust_stopped ), LEGACY_s_who_initiated_string[ who_initiated ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_IRRIGATION_TURNED_OFF_WITH_REASON_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	LEGACY_INITIATED_VIA_ENUM_SIZE who_initiated;


	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &who_initiated, pindex, sizeof( LEGACY_INITIATED_VIA_ENUM_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "SCHEDULED IRRIGATION TURNED OFF%s", LEGACY_who_initiated_string[ who_initiated ] );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "IRRIGACION PROGRAMADO APAGO%s", LEGACY_s_who_initiated_string[ who_initiated ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_IRRIGATION_TURNED_ON_WITH_REASON_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	LEGACY_INITIATED_VIA_ENUM_SIZE who_initiated;


	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &who_initiated, pindex, sizeof( LEGACY_INITIATED_VIA_ENUM_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "SCHEDULED IRRIGATION TURNED ON%s", LEGACY_who_initiated_string[ who_initiated ] );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "IRRIGACION PROGRAMADO PRENDIO%s", LEGACY_s_who_initiated_string[ who_initiated ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_HoldOverChanged_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;
	unsigned short station;
	long was, is;
	LEGACY_INITIATED_VIA_ENUM_SIZE who_initiated;

	rv = 0;
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &was, pindex, sizeof( long ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &is, pindex, sizeof( long ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &who_initiated, pindex, sizeof( LEGACY_INITIATED_VIA_ENUM_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "Sta %d Hold-Over from %.1f to %.1f%s", station+1, was/60.0, is/60.0, LEGACY_who_initiated_string[ who_initiated ] );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "Sta %d Remanente de %.1f a %.1f%s", station+1, was/60.0, is/60.0, LEGACY_s_who_initiated_string[ who_initiated ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_FlowCheckLearning_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	char addr;
	unsigned short station;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &addr, pindex, sizeof( unsigned char ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "Learning to Check Flow: Station %c%02d", addr, station + 1 );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "Aprender a Verificar el Flujo: Estacion %c%d", addr, station + 1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_Ping_SWREV_Error_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	char str_32[ 32 ];

	char addr;


	rv = 0;

	rv += ALERTS_pull_string_off_pile( ppile, (char*)&str_32, sizeof(str_32), pindex );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &addr, pindex, sizeof( char ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "Update ROMs at %c (has %s)", addr, str_32 );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "Actualice ROMs en %c (tiene %s)", addr, str_32 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_Ping_CommVer_Error_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short local_ver, remote_ver;

	char addr;


	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &local_ver, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &remote_ver, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &addr, pindex, sizeof( char ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "Update ROMs at %c (comm ver %03d, reqs %03d)", addr, local_ver, remote_ver );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "Actualice ROMs en %c (comm ver %03d, req %03d)", addr, local_ver, remote_ver );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_Programmed_Irrigation_Running_At_Starttime_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	PROG_ASS_SIZE prog;


	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &prog, pindex, sizeof( PROG_ASS_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "%s Still Running when Start Time Hit", E_ProgStrWithoutColon[ prog ] );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "%s Todavia Correr en Empieza Tiempo", S_ProgStrWithoutColon[ prog ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_ManualProgramIrrigating_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short which_program, which_start;


	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &which_program, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &which_start, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "START: Manual Prog %d, Start Time %d", which_program, which_start );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "COMIENZO: Prog Manual %d, Empiece Tiempo %d", which_program, which_start );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_TestStationStarted_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short station;

	LEGACY_INITIATED_VIA_ENUM_SIZE who_initiated;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &who_initiated, pindex, sizeof( LEGACY_INITIATED_VIA_ENUM_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "START: Test Station %d%s", station + 1, LEGACY_who_initiated_string[ who_initiated ] );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "COMIENZO: Prueba Estacion %d%s", station + 1, LEGACY_s_who_initiated_string[ who_initiated ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_TestProgramStarted_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	PROG_ASS_SIZE prog;

	LEGACY_INITIATED_VIA_ENUM_SIZE who_initiated;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &prog, pindex, sizeof( PROG_ASS_SIZE ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &who_initiated, pindex, sizeof( LEGACY_INITIATED_VIA_ENUM_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "START: Test %s%s", E_ProgStrWithoutColon[ prog ], LEGACY_who_initiated_string[ who_initiated ] );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "COMINEZO: Prueba %s%s", S_ProgStrWithoutColon[ prog ], LEGACY_s_who_initiated_string[ who_initiated ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_TestAllStarted_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	LEGACY_INITIATED_VIA_ENUM_SIZE who_initiated;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &who_initiated, pindex, sizeof( LEGACY_INITIATED_VIA_ENUM_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "START: Test All Stations%s", LEGACY_who_initiated_string[ who_initiated ] );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "COMINEZO: Prueba Todas Estaciones%s", LEGACY_s_who_initiated_string[ who_initiated ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_OldManualStationStarted_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short station;

	LEGACY_INITIATED_VIA_ENUM_SIZE who_initiated;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &who_initiated, pindex, sizeof( LEGACY_INITIATED_VIA_ENUM_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "START: Manual Station %d%s", station + 1, LEGACY_who_initiated_string[ who_initiated ] );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "COMINEZO: Manual Estacion %d%s", station + 1, LEGACY_s_who_initiated_string[ who_initiated ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_OldManualProgramStarted_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	PROG_ASS_SIZE prog;

	LEGACY_INITIATED_VIA_ENUM_SIZE who_initiated;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &prog, pindex, sizeof( PROG_ASS_SIZE ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &who_initiated, pindex, sizeof( LEGACY_INITIATED_VIA_ENUM_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "START: Manual %s%s", E_ProgStrWithoutColon[ prog ], LEGACY_who_initiated_string[ who_initiated ] );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "COMIENZO: Manual %s%s", S_ProgStrWithoutColon[ prog ], LEGACY_s_who_initiated_string[ who_initiated ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_OldManualAllStarted_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	LEGACY_INITIATED_VIA_ENUM_SIZE who_initiated;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &who_initiated, pindex, sizeof( LEGACY_INITIATED_VIA_ENUM_SIZE ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "START: Manual All Stations%s", LEGACY_who_initiated_string[ who_initiated ] );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "COMINEZO: Manual Todas Estaciones%s", LEGACY_s_who_initiated_string[ who_initiated ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_IrrigationEnded_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned char reason;

	char str_32[ 32 ];


	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &reason, pindex, sizeof( unsigned char ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "END: %s", ReasonONString( str_32, sizeof(str_32), reason ) );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "FIN: %s", ReasonONString( str_32, sizeof(str_32), reason ) );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
char *DTMF_how_string( char *pstr, const UNS_32 pstrlen, const unsigned short phow )
{
	#define DTMF_normalcommandsmode_keypad		10
	#define DTMF_normalcommandsmode_xmitted		20
	#define DTMF_addressedcommandsmode_keypad	30

	#define DTMF_deactivated_starstarstar		40
	#define DTMF_deactivated_timedout			50
	#define DTMF_deactivated_programrestart		60

	switch ( phow )
	{
		case DTMF_normalcommandsmode_keypad:
			strlcpy( pstr, "normal keypad", pstrlen );
			break;
	
		case DTMF_normalcommandsmode_xmitted:
			strlcpy( pstr, "normal received", pstrlen );
			break;
	
		case DTMF_addressedcommandsmode_keypad:
			strlcpy( pstr, "addressed keypad", pstrlen );
			break;
	
		case DTMF_deactivated_starstarstar:
			strlcpy( pstr, "*** received", pstrlen );
			break;
	
		case DTMF_deactivated_timedout:
			strlcpy( pstr, "timed out", pstrlen );
			break;
	
		case DTMF_deactivated_programrestart:
			strlcpy( pstr, "restart", pstrlen );
			break;

		default:
			strlcpy( pstr, "unknown case", pstrlen );
	}
	
	return( pstr );
}

/* ---------------------------------------------------------- */
static unsigned short Pull_DTMF_activated_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{
	unsigned short rv;

	unsigned short activated_how;

	char str_32[ 32 ];

	// ----------

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &activated_how, pindex, sizeof( unsigned short ) );
	
	if ( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		sp_strlcat( pdest, pdest_size, "Radio Remote Enabled (%s)", DTMF_how_string( str_32, sizeof(str_32), activated_how ) );
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
static unsigned short Pull_DTMF_deactivated_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short deactivated_how;

	char str_32[ 32 ];

	// ----------

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &deactivated_how, pindex, sizeof( unsigned short ) );
	
	if ( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		sp_strlcat( pdest, pdest_size, "Radio Remote Finished (%s)", DTMF_how_string( str_32, sizeof(str_32), deactivated_how ) );
	}
	
	return( rv );
}

/* ---------------------------------------------------------- */
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! change name to RRE
static unsigned short Pull_DTMF_station_on_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short station;


	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "Radio Remote Sta %d ON", station + 1 );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "Radio Remoto Sta %d Prendio", station + 1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_DTMF_station_off_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short station;


	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "Radio Remote Sta %d OFF", station + 1 );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "Radio Remoto Sta %d APAGO", station + 1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_Programmed_Irrigation_Started_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	PROG_ASS_SIZE prog;


	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile(ppile, &prog, pindex, sizeof( PROG_ASS_SIZE ));

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "START: Programmed Irrigation %s", E_ProgStrWithoutColon[ prog ] );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "COMIENZO: Irrigacion Programada %s", S_ProgStrWithoutColon[ prog ] );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_ETTable_Substitution_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	PROG_ASS_SIZE prog;
	unsigned short original_table_value_us_100u, new_value_us_100u;

	// using the new value instead of the original table value
	//
	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &prog, pindex, sizeof( PROG_ASS_SIZE ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &original_table_value_us_100u, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &new_value_us_100u, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "%s ET: using gage %.2f instead of historical %.2f", E_ProgStrWithoutColon[ prog ], new_value_us_100u/100.0, original_table_value_us_100u/100.0 );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "%s ET: utilizar mide %.2f en vez de historico %.2f", S_ProgStrWithoutColon[ prog ], new_value_us_100u/100.0, original_table_value_us_100u/100.0 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_ETTable_LimitedEntry_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	PROG_ASS_SIZE prog;
	unsigned short original_table_value_us_100u, new_value_us_100u;

	// limiting the original value to the new value
	//
	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &prog, pindex, sizeof( PROG_ASS_SIZE ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &original_table_value_us_100u, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &new_value_us_100u, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "%s ET: limiting gage %.2f to %.2f", E_ProgStrWithoutColon[ prog ], original_table_value_us_100u/100.0, new_value_us_100u/100.0 );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "%s ET: limitar mide %.2f a %.2f", S_ProgStrWithoutColon[ prog ], original_table_value_us_100u/100.0, new_value_us_100u/100.0 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_MOISTURE_SENSOR_READING_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;
	unsigned short station;
	unsigned char reading;
	MS_STATUS_SIZE status;
	unsigned char setpoint;
	BOOLEAN test_only;

	char str_16[ 16 ], str_32[ 32 ];


	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &reading, pindex, sizeof( unsigned char ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &status, pindex, sizeof( MS_STATUS_SIZE ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &setpoint, pindex, sizeof( unsigned char ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &test_only, pindex, sizeof( BOOLEAN ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{


		if( test_only == TRUE )
		{

			if( Language == ENGLISH )
			{

				strlcpy( str_16, " (test)", sizeof(str_16) );

			}
			else
			{

				strlcpy( str_16, " (prueba)", sizeof(str_16)  );

			}

		}
		else
		{

			strlcpy( str_16, "", sizeof(str_16)  );

		}


		// if the status is good only show the reading else only show text about the 
		// errant status
		//
		if( status == MSS_GOT_GOOD_READING )
		{

			if( Language == ENGLISH )
			{

				snprintf( str_32, sizeof(str_32), "Mois: Read %d", reading );

			}
			else
			{

				snprintf( str_32, sizeof(str_32), "Hume: Act %d", reading );

			}

			// only when read as part of programmed irrigation do we include the set point...this way
			// in the alerts you can see when the sensor crossed the set point and by how much...following
			// that you will see the stations falling out of irrigation	alert lines...then you have the
			// complete story
			//
			if( test_only == FALSE )
			{

				if( Language == ENGLISH )
				{

					sp_strlcat( str_32, sizeof(str_32), " Set %d", setpoint );

				}
				else
				{

					sp_strlcat( str_32, sizeof(str_32), " Req %d", setpoint );

				}

			}

		}
		else
			if( status == MSS_NO_FILTERED_READING )
		{

			if( Language == ENGLISH )
			{

				strlcpy( str_32, "COULDN'T READ SENSOR", sizeof(str_32) );

			}
			else
			{

				strlcpy( str_32, "NO PODRIA LEER SENSOR", sizeof(str_32) );

			}

		}
		else
			if( status == MSS_READING_OUT_OF_RANGE )
		{

			if( Language == ENGLISH )
			{

				strlcpy( str_32, "SENSOR READING TOO HIGH", sizeof(str_32) );

			}
			else
			{

				strlcpy( str_32, "SENSOR QUE LEE TAMBIEN ALTO", sizeof(str_32) );

			}

		}
		else
			if( status == MSS_SIGNAL_NEVER_WENT_AWAY )
		{

			if( Language == ENGLISH )
			{

				strlcpy( str_32, "SENSOR HARDWARE PROBLEM", sizeof(str_32) );

			}
			else
			{

				strlcpy( str_32, "PROBLEMA DE HARDWARE DE SENSOR", sizeof(str_32) );

			}

		}
		else
		{

			if( Language == ENGLISH )
			{

				strlcpy( str_32, "SENSOR unknown status", sizeof(str_32) );

			}
			else
			{

				strlcpy( str_32, "SENSOR el estado desconocido", sizeof(str_32) );

			}

		}


		//	Sta: %d Moisture: %d (%s)<TESTING>
		//
		sp_strlcat( pdest, pdest_size, "Sta %d %s%s", station+1, str_32, str_16 );

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_MOISTURE_SLAVE_ON_DIFFERENT_PROGRAM_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short station;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "Sta %d Ignoring Moisture (different program than master)", station + 1 );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "Sta %d Ignorar la Humedad (el programa diferente que amo)", station + 1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_MOISTURE_STATION_TERMINATED_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, ALERT_ID paid, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short station;
	unsigned short repeats;
	unsigned long seconds_left;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &repeats, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &seconds_left, pindex, sizeof( unsigned long ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		// Moisture Stopped Sta 12 after %d repeats( %5.1f mins remaining )
		//
		if( Language == ENGLISH )
		{

			sp_strlcat( pdest, pdest_size, "Moisture Stopped Sta %d after %d cycles (%5.1f mins remaining)", station+1, repeats, seconds_left/60.0 );

		}
		else
		{

			sp_strlcat( pdest, pdest_size, "La humedad Paro Sta %d despues de %d ciclos (%5.1f min que quedan)", station+1, repeats, seconds_left/60.0 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_COMM_COMMAND_RCVD_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	COMM_COMMANDS mid;
	unsigned char length;
	unsigned char raw[ ALERTS_MAX_STORAGE_OF_TEXT_CHARS + 1 ];

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &mid, pindex, sizeof( COMM_COMMANDS ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &length, pindex, sizeof( unsigned char ) );

	// only if length is >0 do we have some accompanying data to pull...but it should never exceed our 
	// accompanying data limit
	//
	if( length > 0 )
	{

		// of course could blow up here if length gets botched to bigger than the "raw" array
		// so protect that
		//
		if( length <= ALERTS_MAX_STORAGE_OF_TEXT_CHARS )
		{

			rv += ALERTS_pull_legacy_object_off_pile( ppile, &raw, pindex, length );

		}

	}


	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			// sanity check on the length
			//
			if( length > ALERTS_MAX_STORAGE_OF_TEXT_CHARS )
			{

				sp_strlcat( pdest, pdest_size, "COMM COMMAND length out of range" );

			}
			else
			{

				switch( mid )
				{
					
					case MID_PING_FROM_PC:
						sp_strlcat( pdest, pdest_size, "Command: Ping from pc" );
						break;  

					case MID_CMOS_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: CMOS to pc" );
						break;  

					case MID_CMOS_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: write CMOS" );
						break;  

					case MID_PDATA_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: Program Data to pc" );
						break;  

					case MID_PDATA_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: write Program Data" );
						break;  

					case MID_ALERTS_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: Alerts to pc" );
						break;  

					case MID_SETTIMEDATE:
						sp_strlcat( pdest, pdest_size, "Command: Set Time & Date" );
						break;  

					case MID_CLEARMLB:
						sp_strlcat( pdest, pdest_size, "Command: Clear Mainline Break" );
						break;  

					case MID_SETHO_ALL_STATIONS:
						sp_strlcat( pdest, pdest_size, "Command: Set HO All Stations" );
						break;  

					case MID_SETHO_BY_PROG:
						sp_strlcat( pdest, pdest_size, "Command: Set HO by Program" );
						break;  

					case MID_SETHO_BY_STATION:
						sp_strlcat( pdest, pdest_size, "Command: Set HO by Station" );
						break;  

					case MID_SETNOWDAYS_ALL_STATIONS:
						sp_strlcat( pdest, pdest_size, "Command: Set No Water All Stations" );
						break;  

					case MID_SETNOWDAYS_BY_PROG:
						sp_strlcat( pdest, pdest_size, "Command: Set No Water by Program" );
						break;  

					case MID_SETNOWDAYS_BY_STATION:
						sp_strlcat( pdest, pdest_size, "Command: Set No Water by Station" );
						break;  

					case MID_TURNON:
						sp_strlcat( pdest, pdest_size, "Command: Scheduled Irrigation ON" );
						break;  

					case MID_TURNOFF:
						sp_strlcat( pdest, pdest_size, "Command: Scheduled Irrigation OFF" );
						break;  

					case MID_PASSWORDS_RESET:
						sp_strlcat( pdest, pdest_size, "Command: Passwords Reset" );
						break;  

					case MID_PASSWORDS_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: Send Passwords to pc" );
						break;  

					case MID_PASSWORDS_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: Write Passwords" );
						break;  

					case MID_PASSWORDS_UNLK:
						sp_strlcat( pdest, pdest_size, "Command: Unlock Passwords" );
						break;  

					case MID_SHARING_GET_ET:
						sp_strlcat( pdest, pdest_size, "Command: ET to pc" );
						break;  

					case MID_SHARING_GET_RAIN:
						sp_strlcat( pdest, pdest_size, "Command: RAIN to pc" );
						break;  

					case MID_SHARING_ETRAIN_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: write ET / RAIN" );
						break;  

					case MID_SHARING_RAIN_POLL:
						sp_strlcat( pdest, pdest_size, "Command: Rain Poll" );
						break;  

					case MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: Rain Shutdown" );
						break;  

					case MID_ENABLE_FOAL_DEBUG_RECORD:
						sp_strlcat( pdest, pdest_size, "Command: Enable FlowSense debug xmit" );
						break;  

					case MID_SEND_FLOW_RECORDER_RECORDS_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: Flow Recorder records to pc" );
						break;  

					case MID_LOGLINES_CONTROLLER_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: Station History to pc" );
						break;  

					case MID_CONTROLLER_REPORT_DATA_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: Controller Report Data to pc" );
						break;  

					case MID_LEGACY_STATION_REPORT_DATA_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: Station Report Data to pc" );
						break;  

					case MID_MVOR_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: request simple MV Override" );
						break;  

					case MID_LIGHTS_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: Lights to pc" );
						break;  

					case MID_LIGHTS_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: write Lights" );
						break;  

					case MID_MANUALSTUFF_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: Manual Programs to pc" );
						break;  

					case MID_MANUALSTUFF_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: write Manual Programs" );
						break;  



					case MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: DA start/stop" );
						break;  

					case MID_DIRECTACCESS_REFRESH_REQUEST_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: DA display refresh" );	// first way
						break;  

					case MID_DIRECTACCESS_STARTTIMER_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: DA display refresh" );	// second way
						break;  

					case MID_DIRECTACCESS_CHANGESCREEN_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: DA change screen" );
						break;  

					case MID_DIRECTACCESS_STOPKEY_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: DA Stop Key" );
						break;  

					case MID_DIRECTACCESS_TEST_ALL_TO_CONTROLLER:
					case MID_DIRECTACCESS_TEST_PROG_TO_CONTROLLER:
					case MID_DIRECTACCESS_TEST_STATION_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: DA Test" );
						break;  

					case MID_DIRECTACCESS_MANUAL_ALL_TO_CONTROLLER:
					case MID_DIRECTACCESS_MANUAL_PROG_TO_CONTROLLER:
					case MID_DIRECTACCESS_MANUAL_STATION_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: DA Manual" );
						break;  

					case MID_DIRECTACCESS_TURN_ON_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: DA Turn ON" );
						break;  

					case MID_DIRECTACCESS_TURN_OFF_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: DA Turn OFF" );
						break;  

					case MID_DIRECTACCESS_GOTO_STATION_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: DA Change Station" );
						break;  

					case MID_DIRECTACCESS_MVOR_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: DA MV Override" );
						break;  



					case MID_TEST_PACKET_TO_CONTROLLER_TO_ECHO:
						sp_strlcat( pdest, pdest_size, "Command: comm test packet rcvd" );
						break;  


					case MID_DERATE_TABLE_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: Flow Table to pc" );
						break;  

					case MID_DERATE_TABLE_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: write Flow Table" );
						break;  

					case MID_TEST_PACKET_BETWEEN_CONTROLLERS_TO_ECHO:
						sp_strlcat( pdest, pdest_size, "Command: comm test packet rcvd" );
						break;          

					case MID_TEST_PACKET_BETWEEN_CONTROLLERS_TO_ECHO_RESP:
						sp_strlcat( pdest, pdest_size, "Command: comm test resp rcvd" );
						break;          


					case MID_SETNOWDAYS_BY_PTAG:

						// NULL terminate the program tag string in case the full
						// 20 characters are used.
						//
						raw[ PTAG_LENGTH ] = 0x00;

						sp_strlcat( pdest, pdest_size, "Command: Set No Water by Program Tag - %s", (char*)raw );
						break;

					case MID_SETSTARTTIME_BY_PTAG:

						// NULL terminate the program tag string in case the full
						// 20 characters are used.
						//
						raw[ PTAG_LENGTH ] = 0x00;

						sp_strlcat( pdest, pdest_size, "Command: Set Start Time by Program Tag - %s", (char*)raw );

						break;

					case MID_SETSTOPTIME_BY_PTAG:

						// NULL terminate the program tag string in case the full
						// 20 characters are used.
						//
						raw[ PTAG_LENGTH ] = 0x00;

						sp_strlcat( pdest, pdest_size, "Command: Set Stop Time by Program Tag - %s", (char*)raw );
						break;

					case MID_SETHO_BY_PTAG:

						// NULL terminate the program tag string in case the full
						// 20 characters are used.
						//
						raw[ PTAG_LENGTH ] = 0x00;

						sp_strlcat( pdest, pdest_size, "Command: Set HO by Program Tag - %s", (char*)raw );
						break;

					case MID_PERCENTADJUST_BY_PTAG:

						// NULL terminate the program tag string in case the full
						// 20 characters are used.
						//
						raw[ PTAG_LENGTH ] = 0x00;

						sp_strlcat( pdest, pdest_size, "Command: Percent Adjust by Program Tag - %s", (char*)raw );
						break;

					case MID_PERCENTADJUST_ALL_PROGS:
						sp_strlcat( pdest, pdest_size, "Command: Percent Adjust all Programs" );
						break;

					case MID_PERCENTADJUST_BY_PROG:
						sp_strlcat( pdest, pdest_size, "Command: Percent Adjust by Program" );
						break;


					case MID_LIGHTS_COMMAND_FROM_PC:
						sp_strlcat( pdest, pdest_size, "Command: Lights Override" );
						break;


					case MID_SEND_DMC_RECORDER_RECORDS_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: Bypass Manifold Recorder records to pc" );
						break;  

					case MID_PDATA_CHANGES_TO_PC:
						sp_strlcat( pdest, pdest_size, "Command: Program Data changes to pc" );
						break;

					case MID_PDATA_CHANGES_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Command: write Program Data changes" );
						break;  

					default:
						sp_strlcat( pdest, pdest_size, "Comm Command %d w/ %d bytes", mid, length );
						break;


				}

			}

		}
		else
		{

			// sanity check on the length
			//
			if( length > ALERTS_MAX_STORAGE_OF_TEXT_CHARS )
			{

				sp_strlcat( pdest, pdest_size, "ORDEN DE COMM la longitud fuera de gama" );

			}
			else
			{

				switch( mid )
				{
					
					case MID_PING_FROM_PC:
						sp_strlcat( pdest, pdest_size, "Orden: Silbido de bala de la PC" );
						break;  

					case MID_CMOS_TO_PC:
						sp_strlcat( pdest, pdest_size, "Orden: CMOS a la PC" );
						break;  

					case MID_CMOS_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: escriba CMOS" );
						break;  

					case MID_PDATA_TO_PC:
						sp_strlcat( pdest, pdest_size, "Orden: Programa los Datos a la PC" );
						break;  

					case MID_PDATA_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: escriba el Programa los Datos" );
						break;  

					case MID_ALERTS_TO_PC:
						sp_strlcat( pdest, pdest_size, "Orden: Alertas a la PC" );
						break;  

					case MID_SETTIMEDATE:
						sp_strlcat( pdest, pdest_size, "Orden: Tiempo & Fecha Fijo" );
						break;  

					case MID_CLEARMLB:
						sp_strlcat( pdest, pdest_size, "Orden: Vacie Linea Principal Rota" );
						break;  

					case MID_SETHO_ALL_STATIONS:
						sp_strlcat( pdest, pdest_size, "Orden: Remanente fijo Todas Estaciones" );
						break;  

					case MID_SETHO_BY_PROG:
						sp_strlcat( pdest, pdest_size, "Orden: Remanente fijo por el Programa" );
						break;  

					case MID_SETHO_BY_STATION:
						sp_strlcat( pdest, pdest_size, "Orden: Remanente fijo por la Estacion" );
						break;  

					case MID_SETNOWDAYS_ALL_STATIONS:
						sp_strlcat( pdest, pdest_size, "Orden: Dias Sin Agua fijo por Todas Estaciones" );
						break;  

					case MID_SETNOWDAYS_BY_PROG:
						sp_strlcat( pdest, pdest_size, "Orden: Dias Sin Agua fijo por el Programa" );
						break;  

					case MID_SETNOWDAYS_BY_STATION:
						sp_strlcat( pdest, pdest_size, "Orden: Dias Sin Agua fijo por la Estacion" );
						break;  

					case MID_TURNON:
						sp_strlcat( pdest, pdest_size, "Orden: Irrigacion Programado PRENDA" );
						break;  

					case MID_TURNOFF:
						sp_strlcat( pdest, pdest_size, "Orden: Irrigacion Programado APAGUE" );
						break;  

					case MID_PASSWORDS_RESET:
						sp_strlcat( pdest, pdest_size, "Orden: Codigos de Acceso Reponen" );
						break;  

					case MID_PASSWORDS_TO_PC:
						sp_strlcat( pdest, pdest_size, "Orden: mande los Codigos de Acceso a la PC" );
						break;  

					case MID_PASSWORDS_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: escribe Codigos de Acceso" );
						break;  

					case MID_PASSWORDS_UNLK:
						sp_strlcat( pdest, pdest_size, "Orden: abra los Codigos de Acceso" );
						break;  

					case MID_SHARING_GET_ET:
						sp_strlcat( pdest, pdest_size, "Orden: ET a la PC" );
						break;  

					case MID_SHARING_GET_RAIN:
						sp_strlcat( pdest, pdest_size, "Orden: LLUVIA a la PC" );
						break;  

					case MID_SHARING_ETRAIN_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: escribe ET / LLUVIA" );
						break;  

					case MID_SHARING_RAIN_POLL:
						sp_strlcat( pdest, pdest_size, "Orden: Llueva el Sondeo" );
						break;  

					case MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: Llueva el Cierre" );
						break;  

					case MID_ENABLE_FOAL_DEBUG_RECORD:
						sp_strlcat( pdest, pdest_size, "Orden: Permita FLOWSENSE depura transmite" );
						break;  

					case MID_SEND_FLOW_RECORDER_RECORDS_TO_PC:
						sp_strlcat( pdest, pdest_size, "Orden: Registrador del Flujo a la PC" );
						break;  

					case MID_LOGLINES_CONTROLLER_TO_PC:
						sp_strlcat( pdest, pdest_size, "Orden: Estacion Diario a la PC" );
						break;  

					case MID_CONTROLLER_REPORT_DATA_TO_PC:
						sp_strlcat( pdest, pdest_size, "Orden: Datos del Informe del Reloj a la PC" );
						break;  

					case MID_MVOR_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: peticion MV sobrederecho simple" );
						break;  

					case MID_LIGHTS_TO_PC:
						sp_strlcat( pdest, pdest_size, "Orden: Luces a la PC" );
						break;  

					case MID_LIGHTS_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: escribe Luces" );
						break;  

					case MID_MANUALSTUFF_TO_PC:
						sp_strlcat( pdest, pdest_size, "Orden: Programas Manual a la PC" );
						break;  

					case MID_MANUALSTUFF_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: escribe Programas Manual" );
						break;  

					case MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: DA partida/parada" );
						break;  

					case MID_DIRECTACCESS_REFRESH_REQUEST_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: DA actualizacion de la pantalla" );	// first way
						break;  

					case MID_DIRECTACCESS_STARTTIMER_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: DA actualizacion de la pantalla" );	// second way
						break;  

					case MID_DIRECTACCESS_CHANGESCREEN_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: DA cambie la pantalla" );
						break;  

					case MID_DIRECTACCESS_STOPKEY_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: DA Tecla para Parar" );
						break;  

					case MID_DIRECTACCESS_TEST_ALL_TO_CONTROLLER:
					case MID_DIRECTACCESS_TEST_PROG_TO_CONTROLLER:
					case MID_DIRECTACCESS_TEST_STATION_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: DA Prueba" );
						break;  

					case MID_DIRECTACCESS_MANUAL_ALL_TO_CONTROLLER:
					case MID_DIRECTACCESS_MANUAL_PROG_TO_CONTROLLER:
					case MID_DIRECTACCESS_MANUAL_STATION_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: DA Manual" );
						break;  

					case MID_DIRECTACCESS_TURN_ON_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: DA Prenda" );
						break;  

					case MID_DIRECTACCESS_TURN_OFF_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: DA Apague" );
						break;  

					case MID_DIRECTACCESS_GOTO_STATION_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: DA Cambie la Estacion" );
						break;  

					case MID_DIRECTACCESS_MVOR_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: DA MV Sobrederecho" );
						break;  

					case MID_TEST_PACKET_TO_CONTROLLER_TO_ECHO:
						sp_strlcat( pdest, pdest_size, "Orden: paquete de la prueba recibio" );
						break;  


					case MID_DERATE_TABLE_TO_PC:
						sp_strlcat( pdest, pdest_size, "Orden: Tabla del Flujo a la PC" );
						break;  

					case MID_DERATE_TABLE_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: escribe Tabla del Flujo" );
						break;  

					case MID_TEST_PACKET_BETWEEN_CONTROLLERS_TO_ECHO:
						sp_strlcat( pdest, pdest_size, "Orden: paquete de la prueba recibio" );
						break;          

					case MID_TEST_PACKET_BETWEEN_CONTROLLERS_TO_ECHO_RESP:
						sp_strlcat( pdest, pdest_size, "Orden: respuesta al paquete de la prueba recibido" );
						break;          


						// 606.a.1 ajv - Added communications commands for by program tag
						// commands.
						//
					case MID_SETNOWDAYS_BY_PTAG:

						// NULL terminate the program tag string in case the full
						// 20 characters are used.
						//
						raw[ PTAG_LENGTH ] = 0x00;

						sp_strlcat( pdest, pdest_size, "Orden: Dias Sin Agua fijo por Etiqueta de Programa %s", (char*)raw );
						break;

					case MID_SETSTARTTIME_BY_PTAG:

						// NULL terminate the program tag string in case the full
						// 20 characters are used.
						//
						raw[ PTAG_LENGTH ] = 0x00;

						sp_strlcat( pdest, pdest_size, "Orden: Tiempo de Fijo del Comienzo por Etiqueta de Programa %s", (char*)raw );
						break;

					case MID_SETSTOPTIME_BY_PTAG:

						// NULL terminate the program tag string in case the full
						// 20 characters are used.
						//
						raw[ PTAG_LENGTH ] = 0x00;

						sp_strlcat( pdest, pdest_size, "Orden: Tiempo Detenido por Etiqueta de Programa %s", (char*)raw );
						break;

					case MID_SETHO_BY_PTAG:

						// NULL terminate the program tag string in case the full
						// 20 characters are used.
						//
						raw[ PTAG_LENGTH ] = 0x00;

						sp_strlcat( pdest, pdest_size, "Orden: Remanente Fijo por Etiqueta de Programa %s", (char*)raw );
						break;

					case MID_PERCENTADJUST_BY_PTAG:

						// NULL terminate the program tag string in case the full
						// 20 characters are used.
						//
						raw[ PTAG_LENGTH ] = 0x00;

						sp_strlcat( pdest, pdest_size, "Orden: Ajuste Por ciento por Etiqueta de Programa %s", (char*)raw );
						break;

					case MID_PERCENTADJUST_ALL_PROGS:
						sp_strlcat( pdest, pdest_size, "Orden: Ajuste Por ciento todos Programas" );
						break;

					case MID_PERCENTADJUST_BY_PROG:
						sp_strlcat( pdest, pdest_size, "Orden: Ajuste Por ciento por Programa" );
						break;

					case MID_LIGHTS_COMMAND_FROM_PC:
						sp_strlcat( pdest, pdest_size, "Orden: Luces Sobrederecho" );
						break;

					case MID_SEND_DMC_RECORDER_RECORDS_TO_PC:
						sp_strlcat( pdest, pdest_size, "Orden: Registrador de Bypass Manifold a la PC" );
						break;  

					case MID_PDATA_CHANGES_TO_PC:
						sp_strlcat( pdest, pdest_size, "Orden: Cambios a Programa los Datos a la PC" );
						break;  

					case MID_PDATA_CHANGES_TO_CONTROLLER:
						sp_strlcat( pdest, pdest_size, "Orden: escriba el Cambios a Programa los Datos" );
						break;  

					default:
						sp_strlcat( pdest, pdest_size, "Orden de Comm %d con %d byte", mid, length );
						break;


				}

			}

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_LIGHTS_Activity_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, ALERT_ID paid, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short station, reason;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &station, pindex, sizeof( unsigned short ) );
	rv += ALERTS_pull_legacy_object_off_pile( ppile, &reason, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		snprintf( pdest, pdest_size, "Lights %d turned ", station+1 );

		if( paid == LEGACY_AID_LIGHTS_STATION_OFF )
		{

			strlcat( pdest, "OFF", pdest_size );

		}
		else
		{

			strlcat( pdest, "ON", pdest_size );

		}

		if( reason == LIGHTS_REASON__ON_DUE_TO_COMMUNICATIONS )
		{

			strlcat( pdest, " (comm)", pdest_size );

		}
		else
			if( reason == LIGHTS_REASON__ON_DUE_TO_MANUALLY_STARTED )
		{

			strlcat( pdest, " (keypad)", pdest_size );

		}
		else
			if( reason == LIGHTS_REASON__ON_DUE_TO_ON_TIME )
		{

			strlcat( pdest, " (start time)", pdest_size );

		}
		else
			if( reason == LIGHTS_REASON__OFF_DUE_TO_OFF_TIME )
		{

			strlcat( pdest, " (stop time)", pdest_size );

		}
		else
			if( reason == nlu_LIGHTS_REASON__OFF_DUE_TO_PROGRAM_RESTART )
		{

			strlcat( pdest, " (program restart)", pdest_size );

		}
		else
			if( reason == LIGHTS_REASON__OFF_DUE_TO_STOP_KEY )
		{

			strlcat( pdest, " (keypad)", pdest_size );

		}
		else
			if( reason == nlu_LIGHTS_REASON__OFF_DUE_TO_TIME_EXPIRED )
		{

			strlcat( pdest, " (stop time)", pdest_size );

		}
		else
			if( reason == LIGHTS_REASON__OFF_DUE_TO_USER_TURNED_OFF )
		{

			strlcat( pdest, " (keypad)", pdest_size );

		}

	}


	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_WIND_alerts_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, ALERT_ID paid, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short speed;


	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &speed, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			if( paid == LEGACY_AID_WIND_PAUSE )
			{

				snprintf( pdest, pdest_size, "Wind: Paused at %d mph", speed );

			}
			else
				if( paid == LEGACY_AID_WIND_RESUME )
			{

				snprintf( pdest, pdest_size, "Wind: Resumed at %d mph", speed );

			}
			else
			{

				snprintf( pdest, pdest_size, "Wind: UNKNOWN!! at %d mph", speed );

			}

		}
		else
		{

			if( paid == LEGACY_AID_WIND_PAUSE )
			{

				snprintf( pdest, pdest_size, "Viento: Paused Detenido en %d mph", speed );

			}
			else
				if( paid == LEGACY_AID_WIND_RESUME )
			{

				snprintf( pdest, pdest_size, "Viento: Reasumido en %d mph", speed );

			}
			else
			{

				snprintf( pdest, pdest_size, "Viento: DESCONOCIDO!! en %d mph", speed );

			}
		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_PAUSE_RESUME_alerts_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, ALERT_ID paid, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short why;


	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &why, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			if( paid == LEGACY_AID_IRRIGATION_PAUSED )
			{

				snprintf( pdest, pdest_size, "Irrigation Paused: " );

			}
			else
				if( paid == LEGACY_AID_IRRIGATION_RESUMED )
			{

				snprintf( pdest, pdest_size, "Irrigation Resumed: " );

			}


			if( why == LEGACY_PR_DUE_TO_WIND )
			{

				strlcat( pdest, "WIND", pdest_size );

			}
			else
				if( why == LEGACY_PR_DUE_TO_KEYPAD )
			{

				strlcat( pdest, "KEYPAD", pdest_size );

			}
			else
				if( why == LEGACY_PR_DUE_TO_CENTRAL_COMMLINK )
			{

				strlcat( pdest, "CENTRAL", pdest_size );

			}
			else
			{

				strlcat( pdest, "unknown", pdest_size );

			}

		}
		else
		{

			if( paid == LEGACY_AID_IRRIGATION_PAUSED )
			{

				snprintf( pdest, pdest_size, "Irrigacion se Detuvo: " );

			}
			else
				if( paid == LEGACY_AID_IRRIGATION_RESUMED )
			{

				snprintf( pdest, pdest_size, "Irrigacion Reasumio: " );

			}


			if( why == LEGACY_PR_DUE_TO_WIND )
			{

				strlcat( pdest, "VIENTO", pdest_size );

			}
			else
				if( why == LEGACY_PR_DUE_TO_KEYPAD )
			{

				strlcat( pdest, "KEYPAD", pdest_size );

			}
			else
				if( why == LEGACY_PR_DUE_TO_CENTRAL_COMMLINK )
			{

				strlcat( pdest, "CENTRAL", pdest_size );

			}
			else
			{

				strlcat( pdest, "desconocido", pdest_size );

			}

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_POC_MainlineBreak_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	// This function is responsible to build the string for the mainline break alert in pdest.
	// It needs to keep pindex current to the next place the data comes from.
	// Must return how many bytes it pulls off of the pile for this alert.
	//
	// pdest points to a string and can be concatenated to as a string
	//

/* 
"Mainline Break POC 1: measured 456 gpm / allowed 123 gpm"
*/

	unsigned short rv;

	unsigned short poc_index, measured, allowed;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &poc_index, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &measured, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &allowed, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		snprintf( pdest, pdest_size, "MAINLINE BREAK POC %d: measured %d gpm / allowed %d gpm", poc_index + 1, measured, allowed );

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_POC_MainlineBreak_reminder_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short poc_index;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &poc_index, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "MAINLINE BREAK POC %d (reminder)", poc_index + 1 );

		}
		else
		{

			snprintf( pdest, pdest_size, "LINEA PRINCIPAL ROTA POC %d (recordatorio)", poc_index + 1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_POC_MainlineBreak_cleared_keypad_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short poc_index;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &poc_index, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Mainline Break Cleared POC %d (keypad)", poc_index + 1 );

		}
		else
		{

			snprintf( pdest, pdest_size, "Linea Princial Rota Vaciado POC %d (teclado)", poc_index + 1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_POC_MainlineBreak_cleared_commlink_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short poc_index;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &poc_index, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Mainline Break Cleared POC %d (comm)", poc_index + 1 );

		}
		else
		{

			snprintf( pdest, pdest_size, "Linea Princial Rota Vaciado POC %d (comm)", poc_index + 1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_POC_MainlineBreak_cleared_short_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short poc_index;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &poc_index, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Mainline Break Cleared POC %d (MV Short)", poc_index + 1);

		}
		else
		{

			snprintf( pdest, pdest_size, "Linea Princial Rota Vaciado POC %d (MV Corto)", poc_index + 1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_POC_MV_Short_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short poc_index;

	unsigned char laddr;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &poc_index, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &laddr, pindex, sizeof( unsigned char ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			if( FOAL_WeArePOAFS() == TRUE )
			{

				snprintf( pdest, pdest_size, "SHORT: Master Valve %d, reported by \"%c\"", poc_index + 1, laddr );

			}
			else
			{

				snprintf( pdest, pdest_size, "SHORT: Master Valve %d", poc_index + 1 );

			}   

		}
		else
		{

			if( FOAL_WeArePOAFS() == TRUE )
			{

				snprintf( pdest, pdest_size, "CORTO: Valvula Maestra %d, informado por \"%c\"", poc_index + 1, laddr );

			}
			else
			{

				snprintf( pdest, pdest_size, "CORTO: Valvula Maestra %d", poc_index + 1 );

			}   

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_POC_MV_Short_During_MLB_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short poc_index;

	unsigned short how_many_repeats;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &poc_index, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &how_many_repeats, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "SHORT: MV %d during MLB (Repeats: %d)", poc_index + 1, how_many_repeats );

		}
		else
		{

			snprintf( pdest, pdest_size, "CORTO: MV %d durante MLB (Repite: %d)", poc_index + 1, how_many_repeats );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_POC_Hit_StartTime_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short poc_index;

	unsigned long seconds;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &poc_index, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &seconds, pindex, sizeof( unsigned long ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			if( seconds < 3600 )
			{

				snprintf( pdest, pdest_size, "POC %d Use Started for %d min", poc_index + 1, seconds / 60 );

			}
			else
			{

				snprintf( pdest, pdest_size, "POC %d Use Started for %d hr %d min", poc_index + 1, seconds / 3600, seconds % 3600 / 60 );

			}

		}
		else
		{

			if( seconds < 3600 )
			{

				snprintf( pdest, pdest_size, "Uso de POC %d Empezo por %d min", poc_index + 1, seconds / 60 );

			}
			else
			{

				snprintf( pdest, pdest_size, "Uso de POC %d Empezo por %d hr %d min", poc_index + 1, seconds / 3600, seconds % 3600 / 60 );

			}

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_POC_Hit_EndTime_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short poc_index;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &poc_index, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "POC %d Use Ended", poc_index + 1 );

		}
		else
		{

			snprintf( pdest, pdest_size, "Uso de POC %d Termino", poc_index + 1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_POC_Stopped_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short poc_index;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &poc_index, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "POC %d Use Stopped", poc_index + 1 );

		}
		else
		{

			snprintf( pdest, pdest_size, "Uso de POC %d Paro", poc_index + 1 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_UserDefined_ProgramTag_Changed_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short prog, rv;

	char from_str_21[ 21 ], to_str_21[ 21 ];

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &prog, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_string_off_pile( ppile, (char*)&from_str_21, sizeof(from_str_21), pindex );

	rv += ALERTS_pull_string_off_pile( ppile, (char*)&to_str_21, sizeof(to_str_21), pindex );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Change: %s User Defined Tag from %s to %s", E_ProgStrWithoutColon[ prog ], from_str_21, to_str_21 );


		}
		else
		{

			snprintf( pdest, pdest_size, "Cambio: %s Usuario Definio Etiqueta de %s a %s", S_ProgStrWithoutColon[ prog ], from_str_21, to_str_21 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_ProgramTag_Changed_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short prog, rv;

	char from_str_21[ 21 ], to_str_21[ 21 ];

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &prog, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_string_off_pile( ppile, (char*)&from_str_21, sizeof(from_str_21), pindex );

	rv += ALERTS_pull_string_off_pile( ppile, (char*)&to_str_21, sizeof(to_str_21), pindex );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( pdest, pdest_size, "Change: %s Tag from %s to %s", E_ProgStrWithoutColon[ prog ], from_str_21, to_str_21 );


		}
		else
		{

			snprintf( pdest, pdest_size, "Cambio: %s Etiqueta de %s a %s", S_ProgStrWithoutColon[ prog ], from_str_21, to_str_21 );

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_PercentAdjustAllProgs_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short percentage;

	unsigned short days;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &percentage, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &days, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			if( percentage == 100 )
			{

				if( days == PERCENT_ADJUST_NO_EXPIRE )
				{

					snprintf( pdest, pdest_size, "Percent Adjust Set: All Programs to %d%% forever", percentage-100 ); 

				}
				else
				{

					snprintf( pdest, pdest_size, "Percent Adjust Set: All Programs to %d%% for %d days", percentage-100, days ); 

				}

			}
			else if( percentage > 100 )
			{

				if( days == PERCENT_ADJUST_NO_EXPIRE )
				{

					snprintf( pdest, pdest_size, "Percent Adjust Set: All Programs to +%d%% forever", percentage-100 ); 

				}
				else
				{

					snprintf( pdest, pdest_size, "Percent Adjust Set: All Programs to +%d%% for %d days", percentage-100, days ); 

				}

			}
			else // if ( percentage < 100 )
			{

				if( days == PERCENT_ADJUST_NO_EXPIRE )
				{

					snprintf( pdest, pdest_size, "Percent Adjust Set: All Programs to -%d%% forever", 100-percentage ); 

				}
				else
				{

					snprintf( pdest, pdest_size, "Percent Adjust Set: All Programs to -%d%% for %d days", 100-percentage, days ); 

				}

			}

		}
		else
		{

			if( percentage == 100 )
			{

				if( days == PERCENT_ADJUST_NO_EXPIRE )
				{

					snprintf( pdest, pdest_size, "Conjunto Ajuste Por ciento: Todos Programas a %d%% por siempre", percentage-100 ); 

				}
				else
				{

					snprintf( pdest, pdest_size, "Conjunto Ajuste Por ciento: Todos Programas a %d%% por %d dias", percentage-100, days ); 

				}

			}
			else if( percentage > 100 )
			{

				if( days == PERCENT_ADJUST_NO_EXPIRE )
				{

					snprintf( pdest, pdest_size, "Conjunto Ajuste Por ciento: Todos Programas a +%d%% por siempre", percentage-100 ); 

				}
				else
				{

					snprintf( pdest, pdest_size, "Conjunto Ajuste Por ciento: Todos Programas a +%d%% por %d dias", percentage-100, days ); 

				}

			}
			else // if ( percentage < 100 )
			{

				if( days == PERCENT_ADJUST_NO_EXPIRE )
				{

					snprintf( pdest, pdest_size, "Conjunto Ajuste Por ciento: Todos Programas a -%d%% por siempre", 100-percentage ); 

				}
				else
				{

					snprintf( pdest, pdest_size, "Conjunto Ajuste Por ciento: Todos Programas a -%d%% por %d dias", 100-percentage, days ); 

				}

			}

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_PercentAdjustByProg_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned short prog;

	unsigned short percentage;

	unsigned short days;

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &prog, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &percentage, pindex, sizeof( unsigned short ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &days, pindex, sizeof( unsigned short ) );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			if( percentage == 100 )
			{

				if( days == PERCENT_ADJUST_NO_EXPIRE )
				{

					snprintf( pdest, pdest_size, "Percent Adjust Set: %s to %d%% forever", E_ProgStrWithoutColon[ prog ], percentage-100 ); 

				}
				else
				{

					snprintf( pdest, pdest_size, "Percent Adjust Set: %s to %d%% for %d days", E_ProgStrWithoutColon[ prog ], percentage-100, days ); 

				}

			}
			else if( percentage > 100 )
			{

				if( days == PERCENT_ADJUST_NO_EXPIRE )
				{

					snprintf( pdest, pdest_size, "Percent Adjust Set: %s to +%d%% forever", E_ProgStrWithoutColon[ prog ], percentage-100 ); 

				}
				else
				{

					snprintf( pdest, pdest_size, "Percent Adjust Set: %s to +%d%% for %d days", E_ProgStrWithoutColon[ prog ], percentage-100, days ); 

				}

			}
			else // if ( percentage < 100 )
			{

				if( days == PERCENT_ADJUST_NO_EXPIRE )
				{

					snprintf( pdest, pdest_size, "Percent Adjust Set: %s to -%d%% forever", E_ProgStrWithoutColon[ prog ], 100-percentage ); 

				}
				else
				{

					snprintf( pdest, pdest_size, "Percent Adjust Set: %s to -%d%% for %d days", E_ProgStrWithoutColon[ prog ], 100-percentage, days ); 

				}

			}

		}
		else
		{

			if( percentage == 100 )
			{

				if( days == PERCENT_ADJUST_NO_EXPIRE )
				{

					snprintf( pdest, pdest_size, "Conjunto Ajuste Por ciento: %s a %d%% por siempre", S_ProgStrWithoutColon[ prog ], percentage-100 ); 

				}
				else
				{

					snprintf( pdest, pdest_size, "Conjunto Ajuste Por ciento: %s a %d%% por %d dias", S_ProgStrWithoutColon[ prog ], percentage-100, days ); 

				}

			}
			else if( percentage > 100 )
			{

				if( days == PERCENT_ADJUST_NO_EXPIRE )
				{

					snprintf( pdest, pdest_size, "Conjunto Ajuste Por ciento: %s a +%d%% por siempre", S_ProgStrWithoutColon[ prog ], percentage-100 ); 

				}
				else
				{

					snprintf( pdest, pdest_size, "Conjunto Ajuste Por ciento: %s a +%d%% por %d dias", S_ProgStrWithoutColon[ prog ], percentage-100, days ); 

				}

			}
			else // if ( percentage < 100 )
			{

				if( days == PERCENT_ADJUST_NO_EXPIRE )
				{

					snprintf( pdest, pdest_size, "Conjunto Ajuste Por ciento: %s a -%d%% por siempre", S_ProgStrWithoutColon[ prog ], 100-percentage ); 

				}
				else
				{

					snprintf( pdest, pdest_size, "Conjunto Ajuste Por ciento: %s a -%d%% por %d dias", S_ProgStrWithoutColon[ prog ], 100-percentage, days ); 

				}

			}

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
static unsigned short Pull_SICStateChanged_OffPile( ALERTS_PILE_STRUCT *const ppile, UNS_32 parse_why, void *pdest, UNS_32 pdest_size, UNS_32 *pindex )
{

	unsigned short rv;

	unsigned char addr;

	unsigned char function;

	unsigned char state;

	char addr_switch_is_on_str_8[ 8 ];

	char switch_name[ 25 ];

	rv = 0;

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &addr, pindex, sizeof( unsigned char ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &function, pindex, sizeof( unsigned char ) );

	rv += ALERTS_pull_legacy_object_off_pile( ppile, &state, pindex, sizeof( unsigned char ) );

	rv += ALERTS_pull_string_off_pile( ppile, (char*)&switch_name, sizeof(switch_name), pindex );

	if( parse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{

		if( Language == ENGLISH )
		{

			snprintf( addr_switch_is_on_str_8, sizeof(addr_switch_is_on_str_8), "at \"%c\" ", addr );

		}
		else
		{

			snprintf( addr_switch_is_on_str_8, sizeof(addr_switch_is_on_str_8), "en \"%c\" ", addr );

		}


		if( Language == ENGLISH )
		{

			if( function == SIC_FUNCTIONALITY_operate_as_a_rain_switch )
			{

				if( state == SIC_SWITCH_STATUS_active )
				{

					snprintf( pdest, pdest_size, "Rain Switch %sshowing Rain", addr_switch_is_on_str_8 );

				}
				else
				{

					snprintf( pdest, pdest_size, "Rain Switch %shas dried out", addr_switch_is_on_str_8 );

				}

			}
			else if( function == SIC_FUNCTIONALITY_operate_as_a_freeze_switch )
			{

				if( state == SIC_SWITCH_STATUS_active )
				{

					snprintf( pdest, pdest_size, "Freeze Switch %spreventing irrigation", addr_switch_is_on_str_8 );

				}
				else
				{

					snprintf( pdest, pdest_size, "Freeze Switch %snow above freezing", addr_switch_is_on_str_8 );

				}

			}
			else if( function == SIC_ACTION_TO_TAKE_stop__throw_away_remaining_time )
			{

				if( state == SIC_SWITCH_STATUS_active )
				{

					snprintf( pdest, pdest_size, "%s %spreventing irrigation", switch_name, addr_switch_is_on_str_8 );

				}
				else
				{

					snprintf( pdest, pdest_size, "%s %sallowing irrigation again", switch_name, addr_switch_is_on_str_8 );

				}

			}
			else if( function == SIC_ACTION_TO_TAKE_pause )
			{

				if( state == SIC_SWITCH_STATUS_active )
				{

					snprintf( pdest, pdest_size, "%s %spausing irrigation", switch_name, addr_switch_is_on_str_8 );

				}
				else
				{

					snprintf( pdest, pdest_size, "%s %sallowing irrigation again", switch_name, addr_switch_is_on_str_8 );

				}

			}

		}
		else
		{

			if( function == SIC_FUNCTIONALITY_operate_as_a_rain_switch )
			{

				if( state == SIC_SWITCH_STATUS_active )
				{

					snprintf( pdest, pdest_size, "Sensor de Lluvia %que muestran la Lluvia",addr_switch_is_on_str_8 );

				}
				else
				{

					snprintf( pdest, pdest_size, "Sensor de Lluvia %sSeco",addr_switch_is_on_str_8 );

				}

			}
			else if( function == SIC_FUNCTIONALITY_operate_as_a_freeze_switch )
			{

				if( state == SIC_SWITCH_STATUS_active )
				{

					snprintf( pdest, pdest_size, "Sensor de Congelacion %simpidiendo riego", addr_switch_is_on_str_8 );

				}
				else
				{

					snprintf( pdest, pdest_size, "Sensor de Congelacion %sya no congelación", addr_switch_is_on_str_8 );

				}

			}
			else if( ( function == SIC_ACTION_TO_TAKE_stop__throw_away_remaining_time ) ||
					 ( function == SIC_ACTION_TO_TAKE_pause ) )
			{

				if( state == SIC_SWITCH_STATUS_active )
				{

					snprintf( pdest, pdest_size, "%s %simpidiendo riego", switch_name, addr_switch_is_on_str_8 );

				}
				else
				{

					snprintf( pdest, pdest_size, "%s %spermitiendo el riego de nuevo", switch_name, addr_switch_is_on_str_8 );

				}

			}

		}

	}

	return( rv );

}

/* ---------------------------------------------------------- */
DLLEXPORT UNS_32 LEGACY_ALERTS_parse_alert_and_return_length( const UNS_32 paid, ALERTS_PILE_STRUCT *const ppile, const UNS_32 pparse_why, char *pdest_ptr, const UNS_32 pallowable_size, UNS_32 pindex, const UNS_8 pis_a_600_series )
{
	// 11/18/2014 rmd : Function returns ZERO if there is a parsing error. That would be an
	// unrecognized AID or any range check error that fails.

	// ----------

	UNS_32 rv;

	BOOL_32 failure_occurred;

	// Make the temporary buffer big so we can fit almost any string into the buffer. This is
	// not the total alert length. But close. We are careful to use the protected string
	// functions, snprintf and strlcat, to avoid overruns.
	char tmpbuf[ MAX_ALERT_LENGTH ];

	// ----------

	rv = 0;

	// NULL string to start.
	memset( tmpbuf, 0x00, sizeof(tmpbuf) );

	// ----------

	failure_occurred = (false);

	// ----------

	switch( paid )
	{
		// Remember rv is returning the byte count on the alert pile of the items we pull off due to
		// this particular alert. For the case of teh "simple" alerts rv is not incremented during
		// this text generation stage.
		
		case LEGACY_AID_USER_RESET:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "User Reset Controller On Powerup!", sizeof(tmpbuf) );
			}
			break;  

		case LEGACY_AID_EE_FAILED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "EE Checksum failure - Program Data to factory defaults!", sizeof(tmpbuf) );
			}
			break;  

		case LEGACY_AID_SRAM_FAILED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "SRAM Checksum failure - Alerts & Station History restarted!", sizeof(tmpbuf) );
			}
			break;  

		case LEGACY_AID_POWER_FAIL:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "POWER FAIL", sizeof(tmpbuf) );
			}
			break;  

		case LEGACY_AID_ROM_UPDATE:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Program Update (new roms)", sizeof(tmpbuf) );
			}
			break;  

		case LEGACY_AID_ROM_VERSION_UNRECOGNIZABLE:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Previous Program Version not recognized", sizeof(tmpbuf) );
			}
			break;  

		case LEGACY_AID_UPDATED_CMOS:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "CMOS updated", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_CMOS_FAILED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "CMOS Checksum failure - CMOS reset to factory default!", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_PROGRAM_RESTART:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Program Restart", sizeof(tmpbuf) );
			}
			break;  

		case LEGACY_AID_EXCEPTION:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Exception: ", sizeof(tmpbuf) );
			}
			rv += Pull_Exception_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case LEGACY_AID_ERROR:
			rv += ALERTS_pull_string_off_pile( ppile, (char*)&tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case LEGACY_AID_UNIT_COMMUNICATED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Unit Communicated: ", sizeof(tmpbuf) );
			}
			rv += ALERTS_pull_string_off_pile(ppile, (char*)&tmpbuf[strlen(tmpbuf)], ( sizeof(tmpbuf) - (INT_32)strlen(tmpbuf) ), &pindex);
			break;  

		case LEGACY_AID_MLB_BREAK:
			rv += Pull_MainlineBreak_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_MLB_REMINDER:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "MAINLINE BREAK (reminder)", sizeof(tmpbuf) );
			}
			break;  

		case LEGACY_AID_MLB_CLEAR_REQUEST_KEYPAD:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Mainline Break Clear Request (keypad)", sizeof(tmpbuf) );
			}
			break;  

		case LEGACY_AID_MLB_CLEAR_REQUEST_COMMLINK:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Mainline Break Clear Request (comm)", sizeof(tmpbuf) );
			}
			break;  

		case LEGACY_AID_MLB_CLEARED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Mainline Break Cleared", sizeof(tmpbuf) );
			}
			break;  

		case LEGACY_AID_SHORT_STATION:
			rv += Pull_Short_STATION_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_SHORT_LIGHT:
			rv += Pull_Short_LIGHT_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_SHORT_UNKNOWN_REASON:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "SHORT: unknown reason", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_OPEN_STATION:
			rv += Pull_OpenStation_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_SHORT_MV:
			rv += Pull_Short_MV_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_SHORT_PUMP:
			rv += Pull_Short_PUMP_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_SHORT_COMBINATION:
			rv += Pull_Short_COMBINATION_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_FLOW_NOT_CHECKED_NO_REASONS:
			rv += Pull_flow_not_checked_no_reasons_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_FLOW_NOT_CHECKED_WITH_REASONS:
			rv += Pull_flow_not_checked_with_reasons_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_FLOW_CHECK_LEARNING:
			rv += Pull_FlowCheckLearning_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_FIRST_HIGHFLOW:
		case LEGACY_AID_FINAL_HIGHFLOW:
		case LEGACY_AID_FIRST_LOWFLOW:
		case LEGACY_AID_FINAL_LOWFLOW:
			rv += Pull_Flow_Error_OffPile( ppile, pparse_why, paid, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_STARTTIME_SKIPPED:
			rv += Pull_StartTime_Skipped_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case LEGACY_AID_IRRIGATION_TURNED_OFF_WITH_REASON:
			rv += Pull_IRRIGATION_TURNED_OFF_WITH_REASON_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case LEGACY_AID_IRRIGATION_TURNED_ON_WITH_REASON:
			rv += Pull_IRRIGATION_TURNED_ON_WITH_REASON_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case LEGACY_AID_IRRIGATION_IS_OFF_REMINDER:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "IRRIGATION IS OFF (reminder)", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_CLOCKSET_KEYPAD:
		case LEGACY_AID_CLOCKSET_COMMLINK:
		case LEGACY_AID_CLOCKSET_DAYLIGHTSAVINGS:
			rv += Pull_ClockSet_OffPile( ppile, pparse_why, paid, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_OVER_BUDGET:
			rv += Pull_OverBudget_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case LEGACY_AID_HIT_STOPTIME:
			rv += Pull_HitStopTime_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case LEGACY_AID_SETHO_BYSTATION:
			rv += Pull_SetHOByStation_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_SETHO_BYPROG:
			rv += Pull_SetHOByProg_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_SETHO_ALLSTATIONS:
			rv += Pull_SetHOAllStations_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_SETNOWDAYS_BYSTATION:
			rv += Pull_SetNOWDaysByStation_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_SETNOWDAYS_BYPROG:
			rv += Pull_SetNOWDaysByProg_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_SETNOWDAYS_ALLSTATIONS:
			rv += Pull_SetNOWDaysAllStations_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_PASSWORDS_RESET_ROMVERSION:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Passwords Set To Factory Default (ROM Version Unk)", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_PASSWORDS_RESET_CMOS:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Passwords Set To Factory Default (CMOS Unitialized)", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_PASSWORDS_RESET_COMMLINK:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Passwords Set To Factory Default (comm)", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_PASSWORDS_RECEIVED_FROM_PC:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Password Settings Received (comm)", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_PASSWORDS_SENT_TO_PC:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Password Settings Sent (comm)", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_PASSWORDS_UNLOCKED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Password UNLOCKED (comm)", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_PASSWORDS_LOGIN:
			rv += Pull_PasswordLogin_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_RRE_PASSWORD_LOGIN:
			rv += Pull_RRE_PasswordLogin_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_PASSWORDS_LOGOUT_KEYPAD_TIMEOUT:
		case LEGACY_AID_PASSWORDS_LOGOUT_USER_LOGGED_OUT:
			rv += Pull_PasswordLogout_OffPile( ppile, pparse_why, paid, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_PASSWORDS_CANCEL_UNLOCK_KEYPAD_TIMEOUT:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Password UNLOCK ended (keypad timeout)", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_PASSWORDS_CANCEL_UNLOCK_USER_CANCELLED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Password UNLOCK ended (user cancelled)", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_SYSTEM_CAPACITY_AUTOMATICALLY_ADJUSTED_UP:
			rv += Pull_SystemCapacityAutomaticallyAdjusted_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_MVOR_REQUESTED:
		case LEGACY_AID_MVOR_STARTED:
		case LEGACY_AID_MVOR_CANCELLED:
		case LEGACY_AID_MVOR_STOPPED:
			rv += Pull_MVOR_Alert_OffPile( ppile, pparse_why, paid, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_MVOR_OPEN_REQUEST_IGNORED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "MVOR CLOSED Request To Open Ignored", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_ENTERED_FORCED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Entering Forced", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_LEAVING_FORCED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Leaving Forced", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_STARTING_RESCAN:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Starting Rescan", sizeof(tmpbuf) );
			}
			break;
		case LEGACY_AID_RAIN_SWITCH_ACTIVE:
			if (pparse_why == PARSE_FOR_LENGTH_AND_TEXT)
			{
				strlcpy(tmpbuf, "Rain Switch showing Rain", sizeof(tmpbuf));
			}
			break;
		case LEGACY_AID_RAIN_SWITCH_INACTIVE:
			if (pparse_why == PARSE_FOR_LENGTH_AND_TEXT)
			{
				strlcpy(tmpbuf, "Rain Switch Dried Out", sizeof(tmpbuf));
			}
			break;

		case LEGACY_AID_MANUALHO_SKIPPED:
		case LEGACY_AID_MANUALPROG_SKIPPED:
			rv += Pull_ManualHOorPROG_Skipped_OffPile( ppile, pparse_why, paid, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case LEGACY_AID_EEWRITE_IN_PROCESS:
			rv += Pull_EEWriteInProcess_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_ETGAGE_PERCENT_FULL_EDITED:
			rv += Pull_ETGAGE_percent_full_edited( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_ETGAGE_PULSE:
			rv += Pull_ETGAGE_pulse( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_ETGAGE_RUNAWAY:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "ET GAGE - Runaway Gage!", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_ETGAGE_FIRST_ZERO:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "ET GAGE - 0 Pulses!", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_ETGAGE_SECOND_OR_MORE_ZEROS:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "ET GAGE - 0 Pulses Multiple Days!", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_ETGAGE_PERCENT_FULL_WARNING :
			rv += Pull_ETGAGE_PercentFull_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;


		case LEGACY_AID_RAIN_AFFECTING_IRRIGATION:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "STOPPED Programmed Irrigation (rain)", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_RAIN_24HOUR_TOTAL:
			rv += Pull_RAIN_24HourTotal_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_STOP_KEY_WITH_REASON:
			rv += Pull_StopKey_withreason_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_ETTABLE_LOADED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "ET Table: New Historical Values Loaded", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_HOLDOVER_CHANGED:
			rv += Pull_HoldOverChanged_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_FUSE_REPLACED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "FUSE Replaced - ok now", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_FUSE_BLOWN:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "FUSE BLOWN", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_FUSE_REMINDER:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "FUSE BLOWN (reminder)", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_PING_SWREV_ERROR:
			rv += Pull_Ping_SWREV_Error_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;          

		case LEGACY_AID_PING_COMMVER_ERROR:
			rv += Pull_Ping_CommVer_Error_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;          

		case LEGACY_AID_PROGRAMMED_IRRIGATION_RUNNING_AT_STARTTIME:
			rv += Pull_Programmed_Irrigation_Running_At_Starttime_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_MANUAL_PROGRAM_IRRIGATING:
			rv += Pull_ManualProgramIrrigating_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_MANUAL_HOLDOVER_IRRIGATING:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "START: Manual Hold-Over", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_MANUAL_SEQUENCE_IRRIGATING:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "START: Walk-Thru", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_TEST_STATION_STARTED_WITH_REASON:
			rv += Pull_TestStationStarted_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_TEST_PROGRAM_STARTED_WITH_REASON:
			rv += Pull_TestProgramStarted_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_TEST_ALL_STARTED_WITH_REASON:
			rv += Pull_TestAllStarted_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_OLD_MANUAL_STATION_STARTED_WITH_REASON:
			rv += Pull_OldManualStationStarted_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_OLD_MANUAL_PROGRAM_STARTED_WITH_REASON:
			rv += Pull_OldManualProgramStarted_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_OLD_MANUAL_ALL_STARTED_WITH_REASON:
			rv += Pull_OldManualAllStarted_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_IRRIGATION_ENDED:
			rv += Pull_IrrigationEnded_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_DTMF_ACTIVATED:
			rv += Pull_DTMF_activated_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_DTMF_DEACTIVATED:
			rv += Pull_DTMF_deactivated_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_DTMF_STATION_ON:
			rv += Pull_DTMF_station_on_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_DTMF_STATION_OFF:
			rv += Pull_DTMF_station_off_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_PROGRAMMED_IRRIGATION_STARTED:
			rv += Pull_Programmed_Irrigation_Started_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_DIRECT_ACCESS_ENTERED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Direct Access Entered", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_DIRECT_ACCESS_EXITED:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Direct Access Exited", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_DIRECT_ACCESS_TIMEDOUT:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Direct Access Timed Out", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_CTS_TIMEOUT_A:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Port A: CTS Timeout", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_CTS_TIMEOUT_B:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Port B: CTS Timeout", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_CTS_TIMEOUT_C:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Port C: CTS Timeout", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_CTS_TIMEOUT_D:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Port D: CTS Timeout", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_CH1794_FAILED_INIT:
			if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
			{
				strlcpy( tmpbuf, "Phone Modem Failed to Initialize", sizeof(tmpbuf) );
			}
			break;

		case LEGACY_AID_ETTABLE_SUBSTITUTION:
			rv += Pull_ETTable_Substitution_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_ETTABLE_LIMITED_ENTRY:
			rv += Pull_ETTable_LimitedEntry_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_MOISTURE_SENSOR_READING:
			rv += Pull_MOISTURE_SENSOR_READING_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_MOISTURE_SLAVE_ON_DIFFERENT_PROGRAM:
			rv += Pull_MOISTURE_SLAVE_ON_DIFFERENT_PROGRAM_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_MOISTURE_MASTER_TERMINATED:
		case LEGACY_AID_MOISTURE_SLAVE_TERMINATED:
			rv += Pull_MOISTURE_STATION_TERMINATED_OffPile( ppile, pparse_why, paid, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_COMM_COMMAND_RCVD:
			rv += Pull_COMM_COMMAND_RCVD_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_LIGHTS_STATION_OFF:
		case LEGACY_AID_LIGHTS_STATION_ON:
			rv += Pull_LIGHTS_Activity_OffPile( ppile, pparse_why, paid, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_WIND_PAUSE:
		case LEGACY_AID_WIND_RESUME:
			rv += Pull_WIND_alerts_OffPile( ppile, pparse_why, paid, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_IRRIGATION_PAUSED:
		case LEGACY_AID_IRRIGATION_RESUMED:
			rv += Pull_PAUSE_RESUME_alerts_OffPile( ppile, pparse_why, paid, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_POC_MLB_BREAK:
			rv += Pull_POC_MainlineBreak_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_POC_MLB_REMINDER:
			rv += Pull_POC_MainlineBreak_reminder_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case LEGACY_AID_POC_MLB_CLEARED_KEYPAD:
			rv += Pull_POC_MainlineBreak_cleared_keypad_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case LEGACY_AID_POC_MLB_CLEARED_COMMLINK:
			rv += Pull_POC_MainlineBreak_cleared_commlink_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;  

		case LEGACY_AID_POC_MLB_CLEARED_SHORT:
			rv += Pull_POC_MainlineBreak_cleared_short_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_POC_MV_SHORT:
			rv += Pull_POC_MV_Short_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_POC_MV_SHORT_DURING_MLB:
			rv += Pull_POC_MV_Short_During_MLB_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_POC_STARTED_USE:
			rv += Pull_POC_Hit_StartTime_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_POC_HIT_ENDTIME:
			rv += Pull_POC_Hit_EndTime_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_POC_STOPPED_USE:
			rv += Pull_POC_Stopped_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_USERDEFINED_PROGRAMTAG_CHANGED:
			rv += Pull_UserDefined_ProgramTag_Changed_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_PROGRAMTAG_CHANGED:
			rv += Pull_ProgramTag_Changed_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_PERCENTADJUST_ALL_PROGS:
			rv += Pull_PercentAdjustAllProgs_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_PERCENTADJUST_BY_PROG:
			rv += Pull_PercentAdjustByProg_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

		case LEGACY_AID_SIC_STATE_CHANGED:
			rv += Pull_SICStateChanged_OffPile( ppile, pparse_why, &tmpbuf, sizeof(tmpbuf), &pindex );
			break;

			/* ---------------------------------------- */
			/* ---------------------------------------- */

		default:

			// aid's > than 0xA000 are for ALL the change lines. That group is split further
			// into PData and NON PData variables
			//
			if( paid > LEGACY_AID_CHANGE_INDEX_START_OF_NON_PDATA_VARS )
			{
				if( pis_a_600_series )
				{
					rv += Pull_ChangeLine_OffPile_600( ppile, pparse_why, paid, &tmpbuf, sizeof(tmpbuf), &pindex ); 
				}
				else
				{
					rv += Pull_ChangeLine_OffPile_500( ppile, pparse_why, paid, &tmpbuf, sizeof(tmpbuf), &pindex ); 
				}
			}
			else
			{

				// 11/19/2014 rmd : We fall through to here for unrecognized AID's. If we were passed an
				// unrecognized AID set the error flag.
				failure_occurred = (true);

				if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
				{
					snprintf( tmpbuf, sizeof(tmpbuf), "UNPARSED LINE TYPE %05d", paid );
				}

			}
			break;

	}

	// ----------

	// 11/18/2014 rmd : And if the parsing was sucessful add the length byte to the length of
	// the alert. All alerts contain the length byte as their last byte. REMEMBER the goal is to
	// return a ZERO if soemthing went wrong during the parsing. So don't bump the returned
	// length if it is still 0.
	if( failure_occurred )
	{
		rv = 0;
	}
	else
	{
		rv += 1;
	}

	// ----------

	if( pparse_why == PARSE_FOR_LENGTH_AND_TEXT )
	{
		// 8/30/2013 rmd : Copy the results to the callers destination. Being careful not to overrun
		// his buffer. Note - tmpbuf starts out as a NULL string. That is its first character is a
		// NULL meaning a zero length string. Providing some protection if the parsing effort failed
		// in that we are returning a NULL string. NOTE - 'strlcpy' is GUARANTEED to NULL terminate
		// the string.
		strlcpy( pdest_ptr, tmpbuf, pallowable_size );
	}

	return( rv );
}


/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

