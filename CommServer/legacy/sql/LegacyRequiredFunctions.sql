CREATE FUNCTION ToLegacyControllerDate( @dateTime TimeStamp )
RETURNS Integer
BEGIN
	 RETURN (IIF(MONTH(@dateTime) > 2,
	 		((((YEAR(@dateTime) - 1900) * 1461) / 4) + ( ((153 * (MONTH(@dateTime) - 3)) + 2) / 5) + DAYOFMONTH(@dateTime) + 58),
			((((YEAR(@dateTime) - 1900 - 1) * 1461) / 4) + ( ((153 * (MONTH(@dateTime) + 9)) + 2) / 5) + DAYOFMONTH(@dateTime) + 58)));
END;

CREATE FUNCTION ToLegacyControllerTime( @dateTime TimeStamp )
RETURNS Integer
BEGIN
	 RETURN ((HOUR(@dateTime) * 60 * 60) + (MINUTE(@dateTime) * 60) + SECOND(@dateTime));
END;