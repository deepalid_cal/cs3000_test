SELECT CAST(Login1 AS SQL_BINARY(3)) Login1,
	   CAST(Login1Level AS SQL_BINARY(1)) Login1Level,
	   CAST(Login2 AS SQL_BINARY(3)) Login2,
	   CAST(Login2Level AS SQL_BINARY(1)) Login2Level,
	   CAST(Login3 AS SQL_BINARY(3)) Login3,
	   CAST(Login3Level AS SQL_BINARY(1)) Login3Level,
	   CAST(Login4 AS SQL_BINARY(3)) Login4,
	   CAST(Login4Level AS SQL_BINARY(1)) Login4Level,
	   CAST(Login5 AS SQL_BINARY(3)) Login5,
	   CAST(Login5Level AS SQL_BINARY(1)) Login5Level,
	   CAST(Login6 AS SQL_BINARY(3)) Login6,
	   CAST(Login6Level AS SQL_BINARY(1)) Login6Level,
	   CAST(Login7 AS SQL_BINARY(3)) Login7,
	   CAST(Login7Level AS SQL_BINARY(1)) Login7Level,
	   CAST(Login8 AS SQL_BINARY(3)) Login8,
	   CAST(Login8Level AS SQL_BINARY(1)) Login8Level,
	   CAST(Login9 AS SQL_BINARY(3)) Login9,
	   CAST(Login9Level AS SQL_BINARY(1)) Login9Level,
	   CAST(Login10 AS SQL_BINARY(3)) Login10,
	   CAST(Login10Level AS SQL_BINARY(1)) Login10Level,
	   CAST(AccessProfile AS SQL_BINARY(100)) AccessProfile
FROM Passwords
WHERE SetID = :SetID;