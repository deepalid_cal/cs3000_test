SELECT CAST(Version AS SQL_BINARY(2)) Version,
	   CAST(CMOSSize AS SQL_BINARY(2)) CMOSSize,
	   CAST(A_BaudRate AS SQL_BINARY(1)) A_BaudRate,
	   CAST(Protocol AS SQL_BINARY(1)) Protocol,	// PROTOCOL_DEFS enum
	   CAST(Comm_Type AS SQL_BINARY(1)) Comm_Type,	// MODEL_TYPES enum
	   CAST(Number_Of_Stations AS SQL_BINARY(1)) Number_Of_Stations,   // NUMBER_OF_STATIONS enum
	   CAST(KeyPad AS SQL_BINARY(1)) KeyPad,
	   CAST(LightsEnabled AS SQL_BINARY(1)) LightsEnabled,
	   CAST(GInterface AS SQL_BINARY(1)) GInterface,
	   CAST(RBInterface AS SQL_BINARY(1)) RBInterface,
	   CAST(FInterface AS SQL_BINARY(1)) FInterface,
	   CAST(DemoAllOptions AS SQL_BINARY(1)) DemoAllOptions,
	   CAST(SizeOfRam AS SQL_BINARY(1)) SizeOfRam,				// RAM_SIZE enum
	   CAST(UseSOutDelay AS SQL_BINARY(1)) UseSOutDelay,
	   CAST(NumOfRings AS SQL_BINARY(1)) NumOfRings,		   // NUM_OF_RINGS_TYPE enum
	   CAST(DTMF_Installed AS SQL_BINARY(1)) DTMF_Installed,
	   CAST(WGInterface AS SQL_BINARY(1)) WGInterface,
	   CAST(UseCTS AS SQL_BINARY(1)) UseCTS,
	   CAST(BuzzerType AS SQL_BINARY(1)) BuzzerType,  		   // BUZZER_TYPE enum
	   CAST(Reserved1 AS SQL_BINARY(1)) Reserved1,
	   CAST(Reserved2 AS SQL_BINARY(1)) Reserved2,
	   CAST(Reserved3 AS SQL_BINARY(1)) Reserved3,
	   CAST(B_BaudRate AS SQL_BINARY(1)) B_BaudRate,
	   CAST(BoardRevision AS SQL_BINARY(1)) BoardRevision,	   // BOARD_REVISION enum
	   CAST(Debuging AS SQL_BINARY(2)) Debuging,
	   CAST(OM_Originator_Retries AS SQL_BINARY(2)) OM_Originator_Retries,
	   CAST(0 AS SQL_BINARY(2)) AS OM_Response_Retries,
	   CAST(OM_Seconds_for_Status_FOAL AS SQL_BINARY(2)) OM_Seconds_for_Status_FOAL,
	   CAST(OM_Minutes_To_Exist AS SQL_BINARY(2)) OM_Minutes_To_Exist,
	   CAST(FOAL_MinutesBetweenRescanAttempts AS SQL_BINARY(2)) FOAL_MinutesBetweenRescanAttempts,
	   CAST(0 AS SQL_BINARY(2)) Comm_Mngr_App_Timer_Seconds,
	   CAST(COMM_Address AS SQL_CHAR(3)) COMM_Address,
	   CAST(0 AS SQL_CHAR(1)) dummy_1,
	   CAST(NormalLevelsA AS SQL_BINARY(2)) NormalLevelsA,
	   CAST(NormalLevelsB AS SQL_BINARY(2)) NormalLevelsB,
	   CAST(NormalLevelsC AS SQL_BINARY(2)) NormalLevelsC,
	   CAST(NormalLevelsD AS SQL_BINARY(2)) NormalLevelsD,
	   CAST(C_BaudRate AS SQL_BINARY(1)) C_BaudRate,
	   CAST(D_BaudRate AS SQL_BINARY(1)) D_BaudRate,
	   CAST(StationsAllowed AS SQL_BINARY(2)) StationsAllowed,
	   CAST(GpmSlotSize AS SQL_BINARY(2)) GpmSlotSize,
	   CAST(MaxStationsOn AS SQL_BINARY(2)) MaxStationsOn,
	   CAST(NumberOfGpmSlots AS SQL_BINARY(2)) NumberOfGpmSlots,
	   CAST(NumberOfStationCycles AS SQL_BINARY(2)) NumberOfStationCycles,
	   CAST(CellIterations AS SQL_BINARY(2)) CellIterations,
	   CAST(CentralMajorVersion AS SQL_BINARY(1)) CentralMajorVersion,
	   CAST(CentralMinorVersion AS SQL_BINARY(1)) CentralMinorVersion,
	   CAST(CentralReleaseVersion AS SQL_BINARY(1)) CentralReleaseVersion,
	   CAST(CentralBuildVersion AS SQL_BINARY(1)) CentralBuildVersion,
	   CAST(ControllerSerialNumber AS SQL_BINARY(2)) ControllerSerialNumber
FROM CMOS
WHERE ControllerID = :ControllerID;