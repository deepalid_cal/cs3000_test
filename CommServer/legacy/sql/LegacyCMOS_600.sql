SELECT CAST(Version AS SQL_BINARY(2)) Version,
	   CAST(CMOSSize AS SQL_BINARY(2)) CMOSSize,
	   CAST(ControllerSerialNumber AS SQL_BINARY(2)) ControllerSerialNumber,
	   CAST(StationsAllowed AS SQL_BINARY(2)) StationsAllowed,
	   CAST(CommTypeA AS SQL_BINARY(2)) CommTypeA,
	   CAST(CommTypeB AS SQL_BINARY(2)) CommTypeB,
	   CAST(CommTypeC AS SQL_BINARY(2)) CommTypeC,
	   CAST(CommTypeD AS SQL_BINARY(2)) CommTypeD,
	   CAST(A_BaudRate AS SQL_BINARY(1)) A_BaudRate,
	   CAST(B_BaudRate AS SQL_BINARY(1)) B_BaudRate,
	   CAST(C_BaudRate AS SQL_BINARY(1)) C_BaudRate,
	   CAST(D_BaudRate AS SQL_BINARY(1)) D_BaudRate,
	   CAST(NormalLevelsA AS SQL_BINARY(2)) NormalLevelsA,
	   CAST(NormalLevelsB AS SQL_BINARY(2)) NormalLevelsB,
	   CAST(NormalLevelsC AS SQL_BINARY(2)) NormalLevelsC,
	   CAST(NormalLevelsD AS SQL_BINARY(2)) NormalLevelsD,
	   CAST(ThisControllerIsAHub AS SQL_BINARY(1)) ThisControllerIsAHub,
	   CAST(FInterface AS SQL_BINARY(1)) FInterface,
	   CAST(GInterface AS SQL_BINARY(1)) GInterface,
	   CAST(RBInterface AS SQL_BINARY(1)) RBInterface,
	   CAST(WGInterface AS SQL_BINARY(1)) WGInterface,
	   CAST(DemoAllOptions AS SQL_BINARY(1)) DemoAllOptions,
	   CAST(LightsEnabled AS SQL_BINARY(1)) LightsEnabled,
	   CAST(RRe_Enabled AS SQL_BINARY(1)) RRe_Enabled,
	   CAST(FOAL_MinutesBetweenRescanAttempts AS SQL_BINARY(2)) FOAL_MinutesBetweenRescanAttempts,
	   CAST(COMM_Address AS SQL_CHAR(3)) COMM_Address,
	   CAST(NumOfRings AS SQL_BINARY(1)) NumOfRings,
	   CAST(CentralMajorVersion AS SQL_BINARY(1)) CentralMajorVersion,
	   CAST(CentralMinorVersion AS SQL_BINARY(1)) CentralMinorVersion,
	   CAST(CentralReleaseVersion AS SQL_BINARY(1)) CentralReleaseVersion,
	   CAST(CentralBuildVersion AS SQL_BINARY(1)) CentralBuildVersion,
	   CAST(OM_Originator_Retries AS SQL_BINARY(2)) OM_Originator_Retries,
	   CAST(OM_Seconds_for_Status_FOAL AS SQL_BINARY(2)) OM_Seconds_for_Status_FOAL,
	   CAST(OM_Minutes_To_Exist AS SQL_BINARY(2)) OM_Minutes_To_Exist,
	   CAST(GpmSlotSize AS SQL_BINARY(2)) GpmSlotSize,
	   CAST(MaxStationsOn AS SQL_BINARY(2)) MaxStationsOn,
	   CAST(NumberOfGpmSlots AS SQL_BINARY(2)) NumberOfGpmSlots,
	   CAST(NumberOfStationCycles AS SQL_BINARY(2)) NumberOfStationCycles,
	   CAST(CellIterations AS SQL_BINARY(2)) CellIterations,
	   CAST(Debuging AS SQL_BINARY(2)) Debuging,
	   CAST(PortASRIsAMaster AS SQL_BINARY(1)) PortASRIsAMaster,
	   CAST(PortBSRIsAMaster AS SQL_BINARY(1)) PortBSRIsAMaster,
	   CAST(HysteresisPercentage AS SQL_BINARY(2)) HysteresisPercentage,
	   CAST(Spacer AS SQL_BINARY(4)) Spacer
FROM CMOS
WHERE ControllerID = :ControllerID;