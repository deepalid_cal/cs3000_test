/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

// Required for strstr
#include	<string.h>

#include	"legacy_report_data.h"

#include	"legacy_common.h"

// Required for strlcpy
#include	"cal_string.h"

// Required for DATE_TIME definition
#include	"cal_td_utils.h"

// Required for SQL functions
#include	"sql_merge.h"



/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef enum
{
	PROG_A,
	PROG_B,
	PROG_C,
	PROG_D,
	PROG_E,
	PROG_F,
	PROG_G,

	// for for loops
	//
	PROG_LAST

} PROG_ASS;

typedef unsigned char PROG_ASS_SIZE;

// ----------

typedef enum
{
    MSS_READING_NEVER_TAKEN,       //  O --
    MSS_GOT_GOOD_READING,          //  G 78
    MSS_READING_OUT_OF_RANGE,      //  R **
    MSS_NO_FILTERED_READING,       //  F **
    MSS_SIGNAL_NEVER_WENT_AWAY,    //  S **

    MSS_READING_NOT_PUT_IN_LINE_YET,  // for the log line
    MSS_MOISTURE_SENSING_NOT_IN_USE   // says what it says

} MS_STATUS;

typedef unsigned char MS_STATUS_SIZE;

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

typedef enum
{  
	ORIGINAL = 0, 
	HISTORICAL = 1, 
	FROM_ET_GAGE = 2, 
	FROM_PAGER = 3,
	EDIT_CONTROLLER = 4, 
	EDIT_CENTRAL = 5,

	// status meaning from the rain bucket
	//
	RAIN_STATUS_R = 6,

	// status meaning below the minimum
	//
	RAIN_STATUS_M = 7,

	// and the newly added 'S' status to support the 2003 rain re-do including central polling
	// 'S' can mean special or stop...your choice
	//
	RAIN_STATUS_S = 8,

	// and the newly added 'P' status to indicate we have been polled to however we got shared
	// to after a start time...if you see this 'P' in slot 2 or beyond it means we did not get
	// shared to at all that day
	//
	RAIN_STATUS_P = 9

} LEGACY_ET_RAIN_STATUS;

typedef unsigned char LEGACY_ET_RAIN_STATUS_SIZE;

// ----------

typedef struct
{ 
	unsigned short	ET_us_10000u;

	LEGACY_ET_RAIN_STATUS_SIZE	Status;

	unsigned char	Dummy;		// automatically from 68k compiler

} LEGACY_ET_TABLE_ENTRY;

// ----------

typedef struct
{ 
	unsigned short	Rain_us_100u;

	LEGACY_ET_RAIN_STATUS_SIZE	Status;

	// 2012.03.05 rmd : A repeated situation has occurred. Customers have been known to set
	// their share time AFTER their irrigation start time and roll time. Because we share to the
	// 1st slot in the table AND the actual negative rain time is calculated from the slot one
	// contents at the start time, the end result is they get NO negative time. Irrigation stops
	// at the share time. But the next day continues. Even though some negative time was
	// supposed to have been generated. Rodger suggested to track as we calculate the negative
	// time for slot 1 (at the start time). And add a new test to the negative time calculation
	// function to also check slot TWO. If the negative time has not been generated we will
	// calculate it. This flag is a bit by bit affair. One bit per program. As we have to track
	// the -'ve time generation by program.
	unsigned char	negative_time_calculated;  // automatically from 68k compiler

} LEGACY_RAIN_TABLE_ENTRY;

// ----------

typedef struct
{
	unsigned short		TableDate;		// used to roll table

	LEGACY_RAIN_TABLE_ENTRY	Table[ 28 ];	// the table itself

} LEGACY_RAIN_TABLE_STRUCT;

// ----------

typedef struct
{
	DATE_TIME           StartDT;				// Date and Time associated with record
	DATE_TIME           EndDT;					// Date and Time associated with record

	unsigned short			HisETNumber_us_10000u;	// Historical ET number for the day	(10000u as of 523.a)
	LEGACY_ET_TABLE_ENTRY	GageNumber;				// ET table number for the day
	unsigned short			TotalRain_us_100u;		// total RAIN fall
	LEGACY_RAIN_TABLE_ENTRY	RainTable;				// RAIN table number for the day
	unsigned long			Budget_ul;				// Budget number in place this day

	// WHEN IT COMES TO THE SECONDS ACCUMULATORS THE AN unsigned short IS ONLY GOOD
	// FOR 9.1 HOURS - WHICH IS OK FOR SOME FIELDS BUT NOT THE SCHEDULED IRRIGATION
	// AND FOAL TOTAL. THEY MUST BE unsigned long.
	//
	unsigned long		TestSeconds_ul;			// controller Test
	unsigned long		TestGallons_ul;
	unsigned long		ManualSeconds_ul;		// controller Manual & Manual Sequence
	unsigned long		ManualGallons_ul;
	unsigned long		RRSeconds_ul;			// Radio Remote usage
	unsigned long		RRGallons_ul;
	unsigned long		ScheduledSeconds_ul;	// in FOAL - the Scheduled totals for this irrimachine
	unsigned long		ScheduledGallons_ul;

	// For the non FOAL case these totals are used for the single controller
	// non-controller totals. In the FOAL case these totals are used for the
	// non-controller totals at the SYSTEM level.
	//
	unsigned long		NonControllerSeconds_ul;	// in FOAL - the Scheduled totals for this irrimachine
	unsigned long		NonControllerGallons_ul;

	// This total is only added to for the master controller of a functioning
	// FOAL system. It is simply the total of all controller orchestrated flow.
	//
	unsigned long		FOALSystemGallons_ul;

} commlink_CONTROLLER_DAILY_RECORD;

// ----------

typedef struct
{
    DATE_TIME           start_dt;				// Date and Time associated with record
    DATE_TIME           end_dt;					

	unsigned long		gallons_during_idle_ul;
	unsigned long		seconds_during_idle_ul;

	unsigned long		gallons_during_use_ul;
	unsigned long		seconds_during_use_ul;

	unsigned long		gallons_during_irrigation_ul;
	unsigned long		seconds_during_irrigation_ul;

} commlink_POC_ACCUMULATOR_STRUCT;

// ----------

typedef struct
{
	DATE_TIME      StartDT;         // Date and Time associated with record
	DATE_TIME      EndDT;           // Date and Time associated with record

	unsigned short	S_TestSeconds_us[ MAX_STATIONS__600_SERIES ];
	unsigned long	S_TestGallons_ul_10u[ MAX_STATIONS__600_SERIES ];
	unsigned short	S_ManualSeconds_us[ MAX_STATIONS__600_SERIES ];
	unsigned long	S_ManualGallons_ul_10u[ MAX_STATIONS__600_SERIES ];
	unsigned short	S_RRSeconds_us[ MAX_STATIONS__600_SERIES ];
	unsigned long	S_RRGallons_ul_10u[ MAX_STATIONS__600_SERIES ];
	unsigned short	S_ScheduledSeconds_us[ MAX_STATIONS__600_SERIES ];
	unsigned long	S_ScheduledGallons_ul_10u[ MAX_STATIONS__600_SERIES ];

} commlink_STATION_DAILY_RECORD;

// ----------

typedef struct
{
	DATE_TIME      StartDT;			// Date and Time associated with record
	DATE_TIME      EndDT;			// Date and Time associated with record

	unsigned short  S_TestSeconds_us[ MAX_STATIONS__500_SERIES ];
	unsigned long   S_TestGallons_ul_10u[ MAX_STATIONS__500_SERIES ];
	unsigned short  S_ManualSeconds_us[ MAX_STATIONS__500_SERIES ];
	unsigned long   S_ManualGallons_ul_10u[ MAX_STATIONS__500_SERIES ];
	unsigned short  S_RRSeconds_us[ MAX_STATIONS__500_SERIES ];
	unsigned long   S_RRGallons_ul_10u[ MAX_STATIONS__500_SERIES ];
	unsigned short  S_ScheduledSeconds_us[ MAX_STATIONS__500_SERIES ];
	unsigned long   S_ScheduledGallons_ul_10u[ MAX_STATIONS__500_SERIES ];

} commlink_STATION_DAILY_RECORD__500_SERIES;

// ----------

typedef struct
{
	// LOG LINE DEFINITION FOR XMISSION ONLY - not for internal use
	//
	DATE_TIME   StartDT;        	   			// 6	first cycle start

	PROG_ASS_SIZE   Program; 		            // 1 
	unsigned char	Repeats;                	// 1 

	// Shown to the user as a 5.1f display ie 999.9 maximum. So stored as a 10u short works.
	//
	unsigned short	RequestedMinutes_us_10u;	// 2	in minutes

	// Because IrrigSec gets added to each second we must store it as a long in order to
	// accumulate on a 1hz basis. A short would overflow. The maximum irrigated time is 999.9 minutes.
	//
	unsigned long   IrrigSec;					// 4	in seconds 

	// The next two are accumulated on a 1 hz basis and therefore we don't want to loose much of
	// the fractional amount. The maximum gallons a station could irrigate would reasonably be
	// say 40gpm times 900minutes = 36000gallons. So we know we are going to store it as an unsigned long.
	// We therefore have enough range to easily go 100u.
	//
	// Well we have to store it here as a float in order to accumulate on a second by second basis. We
	// will ship it to Rodger as an ul_100u
	//
	unsigned long	IrrigGal_ul_100u;			// 4	in gallons

	// Maximum inches - if we store as a uc_100u we get 2.56 maximum inches per irrigation. This is
	// very reasonable but what about the guy who does something out of the ordinary and they irrigate
	// more than that. We'll cover for that by using an us_100u.
	//
	//
	// Well we have to store it here as a float in order to accumulate on a second by second basis. We
	// will ship it to Rodger as an us_100u
	//
	unsigned short	IrrigInch_us_100u;			// 4	in inches 

	unsigned short  FRate;                  	// 2 
	unsigned short  HiLimit;                	// 2 
	unsigned short  LoLimit;                	// 2 

	short  HOSeconds;			              	// 2	seconds can go NEGATIVE 

	unsigned short  bitFlag;                	// 2 

	unsigned char  MS_LastReading; 	        	// 1 
	unsigned char  MS_SetPoint;     	       	// 1 
	MS_STATUS_SIZE MS_LastReadingStatus;		// 1 
	unsigned char  Unused;						// 1 Compiler padding.

} XMIT_LOG_LINE_DEF;

// ----------

typedef struct
{
	DATE_TIME dt;

	unsigned char controller_letter;

	unsigned char station_number;

	unsigned short station_expected_flow;

	unsigned short station_derated_expected_flow;

	unsigned short station_hi_limit;

	unsigned short station_lo_limit;

	unsigned short station_portion_of_actual;

	unsigned char flag;

	unsigned char dummy;

} NEW_FLOW_RECORDER_RECORD;

// ----------

typedef struct
{
	DATE_TIME   dt;

	unsigned char   num_stations_on_uc;

	unsigned char   level_1_mv_state_uc;
	unsigned char   level_2_mv_state_uc;
	unsigned char   level_3_mv_state_uc;

	unsigned short  level_1_avg_flow_us;
	unsigned short  level_2_avg_flow_us;
	unsigned short  level_3_avg_flow_us;

	unsigned short  level_1_trip_point_us;
	unsigned short  level_2_trip_point_us;

	unsigned short  trip_point_up_us;
	unsigned short  trip_point_down_us;

	unsigned short  system_flowrate_us;

	unsigned short  expected_flowrate_us;

} NEW_DMC_RECORDER_RECORD;	// 28 bytes!


static void update_last_timestamp_in_controller_table(const UNS_32 pcontroller_id, 
	DATE_TIME llast_time_report_was_retrieved,
	char *field_name,
	char *pSQL_statements,
	char *pSQL_single_merge_statement_buf)
{
	memset(pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT);

	char	ltimestamp_str[32];

	make_timestamp_SQL_ready(llast_time_report_was_retrieved, ltimestamp_str, sizeof(ltimestamp_str));

	snprintf(pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT,
		"UPDATE Controllers SET %s='%s' WHERE ControllerID=%u;",
		field_name,ltimestamp_str, pcontroller_id);

	strlcat(pSQL_statements, pSQL_single_merge_statement_buf, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS);
}


/* ---------------------------------------------------------- */
static UNS_32 CONTROLLER_REPORT_DATA_extract_and_store_data(   UNS_8 *pucp,
															   const UNS_32 pdlen,
															   const UNS_32 pcontroller_id,
															   const BOOL_32 pinclude_poc_report_data,
															   char *pSQL_statements,
															   char *pSQL_search_condition_str,
															   char *pSQL_insert_values_str,
															   char *pSQL_single_merge_statement_buf
														   )
{
	commlink_CONTROLLER_DAILY_RECORD   lrecord;

	INT_32  last_index_of;

	UNS_32	lpos_of_poc_report_data_end;

	UNS_32	lrecord_count;

	UNS_32  rv;

	UNS_32  i;

	DATE_TIME lstartTime;

	// ----------

	rv = 0;

	// ----------

	if( !pinclude_poc_report_data )
	{
		lrecord_count = (pdlen / sizeof(commlink_CONTROLLER_DAILY_RECORD));
	}
	else
	{
		last_index_of = lastIndexOf(pucp, "Making Water Work", pdlen);

		if (last_index_of != -1)
		{
			// 1/12/2016 ajv : The 17 comes from the length of the Making Water Work string without a
			// NULL terminator.
			lpos_of_poc_report_data_end = last_index_of - 17;
		}
		else
		{
			lpos_of_poc_report_data_end = pdlen;
		}

		lrecord_count = ((lpos_of_poc_report_data_end) / sizeof(commlink_CONTROLLER_DAILY_RECORD));
	}

	// ----------

	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &lrecord, pucp, sizeof(commlink_CONTROLLER_DAILY_RECORD) );
		pucp += sizeof(commlink_CONTROLLER_DAILY_RECORD);
		rv += sizeof(commlink_CONTROLLER_DAILY_RECORD);

		// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
		// start date.
		if( lrecord.StartDT.D != 0 )
		{
			// ----------

			lstartTime = LEGACY_COMMON_swap_endianness_datetime(lrecord.StartDT);

			// ----------

			// 1/11/2017 wjb : add sql statement to update the lastTimestamp_DailyData in Controllers table
			if (i == 0)
			{
				update_last_timestamp_in_controller_table(pcontroller_id, lstartTime, "LastTimestamp_DailyData", pSQL_statements, pSQL_single_merge_statement_buf);
			}

			// ----------

			// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
			// since we rely on this to determine whether the string is empty or not.
			memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
			memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
			
			// ----------

			// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
			// values to insert.
			SQL_MERGE_build_insert_uint32_without_field_names(pcontroller_id, pSQL_insert_values_str);
			SQL_MERGE_build_insert_timestamp_without_field_names(lstartTime, pSQL_insert_values_str);
			SQL_MERGE_build_insert_timestamp_without_field_names( LEGACY_COMMON_swap_endianness_datetime(lrecord.EndDT), pSQL_insert_values_str );
			SQL_MERGE_build_insert_float_without_field_names( (LEGACY_COMMON_swap_endianness_uint16(lrecord.HisETNumber_us_10000u) / 10000.0F), pSQL_insert_values_str );
			SQL_MERGE_build_insert_float_without_field_names( (LEGACY_COMMON_swap_endianness_uint16(lrecord.GageNumber.ET_us_10000u) / 10000.0F), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( lrecord.GageNumber.Status, pSQL_insert_values_str );
			SQL_MERGE_build_insert_float_without_field_names((LEGACY_COMMON_swap_endianness_uint16(lrecord.TotalRain_us_100u) / 100.0F), pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names(lrecord.RainTable.Status, pSQL_insert_values_str);
			SQL_MERGE_build_insert_float_without_field_names( (LEGACY_COMMON_swap_endianness_uint16(lrecord.RainTable.Rain_us_100u) / 100.0F), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.Budget_ul), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.TestSeconds_ul), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.TestGallons_ul), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.ManualSeconds_ul), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.ManualGallons_ul), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.RRSeconds_ul), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names(LEGACY_COMMON_swap_endianness_uint32(lrecord.RRGallons_ul), pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names(LEGACY_COMMON_swap_endianness_uint32(lrecord.NonControllerSeconds_ul), pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names(LEGACY_COMMON_swap_endianness_uint32(lrecord.NonControllerGallons_ul), pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.ScheduledSeconds_ul), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.ScheduledGallons_ul), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.FOALSystemGallons_ul), pSQL_insert_values_str );

			// ----------
			
			// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
			// statements.
			SQL_MERGE_build_legacy_report_insert_and_append_to_SQL_statements("DailyData", lstartTime, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
		}
	}
	
	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static UNS_32 POC_REPORT_DATA_extract_and_store_data(	UNS_8 *pucp,
														const UNS_32 pdlen,
														const UNS_32 pcontroller_id,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf
													)
{
	commlink_POC_ACCUMULATOR_STRUCT   lrecord;

	char	lpoc_names[ POCS_PER_CONTROLLER ][ STATION_DESCRIPTION_LENGTH ];

	INT_32  last_index_of;

	UNS_32	lrecord_count;

	UNS_32  rv;

	UNS_32  i;

	UNS_32	p;

	// ----------

	rv = 0;

	last_index_of = -1;

	// ----------

	last_index_of = lastIndexOf(pucp, "Making Water Work", pdlen);

	if (last_index_of != -1 )
	{
		pucp += last_index_of;
		rv += last_index_of;
	}
	
	lrecord_count = ((pdlen - rv - (POCS_PER_CONTROLLER * STATION_DESCRIPTION_LENGTH)) / sizeof(commlink_POC_ACCUMULATOR_STRUCT));

	// ----------

	// 1/12/2016 ajv : First extract the three POC names for use later.
	for( p = 0; p < POCS_PER_CONTROLLER; ++p )
	{
		memcpy( lpoc_names[ p ], pucp, STATION_DESCRIPTION_LENGTH );
		pucp += STATION_DESCRIPTION_LENGTH;
		rv += STATION_DESCRIPTION_LENGTH;
	}

	// ----------

	for( i = 0; i < (lrecord_count / POCS_PER_CONTROLLER); ++i )
	{
		for( p = 0; p < POCS_PER_CONTROLLER; ++p )
		{
			memcpy( &lrecord, pucp, sizeof(commlink_POC_ACCUMULATOR_STRUCT) );
			pucp += sizeof(commlink_POC_ACCUMULATOR_STRUCT);
			rv += sizeof(commlink_POC_ACCUMULATOR_STRUCT);

			// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
			// start date.
			if( lrecord.start_dt.D != 0 )
			{
				// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
				// since we rely on this to determine whether the string is empty or not.
				memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
				memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
				memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

				// ----------

				// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
				SQL_MERGE_build_search_condition_uint32( "ControllerID", pcontroller_id, pSQL_search_condition_str );
				SQL_MERGE_build_search_condition_uint32( "POCID", p, pSQL_search_condition_str );
				SQL_MERGE_build_search_condition_timestamp("StartTimeStamp", LEGACY_COMMON_swap_endianness_datetime(lrecord.start_dt), pSQL_search_condition_str);

				// ----------

				// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
				// values to insert.
				SQL_MERGE_build_insert_uint32_without_field_names(pcontroller_id, pSQL_insert_values_str);
				SQL_MERGE_build_insert_uint32_without_field_names( p, pSQL_insert_values_str );
				SQL_MERGE_build_insert_timestamp_without_field_names( LEGACY_COMMON_swap_endianness_datetime(lrecord.start_dt), pSQL_insert_values_str );
				SQL_MERGE_build_insert_timestamp_without_field_names( LEGACY_COMMON_swap_endianness_datetime(lrecord.end_dt), pSQL_insert_values_str );
				SQL_MERGE_build_insert_string_without_field_names( lpoc_names[ p ], STATION_DESCRIPTION_LENGTH, pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.gallons_during_idle_ul), pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.seconds_during_idle_ul), pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.gallons_during_use_ul), pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.seconds_during_use_ul), pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.gallons_during_irrigation_ul), pSQL_insert_values_str );
				SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint32(lrecord.seconds_during_irrigation_ul), pSQL_insert_values_str );

				// ----------

				// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
				// statements.
				SQL_MERGE_build_legacy_report_insert_merge_and_append_to_SQL_statements("POCReportData", LEGACY_COMMON_swap_endianness_datetime(lrecord.start_dt), pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
			}
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
static UNS_32 STATION_REPORT_DATA_extract_and_store_500_data(   UNS_8 *pucp,
																const UNS_32 pdlen,
																const UNS_32 pcontroller_id,
																char *pSQL_statements,
																char *pSQL_search_condition_str,
																char *pSQL_insert_values_str,
																char *pSQL_single_merge_statement_buf
															)
{
	commlink_STATION_DAILY_RECORD__500_SERIES   lrecord;

	UNS_8   lstation_in_use[ MAX_STATIONS__500_SERIES ];

	UNS_32  lrecord_count;

	UNS_32  rv;

	UNS_32  i;

	UNS_32  s;

	// ----------

	rv = 0;

	// 1/11/2016 ajv : There can be a maximum of MAX_DAYS_OF_STATION_REPORT_DATA (16) records.
	lrecord_count = ((pdlen - sizeof(lstation_in_use)) / sizeof(commlink_STATION_DAILY_RECORD__500_SERIES));

	// ----------

	memcpy( &lstation_in_use, pucp, sizeof(lstation_in_use) );
	pucp += sizeof(lstation_in_use);
	rv += sizeof(lstation_in_use);

	// ----------

	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &lrecord, pucp, sizeof(commlink_STATION_DAILY_RECORD__500_SERIES) );
		pucp += sizeof(commlink_STATION_DAILY_RECORD__500_SERIES);
		rv += sizeof(commlink_STATION_DAILY_RECORD__500_SERIES);

		// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
		// start date.
		if( lrecord.StartDT.D != 0 )
		{
			for( s = 0; s < MAX_STATIONS__500_SERIES; ++s )
			{
				// 1/11/2016 ajv : Only insert records for stations that are in use
				if( lstation_in_use[ s ] )
				{
					// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
					// since we rely on this to determine whether the string is empty or not.
					memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
					memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
					memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

					// ----------

					// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
					SQL_MERGE_build_search_condition_uint32("ControllerID", pcontroller_id, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_uint32("StationNumber", s, pSQL_search_condition_str);
					SQL_MERGE_build_search_condition_timestamp("StartTimeStamp", LEGACY_COMMON_swap_endianness_datetime(lrecord.StartDT), pSQL_search_condition_str);

					// ----------

					// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
					// values to insert.
					SQL_MERGE_build_insert_uint32_without_field_names(pcontroller_id, pSQL_insert_values_str);
					SQL_MERGE_build_insert_uint32_without_field_names( s, pSQL_insert_values_str );
					SQL_MERGE_build_insert_timestamp_without_field_names( LEGACY_COMMON_swap_endianness_datetime(lrecord.StartDT), pSQL_insert_values_str );
					SQL_MERGE_build_insert_timestamp_without_field_names( LEGACY_COMMON_swap_endianness_datetime(lrecord.EndDT), pSQL_insert_values_str );
					SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.S_TestSeconds_us[ s ]), pSQL_insert_values_str );
					SQL_MERGE_build_insert_float_without_field_names( (LEGACY_COMMON_swap_endianness_uint32(lrecord.S_TestGallons_ul_10u[ s ]) / 10.0F), pSQL_insert_values_str );
					SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.S_ManualSeconds_us[ s ]), pSQL_insert_values_str );
					SQL_MERGE_build_insert_float_without_field_names( (LEGACY_COMMON_swap_endianness_uint32(lrecord.S_ManualGallons_ul_10u[ s ]) / 10.0F), pSQL_insert_values_str );
					SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.S_RRSeconds_us[ s ]), pSQL_insert_values_str );
					SQL_MERGE_build_insert_float_without_field_names( (LEGACY_COMMON_swap_endianness_uint32(lrecord.S_RRGallons_ul_10u[ s ]) / 10.0F), pSQL_insert_values_str );
					SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.S_ScheduledSeconds_us[ s ]), pSQL_insert_values_str );
					SQL_MERGE_build_insert_float_without_field_names( (LEGACY_COMMON_swap_endianness_uint32(lrecord.S_ScheduledGallons_ul_10u[ s ]) / 10.0F), pSQL_insert_values_str );

					// ----------

					// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
					// statements.
					SQL_MERGE_build_legacy_report_insert_merge_and_append_to_SQL_statements("StationData", LEGACY_COMMON_swap_endianness_datetime(lrecord.StartDT), pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
			}
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 STATION_REPORT_DATA_extract_and_store_600_data(   UNS_8 *pucp,
																const UNS_32 pdlen,
																const UNS_32 pcontroller_id,
																char *pSQL_statements,
																char *pSQL_search_condition_str,
																char *pSQL_insert_values_str,
																char *pSQL_single_merge_statement_buf
															)
{
	commlink_STATION_DAILY_RECORD   lrecord;

	UNS_8   lstation_in_use[ MAX_STATIONS__600_SERIES ];

	UNS_32  lrecord_count;

	UNS_32  rv;

	UNS_32  i;

	UNS_32  s;

	// ----------

	rv = 0;

	// 1/11/2016 ajv : There can be a maximum of MAX_DAYS_OF_STATION_REPORT_DATA (16) records.
	lrecord_count = ((pdlen - sizeof(lstation_in_use)) / sizeof(commlink_STATION_DAILY_RECORD));

	// ----------

	memcpy( &lstation_in_use, pucp, sizeof(lstation_in_use) );
	pucp += sizeof(lstation_in_use);
	rv += sizeof(lstation_in_use);

	// ----------

	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &lrecord, pucp, sizeof(commlink_STATION_DAILY_RECORD) );
		pucp += sizeof(commlink_STATION_DAILY_RECORD);
		rv += sizeof(commlink_STATION_DAILY_RECORD);

		// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
		// start date.
		if( lrecord.StartDT.D != 0 )
		{
			for( s = 0; s < MAX_STATIONS__600_SERIES; ++s )
			{
				// 1/11/2016 ajv : Only insert records for stations that are in use
				if( lstation_in_use[ s ] )
				{
					// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
					// since we rely on this to determine whether the string is empty or not.
					memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
					memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
					memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

					// ----------

					// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
					SQL_MERGE_build_search_condition_uint32( "ControllerID", pcontroller_id, pSQL_search_condition_str );
					SQL_MERGE_build_search_condition_uint32( "StationNumber", s, pSQL_search_condition_str );
					SQL_MERGE_build_search_condition_timestamp("StartTimeStamp", LEGACY_COMMON_swap_endianness_datetime(lrecord.StartDT), pSQL_search_condition_str);

					// ----------

					// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
					// values to insert.
					SQL_MERGE_build_insert_uint32_without_field_names(pcontroller_id, pSQL_insert_values_str);
					SQL_MERGE_build_insert_uint32_without_field_names( s, pSQL_insert_values_str );
					SQL_MERGE_build_insert_timestamp_without_field_names( LEGACY_COMMON_swap_endianness_datetime(lrecord.StartDT), pSQL_insert_values_str );
					SQL_MERGE_build_insert_timestamp_without_field_names( LEGACY_COMMON_swap_endianness_datetime(lrecord.EndDT), pSQL_insert_values_str );
					SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.S_TestSeconds_us[ s ]), pSQL_insert_values_str );
					SQL_MERGE_build_insert_float_without_field_names( (LEGACY_COMMON_swap_endianness_uint32(lrecord.S_TestGallons_ul_10u[ s ]) / 10.0F), pSQL_insert_values_str );
					SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.S_ManualSeconds_us[ s ]), pSQL_insert_values_str );
					SQL_MERGE_build_insert_float_without_field_names( (LEGACY_COMMON_swap_endianness_uint32(lrecord.S_ManualGallons_ul_10u[ s ]) / 10.0F), pSQL_insert_values_str );
					SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.S_RRSeconds_us[ s ]), pSQL_insert_values_str );
					SQL_MERGE_build_insert_float_without_field_names( (LEGACY_COMMON_swap_endianness_uint32(lrecord.S_RRGallons_ul_10u[ s ]) / 10.0F), pSQL_insert_values_str );
					SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.S_ScheduledSeconds_us[ s ]), pSQL_insert_values_str );
					SQL_MERGE_build_insert_float_without_field_names( (LEGACY_COMMON_swap_endianness_uint32(lrecord.S_ScheduledGallons_ul_10u[ s ]) / 10.0F), pSQL_insert_values_str );

					// ----------

					// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
					// statements.
					SQL_MERGE_build_legacy_report_insert_merge_and_append_to_SQL_statements("StationData", LEGACY_COMMON_swap_endianness_datetime(lrecord.StartDT), pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
				}
			}
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 STATION_HISTORY_extract_and_store_data(   UNS_8 *pucp,
														const UNS_32 pdlen,
														const UNS_32 pcontroller_id,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf
													)
{
	XMIT_LOG_LINE_DEF   lrecord;

	UNS_16  lstation_num;

	UNS_16  lrecord_count;

	UNS_32  rv;

	UNS_32  i;

	DATE_TIME lstartTime;

	// ----------

	rv = 0;

	// ----------

	while (rv < pdlen)
	{

		memcpy(&lstation_num, pucp, sizeof(UNS_16));
		pucp += sizeof(UNS_16);
		rv += sizeof(UNS_16);

		memcpy(&lrecord_count, pucp, sizeof(UNS_16));
		pucp += sizeof(UNS_16);
		rv += sizeof(UNS_16);

		// ----------

		for (i = 0; i < LEGACY_COMMON_swap_endianness_uint16(lrecord_count); ++i)
		{
			memcpy(&lrecord, pucp, sizeof(XMIT_LOG_LINE_DEF));
			pucp += sizeof(XMIT_LOG_LINE_DEF);
			rv += sizeof(XMIT_LOG_LINE_DEF);

			// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
			// start date.
			if (lrecord.StartDT.D != 0)
			{
				// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
				// since we rely on this to determine whether the string is empty or not.
				memset(pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT);
				memset(pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);
				memset(pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);
				memset(pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);
				memset(pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE);

				// ----------

				lstartTime = LEGACY_COMMON_swap_endianness_datetime(lrecord.StartDT);

				// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
				SQL_MERGE_build_search_condition_uint32("ControllerID", pcontroller_id, pSQL_search_condition_str);
				SQL_MERGE_build_search_condition_uint32("StationNumber", LEGACY_COMMON_swap_endianness_uint16(lstation_num), pSQL_search_condition_str);
				SQL_MERGE_build_search_condition_timestamp("StartTimeStamp", lstartTime, pSQL_search_condition_str);

				// ----------

				// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
				// values to insert.
				SQL_MERGE_build_upsert_uint32(pcontroller_id, SQL_MERGE_include_field_name_in_insert_clause, "ControllerID", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_uint32(LEGACY_COMMON_swap_endianness_uint16(lstation_num), SQL_MERGE_include_field_name_in_insert_clause, "StationNumber", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_timestamp(lstartTime, SQL_MERGE_include_field_name_in_insert_clause, "StartTimeStamp", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_uint32(lrecord.Program, SQL_MERGE_include_field_name_in_insert_clause, "Program", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_uint32(lrecord.Repeats, SQL_MERGE_include_field_name_in_insert_clause, "Repeats", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_float((LEGACY_COMMON_swap_endianness_uint16(lrecord.RequestedMinutes_us_10u) / 10.0F), SQL_MERGE_include_field_name_in_insert_clause, "RequestedTime", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_uint32(LEGACY_COMMON_swap_endianness_uint32(lrecord.IrrigSec), SQL_MERGE_include_field_name_in_insert_clause, "IrrigSec", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_float((LEGACY_COMMON_swap_endianness_uint32(lrecord.IrrigGal_ul_100u) / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "IrrigGal", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_float((LEGACY_COMMON_swap_endianness_uint16(lrecord.IrrigInch_us_100u) / 100.0F), SQL_MERGE_include_field_name_in_insert_clause, "IrrigInch", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_uint32(LEGACY_COMMON_swap_endianness_uint16(lrecord.FRate), SQL_MERGE_include_field_name_in_insert_clause, "FRate", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_uint32(LEGACY_COMMON_swap_endianness_uint16(lrecord.HiLimit), SQL_MERGE_include_field_name_in_insert_clause, "HiLimit", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_uint32(LEGACY_COMMON_swap_endianness_uint16(lrecord.LoLimit), SQL_MERGE_include_field_name_in_insert_clause, "LoLimit", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_int32(LEGACY_COMMON_swap_endianness_int16(lrecord.HOSeconds), SQL_MERGE_include_field_name_in_insert_clause, "HOSeconds", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_uint32(LEGACY_COMMON_swap_endianness_uint16(lrecord.bitFlag), SQL_MERGE_include_field_name_in_insert_clause, "BitFlag", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_uint32(lrecord.MS_LastReading, SQL_MERGE_include_field_name_in_insert_clause, "MS_LastReading", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_uint32(lrecord.MS_SetPoint, SQL_MERGE_include_field_name_in_insert_clause, "MS_SetPoint", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				SQL_MERGE_build_upsert_uint32(lrecord.MS_LastReadingStatus, SQL_MERGE_include_field_name_in_insert_clause, "MS_LastReadingStatus", pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str);
				
				// ----------

				// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
				// statements.
				SQL_MERGE_build_upsert_merge_and_append_to_SQL_statements("ET2kLog", pSQL_search_condition_str, SQL_MERGE_include_field_name_in_insert_clause, pSQL_update_str, pSQL_insert_fields_str, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
			}
		}
	}
	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 FLOW_RECORDING_extract_and_store_data(	UNS_8 *pucp,
														const UNS_32 pdlen,
														const UNS_32 pcontroller_id,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf
													)
{
	NEW_FLOW_RECORDER_RECORD   lrecord;

	UNS_32  lrecord_count;

	UNS_32  rv;

	UNS_32  i;

	DATE_TIME lrecordDate;

	// ----------

	rv = 0;

	lrecord_count = (pdlen / sizeof(NEW_FLOW_RECORDER_RECORD));

	// ----------

	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &lrecord, pucp, sizeof(NEW_FLOW_RECORDER_RECORD) );
		pucp += sizeof(NEW_FLOW_RECORDER_RECORD);
		rv += sizeof(NEW_FLOW_RECORDER_RECORD);

		// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
		// start date.
		if( lrecord.dt.D != 0 )
		{
			// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
			// since we rely on this to determine whether the string is empty or not.
			memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
			memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
			memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

			// ----------

			lrecordDate = LEGACY_COMMON_swap_endianness_datetime(lrecord.dt);

			// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
			SQL_MERGE_build_search_condition_uint32( "ControllerID", pcontroller_id, pSQL_search_condition_str );
			SQL_MERGE_build_search_condition_timestamp("FlowTimeStamp", lrecordDate, pSQL_search_condition_str);
			SQL_MERGE_build_search_condition_char("Controller", lrecord.controller_letter, pSQL_search_condition_str);
			SQL_MERGE_build_search_condition_uint32( "Station", lrecord.station_number, pSQL_search_condition_str );

			// ----------

			// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
			// values to insert.
			SQL_MERGE_build_insert_uint32_without_field_names(pcontroller_id, pSQL_insert_values_str);
			SQL_MERGE_build_insert_timestamp_without_field_names(lrecordDate, pSQL_insert_values_str);
			SQL_MERGE_build_insert_char_without_field_names( lrecord.controller_letter, pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( lrecord.station_number, pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.station_expected_flow), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.station_derated_expected_flow), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.station_hi_limit), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.station_lo_limit), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_uint16(lrecord.station_portion_of_actual), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( lrecord.flag, pSQL_insert_values_str );

			// ----------

			// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
			// statements.
			SQL_MERGE_build_legacy_report_insert_merge_and_append_to_SQL_statements("FlowRec", lrecordDate, pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
static UNS_32 BYPASS_RECORDING_extract_and_store_data(	UNS_8 *pucp,
														const UNS_32 pdlen,
														const UNS_32 pcontroller_id,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf
													)
{
	NEW_DMC_RECORDER_RECORD   lrecord;

	UNS_32  lrecord_count;

	UNS_32  rv;

	UNS_32  i;

	DATE_TIME lrecordDate;

	// ----------

	rv = 0;

	lrecord_count = (pdlen / sizeof(NEW_DMC_RECORDER_RECORD));

	// ----------

	for( i = 0; i < lrecord_count; ++i )
	{
		memcpy( &lrecord, pucp, sizeof(NEW_DMC_RECORDER_RECORD) );
		pucp += sizeof(NEW_DMC_RECORDER_RECORD);
		rv += sizeof(NEW_DMC_RECORDER_RECORD);

		// 10/15/2015 ajv : Only try to insert valid records - that is records that have a valid
		// start date.
		if( lrecord.dt.D != 0 )
		{
			// 9/16/2015 ajv : Make sure the various SQL clause strings are fully initialized to NULL
			// since we rely on this to determine whether the string is empty or not.
			memset( pSQL_single_merge_statement_buf, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT );
			memset( pSQL_search_condition_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );
			memset( pSQL_insert_values_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

			// ----------

			lrecordDate = LEGACY_COMMON_swap_endianness_datetime(lrecord.dt);

			// 9/16/2015 ajv : Build the search condition (e.g., "ON NetworkID=:NetworkID AND ...")
			SQL_MERGE_build_search_condition_uint32( "ControllerID", pcontroller_id, pSQL_search_condition_str );
			SQL_MERGE_build_search_condition_timestamp("FlowTimeStamp", lrecordDate, pSQL_search_condition_str);

			// ----------

			// 9/16/2015 ajv : Populate the INSERT and VALUES clauses of the MERGE statement with the
			// values to insert.
			SQL_MERGE_build_insert_uint32_without_field_names(pcontroller_id, pSQL_insert_values_str);
			SQL_MERGE_build_insert_timestamp_without_field_names(lrecordDate, pSQL_insert_values_str);
			SQL_MERGE_build_insert_uint32_without_field_names( lrecord.num_stations_on_uc, pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( lrecord.level_1_mv_state_uc, pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( lrecord.level_2_mv_state_uc, pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( lrecord.level_3_mv_state_uc, pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_int16(lrecord.level_1_avg_flow_us), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_int16(lrecord.level_2_avg_flow_us), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_int16(lrecord.level_3_avg_flow_us), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_int16(lrecord.level_1_trip_point_us), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_int16(lrecord.level_2_trip_point_us), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_int16(lrecord.trip_point_up_us), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_int16(lrecord.trip_point_down_us), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_int16(lrecord.system_flowrate_us), pSQL_insert_values_str );
			SQL_MERGE_build_insert_uint32_without_field_names( LEGACY_COMMON_swap_endianness_int16(lrecord.expected_flowrate_us), pSQL_insert_values_str );

			// ----------

			// 9/16/2015 ajv : Finally, build the MERGE statement and append it to the list of SQL
			// statements.
			SQL_MERGE_build_legacy_report_insert_merge_and_append_to_SQL_statements("BypassRec", lrecordDate, pSQL_search_condition_str, SQL_MERGE_do_not_include_field_name_in_insert_clause, NULL, pSQL_insert_values_str, pSQL_statements, pSQL_single_merge_statement_buf);
		}
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
DLLEXPORT UNS_32 LEGACY_REPORTS_extract_and_store_data(	const UNS_32 pMID,
														UNS_8 *pucp,
														const UNS_32 pdata_len,
														const UNS_32 pcontroller_id,
														const UNS_8 pis_a_600_series,
														const BOOL_32 phas_a_dash_f_interface,
														char *pSQL_statements,
														char *pSQL_search_condition_str,
														char *pSQL_update_str,
														char *pSQL_insert_fields_str,
														char *pSQL_insert_values_str,
														char *pSQL_single_merge_statement_buf
												   )
{
	UNS_32  rv;

	BOOL_32	lreport_data_includes_poc_report_data;

	// ----------

	rv = 0;

	// ----------

	memset( pSQL_statements, 0x00, SQL_MAX_LENGTH_FOR_ALL_MERGE_STATEMENTS );

	// 12/4/2015 ajv : In most cases, we don't perform an update - just a straight insert if the
	// record doesn't exist. However, for the case of the ET & Rain table, we send two records
	// and need to make sure slot 1's record is updated properly. To support this, Wissam has
	// added the pSQL_update_str buffer. However, since we only use it in one case, let's NULL
	// it just in case we get something that's not already NULL.
	memset( pSQL_update_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

	// 12/4/2015 ajv : Same as above applies to the pSQL_insert_fields_str buffer.
	memset( pSQL_insert_fields_str, 0x00, SQL_MAX_LENGTH_FOR_MERGE_STATEMENT_CLAUSE );

	// ----------

	switch( pMID )
	{
		case MID_CONTROLLER_REPORT_DATA_TO_PC:
			if( (pis_a_600_series) && (phas_a_dash_f_interface) )
			{
				lreport_data_includes_poc_report_data = (true);
			}
			else
			{
				lreport_data_includes_poc_report_data = (false);
			}

			rv += CONTROLLER_REPORT_DATA_extract_and_store_data(	pucp,
																	pdata_len,
																	pcontroller_id,
																	lreport_data_includes_poc_report_data,
																	pSQL_statements,
																	pSQL_search_condition_str,
																	pSQL_insert_values_str,
																	pSQL_single_merge_statement_buf
																);

			if (lreport_data_includes_poc_report_data && (pdata_len>rv) )
			{
				rv += POC_REPORT_DATA_extract_and_store_data(	pucp,
																pdata_len,
																pcontroller_id,
																pSQL_statements,
																pSQL_search_condition_str,
																pSQL_insert_values_str,
																pSQL_single_merge_statement_buf
															);
			}
			break;

		case MID_LEGACY_STATION_REPORT_DATA_TO_PC:
			if( pis_a_600_series )
			{
				rv += STATION_REPORT_DATA_extract_and_store_600_data(	pucp,
																		pdata_len,
																		pcontroller_id,
																		pSQL_statements,
																		pSQL_search_condition_str,
																		pSQL_insert_values_str,
																		pSQL_single_merge_statement_buf
																	);
			}
			else
			{
				rv += STATION_REPORT_DATA_extract_and_store_500_data(	pucp,
																		pdata_len,
																		pcontroller_id,
																		pSQL_statements,
																		pSQL_search_condition_str,
																		pSQL_insert_values_str,
																		pSQL_single_merge_statement_buf
																	);
			}
			break;

		case MID_LOGLINES_CONTROLLER_TO_PC:
			rv += STATION_HISTORY_extract_and_store_data(	pucp,
															pdata_len,
															pcontroller_id,
															pSQL_statements,
															pSQL_search_condition_str,
															pSQL_update_str,
															pSQL_insert_fields_str,
															pSQL_insert_values_str,
															pSQL_single_merge_statement_buf
														);
			break;

		case MID_SEND_FLOW_RECORDER_RECORDS_TO_PC:
			rv += FLOW_RECORDING_extract_and_store_data(	pucp,
															pdata_len,
															pcontroller_id,
															pSQL_statements,
															pSQL_search_condition_str,
															pSQL_insert_values_str,
															pSQL_single_merge_statement_buf
														);
			break;

		case MID_SEND_DMC_RECORDER_RECORDS_TO_PC:
			rv += BYPASS_RECORDING_extract_and_store_data(	pucp,
															pdata_len,
															pcontroller_id,
															pSQL_statements,
															pSQL_search_condition_str,
															pSQL_insert_values_str,
															pSQL_single_merge_statement_buf
														);
			break;
	}

	// ----------

	return( rv );
}

/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */
/* ---------------------------------------------------------- */

