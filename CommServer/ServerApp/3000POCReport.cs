﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using System.Threading;



//
// our representation of a Station History Record
//
namespace Calsense.CommServer
{
    class POCReport
    {
        // 9/4/2012 rmd : With the introduction of a pile of SYSTEM report lines we need to include
        // the gid of the system this record 'belongs' to.
        public UInt32 gid { get; set; }	

        // ------------------------

        // 8/1/2012 rmd : I suppose if there were to be pwr fail we could be off for days. So
        // include both the start and end time & dates. BUT isn't the end time_date of this record
        // the start time_date of the next record? Yes. So no need for both start and end.
        public UInt32 start_time { get; set; }

        public UInt16 start_date { get; set; }

        // ------------------------

        // A confusing subject. But the number of significant digits in a float is not all that
        // great. It is guaranteed to be at least 6. It may be 7 I think after reading. It is
        // certainly not eight. So tallying numbers after about 10 million gallons COULD start to
        // get pretty inaccurate. So not to take a chance we will store this accumulator as a
        // double.
        public Double gallons_total { get; set; }

        // 12/13/2012 ajv : Bob and I discussed the usefulnes of the seconds of flow
        // values at the POC level. We were unable to come up with any specific use
        // for these accumulators. However, rather than remove them, we will
        // continue to tally the seconds of actual flow and store them but simply
        // not display them at the controller or the central for now. This provides
        // some flexibility if someone comes up with a purpose for them in the
        // future.
        public UInt32 seconds_of_flow_total { get; set; }

        public Double gallons_during_idle { get; set; }

        // 12/13/2012 ajv : Bob and I discussed the usefulnes of the seconds of flow
        // values at the POC level. We were unable to come up with any specific use
        // for these accumulators. However, rather than remove them, we will
        // continue to tally the seconds of actual flow and store them but simply
        // not display them at the controller or the central for now. This provides
        // some flexibility if someone comes up with a purpose for them in the
        // future.
        public UInt32 seconds_of_flow_during_idle { get; set; }

        public Double gallons_during_mvor { get; set; }

        // 12/13/2012 ajv : Bob and I discussed the usefulnes of the seconds of flow
        // values at the POC level. We were unable to come up with any specific use
        // for these accumulators. However, rather than remove them, we will
        // continue to tally the seconds of actual flow and store them but simply
        // not display them at the controller or the central for now. This provides
        // some flexibility if someone comes up with a purpose for them in the
        // future.
        public UInt32 seconds_of_flow_during_mvor { get; set; }

        public Double gallons_during_irrigation { get; set; }

        // 12/13/2012 ajv : Bob and I discussed the usefulnes of the seconds of flow
        // values at the POC level. We were unable to come up with any specific use
        // for these accumulators. However, rather than remove them, we will
        // continue to tally the seconds of actual flow and store them but simply
        // not display them at the controller or the central for now. This provides
        // some flexibility if someone comes up with a purpose for them in the
        // future.
        public UInt32 seconds_of_flow_during_irrigation { get; set; }
    }
    class POCReportList
    {
        public IList<POCReport> records;
        public POCReportList()
        {
            records = new List<POCReport>();
        }
        public void LogIt(MyLogger l, NLog.LogLevel lev)
        {
#if LOGGING_STATION_HISTORY
            int i;

            l.WriteMessage(lev, "8001", "-----Dumping Structure from POC Report Message-----");
            l.WriteMessage(lev, "8001", "System GID:" + this.system_gid.ToString());
            for (i = 0; i < this.records.Count; i++ )
            {
                l.WriteMessage(lev, "8001", " ** Record " + i.ToString() + " ** ");
                l.WriteMessage(lev, "8001", "Time:" + this.records[i].time.ToString());
                l.WriteMessage(lev, "8001", "Date:" + this.records[i].date.ToString());
                l.WriteMessage(lev, "8001", " Station Number:" + this.records[i].station_num.ToString());
                l.WriteMessage(lev, "8001", " Box Index0:" + this.records[i].box_index_0.ToString());
                l.WriteMessage(lev, "8001", " Expected Flow:" + this.records[i].expected_flow.ToString());
                l.WriteMessage(lev, "8001", " Derated Expected Flow:" + this.records[i].derated_expected_flow.ToString());
                l.WriteMessage(lev, "8001", " High Limit:" + this.records[i].hi_limit.ToString());
                l.WriteMessage(lev, "8001", " Low Limit:" + this.records[i].lo_limit.ToString());
                l.WriteMessage(lev, "8001", " Portion of actual:" + this.records[i].portion_of_actual.ToString());
                l.WriteMessage(lev, "8001", " Flow check status:" + this.records[i].flow_check_status.ToString());

                l.WriteMessage(lev, "8001", " indicies_out_of_range: " + this.records[i].FRF_NO_CHECK_REASON_indicies_out_of_range.ToString());
                l.WriteMessage(lev, "8001", " station_cycles_too_low: " + this.records[i].FRF_NO_CHECK_REASON_station_cycles_too_low.ToString());
                l.WriteMessage(lev, "8001", " cell_iterations_too_low: " + this.records[i].FRF_NO_CHECK_REASON_cell_iterations_too_low.ToString());
                l.WriteMessage(lev, "8001", " no_flow_meter: " + this.records[i].FRF_NO_CHECK_REASON_no_flow_meter.ToString());
                l.WriteMessage(lev, "8001", " not_enabled_by_user_setting: " + this.records[i].FRF_NO_CHECK_REASON_not_enabled_by_user_setting.ToString());
                l.WriteMessage(lev, "8001", " acquiring_expected: " + this.records[i].FRF_NO_CHECK_REASON_acquiring_expected.ToString());
                l.WriteMessage(lev, "8001", " combo_not_allowed_to_check: " + this.records[i].FRF_NO_CHECK_REASON_combo_not_allowed_to_check.ToString());
                l.WriteMessage(lev, "8001", " unstable_flow: " + this.records[i].FRF_NO_CHECK_REASON_unstable_flow.ToString());
                l.WriteMessage(lev, "8001", " cycle_time_too_short: " + this.records[i].FRF_NO_CHECK_REASON_cycle_time_too_short.ToString());
                l.WriteMessage(lev, "8001", " irrigation_restart: " + this.records[i].FRF_NO_CHECK_REASON_irrigation_restart.ToString());
                l.WriteMessage(lev, "8001", " not_supposed_to_check: " + this.records[i].FRF_NO_CHECK_REASON_not_supposed_to_check.ToString());
                l.WriteMessage(lev, "8001", " zero_flow_rate: " + this.records[i].FRF_NO_UPDATE_REASON_zero_flow_rate.ToString());
                l.WriteMessage(lev, "8001", " thirty_percent: " + this.records[i].FRF_NO_UPDATE_REASON_thirty_percent.ToString());

            }
#endif
        }
    }

}
