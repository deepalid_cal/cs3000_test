﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using System.Threading;

//
// our representation of a Weather Data Record
//
namespace Calsense.CommServer
{
    class WeatherData
    {
        public UInt16? et_status { get; set; }
        public UInt16? et_value { get; set; }
        public UInt16? rain_status { get; set; }
        public UInt16? rain_value { get; set; }
        public DateTime stamp { get; set; }
    }
}
