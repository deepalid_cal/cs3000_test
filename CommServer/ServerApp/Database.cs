//
// Database.cs - common routines to open and close an Advantage Database connection
// uses configurable values for the DB Path, Username, Password
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Advantage.Data.Provider;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Configuration;
using Calsense.Logging;

namespace Calsense.CommServer
{
	internal class ServerDB
	{

		private CancellationTokenSource _cts;
        
        private static string getDB_ConnectionString
        {
            get
            {
                return ConfigurationManager.AppSettings["connectionString"];
            }
        }

		public ServerDB(CancellationTokenSource cts)
		{
			_cts = cts;

		}
        
        // ----

        public async Task<AdsConnection> OpenDBConnection( String id, bool log_it = false )
        {
            var _logger = new MyLogger(typeof(ServerDB).Name, id);
            if( log_it )
            {
                _logger.WriteMessage(NLog.LogLevel.Debug, "7001", "Opening DB connection...");
            }

            AdsConnection connection = new AdsConnection();
            connection.ConnectionString = getDB_ConnectionString;

                try
                {
                    await connection.OpenAsync();
                }
                catch (AdsException e)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "7091", "Database Exception:" + e.Message);
                    return (null);
                }
                catch (Exception e)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "7092", "Exception:" + e.Message);
                    return (null);
                }

            return(connection);
        }

        public  AdsConnection OpenDBConnectionNotAsync(String id, bool log_it = false )
        {
            var _logger = new MyLogger(typeof(ServerDB).Name, id);
            if( log_it )
            {
                _logger.WriteMessage(NLog.LogLevel.Debug, "7001", "Opening DB connection...");
            }

            AdsConnection connection = new AdsConnection();
            connection.ConnectionString = getDB_ConnectionString;

            try
            {
                connection.Open();
            }
            catch (AdsException e)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "7091", "Database Exception:" + e.Message);
                return (null);
            }
            catch (Exception e)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "7092", "Exception:" + e.Message);
                return (null);
            }

            return (connection);
        }

        public void CloseDBConnection(AdsConnection connection, String id, bool log_it = false )
        {
            var _logger = new MyLogger(typeof(ServerDB).Name, id);
            if( log_it )
            {
                _logger.WriteMessage(NLog.LogLevel.Debug, "7001", "Closing DB connection...");
            }

            using (connection)
            {
                try
                {
                    connection.Close();
                }
                catch (AdsException e)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "7093", "Database Exception:" + e.Message);
                }
                catch (Exception e)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "7094", "Exception:" + e.Message);
                }
            }
        }

        // ----

        public async Task<AdsConnection> OpenDBConnection( MyLogger _logger, bool log_it = false )
        {
            if( log_it )
            {
                _logger.WriteMessage(NLog.LogLevel.Debug, "7001", "Opening DB connection...");
            }

            AdsConnection connection = new AdsConnection();
            connection.ConnectionString = getDB_ConnectionString;

            try
            {
                await connection.OpenAsync();
            }
            catch (AdsException e)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "7091", "Database Exception:" + e.Message);
                return (null);
            }
            catch (Exception e)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "7092", "Exception:" + e.Message);
                return (null);
            }

            return (connection);
        }

        public AdsConnection OpenDBConnectionNotAsync(MyLogger _logger, bool log_it = false )
        {
            if( log_it )
            {
                _logger.WriteMessage(NLog.LogLevel.Debug, "7001", "Opening DB connection...");
            }

            AdsConnection connection = new AdsConnection();
            connection.ConnectionString = getDB_ConnectionString;

            try
            {
                connection.Open();
            }
            catch (AdsException e)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "7091", "Database Exception:" + e.Message);
                return (null);
            }
            catch (Exception e)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "7092", "Exception:" + e.Message);
                return (null);
            }

            return (connection);
        }

        public void CloseDBConnection(AdsConnection connection, MyLogger _logger, bool log_it = false )
        {
            if( log_it )
            {
                _logger.WriteMessage(NLog.LogLevel.Debug, "7001", "Closing DB connection...");
            }    

            using (connection)
            {
                try
                {
                    connection.Close();
                }
                catch (AdsException e)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "7093", "Database Exception:" + e.Message);
                }
                catch (Exception e)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "7094", "Exception:" + e.Message);
                }
            }
        }

        // -----

        // Execute query 
        public static async Task<bool> Execute(MyLogger _logger, CancellationTokenSource cts, string query)
        {
            bool abort = true;
            AdsCommand cmd;
            bool success = false;

            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger.MyId());
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                do
                {
                    try
                    {

                        cmd = new AdsCommand(query, conn);

                        _logger.WriteMessage(NLog.LogLevel.Debug, "7095", "SQL: " + cmd.CommandText);

                        cmd.ExecuteNonQuery();

                        abort = false;
                        cmd.Unprepare();

                        success = true;
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "7096", "Database exception during execute query: " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "7097", "Database Error on execute query: " + query);
                }
                db.CloseDBConnection(conn, _logger.MyId());
            }
            return (success);
        }

        /// <summary>
        /// Execute a AdsCommand (that returns a 1x1 resultset) against the specified AdsConnection
        /// using the provided parameters.
        /// </summary>
        public static async Task<object> ExecuteScalar(MyLogger _logger, CancellationTokenSource cts, string query)
        {
            bool abort = true;
            AdsCommand cmd;
            object retval = null;

            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger.MyId());
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                do
                {
                    try
                    {

                        cmd = new AdsCommand(query, conn);

                        _logger.WriteMessage(NLog.LogLevel.Debug, "7095", "SQL: " + cmd.CommandText);

                        // Execute the command & return the results
                        retval = cmd.ExecuteScalar();

                        abort = false;
                        cmd.Unprepare();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "7096", "Database exception during execute query: " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "7097", "Database Error on execute query: " + query);
                }
                db.CloseDBConnection(conn, _logger.MyId());
            }
            return (retval);
        }

        public static void FlushConnectionsPool()
        {
            try
            {
                AdsConnection.FlushConnectionPool();
            }
            catch (AdsException e)
            {
                var _logger = new MyLogger(typeof(ServerDB).Name, "");
                _logger.WriteMessage(NLog.LogLevel.Error, "27091", "Database Exception while flushing db pool :" + e.Message);
            }
        }
	}
}
