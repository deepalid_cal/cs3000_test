﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using Advantage.Data.Provider;
using System.Xml;
using System.Xml.Serialization; // to save state across calls



namespace Calsense.CommServer
{

    public class MsgProgramRequest
    {
        // test call unmanged C code
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint Dummy(ref DUMMY_STRUCT pp,
        uint why,
        [In, Out] byte[] dest,
        uint sz,
        uint idx);

        public class ProgramRequestStore
        {
            public ProgramRequestStore()
            {
                ETID = null;
                RainID = null;
                ETStatus = null;
                ETValue = null;
                RainStatus = null;
                RainValue = null;
                WantsET = false;
                WantsRain = false;
            }
            public Int32? ETID { set; get; }
            public Int32? RainID { set; get; }
            public Int32? ETStatus { set; get; }
            public Int32? ETValue { set; get; }
            public Int32? RainStatus { set; get; }
            public Int32? RainValue { set; get; }
            public bool WantsET { set; get; }
            public bool WantsRain { set; get; }
        }
        public static string CreateStringFromObject(Object YourClassObject)
        {
            XmlDocument xmlDoc = new XmlDocument();   //Represents an XML document, 
            // Initializes a new instance of the XmlDocument class.          
            XmlSerializer xmlSerializer = new XmlSerializer(YourClassObject.GetType());
            // Creates a stream whose backing store is memory. 
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, YourClassObject);
                xmlStream.Position = 0;
                //Loads the XML document from the specified string.
                xmlDoc.Load(xmlStream);
                return xmlDoc.InnerXml;
            }
        }
        public static Object CreateObjectFromString(string XMLString,Object YourClassObject)
        {            
            XmlSerializer oXmlSerializer =new XmlSerializer(YourClassObject.GetType()); 
            //The StringReader will be the stream holder for the existing XML file 
            YourClassObject = oXmlSerializer.Deserialize(new StringReader(XMLString)); 
         //initially deserialized, the data is represented by an object without a defined type 
         return YourClassObject;
         } 
        //
        // Parses Weather Request inbond message from controller
        // returns via Tuple:
        //   List of byte arrays, which are 0 to n messages to send as a response
        //   String that will be passed back in if this connection stays up
        //   Boolean as a flag to terminate connection or not
        //   int as an internal state to track thru session (if needed)
        //
        public static async Task<Tuple<List<byte[]>,String, bool, int>> Parse(ClientInfoStruct client, CancellationTokenSource cts)
        {
            byte[] data;
            MyLogger _logger;
            uint size;
            byte[] Buffer = null;
            AdsCommand cmd;
            bool abort = true;
            DateTime ReceivedTimestamp;
            int State;
            UInt32 crc;
            String latitude;
            String longitude;
            ProgramRequestStore pds = new ProgramRequestStore();   // where we track this request

            _logger = new MyLogger(typeof(MsgProgramRequest).Name);
            size = client.rxlen;
            Buffer = client.rx;

            ReceivedTimestamp = DateTime.Now;

            State = client.internalstate;

            //
            // Database fields set externally to note weather sharing:
            // Controllers -> ETSharingControllerID
            // Controllers -> RainSharingControllerID
            //
            // If the ETSharingControllerID is -1 AND RainSharingControllerID is -1, 
            // there is nothing to do since no controller is specified. You’ll respond 
            // with a simple ACK that no weather is available and the controller will terminate the connection.
            //
            // If the ETSharingControllerID is 0 and/or RainSharingControllerID is 0, 
            // it means you’ll need to issue a request to our internal WeatherSense 
            // application with the controller’s latitude and longitude which should be 
            // available in the Controllers table for you. I’ve attached a sample request. 
            // You need to establish an outbound TCP connection to 64.73.242.102:80, send 
            // the XML file, wait for the associated XML response, parse the response, and 
            // send the ET (if ETCode is 0) and rain (if RainCode is 0) back to the 
            // controller. Any code other than a 0 indicates an error and you shouldn’t 
            // share the weather back in that case. I’ll provide you with a list of those 
            // once you get into it.
            //
            // If the ETSharingControllerID and/or RainSharingControllerID is something 
            // greater than 0, it means they’ve selected a different network to get weather 
            // from. You’ll need to go to the table where you’ve stored the uploaded weather,
            // grab the associated ET and/or rain values, and send them back. There will 
            // likely be some stipulations on this based upon what the ET and rain status 
            // values are in the table, but I don’t have that info for you yet.
            //

            switch (State)
            {
                case 0:     // first connection message from controller, should have one byte with WEATHER_DATA_HAS_ET/RAIN bits possibly set.
                    if (client.mid != Constants.MID_TO_COMMSERVER_CHECK_FOR_WEATHER_init_packet)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "1891", "Expected MID_TO_COMMSERVER_CHECK_FOR_WEATHER_init_packet but got " + client.mid.ToString());
                        State = 99;  // error out
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "1804", "Processing Program Data Request message...");
                        if ((Buffer[0] & (1 << Constants.WEATHER_DATA_HAS_ET)) != 0)
                        {
                            wds.WantsET = true;
                        }
                        else
                        {
                            wds.WantsET = false;
                        }
                        if ((Buffer[0] & (1 << Constants.WEATHER_DATA_HAS_RAIN)) != 0)
                        {
                            wds.WantsRain = true;
                        }
                        else
                        {
                            wds.WantsRain = false;
                        }
                        ReceivedTimestamp = DateTime.Now;
                    }
                    break;

                case 1:     // there was weather data, we have sent init packet, and this should be the ACK, now send data packet
                    if (client.mid != Constants.MID_TO_COMMSERVER_WEATHER_DATA_init_ack)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "1891", "Expected MID_TO_COMMSERVER_WEATHER_DATA_init_ack but got " + client.mid.ToString());
                        State = 99;  // error out
                    }
                    else
                    {
                        // load our variables up from passed in string
                        _logger.WriteMessage(NLog.LogLevel.Debug, "1822", "Internal string: " + client.sessioninfo.ToString());
                        wds = (WeatherDataStore)CreateObjectFromString(client.sessioninfo, wds);
                    }
                    break;

                case 2:     // controller should be acking our data
                    if (client.mid != Constants.MID_TO_COMMSERVER_WEATHER_DATA_data_full_receipt_ack)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "1892", "Expected MID_TO_COMMSERVER_WEATHER_DATA_data_full_receipt_ack but got " + client.mid.ToString());
                        State = 99;  // error out
                    }
                    break;
            }



#if DUMPRAWBYTES
            _logger.WriteMessage(NLog.LogLevel.Info, "3004", "Dump of bytes raw:" + size.ToString());
            for (i = 0, j = (int)size; i < size; i += 8)
            {
                if (j >= 8)
                {
                    s = Buffer[i].ToString("x2") + " " + Buffer[i + 1].ToString("x2") + " " + Buffer[i + 2].ToString("x2") + " " + Buffer[i + 3].ToString("x2") + " " + Buffer[i + 4].ToString("x2") + " " + Buffer[i + 5].ToString("x2") + " " + Buffer[i + 6].ToString("x2") + " " + Buffer[i + 7].ToString("x2");
                    j -= 8;
                }
                else
                {
                    s = "";
                    while (j > 0)
                    {
                        s = s + Buffer[i].ToString("x2") + " ";
                        j--;
                        i++;
                    }
                }
                _logger.WriteMessage(NLog.LogLevel.Debug, "8001", s);
                if (j <= 0) break;
                Thread.Sleep(10);       // allow UDP logger to catch up
            }
#endif
            data = Encoding.Unicode.GetBytes(String.Format("Received {0} bytes.", size));

            // check for configuration in database
            if (State == 0)
            {

                _logger.WriteMessage(NLog.LogLevel.Info, "1820", "Checking for Weather config in Controllers table...");
                ServerDB db = new ServerDB(cts);

                Task<AdsConnection> connTask = db.OpenDBConnection();
                AdsConnection conn = await connTask;
                if (conn != null)
                {

                    // NOTE: Transactions not supported on local server, so ignore that exception
                    // insert data into DB and retrieve new network id to assign to controller
                    AdsTransaction txn = null;

                    try
                    {
                        txn = conn.BeginTransaction();
                    }
                    catch (System.NotSupportedException)
                    {

                    }
                    do
                    {
                        try
                        {
                            if (wds.WantsET)
                            {
                                cmd = new AdsCommand("SELECT IFNULL(ETSharingControllerID,-2) from Controllers c " +
                                                     " LEFT OUTER JOIN CS3000Controllers s ON c.ControllerID=s.ControllerID " +
                                                     " WHERE BoxIndex = 0 and NetworkID=" + client.nid.ToString(), conn, txn);
                                _logger.WriteMessage(NLog.LogLevel.Debug, "1853", "SQL: " + cmd.CommandText);
                                wds.ETID = (Int32?)cmd.ExecuteScalar();
                                cmd.Unprepare();
                            }

                            if (wds.WantsRain)
                            {
                                cmd = new AdsCommand("SELECT IFNULL(RainSharingControllerID,-2) from Controllers c " +
                                                     " LEFT OUTER JOIN CS3000Controllers s ON c.ControllerID=s.ControllerID " +
                                                     " WHERE BoxIndex = 0 and NetworkID=" + client.nid.ToString(), conn, txn);
                                _logger.WriteMessage(NLog.LogLevel.Debug, "1853", "SQL: " + cmd.CommandText);
                                wds.RainID = (Int32?)cmd.ExecuteScalar();
                                cmd.Unprepare();
                            }

                            if ((wds.ETID == null || wds.ETID == -2 || wds.ETID == -1) && (wds.RainID == null | wds.RainID == -2 || wds.RainID == -1))
                            {
                                // we will nack, as we have nothing configured/present to know about rain/et
                                State = 10;
                                _logger.WriteMessage(NLog.LogLevel.Info, "1858", "No ET/Rain configured/requested.");
                            }
                            else
                            {
                                _logger.WriteMessage(NLog.LogLevel.Info, "1859", "ET/Rain configured, gathering...");
                                // see if we need to get lat/long so we can do a WeatherSend API lookup
                                if (wds.ETID == 0 || wds.RainID == 0)
                                {
                                    cmd = new AdsCommand("SELECT ifnull(Latitude,'0.0') from Controllers c " +
                                                         " LEFT OUTER JOIN CS3000Controllers s ON c.ControllerID=s.ControllerID " +
                                                         " WHERE BoxIndex = 0 and NetworkID=" + client.nid.ToString(), conn, txn);
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "1853", "SQL: " + cmd.CommandText);
                                    latitude = (String)cmd.ExecuteScalar();
                                    cmd.Unprepare();
                                    cmd = new AdsCommand("SELECT ifnull(Longitude,'0.0') from Controllers c " +
                                                         " LEFT OUTER JOIN CS3000Controllers s ON c.ControllerID=s.ControllerID " +
                                                         " WHERE BoxIndex = 0 and NetworkID=" + client.nid.ToString(), conn, txn);
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "1853", "SQL: " + cmd.CommandText);
                                    longitude = (String)cmd.ExecuteScalar();
                                    cmd.Unprepare();
                                    // make API call to weather server for data

                                    Tuple<bool, Int32, Double, Int32, Double> r = Misc.CallWeatherAPI(_logger, cts, latitude.ToString(), longitude.ToString()).Result;
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "1850", "Weather Result via API: " + r.Item1.ToString() + ", " + r.Item2.ToString() + ", " + r.Item3.ToString() + ", " + r.Item4.ToString() + ", " + r.Item5.ToString());
                                    // result order: status, etcode, et, raincode, rain
                                    if(wds.ETID == 0)
                                    {
                                        wds.ETStatus = r.Item2;
                                        wds.ETValue = (Int32)(r.Item3 * 100.0); // convert from double to hundreths of inches
                                    }
                                    if(wds.RainID == 0)
                                    {
                                        wds.RainStatus = r.Item4;
                                        wds.RainValue = (Int32)(r.Item5 * 100.0); // convert from double to hundreths of inches
                                    }
                                }
                                if (wds.ETID > 0)
                                {
                                    // different network to look at for ET
                                    cmd = new AdsCommand("SELECT ifnull(ETStatus, 0) FROM CS3000WeatherData WHERE " +
                                        " (NetworkID=" + wds.ETID.ToString() +
                                        " AND CAST(TIMESTAMPADD(SQL_TSI_HOUR, " + Constants.RAIN_ROLLOVER_HOUR_DELTA.ToString() +
                                        ", WeatherTimeStamp) as SQL_DATE) = '" + ReceivedTimestamp.AddHours(Constants.RAIN_ROLLOVER_HOUR_DELTA).ToString("yyyy-MM-dd") +
                                        "')  ", conn, txn);
                                    wds.ETStatus = (Int32?)cmd.ExecuteScalar();
                                    cmd.Unprepare();
                                    cmd = new AdsCommand("SELECT ifnull(ETValue, 0) FROM CS3000WeatherData WHERE " +
                                        " (NetworkID=" + wds.ETID.ToString() +
                                        " AND CAST(TIMESTAMPADD(SQL_TSI_HOUR, " + Constants.RAIN_ROLLOVER_HOUR_DELTA.ToString() +
                                        ", WeatherTimeStamp) as SQL_DATE) = '" + ReceivedTimestamp.AddHours(Constants.RAIN_ROLLOVER_HOUR_DELTA).ToString("yyyy-MM-dd") +
                                        "')  ", conn, txn);
                                    wds.ETValue = (Int32?)cmd.ExecuteScalar();
                                    cmd.Unprepare();

                                }
                                if (wds.RainID > 0)
                                {
                                    // different network to look at for Rain
                                    cmd = new AdsCommand("SELECT ifnull(RainStatus, 0) FROM CS3000WeatherData WHERE " +
                                        " (NetworkID=" + wds.ETID.ToString() +
                                        " AND CAST(TIMESTAMPADD(SQL_TSI_HOUR, " + Constants.RAIN_ROLLOVER_HOUR_DELTA.ToString() +
                                        ", WeatherTimeStamp) as SQL_DATE) = '" + ReceivedTimestamp.AddHours(Constants.RAIN_ROLLOVER_HOUR_DELTA).ToString("yyyy-MM-dd") +
                                        "')  ", conn, txn);
                                    wds.RainStatus = (Int32?)cmd.ExecuteScalar();
                                    cmd.Unprepare();
                                    cmd = new AdsCommand("SELECT ifnull(RainValue, 0) FROM CS3000WeatherData WHERE " +
                                        " (NetworkID=" + wds.ETID.ToString() +
                                        " AND CAST(TIMESTAMPADD(SQL_TSI_HOUR, " + Constants.RAIN_ROLLOVER_HOUR_DELTA.ToString() +
                                        ", WeatherTimeStamp) as SQL_DATE) = '" + ReceivedTimestamp.AddHours(Constants.RAIN_ROLLOVER_HOUR_DELTA).ToString("yyyy-MM-dd") +
                                        "')  ", conn, txn);
                                    wds.RainValue = (Int32?)cmd.ExecuteScalar();
                                    cmd.Unprepare();
                                }
                            }
                            // if we made it to here, we are good to commit all our changes
                            abort = false;
                        }
                        catch (Exception e)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "1791", "Database exception during Weather Request checking: " + e.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    if (abort == true)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "1791", "Database Error on Weather Request checking.");
                        txn.Rollback();
                    }
                    else
                    {
                        txn.Commit();
                    }
                    db.CloseDBConnection(conn);
                }

            }
            else
            {
                abort = false;  // no DB activity that could have failed ;)
            }

            // if nothing saved to database, do not ACK
            if (abort == true)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "1801", "Not acking, problem with DB query.");
                return (Tuple.Create((List<byte[]>)null, (String)null, true, 0));
            }

            /*
            Once you receive this, you need to look at the controller table to see if this network ID 
            has an ET and/or Rain source specified, based upon which bits are set. If it doesn't have 
            either set, you ACK with MID_FROM_COMMSERVER_WEATHER_REQUEST_ACK_NO_WEATHER.

            If one is set, ACK with MID_FROM_COMMSERVER_WEATHER_REQUEST_ACK_WEATHER_AVAILABLE. At that 
            point, it's going to work like the check for updates, where you have control. Once you've 
            gathered the data (either from the weather table or from WeatherSense, if specified), you 
            send the MID_FROM_COMMSERVER_WEATHER_DATA_init_packet, wait for the ACK, followed by the 
            _data_packet. The _data_packet's contents are as follows:

            8-bit bitfield - currently has two bits that can be set (defined in main_app\src\communication\cent_comm.h)
              #define WEATHER_DATA_HAS_ET		(0)
              #define WEATHER_DATA_HAS_RAIN	(1)
            ET_TABLE_ENTRY - only if WEATHER_DATA_HAS_ET bit is set (defined in main_app\src\irrigation\weather_tables.h)
            RAIN_TABLE_ENTRY - only if WEATHER_DATA_HAS_RAIN bit is set

            The controller will ACK once it receives and stores the data.
             */

            // build appropriate ack response
            List<byte[]> rsplist = new List<byte[]>();
            String internalstatestring = null;
            if ((State == 0 || State == 10) && abort != true)
            {
                if (client.nid != 0)
                {
                    // response holder stuff common to any response we send
                    var mem = new MemoryStream(0);

                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(client.sn, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(client.nid, mem);
                    // mid - either ack or nack
                    if(State == 10)
                    {
                        Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_CHECK_FOR_WEATHER_ACK_NO_WEATHER, mem);
                        _logger.WriteMessage(NLog.LogLevel.Info, "1871", "Build Weather Request Nak response.");
                        // get the built response to return and be sent
                        data = mem.ToArray();
                        rsplist.Add(data);  // NACK is our response
                    }
                    else 
                    {
                        Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_CHECK_FOR_WEATHER_ACK_WEATHER_AVAILABLE, mem);
                        _logger.WriteMessage(NLog.LogLevel.Info, "1872", "Build Weather Request Ack response.");
                        // get the built response to return and be sent
                        data = mem.ToArray();
                        rsplist.Add(data);  // Ack is our response, followed by init packet
                        // add an init packet to get the response to controller started
                        var init = new MemoryStream(0);
                        // PID byte
                        init.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                        // message class byte
                        init.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                        // to serial number
                        Misc.Uint32ToNetworkBytes(client.sn, init);
                        // network id
                        Misc.Uint32ToNetworkBytes(client.nid, init);
                        // mid
                        Misc.Uint16ToNetworkBytes((UInt16)(Constants.MID_FROM_COMMSERVER_WEATHER_DATA_init_packet), init);
                        // expected_size, we send a 2 byte date
                        Misc.Uint32ToNetworkBytes((UInt16)2, init);
                        // expected CRC
                        crc = Misc.PackedDateCRC(Misc.CurrentPackedDate());
                        _logger.WriteMessage(NLog.LogLevel.Info, "1873", "Computed data msg CRC is 0x" + crc.ToString("X8"));
                        Misc.Uint32ToBigendianBytes(crc, init);
                        // expected_packets
                        Misc.Uint16ToNetworkBytes((UInt16)1, init);
                        // mid again
                        Misc.Uint16ToNetworkBytes((UInt16)(Constants.MID_FROM_COMMSERVER_WEATHER_DATA_init_packet), init);
                        _logger.WriteMessage(NLog.LogLevel.Info, "1879", "Init packet for Weather Data to Controller built.");
                        // get the built response to return and be sent
                        data = init.ToArray();
                        rsplist.Add(data);
                        State++;    // state 1 next time thru
                        // serialize our wds class so will load back next time thru
                        internalstatestring = CreateStringFromObject(wds);
                    }

                }
            }
            else if (State == 1 && abort != true)
            {
                // send data packet with weather data
                var mem = new MemoryStream(0);

                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(client.sn, mem);
                // network id
                Misc.Uint32ToNetworkBytes(client.nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_WEATHER_DATA_data_packet, mem);
                // packet number
                Misc.Uint16ToNetworkBytes((UInt16)1, mem);
                // mid again
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_WEATHER_DATA_data_packet, mem);

                // now data...which is:
                // 8-bit bitfield - currently has two bits that can be set (defined in main_app\src\communication\cent_comm.h)
                // #define WEATHER_DATA_HAS_ET		(0)
                // #define WEATHER_DATA_HAS_RAIN	(1)
                // ET_TABLE_ENTRY - only if WEATHER_DATA_HAS_ET bit is set (defined in main_app\src\irrigation\weather_tables.h)
	            // UNS_16	et_inches_u16_10000u;
                // UNS_16	status;
                // ET_TABLE_ENTRY;
                // RAIN_TABLE_ENTRY - only if WEATHER_DATA_HAS_RAIN bit is set
                // Date field
                // Misc.Uint16ToNetworkBytes(Misc.CurrentPackedDate(), mem);
                byte v1 = 0;
                if(wds.WantsET)
                {
                    v1 |= (1 << Constants.WEATHER_DATA_HAS_ET);
                }
                if (wds.WantsRain)
                {
                    v1 |= (1 << Constants.WEATHER_DATA_HAS_RAIN);
                }
                mem.WriteByte(v1);
                if(wds.WantsET)
                {
                    UInt16 etinches = (UInt16) (wds.ETValue == null ? 0 : wds.ETValue);
                    Int16 etstatus = (Int16)(wds.ETStatus == null ? 0 : wds.ETStatus);
                    Misc.Uint16ToNetworkBytes((UInt16)etinches, mem);
                    Misc.Uint16ToNetworkBytes((UInt16)etstatus, mem);
                }
                if(wds.WantsRain)
                {
                    UInt16 raininches = (UInt16)(wds.RainValue == null ? 0 : wds.RainValue);
                    Int16 rainstatus = (Int16)(wds.RainStatus == null ? 0 : wds.RainStatus);
                    Misc.Uint16ToNetworkBytes((UInt16)raininches, mem);
                    Misc.Uint16ToNetworkBytes((UInt16)rainstatus, mem);
                }

                data = mem.ToArray();
                rsplist.Add(data);
                State++;
            }
            else if (State == 99 || abort == true)
            {
                // some kind of error
                // make empty struct so termination flag will be set below
                var mem = new MemoryStream(0);
                data = mem.ToArray();
            }

            // we terminate connection in any case so set true flag
            var terminate = data.Length > 0 ? false : true;
            if(State == 10)terminate = true;    // terminate if no data, or naking
            return (Tuple.Create(rsplist, internalstatestring, terminate, State));
        }
    }
}
