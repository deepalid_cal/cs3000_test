﻿using System;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using Advantage.Data.Provider;
using System.Xml;
using System.Xml.Serialization; // to save state across calls
using System.Collections.Specialized;

namespace Calsense.CommServer
{
    public class TableLookupItem
    {
        public StringBuilder tablename;
        public Func<MemoryStream, StringBuilder, DataTable, DataTable, String, int> f;
        public Int32 filebitnumber;
        public TableLookupItem(StringBuilder s, Int32 bitnumber, Func<MemoryStream, StringBuilder, DataTable, DataTable, String, int> t)
        {
            tablename = s;
            f = t;
            filebitnumber = bitnumber;
        }
    }

    public class MsgProgramDataRequest
    {
        // tables to process, needs to be same order as controller pdata_changes.c->PDATA_build_data_to_send()
        // the builder function will add to given byte array
        static Dictionary<int, TableLookupItem> TablesParseBuild =
            new Dictionary<int, TableLookupItem>
            {
                {0, new TableLookupItem(new StringBuilder("CS3000Networks"),       1, ProgramDataBuilder.Networks)},
                {1, new TableLookupItem(new StringBuilder("CS3000Weather"),        6, ProgramDataBuilder.Weather)},
                {2, new TableLookupItem(new StringBuilder("CS3000Systems"),        10, ProgramDataBuilder.Systems)},
                {3, new TableLookupItem(new StringBuilder("CS3000StationGroups"),  13, ProgramDataBuilder.StationGroups)},
                {4, new TableLookupItem(new StringBuilder("CS3000ManualPrograms"), 12, ProgramDataBuilder.ManualPrograms)},
                {5, new TableLookupItem(new StringBuilder("CS3000Stations"),       9, ProgramDataBuilder.Stations)},
                {6, new TableLookupItem(new StringBuilder("CS3000POC"),            11, ProgramDataBuilder.POC)},
                {7, new TableLookupItem(new StringBuilder("CS3000Lights"),         16, ProgramDataBuilder.Lights)},
                {8, new TableLookupItem(new StringBuilder("CS3000MoistureSensors"),18, ProgramDataBuilder.MoistureSensors)},
            };

        //
        // Parses Program Data Request inbond message from controller
        // returns via Tuple:
        //   List of byte arrays, which are 0 to n messages to send as a response
        //   String that will be passed back in if this connection stays up
        //   Boolean as a flag to terminate connection or not
        //   int as an internal state to track thru session (if needed)
        //
        public static async Task<Tuple<List<byte[]>,String, bool, int>> Parse(ClientInfoStruct client, CancellationTokenSource cts)
        {
            byte[] data;
            MyLogger _logger;
            uint size;
            byte[] Buffer = null;
            AdsCommand cmd, schemacmd;
            bool abort = true;
            DateTime ReceivedTimestamp;
            int State;
            UInt32 crc;
            int i;
            AdsDataReader rdr, schemardr;
            List<byte[]> rsplist = new List<byte[]>();
            UInt16 num_packets;
            Int32? NumUpdatesReady;
            Int32? CurrentUserId;
            MemoryStream MStream = null;
            String internalstatestring = null;
            UInt64 filebits = 0;

            _logger = new MyLogger(typeof(MsgProgramDataRequest).Name, Misc.ClientLogString(client), Misc.ClientLogDB(client), Constants.CS3000Model);
            size = client.rxlen;
            Buffer = client.rx;

            ReceivedTimestamp = DateTime.Now;

            State = client.internalstate;

            // assume empty response
            var mem2 = new MemoryStream(0);
            data = mem2.ToArray();
            mem2.Dispose();

            switch (State)
            {
                case 0:     // first connection message from controller
                    if (client.mid != Constants.MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_init_packet)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "1891", "Expected MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_init_packet but got " + client.mid.ToString());
                        State = Constants.LAST_STATE_COMPLETE;  // error out
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "1804", "Processing Program Data Request message...");
                        ReceivedTimestamp = DateTime.Now;
                        MStream = new MemoryStream();
                    }
                    break;

                case 1:     // there were program data changes to send, we have sent init packet, and this should be the ACK, now send data packet
                    if (client.mid != Constants.MID_TO_COMMSERVER_PROGRAM_DATA_init_ack)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "1891", "Expected MID_TO_COMMSERVER_PROGRAM_DATA_init_ack but got " + client.mid.ToString());
                        State = Constants.LAST_STATE_COMPLETE;  // error out
                    }
                    else
                    {
                        // load our variables up from passed in string

                        // Commented out the following logger line because we think its large string length caused
                        // the logger to miss a few of the logger lines that followed, particulalry the lines in the
                        // memorystreamtopackets function.
                        //_logger.WriteMessage(NLog.LogLevel.Debug, "1822", "Internal string: " + client.sessioninfo.ToString());

                        // get MStream memory stream out of session info string
                        try
                        {
                            MStream = new MemoryStream(Convert.FromBase64String(client.sessioninfo));
                        }
                        catch(Exception e)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "1891", "Exception parsing state data - probably bad controller response: " + e.ToString());
                            State = Constants.LAST_STATE_COMPLETE;  // error out
                        }
                        rsplist = Misc.MemoryStreamToPackets(_logger, MStream, client.sn, client.nid, Constants.MID_FROM_COMMSERVER_PROGRAM_DATA_data_packet);
                    }
                    break;

                case 2:     // controller should be acking our data
                    State = Constants.LAST_STATE_COMPLETE;  // whatever happens, we are done
                    if (client.mid != Constants.MID_TO_COMMSERVER_PROGRAM_DATA_data_full_receipt_ack)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "1892", "Expected MID_TO_COMMSERVER_PROGRAM_DATA_data_full_receipt_ack but got " + client.mid.ToString());                       
                    }
                    else
                    {
                        // make empty struct so termination flag will be set to end connection
                        var mem = new MemoryStream(0);
                        data = mem.ToArray();

                        _logger.WriteMessage(NLog.LogLevel.Info, "1888", "Controller Ack'd Program Data from CommServer.");
                        // call stored procedure to release updated data
                        // EXECUTE PROCEDURE UpdateCS3000ProgramData(:userid,:networkid);
                        // get the user-id we have worked on
                        ServerDB db = new ServerDB(cts);

                        Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
                        AdsConnection conn = await connTask;
                        if(conn != null)
                        {
                            // NOTE: Transactions not supported on local server, so ignore that exception
                            // insert data into DB and retrieve new network id to assign to controller
                            AdsTransaction txn = null;

                            try
                            {
                                txn = conn.BeginTransaction();
                            }
                            catch (System.NotSupportedException)
                            {

                            }
                            do
                            {
                                try
                                {
                                    CurrentUserId = null;
                                    // note that status in temp networks has been changes from 2 to 10 once we started
                                    // working the request, so look for a 10 now, instead of a 2.
                                    cmd = new AdsCommand("SELECT TOP 1 UserID, MIN(CreatedDate) from Temp_CS3000Networks c " +
                                                            " WHERE STATUS = 10 and NetworkID=" + client.nid.ToString() +
                                                            " GROUP BY UserID " +
                                                            " order by MIN(CreatedDate), UserID", conn, txn);
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "1854", "SQL: " + cmd.CommandText);
                                    rdr = cmd.ExecuteReader();
                                    if (rdr.HasRows)
                                    {
                                        rdr.Read();
                                        CurrentUserId = rdr.GetInt32(0);
                                    }
                                    rdr.Close();
                                    cmd.Unprepare();

                                    if (CurrentUserId != null)
                                    {
                                        _logger.WriteMessage(NLog.LogLevel.Info, "1777", "Updating program data after controller ACK for User Id " + CurrentUserId.ToString());
                                        cmd = new AdsCommand("EXECUTE PROCEDURE UpdateCS3000ProgramData(" + CurrentUserId.ToString() +
                                            ", " + client.nid.ToString() + ");", conn, txn);
                                        _logger.WriteMessage(NLog.LogLevel.Debug, "1854", "SQL: " + cmd.CommandText);
                                        cmd.ExecuteNonQuery();


                                        String username = await Misc.getUsername(_logger, cts, (int)CurrentUserId);
                                        // add alert line
                                        await Misc.AddAlert(cts, client.nid, "Program Data sent by " + username, true, true);

                                        abort = false;
                                    }
                                }
                                catch (Exception e)
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Error, "1791", "Database exception during Program Data Request stored procedure ack process " + e.ToString());
                                }
                            } while (false);
                            if (abort == true)
                            {
                                _logger.WriteMessage(NLog.LogLevel.Error, "1791", "Database Error on Program Data Request ack process.");
                                txn.Rollback();
                            }
                            else
                            {
                                txn.Commit();
                            }
                            db.CloseDBConnection(conn, _logger);
                        }
                    }
                    break;
            }

            // check for program data changes in database
            if (State == 0)
            {
                //
                // The following "temp" tables are in DB:
                //  Temp_CS3000Controllers
                //  Temp_CS3000ManualPrograms
                //  Temp_CS3000Networks
                //  Temp_CS3000POC
                //  Temp_CS3000StationGroups
                //  Temp_CS3000Stations
                //  Temp_CS3000Systems
                //  Temp_CS3000Weather
                // 
                // All the temporary tables have 3 more columns than the original tables except the CS3000Networks tables which have 5 more columns.
                //   UserId: integer, the user id.
                //   EventType: integer , 0 no changes , 1 record was updated, 2 new record .
                //   Columns: list for all the columns were changed in this record (comma separated) .
                //   CreatedDate: timestamp, (Only CS3000Networks), the submitted timestamp.
                //   Status: integer (Only CS3000Networks) if it was 2 that mean it’s ready to send the program data to the controller
                //
                _logger.WriteMessage(NLog.LogLevel.Info, "1820", "Checking for Program Data changes in database...");
                ServerDB db = new ServerDB(cts);

                Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
                AdsConnection conn = await connTask;
                if (conn != null)
                {

                    // NOTE: Transactions not supported on local server, so ignore that exception
                    // insert data into DB and retrieve new network id to assign to controller
                    AdsTransaction txn = null;

                    try
                    {
                        txn = conn.BeginTransaction();
                    }
                    catch (System.NotSupportedException)
                    {

                    }
                    do
                    {
                        try
                        {
                            // query for any status 3 situations in temp networks table - meaning data ready to send to controller
                            // there could be more than one row, if more than one user has been active 
                            // we will take the oldest changes first
                            cmd = new AdsCommand("SELECT COUNT(*) from Temp_CS3000Networks c " +
                                                    " WHERE STATUS = 3 and NetworkID=" + client.nid.ToString(), conn, txn);
                            _logger.WriteMessage(NLog.LogLevel.Debug, "1853", "SQL: " + cmd.CommandText);
                            NumUpdatesReady = (Int32?)cmd.ExecuteScalar();
                            cmd.Unprepare();

                            CurrentUserId = null;

                            if (NumUpdatesReady == 0 || NumUpdatesReady == null)
                            {
                                // we will nack, as we have no pending updates
                                State = 10;
                                _logger.WriteMessage(NLog.LogLevel.Info, "1858", "No pending Program Data changes ready.");
                            }
                            else
                            {
                                _logger.WriteMessage(NLog.LogLevel.Info, "1859", "We have pending Program Data changes to send...");

                                // see how many users we have, could potentially be more than one
                                // order we get oldest one
                                // subsequent requests will get newer and newer until done
                                var NumUsers = 0;
                                List<int> myUsers = new List<int>();
                                cmd = new AdsCommand("SELECT TOP 1 UserID, MIN(CreatedDate) from Temp_CS3000Networks c " +
                                                        " WHERE STATUS = 3 and NetworkID=" + client.nid.ToString() + 
                                                        " GROUP BY UserID " +
                                                        " order by MIN(CreatedDate), UserID", conn, txn);
                                _logger.WriteMessage(NLog.LogLevel.Debug, "1854", "SQL: " + cmd.CommandText);
                                rdr = cmd.ExecuteReader();
                                if(rdr.HasRows)
                                {
                                    while(rdr.Read())
                                    {
                                        // another userID to process
                                        NumUsers++;
                                        myUsers.Add(rdr.GetInt32(0));
                                    }
                                }
                                rdr.Close();
                                cmd.Unprepare();
                                // NumUsers should be at most 1 in the current scheme of sending one set of updates at a time
                                while(NumUsers > 0)
                                {
                                    NumUsers--;
                                    CurrentUserId = myUsers[NumUsers];

                                    // now we look at each of the temporary tables, looking for situations where there are updates or new records for this userID
                                    for (i = 0; i < TablesParseBuild.Count; i++ )
                                    {
                                        cmd = new AdsCommand("SELECT * from Temp_" + TablesParseBuild[i].tablename.ToString() + " WHERE EventType in (1,2) AND UserId = " + CurrentUserId.ToString() +
                                                                " AND NetworkID = " + client.nid.ToString(), conn, txn);
                                        _logger.WriteMessage(NLog.LogLevel.Debug, "1854", "SQL: " + cmd.CommandText);
                                        rdr = cmd.ExecuteReader();
                                        if(rdr.HasRows)
                                        {
                                            if(filebits == 0)
                                            {
                                                // set aside the 64-bits for file mask in our memory stream
                                                // now that we know we have at least one change to send
                                                MStream.WriteByte((byte)8);                 // byte holding size of bitmask following
                                                Misc.Uint64ToNetworkBytes(filebits, MStream);
                                                // we will fill this in once all changed tables scanned
                                            }
                                            filebits |= (((UInt64)1) << TablesParseBuild[i].filebitnumber);
                                            // generate a datatable containing contents of this table, and another datatable with schema
                                            {
                                                DataTable tabledata = new DataTable();
                                                DataTable schemadata;

                                                tabledata.Load(rdr);
                                                rdr.Close();

                                                schemacmd = new AdsCommand(cmd.CommandText, conn, txn);
                                                schemardr = schemacmd.ExecuteReader(CommandBehavior.SchemaOnly);
                                                schemadata = schemardr.GetSchemaTable();

                                                schemardr.Close();
                                                schemacmd.Unprepare();

                                                // call Data Builder to construct message components for this table
                                                TablesParseBuild[i].f(MStream, TablesParseBuild[i].tablename, tabledata, schemadata, Misc.ClientLogString(client));
                                            }
                                        }
                                        else
                                        {
                                            rdr.Close();
                                            // put 4 bytes of 0 in place indicating no changes for this table
                                            _logger.WriteMessage(NLog.LogLevel.Debug, "1859", "No changes for Table " + TablesParseBuild[i].tablename.ToString() + " so 0 written.");
                                        }
                                        cmd.Unprepare();
                                    }
                                    if(filebits != 0)
                                    {
                                        // now re-write the file bitmask with the table bits that 
                                        // have changed
                                        MStream.Seek(1, SeekOrigin.Begin);  // skip past size of bitfield byte
                                        Misc.Uint64ToNetworkBytes(filebits, MStream);
                                    }
                                }
                                // change the 3 to a 10 in the temp networks table to indicate we are in process of sending
                                // changes to this controller...
                                if (CurrentUserId != null)
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Info, "1877", "Updating Status from 3 to 10 during Program data send for User Id " + CurrentUserId.ToString());
                                    cmd = new AdsCommand("UPDATE Temp_CS3000Networks SET Status = 10 " +
                                                            " WHERE STATUS = 3 and NetworkID=" + client.nid.ToString() +
                                                            " AND UserID = " + CurrentUserId.ToString(), conn, txn);
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "1854", "SQL: " + cmd.CommandText);
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            // if we made it to here, we are good to commit all our changes
                            abort = false;
                        }
                        catch (Exception e)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "1791", "Database exception during Program Data Request checking: " + e.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    if (abort == true)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "1791", "Database Error on Program Data Request checking.");
                        txn.Rollback();
                    }
                    else
                    {
                        txn.Commit();
                    }
                    db.CloseDBConnection(conn, _logger);
                }

            }
            else
            {
                abort = false;  // no DB activity that could have failed ;)
            }


            // debug to dump out our built bytes
#if DUMPPROGRAMDATA
            if (State == 0)
            {
                if (MStream.Length > 0)
                {
                    byte[] dummy = MStream.ToArray();
                    _logger.WriteMessage(NLog.LogLevel.Info, "3004", "Dump of Program Data bytes:" + dummy.Length.ToString());
                    for (i = 0, j = (int)dummy.Length; i < dummy.Length; i += 8)
                    {
                        if (j >= 8)
                        {
                            s = dummy[i].ToString("x2") + " " + dummy[i + 1].ToString("x2") + " " + dummy[i + 2].ToString("x2") + " " + dummy[i + 3].ToString("x2") + " " + dummy[i + 4].ToString("x2") + " " + dummy[i + 5].ToString("x2") + " " + dummy[i + 6].ToString("x2") + " " + dummy[i + 7].ToString("x2");
                            j -= 8;
                        }
                        else
                        {
                            s = "";
                            while (j > 0)
                            {
                                s = s + dummy[i].ToString("x2") + " ";
                                j--;
                                i++;
                            }
                        }
                        _logger.WriteMessage(NLog.LogLevel.Debug, "3001", s);
                        if (j <= 0) break;
                    }
                }
            }
#endif
            
            // if nothing saved to database, do not ACK
            if (abort == true)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "1801", "Not acking, problem with DB query.");
                
                _logger.WriteMessage(NLog.LogLevel.Info, "14333", "Build Program Data Request Nak response.");

                return (Tuple.Create(Misc.buildNakResponse(client.nid, client.sn, client.mid), (String)null, false, Constants.NO_STATE_DATA));
            }

            // build appropriate ack response
            if ((State == 0 || State == 10) && abort != true)
            {
                if (client.nid != 0)
                {
                    // response holder stuff common to any response we send
                    var mem = new MemoryStream(0);

                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(client.sn, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(client.nid, mem);
                    // mid - either ack or nack
                    if(State == 10)
                    {
                        Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_ACK_NO_CHANGES, mem);
                        _logger.WriteMessage(NLog.LogLevel.Info, "1871", "Build Program Data Request Nak response.");
                        // get the built response to return and be sent
                        data = mem.ToArray();
                        rsplist.Add(data);  // NACK is our response
                        State = Constants.LAST_STATE_COMPLETE;  // and we're done
                    }
                    else 
                    {
                        Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_ACK_CHANGES_AVAILABLE, mem);
                        _logger.WriteMessage(NLog.LogLevel.Info, "1872", "Build Program Data Request Ack response.");
                        // get the built response to return and be sent
                        data = mem.ToArray();
                        rsplist.Add(data);  // Ack is our response, followed by init packet
                        // add an init packet to get the response to controller started
                        var init = new MemoryStream(0);
                        // PID byte
                        init.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                        // message class byte
                        init.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                        // to serial number
                        Misc.Uint32ToNetworkBytes(client.sn, init);
                        // network id
                        Misc.Uint32ToNetworkBytes(client.nid, init);
                        // mid
                        Misc.Uint16ToNetworkBytes((UInt16)(Constants.MID_FROM_COMMSERVER_PROGRAM_DATA_init_packet), init);
                        // expected_size in bytes
                        Misc.Uint32ToNetworkBytes((UInt16)MStream.Length, init);
                        // expected CRC
                        crc = Misc.CRC(MStream.ToArray());
                        _logger.WriteMessage(NLog.LogLevel.Info, "1873", "Computed data msg CRC is 0x" + crc.ToString("X8"));
                        Misc.Uint32ToBigendianBytes(crc, init);
                        // expected_packets
                        num_packets = (UInt16)(MStream.Length / Constants.DESIRED_PACKET_PAYLOAD);
                        if ((MStream.Length % Constants.DESIRED_PACKET_PAYLOAD) != 0) num_packets++;
                        Misc.Uint16ToNetworkBytes((UInt16)num_packets, init);
                        // mid again
                        Misc.Uint16ToNetworkBytes((UInt16)(Constants.MID_FROM_COMMSERVER_PROGRAM_DATA_init_packet), init);
                        _logger.WriteMessage(NLog.LogLevel.Info, "1879", "Init packet for Program Data Request to Controller built.");
                        // get the built response to return and be sent
                        data = init.ToArray();
                        rsplist.Add(data);
                        State++;    // state 1 next time thru
                        // put our MStream into the sessioninfo so we can get to it
                        // next time around
                        internalstatestring = Convert.ToBase64String(MStream.ToArray(), 0, (int)MStream.Length);
                    }

                }
            }
            else if (State == 1 && abort != true)
            {
                // send data packet with program request data, rsplist already set up in first part of this function                
                State++;
            }
            else if (State == Constants.LAST_STATE_COMPLETE || abort == true)
            {
                // some kind of error
                var mem = new MemoryStream(0);
                data = mem.ToArray();
            }

            var terminate = false;
            return (Tuple.Create(rsplist, internalstatestring, terminate, State));
        }
    }
}
