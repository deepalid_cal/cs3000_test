﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using Advantage.Data.Provider;
using System.Configuration;

namespace Calsense.CommServer
{

    public class MsgReports
    {
        // common C code that builds SQL statements for given reports buffer, returns
        // number of bytes parsed in message buffer.
        // sqlBuffer - 1MB
        // SQL_search_condition_str - 4 KB
        // SQL_update_str - 4 KB
        // SQL_insert_fields_str - 4 KB
        // SQL_insert_values_str - 4 KB
        // SQL_single_merge_statement_buf - 8 KB
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint REPORTS_extract_and_store_changes(
            uint pMID,
            byte[] receiveddata,
            uint pdata_len,
            uint network_ID,
            ushort lastReportDate,
            uint lastReportTime,
            [In] byte[] sqlBuffer,
            [In, Out] byte[] pSQL_search_condition_str,
            [In, Out] byte[] pSQL_update_str,
            [In, Out] byte[] pSQL_insert_fields_str,
            [In, Out] byte[] pSQL_insert_values_str,
            [In, Out] byte[] pSQL_single_merge_statement_buf);

        // wrapper function for above DLL call with attribute to allow exception catching
        // if it throws access exception (not normally catchable in C#)
        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptionsAttribute]
        [System.Security.SecurityCriticalAttribute]
        static uint pdata_wrapper(MyLogger _logger, uint pMID, byte[] receiveddata, uint pdata_len,
            uint network_ID, ushort lastReportDate, uint lastReportTime, byte[] sqlBuffer)
        {
            uint i = 0;
            try
            {
                // allocate the buffers that the parsing function uses to build SQL statements internally
                byte[] a1 = new byte[4 * 1024];
                byte[] a2 = new byte[4 * 1024];
                byte[] a3 = new byte[4 * 1024];
                byte[] a4 = new byte[4 * 1024];
                byte[] a5 = new byte[8 * 1024];
                i = REPORTS_extract_and_store_changes(pMID, receiveddata, pdata_len, network_ID, lastReportDate, lastReportTime, sqlBuffer, a1, a2, a3, a4, a5);
            }
            catch (Exception e)
            {
                i = 0;
                _logger.WriteMessage(NLog.LogLevel.Error, "14544",
                    Misc.getReportName(pMID) + " Reports parse exception in REPORTS_extract_and_store_changes : " + e.ToString());
            }
            return (i);
        }

        //
        // Parses Reports inbond message from controller
        // returns via Tuple:
        //   List of byte arrays, which are 0 to n messages to send as a response
        //   String that will be passed back in if this connection stays up
        //   Boolean as a flag to terminate connection or not
        //   int as an internal state to track thru session (if needed)
        //
        public static async Task<Tuple<List<byte[]>, String, bool, int>> Parse(ClientInfoStruct client, CancellationTokenSource cts)
        {
            byte[] data;
            MyLogger _logger;
            uint size;
            uint mid;
            uint nid;
            byte[] Buffer = null;
            byte[] sqlBuffer = new byte[Constants.SQL_BUFFER_SIZE];
            AdsCommand cmd;
            bool abort = true;
            byte response_mid;
            int offset;
            String sqlcommands = "";
            String reportName = Misc.getReportName(client.mid);
            
            bool wantsReportTimestamp= false;
            DateTime lastReportDateTime;
            ushort lastReportDate=0;
            uint lastReportTime=0;
            String lastReportColumnName="";


            _logger = new MyLogger(typeof(MsgReports).Name, Misc.ClientLogString(client), Misc.ClientLogDB(client), Constants.CS3000Model);
            _logger.WriteMessage(NLog.LogLevel.Info, "14304", "Processing " + reportName + " Reports message...");

            size = client.rxlen;
            Buffer = client.rx;
            mid = client.mid;
            nid = client.nid;

            switch (mid)
            {
                case Constants.MID_TO_COMMSERVER_FLOW_RECORDING_init_packet:
                    response_mid = Constants.MID_FROM_COMMSERVER_FLOW_RECORDING_full_receipt_ack;
                    wantsReportTimestamp = true;
                    lastReportColumnName = "LastTimestamp_FlowRec";
                    break;
                case Constants.MID_TO_COMMSERVER_STATION_HISTORY_init_packet:
                    response_mid = Constants.MID_FROM_COMMSERVER_STATION_HISTORY_full_receipt_ack;
                    wantsReportTimestamp = true;
                    lastReportColumnName = "LastTimestamp_StationHistory";
                    break;
                case Constants.MID_TO_COMMSERVER_STATION_REPORT_DATA_init_packet:
                    response_mid = Constants.MID_FROM_COMMSERVER_STATION_REPORT_DATA_full_receipt_ack;
                    wantsReportTimestamp = true;
                    lastReportColumnName = "LastTimestamp_StationReport";
                    break;
                case Constants.MID_TO_COMMSERVER_POC_REPORT_DATA_init_packet:
                    response_mid = Constants.MID_FROM_COMMSERVER_POC_REPORT_DATA_full_receipt_ack;
                    wantsReportTimestamp = true;
                    lastReportColumnName = "LastTimestamp_POCReport";
                    break;
                case Constants.MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_init_packet:
                    response_mid = Constants.MID_FROM_COMMSERVER_SYSTEM_REPORT_DATA_full_receipt_ack;
                    wantsReportTimestamp = true;
                    lastReportColumnName = "LastTimestamp_SystemReport";
                    break;
                case Constants.MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_init_packet:
                    response_mid = Constants.MID_FROM_COMMSERVER_LIGHTS_REPORT_DATA_full_receipt_ack;
                    wantsReportTimestamp = true;
                    lastReportColumnName = "LastTimestamp_LightsReport";
                    break;
                case Constants.MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_init_packet:
                    response_mid = Constants.MID_FROM_COMMSERVER_ET_AND_RAIN_TABLE_full_receipt_ack;
                    break;
                case Constants.MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet:
                    response_mid = Constants.MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_full_receipt_ack;
                    wantsReportTimestamp = true;
                    lastReportColumnName = "LastTimestamp_BudgetReport";
                    break;
                case Constants.MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet2:
                    response_mid = Constants.MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_full_receipt_ack2;
                    wantsReportTimestamp = true;
                    lastReportColumnName = "LastTimestamp_BudgetReport";
                    break;
                case Constants.MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_init_packet:
                    response_mid = Constants.MID_FROM_COMMSERVER_MOISTURE_SENSOR_RECORDER_full_receipt_ack;
                    break;
                default:
                    _logger.WriteMessage(NLog.LogLevel.Error, "14991", "Not acking, unknown report MID: " + mid);
                    _logger.WriteMessage(NLog.LogLevel.Info, "14333", "Build " + reportName + " Report Nak response.");

                    return (Tuple.Create(Misc.buildNakResponse(nid, client.sn, client.mid), (String)null, false, Constants.NO_STATE_DATA));
            }

            if (wantsReportTimestamp)
            {
                DateTime? dt = await Misc.GetReportLastDateTime(_logger, cts, lastReportColumnName, nid);

                if (dt != null)
                {
                    lastReportDateTime = (DateTime)dt;

                    // remember the 2 day excel/VB bug, compensate by subtracting two days
                    lastReportDate = (ushort)(Convert.ToUInt32(lastReportDateTime.Date.ToOADate()) - 2);
                    lastReportTime = (uint)lastReportDateTime.Second;
                    lastReportTime += (uint)lastReportDateTime.Minute * 60;
                    lastReportTime += (uint)lastReportDateTime.Hour * 60 * 60;
                }
                else
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "14336", "Couldn't get the last timestamp for " + reportName);

                    _logger.WriteMessage(NLog.LogLevel.Info, "14336", "Build " + reportName + " Report Nak response.");

                    return (Tuple.Create(Misc.buildNakResponse(nid, client.sn, client.mid), (String)null, false, Constants.NO_STATE_DATA));
                }
            }

            offset = 0;

            // call C language parsing routine to get SQL statements necessary for this message
            //sqlcommands = cfunction(Buffer);
            /*
               - ucp is an unsigned char pointer to the data you received
             * 
                - pnetwork_ID is the network ID from which the message originated, 
             * as indicated in the header.
             * 
             * - mid , report message id
             * 
             * - size, message length
             */
            offset = (int)pdata_wrapper(_logger, mid, Buffer, size, nid, lastReportDate, lastReportTime, sqlBuffer);
            // make sure something got looked at
            if (offset == 0)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "14901", "Not acking, problem with parsing of " + reportName + " reports data.");

                _logger.WriteMessage(NLog.LogLevel.Info, "14333", "Build " + reportName + " Report Nak response.");

                return (Tuple.Create(Misc.buildNakResponse(nid, client.sn, client.mid), (String)null, false, Constants.NO_STATE_DATA));
            }
            sqlcommands = System.Text.Encoding.Default.GetString(sqlBuffer).TrimEnd('\0');  // remove extra nulls
            _logger.WriteMessage(NLog.LogLevel.Info, "14328", "Parse routine looked at " + offset.ToString() + " bytes, data size: " + size);

            // get rid of mysterious non-ascii characters
            if (Regex.Matches(sqlcommands, @"[^\u0000-\u007F]").Count > 0)
            {
                _logger.WriteMessage(NLog.LogLevel.Warn, "14329", "FYI: Replacing non-ASCII characters in SQL with question marks. Probably indicates an error.");
                sqlcommands = Regex.Replace(sqlcommands, @"[^\u0000-\u007F]", "?");
            }
            _logger.WriteMessage(NLog.LogLevel.Debug, "14329", "Resulting SQL is " + sqlcommands.ToString());
#if DUMPRAWBYTES
            _logger.WriteMessage(NLog.LogLevel.Info, "3004", "Dump of bytes raw:" + size.ToString());
            for (i = 0, j = (int)size; i < size; i += 8)
            {
                if (j >= 8)
                {
                    s = Buffer[i].ToString("x2") + " " + Buffer[i + 1].ToString("x2") + " " + Buffer[i + 2].ToString("x2") + " " + Buffer[i + 3].ToString("x2") + " " + Buffer[i + 4].ToString("x2") + " " + Buffer[i + 5].ToString("x2") + " " + Buffer[i + 6].ToString("x2") + " " + Buffer[i + 7].ToString("x2");
                    j -= 8;
                }
                else
                {
                    s = "";
                    while (j > 0)
                    {
                        s = s + Buffer[i].ToString("x2") + " ";
                        j--;
                        i++;
                    }
                }
                _logger.WriteMessage(NLog.LogLevel.Debug, "8001", s);
                if (j <= 0) break;
                Thread.Sleep(10);       // allow UDP logger to catch up
            }
#endif

            if (!String.IsNullOrEmpty(sqlcommands))
            {
                // record in database
                ServerDB db = new ServerDB(cts);

                Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
                AdsConnection conn = await connTask;
                if (conn != null)
                {
                    _logger.WriteMessage(NLog.LogLevel.Info, "14320", "Inserting " + reportName + " report into DB...");
                    // NOTE: Transactions not supported on local server, so ignore that exception
                    // insert data into DB and retrieve new network id to assign to controller
                    AdsTransaction txn = null;

                    try
                    {
                        txn = conn.BeginTransaction();
                    }
                    catch (System.NotSupportedException)
                    {

                    }
                    do
                    {
                        try
                        {
                            cmd = new AdsCommand(sqlcommands, conn, txn);

                            cmd.CommandTimeout = Int32.Parse(ConfigurationManager.AppSettings["SqlLongCommandsTimeouts"]);
                            
                            _logger.WriteMessage(NLog.LogLevel.Debug, "14353", "SQL: " + cmd.CommandText);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();

                            // if we made it to here, we are good to commit all our changes
                            abort = false;
                        }
                        catch (Exception e)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "14391", "Database exception during " + reportName + " report insert(s): " + e.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    if (abort == true)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "14391", "Database Error on " + reportName + " report inserts.");
                        txn.Rollback();
                    }
                    else
                    {
                        txn.Commit();
                    }
                    db.CloseDBConnection(conn, _logger);
                }

                // if nothing saved to database, do not ACK
                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "14901", "Not acking, problem with DB update.");
                    _logger.WriteMessage(NLog.LogLevel.Info, "14333", "Build " + reportName + " Report Nak response.");

                    return (Tuple.Create(Misc.buildNakResponse(nid, client.sn, client.mid), (String)null, false, Constants.NO_STATE_DATA));
                }
            }
            else
            {
                _logger.WriteMessage(NLog.LogLevel.Warn, "14905", "FYI: Parsing routine returned empty string. Probably indicates an error.");
            }

            var mem = new MemoryStream(0);
            // build ack response
            if (client.nid != 0)
            {
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(client.sn, mem);
                // network id
                Misc.Uint32ToNetworkBytes(client.nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)response_mid, mem);
                _logger.WriteMessage(NLog.LogLevel.Info, "14371", "Build " + reportName + " Report Ack response.");
            }

            // get the built response to return and be sent
            data = mem.ToArray();
            List<byte[]> rsplist = new List<byte[]>();
            rsplist.Add(data);
            return (Tuple.Create(rsplist, (String)null, false, Constants.NO_STATE_DATA));
        }
    }
}
