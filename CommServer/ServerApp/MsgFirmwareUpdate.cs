﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using Advantage.Data.Provider;



namespace Calsense.CommServer
{

    public class MsgFirmwareUpdate
    {
        //
        // Parses Firmware Update Message inbond message from controller
        // returns via Tuple:
        //   List of byte arrays, which are 0 to n messages to send as a response
        //   String that will be passed back in if this connection stays up
        //   Boolean as a flag to terminate connection or not
        //   int as an internal state to track thru session (if needed)
        //
        public static async Task<Tuple<List<byte[]>,String,bool,int>> Parse(ClientInfoStruct client, CancellationTokenSource cts)
        {
            byte[] data;
            MyLogger _logger;
            uint size;
            byte[] Buffer = null;
            int offset;
            UInt16 main_date = 0;
            UInt32 main_time = 0;
            UInt16 tp_date;
            UInt32 tp_time;
            DateTime main_dt;
            DateTime tp_dt;
            DateTime file_dt;
            bool terminate_connection = false;
            List<byte[]> rsplist = new List<byte[]>();
            UInt32 filelength = 0;
            UInt32 init_filelength = 0;
            UInt16 num_packets;
            UInt32 filecrc;
            String binfile = null;
            String temp_binfile = null;
            int num_files_remaining;
            bool do_scan = false;
            bool do_ack = false;
            bool do_init = false;
            UInt16 init_mid = 0;


            _logger = new MyLogger(typeof(MsgFirmwareUpdate).Name, Misc.ClientLogString(client), Misc.ClientLogDB(client), Constants.CS3000Model);
            _logger.WriteMessage(NLog.LogLevel.Info, "6004", "Processing Firmware Update message...");
            main_dt = new DateTime();
            tp_dt = new DateTime();
            data = null;
            // get access to runtime control settings via singleton
            WebFormData wf = WebFormData.Instance;

            if(client.sessioninfo != null)
            {
                binfile = client.sessioninfo;   // continue using same file name for processing
            }

            // recover the number of files we have to process on this client session
            num_files_remaining = client.internalstate;

            // what we do next depends on the incoming MID and what we have done so far...
            switch (client.mid)
            {
                case Constants.MID_TO_COMMSERVER_CHECK_FOR_UPDATES_init_packet:
                    size = client.rxlen;
                    Buffer = client.rx;
                    offset = 0;
                    main_date = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                    main_time = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                    tp_date = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                    tp_time = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                    main_dt = Misc.PackedTimeDatetoDateTime(main_time, main_date);
                    tp_dt = Misc.PackedTimeDatetoDateTime(tp_time, tp_date);
                    do_scan = true;
                    do_ack = true;
                    break;

                // generate file to download, identical for main and TP except for MID in data packet headers
                case Constants.MID_CODE_DISTRIBUTION_main_init_ack:
                case Constants.MID_CODE_DISTRIBUTION_tpmicro_init_ack:
                    // crank out the bin file we found
                    if(binfile == null)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "6098", "Unexpected MID for CODE DISTRIBUTION.");
                        rsplist = new List<byte[]>();
                        // terminate process via 
                        num_files_remaining = Constants.LAST_STATE_COMPLETE;
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "6021", "Preparing file for download " + binfile);
                        // for hubs, poll timeout will need to be extended to allow possibly big file to transmit before ack
                        ActiveLegacyConnection ac;
                        ac = HubConnectionTracker.FindACfrom3000NID(client.nid);
                        if(ac != null)
                        {
                            int extendtime = HubConnectionTracker.GetPollTimeout() + (15 * ((binfile.Length) / ServerInternal.MAX_DATA_IN_PACKET));
                            _logger.WriteMessage(NLog.LogLevel.Info, "3040", "Since firmware download via hub, extending poll timeout to " + extendtime.ToString() + " secs.");
                            ac.pollTimeout = extendtime;   // ticks down from here
                        }
                        rsplist = Misc.CrankoutBinFile(_logger, binfile, client.sn, client.nid, (UInt16)(client.mid == Constants.MID_CODE_DISTRIBUTION_main_init_ack ? Constants.MID_CODE_DISTRIBUTION_main_data_packet : Constants.MID_CODE_DISTRIBUTION_tpmicro_data_packet));
                    }
                    break;

                case Constants.MID_CODE_DISTRIBUTION_main_full_receipt_ack:
                    _logger.WriteMessage(NLog.LogLevel.Info, "6022", "Code Distribution main recipt ack from SN:" + client.sn.ToString());
                    rsplist = new List<byte[]>();
                    num_files_remaining = Constants.LAST_STATE_COMPLETE;    // after main firmware sent, we are done
                    // if hub update clear flags
                    
                    if (HubWrapper.HubProcessing3000Yes(client.nid, cts))
                    {
                        ActiveLegacyConnection vac = null;
                        uint vnid;
                        vac = HubConnectionTracker.FindACfrom3000NID(client.nid);
                        vnid = HubWrapper.GetHubID(0, client.nid, cts);
                        if (vnid == client.nid && vac != null)
                        {
                            // we have updated hub main code, clear flag
                            vac.Slave3000MainOutofdate = false;
                        }
                    }
                    break;

                case Constants.MID_CODE_DISTRIBUTION_tpmicro_full_receipt_ack:
                    _logger.WriteMessage(NLog.LogLevel.Info, "6022", "Code Distribution TP recipt ack from SN:" + client.sn.ToString());
                    // we may also need to continue on and send the main firmware if it is newer too...
                    if(num_files_remaining > 1)
                    {
                        num_files_remaining--;
                        // since we already have sent more than 1 file, we know we need to 
                        // now move on to app file. reload file parameters, either from forced location, or "newest" file
                        // check database for any ForceFirmare settings on this unit.
                        Task<Tuple<String, DateTime?, UInt32>> gm2 = Misc.GetMainboardForcedFilename(_logger, cts, client.nid);
                        Tuple<String, DateTime?, UInt32> r3 = await gm2;
                        // r3.Item1 = filename, r3.Item2 = binary timestamp, r3.Item3 = filesize
                        if (!String.IsNullOrEmpty(r3.Item1) && r3.Item3 > 0)
                        {
                            init_filelength = r3.Item3;
                            file_dt = (DateTime)r3.Item2;
                            binfile = ConfigurationManager.AppSettings["AppFirmwareFolder"] + "\\" + ConfigurationManager.AppSettings["PreReleaseSubfolder"] + "\\" + r3.Item1;
                        }
                        else 
                        { 
                            binfile = ConfigurationManager.AppSettings["AppFirmwareFolder"] + "\\" + Misc.FindNewestBinFile(_logger, ConfigurationManager.AppSettings["AppFirmwareFolder"], Constants.MAIN_APP_BUILD_DT_OFFSET, out file_dt, out init_filelength);
                        }
                        do_init = true;
                        // do an init for the main App file now, using this mid
                        init_mid = Constants.MID_CODE_DISTRIBUTION_main_init_packet;
                    }
                    else
                    {
                        rsplist = new List<byte[]>();
                        num_files_remaining = Constants.LAST_STATE_COMPLETE;
                    }
                    if (HubWrapper.HubProcessing3000Yes(client.nid, cts))
                    {
                        ActiveLegacyConnection vac = null;
                        uint vnid;
                        vac = HubConnectionTracker.FindACfrom3000NID(client.nid);
                        vnid = HubWrapper.GetHubID(0, client.nid, cts);
                        if (vnid == client.nid && vac != null)
                        {
                            // we have updated hub TP code, clear flag
                            vac.Slave3000TPOutofdate = false;
                        }
                    }
                    break;

                default:
                    // unexpected message
                    _logger.WriteMessage(NLog.LogLevel.Error, "6090", "Unknown MID in Firmware Update code: " + client.mid.ToString());
                    rsplist = new List<byte[]>();
                    num_files_remaining = Constants.LAST_STATE_COMPLETE;
                    break;
            }

            // if checking for updates, get most recent .bin file
            // also come back into this logic for 2nd pass if multiple firmwares being updated
            if (do_scan)
            {
                // for a hub connected 3000, check against *hub* not against the particular slave
                // for a hub connected 3000, check against *hub* not against the particular slave
                uint check_nid;
                bool hub = false;
                ActiveLegacyConnection ac = null;
                if (HubWrapper.HubProcessing3000Yes(client.nid, cts))
                {
                    ac = HubConnectionTracker.FindACfrom3000NID(client.nid);
                    check_nid = HubWrapper.GetHubID(0, client.nid, cts);
                    if (check_nid != client.nid)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "6090", "Since hub connected, will check for forced firmware on *hub* " + check_nid.ToString());
                        hub = true;
                    }
                }
                else
                {
                    check_nid = client.nid;
                }

                if (ac != null && check_nid == client.nid && ac.Slave3000MainOutofdate)
                {
                    // this is a 3000 hub that has slave devices needing update
                    _logger.WriteMessage(NLog.LogLevel.Info, "6080", "Slave 3000 on this hub needs main update.");
                }
                // check database for any ForceFirmare settings on this unit.
                Task<Tuple<String, DateTime?, UInt32>> gm = Misc.GetMainboardForcedFilename(_logger, cts, check_nid);
                Tuple<String, DateTime?, UInt32> r1 = await gm;
                // r1.Item1 = filename, r1.Item2 = binary timestamp, r1.Item3 = filesize

                num_files_remaining = 0;

                // check for newer App firmware 
                temp_binfile = Misc.FindNewestBinFile(_logger, ConfigurationManager.AppSettings["AppFirmwareFolder"], Constants.MAIN_APP_BUILD_DT_OFFSET, out file_dt, out filelength);
                // override via DB to exact file
                if(!String.IsNullOrEmpty(r1.Item1) && r1.Item3 > 0)
                {
                    if (main_dt != r1.Item2 || (ac != null && check_nid == client.nid && ac.Slave3000MainOutofdate))
                    {
                        num_files_remaining++;
                        do_init = true;
                        init_filelength = r1.Item3;
                        init_mid = Constants.MID_CODE_DISTRIBUTION_main_init_packet;
                        binfile = ConfigurationManager.AppSettings["AppFirmwareFolder"] + "\\" + ConfigurationManager.AppSettings["PreReleaseSubfolder"] + "\\" + r1.Item1;
                        _logger.WriteMessage(NLog.LogLevel.Warn, "6080", "Forced App bin file per DB is " + r1.Item1.ToString());
                    }
                    else
                    {
                        // controller already running binary matching database file
                        _logger.WriteMessage(NLog.LogLevel.Info, "6080", "Forced App bin file already in place.");
                    }
                }
                // compare this .bin timestamp with controller info to see if update required...
                else if (temp_binfile != null)
                {
                    // turn into a full path for use from here on out
                    temp_binfile = ConfigurationManager.AppSettings["AppFirmwareFolder"] + "\\" + temp_binfile;
                    if ((file_dt > main_dt) || wf.ForceFirmwareUpdate || (ac != null && check_nid == client.nid && ac.Slave3000MainOutofdate))
                    {
                        binfile = temp_binfile;
                        if(file_dt <= main_dt)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Warn, "6080", "Forcing App update to newest file on hand which is: " + binfile);
                        }
                        else
                        {
                            _logger.WriteMessage(NLog.LogLevel.Info, "6080", "NEWER App bin file found: " + binfile);
                        }
                        num_files_remaining++;
                        do_init = true;
                        init_filelength = filelength;
                        init_mid = Constants.MID_CODE_DISTRIBUTION_main_init_packet;
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "6081", "Controller already running App code same/newer than " + temp_binfile);

                    }
                }
                else
                {
                    _logger.WriteMessage(NLog.LogLevel.Warn, "6077", "No valid App .bin files found in " + ConfigurationManager.AppSettings["AppFirmwareFolder"]);

                }


                if (ac != null && check_nid == client.nid && ac.Slave3000TPOutofdate)
                {
                    // this is a 3000 hub that has slave devices needing update
                    _logger.WriteMessage(NLog.LogLevel.Info, "6080", "Slave 3000 on this hub needs TP update.");
                }
                // check database for any ForceFirmare settings on this unit.
                Task<Tuple<String, DateTime?, UInt32>> gtp = Misc.GetTPMicroForcedFilename(_logger, cts, check_nid);
                Tuple<String, DateTime?, UInt32> r2 = await gtp;
                // r2.Item1 = filename, r2.Item2 = binary timestamp, r2.Item3 = filesize

                // check for newer TP firmware 
                temp_binfile = Misc.FindNewestBinFile(_logger, ConfigurationManager.AppSettings["TPFirmwareFolder"], Constants.TPMICRO_BUILD_DT_OFFSET, out file_dt, out filelength);
                // compare this .bin timestamp with controller info to see if update required...
                if (!String.IsNullOrEmpty(r2.Item1) && r2.Item3 > 0)
                {
                    if (tp_dt != r2.Item2 || (ac != null && check_nid == client.nid && ac.Slave3000TPOutofdate))
                    {
                        num_files_remaining++;
                        do_init = true;
                        init_filelength = r2.Item3;
                        init_mid = Constants.MID_CODE_DISTRIBUTION_tpmicro_init_packet;
                        binfile = ConfigurationManager.AppSettings["TPFirmwareFolder"] + "\\" + ConfigurationManager.AppSettings["PreReleaseSubfolder"] + "\\" + r2.Item1;
                        // note: if we also have a main app to update, it will happen 2nd.
                        _logger.WriteMessage(NLog.LogLevel.Warn, "6080", "Forced TP bin file per DB is " + r2.Item1.ToString());
 
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "6080", "Forced TP bin file already in place.");
                    }
                }
                else if (temp_binfile != null)
                {
                    // turn into a full path for use from here on out
                    temp_binfile = ConfigurationManager.AppSettings["TPFirmwareFolder"] + "\\" + temp_binfile;
                    if ((file_dt > tp_dt) || wf.ForceTPUpdate || (ac != null && check_nid == client.nid && ac.Slave3000TPOutofdate))
                    {
                        binfile = temp_binfile;
                        if (file_dt <= tp_dt)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Warn, "6080", "Forcing update of TP bin to latest on hand: " + temp_binfile);
                        }
                        else
                        {
                            _logger.WriteMessage(NLog.LogLevel.Info, "6080", "NEWER TP bin file found: " + temp_binfile);
                        }
                        num_files_remaining++;
                        do_init = true;
                        init_filelength = filelength;
                        init_mid = Constants.MID_CODE_DISTRIBUTION_tpmicro_init_packet;
                        // note: if we also have a main app to update, it will happen 2nd.
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "6081", "Controller already running TP code same/newer than " + temp_binfile);

                    }
                }
                else
                {
                    _logger.WriteMessage(NLog.LogLevel.Warn, "6077", "No valid TP .bin files found in " + ConfigurationManager.AppSettings["TPFirmwareFolder"]);

                }
                if(hub)
                {
                    // store the knowledge of what is out of date
                    if(num_files_remaining > 0)
                    {
                        if(ac != null)
                        {
                            if(init_mid == Constants.MID_CODE_DISTRIBUTION_tpmicro_init_packet)
                            {
                                ac.Slave3000TPOutofdate = true;
                            }
                            if(init_mid == Constants.MID_CODE_DISTRIBUTION_main_init_packet || num_files_remaining > 1)
                            {
                                ac.Slave3000MainOutofdate = true;
                            }
                            HubConnectionTracker.TellHubToCheckFirmware(ac, check_nid, cts);    // queue up a check firmware for this slaves hub
                        }
                    }
                }
            }

            if (do_ack)
            {
                if (num_files_remaining > 0 && num_files_remaining != Constants.LAST_STATE_COMPLETE)
                {
                    // send the initial ack response if this is a check for updates pass
                    var mem = new MemoryStream(0);
                    // build ack response
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(client.sn, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(client.nid, mem);
                    // mid
                    if (num_files_remaining > 1)
                    {
                        Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_MULTIPLE_UPDATES_AVAILABLE, mem);
                    }
                    else
                    {
                        Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_UPDATE_AVAILABLE, mem);
                    }
                    _logger.WriteMessage(NLog.LogLevel.Info, "6071", "Build Firmware Update(s) Available ACK response. Number of updates available " + num_files_remaining.ToString());
                    data = mem.ToArray();
                    rsplist.Add(data);
                }
                else
                {
                    var mem = new MemoryStream(0);
                    // build ack response
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(client.sn, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(client.nid, mem);
                    // mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_NO_UPDATES, mem);
                    _logger.WriteMessage(NLog.LogLevel.Info, "6071", "Build Firmware Update No Updates Ack response.");

                    // get the built response to return and be sent
                    data = mem.ToArray();
                    rsplist.Add(data);
                    num_files_remaining = Constants.LAST_STATE_COMPLETE;
                }
            }

            if(do_init)
            {
                // build the appropriate init packet
                var init = new MemoryStream(0);
                // PID byte
                init.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                init.WriteByte(Constants.MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION);
                // to serial number
                Misc.Uint32ToNetworkBytes(client.sn, init);
                // network id
                Misc.Uint32ToNetworkBytes(client.nid, init);
                // mid
                Misc.Uint16ToNetworkBytes(init_mid, init);
                _logger.WriteMessage(NLog.LogLevel.Info, "6079", "Init packet for file: " + binfile.ToString());
                // expected_size
                _logger.WriteMessage(NLog.LogLevel.Info, "6073", "Length of file is " + init_filelength.ToString());
                Misc.Uint32ToNetworkBytes(init_filelength, init);
                // file will either be TP Micro if it is newer, or App if it is newer and no TP Micro file to do first.
                // expected CRC
                filecrc = Misc.FileCRC(binfile);
                _logger.WriteMessage(NLog.LogLevel.Info, "6072", "Computed File CRC is 0x" + filecrc.ToString("X8"));
                Misc.Uint32ToBigendianBytes(filecrc, init);
                // expected_packets
                num_packets = (UInt16)(init_filelength / Constants.DESIRED_PACKET_PAYLOAD);
                if ((init_filelength % Constants.DESIRED_PACKET_PAYLOAD) != 0) num_packets++;
                _logger.WriteMessage(NLog.LogLevel.Info, "6073", "Number of packets is " + num_packets.ToString());
                Misc.Uint16ToNetworkBytes(num_packets, init);
                // mid again
                Misc.Uint16ToNetworkBytes(init_mid, init);
                _logger.WriteMessage(NLog.LogLevel.Info, "6074", "Init MID used is " + init_mid.ToString());
                // 2/4/2014 rmd : Any needed message specific information can follow this STRUCT in the
                // intialization message content as an additional secondary structure. It's presence or not,
                // and content, if any, are by definition as per the mid.
                _logger.WriteMessage(NLog.LogLevel.Info, "6072", "Build Firmware Code Distribution init response.");

                // get the built response to return and be sent
                data = init.ToArray();
                rsplist.Add(data);

            }

            return (Tuple.Create(rsplist, binfile, terminate_connection, num_files_remaining));
        }
    }
}
