//
// SocketTracker - manages table to track all active sockets/controller pairs. 
// (Static class, as only one copy ever exists in the CommServer)
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Calsense.Logging;
using ServerApp;
using System.Collections.Concurrent;
using System.Configuration;
using Advantage.Data.Provider;
using System.IO;

namespace Calsense.CommServer
{
    // 
    // store a ServerSocketInstance for each active Controller we are talking to
    class ActiveController
    {
        public ActiveController(UInt32 nid, UInt32 sn, NetworkStream ns, Socket s, CancellationTokenSource _cts)
        {
            network_id = nid;
            idle = false;
            net_stream = ns;
            serial_number = sn;
            Status10Seen = 0;       // looking for a persistent 10 in the Temp networks table
                                    // indicating controller never ack'd the program data changes
            sock = s;               // we keep a socket copy so we can close a 3000 becoming a hub in those special cases
            cts = _cts;
        }
        public NetworkStream net_stream { set; get;}
        public UInt32 network_id { set; get; }
        public Boolean idle {set; get;}
        public UInt32 serial_number { set; get; }
        public UInt32 Status10Seen { set; get; }   // number of seconds status 3,10 has persisted
        public CancellationTokenSource cts { set; get; }
        public Socket sock { set; get; }
    }
    //
    // class that handles populating and manipulating lookup table containing ServerSocketInstances
    static class ServerSocketTracker
    {
        // constructor
        static ServerSocketTracker() 
        { 
            TrackingTable = new ConcurrentDictionary<uint, ActiveController>();
            _log = new MyLogger("SocketTracker", "", "", Constants.CS3000Model);
            _cts = null;    // this will get set at runtime
            _pollrequest_cts = null;
            _combo_cts = null;
            // start a task to periodically scan for something to send based
            // on database flags getting set for a controller...
            Task.Factory.StartNew(PollLoop, TaskCreationOptions.LongRunning);
        }

        // properties
        private static ConcurrentDictionary<uint, ActiveController> TrackingTable;
        private static MyLogger _log;
        private static CancellationTokenSource _cts;
        private static CancellationTokenSource _pollrequest_cts;
        private static CancellationTokenSource _combo_cts;
        private static int check_network_id;

        //
        // run as a task
        async private static void PollLoop()
        {
            int delay;
            AdsCommand cmd;
            Int32? NumUpdatesReady;
            Int32? NumStatus10Seen;
            ActiveController ac;
            uint nid;
            byte[] data;
            UInt32 crc;
            check_network_id = 0;
            List<int> nidsWithPdata;

            while (_cts == null)
            {
                await Task.Delay(50);  // wait for somebody to tell us the cancellation token
            }

            _pollrequest_cts = new CancellationTokenSource();
            _combo_cts = CancellationTokenSource.CreateLinkedTokenSource(_pollrequest_cts.Token, _cts.Token);

            await Task.Delay(1500);       // let everything spin up...

            while (true)
            {
                check_network_id = 0; // set to zero so next loop will check all the networks in the tracking table, unless web signal sets it
                
                DateTime beginstamp = DateTime.Now;
                delay = Convert.ToInt32(ConfigurationManager.AppSettings["PollSeconds"]);

                try
                {
                    // either a QUIT signal, or a CHECK program data NOW signal will get us out of delay early
                    await Task.Delay(Convert.ToInt32(delay * 1001), _combo_cts.Token);
                }
                catch (TaskCanceledException ex)
                {
                    if (_cts.IsCancellationRequested)
                    {
                        _log.WriteMessage(NLog.LogLevel.Debug, "190", "Exiting Tracking Table task " + ex.ToString());
                    }
                    else
                    {
                        _log.WriteMessage(NLog.LogLevel.Debug, "190", "Web signal to check program data now for nid: " + check_network_id);
                        // recreate the tokens needed to look next time
                        _pollrequest_cts.Dispose();
                        _combo_cts.Dispose();
                        _pollrequest_cts = new CancellationTokenSource();
                        _combo_cts = CancellationTokenSource.CreateLinkedTokenSource(_pollrequest_cts.Token, _cts.Token);
                    }
                }

                if (_cts.IsCancellationRequested) break; // program is exiting

                DateTime endstamp = DateTime.Now;
                TimeSpan extratime = endstamp - beginstamp;
                Int32 delayseconds = Convert.ToInt32(extratime.TotalSeconds);

                if (TrackingTable.Count > 0 || HubConnectionTracker.HubTrackingTable.Count > 0)
                {
                    // scan for any database requests
                    _log.WriteMessage(NLog.LogLevel.Debug, "15", "Checking Database for any Updates to send unsolicited to Controller(s)...");

                    // check all the controllers in db with status equals to 2 or 10
                    nidsWithPdata = await Misc.ControllersHasPDtoSend(_log, _cts);

                    _log.WriteMessage(NLog.LogLevel.Debug, "17", nidsWithPdata.Count() + " controller(s) have pdata to be sent.");

                    if (nidsWithPdata.Count() > 0)
                    {
                        ServerDB db = new ServerDB(_cts);

                        Task<AdsConnection> connTask = db.OpenDBConnection(_log);
                        AdsConnection conn = await connTask;

                        if (conn != null)
                        {

                            // loop thru all controllers in the normal and hub tracking table
                            List<uint> netids = new List<uint>();
                            foreach (var item in TrackingTable)
                            {
                                netids.Add(item.Key);
                            }
                            foreach (var item in HubConnectionTracker.HubTrackingTable)
                            {
                                foreach (var cont in item.Value.controllers3000)
                                {
                                    netids.Add(cont.NID);
                                }
                            }
                            foreach (var item in netids)
                            {
                                // for each controller, query the database for a) pending program data
                                NumUpdatesReady = 0;
                                NumStatus10Seen = null;
                                nid = item;

                                // only check the nid we have them in nidsWithPdata list
                                if (nidsWithPdata.Count(v => v == nid) > 0)
                                {

                                    _log.WriteMessage(NLog.LogLevel.Debug, "19", "Network ID: " + nid.ToString());

                                    do
                                    {
                                        try
                                        {
                                            if (check_network_id == nid)
                                            {
                                                // we have signal to CHECK program data NOW for particular network id
                                                cmd = new AdsCommand("SELECT COUNT(*) from Temp_CS3000Networks c " +
                                                                        " WHERE STATUS = 2 and NetworkID=" + nid.ToString(), conn);
                                                _log.WriteMessage(NLog.LogLevel.Debug, "24", "SQL: " + cmd.CommandText);
                                                NumUpdatesReady = (Int32?)cmd.ExecuteScalar();
                                                cmd.Unprepare();
                                            }
                                            else if (check_network_id == 0)
                                            {
                                                // normal program data scanning, look for status 2, 3 or 10
                                                cmd = new AdsCommand("SELECT COUNT(*) from Temp_CS3000Networks c " +
                                                                       " WHERE STATUS = 2 and NetworkID=" + nid.ToString(), conn);

                                                _log.WriteMessage(NLog.LogLevel.Debug, "24", "SQL: " + cmd.CommandText);
                                                NumUpdatesReady = (Int32?)cmd.ExecuteScalar();
                                                cmd.Unprepare();


                                                cmd = new AdsCommand("SELECT COUNT(*) from Temp_CS3000Networks c " +
                                                                        " WHERE STATUS in (3,10) and NetworkID=" + nid.ToString(), conn);
                                                _log.WriteMessage(NLog.LogLevel.Debug, "29", "SQL: " + cmd.CommandText);
                                                NumStatus10Seen = (Int32?)cmd.ExecuteScalar();
                                                cmd.Unprepare();
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            _log.WriteMessage(NLog.LogLevel.Error, "23", "Database exception during Tracking Table checking: " + e.ToString());
                                        }
                                    } while (false);    // allows us to break out early if SQL problem

                                    Boolean hub = false;

                                    if (HubConnectionTracker.FindACfrom3000NID(nid) != null)
                                    {
                                        hub = true;
                                    }

                                    // if any status 10's seen, flag it, otherwise clear it
                                    if (NumStatus10Seen == 0 && NumStatus10Seen != null)
                                    {
                                        if (TrackingTable.TryGetValue(nid, out ac))
                                        {
                                            ac.Status10Seen = 0;    // indicate no status 10 seen for this network ID in its Temp Network table
                                        }
                                        else
                                        {
                                            if (hub)
                                            {
                                                HubConnectionTracker.HubSetStatus10(nid, 0);
                                            }
                                        }
                                    }
                                    if (NumStatus10Seen > 0 && NumStatus10Seen != null)
                                    {
                                        if (hub)
                                        {
                                            // for a hub, program data can take longer than non-hub
                                            // so we look for a status 10 lasting more than X seconds
                                            if (HubConnectionTracker.HubGetStatus10(nid) > 300)
                                            {
                                                // if status 10 on a hub has persisted for too long, try again
                                                try
                                                {
                                                    _log.WriteMessage(NLog.LogLevel.Warn, "37", "A Status 10 has persisted for too long in Temp Networks table for network ID (indicates no ACK from controller to Program Data) " + nid.ToString() + " it will now be reset to 2 so Program Data can be sent again.");
                                                    cmd = new AdsCommand("UPDATE Temp_CS3000Networks SET Status = 2 " +
                                                                            " WHERE STATUS IN (3,10) and NetworkID=" + nid.ToString(), conn);
                                                    _log.WriteMessage(NLog.LogLevel.Debug, "38", "SQL: " + cmd.CommandText);
                                                    cmd.ExecuteNonQuery();

                                                    HubConnectionTracker.HubSetStatus10(nid, 0);  // knock it down to zero to start over
                                                }
                                                catch (Exception e)
                                                {
                                                    _log.WriteMessage(NLog.LogLevel.Error, "39", "Database exception during Tracking Table reset of Temp Networks status from 10 to 2: " + e.ToString());

                                                }
                                            }
                                            else
                                            {
                                                // next time seeing a status 10 on this controller, indicate such by bumping counter our delay number of seconds
                                                HubConnectionTracker.HubSetStatus10(nid, HubConnectionTracker.HubGetStatus10(nid) + delayseconds);  
                                            }
                                        }
                                        else if (TrackingTable.TryGetValue(nid, out ac))
                                        {
                                            if (ac.Status10Seen > 60)
                                            {
                                                // this is 60 seconds we have seen a status 3,10 
                                                // elapsed without controller ack'd changes we have sent. 
                                                // knock status back down to a 2
                                                // so changes can be sent again.
                                                try
                                                {
                                                    _log.WriteMessage(NLog.LogLevel.Warn, "37", "A Status 3,10 has persisted for too long in Temp Networks table for network ID (indicates no ACK from controller to Program Data) " + nid.ToString() + " it will now be reset to 2 so Program Data can be sent again.");
                                                    cmd = new AdsCommand("UPDATE Temp_CS3000Networks SET Status = 2 " +
                                                                            " WHERE STATUS in (3,10) and NetworkID=" + nid.ToString(), conn);
                                                    _log.WriteMessage(NLog.LogLevel.Debug, "38", "SQL: " + cmd.CommandText);
                                                    cmd.ExecuteNonQuery();

                                                    ac.Status10Seen = 0;    // knock it down
                                                }
                                                catch (Exception e)
                                                {
                                                    _log.WriteMessage(NLog.LogLevel.Error, "39", "Database exception during Tracking Table reset of Temp Networks status from 10 to 2: " + e.ToString());

                                                }
                                            }
                                            else
                                            {
                                                // first time seeing a status 10 on this controller, indicate such
                                                ac.Status10Seen += (uint)delayseconds;    // increment by number of seconds delayed
                                            }
                                        }
                                    }


                                    // for any pending data found, send a IHSFY message out to the controller via its connected socket
                                    if (NumUpdatesReady != 0 && NumUpdatesReady != null)
                                    {
                                        // update 2 to 3 indicating we are sending out IHSFY message, and now will no longer be
                                        // be picked up as a 2 in this loop.
                                        try
                                        {
                                            _log.WriteMessage(NLog.LogLevel.Info, "37", "Updating status from 2 to 3 indicating IHSFY about to go out for " + nid.ToString());
                                            cmd = new AdsCommand("UPDATE Temp_CS3000Networks SET Status = 3 " +
                                                                    " WHERE STATUS = 2 and NetworkID=" + nid.ToString(), conn);
                                            _log.WriteMessage(NLog.LogLevel.Debug, "38", "SQL: " + cmd.CommandText);
                                            cmd.ExecuteNonQuery();
                                        }
                                        catch (Exception e)
                                        {
                                            _log.WriteMessage(NLog.LogLevel.Error, "39", "Database exception during Tracking Table update of Temp Networks status from 2 to 3: " + e.ToString());

                                        }

                                        if (hub)
                                        {
                                            _log.WriteMessage(NLog.LogLevel.Info, "23", "New Temp data for hub based 3000 controller, queueing up IHSFY message. NID: " + nid.ToString());
                                            HubConnectionTracker.QueueUpIHSFYHub(nid);
                                        }
                                        else if (TrackingTable.TryGetValue(nid, out ac))
                                        {
                                            var mem = new MemoryStream(0);
                                            // PID byte
                                            mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                                            // message class byte
                                            mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                                            // to serial number
                                            Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                                            // network id
                                            Misc.Uint32ToNetworkBytes(nid, mem);
                                            // I Have Something For You, namely Program Data
                                            Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_IHSFY_program_data, mem);

                                            data = mem.ToArray();

                                            crc = Misc.CRC(data);
                                            //_log.WriteMessage(NLog.LogLevel.Debug, "40", "Calculated CRC: 0x" + crc.ToString("X"));
                                            _log.WriteMessage(NLog.LogLevel.Debug, "28", "IHSFY to be transmitted to SN:" + ac.serial_number.ToString());

                                            // combine preamble, data, CRC and then postamble
                                            byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                                            System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                                            System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                                            byte[] crcbytes;
                                            crcbytes = System.BitConverter.GetBytes(crc);
                                            if (BitConverter.IsLittleEndian)
                                            {
                                                //_logger.WriteMessage(NLog.LogLevel.Debug, "3029", "FYI: CRC Bytes swapped to send ");
                                                Array.Reverse(crcbytes, 0, crcbytes.Length);
                                            }
                                            System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                                            System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                                            try
                                            {
                                                await ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length).ConfigureAwait(false);
                                                await ac.net_stream.FlushAsync().ConfigureAwait(false);

                                                // update stats tracking table
                                                CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                                                CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                                            }
                                            catch (Exception e)
                                            {
                                                _log.WriteMessage(NLog.LogLevel.Error, "75", "Exception during transmit of IHSFY to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                                                // remove this entry from socket table so we don't keep trying
                                                RemoveTrackingEntry(nid, ac.net_stream);
                                            }

                                            mem.Dispose();
                                        }
                                        else
                                        {
                                            _log.WriteMessage(NLog.LogLevel.Error, "26", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table!");
                                        }
                                    }
                                }
                            }
                            db.CloseDBConnection(conn, _log);
                        }
                    }
                    else
                    {
                        // 05/11/2017 rmd : Commented out to manage number of logger lines.
                        //_log.WriteMessage(NLog.LogLevel.Debug, "97", "No controllers in the database has program data to be sent, skipping IHSFY database check.");
                    }
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Debug, "99", "No controllers in tracking table, skipping IHSFY database check.");
                }
            }
        }

        public static void CheckJobsNow(int nid)
        {
            check_network_id = nid; // the network id we want check now

            if (_pollrequest_cts != null)
            {
                _pollrequest_cts.Cancel();
            }
            return;
        }

        public static void SetCts(CancellationTokenSource c)
        {
            _cts = c;
        }

        public static void SetBusy(uint nid)
        {
            ActiveController ac;

            ac = GetTrackingEntry(nid);
            if(ac != null)
            {
                ac.idle = false;
            }
        }
        public static void ClearBusy(uint nid)
        {
            ActiveController ac;

            ac = GetTrackingEntry(nid);
            if (ac != null)
            {
                ac.idle = true;
            }
        }


        public static void RemoveTrackingEntry(uint nid, NetworkStream ns)
        {
            ActiveController ac;

            //
            // for a hub environment, handle it there
            //
            if (HubWrapper.HubProcessing3000Yes(nid, _cts))
            {
                // this is hub processing
                HubWrapper.HubTrackingRemoval(nid);
                return;
            }

            if (TrackingTable.TryGetValue(nid, out ac))
            {
                // ensure this is the correct match, as there are situations where a removal call is
                // made for a stale already removed tracking entry, and we don't want to remove
                // the current good one.
                if(ac.net_stream != ns)
                {
                    // ignore this call
                    _log.WriteMessage(NLog.LogLevel.Warn, "87", "Ignoring call to remove stale tracking table entry on NID " + nid.ToString());
                }
                else if (!TryRemove(nid, out ac))
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "88", "Unable to find Network ID " + nid.ToString() + " in tracking table while removing it?");
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Info, "11", "Removed Network ID " + nid.ToString() + " from tracking table.");
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "8", "Unable to find Network ID " + nid.ToString() + " in tracking table while removing it?");
            }
        }
        
        public static bool TryRemove(uint nid, out ActiveController ac) 
        {
            ActiveController _ac;

            TrackingTable.TryGetValue(nid, out _ac); 
            
            if (_ac != null)
            {
                if (!_ac.cts.IsCancellationRequested)
                {
                    try
                    {
                        _ac.cts.Cancel();
                    }
                    catch (OperationCanceledException)
                    {
                        _log.WriteMessage(NLog.LogLevel.Info, "14", "Socket was successfully canceled for Network ID " + nid.ToString() + " from tracking table.");
                    }
                    catch (Exception)
                    {
                        _log.WriteMessage(NLog.LogLevel.Warn, "9", "Unable to cancel socket for Network ID " + nid.ToString() + " in tracking table while removing it?");
                    }
                }
            }

            return TrackingTable.TryRemove(nid, out ac);
        } 

        public static void AddTrackingEntry(uint nid, uint sn, NetworkStream ns, Socket s, CancellationTokenSource _cts)
        {
            ActiveController ac;
            // if we already have a connection for this serial number, knock it down
            if (TrackingTable.TryGetValue(nid, out ac))
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "9", "Socket for Network ID " + nid.ToString() + " already exists! Using new connection for any unsolicited outbound messages to this network ID.");
                RemoveTrackingEntry(nid, ac.net_stream);
            }
            // should be able to add this guy now
            TrackingTable.TryAdd(nid, new ActiveController(nid, sn, ns, s, _cts));
            _log.WriteMessage(NLog.LogLevel.Info, "10", "Added network ID " + nid.ToString() + " to tracking table.");
        }

        //
        // called on each subsequence network data, verify nid hasn't changed
        //
        public static int DoubleCheckNid(uint nid, uint sn, NetworkStream ns, MyLogger _logger)
        {
            ActiveController ac;

            // if we already have a connection for this serial number, knock it down
            if (!TrackingTable.TryGetValue(nid, out ac))
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "90", "Tracking table entry for Network ID " + nid.ToString() + " disappeared while socket active?!");
                return(-1);
            }
            else
            {
                if(ac.net_stream != ns)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "91", "NID changed on existing socket?? Latest NID: " + nid.ToString() + " original NID: " + ac.network_id.ToString());
                    RemoveTrackingEntry(ac.network_id, ac.net_stream); // remove conflicting entry
                    return(-1);
                }
            }
            return(0);
        }

        //
        // return he current ActiveController entry for a given NID
        // if not connected/present will return null
        //
        // for a hub based controller, returns a pseudo ActiveController
        // that can be used to write data out to the controller
        //
        public static ActiveController GetTrackingEntry(uint nid)
        {
            ActiveController ac;
            if (HubWrapper.HubProcessing3000Yes(nid, _cts))
            {
                // this is hub processing
                return (HubWrapper.GetPseudoActiveController(nid));
            }
            // normal processing
            if (!TrackingTable.TryGetValue(nid, out ac))
            {
                return (null);
            }
            return (ac);
        }


        //
        // return he current ActiveController entry for a given NID
        // if not connected/present will return null
        //
        // will ONLY look for a non-hub entry
        public static ActiveController GetNonHubTrackingEntry(uint nid)
        {
            ActiveController ac;
            // normal processing
            if (!TrackingTable.TryGetValue(nid, out ac))
            {
                return (null);
            }
            return (ac);
        }
    }
}