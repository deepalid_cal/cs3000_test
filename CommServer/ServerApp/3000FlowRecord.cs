﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using System.Threading;



//
// our representation of Flow Record
//
namespace Calsense.CommServer
{
    class FlowRecord
    {
        public UInt32 time { get; set; }        // seconds past midnight
        public UInt16 date { get; set; }        // Julian day
        public UInt16 station_num { get; set; }
        public UInt16 box_index_0 { get; set; }
        public UInt16 expected_flow { get; set; }
        public UInt16 derated_expected_flow { get; set; }
        public UInt16 hi_limit { get; set; }
        public UInt16 lo_limit { get; set; }
        public UInt16 portion_of_actual { get; set; }
        public UInt16 flow_check_status {get; set; }
		public Boolean	FRF_NO_CHECK_REASON_indicies_out_of_range {get; set; }
		public Boolean	FRF_NO_CHECK_REASON_station_cycles_too_low {get; set; }
		public Boolean	FRF_NO_CHECK_REASON_cell_iterations_too_low {get; set; }
		public Boolean	FRF_NO_CHECK_REASON_no_flow_meter {get; set; }
		public Boolean	FRF_NO_CHECK_REASON_not_enabled_by_user_setting {get; set; }

		public Boolean	FRF_NO_CHECK_REASON_acquiring_expected {get; set; }
		public Boolean	FRF_NO_CHECK_REASON_combo_not_allowed_to_check {get; set; }
		public Boolean	FRF_NO_CHECK_REASON_unstable_flow {get; set; }
		public Boolean	FRF_NO_CHECK_REASON_cycle_time_too_short {get; set; }
		public Boolean	FRF_NO_CHECK_REASON_irrigation_restart {get; set; }
		public Boolean	FRF_NO_CHECK_REASON_not_supposed_to_check {get; set; }

		public Boolean	FRF_NO_UPDATE_REASON_zero_flow_rate	{get; set; }
        public Boolean FRF_NO_UPDATE_REASON_thirty_percent { get; set; }
        public UInt16 raw_flags { get; set; }
    }
    class FlowRecordList
    {
        public UInt32 system_gid { get; set; }
        public IList<FlowRecord> records;
        public FlowRecordList()
        {
            system_gid = 0;
            records = new List<FlowRecord>();
        }
        public void LogIt(MyLogger l, NLog.LogLevel lev)
        {
            int i;

            l.WriteMessage(lev, "8001", "-----Dumping Structure from Flow Recording Message-----");
            l.WriteMessage(lev, "8001", "System GID:" + this.system_gid.ToString());
            for (i = 0; i < this.records.Count; i++ )
            {
                l.WriteMessage(lev, "8001", " ** Record " + i.ToString() + " ** ");
                l.WriteMessage(lev, "8001", "Time:" + this.records[i].time.ToString());
                l.WriteMessage(lev, "8001", "Date:" + this.records[i].date.ToString());
                l.WriteMessage(lev, "8001", " Station Number:" + this.records[i].station_num.ToString());
                l.WriteMessage(lev, "8001", " Box Index0:" + this.records[i].box_index_0.ToString());
                l.WriteMessage(lev, "8001", " Expected Flow:" + this.records[i].expected_flow.ToString());
                l.WriteMessage(lev, "8001", " Derated Expected Flow:" + this.records[i].derated_expected_flow.ToString());
                l.WriteMessage(lev, "8001", " High Limit:" + this.records[i].hi_limit.ToString());
                l.WriteMessage(lev, "8001", " Low Limit:" + this.records[i].lo_limit.ToString());
                l.WriteMessage(lev, "8001", " Portion of actual:" + this.records[i].portion_of_actual.ToString());
                l.WriteMessage(lev, "8001", " Flow check status:" + this.records[i].flow_check_status.ToString());

                l.WriteMessage(lev, "8001", " indicies_out_of_range: " + this.records[i].FRF_NO_CHECK_REASON_indicies_out_of_range.ToString());
                l.WriteMessage(lev, "8001", " station_cycles_too_low: " + this.records[i].FRF_NO_CHECK_REASON_station_cycles_too_low.ToString());
                l.WriteMessage(lev, "8001", " cell_iterations_too_low: " + this.records[i].FRF_NO_CHECK_REASON_cell_iterations_too_low.ToString());
                l.WriteMessage(lev, "8001", " no_flow_meter: " + this.records[i].FRF_NO_CHECK_REASON_no_flow_meter.ToString());
                l.WriteMessage(lev, "8001", " not_enabled_by_user_setting: " + this.records[i].FRF_NO_CHECK_REASON_not_enabled_by_user_setting.ToString());
                l.WriteMessage(lev, "8001", " acquiring_expected: " + this.records[i].FRF_NO_CHECK_REASON_acquiring_expected.ToString());
                l.WriteMessage(lev, "8001", " combo_not_allowed_to_check: " + this.records[i].FRF_NO_CHECK_REASON_combo_not_allowed_to_check.ToString());
                l.WriteMessage(lev, "8001", " unstable_flow: " + this.records[i].FRF_NO_CHECK_REASON_unstable_flow.ToString());
                l.WriteMessage(lev, "8001", " cycle_time_too_short: " + this.records[i].FRF_NO_CHECK_REASON_cycle_time_too_short.ToString());
                l.WriteMessage(lev, "8001", " irrigation_restart: " + this.records[i].FRF_NO_CHECK_REASON_irrigation_restart.ToString());
                l.WriteMessage(lev, "8001", " not_supposed_to_check: " + this.records[i].FRF_NO_CHECK_REASON_not_supposed_to_check.ToString());
                l.WriteMessage(lev, "8001", " zero_flow_rate: " + this.records[i].FRF_NO_UPDATE_REASON_zero_flow_rate.ToString());
                l.WriteMessage(lev, "8001", " thirty_percent: " + this.records[i].FRF_NO_UPDATE_REASON_thirty_percent.ToString());

            }
        }
    }

}
