﻿//
// wraps existing 3000 stuff so can be used with hubs
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Calsense.Logging;
using ServerApp;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Advantage.Data.Provider;

//
// The PeriodicTasks can generate calls to functions of type
//        static void ActionInitiateWeatherData(uint nid, uint timezonekey)
// whenever it is scheduled time for one to run, or when an IHSFY job
// shows up.
//
// When these are for a hub connected controller, they have to be processed
// through the hub logic instead of the normal 3000 logic.
//
//

namespace Calsense.CommServer
{
    class HubWrapper
    {
        // is this NID hub/hub connected 3000?
        public static bool HubProcessing3000Yes(uint nid, CancellationTokenSource cts)
        {
            bool isahub = false;
            AdsCommand cmd;

            try
            {
                ServerDB db = new ServerDB(cts);
                AdsConnection conn = db.OpenDBConnectionNotAsync("");
                if (conn != null)
                {

                    // 1: Controller using Legacy LR hub.
                    // 2: Controller using Legacy SR hub.
                    // 3: Controller using CS3000 LR hub.
                    // 4: Controller using CS3000 SR hub.
                    // 5: Controller is an LR - HUB
                    // 6: Controller is an SR - HUB
                    // 7: Controller is a CS3000 LR HUB
                    // 8: Controller is a CS3000 SR HUB
                    // 9: Controller is an Inbound 2000 HUB
                    // 10: Controller is a 2000 behind an Inbound 2000 Hub
                    cmd = new AdsCommand("SELECT ISNULL(UseHubType,0) FROM CS3000Networks n " + 
                                         "JOIN ControllerSites s ON s.SiteID=n.NetworkID " +
                                         "WHERE s.Deleted = false AND s.CompanyID <> -1 AND UseHubType IN (3,4,7,8) AND NetworkId=" + nid.ToString(), conn);
                    var existsID = cmd.ExecuteScalar();
                    cmd.Unprepare();
                    if (existsID != null)
                    {
                        isahub = true;
                    }

                    db.CloseDBConnection(conn, "");
                }
            }
            catch
            {
                return (isahub);
            }
            return (isahub);
        }

        //
        // called via SocketTracker when nid is being processed in hub logic
        // usually this will be because of an error when trying to build/send
        // a periodic or IHSFY message. in that case, we won't be getting back
        // the normal no-more-messages mid, so we simulate that here so next
        // item in queue can be processed, if any.
        //
        public static void HubTrackingRemoval(uint nid)
        {
            ActiveLegacyConnection ac;

            ac = HubConnectionTracker.FindACfrom3000NID(nid);

            if(ac != null)
            {
                HubConnectionTracker.RemoveTrackingEntryIfDone(ac);
            }

            return;
        }

        public static uint GetHubID(uint controllerid, uint nid, CancellationTokenSource cts)
        {
            AdsCommand cmd = null;
            uint hubid = 0;

            try
            {
                ServerDB db = new ServerDB(cts);
                AdsConnection conn = db.OpenDBConnectionNotAsync("");
                if (conn != null)
                {
                    if(controllerid != 0)
                    { 
                        cmd = new AdsCommand("SELECT CASE WHEN UseHubType in (5,6,9) then ISNULL(ControllerID,0) ELSE ISNULL(HubID,0) END FROM CONTROLLERS WHERE Deleted = false AND ControllerID =" + controllerid.ToString(), conn);
                    }
                    if (nid != 0)
                    {
                        cmd = new AdsCommand("SELECT CASE WHEN UseHubType in (7,8) then ISNULL(n.NetworkID,0) ELSE ISNULL(HubID,0) END FROM cs3000networks n join controllersites s on s.siteid=n.networkid WHERE s.deleted = false  AND n.NetworkID =" + nid.ToString(), conn);
                    }
                    var existsID = cmd.ExecuteScalar();
                    cmd.Unprepare();
                    if (existsID != null)
                    {
                        hubid = (uint)Convert.ToInt32(existsID);
                    }

                    db.CloseDBConnection(conn, "");
                }
            }
            catch
            {
                return (0);
            }
            return (hubid); // either 0 if not a hub, or the hubid (which could be a 2000 controllerid or a 3000 nid)
        }
        //
        // called by Legacy tasks with a function that we decide if for a hub/hub-attached controller
        // if so, and the hub/controller is connected, queue up the job
        // if not, return 0
        // if for a hub/controller, but not connected, return negative number so caller
        // can finalize job
        //
        public static int Hub2000Function(Action<ActionArgs> func, uint cid, uint timezonekey, uint jobnumber, int priority, int hubtype, String adr, int connection_type, int multiplier, bool repeaterInUse, int dbTimerAddon, CancellationTokenSource cts)
        {
            bool isahub = false;
            uint hubid = 0;

            ActiveLegacyConnection ac;
            MyLogger _log;

            // 0 or NULL: Controller is not using a hub to communicate.
            // 1: Controller using Legacy LR hub.
            // 2: Controller using Legacy SR hub.
            // 3: Controller using CS3000 LR hub.
            // 4: Controller using CS3000 SR hub.
            // 5: Controller is an LR - HUB
            // 6: Controller is an SR - HUB
            // 7: Controller is a CS3000 LR HUB
            // 8: Controller is a CS3000 SR HUB

            if(hubtype == 1 || hubtype == 3 || hubtype == 4 || hubtype == 5 || hubtype == 7 || hubtype == 8)
            {
                isahub = true;
            }

            // get the hubid if needed
            if(isahub)
            {
                hubid = GetHubID(cid, 0, cts);
            }

            // use the NID to look in database and determine if this message is for a hub
            if (isahub)
            {
                _log = new MyLogger("2000Hub", cid.ToString(), cid.ToString(), Constants.ET2000Model);

                if(hubid <= 0)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "182", "HubID missing in DB for controller " + cid.ToString(), cid.ToString());
                    return (-3);    // no hub defined for a hub based legacy controller
                }

                _log = new MyLogger("2000hub", cid.ToString(), cid.ToString(), Constants.CS3000Model);

                _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Hub related periodic/IHSFY legacy command for: " + cid.ToString(), cid.ToString());

                // see if this legacy controller is connected via a 3000 LR/SR hub
                if (hubtype == 3 || hubtype == 4)
                {
                    // verify we have a connection to the 3000 hub/controller already (hub establishes, we can't make it)
                    ac = HubConnectionTracker.FindACfrom3000NID((uint)hubid);
                    if (ac != null)
                    {
                        //
                        // don't process commands if hub is distributing code (which can take an hour or more)
                        //
                        if (ac.codedownloadinprogress)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "182", "Code download in progress, not processing command for legacy: " + cid.ToString(), cid.ToString());
                            return (-7);
                        }

                        ac.acsem.Wait(cts.Token);
                        // if the controller isn't already known about, add it
                        if (!ac.controllers.Any(x => x.CID == cid))
                        {
                            // no, so add it
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Adding legacy controller to existing queuing/connection.", cid.ToString());
                            ac.controllers.Add(new ActiveLegacyConnection.ControllerInfo<UInt32, Int32, String, int>(cid, 0, "N" + hubid.ToString(), multiplier, hubtype, repeaterInUse, dbTimerAddon));    // new controller on existing connection
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Legacy Transport Timer multiplier is " + multiplier.ToString() +
                            " , system has a repeater? " + repeaterInUse.ToString() +
                            " , additional timer in DB is " + dbTimerAddon.ToString(), cid.ToString());
                        }

                        // queue up a wrapper message holding this function
                        ActionArgs args = new ActionArgs();
                        args.cid = cid;
                        args.adr = adr;
                        args.connectiontype = (uint)connection_type;
                        args.hostname = "N" + hubid.ToString();
                        args.timezonekey = timezonekey;
                        args.jobid = jobnumber;
                        args.cityid = ac.key[0] == 'N' ? 0 : (uint)Convert.ToInt32(ac.key);    // convert key back to integer
                        args.legacyaction = func;
                        _log.WriteMessageWithId(NLog.LogLevel.Debug, "182", "Queuing command for: " + cid.ToString(), cid.ToString());
                        ac.queued_functions.Add(new Tuple<int, Action<ActionArgs>, ActionArgs>(priority, Generic2000Function, args));

                        ac.acsem.Release();

                        // we have added the first outgoing function to the queue, we have to kick start it to make it go
                        HubConnectionTracker.RemoveTrackingEntryIfDone(ac);

                        // let caller know we have taken over this connection
                        return (1);
                    }
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "182", "2000 Message via hub, but hub not connected.", cid.ToString());
                    return (-3);
                }
                else if (hubtype == 1 || hubtype == 5)
                {
                    // this is a 2000 hub, or connected via a 2000 hub
                    ac = HubConnectionTracker.FindACfrom2000NID((uint)hubid);
                    if (ac != null)
                    {
                        //
                        // don't process commands if hub is distributing code (which can take an hour or more)
                        //
 
                        ac.acsem.Wait(cts.Token);
                        // if the controller isn't already known about, add it
                        if (!ac.controllers.Any(x => x.CID == cid))
                        {
                            // no, so add it
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Adding legacy controller to existing queuing/connection.", cid.ToString());
                            ac.controllers.Add(new ActiveLegacyConnection.ControllerInfo<UInt32, Int32, String, int>(cid, 0, "N" + hubid.ToString(), multiplier, hubtype, repeaterInUse, dbTimerAddon));    // new controller on existing connection
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Legacy Transport Timer multiplier is " + multiplier.ToString() +
                            " , system has a repeater? " + repeaterInUse.ToString() +
                            " , additional timer in DB is " + dbTimerAddon.ToString(), cid.ToString());
                        }

                        // queue up a wrapper message holding this function
                        ActionArgs args = new ActionArgs();
                        args.cid = cid;
                        args.adr = adr;
                        args.connectiontype = (uint)connection_type;
                        args.hostname = "N" + hubid.ToString();
                        args.timezonekey = timezonekey;
                        args.jobid = jobnumber;
                        args.cityid = ac.key[0] == 'N' ? 0 : (uint)Convert.ToInt32(ac.key);    // convert key back to integer
                        args.legacyaction = func;
                        _log.WriteMessageWithId(NLog.LogLevel.Debug, "182", "Queuing command for: " + cid.ToString(), cid.ToString());
                        ac.queued_functions.Add(new Tuple<int, Action<ActionArgs>, ActionArgs>(priority, Generic2000Function, args));

                        ac.acsem.Release();

                        // we have added the first outgoing function to the queue, we have to kick start it to make it go
                        HubConnectionTracker.RemoveTrackingEntryIfDone(ac);

                        // let caller know we have taken over this connection
                        return (1);
                    }
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "182", "2000 Message via hub, but hub not connected.", cid.ToString());
                    return (-3);
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "182", "Unexpected hubtype, should be 1,3,4,5 was " + hubtype.ToString(), cid.ToString());
                    return (-2);
                }
            }
            else
            {
                // let caller know not a hub involved
                return (0);
            }
        }

        //
        // called by Periodic tasks with a function that we decide is for a 3000 hub/hub-attached controller
        // if so, and the hub/controller is connected, queue up the job
        // if not, return 0
        // if for a hub/controller, but not connected, return negative number so caller
        // can finalize job
        //
        public static int HubFunction(Action<uint, uint> periodicfunc, Func<uint, uint, int> ihsfyfunc, uint nid, uint timezonekey, uint jobnumber, int priority, CancellationTokenSource cts)
        {
            bool isahub = false;
            AdsCommand cmd;
            int hubtype = 0;
            ActiveLegacyConnection ac;
            MyLogger _log;

            try
            {
                ServerDB db = new ServerDB(cts);
                AdsConnection conn = db.OpenDBConnectionNotAsync("");
                if (conn != null)
                {

                    // 1: Controller using Legacy LR hub.
                    // 2: Controller using Legacy SR hub.
                    // 3: Controller using CS3000 LR hub.
                    // 4: Controller using CS3000 SR hub.
                    // 5: Controller is an LR - HUB
                    // 6: Controller is an SR - HUB
                    // 7: Controller is a CS3000 LR HUB
                    // 8: Controller is a CS3000 SR HUB
                    cmd = new AdsCommand("SELECT ISNULL(UseHubType,0) FROM CS3000Networks n " + 
                                         "JOIN ControllerSites s ON s.SiteID=n.NetworkID " +
                                         "WHERE s.Deleted = false AND s.CompanyID <> -1 AND UseHubType IN (3,4,7,8) AND NetworkId=" + nid.ToString(), conn);
                    var existsID = cmd.ExecuteScalar();
                    cmd.Unprepare();
                    if (existsID != null)
                    {
                        isahub = true;
                        hubtype = Convert.ToInt32(existsID);
                    }

                    db.CloseDBConnection(conn, "");
                }
            }
            catch
            {
                return (-1);
            }

            // use the NID to look in database and determine if this message is for a hub/hub connected controller
            if (isahub)
            {
                _log = new MyLogger("3000Hub", nid.ToString(), nid.ToString(), Constants.CS3000Model);

                _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Hub related periodic/IHSFY command for: " + nid.ToString(), nid.ToString());

                // verify we have a connection to this hub/controller already (hub establishes, we can't make it)
                ac = HubConnectionTracker.FindACfrom3000NID(nid);
                if (ac != null)
                {
                    //
                    // don't process commands if hub is distributing code (which can take an hour or more)
                    //
                    if(ac.codedownloadinprogress)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "182", "Code download in progress, not processing command for: " + nid.ToString(), nid.ToString());
                        return (-7);
                    }

                    // if this is a hub change notification, and hub is not connected, and no slave controllers exist, we can safely return now
                    if (ihsfyfunc != null && ihsfyfunc == PeriodicTasks.JOB_hubupdate)
                    {
                        if (!ac.controllers3000.Exists(x => x.NID == nid))
                        {
                            int slaves = 0;
                            for (int i = ac.controllers.Count - 1; i >= 0; i--)
                            {
                                if (HubConnectionTracker.IsThisControllerMeOrBehindMe(ac, nid, ac.controllers[i].CID))
                                {
                                    slaves++;
                                    break;  // 1 is enough
                                }
                            }
                            if (ac.controllers3000.Count > 0)
                            {
                                // cid is a 3000 that has this hub as its hub
                                if (ac.controllers3000.Exists(x => x.myHubID == nid)) slaves++;
                            }
                            if (slaves == 0)
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Ignoring 3000 hub change IHSFY since hub already out of system.", nid.ToString());
                                return (-3);
                            }
                        }
                    }

                    ac.acsem.Wait(cts.Token);

                    // check for the special case of a controller being removed via hub change while we waited on semaphore
                    if(!ac.controllers3000.Exists(x => x.NID == nid))
                    {
                        ac.acsem.Release();
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Ignoring command since hub/controller removed at the moment.", nid.ToString());
                        return (-3);
                    }

                    // queue up a wrapper message holding this function
                    ActionArgs args = new ActionArgs();
                    args.adr = "3000";    // not used for a 3000
                    args.cid = nid;
                    args.connectiontype = 1;    // 5 or 7 for 2000's, 1 indicates a 3000
                    args.hostname = "N" + ac.controllers3000.Find(x => x.NID == nid).myHubID.ToString();
                    args.timezonekey = timezonekey;
                    args.jobid = jobnumber; 
                    args.cityid = ac.key[0] == 'N' ? 0 : (uint)Convert.ToInt32(ac.key);    // convert key back to integer
                    if (periodicfunc != null) args.periodicaction = periodicfunc;
                    else args.ihsfyaction = ihsfyfunc;
                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "182", "Queuing command for: " + nid.ToString(), nid.ToString());
                    ac.queued_functions.Add(new Tuple<int, Action<ActionArgs>, ActionArgs>(priority, Generic3000Function, args)); 

                    ac.acsem.Release();

                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "182", "FYI: No other commands queued.", nid.ToString());
                    // treat this as a poll, so set the pollstate to 0
                    ac.controllers3000.Find(x => x.NID == nid).pollstate = 0;
                    // if we are issuing a IHSFY command, this controller is in highest mobile state,
                    // that will drop down as it idles
                    if (ihsfyfunc != null)
                    {
                        // only mobile jobs kick us into higher polling state
                        if (PeriodicTasks.IsThisA3000MobileJob(ihsfyfunc))
                        {
                            if (ac.controllers3000.Find(x => x.NID == nid).consecutiveErrors < (HubConnectionTracker.HUB_POLL_CONSEC_ERROR1_COUNT + 1))
                            {                                
                                ac.controllers3000.Find(x => x.NID == nid).mobilestate = 2;
                                ac.controllers3000.Find(x => x.NID == nid).mobileduration = 0;

                                // flush any standard polls on this group so only mobiles occur during mobile session
                                ac.acsem.Wait(cts.Token);

                                for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                                {
                                    if (ac.queued_functions[i].Item3.adr == "3000")
                                    {
                                        if (ac.queued_functions[i].Item1 <= 10)
                                        {
                                            ac.queued_functions.RemoveAt(i);
                                            _log.WriteMessage(NLog.LogLevel.Info, "8866", "Removing queued poll(s) as new mobile job.");
                                        }                                        
                                    }
                                }
                                ac.acsem.Release();
                            }
                            else
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "182", "Not entering higher mobile rate of polling because of consecutive errors already seen.", nid.ToString());
                            }
                        }
                    }
                    // we have added function to the queue, we have to kick start it to make it go
                    HubConnectionTracker.RemoveTrackingEntryIfDone(ac);

                    // let caller know we have taken over this connection
                    return (1);
                }
                else
                {
                    // possibly this controller is connected in as not a hub (i.e. it is being changed to a hub)
                    // check for that...
                    ActiveController oldac;
                    oldac = ServerSocketTracker.GetNonHubTrackingEntry(nid);
                    if (oldac != null)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "FYI: probably newly defined hub currently connected as non-hub.", nid.ToString());
                        return (0); // 0 indicates to process this command as a non-hub, since controller connected that way
                    }
                    if(ihsfyfunc != null && ihsfyfunc == PeriodicTasks.JOB_hubupdate)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Ignoring hub change IHSFY since hub not connected, and no others in group.", nid.ToString());
                    }
                    return (-2);
                }
            }
            else
            {
                // let caller know not a hub involved
                return (0);
            }
        }

        //
        // Get a pseudo ActiveController object for a hub based 3000
        //
        // to avoid duplicated/rewriting the 3000 transmission functions in PeriodicTasks which
        // work off an ActiveController, we generate enough of one here based off an existing
        // LegacyActiveController as used with hubs to work for transmission. basically needs
        // to contain the network stream to write to.
        //
        //
        public static ActiveController GetPseudoActiveController(uint nid)
        {
            ActiveLegacyConnection ac;
            ActiveController pseudoac = null;

            // see if we are connected to the given NID
            ac = HubConnectionTracker.FindACfrom3000NID(nid);
            if(ac != null)
            {
                // get the serial number for this controller out of our data
                var cont = ac.controllers3000.Find(x => x.NID == nid);
                if(cont != null)
                {
                    pseudoac = new ActiveController(nid, cont.sn, ac.net_stream, null, null);    // cts null should be okay, no one on the transmit side uses it., socket null too
                }
            }
            return (pseudoac);
        }

        //
        // Call a generic 3000 Periodic/IHSFY function
        // (for a hub/hub-connected 3000)
        //
        static void Generic3000Function(ActionArgs args)
        {
            MyLogger _log;

            _log = new MyLogger("3000Hub", args.cid.ToString(), args.cid.ToString(), Constants.CS3000Model);

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Calling outbound message function for 3000 hub based controller: " + args.cid.ToString(), args.cid.ToString());

            // call the function for this device, only one of these two possibilities will be non-null
            if (args.ihsfyaction != null) args.ihsfyaction(args.cid, args.jobid);
            if (args.periodicaction != null) args.periodicaction(args.cid, args.timezonekey);

            // if caller has a problem it will call
            // ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
        }

        //
        // Call a generic 3000 Periodic/IHSFY function
        // (for a hub/hub-connected 3000)
        //
        static void Generic2000Function(ActionArgs args)
        {
            MyLogger _log;

            _log = new MyLogger("2000Hub", args.cid.ToString(), args.cid.ToString(), Constants.ET2000Model);

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Calling outbound message function for legacy controller on hub. " + args.cid.ToString(), args.cid.ToString());

            // call the function for this device, only one of these two possibilities will be non-null
            args.legacyaction(args);

            // if caller has a problem it will call
            // ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
        }


    }
}
