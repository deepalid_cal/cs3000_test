﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using Advantage.Data.Provider;



namespace Calsense.CommServer
{

    public class MsgRainShutdown
    {
        // test call unmanged C code
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint Dummy(ref DUMMY_STRUCT pp,
        uint why,
        [In, Out] byte[] dest,
        uint sz,
        uint idx);


        //
        // Parses Rain Shutdown inbond message from controller
        // returns via Tuple:
        //   List of byte arrays, which are 0 to n messages to send as a response
        //   String that will be passed back in if this connection stays up
        //   Boolean as a flag to terminate connection or not
        //   int as an internal state to track thru session (if needed)
        //
        public static async Task<Tuple<List<byte[]>,String, bool, int>> Parse(ClientInfoStruct client, CancellationTokenSource cts)
        {
            byte[] data;
            MyLogger _logger;
            uint size;
            byte[] Buffer = null;
            AdsCommand cmd, ins;
            AdsDataReader rdr;
            bool abort = true;
            UInt16 RainShutdownDateRaw;
            DateTime RainShutdownDate;
            DateTime ReceivedTimestamp;
            int n, z;
            int offset;
            int mins;

            _logger = new MyLogger(typeof(MsgRainShutdown).Name, Misc.ClientLogString(client), Misc.ClientLogDB(client), Constants.CS3000Model);
            _logger.WriteMessage(NLog.LogLevel.Info, "9704", "Processing Rain Indication message...");

            size = client.rxlen;
            Buffer = client.rx;

            offset = 0;
            RainShutdownDateRaw = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
            RainShutdownDate = Misc.PackedDatetoDate(RainShutdownDateRaw);
            ReceivedTimestamp = DateTime.Now;

#if DUMPRAWBYTES
            _logger.WriteMessage(NLog.LogLevel.Info, "3004", "Dump of bytes raw:" + size.ToString());
            for (i = 0, j = (int)size; i < size; i += 8)
            {
                if (j >= 8)
                {
                    s = Buffer[i].ToString("x2") + " " + Buffer[i + 1].ToString("x2") + " " + Buffer[i + 2].ToString("x2") + " " + Buffer[i + 3].ToString("x2") + " " + Buffer[i + 4].ToString("x2") + " " + Buffer[i + 5].ToString("x2") + " " + Buffer[i + 6].ToString("x2") + " " + Buffer[i + 7].ToString("x2");
                    j -= 8;
                }
                else
                {
                    s = "";
                    while (j > 0)
                    {
                        s = s + Buffer[i].ToString("x2") + " ";
                        j--;
                        i++;
                    }
                }
                _logger.WriteMessage(NLog.LogLevel.Debug, "8001", s);
                if (j <= 0) break;
                Thread.Sleep(10);       // allow UDP logger to catch up
            }
#endif
            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                // generate a "job" for each controller that gets rain from this controller to
                // tell them of the shutdown.

                
                //SELECT NetworkID,TimeZone FROM CS3000Networks n
                //JOIN ControllerSites s ON n.NetworkID=s.SiteId
                //WHERE RainSharingNetworkID=:NETWORKID
                //AND s.Deleted=false

                // first get a list of controllers that use this one...
                try
                {
                    cmd = new AdsCommand("SELECT NetworkID, TimeZone FROM CS3000Networks n "
                        + " JOIN ControllerSites s ON n.NetworkID=s.SiteId WHERE "
                        + " s.Deleted=false and RainSharingNetworkID= " + client.nid.ToString(),conn);

                    _logger.WriteMessage(NLog.LogLevel.Debug, "9753", "SQL: " + cmd.CommandText);

                    rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            n = rdr.GetInt32(0);
                            if(!rdr.IsDBNull(1))
                            {
                                z = rdr.GetInt32(1);
                            }
                            else
                            {
                                z = 0;
                            }
                            // we have a network ID and timezone - 
                            // compute the number of minutes left before the next 8pm in the controller timezone
                            DateTime ControllerNow = PeriodicTasks.CurrentTimeAtControllerInTz(z);
                            DateTime ControllerStartOfDay = ControllerNow.Date;
                            DateTime Controller8pm = ControllerStartOfDay.AddHours(20);
                            DateTime ControllerNext8pm = ControllerNow <= Controller8pm ? Controller8pm : Controller8pm.AddDays(1);
                            TimeSpan RemainingMinutes = ControllerNext8pm - ControllerNow;
                            mins = (int) RemainingMinutes.TotalMinutes;

#if IGNORE_RAINSHUTDOWN_CLOSE_TO_WINDOW
                            if (mins > 2)
                            {
#endif
                                // insert a job
                                ins = new AdsCommand("INSERT INTO IHSFY_jobs(Command_id,Network_id,User_id,Date_created,Status,Param1,Param2,Param3) "
                                    + " VALUES(" + Constants.MID_FROM_COMMSERVER_RAIN_SHUTDOWN_packet.ToString() + ","
                                    + n.ToString() + ",-1,'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                                    + "0," + mins.ToString() + ",0,0)", conn);

                                _logger.WriteMessage(NLog.LogLevel.Debug, "9753", "SQL: " + ins.CommandText);

                                ins.ExecuteNonQuery();
                                ins.Unprepare();
#if IGNORE_RAINSHUTDOWN_CLOSE_TO_WINDOW
                            }
                            else
                            {
                                _logger.WriteMessage(NLog.LogLevel.Debug, "9753", "Not bothering to insert a rain shutoff so close to controller 8pm time " + ControllerNow.ToString() + " for NID " + n.ToString());
                            }
#endif
                        }
                    }
                    rdr.Close();
                    cmd.Unprepare();

                    // if there are any 2000 legacy controllers that share from this network ID, build a job for each of them now
                    List<uint> cids = new List<uint>();   // we will build this up based on query results
                    UInt16 raindate = Misc.CurrentPackedDate();


                    // query all the controllers we need to build a job for
                    cmd = new AdsCommand("SELECT distinct ControllerID FROM Controllers WHERE RainSharingNetworkID = " + client.nid.ToString() + " AND Communicate=1 AND Deleted=0", conn);
                    _logger.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);
                    rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        var cid = 0;
                        while (rdr.Read())
                        {
                            cid = 0;
                            if (!rdr.IsDBNull(0)) cid = rdr.GetInt32(0);

                            if (cid != 0)
                            {
                                var zz = LegacyTransport.AlreadyShutdown.Find(x => ((x.controllerId == cid) && (x.dateofinterest == raindate)));
                                if (zz == null)
                                {
                                    cids.Add((uint)cid);
                                }
                                else
                                {
                                    _logger.WriteMessageWithId(NLog.LogLevel.Info, "154", "Skipping Rain Shutdown to controller since already OK'd it this period.", cid.ToString());
                                }
                            }
                        }
                    }
                    rdr.Close();
                    cmd.Unprepare();

                    foreach (uint c in cids)
                    {


#if IGNORE_RAINSHUTDOWN_CLOSE_TO_WINDOW
                            if (mins > 2)
                            {
#endif
                        // insert a job
                        ins = new AdsCommand("INSERT INTO IHSFY_jobs(Command_id,Controller_id,User_id,Date_created,Status,Param1,Param2,Param3) "
                            + " VALUES(" + LegacyTasks.MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER.ToString() + ","
                            + c.ToString() + ",-1,'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                            + "0," + raindate.ToString() + ",0,0)", conn);

                        _logger.WriteMessage(NLog.LogLevel.Debug, "9753", "SQL: " + ins.CommandText);

                        ins.ExecuteNonQuery();
                        ins.Unprepare();
#if IGNORE_RAINSHUTDOWN_CLOSE_TO_WINDOW
                            }
                            else
                            {
                                _logger.WriteMessage(NLog.LogLevel.Debug, "9753", "Not bothering to insert a rain shutoff so close to controller 8pm time " + ControllerNow.ToString() + " for NID " + n.ToString());
                            }
#endif
                    }

                    abort = false;
                }
                catch(Exception e)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "9791", "Database exception during Rain Indication processing " + e.ToString());
                }

				db.CloseDBConnection(conn, _logger);
            }
            
            // if nothing saved to database, do not ACK
            if (abort == true)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "9701", "Not acking, problem with DB update.");

                _logger.WriteMessage(NLog.LogLevel.Info, "14333", "Build Rain Indication Nak response.");

                return (Tuple.Create(Misc.buildNakResponse(client.nid, client.sn, client.mid), (String)null, false, Constants.NO_STATE_DATA));
            }

            var mem = new MemoryStream(0);
            // build ack response
            if (client.nid != 0)
            {
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(client.sn, mem);
                // network id
                Misc.Uint32ToNetworkBytes(client.nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_RAIN_INDICATION_receipt_ack, mem);
                _logger.WriteMessage(NLog.LogLevel.Info, "9771", "Build Rain Indication Ack response.");
            }

            // get the built response to return and be sent
            data = mem.ToArray();
            List<byte[]> rsplist = new List<byte[]>();
            rsplist.Add(data);
            return (Tuple.Create(rsplist, (String)null, false, Constants.NO_STATE_DATA));
        }
    }
}
