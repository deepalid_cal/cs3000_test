//
// HubConnections - manages table to track network connections to controllers behind
// 3000 series hubs.
// (Static class, as only one copy ever exists in the CommServer)
//
//
// A "TrackingTable" stores an entry for each IP address that is currently has
// communications active or queued up to be active.
//
// Each entry in the table points to a "ActiveLegacyConnection" object. One
// of these objects will exist per IP address for non-RF-hub controllers or
// one per RF-hubs (controllers that can interfere with each other).
//
// This means commands are held in a queue either:
// a) queue for all commands directed to a single IP address (which could
//    be a single controller, or multiple controllers behind a hub)
// b) queue for all commmands directed to multiple IP address that make
//    up a customer "site".
//
// The command queues contain priority information for each message to
// be sent, so highest priority per queue becomes the next message to send.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Calsense.Logging;
using ServerApp;
using System.Collections.Concurrent;
using System.Configuration;
using Advantage.Data.Provider;
using System.IO;

namespace Calsense.CommServer
{
    //
    // class that handles populating and manipulating lookup table containing ActiveLegacyController
    //
    static class HubConnectionTracker
    {
        static int HUB_POLL_LOOP_SECS;         // run loop this often
        static int HUB_POLL_MOBILE_HIGH ;         // poll a mobile state this often at first
        static int HUB_POLL_MOBILE_HIGH_DURATION;  // in high state at least this long
        static int HUB_POLL_MOBILE_MED;      // drop back to this if nothing during high
        static int HUB_POLL_MOBILE_MED_DURATION;   // in medium state at least this long
        static int HUB_POLL_STANDARD;        // poll duration unless in mobile
        static public int HUB_POLL_TIMEOUT;        // once poll issued, need to see No-More-Messages or Busy within this amount of seconds
        static public int HUB_POLL_CONSEC_ERROR1_COUNT;
        static int HUB_POLL_ERROR1_SLOW_FACTOR;
        static public int HUB_POLL_CONSEC_ERROR2_COUNT;
        static int HUB_POLL_ERROR2_SLOW_FACTOR;

        // constructor
        static HubConnectionTracker() 
        {
            HubTrackingTable = new ConcurrentDictionary<String, ActiveLegacyConnection>();     // Dictionary of hostnames to Connection objects
            _log = new MyLogger("HubTracker", "", "", Constants.CS3000Model);
            _sem = new SemaphoreSlim(1);
            _cts = null;        // this will get set at runtime
            rand = new Random();
            // start tast that will poll the 3000 series controllers we have connected
            Task.Factory.StartNew(HubPollLoop, TaskCreationOptions.LongRunning);
            HUB_POLL_LOOP_SECS = Int32.Parse(ConfigurationManager.AppSettings["HUB_POLL_LOOP_SECS"]);
            HUB_POLL_MOBILE_HIGH = Int32.Parse(ConfigurationManager.AppSettings["HUB_POLL_MOBILE_HIGH"]);
            HUB_POLL_MOBILE_HIGH_DURATION = Int32.Parse(ConfigurationManager.AppSettings["HUB_POLL_MOBILE_HIGH_DURATION"]);
            HUB_POLL_MOBILE_MED = Int32.Parse(ConfigurationManager.AppSettings["HUB_POLL_MOBILE_MED"]);
            HUB_POLL_MOBILE_MED_DURATION = Int32.Parse(ConfigurationManager.AppSettings["HUB_POLL_MOBILE_MED_DURATION"]);
            HUB_POLL_STANDARD = Int32.Parse(ConfigurationManager.AppSettings["HUB_POLL_STANDARD"]);
            HUB_POLL_TIMEOUT = Int32.Parse(ConfigurationManager.AppSettings["HUB_POLL_TIMEOUT"]);
            HUB_POLL_CONSEC_ERROR1_COUNT = Int32.Parse(ConfigurationManager.AppSettings["HUB_POLL_CONSEC_ERROR1_COUNT"]);
            HUB_POLL_ERROR1_SLOW_FACTOR = Int32.Parse(ConfigurationManager.AppSettings["HUB_POLL_ERROR1_SLOW_FACTOR"]);
            HUB_POLL_CONSEC_ERROR2_COUNT = Int32.Parse(ConfigurationManager.AppSettings["HUB_POLL_CONSEC_ERROR2_COUNT"]);
            HUB_POLL_ERROR2_SLOW_FACTOR = Int32.Parse(ConfigurationManager.AppSettings["HUB_POLL_ERROR2_SLOW_FACTOR"]);
        }

        // properties
        public static ConcurrentDictionary<String, ActiveLegacyConnection> HubTrackingTable;
        private static MyLogger _log;
        private static SemaphoreSlim _sem;  // mutex to protect access to our dictionary
        private static CancellationTokenSource _cts;
        private static Random rand;         // for random delay time
        private static CancellationTokenSource comboCts { set; get; }

        public static void SetCts(CancellationTokenSource c)
        {
            _cts = c;
            comboCts = CancellationTokenSource.CreateLinkedTokenSource(_cts.Token, LegacyConnectionTracker.throttleCts.Token);
        }

        public static int GetPollTimeout()
        {
            return HUB_POLL_TIMEOUT;
        }

        //
        // called on each subsequence network data, verify nid is one we know about on this hub
        //
        public static int DoubleCheckNid(String key, uint nid, uint sn, NetworkStream ns, MyLogger _logger)
        {
            ActiveLegacyConnection ac;

            // tracking table key for the hub is either
            // N + nid or
            // cityid

            // we should have a tracking entry for this hub
            if (!HubTrackingTable.TryGetValue(key, out ac))
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "90", "Hub tracking table containing " + nid.ToString() + " disappeared while socket active?!");
                return (-1);
            }
            else
            {
                // verify given nid is one we think is on this connection
                if (!ac.controllers3000.Any(x => x.NID == nid))
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "90", "Message from NID not defined in DB as on this hub. NID:" + nid.ToString());
                    return (-1);
                }
            }
            return (0);
        }

        private static void DisconnectController(ActiveLegacyConnection ac)
        {
            // signal listener to close
            // we let the socket exception close the listener...if we do a ac.listenCts.Cancel();
            // we can accidently close the old *and* the new socket when switching between controllers
            // on a hub.
            // close socket
            if (ac.sock == null) return;
            if (!ac.sock.Connected) return;
            ac.sock.Shutdown(SocketShutdown.Both);
            ac.sock.Disconnect(false);
        }
        private static Boolean ConnectController(ActiveLegacyConnection ac)
        {
            IPAddress ip;
            int port = 0;
            int retries;
            int delay;
            bool fail = true;

            retries = Convert.ToInt32(ConfigurationManager.AppSettings["LegacyConnectionRetryCount"]);
            delay = Convert.ToInt32(ConfigurationManager.AppSettings["LegacyConnectionFailSquelchSeconds"]);
            if (delay < 0) delay = 0;

            String connectname = ac.activehost.ToString();

            // if there is a colon, then we have a hard coded port number to deal with
            if(connectname.Contains(':'))
            {
                port = int.Parse(connectname.Split(':')[1]);
                connectname = connectname.Split(':')[0];
            }

            if (!IPAddress.TryParse(connectname, out ip))
            {
                connectname = ac.activehost + ".eairlink.com";   // if not already an IP address, assume a phone number, convert to hostname
            }
            if (ac.connection_type == 5 && port == 0)
            {
                port = 2000;
            }
            if (ac.connection_type == 7 && port == 0)
            {
                port = 12345;
            }

            // if this host has had connection problems recently, don't even bother until
            // timeout expires
            DateTime checktime;
            if (LegacyConnectionTracker.LockoutTable.TryGetValue(connectname, out checktime))
            {
                // this host may be locked out, see if that is the case and bail if so
                if (DateTime.Now < checktime)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "199", "Not attempting connection as controller recently failed connection attempt.", ac.activecid.ToString());
                    return (false);
                }
            }

            _log.WriteMessageWithId(NLog.LogLevel.Info, "3005", "Attempting connection attempt to " + connectname, ac.activecid.ToString());

            try
            {
                ac.sock = new Socket(AddressFamily.InterNetwork,
                                    SocketType.Stream,
                                    ProtocolType.Tcp);
            }
            catch (Exception e)
            {
                if (e is SocketException)
                {
                    var ex = (SocketException)e;
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "3045", "Socket error " + e.ToString() + " during adding new connection to " + connectname, ac.activecid.ToString());
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "3045", "Unexpected exception during creating a new connection to " + connectname, ac.activecid.ToString());
                }

                return (false);
            }

            while (true)
            {

                try
                {

                    fail = false;

                    IPAddress[] IPs = Dns.GetHostAddresses(connectname);

                    ac.lastConnectattempt = DateTime.Now;

                    ac.sock.Connect(IPs[0], port);
                }
                catch (Exception e)
                {
                    if (e is SocketException)
                    {
                        var ex = (SocketException)e;
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "3005", "Socket error " + e.ToString() + " during connection attempt to " + connectname, ac.activecid.ToString());
                    }
                    else
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "3005", "Unexpected exception during connection attempt to " + connectname, ac.activecid.ToString());
                    }

                    fail = true;
                }

                if (fail != true)
                {
                    // connection successful
                    break;
                }


                // connection failure, see if we should retry...
                retries--;

                if(retries >= 0)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "3005", "Attempt to connect failed, will retry in Will retry this connection in " + delay.ToString() + " secs.", ac.activecid.ToString());
                    try
                    {
                        Task.Delay(delay * 1000, _cts.Token).Wait();
                    }
                    catch
                    {
                        return(false); // exiting program
                    }
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "3005", "*Retrying* connection attempt to " + connectname, ac.activecid.ToString());
                }
                else
                {
                    // done retrying
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "3005", "Failed connection attempt, after retries, to " + connectname, ac.activecid.ToString());

#if LOCKOUTLEGACYCONTROLLERIMPLEMENTED
                    // flag this controller as a problem 
                    DateTime when;
                    int lockouttime;
                    lockouttime = Convert.ToInt32(ConfigurationManager.AppSettings["LegacyConnectionFailSquelchSeconds"]);

                    if (!LockoutTable.TryGetValue(connectname, out when))
                    {
                        if (lockouttime != 0)
                        {
                            when = DateTime.Now;
                            LockoutTable.TryAdd(connectname, when.AddSeconds((double)lockouttime));
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "3005", "Since connect failed, locking out connection attempts for  " + lockouttime + " secs, until " + when.AddSeconds((double)lockouttime).ToString(), ac.activecid.ToString());
                        }                        
                    }
                    else
                    {
                        // entry already exists, update time
                        if(lockouttime != 0)
                        {
                            when = DateTime.Now;
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "3005", "Connect failed, locking out connection attempts for  " + lockouttime + " secs, until " + when.AddSeconds((double)lockouttime).ToString(), ac.activecid.ToString());
                            LockoutTable[connectname] = when.AddSeconds((double)lockouttime);
                        }
                        else
                        {
                            // no more lockout, remove
                            LockoutTable.TryRemove(connectname, out when);
                        }
                    }
#endif
                    return (false); 
                }
            }


            // we've connected... set up the network stream object
            ac.net_stream = new NetworkStream(ac.sock, true);

            ac.NeedsReconnect = false;

            // set up a legacy listening task to handle responses for controller(s) on this connection
            // note: we use a linked cancellation token so listener can be terminated individually when
            // we want to close connection - or overall when program exiting.
            var client = new LegacyInternal(ac, CancellationTokenSource.CreateLinkedTokenSource(ac.listenCts.Token, _cts.Token));
            Task.Run(() => client.Do());

            return (true);  // success
        }

        //
        // used by LegacyTransport at end of a reception so we can
        // either:
        // a) move to next queue thing to do on connection
        // or
        // b) close connection
        //
        // btw, we only get here if we know this connection is managed/tracked by HubConnection class.
        //
        //
        //
        public static void RemoveTrackingEntryIfDone(ActiveLegacyConnection ac)
        {            
            if(ac == null)return;

        reenter:

            try
            {
                // lock the priority list so no one messes with it while we search for function to call
                ac.acsem.Wait(_cts.Token);

                if (ac.TaskActive)
                {
                    ac.acsem.Release();
                    return;
                }

                if (ac.activeadr.ToString() == "3000")
                {
                    // done talking to a 3000, clear the active copy of the socket variable so it isn't accidently used/closed
                    // each 3000 has its own socket/stream stored in the controller if/when needed. 2000's connected to on
                    // demand
                    ac.sock = null;
                }

                // when 2000 controllers are moved, there could be a job for a controller that is not in the ac controller list yet. lets get rid of any of those
                // before we start looking
                if(ac.queued_functions.Count > 0)
                {
                    for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                    {
                        Tuple<int, Action<ActionArgs>, ActionArgs> tf = ac.queued_functions[i];
                        if(tf.Item3.adr != "3000" && tf.Item3.hostname != "INTERNAL")  // is this for a 2000 and not an internal hub change command
                        {
                            if (!ac.controllers.Exists(x => x.CID == tf.Item3.cid))
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "19", "Removing job for 2000 controller moved to new connection, not there yet.", tf.Item3.cid.ToString()); 
#pragma warning disable 4014
                                Misc.AddET2000Alert(_cts, tf.Item3.cid, "Controller moved, no comm on new conn yet.", true);
                                LegacyTasks.CompleteLegacyJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
#pragma warning restore 4014
                                ac.queued_functions.RemoveAt(i); 
                            }
                        }
                    }
                }


                // see if anyone pending on queue
                if(ac.queued_functions.Count == 0)
                {
                    // nothing else to process...okay to finish and close connection
                    if ((ac.activeadr.ToString() != "3000" && !ac.inbound2000 && ac.controllers.Count > 0 && !ac.controllers.Find(x => x.CID == ac.activecid).imBehind3000) || (ac.sock != null && ac.sock.Connected != true))
                    {
                        if (ac.sock != null && ac.sock.Connected)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "19", "Since no more jobs, and was communicating with 2000 not behind 3000, disconnecting.", ac.activecid.ToString());
                            DisconnectController(ac);
                        }
                        // give up semaphore for legacy controllers
                        if (ac.activeadr.ToString() != "3000")
                        {
                            if (LegacyConnectionTracker.throttle != null)
                            {
                                LegacyConnectionTracker.totalThrottleFrees++;
                                LegacyConnectionTracker.throttle.Release();
                            }
                        }
                        ac.NeedsReconnect = true;   // needs to reconnect next time using
                    }
                    if (!ac.inbound2000)
                    {
                        ac.sock = null; // all done, nothing else queued, no more connection memory
                    }
                }
                else
                {
                    int highest_priority;
                    int idx_to_work;
                recycle:
                    // there are still things to so, so 
                    // look for next highest priority thing to send
                    highest_priority = -1;  // start at impossibly low priority, we know there is at least one higher than this present
                    idx_to_work = -1;
                    ActionArgs args = new ActionArgs();
                    Action<ActionArgs> func = null;
                    foreach (Tuple<int, Action<ActionArgs>, ActionArgs> tf in ac.queued_functions)
                    {
                        if(tf.Item1 > highest_priority)
                        {
                            // note the highest priority we have found so far
                            highest_priority = tf.Item1;
                            // index of this function in our list, so we can access/call it
                            idx_to_work = ac.queued_functions.IndexOf(tf);
                            // get the arguments to pass into action when calling it
                            args.adr = tf.Item3.adr;
                            args.cid = tf.Item3.cid;
                            args.hostname = tf.Item3.hostname;
                            args.timezonekey = tf.Item3.timezonekey;
                            args.connectiontype = tf.Item3.connectiontype;
                            args.jobid = tf.Item3.jobid;
                            args.cityid = tf.Item3.cityid;
                            args.ihsfyaction = tf.Item3.ihsfyaction;
                            args.periodicaction = tf.Item3.periodicaction;
                            args.legacyaction = tf.Item3.legacyaction;
                            func = tf.Item2;
                        }
                    }
                    // if switching to different IP address/port, we need to close current connection and connect to new connection
                    // also, if connection has spontaneously dropped, make a reconnect attempt here

                    // if switching between 2000's on different 3000 hubs, note that here
                    uint oldhubid = 0;
                    uint newhubid = 0;
                    if (args.adr != "3000" && ac.activeadr.ToString() != "3000" && !ac.inbound2000)
                    {
                        newhubid = HubWrapper.GetHubID(args.cid, 0, _cts);
                        oldhubid = HubWrapper.GetHubID(ac.activecid, 0, _cts);
                    }

                    if(args.hostname == "INTERNAL")
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "201", "Processing internal command.", args.cid.ToString());
                    }
                    else if(!ac.inbound2000 && (ac.activehost.ToString() != args.hostname || ac.connection_type != args.connectiontype || ac.sock == null || !ac.sock.Connected || newhubid != oldhubid))
                    {
                        // disconnect from current controller only if pure 2000 environment
                        // or if connected to a 2000 controller in a mixed environment
                        if (ac.activeadr.ToString() != "3000" && ac.controllers.Count > 0 && !ac.controllers.Find(x => x.CID == ac.activecid).imBehind3000 && ac.sock != null)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "19", "Since changing controllers, and was talking to 2000 not behind 3000, disconnecting first.", ac.activecid.ToString());
                            DisconnectController(ac);
                        }

                        // if this command is for a 3000 controller, it will
                        // have an established stream, use that.
                        if (args.adr == "3000")
                        {
                            if (ac.controllers3000.Exists(x => x.NID == args.cid))
                            {
                                ac.net_stream = ac.controllers3000.Find(x => x.NID == args.cid).net_stream3000;
                                ac.sock = ac.controllers3000.Find(x => x.NID == args.cid).sock_3000;
                            }
                            else
                            {
                                // command for a 3000 that is not present?
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "19", "When looking for 3000, did not find it. NID was " + args.cid.ToString(), args.cid.ToString());
                            }
                        }
                        else
                        {
                            // this command is for a 2000 controller, which could
                            // be behind a 3000 hub.
                            uint hubid = HubWrapper.GetHubID(args.cid, 0, _cts);
                            if (ac.controllers.Find(x => x.CID == args.cid).myhubtype == 3 || ac.controllers.Find(x => x.CID == args.cid).myhubtype == 4)
                            {
                                if (ac.controllers3000.Exists(x => x.NID == hubid))
                                {
                                    // this 2000 is behind a 3000 hub
                                    ac.net_stream = ac.controllers3000.Find(x => x.NID == hubid).net_stream3000;
                                    ac.sock = ac.controllers3000.Find(x => x.NID == hubid).sock_3000;
                                    // if behind a 3000 SR hub, the hostname of 2000 does not matter, what matters is the hub NID
                                    // and the "key" for the Connection object in this case is "N" + NID
                                    if(ac.controllers.Find(x => x.CID == args.cid).myhubtype == 4)
                                    {
                                        _log.WriteMessageWithId(NLog.LogLevel.Info, "19", "Command for a 2000 behind 3000 SR NID: " + hubid.ToString(), args.cid.ToString());
                                        args.hostname = "N" + hubid.ToString();
                                    }
                                }
                                else
                                {
                                    // command for a 2000 behind a 3000 that is not connected?
                                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "19", "When looking for 3000 fronting a 2000, did not find it. NID was " + hubid.ToString(), args.cid.ToString());
                                }
                            }
                            //
                            // else the 2000 is behind or is a 2000 hub which will get connected to
                            //
                        }


                        // make new controller the active one
                        ac.activehost.Replace(ac.activehost.ToString(), args.hostname);
                        ac.activecid = args.cid;
                        ac.connection_type = args.connectiontype;
                        // update 2000 related variables if now talking to 2000
                        if (args.adr != "3000")
                        {
                            ac.activeadr.Replace(ac.activeadr.ToString(), args.adr);
                            ac.activemultiplier = ac.controllers.Find(x => x.CID == ac.activecid).TransportTimerMultiplier;
                            ac.activeDBaddon = ac.controllers.Find(x => x.CID == ac.activecid).dbTimerAddon;
                            ac.activeRepeater = ac.controllers.Find(x => x.CID == ac.activecid).repeaterInUse;
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "201", "Switching controllers/connections, Legacy Transport Timer Multiplier now " + ac.activemultiplier.ToString() +
                            " , system has a repeater? " + ac.activeRepeater.ToString() +
                            " , additional timer in DB is " + ac.activeDBaddon.ToString(), ac.activecid.ToString());
                        }
                        else
                        {
                            ac.activeadr.Replace(ac.activeadr.ToString(), "3000");    // 3000
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "201", "Switching controllers/connections to a 3000", ac.activecid.ToString());

                        }
                        // connect to new controller if this is a 2000 environment
                        if (args.adr != "3000")
                        {
                            if (!ac.controllers.Find(x => x.CID == args.cid).imBehind3000)
                            {
                                ac.NeedsReconnect = false;  // assume will work
                            }
                            if (!ac.controllers.Find(x => x.CID == args.cid).imBehind3000 && !ConnectController(ac))
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Error, "201", "Could not connect to controller when switching from one to another.", ac.activecid.ToString());
                                // we couldn't connect, so flush pending messages
                                // there can be several different controllers with things queued up, in the case of an LR hub

                                // mark complete with error any tasks queued up
                                if (ac.queued_functions.Count > 0)
                                {
                                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "190", "Ending other jobs queued for same host/IP", ac.activecid.ToString());
                                    for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                                    {
                                        Tuple<int, Action<ActionArgs>, ActionArgs> tf = ac.queued_functions[i];
                                        if (tf.Item3.hostname == args.hostname)
                                        {
                                            ac.queued_functions.RemoveAt(i);
                                            if (tf.Item3.jobid > 0)
                                            {
#pragma warning disable 4014
                                                if (tf.Item3.adr == "3000")
                                                {
                                                    PeriodicTasks.CompleteJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                                                }
                                                else
                                                {
                                                    Misc.AddET2000Alert(_cts, tf.Item3.cid, "Unable to connect to the controller", true);
                                                    LegacyTasks.CompleteLegacyJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                                                }
#pragma warning restore 4014
                                            }
                                        }
                                    }
                                }
                                if (ac.queued_functions.Count > 0)
                                {
                                    _log.WriteMessageWithId(NLog.LogLevel.Info, "190", "Still some queued functions to work on, looking for next one", ac.activecid.ToString());
                                    // if there are still functions left to process, give next one a try
                                    goto recycle;
                                }
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "190", "No remaining functions, so ending this connection attempt.", ac.activecid.ToString());
                                // clear the lists of functions
                                ac.NeedsReconnect = true;
                                ac.acsem.Release();
                                LegacyConnectionTracker.totalThrottleFrees++;
                                LegacyConnectionTracker.throttle.Release();
                                return;
                            }
                        }
                    }
                    else if(ac.inbound2000)
                    {
                        // switch to this 2000 on an Inbound 2000 connection that is already established
                        ac.activecid = args.cid;
                        ac.activeadr.Replace(ac.activeadr.ToString(), args.adr);
                        ac.activemultiplier = ac.controllers.Find(x => x.CID == ac.activecid).TransportTimerMultiplier;
                        ac.activeDBaddon = ac.controllers.Find(x => x.CID == ac.activecid).dbTimerAddon;
                        ac.activeRepeater = ac.controllers.Find(x => x.CID == ac.activecid).repeaterInUse;
                        _log.WriteMessageWithId(NLog.LogLevel.Debug, "202", "Inbound 2000 Controller, Legacy Transport Timer Multiplier now " + ac.activemultiplier.ToString() +
                        " , system has a repeater? " + ac.activeRepeater.ToString() +
                        " , additional timer in DB is " + ac.activeDBaddon.ToString(), ac.activecid.ToString());
                    }
                    else if (ac.activecid != 0 && args.cid != ac.activecid)
                    {
                        // changing between controllers on existing connection
                        // update to the new valiues including controller address
                        ac.activecid = args.cid;
                        if (args.adr != "3000")
                        {
                            ac.activeadr.Replace(ac.activeadr.ToString(), args.adr);
                            ac.activemultiplier = ac.controllers.Find(x => x.CID == ac.activecid).TransportTimerMultiplier;
                            ac.activeDBaddon = ac.controllers.Find(x => x.CID == ac.activecid).dbTimerAddon;
                            ac.activeRepeater = ac.controllers.Find(x => x.CID == ac.activecid).repeaterInUse;
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "202", "Switching controllers, Legacy Transport Timer Multiplier now " + ac.activemultiplier.ToString() +
                            " , system has a repeater? " + ac.activeRepeater.ToString() +
                            " , additional timer in DB is " + ac.activeDBaddon.ToString(), ac.activecid.ToString());
                        }
                        else
                        {
                            // indicate a 3000 is active
                            ac.activeadr.Replace(ac.activeadr.ToString(), "3000");
                        }
                    }
// pragma to remove build warning about not await these "fire and forget tasks"
#pragma warning disable 4014
                    // set the task active flag
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "999", "TASK Start", args.cid.ToString());
                    ac.TaskActive = true;
                    LegacyConnectionTracker.totalTaskActiveSets++;

                    // fire up task with this work
                    Task.Run(() => func(args), _cts.Token).ConfigureAwait(false);
                    // for 3000 controllers, start the poll state as we are transmitting a message
                    if(args.adr == "3000")
                    {
                        ac.controllers3000.Find(x => x.NID == args.cid).pollstate = 1;
                        ac.pollTimeout = HUB_POLL_TIMEOUT;
                    }
                    else
                    {
                        ac.pollTimeout = -1;    // no poll timeout if talking to a 2000, the transport layer will take care of that.
                    }
                    // remove from list
                    ac.queued_functions.RemoveAt(idx_to_work);                    
                }
                ac.acsem.Release();
            }
            catch(OperationCanceledException)
            {
                // program exiting
                return;
            }
            catch(SocketException)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Warn, "202", "Socket exception during close.", ac.activecid.ToString());
                ac.NeedsReconnect = true;   // needs a connection if used again
                // error during socket closure
                ac.acsem.Release(); // in case not already performed
                // give up semaphore
                LegacyConnectionTracker.throttle.Release();
                LegacyConnectionTracker.totalThrottleFrees++;
                goto reenter;
            }
            catch(IOException)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Warn, "202", "Socket exception during close.", ac.activecid.ToString());
                ac.NeedsReconnect = true;   // needs a connection if used again
                // error during socket closure
                ac.acsem.Release(); // in case not already performed
                // give up semaphore
                LegacyConnectionTracker.throttle.Release();
                LegacyConnectionTracker.totalThrottleFrees++;
                goto reenter;
            }
        }

        //
        // will only get here for UseHubType case 1,3,4,5,7,8,9,10
        // and not for:
        // 0 - no hub
        // 2 - using legacy SR hub (since only 2000 non-interfering controllers)
        // 6 - is legacy SR hub
        //

        public static ActiveLegacyConnection AddOrReuseConnection(String hostname, UInt32 controller_id, UInt32 connection_type, String adr, UInt32 SiteID, int Multiplier, int Hubtype, Boolean InternalOnly, bool RepeaterInUse, int DBTimerAddon)
        {
            ActiveLegacyConnection ac = null;
            Boolean connection_required = false;
            int invalid_arg = 0;
            String key = null;
            Boolean is3000present = false;
            Boolean failed = true;
            uint hubid = 0;
            Boolean giveupearly = false;
           
            if (connection_type != 5 && connection_type != 7) invalid_arg = -1;
            if (controller_id == 0) invalid_arg = -2;
            // we only care about hostname for non Inbound 2000 hubs
            if (Hubtype == 5 && hostname == null) invalid_arg = -3;
            if (Hubtype == 5 && hostname.Length < 7) invalid_arg = -4;
            if (adr == null) invalid_arg = -5;
            if (adr.Length != 3) invalid_arg = -6;  // legacy controllers use 3 ascii character addresses
            if (SiteID <= 0) invalid_arg = -7;   // must have a site id

            if(invalid_arg != 0)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "9", "Attempt to connect to a legacy controller using invalid IP/name/network_type/address. Error = " + invalid_arg.ToString(), controller_id.ToString());
                return (null);
            }

            // UseHubType types:
            // XX 0 or NULL: Controller is not using a hub to communicate.
            // 1: Controller using Legacy LR hub.
            // XX 2: Controller using Legacy SR hub.
            // 3: Controller using CS3000 LR hub.
            // 4: Controller using CS3000 SR hub.
            // 5: Controller is an LR - HUB
            // XX 6: Controller is an SR - HUB
            // 7: Controller is a CS3000 LR HUB
            // 8: Controller is a CS3000 SR HUB
            // 9: Controller is an Inbound 2000 hub
            // 10: Controller is a 2000 behind an Inbound 2000 hub

            if(Hubtype == 3 || Hubtype == 4 || Hubtype == 7 || Hubtype == 8)
            {
                is3000present = true;
            }

            if(Hubtype == 1 || Hubtype == 5 || Hubtype == 3 || Hubtype == 7)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Info, "9", "This controller attached via/is LR hub, queueing around company ID.", controller_id.ToString());
                // for a legacy RF Hub, we group/queue by Site ID
                key = SiteID.ToString();
                hubid = HubWrapper.GetHubID(controller_id, 0, _cts);
            }
            else if(Hubtype == 4)
            {
                hubid = HubWrapper.GetHubID(controller_id, 0, _cts);
                _log.WriteMessageWithId(NLog.LogLevel.Info, "9", "This controller using 3000 SR hub based queueing.", controller_id.ToString());
                // controller using 3000 SR hub, determine network ID of that hub
                ServerDB db = new ServerDB(_cts);
                AdsConnection conn = db.OpenDBConnectionNotAsync(_log);
                if (conn != null)
                {
                    var x = (Object)null;
                    try
                    {

                        AdsCommand cmd = new AdsCommand("SELECT HubID from Controllers where Deleted = false AND ControllerID = " + controller_id.ToString(), conn);

                        _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);

                        x = cmd.ExecuteScalar();
                        cmd.Unprepare();
                        if (x != null)
                        {
                            key = "N" + Convert.ToInt32(x).ToString();
                        }
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "14391", "Database exception during Hub ID query: " + e.ToString());
                    }
                    db.CloseDBConnection(conn, _log);
                    if(x == null)
                    {
                        return (null);
                    }
                }
                else
                {
                    return (null);
                }
            }
            else if (Hubtype == 9 || Hubtype == 10)
            {
                hubid = HubWrapper.GetHubID(controller_id, 0, _cts);
                
                if (hubid != 0)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "9", "This controller using 2000 Inbound hub based queueing.", controller_id.ToString());
                    key = "E2000-" + hubid.ToString();
                }
                else
                {
                    return (null);
                }
            }
            else
            {
                // don't think we'll ever get here, but this is the case where
                // controller is 3000 SR Hub situation               
                _log.WriteMessageWithId(NLog.LogLevel.Info, "9", "2000 Routine called for 3000 SR hub??", controller_id.ToString());
                return (null);
            }

            // grab the mutex while we mess with things so two players don't try adding
            // same connection 
            try
            {
                _sem.Wait(_cts.Token);

                // if we already have a connection entry for this host, don't need a new object
                if (!HubTrackingTable.TryGetValue(key, out ac))
                {
                    if (is3000present && !InternalOnly)
                    {
                        // for any connection attempt to a 3000 connected controller, the controller
                        // establishes connection and the ActiveLegacyConnection will already be 
                        // present and constructed.
                        // if it is not present we cannot talk to controller'
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "199", "Attempt to communicate with controller but no current connection from controller/hub.", controller_id.ToString());
                        _sem.Release();
                        return (null);
                    }
                    else
                    {
                        if(Hubtype == 9 || Hubtype == 10)
                        {
                            // for any connection attempt to a 2000 Inbound connected controller, the controller
                            // establishes connection and the ActiveLegacyConnection will already be 
                            // present and constructed.
                            // if it is not present we cannot talk to controller'
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "199", "Attempt to communicate with controller but no current connection from Inbound 2000 controller/hub.", controller_id.ToString());
                            _sem.Release();
                            return (null);
                        }
                        // 2000 controller
                        HubTrackingTable.TryAdd(key, new ActiveLegacyConnection(hostname, connection_type, SiteID));
                        HubTrackingTable.TryGetValue(key, out ac);
                        if(ac != null)
                        {
                            ac.key = key;
                            ac.hubhandler = true;
                            ac.activeadr = new StringBuilder(adr);
                            ac.activehost = new StringBuilder(hostname);
                            if (!InternalOnly)
                            {
                                ac.AddReuseGoodToGo = false;
                                // we know we need a connection, we just created this object fresh
                                connection_required = true;
                                ac.controllers.Add(new ActiveLegacyConnection.ControllerInfo<UInt32, Int32, String, int>(controller_id, 0, hostname, Multiplier, Hubtype, RepeaterInUse, DBTimerAddon));    // this controller is on the connection, with unknown version number
                            }
                        }


                    }
                }
                else
                {
                    ac.AddReuseGoodToGo = false;
                    // existing connection, see if we already know this controller is on this connection
                    if (!ac.controllers.Any(x => x.CID == controller_id))
                    {
                        // no, so add it
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Adding legacy controller to existing queuing/connection.", controller_id.ToString());
                        ac.controllers.Add(new ActiveLegacyConnection.ControllerInfo<UInt32, Int32, String, int>(controller_id, 0, hostname, Multiplier, Hubtype, RepeaterInUse, DBTimerAddon));    // new controller on existing connection
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Legacy Transport Timer multiplier is " + Multiplier.ToString() +
                        " , system has a repeater? " + RepeaterInUse.ToString() +
                        " , additional timer in DB is " + DBTimerAddon.ToString(), controller_id.ToString());
                    }
                    if (!is3000present && ac.NeedsReconnect && !InternalOnly && !ac.inbound2000)
                    {
                        // re-using, and needs to re-connect
                        connection_required = true;
                        ac.NeedsReconnect = false;
                        // can't set active variables yet, as we could be talking to a 2000 behind a 3000 LR hub for same company
                    }
                    // see if connection to hub is up
                    if(is3000present && hubid > 0)
                    {
                        if(ac.controllers3000.Exists(x => x.NID == hubid))
                        {
                            if(ac.controllers3000.Find(x => x.NID == hubid).sock_3000.Connected)
                            {
                                ac.AddReuseGoodToGo = true;
                            }
                            else
                            {
                                // there is a 3000 hub for this controller, and it is not connected
                                // therefore we cannot talk to this controller until that 3000 hub calls in
                                giveupearly = true;
                            }
                        }
                    }
                    // see if Inbound 2000 connection is up
                    if(Hubtype == 9 || Hubtype == 10)
                    {
                        if(ac.controllers.Exists(x => x.CID == hubid))
                        {
                            if(ac.sock.Connected)
                            {
                                ac.AddReuseGoodToGo = true;
                            }
                            else
                            {
                                // see above
                                giveupearly = true;
                            }
                        }
                    }
                }

                // done manipulating dictionary/list
                _sem.Release();
            }
            catch (OperationCanceledException)
            {
                // program exiting
                return (null);
            }

            if(giveupearly)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Warn, "199", "3000/2000 inbound hub not connected, so not processing this connection.", controller_id.ToString());
                return (null);
            }

            if(InternalOnly)
            {
                return (ac);
            }

            // if there is a code download in progress, don't queue up new stuff
            if(ac.codedownloadinprogress)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Warn, "199", "Code download in progress, not processing new commands.", controller_id.ToString());
                return (null);
            }

            // this connection required will only happen in a pure 2000 situation
            if(connection_required)
            {
                // if this host has had connection problems recently, don't even bother until
                // timeout expires
                DateTime when;
                if (LegacyConnectionTracker.LockoutTable.TryGetValue(ac.activehost.ToString(), out when))
                {
                    // this host may be locked out, see if that is the case and bail if so
                    if(DateTime.Now < when)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "199", "Not attempting connection as this controller recently failed connection attempt.", controller_id.ToString());
                        ac.acsem.Wait(_cts.Token);
                        // mark complete with error any tasks queued up
                        if (ac.queued_functions.Count > 0)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "190", "Since no connection made, ending other jobs queued for same host/IP", controller_id.ToString());
                            for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                            {
                                Tuple<int, Action<ActionArgs>, ActionArgs> tf = ac.queued_functions[i];
                                if (tf.Item3.hostname == ac.activehost.ToString())
                                {
                                    ac.queued_functions.RemoveAt(i);
                                    if (tf.Item3.jobid > 0)
                                    {
#pragma warning disable 4014
                                        if (tf.Item3.adr == "3000")
                                        {
                                            PeriodicTasks.CompleteJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                                        }
                                        else
                                        {
                                            Misc.AddET2000Alert(_cts, tf.Item3.cid, "Unable to connect to the controller", true);
                                            LegacyTasks.CompleteLegacyJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                                        }
#pragma warning restore 4014
                                    }
                                }
                            }
                        }
                        ac.NeedsReconnect = true;
                        ac.acsem.Release();
                        // if there are still pending functions queued on this group, we need to kick start them now
                        if (ac.queued_functions.Count > 0)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "190", "Still some other jobs queued, so giving them a chance to go after no connection.", controller_id.ToString());
                            RemoveTrackingEntryIfDone(ac);
                        }
                        return (null);
                    }
                }

                // use the semaphore on this connection to hold off other messages
                // until connection completes (may be several seconds for a radio controller)
                ac.acsem.Wait(_cts.Token);
                ac.NeedsReconnect = false;  // assume connect will work
                if(ac.TaskActive)
                {
                    failed = false;
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Task active already on group, allowing " + controller_id.ToString() + " to queue up job.", controller_id.ToString());
                }
                else if (ac.sock == null || !ac.sock.Connected)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Calling initial connect for" + controller_id.ToString(), controller_id.ToString());
                    if (ac.activehost == null) ac.activehost = new StringBuilder("dummy");
                    if (ac.activeadr == null) ac.activeadr = new StringBuilder("dummy");
                    ac.activehost.Replace(ac.activehost.ToString(), hostname);
                    ac.activeadr.Replace(ac.activeadr.ToString(), adr);
                    ac.activecid = controller_id;
                    ac.activemultiplier = Multiplier;
                    ac.connection_type = connection_type;
                    ac.activeRepeater = RepeaterInUse;
                    ac.activeDBaddon = DBTimerAddon;
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Legacy Transport Timer multiplier is " + Multiplier.ToString() +
                        " , system has a repeater? " + RepeaterInUse.ToString() +
                        " , additional timer in DB is " + DBTimerAddon.ToString(), controller_id.ToString());

                    if (CallConnectSub(ac)) failed = false;
                }
                else
                {
                    failed = false;
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "No connect call for " + controller_id.ToString() + " needed as already connected. ", controller_id.ToString());
                }
                ac.acsem.Release();
            }
            else
            {
                // it's possible on slow connections (say radio) for connection to take several seconds
                // to establish ... when multiple commands to the same controller occur, we need to
                // make sure the first one that is establishing the connection happens before the
                // other commands try to go - otherwise, they can try and go out before the connection
                // is even made.
                bool kick = false;
                ac.acsem.Wait(_cts.Token);
                if (!is3000present && Hubtype != 9 && Hubtype != 10)
                {
                    // if we didn't actually get a connection via the original attempt, we can try the connection for this call
                    if (ac.TaskActive == false && (ac.sock == null || !ac.sock.Connected))
                    {
                        if (ac.activehost.ToString() != hostname || (DateTime.Now - ac.lastConnectattempt).TotalSeconds > 299)
                        {
                            // don't bother trying to connect if the connection we are waiting on is the same that just failed, unless enough time has gone by
                            connection_required = true;
                            if (ac.activehost == null) ac.activehost = new StringBuilder("dummy");
                            if (ac.activeadr == null) ac.activeadr = new StringBuilder("dummy");
                            ac.activehost.Replace(ac.activehost.ToString(), hostname);
                            ac.activeadr.Replace(ac.activeadr.ToString(), adr);
                            ac.activecid = controller_id;
                            ac.connection_type = connection_type;
                            ac.activemultiplier = ac.controllers.Find(x => x.CID == ac.activecid).TransportTimerMultiplier;
                            ac.activeDBaddon = ac.controllers.Find(x => x.CID == ac.activecid).dbTimerAddon;
                            ac.activeRepeater = ac.controllers.Find(x => x.CID == ac.activecid).repeaterInUse;
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Legacy Transport Timer multiplier is " + Multiplier.ToString() +
                            " , system has a repeater? " + RepeaterInUse.ToString() +
                            " . additional timer in DB is " + DBTimerAddon.ToString(), controller_id.ToString());
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Calling secondary connect for " + controller_id.ToString(), controller_id.ToString());
                            ac.NeedsReconnect = false;  // assume connect will work
                            if (CallConnectSub(ac)) failed = false;
                            else kick = true;
                        }
                        else
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "No connect call made for " + controller_id.ToString() + " as too soon or recently failed. ", controller_id.ToString());
                            kick = true;
                        }
                    }
                    else
                    {
                        failed = false;
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "No connect call for " + controller_id.ToString() + " needed as already connected or LR group active.", controller_id.ToString());
                    }
                }
                else
                {
                    failed = false; // a 3000 / 2000 Inbound is involved in this controllers connection, so it will make the connection
                }
                ac.acsem.Release();
                if (kick)
                {
                    if (ac.queued_functions.Count > 0)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "190", "Still some other jobs queued so giving them a chance to go after no connection.", controller_id.ToString());
                        RemoveTrackingEntryIfDone(ac);
                    }
                }
            }
            if (failed) return (null);
            return (ac);
        }

        public static bool CallConnectSub(ActiveLegacyConnection ac)
        {
            // here we wait for a free semaphore entry that could throttle us down
            if (++LegacyConnectionTracker.throttleCurrentWaiters > LegacyConnectionTracker.throttleMaxWaiters)
            {
                LegacyConnectionTracker.throttleMaxWaiters = LegacyConnectionTracker.throttleCurrentWaiters;
                if (LegacyConnectionTracker.throttleMaxWaiters > 1)
                {
                    _log.WriteMessage(NLog.LogLevel.Info, "190", "Maximum throttled connections seen now " + (LegacyConnectionTracker.throttleMaxWaiters - 1).ToString());
                }
            }

            DateTime beginstamp = DateTime.Now;
            try
            {
                // @wb I changed it to -1 for testing
                LegacyConnectionTracker.totalThrottleSets++;
                LegacyConnectionTracker.throttle.Wait(-1, comboCts.Token);   // never wait more than 5 minutes
            }
            catch (OperationCanceledException)
            {
                // someone changing throttle size, go ahead and go
            }
            LegacyConnectionTracker.throttleCurrentWaiters--;
            DateTime endstamp = DateTime.Now;

            TimeSpan semtime = endstamp - beginstamp;
            _log.WriteMessageWithId(NLog.LogLevel.Debug, "9777", "FYI: waited on throttle semaphore for " + semtime.ToString(), ac.activecid.ToString());

            if (!ConnectController(ac))
            {
                // give up semaphore
                LegacyConnectionTracker.totalThrottleFrees++;
                LegacyConnectionTracker.throttle.Release();
                // if we couldn't connect, flush some stuff
                // mark complete with error any tasks queued up
                if (ac.queued_functions.Count > 0)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "190", "Since no connection made, ending other jobs queued for same host/IP", ac.activecid.ToString());
                    for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                    {
                        Tuple<int, Action<ActionArgs>, ActionArgs> tf = ac.queued_functions[i];
                        if (tf.Item3.hostname == ac.activehost.ToString())
                        {
                            ac.queued_functions.RemoveAt(i);
                            if (tf.Item3.jobid > 0)
                            {
#pragma warning disable 4014
                                if (tf.Item3.adr == "3000")
                                {
                                    PeriodicTasks.CompleteJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                                }
                                else
                                {
                                    Misc.AddET2000Alert(_cts, tf.Item3.cid, "Unable to connect to the controller", true);
                                    LegacyTasks.CompleteLegacyJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                                }
#pragma warning restore 4014
                            }
                        }
                    }
                }
                ac.NeedsReconnect = true;
                return (false);
            }
            return (true);
        }

        public static ActiveLegacyConnection GetTrackingEntry(String hostname, UInt32 SiteID)
        {
            ActiveLegacyConnection ac;
            uint cid;

            // if hostname is an integer, we have an Inbound 2000 situation
            if(hostname.All(Char.IsDigit))
            {
                if (UInt32.TryParse(hostname, out cid))
                {
                    // key for an Inbound 2000 is E2000-hubnumber, so we need hubnumber for this controller
                    if(HubTrackingTable.Values.Any(x => x.controllers.Exists(y => y.CID == cid)))
                    {
                        if(HubTrackingTable.TryGetValue(HubTrackingTable.Where(x => x.Value.controllers.Exists(y => y.CID == cid)).Max(x => x.Key), out ac))
                        {
                            return (ac);
                        }
                    }
                }
            }
            // hostname is either the key, or for RF hubs, SiteID as a string
            if (HubTrackingTable.TryGetValue(hostname, out ac))
            {
                return (ac);
            }
            if (HubTrackingTable.TryGetValue(SiteID.ToString(), out ac))
            {
                return (ac);
            }
            return (null);  // couldn't find it.
        }

        //
        // called when a 3000 LR or SR hub connects to commserver
        //
        // we create a connection tracking entry, if one doesn't exist,
        // and seed a list of controllers onto the tracking entry based
        // on database.
        //
        // connection will be tracked by IP for an SR hub and Site-ID
        // for an LR hub.
        //
        // hub_type will either be 7 or 8
        //
        public static ActiveLegacyConnection EstablishConnectionTracking(UInt32 nid, UInt32 sn, int hub_type, Socket sock, NetworkStream stream)
        {
            ActiveLegacyConnection ac = null;
            String key;
            Int32 companyID = -1;

            // establish key 
            ServerDB db = new ServerDB(_cts);
            AdsConnection conn = db.OpenDBConnectionNotAsync(_log);
            _log.WriteMessageWithId(NLog.LogLevel.Debug, "199", "Establishing connection tracking for NID " + nid.ToString(), nid.ToString());
            if (conn != null)
            {
                try
                {

                    AdsCommand cmd = new AdsCommand("SELECT s.CompanyID from CS3000Networks n " +
                                                    "JOIN ControllerSites s ON s.SiteID=n.NetworkID " + 
                                                    "where s.Deleted = false AND NetworkID = " + nid.ToString(), conn);

                    _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);

                    var x = cmd.ExecuteScalar();
                    cmd.Unprepare();
                    if (x != null)
                    {
                        companyID = Convert.ToInt32(x);
                    }
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "14391", "Database exception during Company ID query: " + e.ToString());
                }
                db.CloseDBConnection(conn, _log);
            }

            if(companyID == -1)
            {
                return(null);  // couldn't find company ID for this hub
            }

            // LR hub uses Company ID as key
            if (hub_type == 7)
            {
                key = companyID.ToString();
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "199", "LR hub uses key of " + key.ToString(), nid.ToString());
            }
            // SR hub uses network ID to make a key
            else
            {
                // make key unique from any company ID so we can't overlap
                key = "N" + nid.ToString();
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "199", "SR hub uses key of " + key.ToString(), nid.ToString());
            }

            // see if tracking entry already present
            // grab the mutex while we mess with things so two players don't try adding
            // same connection 
            try
            {
                _sem.Wait(_cts.Token);

                // if we already have a tracking entry for this hub and we're grouping by network ID, it must be stale, so get rid of it and make a new one.
                if (HubTrackingTable.TryGetValue(key, out ac))
                {
                    ActiveLegacyConnection tc;

                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "199", "We have an existing tracking entry", nid.ToString());

                    if (hub_type == 8)
                    {

                        _log.WriteMessageWithId(NLog.LogLevel.Debug, "199", "Since SR hub, we will remove old tracking (solo) entry.", nid.ToString());

                        HubTrackingTable.TryRemove(ac.key, out tc);
                        if (tc == null)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "19", "Couldn't find expected hub tracking entry for key: " + ac.key.ToString(), nid.ToString());
                        }

                        // mark complete with error any tasks queued up
                        if (ac.queued_functions.Count > 0)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "190", "Since connection re-established, ending " + ac.queued_functions.Count.ToString() + " other jobs queued for same connection.", nid.ToString());
                            for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                            {
                                Tuple<int, Action<ActionArgs>, ActionArgs> tf = ac.queued_functions[i];
                                if (tf.Item3.jobid > 0)
                                {
#pragma warning disable 4014
                                    if (tf.Item3.adr == "3000")
                                    {
                                        PeriodicTasks.CompleteJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                                    }
                                    else
                                    {
                                        Misc.AddET2000Alert(_cts, tf.Item3.cid, "Unable to connect to the controller", true);
                                        LegacyTasks.CompleteLegacyJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                                    }
#pragma warning restore 4014
                                }
                            }
                        }
                        // clear the lists of controllers/functions
                        ac.controllers.Clear();
                        ac.queued_functions.Clear();
                        ac = null;
                    }
                }
                
                // create new if needed
                if (ac == null)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "199", "Adding new tracking entry with key " + key.ToString(), nid.ToString());
                    HubTrackingTable.TryAdd(key, new ActiveLegacyConnection(key, (uint)1, (uint)companyID));
                    HubTrackingTable.TryGetValue(key, out ac);
                    if (ac != null)
                    {
                        // for a brand new connection object, initialize vars
                        ac.sock = sock;
                        ac.net_stream = stream;
                        ac.key = key;
                        // the active controller will be this first one 
                        ac.activehost = new StringBuilder("N" + nid.ToString());
                        ac.activeadr = new StringBuilder("3000");
                        ac.activecid = 0;
                        ac.activemultiplier = 0;
                        ac.hubhandler = true;
                        ac.activeDBaddon = 0;
                        ac.activeRepeater = false;
                    }
                    else
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "199", "Unable to add entry!?", nid.ToString());
                    }
                }
                if (ac != null)
                {
                    ac.acsem.Wait(_cts.Token);
                    // see if this controller already exists on the connection. i guess that could happen in a LR hub if
                    // one of the grouped hubs went away and came back. in that case we don't need to re-add it.
                    if (!ac.controllers3000.Any(x => x.NID == nid))
                    {
                        var new3000 = new ActiveLegacyConnection.Controller3000Info<UInt32, String>((uint)nid, "N" + nid.ToString());
                        new3000.myHubID = nid;      // the hub's hub is himself
                        new3000.imAhub = true;      // yes, the hub is a hub
                        new3000.sn = sn;
                        new3000.net_stream3000 = stream;
                        new3000.sock_3000 = sock;
                        new3000.pollstate = 1;      // assume we have issued a poll, as this controller should end intial blast with No-more-messages
                        if (ac.pollTimeout <= 0) ac.pollTimeout = HUB_POLL_TIMEOUT;   // if not already polling someone, assume we are the active poll 
                        else new3000.ignoreNextNoMoreMessages = true;       // connected while already in a poll sequence, ignore initial poll
                        new3000.needsHublist = true;   // we need to send this hub a hub list
                        ac.controllers3000.Add(new3000);
                    }
                    else
                    {
                        // update existing controller variables to new data we know about now
                        var existing3000 = ac.controllers3000.Find(x => x.NID == nid);
                        existing3000.myHubID = nid;
                        existing3000.imAhub = true;
                        existing3000.sn = sn;
                        existing3000.net_stream3000 = stream;
                        existing3000.sock_3000 = sock;
                        existing3000.pollstate = 1;
                        existing3000.needsHublist = true;   // we need to send this hub a hub list
                        if (ac.pollTimeout > 0) existing3000.ignoreNextNoMoreMessages = true;   // connected while in middle of poll sequence ignore initial poll
                        // we'll leave mobile state/duration alone
                    }
                    ac.acsem.Release();
                }
                // done manipulating dictionary/list
                _sem.Release();
            }
            catch (OperationCanceledException)
            {
                // program exiting
                return (null);
            }

            // query for 3000 series controllers connected via this hub and add to the tracking entry
            conn = db.OpenDBConnectionNotAsync(_log);

            if (conn != null)
            {
                List<uint> nids = new List<uint>();   // we will build this list of 3000 series controllers based on query results
                List<uint> cids = new List<uint>();     // 2000 series controllers
                try
                {
                    // query all the controllers we need to build a job for
                    AdsCommand cmd = new AdsCommand("SELECT NetworkID, MasterSerialNumber from CS3000Networks n join controllersites s on s.siteid=n.networkid WHERE s.deleted = false and n.HubID = " + nid.ToString(), conn);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);
                    AdsDataReader rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        ac.acsem.Wait(_cts.Token);

                        var thisone = 0;
                        var thissn = 0;
                        while (rdr.Read())
                        {
                            if (!rdr.IsDBNull(0)) thisone = rdr.GetInt32(0);
                            if (!rdr.IsDBNull(1)) thissn = rdr.GetInt32(1);

                            if (thisone != 0)
                            {
                                if (!ac.controllers3000.Any(x => x.NID == thisone))
                                {
                                    var new3000 = new ActiveLegacyConnection.Controller3000Info<UInt32, String>((uint)thisone, "N" + nid.ToString());
                                    new3000.myHubID = nid;
                                    new3000.sn = (uint)thissn;
                                    new3000.sock_3000 = sock;
                                    new3000.net_stream3000 = stream;
                                    new3000.ignoreUntilHubListAcked = true;
                                    ac.controllers3000.Add(new3000);
                                    _log.WriteMessage(NLog.LogLevel.Info, "199", "Noting that controller " + thisone.ToString() + " is on this connection behind hub.");
                                }
                                else
                                {
                                    // controller already exists, we need to update its socket/stream
                                    ac.controllers3000.Find(x => x.NID == thisone).net_stream3000 = stream;
                                    ac.controllers3000.Find(x => x.NID == thisone).sock_3000 = sock;
                                    ac.controllers3000.Find(x => x.NID == thisone).myHubID = nid;
                                    ac.controllers3000.Find(x => x.NID == thisone).sn = (uint)thissn;
                                    ac.controllers3000.Find(x => x.NID == thisone).ignoreUntilHubListAcked = true;
                                    // set the poll time so slaves poll soon after we expect initial message finish from master hub
                                    switch(ac.controllers3000.Find(x => x.NID == thisone).mobilestate)
                                    {
                                        case 2:
                                            ac.controllers3000.Find(x => x.NID == thisone).pollstate = HUB_POLL_MOBILE_HIGH - 30 < 1 ? 1 : HUB_POLL_MOBILE_HIGH - 10;
                                            break;
                                        case 1:
                                            ac.controllers3000.Find(x => x.NID == thisone).pollstate = HUB_POLL_MOBILE_MED - 30 < 1 ? 1 : HUB_POLL_MOBILE_MED - 10;
                                            break;
                                        default:
                                            ac.controllers3000.Find(x => x.NID == thisone).pollstate = HUB_POLL_STANDARD - 30 < 1 ? 1 : HUB_POLL_STANDARD - 10;
                                            break;
                                    }

                                    _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Updating controller " + thisone.ToString() + " to new socket info behind hub.", thisone.ToString());
                                }
                            }
                        }
                        ac.acsem.Release();
                    }
                    rdr.Close();
                    cmd.Unprepare();
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "14394", "Database exception during Hub/NID/CID query: " + e.ToString());
                }

                db.CloseDBConnection(conn, _log);

            }
            return ac;
        }

        private static void QueueUpHubList(ActiveLegacyConnection ac, uint nid)
        {
            if (ac != null)
            {
                // queue up a hub list message to go out
                ac.acsem.Wait(_cts.Token);
                // queue up a Hub List message
                ActionArgs args = new ActionArgs();
                args.adr = "3000";    // not used for a 3000
                args.cid = nid;
                args.connectiontype = 1;    // 5 or 7 for 2000's, 1 indicates a 3000
                args.hostname = "N" + ac.controllers3000.Find(x => x.NID == nid).myHubID.ToString();
                args.timezonekey = 0;   // not used on 3000
                args.jobid = 0;         // not a job based thing
                args.cityid = ac.key[0] == 'N' ? 0 : (uint)Convert.ToInt32(ac.key);    // convert key back to integer
                ac.queued_functions.Add(new Tuple<int, Action<ActionArgs>, ActionArgs>(800, ActionHubList3000, args));  // 800 = very high priority, hub needs this immediately
                // already set above ac.controllers3000.Find(x => x.NID == nid).pollstate = 0; // poll has been queued
                ac.acsem.Release();
            }
        }



        async private static void HubPollLoop()
        {
            int timeout;
            int has_polled = 0;

            while (_cts == null)
            {
                await Task.Delay(50);  // wait for somebody to tell us the cancellation token
            }

            await Task.Delay(2500);       // let everything spin up...

            while (true)
            {
                // for each connection, we can allow one 3000 controller (including hub) to talk
                // at a time. once we poll a controller, we keep polling until it reports
                // that it has nothing else to do.
                // if a hub is busy doing code distribution we will get a BUSY back until
                // that is complete - probably at least 1 hour.

                // loop thru all the connections we are tracking
                // if there are 3000's present
                // see if we are free to poll the next controller
                try
                {
                    // a QUIT signal gets us out early
                    await Task.Delay(Convert.ToInt32(HUB_POLL_LOOP_SECS * 1000), _cts.Token);        // run every X seconds


                    // perform polling logic
                    if (true)
                    {
                        //_sem.Wait(_cts.Token); not needed because ConcurrentDictionary okay with another thread add/removing entry?
                        int entered_loop;
                        entered_loop = 0;
                        foreach (var c in HubTrackingTable)
                        {
                            entered_loop = 1;
                            // see if any cs3000 controllers present, should always be.
                            if (c.Value.controllers3000.Count > 0)
                            {

                                if (c.Value.controllers3000.Exists(x => x.NID == 2529))
                                {
                                    _log.WriteMessageWithId(NLog.LogLevel.Info, "9777", "About to enter HubPollLoop section for test controller", "2529");

                                }
                                DateTime beginstamp = DateTime.Now;
                                if(!c.Value.acsem.Wait(300000, _cts.Token))
                                {
                                    // did not get semaphore in 300000 seconds ... could be a problem?
                                    _log.WriteMessageWithId(NLog.LogLevel.Error, "9777", "Could not obtain group semaphore after 300 seconds. Continuing.", c.Value.activecid.ToString());

                                }
                                DateTime endstamp = DateTime.Now;
                                TimeSpan extratime = endstamp - beginstamp;
                                Int32 extraseconds = Convert.ToInt32(extratime.TotalSeconds);
                                if (extraseconds > 0)
                                {
                                    _log.WriteMessageWithId(NLog.LogLevel.Info, "9777", "FYI: extra wait in poll loop " + extraseconds.ToString() + " seconds.", c.Value.activecid.ToString());
                                }

                                Boolean command_queued = false;
                                Boolean send_hub_list = false;
                                uint send_hub_nid = 0;
                                // for each connection timeout any dead polls
                                if(c.Value.pollTimeout > 0)
                                {

                                    if (c.Value.controllers3000.Exists(x => x.NID == 2529))
                                    {
                                        _log.WriteMessageWithId(NLog.LogLevel.Info, "9777", "Processing poll logic since pollTimeout > 0", "2529");

                                    }

                                    c.Value.pollTimeout -= (HUB_POLL_LOOP_SECS + extraseconds);
                                    if(c.Value.pollTimeout <= 0)
                                    {
                                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "1667", "No end of poll message seen before poll timeout.", c.Value.activecid.ToString() );
                                        if(c.Value.TaskActive)
                                        {
                                            if (c.Value.controllers3000.Exists(x => x.NID == c.Value.activecid))
                                            {
                                                if(c.Value.controllers3000.Exists(x => x.NID == c.Value.controllers3000.Find(y => y.NID == c.Value.activecid).myHubID))
                                                {
                                                    if (c.Value.controllers3000.Find(x => x.NID == c.Value.controllers3000.Find(y => y.NID == c.Value.activecid).myHubID).ResetRxToken != null)
                                                    {
                                                        c.Value.controllers3000.Find(x => x.NID == c.Value.controllers3000.Find(y => y.NID == c.Value.activecid).myHubID).ResetRxToken.Cancel();  // tell receiver to start again
                                                    }
                                                }
                                            }
                                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "1667", "TASK END via Timeout", c.Value.activecid.ToString());
                                            c.Value.TaskActive = false;
                                            LegacyConnectionTracker.totalTaskActiveFrees++;
                                            // call remove tracking entry to step to next possible message
                                            command_queued = true;
                                        }
                                        else
                                        {
                                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "1667", "Strange that poll timeout was active while Task not Active?", c.Value.activecid.ToString());
                                        }
                                        // see who we were polling and clear it for a new poll
                                        if (c.Value.controllers3000.Exists(x => x.NID == c.Value.activecid))
                                        {
                                            c.Value.controllers3000.Find(x => x.NID == c.Value.activecid).pollstate = 1;    // will poll again at next interval
                                            c.Value.controllers3000.Find(x => x.NID == c.Value.activecid).consecutiveErrors++;  // another error
                                            if(c.Value.controllers3000.Find(x => x.NID == c.Value.activecid).consecutiveErrors == (HUB_POLL_CONSEC_ERROR1_COUNT+1))
                                            {
                                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "1667", "3000 controller " + c.Value.activecid.ToString() + " now in 1st degraded polling level due to consecutive errors.", c.Value.activecid.ToString());
                                                c.Value.controllers3000.Find(x => x.NID == c.Value.activecid).mobilestate = 0;  // knock out of mobile if in mobile
                                            }
                                            if (c.Value.controllers3000.Find(x => x.NID == c.Value.activecid).consecutiveErrors == (HUB_POLL_CONSEC_ERROR2_COUNT + 1))
                                            {
                                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "1667", "3000 controller " + c.Value.activecid.ToString() + " now in 2nd degraded polling level due to consecutive errors.", c.Value.activecid.ToString());
                                                c.Value.controllers3000.Find(x => x.NID == c.Value.activecid).mobilestate = 0;  // knock out of mobile if in mobile
                                            }
                                            // if we timed out sending hub list, we need to keep trying
                                            if(c.Value.controllers3000.Find(x => x.NID == c.Value.activecid).needsHublist)
                                            {
                                                send_hub_list = true;
                                                send_hub_nid = c.Value.activecid;
                                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "1667", "3000 hub still needs hub list, will queue up another.", c.Value.activecid.ToString());
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (c.Value.controllers3000.Exists(x => x.NID == 2529))
                                    {
                                        _log.WriteMessageWithId(NLog.LogLevel.Info, "9777", "SKIPPING poll logic since pollTimeout <= 0", "2529");

                                    }
                                }
                                    

                                // look at each controller and see if time to queue up another poll for it
                                foreach(var cont in c.Value.controllers3000)
                                {
                                    // controller has a poll queued up already, but hasn't been sent
                                    if(cont.pollstate == 0)
                                    {
                                        if (cont.NID == 2529)
                                        {
                                            _log.WriteMessageWithId(NLog.LogLevel.Info, "9777", "No poll since one already queued according to pollstate.", "2529");

                                        }
                                    }
                                    else if(cont.ignoreUntilHubListAcked)
                                    {
                                        // can't poll a controller that the hub hasn't been told about yet
                                        if (cont.NID == 2529)
                                        {
                                            _log.WriteMessageWithId(NLog.LogLevel.Info, "9777", "No poll since hub list pending.", "2529");

                                        }
                                    }
                                    else if(c.Value.codedownloadinprogress && cont.NID != c.Value.downloadingNID)
                                    {
                                        // there is a code download in progress, don't poll anyone except the
                                        // hub doing the download
                                        if (cont.NID == 2529)
                                        {
                                            _log.WriteMessageWithId(NLog.LogLevel.Info, "9777", "No poll since download in progress.", "2529");

                                        }
                                    }
                                    else if (cont.mobilestate <= 0 && c.Value.controllers3000.Exists(x => x.mobilestate > 0))
                                    {
                                        // this controller is not in mobile, but there are some that are - so don't poll while
                                        // others in mobile
                                        if (cont.NID == 2529)
                                        {
                                            _log.WriteMessageWithId(NLog.LogLevel.Info, "9777", "No poll since another controller is mobile.", "2529");

                                        }
                                    }
                                    // controller has been sent a poll
                                    else
                                    {
                                        if (cont.NID == 2529)
                                        {
                                            _log.WriteMessageWithId(NLog.LogLevel.Info, "9777", "Ticking pollstate up", "2529");

                                        }
                                        cont.pollstate += HUB_POLL_LOOP_SECS + extraseconds;
                                        switch(cont.mobilestate)
                                        {
                                            case 2: // high
                                                cont.mobileduration += (HUB_POLL_LOOP_SECS + extraseconds);
                                                if(cont.mobileduration > HUB_POLL_MOBILE_HIGH_DURATION)
                                                {
                                                    cont.mobilestate = 1;   // drop down to medium
                                                    cont.mobileduration = 0;    // restart time
                                                }
                                                timeout = HUB_POLL_MOBILE_HIGH;
                                                break;
                                            case 1: // medium
                                                cont.mobileduration += (HUB_POLL_LOOP_SECS + extraseconds);
                                                if(cont.mobileduration > HUB_POLL_MOBILE_MED_DURATION)
                                                {
                                                    cont.mobilestate = 0;   // drop down to standard
                                                    cont.mobileduration = 0;
                                                }
                                                timeout = HUB_POLL_MOBILE_MED;
                                                break;
                                            default: //normal
                                                timeout = HUB_POLL_STANDARD;
                                                if (cont.consecutiveErrors > HUB_POLL_CONSEC_ERROR2_COUNT)
                                                {
                                                    timeout = HUB_POLL_STANDARD * HUB_POLL_ERROR2_SLOW_FACTOR;
                                                }
                                                else if(cont.consecutiveErrors > HUB_POLL_CONSEC_ERROR1_COUNT)
                                                {
                                                    timeout = HUB_POLL_STANDARD * HUB_POLL_ERROR1_SLOW_FACTOR;
                                                }
                                                else
                                                {
                                                    timeout = HUB_POLL_STANDARD;
                                                }
                                                break;
                                        }
                                        if (cont.pollstate > timeout && cont.sock_3000 != null && cont.sock_3000.Connected)
                                        {
                                            if (cont.NID == 2529)
                                            {
                                                _log.WriteMessageWithId(NLog.LogLevel.Info, "9777", "queueing poll.", "2529");

                                            }
                                            // if code download is in progress, we only poll the hub reporting busy, so ignore others
                                            // queue up a poll message to this connected 3000 controller, enough time has gone by
                                            ActionArgs args = new ActionArgs();
                                            args.adr = "3000";    // not used for a 3000
                                            args.cid = cont.NID;
                                            args.connectiontype = 1;    // 5 or 7 for 2000's, 1 indicates a 3000
                                            args.hostname = "N" + cont.myHubID.ToString();
                                            args.timezonekey = 0;   // not used on 3000
                                            args.jobid = 0;         // not a job based thing
                                            args.cityid = c.Value.key[0] == 'N' ? 0 : (uint)Convert.ToInt32(c.Value.key);    // convert key back to integer
                                            // base poll priority is 10
                                            // if controller is in a mobile state, set priority slightly higher so that mobile polls beat out standard polls
                                            // final priority for the poll will be either 10, 11, or 12 which is always lower than anything else sent.
                                            c.Value.queued_functions.Add(new Tuple<int, Action<ActionArgs>, ActionArgs>(10+cont.mobilestate, ActionPoll3000, args));  // 3000 poll priority here. 10 = very low
                                            cont.pollstate = 0; // poll has been queued
                                            command_queued = true;
                                            has_polled = 1;
                                        }
                                        else
                                        {
                                            if (cont.NID == 2529)
                                            {
                                                _log.WriteMessageWithId(NLog.LogLevel.Info, "9777", "Not time yet for another poll, or socket not connected.", "2529");

                                            }
                                        }
                                    }
                                }
                                c.Value.acsem.Release();
                                // if hub list needed, do it
                                if(send_hub_list)
                                {
                                    QueueUpHubList(c.Value, send_hub_nid);
                                    command_queued = true;
                                }

                                // make it go
                                if (command_queued)
                                {
                                    RemoveTrackingEntryIfDone(c.Value);
                                }
                            }
                        }
                        if(entered_loop == 0)
                        {
                            if(has_polled == 1)
                            {
                                // odd - we have polled in the past, and now the tracking table is empty??
                                _log.WriteMessage(NLog.LogLevel.Warn, "1667", "No controllers in hub tracking table?");
                                has_polled = 0;
                            }
                        }
                    }
                }
                catch (TaskCanceledException ex)
                {
                    if (_cts.IsCancellationRequested)
                    {
                        _log.WriteMessage(NLog.LogLevel.Info, "190", "Exiting Hub Poll loop. " + ex.ToString());
                        break;
                    }
                    _log.WriteMessage(NLog.LogLevel.Warn, "190", "*NOTE* This condition would have stopped polling." + ex.ToString());
                }
                catch (SemaphoreFullException ex2)
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "190", "Freeing already released semaphore in Poll Loop " + ex2.ToString());
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "190", "Exception in Poll Loop " + e.ToString());
                }

                // 
            }
        }
        //
        // queue up an IHSFY to hub/hub attached controller
        //
        public static Boolean QueueUpIHSFYHub(uint nid)
        {
            ActiveLegacyConnection ac;
            Boolean didit = false;

            ac = FindACfrom3000NID(nid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for 3000 controller! ", nid.ToString());
            }
            else
            {
                ac.acsem.Wait(_cts.Token);
                // queue up a IHSFY message
                ActionArgs args = new ActionArgs();
                args.adr = "3000";    // not used for a 3000
                args.cid = nid;
                args.connectiontype = 1;    // 5 or 7 for 2000's, 1 indicates a 3000
                args.hostname = "N" + ac.controllers3000.Find(x => x.NID == nid).myHubID.ToString();
                args.timezonekey = 0;   // not used on 3000
                args.jobid = 0;         // not a job based thing
                args.cityid = ac.key[0] == 'N' ? 0 : (uint)Convert.ToInt32(ac.key);    // convert key back to integer
                ac.queued_functions.Add(new Tuple<int, Action<ActionArgs>, ActionArgs>(400, ActionHubIHSFY, args));  // 400 = relatively high
                ac.controllers3000.Find(x => x.NID == nid).pollstate = 0; // poll has been queued
                didit = true;
                ac.acsem.Release();

                // we have added the an outgoing function to the queue, we have to kick start it to make it go
                RemoveTrackingEntryIfDone(ac);
            }

            return (didit);
        }


        public static void TellHubToCheckFirmware(ActiveLegacyConnection ac, uint nid, CancellationTokenSource cts)
        {
            ac.acsem.Wait(cts.Token);
            // queue up a Hub List message
            ActionArgs args = new ActionArgs();
            args.adr = "3000";    // not used for a 3000
            args.cid = nid;
            args.connectiontype = 1;    // 5 or 7 for 2000's, 1 indicates a 3000
            args.hostname = "N" + ac.controllers3000.Find(x => x.NID == nid).myHubID.ToString();
            args.timezonekey = 0;   // not used on 3000
            args.jobid = 0;         // not a job based thing
            args.cityid = ac.key[0] == 'N' ? 0 : (uint)Convert.ToInt32(ac.key);    // convert key back to integer
            ac.queued_functions.Add(new Tuple<int, Action<ActionArgs>, ActionArgs>(13, ActionHubCheckUpdates, args));  // 13 is just higher than a poll priority of 10 or 11 or 12
            ac.acsem.Release();
        }


        //
        // tell hub to check for updates, this one use when slave out of date
        //
        static void ActionHubCheckUpdates(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            UInt32 crc;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Check for Updates to hub controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for 3000 controller! ", args.cid.ToString());
            }
            else
            {
                // get serial number of controller
                var cont = ac.controllers3000.Find(x => x.NID == args.cid);
                if (cont == null)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Couldn't find controller for IHSFY action call ", args.cid.ToString());
                }
                else
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number

                    Misc.Uint32ToNetworkBytes(cont.sn, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(cont.NID, mem);
                    // mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_check_for_updates, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "40", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "28", "Check for Updates to be transmitted to SN:" + cont.sn.ToString(), args.cid.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        //_logger.WriteMessage(NLog.LogLevel.Debug, "3029", "FYI: CRC Bytes swapped to send ");
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);


                    //
                    // spit out data onto network stream
                    // this should elicit 
                    // a) messages from this controller (if any)
                    // b) finally, i have nothing else message
                    forceToEndTxMessage = false;
                    try
                    {
                        ac.net_stream.Write(finalpacket, 0, (int)finalpacket.Length);
                        ac.net_stream.Flush();

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)cont.sn, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)cont.sn, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "75", "Exception during transmit of Check for Updates to hub controller SN:" + cont.sn.ToString() + ", " + e.ToString(), args.cid.ToString());
                        forceToEndTxMessage = true;
                    }
                    mem.Dispose();
                }
            }

            if (forceToEndTxMessage)
            {
                // there is no message to controller, so "end" the task now
                _log.WriteMessageWithId(NLog.LogLevel.Info, "1667", "TASK END", args.cid.ToString());
                ac.TaskActive = false;
                LegacyConnectionTracker.totalTaskActiveFrees++;
                ac.pollTimeout = 0;
                // move to next message if any
                RemoveTrackingEntryIfDone(ac);
            }
        }

        //
        // send IHSFY to hub/hub attached controller
        //
        static void ActionHubIHSFY(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            UInt32 crc;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send IHSFY to hub controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for 3000 controller! ", args.cid.ToString());
            }
            else
            {
                // get serial number of controller
                var cont = ac.controllers3000.Find(x => x.NID == args.cid);
                if(cont == null)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Couldn't find controller for IHSFY action call ", args.cid.ToString());
                }
                else
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number

                    Misc.Uint32ToNetworkBytes(cont.sn, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(cont.NID, mem);
                    // mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_IHSFY_program_data, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "40", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "28", "IHSFY to be transmitted to SN:" + cont.sn.ToString(), args.cid.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        //_logger.WriteMessage(NLog.LogLevel.Debug, "3029", "FYI: CRC Bytes swapped to send ");
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);


                    //
                    // spit out data onto network stream
                    // this should elicit 
                    // a) messages from this controller (if any)
                    // b) finally, i have nothing else message
                    forceToEndTxMessage = false;
                    try
                    {
                        ac.net_stream.Write(finalpacket, 0, (int)finalpacket.Length);
                        ac.net_stream.Flush();

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)cont.sn, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)cont.sn, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "75", "Exception during transmit of IHSFY to hub controller SN:" + cont.sn.ToString() + ", " + e.ToString(), args.cid.ToString());
                        forceToEndTxMessage = true;
                    }
                    mem.Dispose();
                }
            }

            if (forceToEndTxMessage)
            {
                // there is no message to controller, so "end" the task now
                _log.WriteMessageWithId(NLog.LogLevel.Info, "1667", "TASK END", args.cid.ToString());
                ac.TaskActive = false;
                LegacyConnectionTracker.totalTaskActiveFrees++;
                ac.pollTimeout = 0;
                // move to next message if any
                RemoveTrackingEntryIfDone(ac);
            }
        }

        //
        // send list of attached controllers to hub
        //
        static void ActionHubList3000(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            UInt32 crc;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Hub list to hub controller: " + args.cid.ToString(), args.cid.ToString());

            //
            // hostname in 3000 hub environment is Nxxx where xxx is the 3000 hub network ID
            // 
            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for 3000 controller! ", args.cid.ToString());
            }
            else
            {
                // get serial number of controller
                var cont = ac.controllers3000.Find(x => x.NID == args.cid);
                if (cont == null)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Couldn't find controller to pull in connection list. ", args.cid.ToString());
                }
                else
                {

                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number

                    Misc.Uint32ToNetworkBytes(cont.sn, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(cont.NID, mem);
                    // hub list
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_here_is_the_hub_list, mem);

                    ServerDB db = new ServerDB(_cts);
                    AdsConnection conn = db.OpenDBConnectionNotAsync(args.cid.ToString());

                    if (conn != null)
                    {
                        try
                        {
                            // query for 3000 series controllers connected via this hub to put in hub list, not including the hub itself
                            AdsCommand cmd = new AdsCommand("SELECT NetworkID, MasterSerialNumber from CS3000Networks n join controllersites s on s.siteid=n.networkid WHERE s.deleted = false and n.HubID = " + args.cid.ToString(), conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);                 
                            AdsDataReader rdr = cmd.ExecuteReader();
                            if (rdr.HasRows)
                            {
                                uint thisone = 0;
                                uint thissn = 0;
                                while (rdr.Read())
                                {
                                    if (!rdr.IsDBNull(0)) thisone = (uint)rdr.GetInt32(0);
                                    if (!rdr.IsDBNull(1)) thissn = (uint)rdr.GetInt32(1);
                                    if (thisone != 0 && thisone != args.cid)    // exclude hub itself
                                    {
                                        Misc.Uint32ToNetworkBytes(thissn, mem); 
                                        _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Adding 3000 Controller " + thisone.ToString() + " sn " + thissn.ToString() + " to hub list.", args.cid.ToString());
                                    }
                                }
                            }
                            rdr.Close();
                            cmd.Unprepare();
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "14394", "Database exception during Hub/NID/CID query: " + e.ToString(), args.cid.ToString());
                        }
                        db.CloseDBConnection(conn, _log);
                    }

                    // 0xffffffff separator between 3000's and 2000's
                    Misc.Uint32ToNetworkBytes(0xffffffff, mem);

                    // query for 2000 series controllers connected via this hub and add to the tracking entry
                    conn = db.OpenDBConnectionNotAsync(args.cid.ToString());

                    if (conn != null)
                    {
                        try
                        {
                            // query all the 2000 controllers behind this 3000 hub                           
                            AdsCommand cmd = new AdsCommand("SELECT c.Address, c.ControllerID from Controllers c WHERE c.deleted = false and Model < 7 and c.UseHubType in (3,4) and c.HubId = " + args.cid.ToString(), conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);
                            AdsDataReader rdr = cmd.ExecuteReader();
                            if (rdr.HasRows)
                            {
                                String thisadr = "";
                                int thiscid = 0;
                                UInt32 encoded = 0;
                                while (rdr.Read())
                                {
                                    if (!rdr.IsDBNull(0)) thisadr = rdr.GetString(0);
                                    if (!rdr.IsDBNull(1)) thiscid = rdr.GetInt32(1);

                                    if (thiscid != 0 && thisadr.Length > 1)
                                    {
                                        encoded = (UInt32)thisadr[0];
                                        encoded |= ((UInt32)thisadr[1]) << 8;
                                        encoded |= ((UInt32)thisadr[2]) << 16;
                                        Misc.Uint32ToNetworkBytes(encoded, mem);
                                        _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Adding 2000 Controller " + thiscid.ToString() + " adr " + thisadr.ToString() + " to hub list.", args.cid.ToString());
                                    }
                                }
                            }
                            rdr.Close();
                            cmd.Unprepare();
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "14394", "Database exception during Hub/NID/CID query: " + e.ToString(), args.cid.ToString());
                        }
                        db.CloseDBConnection(conn, _log);
                    }

                    _log.WriteMessageWithId(NLog.LogLevel.Info, "9771", "Build list for Hub.", args.cid.ToString());

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "40", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "28", "Hub list to be transmitted to SN:" + cont.sn.ToString(), args.cid.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        //_logger.WriteMessage(NLog.LogLevel.Debug, "3029", "FYI: CRC Bytes swapped to send ");
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);


                    //
                    // spit out data onto network stream
                    // this should elicit 
                    // a) messages from this controller (if any)
                    // b) finally, i have nothing else message
                    forceToEndTxMessage = false;
                    try
                    {
                        ac.net_stream.Write(finalpacket, 0, (int)finalpacket.Length);
                        ac.net_stream.Flush();

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)cont.sn, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)cont.sn, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "75", "Exception during transmit of list to hub controller SN:" + cont.sn.ToString() + ", " + e.ToString(), args.cid.ToString());
                        forceToEndTxMessage = true;
                        if(e is IOException)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "75", "Giving up trying to send hub list to controller SN:" + cont.sn.ToString(), args.cid.ToString());
                            cont.needsHublist = false;
                        }
                    }
                    mem.Dispose();
                }
            }


            if (forceToEndTxMessage)
            {
                // there is no message to controller, so "end" the task now
                _log.WriteMessageWithId(NLog.LogLevel.Info, "1667", "TASK END", args.cid.ToString());
                ac.TaskActive = false;
                LegacyConnectionTracker.totalTaskActiveFrees++;
                ac.pollTimeout = 0;
                // move to next message if any
                RemoveTrackingEntryIfDone(ac);
            }
        }

#pragma warning disable 1998
        public static async Task<Tuple<List<byte[]>, String, bool, int>> HubListResponse(ClientInfoStruct client, CancellationTokenSource cts)
        {
            byte[] data;
            bool terminate_connection = false;
            List<byte[]> rsplist = new List<byte[]>();
            ActiveLegacyConnection ac = null;
            UInt32 crc;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Processing Hub List Response", client.nid.ToString());

            data = null;

            ac = FindACfrom3000NID(client.nid);

            if (ac != null)
            {
                var mem = new MemoryStream(0);
                // build ack response
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(client.sn, mem);
                // network id
                Misc.Uint32ToNetworkBytes(client.nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_please_send_me_the_hub_list_ack, mem);

                _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "ACKing Hub List request.", client.nid.ToString());

                // get the built response to return and be sent
                data = mem.ToArray();

                // send it immediately so it happens before the next dequeue
                crc = Misc.CRC(data);
                //_log.WriteMessage(NLog.LogLevel.Debug, "40", "Calculated CRC: 0x" + crc.ToString("X"));
                _log.WriteMessage(NLog.LogLevel.Debug, "28", "ACK to be transmitted to SN:" + client.sn.ToString());

                // combine preamble, data, CRC and then postamble
                byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                byte[] crcbytes;
                crcbytes = System.BitConverter.GetBytes(crc);
                if (BitConverter.IsLittleEndian)
                {
                    //_logger.WriteMessage(NLog.LogLevel.Debug, "3029", "FYI: CRC Bytes swapped to send ");
                    Array.Reverse(crcbytes, 0, crcbytes.Length);
                }
                System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                // get the built response to return and be sent
                data = mem.ToArray();
                rsplist.Add(data);
            }
            else
            {
                // we don't expect any exceptions pulling out registration data, if so, log and return error
                _log.WriteMessage(NLog.LogLevel.Error, "3000", "Error in hub list request hub id " + client.nid.ToString());
                List<byte[]> nulllist = new List<byte[]>();

                return (Tuple.Create(nulllist, (String)null, true, 0));
            }

            // queue up a hub list message to go out
            ac.acsem.Wait(_cts.Token);
            // queue up a Hub List message
            ActionArgs args = new ActionArgs();
            args.adr = "3000";    // not used for a 3000
            args.cid = client.nid;
            args.connectiontype = 1;    // 5 or 7 for 2000's, 1 indicates a 3000
            args.hostname = "N" + ac.controllers3000.Find(x => x.NID == client.nid).myHubID.ToString();
            args.timezonekey = 0;   // not used on 3000
            args.jobid = 0;         // not a job based thing
            args.cityid = ac.key[0] == 'N' ? 0 : (uint)Convert.ToInt32(ac.key);    // convert key back to integer
            ac.queued_functions.Add(new Tuple<int, Action<ActionArgs>, ActionArgs>(390, ActionHubList3000, args));  // 400 = relatively high
            ac.controllers3000.Find(x => x.NID == client.nid).pollstate = 0; // poll has been queued
            ac.acsem.Release();

            // since controller should still be including a "no more messages" as part of his request, 
            // that will end up running the dequeue that will pull this hub list message off queue

            return (Tuple.Create(rsplist, (String)null, terminate_connection, Constants.NO_STATE_DATA));
        }
#pragma warning restore 1998


        //
        // Controller has ACK'd the hub list. we can knock down our flag so we no longer
        // try and send the hub list.
        //
#pragma warning disable 1998
        public static async Task<Tuple<List<byte[]>, String, bool, int>> HubListReceived(ClientInfoStruct client, CancellationTokenSource cts)
        {
            List<byte[]> rsplist = new List<byte[]>();
            ActiveLegacyConnection ac = null;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Processing Hub List Ack", client.nid.ToString());

            ac = FindACfrom3000NID(client.nid);

            if (ac != null)
            {
                if(ac.controllers3000.Exists(x => x.NID == client.nid))
                {
                    ac.controllers3000.Find(x => x.NID == client.nid).needsHublist = false;
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "3000 hub has ACK'd receipt of hub list", client.nid.ToString());
                    // clear the flag blocking comm with 3000's behind this hub
                    foreach(var c in ac.controllers3000)
                    {
                        if(c.myHubID == client.nid)
                        {
                            c.ignoreUntilHubListAcked = false;
                        }
                    }
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "8866", "Couldn't find tracking entry for hub list ack", client.nid.ToString());
                }

            }
            else
            {
                // we don't expect any exceptions pulling out registration data, if so, log and return error
                _log.WriteMessage(NLog.LogLevel.Error, "3000", "Error finding tracking entry." + client.nid.ToString());

            }

            List<byte[]> nulllist = new List<byte[]>();

            return (Tuple.Create(nulllist, (String)null, true, 0));
        }
#pragma warning restore 1998

        // 
        // Poll a 3000 Controller (hub environment)
        // Uses method type similar to all the 2000 messages so it can
        // be held in the same function queue
        //
        static void ActionPoll3000(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            UInt32 crc;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Poll for 3000 hub based controller: " + args.cid.ToString(), args.cid.ToString());

            //
            // hostname in 3000 hub environment is Nxxx where xxx is the 3000 hub network ID
            // 
            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for 3000 controller! ", args.cid.ToString());
            }
            else
            {
                // get serial number of controller
                var cont = ac.controllers3000.Find(x => x.NID == args.cid);
                if (cont == null)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Couldn't find controller to pull in connection list. ", args.cid.ToString());
                }
                else
                {

                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number

                    Misc.Uint32ToNetworkBytes(cont.sn, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(cont.NID, mem);
                    // I Have Something For You, namely Program Data
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_go_ahead_send_your_messages, mem);

                    _log.WriteMessageWithId(NLog.LogLevel.Info, "9771", "Build Hub poll.", args.cid.ToString());

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "40", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "28", "Poll to be transmitted to SN:" + cont.sn.ToString(), args.cid.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        //_logger.WriteMessage(NLog.LogLevel.Debug, "3029", "FYI: CRC Bytes swapped to send ");
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);


                    //
                    // spit out data onto network stream
                    // this should elicit 
                    // a) messages from this controller (if any)
                    // b) finally, i have nothing else message
                    forceToEndTxMessage = false;
                    try
                    {
                        ac.net_stream.Write(finalpacket, 0, (int)finalpacket.Length);
                        ac.net_stream.Flush();

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)cont.sn, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)cont.sn, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "75", "Exception during transmit of Hub poll to controller SN:" + cont.sn.ToString() + ", " + e.ToString());
                        forceToEndTxMessage = true;
                    }
                    mem.Dispose();
                }
            }

            if (forceToEndTxMessage)
            {
                // if we can't generate the poll, move to next message if any
                if(ac != null)
                {
                    // there is no message to controller, so "end" the task now
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "1667", "TASK END", args.cid.ToString());
                    ac.TaskActive = false;
                    LegacyConnectionTracker.totalTaskActiveFrees++;
                    ac.pollTimeout = 0;
                    RemoveTrackingEntryIfDone(ac);
                }
            }
        } 

        public static ActiveLegacyConnection FindACfrom3000NID(uint nid)
        {
            ActiveLegacyConnection ac = null;

            //_sem.Wait(_cts.Token);
            foreach (var c in HubTrackingTable)
            {
                //c.Value.acsem.Wait(_cts.Token);
                if (c.Value.controllers3000.Count > 0)
                {
                    if(c.Value.controllers3000.Exists(x => x.NID == nid))
                    {
                        ac = c.Value;
                    }
                }
                //c.Value.acsem.Release();
            }
            //_sem.Release();

            return (ac);
        }

        public static ActiveLegacyConnection FindACfrom2000NID(uint cid)
        {
            ActiveLegacyConnection ac = null;

            //_sem.Wait(_cts.Token);
            foreach (var c in HubTrackingTable)
            {
                //c.Value.acsem.Wait(_cts.Token);
                if (c.Value.controllers.Exists(x => x.CID == cid))
                {
                    ac = c.Value;
                }
                //c.Value.acsem.Release();
            }
            //_sem.Release();

            return (ac);
        }

        //
        // return the number of times status 10 seen
        //
        public static int HubGetStatus10(uint nid)
        {
            ActiveLegacyConnection ac = null;

            ac = FindACfrom3000NID(nid);

            if (ac != null) return (ac.controllers3000.Find(x => x.NID == nid).Status10Seen);

            return (0);
        }

        public static void HubSetStatus10(uint nid, int val)
        {
            ActiveLegacyConnection ac = null;

            ac = FindACfrom3000NID(nid);

            if (ac != null)
            {
                ac.controllers3000.Find(x => x.NID == nid).Status10Seen = val;
            }

            return;
        }

        //
        // called when a hub has had controller(s) added/removed
        //
        // returns 0 unless error, then returns negative code
        //
        public static int ProcessHubChange(uint nid)
        {
            ActiveLegacyConnection ac = null;
            ActiveController stdac = null;

            ac = FindACfrom3000NID(nid);

            //
            // queue up a pseudo message to disconnect this hub
            //
            // when it reconnects, it will get a new hub list
            // with updated data per database
            //
            // queue up a hub list message to go out
            if (ac != null)
            {
                ac.acsem.Wait(_cts.Token);
                // queue up a Hub List message
                ActionArgs args = new ActionArgs();
                args.adr = "3000";    // not used for a 3000
                args.cid = nid;
                args.connectiontype = 1;    // 5 or 7 for 2000's, 1 indicates a 3000
                args.hostname = "N" + ac.controllers3000.Find(x => x.NID == nid).myHubID.ToString();
                args.timezonekey = 0;   // not used on 3000
                args.jobid = 0;         // not a job based thing
                args.cityid = ac.key[0] == 'N' ? 0 : (uint)Convert.ToInt32(ac.key);    // convert key back to integer
                ac.queued_functions.Add(new Tuple<int, Action<ActionArgs>, ActionArgs>(13, ActionDisconnect, args));  // 13 is just higher than a poll priority of 10,11,12
                ac.acsem.Release();

                // there is no message to controller, so "end" the task now
                _log.WriteMessageWithId(NLog.LogLevel.Info, "1667", "TASK END", nid.ToString());
                ac.TaskActive = false;
                LegacyConnectionTracker.totalTaskActiveFrees++;
                ac.pollTimeout = 0;

                // kick next queued job if ready
                RemoveTrackingEntryIfDone(ac);
                return (0);
            }
            else
            {
                // see if there is a "standard" connection to this nid (for the special case of a non-hub controller
                // being switched to a hub
                stdac = ServerSocketTracker.GetNonHubTrackingEntry(nid);
                if (stdac != null)
                {
                    if(stdac.sock != null)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "1667", "Closing existing connection from standard 3000, as it has become a hub.", nid.ToString());
                        stdac.sock.Shutdown(SocketShutdown.Both);  // ensure all data sent/received
                        stdac.sock.Disconnect(false);
                        return (0);
                    }
                    else
                    {
                        return (-1);
                    }
                }
                else
                {
                    return (-1);
                }
            }
        }

        //
        // Action spun up by Task.Run() above when a legacy hub has a configuration change
        //
        // no message sent, just used internally to update data/objects
        //
        public static int Process2000HubChange(String hostname, uint cityid, uint cid)
        {
            ActiveLegacyConnection ac;
            int ret = -1;
            bool alreadyempty = false;
            bool removedac = false;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Hub Change notification on Legacy Controller: " + cid.ToString(), cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(hostname, cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Info, "987", "Since no connection to controller, nothing to do on hub change.", cid.ToString());
                ret = 0;
            }
            else
            {
                // we may be removing this entry from tracking table, so lock everything
                //_sem.Wait(_cts.Token);
                ac.acsem.Wait(_cts.Token);

                if (ac.controllers.Count == 0 && ac.controllers3000.Count == 0) alreadyempty = true;

                //
                // remove any 2000 jobs queued up, in theory shouldn't be any as hub change very low priority
                //
                for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                {
                    if (ac.queued_functions[i].Item3.adr != "3000")
                    {
                        if (ac.queued_functions[i].Item3.jobid != 0)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "8866", "Removing queued legacy job at 2000 hub change, why is it still queued? Job ID:" + ac.queued_functions[i].Item3.jobid.ToString(), ac.queued_functions[i].Item3.cid.ToString());
                        }
                        else
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Removing queued legacy job at 2000 hub change.", ac.queued_functions[i].Item3.cid.ToString());
                        }
                        ac.queued_functions.RemoveAt(i);
                    }
                }


                // remove 2000 controllers in this queue grouping
                for (int i = ac.controllers.Count - 1; i >= 0; i--)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Per hub change, clearing 2000 controller on this group: " + ac.controllers[i].CID.ToString(), cid.ToString());
                    ac.controllers.RemoveAt(i);
                }

                // remove from tracking table if we emptied it
                if (!alreadyempty && ac.controllers3000.Count == 0 && ac.controllers.Count == 0)
                {
                    ActiveLegacyConnection dummyac;
                    if (!HubTrackingTable.TryRemove(ac.key, out dummyac))
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "3045", "Unexpected error during remove of empty AC object from hub tracking table", cid.ToString());
                    }
                    else
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Removed now empty tracking entry because hub change.", cid.ToString());
                        removedac = true;
                    }
                }

                ac.acsem.Release();
                //_sem.Release();

                // there is no message to controller, so "end" the task now
                _log.WriteMessageWithId(NLog.LogLevel.Info, "1667", "TASK END", cid.ToString());
                ac.TaskActive = false;
                LegacyConnectionTracker.totalTaskActiveFrees++;

                if (ac.queued_functions.Count > 0 && !removedac)
                {
                    // kick next queued job if ready
                    LegacyConnectionTracker.RemoveTrackingEntryIfDone(ac);
                }
                else
                {
                    // give up semaphore for legacy controllers
                    if (LegacyConnectionTracker.throttle != null)
                    {
                        LegacyConnectionTracker.totalThrottleFrees++;
                        LegacyConnectionTracker.throttle.Release();
                    }
                    ac.NeedsReconnect = true;   // needs to reconnect next time using
                }
                ret = 0;
            }
            return (ret);
        }


        public static Boolean IsThisControllerMeOrBehindMe(ActiveLegacyConnection ac, uint hubid, uint cid)
        {
            if (hubid == cid) return (true);

            if(ac.controllers3000.Exists(x => x.NID == cid))
            {
                // cid is a 3000 that has this hub as its hub
                if (ac.controllers3000.Find(x => x.NID == cid).myHubID == hubid) return (true);
            }

            if(ac.controllers.Exists(x => x.CID == cid))
            {
                // cid is a 2000 that is behind a 3000 which has this hubid
                if (ac.controllers.Find(x => x.CID == cid).imBehind3000)
                {
                    if (HubWrapper.GetHubID(cid, 0, _cts) == hubid) return (true);
                }
            }

            return (false);
        }

        static void ActionDisconnect(ActionArgs args)
        {
            ActiveLegacyConnection ac = null;
            Socket s = null;
            bool alreadyempty = false;
            bool removedac = false;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Processing Disconnection to hub since DB change.", args.cid.ToString());

            // called when queue has gotten to this function...
            // the only remaining queued jobs for this controller should just be polls which we don't really care about now

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac != null)
            {
                // we may be removing this entry from tracking table, so lock everything
                //_sem.Wait(_cts.Token);
                ac.acsem.Wait(_cts.Token);

                if (ac.controllers3000.Count == 0 && ac.controllers.Count == 0) alreadyempty = true;

                //
                // remove queued jobs related to this hub (these should be simple polls, nothing to do with them)
                //
                for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                {
                    if (IsThisControllerMeOrBehindMe(ac, args.cid, ac.queued_functions[i].Item3.cid))
                    {
                        if (ac.queued_functions[i].Item3.jobid != 0)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "8866", "Removing queued job at disconnect, why is it still queued? Job ID:" + ac.queued_functions[i].Item3.jobid.ToString(), args.cid.ToString());
                        }
                        else
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Debug, "8866", "Removing queued job at disconnect.", args.cid.ToString());
                        }
                        ac.queued_functions.RemoveAt(i);
                    }
                }


                // remove 2000 controllers related to this hub
                for (int i = ac.controllers.Count - 1; i >= 0; i--)
                {
                    if (IsThisControllerMeOrBehindMe(ac, args.cid, ac.controllers[i].CID))
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Removing 2000 behind 2000, CID " + ac.controllers[i].CID.ToString(), args.cid.ToString());
                        ac.controllers.RemoveAt(i);
                    }
                }

                // save the socket to be able to close it
                if(ac.controllers3000.Exists(x => x.NID == args.cid))
                {
                    s = ac.controllers3000.Find(x => x.NID == args.cid).sock_3000;
                }

                // remove 3000 controllers related to this hub
                for (int i = ac.controllers3000.Count - 1; i >= 0; i--)
                {
                    if (IsThisControllerMeOrBehindMe(ac, args.cid, ac.controllers3000[i].NID))
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Removing 3000 NID " + ac.controllers3000[i].NID.ToString(), args.cid.ToString());
                        ac.controllers3000.RemoveAt(i);
                    }
                }

                // remove from tracking table if we emptied it
                if (!alreadyempty && ac.controllers3000.Count == 0 && ac.controllers.Count == 0)
                {
                    ActiveLegacyConnection dummyac;
                    if (!HubTrackingTable.TryRemove(ac.key, out dummyac))
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "3045", "Unexpected error during remove of empty AC object from hub tracking table", args.cid.ToString());
                    }
                    else
                    {
                        removedac = true;
                    }
                }

                // close the socket
                if(s != null)
                {
                    if(s.Connected)
                    {
                        try
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Closing socket as part of DB change.", args.cid.ToString());
                            s.Shutdown(SocketShutdown.Both);
                            s.Disconnect(false);
                        }
                        catch(Exception e)
                        {
                            if (e is SocketException)
                            {
                                var ex = (SocketException)e;
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "3045", "Socket error " + e.ToString() + " during adding disconnect", args.cid.ToString());
                            }
                            else
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "3045", "Unexpected exception during disconnect", args.cid.ToString());
                            }
                        }
                    }
                }

                ac.acsem.Release();
                //_sem.Release();
            }

            // at this point, there should be no more references to AC if all related controllers removed above, so it should be gone gone

            // there is no message to controller, so "end" the task now
            _log.WriteMessageWithId(NLog.LogLevel.Info, "1667", "TASK END", args.cid.ToString());
            if (!removedac && !alreadyempty)
            {
                ac.TaskActive = false;
                LegacyConnectionTracker.totalTaskActiveFrees++;
                ac.pollTimeout = 0;

                // kick next queued job if ready
                if (ac.queued_functions.Count > 0)
                {
                    RemoveTrackingEntryIfDone(ac);
                }
            }

            return;
        }

#pragma warning disable 1998
        public static async Task<Tuple<List<byte[]>, String, bool, int>> HubPollResponse(ClientInfoStruct client, CancellationTokenSource cts)
        {
            byte[] data;
            uint size;
            byte[] Buffer = null;
            bool do_ack = false;
            ActiveLegacyConnection ac = null;
            int ack_mid = 0;
            UInt32 crc;
            Boolean ignore = false;

            _log.WriteMessageWithId(NLog.LogLevel.Debug, "8866", "Processing Hub Poll Response", client.nid.ToString());

            data = null;
            
            ac = FindACfrom3000NID(client.nid);
            
            // what we do next depends on the incoming MID and what we have done so far...
            switch (client.mid)
            {
                case Constants.MID_TO_COMMSERVER_no_more_messages_init_packet:
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Recvd: Hub No more messages.", client.nid.ToString());
                    size = client.rxlen;
                    Buffer = client.rx;
                    if (ac != null)
                    {
                        ac.codedownloadinprogress = false;
                        ac.downloadingNID = 0;
                        do_ack = true;
                        ack_mid = Constants.MID_FROM_COMMSERVER_no_more_messages_ack;
                        if(ac.controllers3000.Exists(x => x.NID == client.nid))
                        {
                            ac.controllers3000.Find(x => x.NID == client.nid).consecutiveErrors = 0;    // got a valid poll response
                            if(ac.controllers3000.Find(x => x.NID == client.nid).needsHublist)
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "At end-of-messages, hub still needs hub list, queueing.", client.nid.ToString());
                                QueueUpHubList(ac, client.nid);
                            }
                            if(ac.controllers3000.Find(x => x.NID == client.nid).ignoreNextNoMoreMessages)
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "8866", "Ignoring initial hub no-more-messages as it came in middle of other poll", client.nid.ToString());
                                ac.controllers3000.Find(x => x.NID == client.nid).ignoreNextNoMoreMessages = false;
                                ignore = true;
                            }
                        }
                        if(!ignore)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "1667", "TASK END", client.nid.ToString());
                            ac.TaskActive = false;
                            LegacyConnectionTracker.totalTaskActiveFrees++;
                            ac.pollTimeout = 0;
                        }
                    }
                    break;

                case Constants.MID_TO_COMMSERVER_hub_is_busy_init_packet:
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Recvd: Hub reports busy.", client.nid.ToString());
                    size = client.rxlen;
                    Buffer = client.rx;
                    if (ac != null)
                    {
                        ac.codedownloadinprogress = true;
                        ac.downloadingNID = client.nid;
                        do_ack = true;
                        ack_mid = Constants.MID_FROM_COMMSERVER_hub_is_busy_ack;
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "1667", "TASK END", client.nid.ToString());
                        ac.TaskActive = false;
                        LegacyConnectionTracker.totalTaskActiveFrees++;
                        ac.pollTimeout = 0;
                        if (ac.controllers3000.Exists(x => x.NID == client.nid))
                        {
                            ac.controllers3000.Find(x => x.NID == client.nid).consecutiveErrors = 0;    // got a valid poll response
                        }
                        // flush any queued jobs other than polls to this hub - not allowed to
                        // talk to anyone else until this hub is not busy
                        ac.acsem.Wait(_cts.Token);
                        for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                        {
                            Tuple<int, Action<ActionArgs>, ActionArgs> tf = ac.queued_functions[i];
                            if (tf.Item3.cid != client.nid || tf.Item2 != ActionPoll3000)
                            {
                                ac.queued_functions.RemoveAt(i);
                                if (tf.Item3.jobid > 0)
                                {
#pragma warning disable 4014
                                    if (tf.Item3.adr == "3000")
                                    {
                                        PeriodicTasks.CompleteJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                                    }
                                    else
                                    {
                                        Misc.AddET2000Alert(_cts, tf.Item3.cid, "Hub is busy, no other comm allowed.", true);
                                        LegacyTasks.CompleteLegacyJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                                    }
#pragma warning restore 4014
                                }
                            }
                        }
                        ac.acsem.Release();
                    }
                    break;

                default:
                    // unexpected message
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "8866", "Recvd: Unknown MID in Hub Poll Response", client.nid.ToString());
                    break;
            }

            if (do_ack)
            {

                var mem = new MemoryStream(0);
                // build ack response
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(client.sn, mem);
                // network id
                Misc.Uint32ToNetworkBytes(client.nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)ack_mid, mem);

                _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "ACKing Hub poll response.", client.nid.ToString());

                // get the built response to return and be sent
                data = mem.ToArray();

                // send it immediately so it happens before the next dequeue
                crc = Misc.CRC(data);
                //_log.WriteMessage(NLog.LogLevel.Debug, "40", "Calculated CRC: 0x" + crc.ToString("X"));
                _log.WriteMessage(NLog.LogLevel.Debug, "28", "ACK to be transmitted to SN:" + client.sn.ToString());

                // combine preamble, data, CRC and then postamble
                byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                byte[] crcbytes;
                crcbytes = System.BitConverter.GetBytes(crc);
                if (BitConverter.IsLittleEndian)
                {
                    //_logger.WriteMessage(NLog.LogLevel.Debug, "3029", "FYI: CRC Bytes swapped to send ");
                    Array.Reverse(crcbytes, 0, crcbytes.Length);
                }
                System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);
                //
                // spit out data onto network stream
                try
                {
                    ac.controllers3000.Find(x => x.NID == client.nid).net_stream3000.Write(finalpacket, 0, (int)finalpacket.Length);
                    ac.controllers3000.Find(x => x.NID == client.nid).net_stream3000.Flush();

                    _log.WriteMessage(NLog.LogLevel.Debug, "75", "ACK send complete for" + client.sn.ToString());

                    // update stats tracking table
                    CalsenseControllers.BytesSent((int)client.sn, (int)finalpacket.Length);
                    CalsenseControllers.MsgsSent((int)client.sn, 1);
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "75", "Exception during transmit of ACK to controller SN:" + client.sn.ToString() + ", " + e.ToString());
                }
                mem.Dispose();
            }
            // if this is a demand poll, finish off the job
            if(ac != null)
            {
                if (ac.controllers3000.Exists(x => x.NID == client.nid))
                {
                    if(ac.controllers3000.Find(x => x.NID == client.nid).demandPollJobId != 0)
                    {
                        await PeriodicTasks.CompleteJob((Int32)ac.controllers3000.Find(x => x.NID == client.nid).demandPollJobId, Constants.JOB_SUCCESSFUL, null);
                        ac.controllers3000.Find(x => x.NID == client.nid).demandPollJobId = 0;
                    }
                }
            }
            // a poll response is always the end of a message sequence, so we take
            // this opportunity to move on to the next queued message.
            if (ac != null && !ignore)RemoveTrackingEntryIfDone(ac);
            // return nothing, leave connection up
            return (Tuple.Create((List<byte[]>)null, (String)null, false, Constants.NO_STATE_DATA));
        }
#pragma warning restore 1998

        //
        // Called when an initial message has been received from an inbound 2000 controller.
        // Responsible for validating legit 2000 inbound controller in DB and 
        //estabilishing the connection tracking structure(s)
        //
        public static ActiveLegacyConnection InboundHookup(CancellationTokenSource cts, ActiveLegacyConnection fakeac, TransportMessage tm, int serialnumber)
        {
            ActiveLegacyConnection ac = null;
            String key;
            AdsCommand cmd = null;
            uint cid = 0;
            AdsDataReader rdr;
            String adr = null;
            uint connectiontype = 0;
            uint companyid = 0;
            int multiplier = 0;
            bool repeaterInUse = false;
            int txTimerAddon = 0;

            // look up a controller with this serial number and HubType 9, it should exist
            try
            {
                ServerDB db = new ServerDB(cts);
                AdsConnection conn = db.OpenDBConnectionNotAsync("");
                if (conn != null)
                {
                    cmd = new AdsCommand("SELECT c.ControllerID, " +
                            " c.ConnectionType, c.Address, c.CompanyID, NoOfControllersInLegacyChain3(c.PhoneNumber,c.Address,c.HubID), c.UseHubType, ISNULL(Left(c.SoftwareVersion,1),'0'), ISNULL(RepeaterInUse,false), ISNULL(TxTimerAddon,0) " +
                            " FROM  Controllers c " +
                            " JOIN CMOS cm on cm.ControllerID = c.ControllerID " +
                            " WHERE c.UseHubType = 9 and isnull(c.ControllerID,0) > 0" +
                            " AND cm.ControllerSerialNumber = " + serialnumber.ToString() +
                            " AND c.CompanyID IN (SELECT CompanyID FROM Companies WHERE CommServerID=" + Constants.CommServerID + ")", conn);
                    if (cmd != null)
                    {
                        _log.WriteMessage(NLog.LogLevel.Debug, "199", "Query FYI: " + cmd.CommandText.ToString());
                    }
                    rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            if (!rdr.IsDBNull(2))
                            {
                                adr = rdr.GetString(2);    // controller address, 3 characters
                            }
                            else
                            {
                                continue;   // no address
                            }

                            if (!rdr.IsDBNull(3))
                            {
                                companyid = (uint)rdr.GetInt32(3);
                            }
                            else
                            {
                                continue;   // no company id
                            }

                            if (!rdr.IsDBNull(1))
                            {
                                connectiontype = (uint)rdr.GetInt32(1);
                            }
                            else
                            {
                                continue;   // no connectiontype
                            }

                            if (!rdr.IsDBNull(4))
                            {
                                multiplier = rdr.GetInt32(4);
                            }
                            else
                            {
                                continue;   // no connectiontype
                            }

                            if (!rdr.IsDBNull(7))
                            {
                                repeaterInUse = rdr.GetBoolean(7);
                            }

                            if (!rdr.IsDBNull(8))
                            {
                                txTimerAddon = rdr.GetInt32(8);
                            }

                            cid = (uint)rdr.GetInt32(0);

                            break;  // only top row
                        }
                    }

                    rdr.Close();
                    cmd.Unprepare();
                    db.CloseDBConnection(conn, "");
                }
            }
            catch (Exception e)
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "199", "Exception on Inbound 2000 controller match query: " + e.ToString());
                return (null);
            }
            if (cid == 0)
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "199", "No inbound 2000 controller found in DB with S/N: " + serialnumber.ToString());
                return (null);
            }

            _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Inbound 2000 controller " + cid.ToString() + " matched with S/N:  " + serialnumber.ToString(), cid.ToString());

            // update "key" to be based on the controller number which will group/queue commands for this controller with
            // any controllers behind it.
            key = "E2000-" + cid.ToString();

            // set up tracking
            // grab the mutex while we mess with things so two players don't try adding
            // same connection 
            try
            {
                _sem.Wait(_cts.Token);

                // if there already is a tracking entry, get rid of it, must be for stale/old connection
                if (HubTrackingTable.TryGetValue(key, out ac))
                {
                    // remove any queued functions
                    //
                    // remove any 2000 jobs queued up, in theory shouldn't be any as hub change very low priority
                    //
                    for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                    {
                        if (ac.queued_functions[i].Item3.adr != "3000")
                        {
                            if (ac.queued_functions[i].Item3.jobid != 0)
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "8866", "Removing queued legacy job at inbound 2000 re-connect, why is it still queued? Job ID:" + ac.queued_functions[i].Item3.jobid.ToString(), ac.queued_functions[i].Item3.cid.ToString());
                            }
                            else
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Removing queued legacy job at 2000 re-connect", ac.queued_functions[i].Item3.cid.ToString());
                            }
                            ac.queued_functions.RemoveAt(i);
                        }
                    }
                    // remove 2000 controllers in this queue grouping
                    for (int i = ac.controllers.Count - 1; i >= 0; i--)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "8866", "Clearing 2000 controller on 2000 re-connect: " + ac.controllers[i].CID.ToString(), cid.ToString());
                        ac.controllers.RemoveAt(i);
                    }
                    HubTrackingTable.TryRemove(key, out ac);
                }

                // if we already have a connection entry for this host, don't need a new object
                if (!HubTrackingTable.TryGetValue(key, out ac))
                {
                    HubTrackingTable.TryAdd(key, new ActiveLegacyConnection(key, connectiontype, companyid));
                    HubTrackingTable.TryGetValue(key, out ac);
                    if (ac != null)
                    {
                        ac.AddReuseGoodToGo = false;
                        // we'll set the 'hostname' thing to the controller ID of this controller, might as well
                        ac.controllers.Add(new ActiveLegacyConnection.ControllerInfo<UInt32, Int32, String, int>(cid, 0, cid.ToString(), multiplier, 9, repeaterInUse, txTimerAddon));
                        ac.key = key;
                        ac.hubhandler = true;
                        ac.inbound2000 = true;
                        // the active controller will be this first one 
                        ac.activehost = new StringBuilder(cid.ToString());
                        ac.activeadr = new StringBuilder(adr);
                        ac.activecid = cid;
                        ac.activemultiplier = ac.controllers.Find(x => x.CID == ac.activecid).TransportTimerMultiplier;
                        ac.activeDBaddon = ac.controllers.Find(x => x.CID == ac.activecid).dbTimerAddon;
                        ac.activeRepeater = ac.controllers.Find(x => x.CID == ac.activecid).repeaterInUse;

                        _log.WriteMessageWithId(NLog.LogLevel.Debug, "199", "Transport Timer multiplier is " + multiplier.ToString() +
                        " , system has a repeater? " + repeaterInUse.ToString() +
                        " , additional timer in DB is " + txTimerAddon.ToString(), cid.ToString());
                    }
                }
                else
                {
                    // error, should have been removed above
                }

                // done manipulating dictionary/list
                _sem.Release();
            }
            catch (OperationCanceledException)
            {
                // program exiting
                return (null);
            }

            // use the fake ac socket values that are already established for inbound connection
            ac.net_stream = fakeac.net_stream;
            ac.sock = fakeac.sock;

            return (ac);
        }

    }
}