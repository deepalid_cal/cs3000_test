﻿//
//
// ServerInternal class 
//
// Handles the common TCP/IP data reception/transmission that is not MID specific.
// One instance per remote client (IP address). Receives INIT packets and subsequent
// DATA packets. Combines application data into one potentially large buffer and
// passes that buffer off to a parsing table based on the incoming MID.
//
// Each parsing function is contained in a class such as MsgFlowRecorder.cs
// The parsing functions are connected to a particular MID via a lookup 
// table below.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using ServerApp;

[StructLayout(LayoutKind.Sequential, Pack = 8)]
public unsafe struct DUMMY_STRUCT           // unsafe require if buffers inside are fixed
{
    //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
    public fixed char header[10];
    public int body1;
    public int body2;
    //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
    public fixed char footer[8];
}

public struct ClientInfoStruct
{
    public IPAddress ip;// ip address of socket ... remote (controller)
    public UInt32 mid;  // MID from header
    public UInt32 sn;   // serial number from header
    public UInt32 nid;  // network id from header
    public UInt32 remoteport;   // remote port used in connection
    public byte[] rx;   // received from controller
    public UInt32 rxlen;
    public String sessioninfo;      // string returned from Parse call, passed back in for further reference by client
    public int internalstate;       // int returned from Parse call, passed back in for further reference by client
}

namespace Calsense.CommServer
{
    internal class ServerInternal
    {
        public const int THRU_MID_SIZE = 18;                // number of bytes to get up to first MID value in any message
        public const int INIT_SIZE = 38;                     // init packet size
        public const int MSG_INITIALIZATION_STRUCT_SIZE = 12;
        public const int ACK_SIZE = (INIT_SIZE - (MSG_INITIALIZATION_STRUCT_SIZE));      // no MSG_INITIALIZATION_STRUCT

        public const int MAX_APP_BUFFER_SIZE = 1024*1024;     // biggest message probably station history 591386
        public const int MAX_DATA_IN_PACKET = 2048;          // maximum number of application data bytes in data packet
        public const int PACKET_OVERHEAD = (ACK_SIZE + 4);     // ambles, header, pkt num, and mid (each of those 2 bytes)
        public const int MAX_PACKET_SIZE = (MAX_DATA_IN_PACKET + PACKET_OVERHEAD);   // most bytes on line for a data packet

        public const int OFFSET_PRE = 0;                // preamble
        public const int OFFSET_SN = 4;                 // offset of serial number
        public const int OFFSET_NID = 8;                // offset of from network id
        public const int OFFSET_LEN = 12;                // offset of length
        public const int OFFSET_MID = 16;               // offset of MID
        public const int INIT_OFFSET_EXP_SIZE = 18;          // offset of expected size (of data in packets that follow)
        public const int INIT_OFFSET_EXP_CRC = 22;           // offset of expected CRC (of data in packets that follow)
        public const int INIT_OFFSET_EXP_PKTS = 26;          // offset of expected number of packets
        public const int INIT_OFFSET_MID_DUP = 28;           // another copy of MID
        public const int INIT_OFFSET_CRC = 30;               // CRC
        public const int INIT_OFFSET_POST = 34;              // offset of postamble
        public const int DATA_OFFSET_PKT_NUM = 18;          // offset of to packet number in data packet
        public const int DATA_OFFSET_MID_DUP = 20;           // duplicate MID
        public const int DATA_OFFSET_DATA = 22;             // application data offset in data packet

        public struct MsgLen
        {
            public int max;
            public int min;
            public bool ack;    // true if this is an "ack" type message"
        }

        public static Dictionary<UInt32, Func<ClientInfoStruct, CancellationTokenSource, Task<Tuple<List<byte[]>, String, bool, int>>>> ParseFunctions =
            new Dictionary<UInt32, Func<ClientInfoStruct, CancellationTokenSource, Task<Tuple<List<byte[]>, String, bool, int>>>> 
        {
             { Constants.MID_TO_COMMSERVER_REGISTRATION_DATA_init_packet, MsgRegistration.Parse },
             { Constants.MID_TO_COMMSERVER_FLOW_RECORDING_init_packet, MsgReports.Parse },
             { Constants.MID_TO_COMMSERVER_CHECK_FOR_UPDATES_init_packet, MsgFirmwareUpdate.Parse },
             { Constants.MID_CODE_DISTRIBUTION_main_init_ack, MsgFirmwareUpdate.Parse },
             { Constants.MID_CODE_DISTRIBUTION_main_full_receipt_ack, MsgFirmwareUpdate.Parse },
             { Constants.MID_CODE_DISTRIBUTION_tpmicro_init_ack, MsgFirmwareUpdate.Parse },
             { Constants.MID_CODE_DISTRIBUTION_tpmicro_full_receipt_ack, MsgFirmwareUpdate.Parse },
             { Constants.MID_TO_COMMSERVER_ENGINEERING_ALERTS_init_packet, MsgAlerts.Parse },
             { Constants.MID_TO_COMMSERVER_STATION_HISTORY_init_packet, MsgReports.Parse },
             { Constants.MID_TO_COMMSERVER_STATION_REPORT_DATA_init_packet, MsgReports.Parse },
             { Constants.MID_TO_COMMSERVER_POC_REPORT_DATA_init_packet, MsgReports.Parse },
             { Constants.MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_init_packet, MsgReports.Parse },
             { Constants.MID_TO_COMMSERVER_WEATHER_DATA_init_packet,  MsgWeatherData.Parse },
             { Constants.MID_TO_COMMSERVER_RAIN_INDICATION_init_packet, MsgRainShutdown.Parse },
             { Constants.MID_TO_COMMSERVER_PROGRAM_DATA_init_packet, MsgProgramData.Parse },
             { Constants.MID_TO_COMMSERVER_VERIFY_FIRMWARE_VERSION_init_packet, MsgVerifyFirmware.Parse },
             { Constants.MID_TO_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_init_packet, MsgVerifyFirmware.Parse },
             { Constants.MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_init_packet, MsgProgramDataRequest.Parse },
             { Constants.MID_TO_COMMSERVER_PROGRAM_DATA_data_full_receipt_ack, MsgProgramDataRequest.Parse },
             { Constants.MID_TO_COMMSERVER_PROGRAM_DATA_init_ack, MsgProgramDataRequest.Parse },
             { Constants.MID_TO_COMMSERVER_WEATHER_DATA_receipt_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_mobile_station_ON_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_mobile_station_OFF_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_RAIN_SHUTDOWN_receipt_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_send_a_status_screen_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_status_screen_init_packet, PeriodicTasks.ReceivedStatusScreen},
             { Constants.MID_TO_COMMSERVER_panel_swap_factory_reset_to_new_panel_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_panel_swap_factory_reset_to_old_panel_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_set_all_bits_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_init_packet, MsgReports.Parse },
             { Constants.MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_init_packet, MsgReports.Parse },
             { Constants.MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet, MsgReports.Parse },
             { Constants.MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet2, MsgReports.Parse },
             { Constants.MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_init_packet, MsgReports.Parse },
             { Constants.MID_TO_COMMSERVER_force_registration_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_mobile_MVOR_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_mobile_light_cmd_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_mobile_turn_controller_on_off_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_mobile_now_days_all_stations_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_mobile_now_days_by_group_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_mobile_now_days_by_box_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_mobile_now_days_by_station_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_clear_MLB_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_stop_all_irrigation_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_stop_irrigation_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_enable_or_disable_FL_option_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_enable_or_disable_HUB_option_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_check_for_updates_ack, PeriodicTasks.BlastAck },
             { Constants.MID_TO_COMMSERVER_keep_alive, KeepaliveAck },
             { Constants.MID_TO_COMMSERVER_no_more_messages_init_packet, HubConnectionTracker.HubPollResponse },
             { Constants.MID_TO_COMMSERVER_hub_is_busy_init_packet, HubConnectionTracker.HubPollResponse },
             { Constants.MID_TO_COMMSERVER_please_send_me_the_hub_list, HubConnectionTracker.HubListResponse },
             { Constants.MID_TO_COMMSERVER_here_is_the_hub_list_ack, HubConnectionTracker.HubListReceived },
             { Constants.MID_TO_COMMSERVER_send_engineering_alerts_ack, PeriodicTasks.BlastAck },
        };
        
        // test call unmanged C code
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern double Add(double a, double b);
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint Dummy(ref DUMMY_STRUCT pp,
        uint why,
        [In, Out] byte[] dest,
        uint sz,
        uint idx);
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int MaxSizeMessage(uint mid);
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int MinSizeMessage(uint mid);
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int AckOrNotMessage(uint mid);
        

        private readonly Server _server;
        private readonly Socket _socket;
        private readonly NetworkStream _networkStream;
        private CancellationTokenSource _cts;
        private MyLogger _logger;
        private int _rxBytecount;
        private int _txBytecount;
        public static Dictionary<int, MsgLen> MessageLimits = new Dictionary<int, MsgLen>();
        private UInt32 _sn;
        private UInt32 _nid;
        private UInt32 _len;
        private UInt16 _mid;
        private UInt32 _expected_size;
        private UInt32 _expected_crc;
        private UInt16 _expected_packets;
        private UInt32 _crc;
        private UInt16 _mid_dupe;

        // we'll wait for 5 minutes after the connection is established to see the fisrt packets, 
        // normally the controller sent the first in-bound packet after 30 seconds.
        private const int SocketTimeout = 300000; // 5 minutes

        // time out for the identified socket "received the first in-bound packet
        // (complete pre amble / post amble packet that passes CRC)"
        private const int IdentifiedSocketTimeout = 86400000; // 24 hours

        public enum ReceiveState
        {
            RX_BEGIN_HEADER,
            RX_FINISH_HEADER,
            RX_DATA,
        }

        public ServerInternal(Server server, Socket socket, CancellationTokenSource cts)
        {
            _server = server;
            _socket = socket;
            _cts = cts;
            _rxBytecount = 0;
            _txBytecount = 0;
            _logger = new MyLogger(this.GetType().Name, "", "", Constants.CS3000Model);   // unknown controller at this point
            // Set up streams
            _logger.WriteMessage(NLog.LogLevel.Debug, "3001", "Starting task for " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) + 
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString());

            _networkStream = new NetworkStream(socket, true);
        }

        //
        // KeepaliveAck routine - called when we get keepalive from controller
        //
#pragma warning disable 1998
        public static async Task<Tuple<List<byte[]>, String, bool, int>> KeepaliveAck(ClientInfoStruct client, CancellationTokenSource cts)
        {
            uint size;
            byte[] Buffer = null;
            byte[] data;

            size = client.rxlen;
            Buffer = client.rx;

            var mem = new MemoryStream(0);
            // build ack response
            // PID byte
            mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
            // message class byte
            mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
            // to serial number
            Misc.Uint32ToNetworkBytes(client.sn, mem);
            // network id
            Misc.Uint32ToNetworkBytes(client.nid, mem);
            // mid
            Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_keep_alive_ack, mem);

            // get the built response to return and be sent
            data = mem.ToArray();
            List<byte[]> rsplist = new List<byte[]>();
            rsplist.Add(data);
            return (Tuple.Create(rsplist, (String)null, false, Constants.NO_STATE_DATA));
        }

        private void OurSocketClose()
        {
            if (_nid != 0)
            {
                ServerSocketTracker.RemoveTrackingEntry(_nid, _networkStream);
            }
            _socket.Shutdown(SocketShutdown.Both);  // ensure all data sent/received
            _socket.Disconnect(false);
        }

        public async void Do()
        {
            Int32 amount;
            Int32 got;
            Int32 rxmax;
            Int32 expected_packet_number;
            MsgLen msglen;
            byte[] buf = new byte[MAX_APP_BUFFER_SIZE];  // assemble application data into this buffer
            byte[] pkt = new byte[MAX_PACKET_SIZE];     // receive raw init/data/ack packets into this buffer
            Int32 offset;
            Int32 app_offset;
            ReceiveState _state;
            ClientInfoStruct client;
            List<byte[]> responseList = new List<byte[]>();
            bool terminate_connection;
            int i,j;
            bool first_time = true;
            bool hub_mode = false;
            CancellationTokenRegistration registrationThingy = new CancellationTokenRegistration();
 
            if (System.Threading.Thread.CurrentThread.Name == null)
            {
                System.Threading.Thread.CurrentThread.Name = "TCP Do()";
            }

            // start out looking for a header from remote controller
            _state = ReceiveState.RX_BEGIN_HEADER;
            _sn = 0;
            offset = 0;
            app_offset = 0;
            amount = 0;
            rxmax = THRU_MID_SIZE;          // receive up to the MID value to see what we are getting
            msglen.max = 0;
            msglen.min = 0;
            msglen.ack = false;
            client.sessioninfo = null;  // for repeated passes, will get filled in by return value from client
            client.internalstate = 0;   // ditto
            expected_packet_number = 0;
            client.remoteport = (uint)((IPEndPoint)(_socket.RemoteEndPoint)).Port;

            // set the remote IP as a thread context for logging, should allow easier filtering per controller
            // the nlog,network,chainsaw veiwers don't seem to support the ndc/mdc output, so don't bother
            // NLog.MappedDiagnosticsContext.Set("ip", IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()).ToString());
            // NLog.NestedDiagnosticsContext.Push(IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()).ToString());

            // Executed on a separate thread from listener
            for (; ; )
            {
                _logger.WriteMessage(NLog.LogLevel.Debug, "3002", "Ready to receive " + (rxmax - amount).ToString() + " bytes from " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                                  " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString());

                DateTime beginstamp = DateTime.Now;

                try
                {

                    // receive next chunk of data into our packet buffer
                    Task<Int32> rx = _networkStream.ReadAsync(pkt, offset, rxmax-amount);
                    registrationThingy.Dispose();   // clear old one
                    TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
                    registrationThingy = _cts.Token.Register(() => tcs.TrySetCanceled(),
                        useSynchronizationContext: false);

                    // timeout used below in Delay() call - remote client needs to send
                    // something within this amount of time or we give up on him
                    // Note: Controller times out in about 90 seconds. Since controller
                    // disconnects in almost all case, we'll try and wait longer than 90
                    // in case he decides to resend/send new something on this connection.
                    if (rx == await Task.WhenAny(rx, tcs.Task, Task.Delay(first_time ? SocketTimeout : IdentifiedSocketTimeout)).ConfigureAwait(false))
                    {
                        got = await rx;         // bytes received this call
                        if (got == 0)
                        {
                            // dead socket, nothing actually received
                            _logger.WriteMessage(NLog.LogLevel.Info, "3003", "Controller closed socket. IP: " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString());
                            OurSocketClose();
                            registrationThingy.Dispose();
                            return;
                        }
                        _logger.WriteMessage(NLog.LogLevel.Debug, "3002", "Received " + got.ToString() + " bytes from " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString());
                        amount += got;          // total this message
                        _rxBytecount += got;    // total this client
                        if(_sn != 0)
                        {
                            // update stats tracking table
                            CalsenseControllers.BytesReceived((int)_sn, got);
                        }
                    }
                    else if (_cts.IsCancellationRequested)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "3002", "Stopped on " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString());
                        // Close connection
                        OurSocketClose();
                        registrationThingy.Dispose();
                        return;
                    }
                    else
                    {
                        // Close connection
                        // (in hub_mode, this task has already switched processing to HubInternal, the timeout now
                        // is the Task.Wait timing out. safe to ignore)
                        if (hub_mode != true)
                        {  
                            _logger.WriteMessage(NLog.LogLevel.Warn, "3003", "Timed out waiting for data from " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString());
 
                            // when the timeout happens after switching over to hub, don't close socket since HubInternal has control now
                            OurSocketClose();
                        }
                        registrationThingy.Dispose();
                        return;
                    }
                }
                catch(Exception e)
                {
                    if (e is SocketException)
                    {
                        var ex = (SocketException)e;
                        if (ex.SocketErrorCode == SocketError.Interrupted)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Debug, "3005", "Socket terminating normally for " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString());
                            if (_nid != 0) ServerSocketTracker.RemoveTrackingEntry(_nid, _networkStream);
                            break;
                        }
                        else if (ex.SocketErrorCode == SocketError.ConnectionReset)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Warn, "3005", "Remote controller disconnected " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString());
                            if (_nid != 0) ServerSocketTracker.RemoveTrackingEntry(_nid, _networkStream);
                            break;
                        }
                        else
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "3005", "Unexpected Socket exception during network read " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString() + " " + ex.ToString());
                            if (_nid != 0) ServerSocketTracker.RemoveTrackingEntry(_nid, _networkStream);
                            break;
                        }

                    }
                    else if (e is IOException)
                    {
                        if (_state == ReceiveState.RX_BEGIN_HEADER)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Info, "3005", "Connection terminated by controller: " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString());
                        }
                        else
                        {
                            _logger.WriteMessage(NLog.LogLevel.Warn, "3005", "Unexpected I/O exception during network read " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString() + " " + e.ToString());
                        }
                        if (_nid != 0) ServerSocketTracker.RemoveTrackingEntry(_nid, _networkStream);
                        break;
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "3005", "Unexpected exception during network read " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString() + " " + e.ToString());
                        if (_nid != 0) ServerSocketTracker.RemoveTrackingEntry(_nid, _networkStream);
                        break;
                    }
                }

                if (!_socket.Connected || amount == 0)
                {
                    _logger.WriteMessage(NLog.LogLevel.Info, "3095", "Socket terminating, not connected anymore." + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString());
                    if (_nid != 0) ServerSocketTracker.RemoveTrackingEntry(_nid, _networkStream);
                    registrationThingy.Dispose();
                    return;
                }

                switch(_state){
                    case ReceiveState.RX_BEGIN_HEADER:
                        // see if we got all the bytes we want up to and including MID
                        if (amount == rxmax)
                        {
                            // check pre amble
                            for (i = 0; i < Constants.PREAMBLE.Length; i++)
                            {
                                if (pkt[OFFSET_PRE + i] != Constants.PREAMBLE[i])
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3087", "Expected Preamble from controller, but got:" + pkt[0 + OFFSET_PRE].ToString() + " " + pkt[1 + OFFSET_PRE].ToString() + " " + pkt[2 + OFFSET_PRE].ToString() + " " + pkt[3 + OFFSET_PRE].ToString());
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3087", "Closing socket because of preamble mistmatch.");
                                    // Close connection
                                    OurSocketClose();
                                    registrationThingy.Dispose();
                                    return;
                                }
                            }

                            // because we got a full packet worth of data, it is safe to pull data out
                            // from these various offsets
                            _sn = Misc.NetworkBytesToUint32(pkt, OFFSET_SN);
                            client.sn = _sn;    // go ahead and set this, so available for logging
                            _nid = Misc.NetworkBytesToUint32(pkt, OFFSET_NID);
                            _logger.UpdateId(_nid.ToString()); // now we can log with an ID = network ID
                            _logger.UpdateControllerId(_nid.ToString()); // now we can log into databse table with an ID = network ID
                            
                            _len = Misc.NetworkBytesToUint32(pkt, OFFSET_LEN);
                            _mid = Misc.NetworkBytesToUint16(pkt, OFFSET_MID);

                            // see if this is a 3000 hub or not
                            if(first_time)
                            {

                                switch(HubInternal.HubHandoff(pkt, _socket, _cts, _networkStream, _nid, _sn))
                                {
                                    case HubHandoffCode.HANDOFF_FATAL_ERROR:
                                        _logger.WriteMessage(NLog.LogLevel.Error, "3087", "Closing connection, fatal error during Hub handoff check.");
                                        // Close connection
                                        OurSocketClose();
                                        registrationThingy.Dispose();
                                        return;
                                    case HubHandoffCode.HANDOFF_YES:
                                        _logger.WriteMessage(NLog.LogLevel.Info, "3087", "Hub processing initiated.");
                                        hub_mode = true;
                                        registrationThingy.Dispose();
                                        return;
                                    case HubHandoffCode.HANDOFF_NO:
                                        _logger.WriteMessage(NLog.LogLevel.Debug, "3087", "FYI this is not a hub, per database.");
                                        break;
                                    default:
                                        _logger.WriteMessage(NLog.LogLevel.Warn, "3087", "Unknown code returned from hub handoff check");
                                        break;
                                }

                            }

                            if (first_time && _nid != 0)
                            {
                                _logger.WriteMessage(NLog.LogLevel.Info, "3098", "Socket is identified as NID: " + _nid.ToString() +
                                    " , IP: " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                    " , remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString());
                            }

                            // Check if the controller was a part of swapping process and the process didn't complete 
                            // either because the controller was powered down or because a communication failure. 
                            // if so we need to send a factory reset command 
                            if (_nid != 0)
                            {
                                int s = await Misc.NeedToDoFactoryReset(_logger, _cts, _nid, _sn);

                                if (s == Constants.FACTORY_RESET_IS_REQIURED)
                                {
                                    // send a factory reset command
                                    await FactoryResetToOldControllerRequest(_logger, _nid, _sn);

                                    // Close connection
                                    OurSocketClose();
                                    registrationThingy.Dispose();
                                    return;
                                }
                                else if (s == Constants.FACTORY_RESET_ERROR_WHILE_CHECKING)
                                {
                                    // some error occured, close connection
                                    OurSocketClose();
                                    registrationThingy.Dispose();
                                    return;
                                }
                            }

                            // note contact in our stats tracking table
                            CalsenseControllers.Contacted((int)_sn, ((IPEndPoint)_socket.RemoteEndPoint).Address);
                            // update Last Communicated field in database
                            await Misc.LastCommunicated(_logger, _cts, _nid);

                            // check for error condition of nid changing on an already connected socket
                            if(_nid != 0 && first_time == false)
                            {
                                // look up the new nid in socket tracking table
                                if(ServerSocketTracker.DoubleCheckNid(_nid, _sn, _networkStream,_logger) != 0)
                                {
                                    // some kind of problem, NID changing or something...
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3099", "Closing connection, NID: " + _nid.ToString());
                                    // Close connection
                                    OurSocketClose();
                                    registrationThingy.Dispose();
                                    return;
                                }
                            }
                            // for first time we know controller serial number on this socket, insert into the tracking table
                            if (first_time && _nid != 0 && _sn != 0)
                            {
                                first_time = false;
                                ServerSocketTracker.AddTrackingEntry(_nid, _sn, _networkStream, _socket, _cts);
                            }


                            // see how many application bytes could be possible for this message type
                            if (MessageLimits.TryGetValue(_mid, out msglen))
                            {
                                // Key was in dictionary, msglen set to range we could receive max, min
                            }
                            else
                            {
                                msglen.max = MaxSizeMessage(_mid);
                                // override max - allow 1 megabyte for all messages with data now
                                if(msglen.max > 0)msglen.max = 1024 * 1024;
                                msglen.min = MinSizeMessage(_mid);
                                // check for a message we don't know about
                                if (msglen.max == -1 || msglen.min == -1)
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Error, "3023", "Unknown MID received " + _mid.ToString());
                                    // Close connection
                                    OurSocketClose();
                                    registrationThingy.Dispose();
                                    return;
                                }
                                if (msglen.max > MAX_APP_BUFFER_SIZE)
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Error, "3043", "Internal sizing error, msg maximum " + msglen.max.ToString() + " too big for buffer of " + MAX_APP_BUFFER_SIZE);
                                    // Close connection
                                    OurSocketClose();
                                    registrationThingy.Dispose();
                                    return;
                                }
                                // see if this is an ACK structure message or not...
                                msglen.ack = AckOrNotMessage(_mid) == 0 ? false : true;
                                // add lookup to dictionary for later use to avoid trip in C DLL
                                MessageLimits.Add(_mid, msglen);
                            }

                            // for ACK messages we receive remaining data (rare, only mobile on/off at this time), CRC and postamble.
                            // for all other messages we receive that plus Init struct
                            if (msglen.ack)
                            {
                                // how much to receive in remainder of header - note for some special case ACKs there is a bit of data included (e.g. mobile ON/OFF)
                                rxmax = ACK_SIZE - THRU_MID_SIZE + msglen.min;
                            }
                            else
                            {
                                // how much to receive in remainder of header
                                rxmax = INIT_SIZE - THRU_MID_SIZE;
                            }
                            offset = amount;    // keep appending 
                            _state = ReceiveState.RX_FINISH_HEADER;
                            amount = 0;
                        }
                        else
                        {
                            _logger.WriteMessage(NLog.LogLevel.Debug, "3021", "Did not get full INIT packet yet, instead got so far:" + amount.ToString());
                            offset = amount;
                        }
                        continue;   // always keep trying while working on header...

                    case ReceiveState.RX_FINISH_HEADER:
                        // see if we got a full packet...
                        if(amount == rxmax)
                        {
                            // check post amble, pre amble already good
                            
                            for (i = 0; i < Constants.POSTAMBLE.Length; i++)
                            {
                                if (pkt[(msglen.ack ? ACK_SIZE + msglen.min : INIT_SIZE) + i - Constants.POSTAMBLE.Length] != Constants.POSTAMBLE[i])
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3087", "Expected Postamble from controller, but got:" +
                                        pkt[(msglen.ack ? ACK_SIZE + msglen.min : INIT_SIZE) + 0 - Constants.POSTAMBLE.Length].ToString() + " " +
                                        pkt[(msglen.ack ? ACK_SIZE + msglen.min : INIT_SIZE) + 1 - Constants.POSTAMBLE.Length].ToString() + " " +
                                        pkt[(msglen.ack ? ACK_SIZE + msglen.min : INIT_SIZE) + 2 - Constants.POSTAMBLE.Length].ToString() + " " +
                                        pkt[(msglen.ack ? ACK_SIZE + msglen.min : INIT_SIZE) + 3 - Constants.POSTAMBLE.Length].ToString());
                                    // Close connection
                                    OurSocketClose();
                                    registrationThingy.Dispose();
                                    return;
                                }
                            }

                            // for everything except ACKs process the MSG_INITIALIZATION_STRUCT stuff
                            if (!msglen.ack)
                            {
                                _expected_size = Misc.NetworkBytesToUint32(pkt, INIT_OFFSET_EXP_SIZE);
                                _expected_crc = Misc.NetworkBytesToUint32(pkt, INIT_OFFSET_EXP_CRC);
                                _expected_packets = Misc.NetworkBytesToUint16(pkt, INIT_OFFSET_EXP_PKTS);
                                _mid_dupe = Misc.NetworkBytesToUint16(pkt, INIT_OFFSET_MID_DUP);
                                _crc = Misc.NetworkBytesToUint32(pkt, INIT_OFFSET_CRC);

                                // sanity check that both MIDs received in init packet are the same
                                if (_mid_dupe != _mid)
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3087", "Expected MIDs to match in Init packet but got 2 different: " + _mid.ToString() + " " + _mid_dupe.ToString());
                                    // Close connection
                                    OurSocketClose();
                                    registrationThingy.Dispose();
                                    return;
                                }


                                // check CRC - it is calc'd across packet less pre/postambles and 4-byte CRC itself
                                if (Misc.OrderedCRC(pkt, OFFSET_SN, INIT_SIZE - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))) != _crc)
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3044", "CRC mismatch on init packet, calculated: " + Misc.OrderedCRC(pkt, OFFSET_SN, INIT_SIZE - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))).ToString("X") + " recv'd: " + _crc.ToString("X"));
                                    // Close connection
                                    OurSocketClose();
                                    registrationThingy.Dispose();
                                    return;
                                }

                                _logger.WriteMessage(NLog.LogLevel.Debug, "3020", "Rx'd full init packet: SN:" + _sn.ToString() +
                                    " NID:" + _nid.ToString() + " Len:" + _len.ToString() + " MID:" + _mid.ToString() +
                                    " Exp Size:" + _expected_size.ToString() + " Exp CRC: 0x" + _expected_crc.ToString("X") +
                                    " Exp Packets:" + _expected_packets.ToString() + " CRC: 0x" + _crc.ToString("X"));
                            }
                            // ACKs have no MSG_INITIALIZATION_STRUCT, so just skip to the CRC
                            else
                            {

                                _crc = Misc.NetworkBytesToUint32(pkt, msglen.min + INIT_OFFSET_CRC - MSG_INITIALIZATION_STRUCT_SIZE);
                                _logger.WriteMessage(NLog.LogLevel.Debug, "3020", "Rx'd ACK packet: SN:" + _sn.ToString() +
                                    " NID:" + _nid.ToString() + " Len:" + _len.ToString() + " MID:" + _mid.ToString() +
                                    " CRC: 0x" + _crc.ToString("X"));
                                if (Misc.OrderedCRC(pkt, OFFSET_SN, msglen.min + ACK_SIZE - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))) != _crc)
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3044", "CRC mismatch on ack packet, calculated: " + Misc.OrderedCRC(pkt, OFFSET_SN, msglen.min + ACK_SIZE - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))).ToString("X") + " recv'd: " + _crc.ToString("X"));
                                    // Close connection
                                    OurSocketClose();
                                    registrationThingy.Dispose();
                                    return;
                                }
                            }

                            // for ACK messages - we are done, move on to parsing
                            if(msglen.ack)
                            {
                                // indicate any size of data contained in the ACK (normally 0, but Mobile ON/OFF has a job number here)
                                app_offset = msglen.min;
                                // if there is data in the ACK, move it into the final buffer that holds data for Parse routine
                                for (i = 0; i < app_offset; i++ )
                                {
                                    buf[i] = pkt[i + INIT_OFFSET_CRC - MSG_INITIALIZATION_STRUCT_SIZE]; // any ACK data is where CRC would normally start in an ACK
                                }
                                break;
                            }

                            // safety check length we have been told makes sense
                            // against what we know of message
                            if(_expected_size > msglen.max)
                            {
                                _logger.WriteMessage(NLog.LogLevel.Error, "3023", "Received expected size of " + _expected_size.ToString() + " expecting no more than " + msglen.max.ToString());
                                // Close connection
                                OurSocketClose();
                                registrationThingy.Dispose();
                                return;
                            }
                            if (_expected_size < msglen.min)
                            {
                                _logger.WriteMessage(NLog.LogLevel.Error, "3023", "Received expected size of " + _len.ToString() + " expecting at least " + msglen.min.ToString());
                                // Close connection
                                OurSocketClose();
                                registrationThingy.Dispose();
                                return;
                            }


                            // now start receiving application data, MAX_PACKET_SIZE is most in one packet

                            _state = ReceiveState.RX_DATA;                           
                            offset = 0;
                            app_offset = 0;
                            expected_packet_number = 1;
                            // how much to receive, one data packet worth
                            rxmax = (_expected_size > MAX_DATA_IN_PACKET) ? MAX_PACKET_SIZE : (Int32)((_expected_size + PACKET_OVERHEAD));
                            amount = 0;
                        }
                        else
                        {
                            _logger.WriteMessage(NLog.LogLevel.Debug, "3021", "Did not get full INIT packet yet, instead got so far:" + amount.ToString());
                            offset = THRU_MID_SIZE + amount; // keep appending after the MID we already received
                        }
                        continue;   // always keep trying while working on header, no parsing until body

                    case ReceiveState.RX_DATA:
                        if(amount == rxmax)
                        {
                            // we have a full data packet ... verify and pull the application data out into our buffer
                            // check pre and post amble
                            for(i = 0; i < Constants.PREAMBLE.Length; i++)
                            {
                                if (pkt[OFFSET_PRE+i] != Constants.PREAMBLE[i])
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3097", "Expected Preamble from controller, but got:" + pkt[0 + OFFSET_PRE].ToString() + " " + pkt[1 + OFFSET_PRE].ToString() + " " + pkt[2 + OFFSET_PRE].ToString() + " " + pkt[3 + OFFSET_PRE].ToString());
                                    // Close connection
                                    OurSocketClose();
                                    registrationThingy.Dispose();
                                    return;
                                }
                            }
                            
                            for (i = 0; i < Constants.POSTAMBLE.Length; i++)
                            {
                                if (pkt[i + amount - Constants.POSTAMBLE.Length] != Constants.POSTAMBLE[i])
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3097", "Expected Postamble from controller, but got:" + pkt[0 + amount - Constants.POSTAMBLE.Length].ToString() + " " + pkt[1 + amount - Constants.POSTAMBLE.Length].ToString() + " " + pkt[2 + amount - Constants.POSTAMBLE.Length].ToString() + " " + pkt[3 + amount - Constants.POSTAMBLE.Length].ToString());
                                    // Close connection
                                    OurSocketClose();
                                    registrationThingy.Dispose();
                                    return;
                                }
                            }
                            // sanity check for matching header values
                            if(_sn != Misc.NetworkBytesToUint32(pkt, OFFSET_SN))
                            {
                                _logger.WriteMessage(NLog.LogLevel.Warn, "3098", "Got mismatched SN in Data Pkt vs. Init Pkt.");
                                // Close connection
                                OurSocketClose();
                                registrationThingy.Dispose();
                                return;
                            }
                            if(_nid != Misc.NetworkBytesToUint32(pkt, OFFSET_NID))
                            {
                                _logger.WriteMessage(NLog.LogLevel.Warn, "3098", "Got mismatched Network ID in Data Pkt vs. Init Pkt.");
                                // Close connection
                                OurSocketClose();
                                registrationThingy.Dispose();
                                return;
                            }
                            if(expected_packet_number != Misc.NetworkBytesToUint16(pkt, DATA_OFFSET_PKT_NUM))
                            {
                                _logger.WriteMessage(NLog.LogLevel.Warn, "3099", "Got out of sequence Packet Number in Data Pkt. Got " + Misc.NetworkBytesToUint16(pkt, DATA_OFFSET_PKT_NUM).ToString() + " expected " + expected_packet_number.ToString());
                                // Close connection
                                OurSocketClose();
                                registrationThingy.Dispose();
                                return;
                            }
                            if (Misc.OrderedCRC(pkt, OFFSET_SN, amount - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))) != Misc.NetworkBytesToUint32(pkt, amount-(Constants.POSTAMBLE.Length + sizeof(UInt32))))   // CRC is right before post amble
                            {
                                _logger.WriteMessage(NLog.LogLevel.Warn, "3044", "CRC mismatch on data packet, calculated: " + Misc.OrderedCRC(pkt, OFFSET_SN, amount - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))).ToString("X") + " recv'd: " + Misc.NetworkBytesToUint32(pkt, amount - (Constants.POSTAMBLE.Length + sizeof(UInt32))).ToString("X"));
                                // Close connection
                                OurSocketClose();
                                registrationThingy.Dispose();
                                return;
                            }

                            // append application data into the big buffer
                            Array.Copy(pkt, DATA_OFFSET_DATA, buf, app_offset, amount - PACKET_OVERHEAD);
                            app_offset += (amount - PACKET_OVERHEAD);

                            if(app_offset >=_expected_size)
                            {
                                // we have copied into buf all the application data that was expected...move on to parsing that
                                // will hit break for this case
                            }
                            else
                            {
                                // receive next data packet
                                offset = 0;
                                rxmax = ((_expected_size - app_offset) > MAX_DATA_IN_PACKET) ? MAX_PACKET_SIZE : (Int32)(((_expected_size-app_offset) + PACKET_OVERHEAD));
                                amount = 0;
                                expected_packet_number++;
                                continue;
                            }
                        }
                        else
                        {
                            _logger.WriteMessage(NLog.LogLevel.Debug, "3027", "Received data, more expected in this DATA packet...receiving again...so far:" + amount.ToString());
                            offset = amount;
                            continue;   // more to receive
                        }
                        break;
                }

                _logger.WriteMessage(NLog.LogLevel.Debug, "3020", "Total Rx'd so far from client is " + _rxBytecount.ToString());

                client.ip = ((IPEndPoint)(_socket.RemoteEndPoint)).Address;
                client.sn = _sn;
                client.nid = _nid;
                client.rx = buf;
                client.rxlen = (UInt32)app_offset;
                client.mid = _mid;
                if (responseList != null)
                {
                    responseList.Clear();
                }
                terminate_connection = true;    // assume we will drop connection

                if (_sn != 0)
                {
                    // update stats tracking table
                    CalsenseControllers.MsgsReceived((int)_sn, 1);
                }

                //
                // call the appropriate parsing function based on received mid
                //
                if (ParseFunctions.ContainsKey(_mid))
                {
                    beginstamp = DateTime.Now;

                    Task<Tuple<List<byte[]>,String,bool,int>> tx = ParseFunctions[_mid](client, _cts);
                    Tuple<List<byte[]>, String, bool,int> result;
                    result = await tx;
                    responseList = result.Item1;
                    terminate_connection = result.Item3;
                    // if Parser routine is "long-lived" i.e. multi-pass messaging, store
                    // any state/session data it has.
                    if (result.Item4 != Constants.NO_STATE_DATA)
                    {
                        client.sessioninfo = result.Item2;
                        client.internalstate = result.Item4;
                    }
                    // if Parser routine is done with it's state information, re-initialize it
                    // so okay for next message sequence that might look at it.
                    if (result.Item4 == Constants.LAST_STATE_COMPLETE)
                    {
                        client.sessioninfo = null;
                        client.internalstate = 0;
                    }
                }
                else
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "3030", "No parsing routine available for MID " + _mid);
                }

                // if response generated for controller, send it
                if (responseList != null)
                {
#if FLUSHPOSSIBLEGARBAGEDATA
                    //
                    // flush any extra incoming data...
                    // i have seen extra garbage data on the end of some controller requests. this clears that out of the way
                    // 
                    while (_networkStream.DataAvailable)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "3031", "Unexpected data present from controller:");
                        await _networkStream.ReadAsync(buf, 0, 8);
                        Misc.LogMsg(_logger, buf, 8);
                    }
#endif

                    i = 0;
                    j = 0;
                    while(i < responseList.Count)
                    {
                        if (responseList[i].Length > 0)
                        {
                            UInt32 crc;
                            crc = Misc.CRC(responseList[i]);
                            //_logger.WriteMessage(NLog.LogLevel.Debug, "3040", "Calculated CRC: 0x" + crc.ToString("X"));
                            _logger.WriteMessage(NLog.LogLevel.Debug, "3028", "Response to be transmitted to SN:" + client.sn.ToString());

                            // combine preamble, data, CRC and then postamble
                            byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + responseList[i].Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                            System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                            System.Buffer.BlockCopy(responseList[i], 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, responseList[i].Length);

                            byte[] crcbytes;
                            crcbytes = System.BitConverter.GetBytes(crc);
                            if (BitConverter.IsLittleEndian)
                            {
                                //_logger.WriteMessage(NLog.LogLevel.Debug, "3029", "FYI: CRC Bytes swapped to send ");
                                Array.Reverse(crcbytes, 0, crcbytes.Length);
                            }
                            System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + responseList[i].Length, crcbytes.Length);
                            System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + responseList[i].Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);


                            //_logger.WriteMessage(NLog.LogLevel.Info, "3044", "Dump of response bytes raw:" + finalpacket.Length.ToString());
                            //Misc.LogMsg(_logger, finalpacket, finalpacket.Length);

                            try
                            {
                                await SendResponseAsync(finalpacket, (uint)finalpacket.Length);
                            }
                            catch 
                            { 
                                // bail out for any exception
                                _logger.WriteMessage(NLog.LogLevel.Error, "3075", "Exception during transmit to controller SN" + client.sn.ToString());
                                terminate_connection = true;
                                break;
                            }
                            _txBytecount += (int)finalpacket.Length;
                            _logger.WriteMessage(NLog.LogLevel.Debug, "3029", "Total bytes TX'd to client this connection " + _txBytecount);
                            j++;    // number of messages this pass
                            _logger.WriteMessage(NLog.LogLevel.Debug, "3029", "Total messages this sequence " + j.ToString());
                            if (_sn != 0)
                            {
                                // update stats tracking table
                                CalsenseControllers.BytesSent((int)_sn, (int)finalpacket.Length);
                                CalsenseControllers.MsgsSent((int)_sn, 1);
                            }
                        }
                        i++;    // next response if any
                    }


                    DateTime endstamp = DateTime.Now;
                    TimeSpan msgTime = endstamp - beginstamp;

                    _logger.WriteMessage(NLog.LogLevel.Debug, "3033", "Message time total: " + msgTime.TotalSeconds.ToString() + " seconds , mid: " + _mid);
                }

                if (terminate_connection)
                {
                    _logger.WriteMessage(NLog.LogLevel.Info, "3030", "Normal Server termination of connection from server... ");
                    // Close connection and return
                    OurSocketClose();
                    registrationThingy.Dispose();
                    return;
                }

                // prepare to receive additional messages from controller.
                _state = ReceiveState.RX_BEGIN_HEADER;
                offset = 0;
                amount = 0;
                app_offset = 0;
                rxmax = THRU_MID_SIZE;
                msglen.max = 0;
                msglen.min = 0;
            }
        }



        public async Task SendResponseAsync(byte[] data, UInt32 len)
        {
            await _networkStream.WriteAsync(data, 0, (int)len).ConfigureAwait(false);
            await _networkStream.FlushAsync().ConfigureAwait(false);
        }

        public static unsafe void ProofDLLCall(MyLogger _logger)
        {
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];
            byte[] a = new byte[20];
            DUMMY_STRUCT s = new DUMMY_STRUCT();

            // test call to C++ unmanged code
            _logger.WriteMessage(NLog.LogLevel.Debug, "9990", "Dummy DLL call should return 3.5 : " + Add(1.2, 2.3));
            s.header[0] = '1';
            s.header[2] = '2';
            s.footer[0] = 'e';
            s.footer[1] = 'n';
            s.footer[2] = 'd';

            _logger.WriteMessage(NLog.LogLevel.Debug, "9991", "First arg is: " + s);

            _logger.WriteMessage(NLog.LogLevel.Debug, "9992", "Dummy call returns " + Dummy(ref s, 0, a, 0, 0));

            _logger.WriteMessage(NLog.LogLevel.Debug, "9993", "After call footer byte is " + s.footer[0]);

            _logger.WriteMessage(NLog.LogLevel.Debug, "9994", "After call destination byte is " + a[0].ToString("x2"));



        }

        public async Task FactoryResetToOldControllerRequest(MyLogger _log, uint nid, uint serial_number)
        {
            byte[] data;

            _log.WriteMessage(NLog.LogLevel.Info, "382", "Sending a factory reset to NID " + nid.ToString());

            var mem = new MemoryStream(0);
            // PID byte
            mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
            // message class byte
            mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
            // to serial number
            Misc.Uint32ToNetworkBytes(serial_number, mem);
            // network id
            Misc.Uint32ToNetworkBytes(nid, mem);
            // mid
            Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_panel_swap_factory_reset_to_old_panel, mem);

            data = mem.ToArray();

            mem.Dispose();

            UInt32 crc;
            crc = Misc.CRC(data);
            //_log.WriteMessage(NLog.LogLevel.Debug, "3040", "Calculated CRC: 0x" + crc.ToString("X"));
            _log.WriteMessage(NLog.LogLevel.Debug, "3028", "Response to be transmitted to SN:" + _sn.ToString());

            // combine preamble, data, CRC and then postamble
            byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
            System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
            System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

            byte[] crcbytes;
            crcbytes = System.BitConverter.GetBytes(crc);
            if (BitConverter.IsLittleEndian)
            {
                //_logger.WriteMessage(NLog.LogLevel.Debug, "3029", "FYI: CRC Bytes swapped to send ");
                Array.Reverse(crcbytes, 0, crcbytes.Length);
            }
            System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
            System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

            try
            {
                await SendResponseAsync(finalpacket, (uint)finalpacket.Length);
            }
            catch
            {
                // bail out for any exception
                _logger.WriteMessage(NLog.LogLevel.Error, "3075", "Exception during transmit to controller SN " + _sn.ToString());
            }
        }
    }
}
