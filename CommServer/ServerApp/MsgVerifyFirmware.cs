﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using Advantage.Data.Provider;



namespace Calsense.CommServer
{

    public class MsgVerifyFirmware
    {
        //
        // Parses Firmware verification inbond message from controller
        // returns via Tuple:
        //   List of byte arrays, which are 0 to n messages to send as a response
        //   String that will be passed back in if this connection stays up
        //   Boolean as a flag to terminate connection or not
        //   int as an internal state to track thru session (if needed)
        //
        public static async Task<Tuple<List<byte[]>,String,bool,int>> Parse(ClientInfoStruct client, CancellationTokenSource cts)
        {
            byte[] data;
            MyLogger _logger;
            uint size;
            byte[] Buffer = null;
            int offset;
            UInt16 main_date = 0;
            UInt32 main_time = 0;
            UInt16 tp_date;
            UInt32 tp_time;
            DateTime main_dt;
            DateTime tp_dt;
            DateTime file_dt;
            bool terminate_connection = false;
            List<byte[]> rsplist = new List<byte[]>();
            UInt32 filelength = 0;
            String binfile = null;
            String temp_binfile = null;
            int num_files_remaining;
            bool do_scan = false;
            bool do_ack = false;
            byte mid_ack_out_of_date;
            byte mid_ack_up_to_date;


            _logger = new MyLogger(typeof(MsgVerifyFirmware).Name, Misc.ClientLogString(client), Misc.ClientLogDB(client), Constants.CS3000Model);
            _logger.WriteMessage(NLog.LogLevel.Info, "6004", "Processing Firmware Verification message...");
            main_dt = new DateTime();
            tp_dt = new DateTime();
            data = null;
            // get access to runtime control settings via singleton
            WebFormData wf = WebFormData.Instance;

            if(client.sessioninfo != null)
            {
                binfile = client.sessioninfo;   // continue using same file name for processing
            }

            // recover the number of files we have to process on this client session
            num_files_remaining = client.internalstate;

            // what we do next depends on the incoming MID and what we have done so far...
            switch (client.mid)
            {
                case Constants.MID_TO_COMMSERVER_VERIFY_FIRMWARE_VERSION_init_packet:
                    size = client.rxlen;
                    Buffer = client.rx;
                    offset = 0;
                    main_date = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                    main_time = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                    tp_date = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                    tp_time = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                    main_dt = Misc.PackedTimeDatetoDateTime(main_time, main_date);
                    tp_dt = Misc.PackedTimeDatetoDateTime(tp_time, tp_date);
                    do_scan = true;
                    do_ack = true;
                    mid_ack_out_of_date = Constants.MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_OUT_OF_DATE;
                    mid_ack_up_to_date = Constants.MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_UP_TO_DATE;
                    break;

                case Constants.MID_TO_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_init_packet:
                    size = client.rxlen;
                    Buffer = client.rx;
                    offset = 0;
                    main_date = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                    main_time = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                    tp_date = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                    tp_time = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                    main_dt = Misc.PackedTimeDatetoDateTime(main_time, main_date);
                    tp_dt = Misc.PackedTimeDatetoDateTime(tp_time, tp_date);
                    do_scan = true;
                    do_ack = true;
                    mid_ack_out_of_date = Constants.MID_FROM_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_ACK_FW_OUT_OF_DATE;
                    mid_ack_up_to_date = Constants.MID_FROM_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_ACK_FW_UP_TO_DATE;
                    break;

                default:
                    // unexpected message
                    _logger.WriteMessage(NLog.LogLevel.Error, "6090", "Unknown MID in Firmware Verification code: " + client.mid.ToString());
                    mid_ack_out_of_date = Constants.MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_OUT_OF_DATE;
                    mid_ack_up_to_date = Constants.MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_UP_TO_DATE;
                    rsplist = new List<byte[]>();
                    break;
            }

            // if verifying firmware, get most recent .bin file
            if (do_scan)
            {
                // for a hub connected 3000, check against *hub* not against the particular slave
                uint check_nid;
                bool hub = false;
                bool mainneeded = false;
                bool tpneeded = false;
                ActiveLegacyConnection ac = null;
                if (HubWrapper.HubProcessing3000Yes(client.nid, cts))
                {                   
                    ac = HubConnectionTracker.FindACfrom3000NID(client.nid);
                    check_nid = HubWrapper.GetHubID(0, client.nid, cts);                    
                    if(check_nid != client.nid)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "6090", "Since hub connected, will check for forced firmware on *hub* " + check_nid.ToString());
                        hub = true;
                    }
                }
                else
                {
                    check_nid = client.nid;
                }
                // check database for any ForceFirmare settings on this unit. (or its hub if hub connected)
                Task<Tuple<String, DateTime?, UInt32>> gm = Misc.GetMainboardForcedFilename(_logger, cts, check_nid);
                Tuple<String, DateTime?, UInt32> r1 = await gm;
                // r1.Item1 = filename, r1.Item2 = binary timestamp, r1.Item3 = filesize

                num_files_remaining = 0;

                // check for newer App firmware 
                temp_binfile = Misc.FindNewestBinFile(_logger, ConfigurationManager.AppSettings["AppFirmwareFolder"], Constants.MAIN_APP_BUILD_DT_OFFSET, out file_dt, out filelength);
                // override via DB to exact file
                if (ac != null && check_nid == client.nid && ac.Slave3000MainOutofdate)
                {
                    // this is a 3000 hub that has slave devices needing update
                    _logger.WriteMessage(NLog.LogLevel.Info, "6080", "Slave 3000 on this hub needs main update.");
                }
                if (!String.IsNullOrEmpty(r1.Item1) && r1.Item3 > 0)
                {
                    if (main_dt != r1.Item2 || (ac != null && check_nid == client.nid && ac.Slave3000MainOutofdate))
                    {
                        num_files_remaining++;
                        mainneeded = true;
                        binfile = ConfigurationManager.AppSettings["AppFirmwareFolder"] + "\\" + ConfigurationManager.AppSettings["PreReleaseSubfolder"] + "\\" + r1.Item1;
                        _logger.WriteMessage(NLog.LogLevel.Warn, "6080", "Forced App bin file per DB is " + r1.Item1.ToString());
                    }
                    else
                    {
                        // controller already running binary matching database file
                        _logger.WriteMessage(NLog.LogLevel.Info, "6080", "Forced App bin file already in place.");
                    }
                }
                // compare this .bin timestamp with controller info to see if update required...
                else if (temp_binfile != null)
                {
                    // turn into a full path for use from here on out
                    temp_binfile = ConfigurationManager.AppSettings["AppFirmwareFolder"] + "\\" + temp_binfile;
                    if ((file_dt > main_dt) || wf.ForceFirmwareUpdate || (ac != null && check_nid == client.nid && ac.Slave3000MainOutofdate))
                    {
                        binfile = temp_binfile;
                        if (file_dt <= main_dt)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Warn, "6080", "Forcing App update to newest file on hand which is: " + binfile);
                        }
                        else
                        {
                            _logger.WriteMessage(NLog.LogLevel.Info, "6080", "NEWER App bin file found: " + binfile);
                        }
                        num_files_remaining++;
                        mainneeded = true;
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "6081", "Controller already running App code same/newer than " + temp_binfile);

                    }
                }
                else
                {
                    _logger.WriteMessage(NLog.LogLevel.Warn, "6077", "No valid App .bin files found in " + ConfigurationManager.AppSettings["AppFirmwareFolder"]);

                }


                if (ac != null && check_nid == client.nid && ac.Slave3000TPOutofdate)
                {
                    // this is a 3000 hub that has slave devices needing update
                    _logger.WriteMessage(NLog.LogLevel.Info, "6080", "Slave 3000 on this hub needs TP update.");
                }
                // check database for any ForceFirmare settings on this unit.
                Task<Tuple<String, DateTime?, UInt32>> gtp = Misc.GetTPMicroForcedFilename(_logger, cts, check_nid);
                Tuple<String, DateTime?, UInt32> r2 = await gtp;
                // r2.Item1 = filename, r2.Item2 = binary timestamp, r2.Item3 = filesize

                // check for newer TP firmware 
                temp_binfile = Misc.FindNewestBinFile(_logger, ConfigurationManager.AppSettings["TPFirmwareFolder"], Constants.TPMICRO_BUILD_DT_OFFSET, out file_dt, out filelength);
                // compare this .bin timestamp with controller info to see if update required...
                if (!String.IsNullOrEmpty(r2.Item1) && r2.Item3 > 0)
                {
                    if (tp_dt != r2.Item2 || (ac != null && check_nid == client.nid && ac.Slave3000TPOutofdate))
                    {
                        num_files_remaining++;
                        tpneeded = true;
                        binfile = ConfigurationManager.AppSettings["TPFirmwareFolder"] + "\\" + ConfigurationManager.AppSettings["PreReleaseSubfolder"] + "\\" + r2.Item1;
                        // note: if we also have a main app to update, it will happen 2nd.
                        _logger.WriteMessage(NLog.LogLevel.Warn, "6080", "Forced TP bin file per DB is " + r2.Item1.ToString());
 
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "6080", "Forced TP bin file already in place.");
                    }
                }
                else if (temp_binfile != null)
                {
                    // turn into a full path for use from here on out
                    temp_binfile = ConfigurationManager.AppSettings["TPFirmwareFolder"] + "\\" + temp_binfile;
                    if ((file_dt > tp_dt) || wf.ForceTPUpdate || (ac != null && check_nid == client.nid && ac.Slave3000TPOutofdate))
                    {
                        binfile = temp_binfile;
                        if (file_dt <= tp_dt)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Warn, "6080", "Forcing update of TP bin to latest on hand: " + temp_binfile);
                        }
                        else
                        {
                            _logger.WriteMessage(NLog.LogLevel.Info, "6080", "NEWER TP bin file found: " + temp_binfile);
                        }
                        num_files_remaining++;
                        tpneeded = true;
                        // note: if we also have a main app to update, it will happen 2nd.
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "6081", "Controller already running TP code same/newer than " + temp_binfile);

                    }
                }
                else
                {
                    _logger.WriteMessage(NLog.LogLevel.Warn, "6077", "No valid TP .bin files found in " + ConfigurationManager.AppSettings["TPFirmwareFolder"]);

                }
                if (hub)
                {
                    // store the knowledge of what is out of date
                    if (num_files_remaining > 0)
                    {
                        if (ac != null)
                        {
                            if (tpneeded)
                            {
                                ac.Slave3000TPOutofdate = true;
                            }
                            if (mainneeded)
                            {
                                ac.Slave3000MainOutofdate = true;
                            }
                            HubConnectionTracker.TellHubToCheckFirmware(ac, check_nid, cts);    // queue up a check firmware for this slaves hub
                        }
                    }
                }
            }

            if (do_ack)
            {
                if (num_files_remaining > 0)
                {
                    // send the initial ack response if this is a check for updates pass
                    var mem = new MemoryStream(0);
                    // build ack response
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(client.sn, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(client.nid, mem);
                    // mid
                    Misc.Uint16ToNetworkBytes((UInt16)mid_ack_out_of_date, mem);
                    _logger.WriteMessage(NLog.LogLevel.Info, "6071", "Build Verify Firmware Ack telling controller it is Out of Date");
                    data = mem.ToArray();
                    rsplist.Add(data);
                }
                else
                {
                    var mem = new MemoryStream(0);
                    // build ack response
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(client.sn, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(client.nid, mem);
                    // mid
                    Misc.Uint16ToNetworkBytes((UInt16)mid_ack_up_to_date, mem);
                    _logger.WriteMessage(NLog.LogLevel.Info, "6071", "Build Verify Firmware Ack, controller is up to date.");

                    // get the built response to return and be sent
                    data = mem.ToArray();
                    rsplist.Add(data);
                }
            }

            return (Tuple.Create(rsplist, (String)null, terminate_connection, Constants.NO_STATE_DATA));
        }
    }
}
