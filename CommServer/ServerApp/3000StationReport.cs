﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using System.Threading;



//
// our representation of a Station Report Record
//
namespace Calsense.CommServer
{
    class StationReport
    {
        public UInt32 record_start_time { get; set; }        // seconds past midnight

        public Single programmed_irrigation_gallons_irrigated_fl { get; set; }

        public Single manual_program_gallons_fl { get; set; }

        public Single manual_gallons_fl { get; set; }

        public Single walk_thru_gallons_fl { get; set; }

        public Single test_gallons_fl { get; set; }

        public Single rre_gallons_fl { get; set; }

        // ------------------------

        //	In seconds. A long. As a short is too ... short. May need to accomodate 24+ hours.
        public UInt32 programmed_irrigation_seconds_irrigated_ul { get; set; }

        // ------------------------

        // 6/28/2012 rmd : What group ids do we need to keep? Running reports about say the test use
        // for the turf stations belonging to a particular system over a period of time sounds
        // reasonable. So here they are.

        // 6/28/2012 rmd : The log lines need the system and the schedule GID's and that's all. The
        // station by station needs 'em all as you see below. To be used as report data filters and
        // organizers.
        public UInt16 GID_station_group { get; set; }

        // -----------------------------

        public UInt16 record_start_date { get; set; }		//	Date associated with record start. Should occur each and every day!

        // -----------------------------

        public UInt16 rre_seconds_us { get; set; }

        public UInt16 test_seconds_us { get; set; }

        public UInt16 walk_thru_seconds_us { get; set; }

        public UInt16 manual_seconds_us { get; set; }

        public UInt16 manual_program_seconds_us { get; set; }

        // -----------------------------

        // 6/28/2012 rmd : From 0 to 127 representing station 1 through 128 at a box.
        public byte station_number { get; set; }

        // 4/18/2014 rmd : This is the 0..11 index of the box the station belongs to.
        public byte box_index_0 { get; set; }

    }
    class StationReportList
    {
        public IList<StationReport> records;
        public StationReportList()
        {
            records = new List<StationReport>();
        }
        public void LogIt(MyLogger l, NLog.LogLevel lev)
        {
#if LOGGING_STATION_HISTORY
            int i;

            l.WriteMessage(lev, "8001", "-----Dumping Structure from Station Report Message-----");
            l.WriteMessage(lev, "8001", "System GID:" + this.system_gid.ToString());
            for (i = 0; i < this.records.Count; i++ )
            {
                l.WriteMessage(lev, "8001", " ** Record " + i.ToString() + " ** ");
                l.WriteMessage(lev, "8001", "Time:" + this.records[i].time.ToString());
                l.WriteMessage(lev, "8001", "Date:" + this.records[i].date.ToString());
                l.WriteMessage(lev, "8001", " Station Number:" + this.records[i].station_num.ToString());
                l.WriteMessage(lev, "8001", " Box Index0:" + this.records[i].box_index_0.ToString());
                l.WriteMessage(lev, "8001", " Expected Flow:" + this.records[i].expected_flow.ToString());
                l.WriteMessage(lev, "8001", " Derated Expected Flow:" + this.records[i].derated_expected_flow.ToString());
                l.WriteMessage(lev, "8001", " High Limit:" + this.records[i].hi_limit.ToString());
                l.WriteMessage(lev, "8001", " Low Limit:" + this.records[i].lo_limit.ToString());
                l.WriteMessage(lev, "8001", " Portion of actual:" + this.records[i].portion_of_actual.ToString());
                l.WriteMessage(lev, "8001", " Flow check status:" + this.records[i].flow_check_status.ToString());

                l.WriteMessage(lev, "8001", " indicies_out_of_range: " + this.records[i].FRF_NO_CHECK_REASON_indicies_out_of_range.ToString());
                l.WriteMessage(lev, "8001", " station_cycles_too_low: " + this.records[i].FRF_NO_CHECK_REASON_station_cycles_too_low.ToString());
                l.WriteMessage(lev, "8001", " cell_iterations_too_low: " + this.records[i].FRF_NO_CHECK_REASON_cell_iterations_too_low.ToString());
                l.WriteMessage(lev, "8001", " no_flow_meter: " + this.records[i].FRF_NO_CHECK_REASON_no_flow_meter.ToString());
                l.WriteMessage(lev, "8001", " not_enabled_by_user_setting: " + this.records[i].FRF_NO_CHECK_REASON_not_enabled_by_user_setting.ToString());
                l.WriteMessage(lev, "8001", " acquiring_expected: " + this.records[i].FRF_NO_CHECK_REASON_acquiring_expected.ToString());
                l.WriteMessage(lev, "8001", " combo_not_allowed_to_check: " + this.records[i].FRF_NO_CHECK_REASON_combo_not_allowed_to_check.ToString());
                l.WriteMessage(lev, "8001", " unstable_flow: " + this.records[i].FRF_NO_CHECK_REASON_unstable_flow.ToString());
                l.WriteMessage(lev, "8001", " cycle_time_too_short: " + this.records[i].FRF_NO_CHECK_REASON_cycle_time_too_short.ToString());
                l.WriteMessage(lev, "8001", " irrigation_restart: " + this.records[i].FRF_NO_CHECK_REASON_irrigation_restart.ToString());
                l.WriteMessage(lev, "8001", " not_supposed_to_check: " + this.records[i].FRF_NO_CHECK_REASON_not_supposed_to_check.ToString());
                l.WriteMessage(lev, "8001", " zero_flow_rate: " + this.records[i].FRF_NO_UPDATE_REASON_zero_flow_rate.ToString());
                l.WriteMessage(lev, "8001", " thirty_percent: " + this.records[i].FRF_NO_UPDATE_REASON_thirty_percent.ToString());

            }
#endif
        }
    }

}
