﻿using System;
using Calsense.CommServer;
using System.Threading.Tasks;
using Calsense.Logging;

public static class Extensions
{
    //private static MyLogger _log;

    public static async Task<Single> isNaN(this Single value,
                               System.Threading.CancellationTokenSource _cts, UInt32 nid, String fieldName)
    {
        if (!Single.IsNaN(value))
        {
            return value;
        }

        await Misc.AddAlert(_cts, nid, fieldName + " Out of Range", true, true);

        return 0;
    }


    public static async Task<double> isNaN(this double value,
                           System.Threading.CancellationTokenSource _cts, UInt32 nid, String fieldName)
    {
        if (!double.IsNaN(value))
        {
            return value;
        }

        await Misc.AddAlert(_cts, nid, fieldName + " Out of Range", true, true);

        return 0;
    }
}
