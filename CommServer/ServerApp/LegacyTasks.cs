﻿//
//
// LegacyTasks class 
//
// Handles communications with Legacy Controllers...
// two broad types of communications:
// 1) Regular/periodic communications occur at particular time-of-day/day-of-month
// 2) UI initiated jobs showing up in the IHSFY_table
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using ServerApp;
using System.Collections.Concurrent;
using System.Configuration;
using Advantage.Data.Provider;
using System.Data;

namespace Calsense.CommServer
{
    public struct ActionArgs
    {
        public uint cid { get; set; }   // controller id
        public String hostname { get; set; }
        public uint connectiontype { get; set; }
        public uint timezonekey { get; set; }
        public String adr { get; set; }
        public uint cityid { get; set; }    // used to match hubs that share an RF space in same city
        public uint jobid { get; set; }
        public Action<uint, uint> periodicaction { get; set; }  // used to hold 3000 IHSFY queued jobs, otherwise null
        public Func<uint, uint, int>  ihsfyaction { get; set; }        // used to hold 3000 periodic queued jobs, otherwise null
        public Action<ActionArgs> legacyaction {get; set; }       // used to hold 2000 queued job, otherwise null
    }

    public class ControllerWeatherInfo
    {
        public double? lat {get; set; }         // lat, lon for given controller - used to call WeatherSense API
        public double? lon {get; set; }
        public int? rainID { get; set; }        // rainID from controllers DB entry
        public int? ETID { get; set; }          // ETID from controllers DB entry
        public bool rainIsNID { get; set; }     // if above number is an NID
        public bool ETIsNID { get; set; }       // if above number is an NID
        public int ETStatus { get; set; }      // if controller gets ET from another controller, this is data from ETShare 
        public int ETValue { get; set; }
        public int RainStatus { get; set; }     // if controller gets Rain from another controller, this is the data from RainShare
        public int RainValue { get; set; }
    }

    public static class LegacyTasks
    {

        public const int MID_CMOS_TO_PC = (1002);
        public const int MID_CMOS_TO_PC_RESP = (1003);

        public const int MID_CMOS_TO_CONTROLLER = (1004);
        public const int MID_CMOS_TO_CONTROLLER_RESP = (1005);

        public const int MID_PDATA_TO_PC = (1006);
        public const int MID_PDATA_TO_PC_RESP = (1007);

        public const int MID_PDATA_TO_CONTROLLER = (1008);
        public const int MID_PDATA_TO_CONTROLLER_RESP = (1009);

        public const int MID_GETALERTS = (1010);
        public const int MID_GETALERTS_RESP = (1011);

        public const int MID_SETTIMEDATE = (1012);
        public const int MID_SETTIMEDATE_SUCCESS_RESP = (1013);
        public const int MID_SETTIMEDATE_FAILED_RESP = (1014);  // user is viewing screen that prevents setting date/time

        public const int MID_CLEARMLB = (1016);
        public const int MID_CLEARMLB_SUCCESS_RESP = (1017);    // 500 series
        public const int MID_CLEARMLB_FAILED_RESP = (1018);     // 500 series
        public const int MID_CLEARMLB_RESP = (1019);            // 600 series

        public const int MID_SEND_FLOW_RECORDER_RECORDS_TO_PC = (1110);
        public const int MID_SEND_FLOW_RECORDER_RECORDS_TO_PC_RESP = (1111);

        public const int MID_SETHO_ALL_STATIONS = (1020);
        public const int MID_SETHO_BY_PROG = (1021);
        public const int MID_SETHO_BY_STATION = (1022);
        public const int MID_SETHO_RESP = (1023);

        public const int MID_SETNOWDAYS_ALL_STATIONS = (1024);
        public const int MID_SETNOWDAYS_BY_PROG = (1025);
        public const int MID_SETNOWDAYS_BY_STATION = (1026);

        public const int MID_TURNON = (1028);
        public const int MID_TURNON_RESP = (1029);

        public const int MID_TURNOFF = (1030);
        public const int MID_TURNOFF_RESP = (1031);

        public const int MID_PASSWORDS_RESET = (1032);
        public const int MID_PASSWORDS_RESET_RESP = (1033);

        public const int MID_PASSWORDS_TO_CONTROLLER = (1036);
        public const int MID_PASSWORDS_TO_CONTROLLER_RESP = (1037);

        public const int MID_PASSWORDS_UNLK = (1038);
        public const int MID_PASSWORDS_UNLK_RESP = (1039);

        public const int MID_SHARING_GET_ET = (1040);
        public const int MID_SHARING_GET_ET_RESP_OK = (1041);
        public const int MID_SHARING_GET_ET_RESP_NOT_A_G = (1042);// No content; no –G option
        public const int MID_SHARING_GET_ET_RESP_GAGE_NOT_IN_USE = (1043);// No content; –G exists but not in use

        public const int MID_SHARING_GET_RAIN = (1044);
        public const int MID_SHARING_GET_RAIN_RESP_OK = (1045);
        public const int MID_SHARING_GET_RAIN_RESP_NOT_AN_RB = (1046); // No content; no –RB option

        public const int MID_SHARING_ETRAIN_TO_CONTROLLER = (1048);
        public const int MID_SHARING_ETRAIN_TO_CONTROLLER_RESP_OK = (1049); // content: DATE_TIME – the current date/time
        public const int MID_SHARING_ETRAIN_TO_CONTROLLER_RESP_ERROR = (1050); // content: ensigned char – error bit field, DATE_TIME – the current date/time

        public const int MID_SHARING_RAIN_POLL = (1060);
        public const int MID_SHARING_RAIN_POLL_RESP_NOT_RB = (1061);	// No –RB option
        public const int MID_SHARING_RAIN_POLL_RESP_NO_RAIN = (1062);	// No rain measured
        public const int MID_SHARING_RAIN_POLL_RESP_RAIN = (1063);      // •	unsigned short – the date the rain was collected for. 

        public const int MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER = (1070);
        public const int MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER_RESP_OK = (1071);
        public const int MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER_DATES_DONT_MATCH = (1072);

        public const int MID_SETNOWDAYS_NUMBER_OF_DAYS_OUT_OF_RANGE_RESP = (1120);

        public const int MID_CONTROLLER_REPORT_DATA_TO_PC = (1210);
        public const int MID_CONTROLLER_REPORT_DATA_TO_PC_RESP = (1211);

        public const int MID_LEGACY_STATION_REPORT_DATA_TO_PC = (1220);
        public const int MID_LEGACY_STATION_REPORT_DATA_TO_PC_RESP = (1221);

        public const int MID_MVOR_TO_CONTROLLER = (1230);
        public const int MID_MVOR_TO_CONTROLLER_RESP = (1231);

        public const int MID_LIGHTS_TO_PC = (1240);
        public const int MID_LIGHTS_TO_PC_RESP = (1241);

        public const int MID_LIGHTS_TO_CONTROLLER = (1242);
        public const int MID_LIGHTS_TO_CONTROLLER_RESP = (1243);

        public const int MID_MANUALSTUFF_TO_PC = (1250);
        public const int MID_MANUALSTUFF_TO_PC_RESP = (1251);
        public const int MID_MANUALSTUFF_TO_PC_RESP_NODATA = (1252);

        public const int MID_MANUALSTUFF_TO_CONTROLLER = (1255);
        public const int MID_MANUALSTUFF_TO_CONTROLLER_RESP = (1256);
        public const int MID_MANUALSTUFF_TO_CONTROLLER_RESP_NOTOK01 = (1257);	// Manual Program A data length invalid
        public const int MID_MANUALSTUFF_TO_CONTROLLER_RESP_NOTOK02 = (1258);	// Manual Program B data length invalid
        public const int MID_MANUALSTUFF_TO_CONTROLLER_RESP_NOTOK04 = (1260);	// Walk-Thru data length invalid
        public const int MID_MANUALSTUFF_TO_CONTROLLER_RESP_NOTOK08 = (1246);	// Scheduled Use of Hold-Over data length invalid
        public const int MID_MANUALSTUFF_TO_CONTROLLER_RESP_NOTOK10 = (1265);	// Master Valve Override Schedule data length invalid

        public const int MID_LOGLINES_CONTROLLER_TO_PC = (1200);
        public const int MID_LOGLINES_CONTROLLER_TO_PC_RESP = (1201);

        public const int MID_DERATE_TABLE_TO_CONTROLLER = (1446);
        public const int MID_DERATE_TABLE_TO_CONTROLLER_RESP = (1447);


        public const int MID_SETNOWDAYS_BY_PTAG = (1452);
        public const int MID_SETNOWDAYS_BY_PTAG_RESP = (1453);


        public const int MID_SETSTARTTIME_BY_PTAG = (1455);
        public const int MID_SETSTARTTIME_BY_PTAG_RESP = (1456);
        public const int MID_SETSTARTTIME_TIME_OUT_OF_RANGE_RESP = (1457);
        public const int MID_SETSTARTTIME_MONTH_OUT_OF_RANGE_RESP = (1458);
        public const int MID_SETSTARTTIME_12_MONTH_CUR_MONTH_CHANGED_RESP = (1459);

        public const int MID_SETSTOPTIME_BY_PTAG = (1460);   
        public const int MID_SETSTOPTIME_BY_PTAG_RESP = (1461);	    
        public const int MID_SETSTOPTIME_TIME_OUT_OF_RANGE_RESP = (1462);

        public const int MID_SETHO_BY_PTAG = (1465);
        public const int MID_SETHO_BY_PTAG_RESP = (1466);
        public const int MID_SETHO_HO_OUT_OF_RANGE_RESP = (1467);



        public const int MID_SETPERCENTADJUST_BY_PTAG = (1470);
        public const int MID_SETPERCENTADJUST_BY_PTAG_RESP = (1471);
        public const int MID_SETPERCENTADJUST_PERCENTAGE_OUT_OF_RANGE_RESP = (1472);
        public const int MID_SETPERCENTADJUST_DAYS_OUT_OF_RANGE_RESP = (1473);
        public const int MID_PTAG_DOES_NOT_EXIST_RESP = (1475);


        public const int MID_LIGHTS_COMMAND_FROM_PC = (1485);
	    public const int MID_LIGHTS_COMMAND_FROM_PC_GOOD_RESP = (1486);
	    public const int MID_LIGHTS_COMMAND_FROM_PC_BAD_RESP = (1487);	// no lights output set
        public const int MID_LIGHTS_COMMAND_FROM_PC_BAD_TIME_RESP = (1488);	// invalid time


        public const int MID_SEND_DMC_RECORDER_RECORDS_TO_PC = (1490);
        public const int MID_SEND_DMC_RECORDER_RECORDS_TO_PC_RESP = (1491);

        // Direct Access
        public const int MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER = (1400);
        public const int MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER_RESP_START = (1401);
        public const int MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER_RESP_STOP = (1402);
        public const int MID_DIRECTACCESS_SCREEN_UPDATE_TO_PC = (1403);
        public const int MID_DIRECTACCESS_STARTTIMER_TO_CONTROLLER = (1405);
        public const int MID_DIRECTACCESS_CHANGESCREEN_TO_CONTROLLER = (1406);
        public const int MID_DIRECTACCESS_STOPKEY_TO_CONTROLLER = (1407);
        public const int MID_DIRECTACCESS_TEST_STATION_TO_CONTROLLER = (1408);
        public const int MID_DIRECTACCESS_MANUAL_STATION_TO_CONTROLLER = (1411);
        public const int MID_DIRECTACCESS_MANUAL_PROG_TO_CONTROLLER = (1412);
        public const int MID_DIRECTACCESS_MANUAL_ALL_TO_CONTROLLER = (1413);
        public const int MID_DIRECTACCESS_TURN_ON_TO_CONTROLLER = (1414);
        public const int MID_DIRECTACCESS_TURN_OFF_TO_CONTROLLER = (1415);
        public const int MID_DIRECTACCESS_GOTO_STATION_TO_CONTROLLER = (1416);
        public const int MID_DIRECTACCESS_MVOR_TO_CONTROLLER = (1417);
        public const int MID_DIRECTACCESS_REFRESH_REQUEST_TO_CONTROLLER = (1418);

        public const int MID_COMMSERVER_REQUEST_TO_SEND_SERIAL_NUMBER = (1600);  // Sent whent inbound 2000 connects TBD
        public const int MID_COMMSERVER_REQUEST_TO_SEND_SERIAL_NUMBER_RESP = (1601);

        
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GetTimeZoneStr(uint TimeZoneID);

        public static short SequenceNumber { get; set;}

        //
        // Define Legacy actions that happen at a particular time of day - which is
        // picked up from database table CS3000CommServerSettings at runtime.
        //
        class TimeTriggerClass
        {
            public TimeSpan? StartTime { get; set; }
            public TimeSpan? RepeatDuration { get; set; }
            public TimeSpan? RepeatFrequency { get; set; }
            public int? DayOfWeek { get; set; }
            public String SqlDayOfWeekColumnName { get; set; }
            public String SqlStartTimeColumnName { get; set; }
            public String SqlStopTimeColumnName { get; set; }
            public String SqlFrequencyColumnName { get; set; }
            public String SqlCompanyColumnName { get; set; }
            public int Priority { get; set; }  // higher priority actions happen first if multiple queued up on same network connection (Get CMOS is a 1000 priority)
            public Action<ActionArgs> ProcessingFunction { get; set; } // controller id, hostname, connectiontype, timezonekey
            public String CIDQuery { get; set; }
            public TimeTriggerClass(TimeSpan? tsStart, TimeSpan? tsDuration, TimeSpan? tsFreq, int? dayOfWeek, String sDayOfWeek, String sStart, String sStop, String sFreq, String sCompCol, int pri, Action<ActionArgs> pf, String q)
            {
                StartTime = tsStart;
                RepeatDuration = tsDuration;
                RepeatFrequency = tsFreq;
                SqlDayOfWeekColumnName = sDayOfWeek;
                SqlStartTimeColumnName = sStart;
                SqlStopTimeColumnName = sStop;
                SqlFrequencyColumnName = sFreq;
                SqlCompanyColumnName = sCompCol;
                ProcessingFunction = pf;
                CIDQuery = q;
                Priority = pri;
                DayOfWeek = dayOfWeek;
            }
        }

        class DerivedTriggerDataClass
        {
            public TimeTriggerClass entry {get; set;}
            public int CompanyID {get; set;}
            public string TimeZoneName { get; set; }
        }

        static List<DerivedTriggerDataClass> DerivedTriggerData = new List<DerivedTriggerDataClass>();

        //
        // Define the known periodic legacy functions here:
        //
        static TimeTriggerClass[] BaseTimeTriggerData = new TimeTriggerClass[]
        {
            //
            // Times are "local" times at the controller, so controllers in different
            // time zones will be acted upon at different commserver times
            //
            // times will be filled in via SQL query of CS3000CommServerSettings table
            new TimeTriggerClass(null, null, null, null, "LegacyDayOfWeekTask", "SetControllersClock", null, null, null, 500, ActionSetControllerClock, "SELECT n.ControllerID, IIF(n.PhoneNumber='' OR n.PhoneNumber IS NULL, '0.0.0.0',n.PhoneNumber), n.ConnectionType, n.Address, z.CompanyID, NoOfControllersInLegacyChain3(n.PhoneNumber,n.Address,n.HubID), n.UseHubType, ISNULL(Left(n.SoftwareVersion,1),'0'),ISNULL(RepeaterInUse,false),ISNULL(TxTimerAddon,0)  "
                                + " FROM Controllers n  JOIN Companies s ON n.CompanyID=s.CompanyID JOIN ControllerSites z ON n.SiteID = z.SiteID "
                                + " WHERE n.Deleted = false AND s.Deleted = false AND n.Communicate=true AND n.Model in (5,6) AND s.CommServerID=" + Constants.CommServerID + " AND s.Company_TimeZone = "),

            new TimeTriggerClass(null, null, null, null, null, null, null, null, "LegacyGetAlerts", 40, ActionGetAlerts, "SELECT n.ControllerID, IIF(n.PhoneNumber='' OR n.PhoneNumber IS NULL, '0.0.0.0',n.PhoneNumber), n.ConnectionType, n.Address, z.CompanyID, NoOfControllersInLegacyChain3(n.PhoneNumber,n.Address,n.HubID), n.UseHubType, ISNULL(Left(n.SoftwareVersion,1),'0'),ISNULL(RepeaterInUse,false),ISNULL(TxTimerAddon,0) "
                                + " FROM Controllers n  JOIN Companies s ON n.CompanyID=s.CompanyID  JOIN ControllerSites z ON n.SiteID = z.SiteID "
                                + " WHERE n.Deleted = false AND s.Deleted = false AND n.Communicate=true AND n.Model in (1,5,6) AND s.CommServerID=" + Constants.CommServerID + " AND s.Company_TimeZone = "),

            new TimeTriggerClass(null, null, null, null, null, null, null, null, "LegacyGetReport", 20, ActionGetReports, "SELECT n.ControllerID, IIF(n.PhoneNumber='' OR n.PhoneNumber IS NULL, '0.0.0.0',n.PhoneNumber), n.ConnectionType, n.Address, z.CompanyID, NoOfControllersInLegacyChain3(n.PhoneNumber,n.Address,n.HubID), n.UseHubType, ISNULL(Left(n.SoftwareVersion,1),'0'),ISNULL(RepeaterInUse,false),ISNULL(TxTimerAddon,0) "
                                + " FROM Controllers n  JOIN Companies s ON n.CompanyID=s.CompanyID  JOIN ControllerSites z ON n.SiteID = z.SiteID "
                                + " WHERE n.Deleted = false AND s.Deleted = false AND n.Communicate=true AND n.Model in (1,5,6) AND s.CommServerID=" + Constants.CommServerID + " AND s.Company_TimeZone = "),

            new TimeTriggerClass(null, null, null, null, null, null, null, null, "LegacyGetStationHistory", 30, ActionGetStationHistory, "SELECT n.ControllerID, IIF(n.PhoneNumber='' OR n.PhoneNumber IS NULL, '0.0.0.0',n.PhoneNumber), n.ConnectionType, n.Address, z.CompanyID, NoOfControllersInLegacyChain3(n.PhoneNumber,n.Address,n.HubID), n.UseHubType, ISNULL(Left(n.SoftwareVersion,1),'0'),ISNULL(RepeaterInUse,false),ISNULL(TxTimerAddon,0) "
                                + " FROM Controllers n  JOIN Companies s ON n.CompanyID=s.CompanyID  JOIN ControllerSites z ON n.SiteID = z.SiteID "
                                + " WHERE n.Deleted = false AND s.Deleted = false AND n.Communicate=true AND n.Model in (1,5,6) AND s.CommServerID=" + Constants.CommServerID + " AND s.Company_TimeZone = "),

            new TimeTriggerClass(null, null, null, null, null, "LegacyGetRainET", null, null, null, 210, ActionGetRain, "SELECT ControllerID, IIF(PhoneNumber='' OR PhoneNumber IS NULL, '0.0.0.0',PhoneNumber), ConnectionType, Address, CompanyID, NoOfControllersInLegacyChain3, UseHubType, SoftwareVersion,RepeaterInUse,TxTimerAddon  "
                                + " FROM Legacy_GetRainfromControllers3 s WHERE CommServerID=" + Constants.CommServerID + " AND Company_TimeZone = "),

            new TimeTriggerClass(null, null, null, null, null, "LegacyGetRainET", null, null, null, 200, ActionGetET, "SELECT ControllerID, IIF(PhoneNumber='' OR PhoneNumber IS NULL, '0.0.0.0',PhoneNumber), ConnectionType, Address, CompanyID, NoOfControllersInLegacyChain3, UseHubType, SoftwareVersion,RepeaterInUse,TxTimerAddon "
                                + " FROM Legacy_GetETfromControllers3 s WHERE CommServerID=" + Constants.CommServerID + " AND Company_TimeZone = "),

            new TimeTriggerClass(null, null, null, null, null, "SendWeatherData", null, null, null, 200, ActionSendWeatherData, "SELECT n.ControllerID, IIF(n.PhoneNumber='' OR n.PhoneNumber IS NULL, '0.0.0.0',n.PhoneNumber), n.ConnectionType, n.Address, z.CompanyID, NoOfControllersInLegacyChain3(n.PhoneNumber,n.Address,n.HubID), n.UseHubType, ISNULL(Left(n.SoftwareVersion,1),'0'),ISNULL(RepeaterInUse,false),ISNULL(TxTimerAddon,0)  "
                                + " FROM Controllers n  JOIN Companies s ON n.CompanyID=s.CompanyID  JOIN ControllerSites z ON n.SiteID = z.SiteID "
                                + " WHERE (n.ETSharingControllerID>-1 OR n.RainSharingControllerID>-1 OR n.ETSharingNetworkID>-1 OR n.RainSharingNetworkID>-1) AND n.Communicate=true AND n.Deleted = false AND s.Deleted = false AND n.Model in (5,6) AND s.CommServerID=" + Constants.CommServerID.ToString() + " AND s.Company_TimeZone = "),

            new TimeTriggerClass(null, null, null, null, null, "LegacyRainPollStart", "LegacyRainPollStop", "LegacyRainPollIntervalMins", null, 90, ActionPollRain, "SELECT ControllerID, IIF(PhoneNumber='' OR PhoneNumber IS NULL, '0.0.0.0',PhoneNumber), ConnectionType, Address, CompanyID, NoOfControllersInLegacyChain3, UseHubType, SoftwareVersion,RepeaterInUse,TxTimerAddon  "
                                + " FROM Legacy_GetRainPollingfromControllers3 s WHERE CommServerID=" + Constants.CommServerID + " AND Company_TimeZone = "),

            new TimeTriggerClass(null, null, null, null, "LegacyDayOfWeekTask", "LegacyGetCMOS", null, null, null, 60, ActionGetCMOS, "SELECT n.ControllerID, IIF(n.PhoneNumber='' OR n.PhoneNumber IS NULL, '0.0.0.0',n.PhoneNumber), n.ConnectionType, n.Address, z.CompanyID, NoOfControllersInLegacyChain3(n.PhoneNumber,n.Address,n.HubID), n.UseHubType, ISNULL(Left(n.SoftwareVersion,1),'0'),ISNULL(RepeaterInUse,false),ISNULL(TxTimerAddon,0) "
                                + " FROM Controllers n  JOIN Companies s ON n.CompanyID=s.CompanyID JOIN ControllerSites z ON n.SiteID = z.SiteID "
                                + " WHERE n.Deleted = false AND s.Deleted = false AND n.Communicate=true AND n.Model in (5,6) AND s.CommServerID=" + Constants.CommServerID + " AND s.Company_TimeZone = "),

            // Task to get pdata for all the controllers don't have any pdata record in the database
            // I've hard coded NoOfControllersInLegacyChain3 to the default number "8" as the function returns "8" anyway
            new TimeTriggerClass(null, null, null, null, "LegacyDayOfWeekTask", "LegacyGetCMOS", null, null, null, 50, ActionGetProgramData, "SELECT n.ControllerID, IIF(n.PhoneNumber='' OR n.PhoneNumber IS NULL, '0.0.0.0',n.PhoneNumber), n.ConnectionType, n.Address, z.CompanyID, 8, n.UseHubType, ISNULL(Left(n.SoftwareVersion,1),'0'),RepeaterInUse,TxTimerAddon " 
                                + " FROM Controllers n  JOIN Companies s ON n.CompanyID=s.CompanyID JOIN ControllerSites z ON n.SiteID = z.SiteID "
                                + " WHERE n.ControllerID NOT IN (SELECT DISTINCT c.ControllerID FROM Controllers c JOIN BulkPgmData1 b ON b.ControllerID=c.ControllerID WHERE Communicate=true and deleted=false and model IN (5,6))"
                                + " AND n.Deleted = false AND s.Deleted = false AND n.Communicate=true AND n.Model in (5,6) AND s.CommServerID=" + Constants.CommServerID + " AND s.Company_TimeZone = "),
        };

        //
        // Define Legacy jobs that happen per rows in the IHSFY_jobs table:
        //
        struct JobListStruct
        {
            public Int32 MessageID { get; set; }            // message ID (MID)
            public TimeSpan RetryPeriod { get; set; }          // seconds between retries
            public TimeSpan Lifetime { get; set; }          // amount of time job can stay alive in table
            public bool MobileJobFunction { get; set; }     // true if this is considered a "mobile" job
            public int Priority { get; set; }
            public Action<ActionArgs> JobFunction { get; set; }   // controller ID, hostname, job number, connection type, return
            public JobListStruct(Int32 mid, TimeSpan retry, TimeSpan life, bool m, int pri, Action<ActionArgs> jf)
                : this()
            {
                MessageID = mid;
                RetryPeriod = retry;
                Lifetime = life;
                MobileJobFunction = m;
                JobFunction = jf;
                Priority = pri;
            }
        }

        //
        // make sure the lifetime is at least as long as the frequency the job table is
        // checked - or else jobs will expire without getting attempted!
        // note: we can set the "retry" to a long time, like 8 hours to effectively not retry a job at this level
        //
        static JobListStruct[] JobListData = new JobListStruct[]
        {
            new JobListStruct(MID_TURNON, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSendTurnOn),
            new JobListStruct(MID_TURNOFF, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSendTurnOff),
            new JobListStruct(MID_PASSWORDS_RESET, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSendPasswordReset),
            new JobListStruct(MID_PASSWORDS_UNLK, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSendPasswordUnlock),   
            new JobListStruct(MID_CLEARMLB, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSendClearMLB),    
            new JobListStruct(MID_SETHO_ALL_STATIONS, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSetHOAllStations), 
            new JobListStruct(MID_SETHO_BY_PROG, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSetHOByProgram), 
            new JobListStruct(MID_SETHO_BY_STATION, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSetHOByStation), 
            new JobListStruct(MID_SETNOWDAYS_ALL_STATIONS, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSetNOWAllStations),
            new JobListStruct(MID_SETNOWDAYS_BY_PROG, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSetNOWByProgram),
            new JobListStruct(MID_SETNOWDAYS_BY_STATION, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSetNOWByStation),
            new JobListStruct(MID_SEND_FLOW_RECORDER_RECORDS_TO_PC, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionGetFlowRecords),
            new JobListStruct(MID_MVOR_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionMasterValveOverride),     
            new JobListStruct(MID_DERATE_TABLE_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionResetFlowTable),               
            new JobListStruct(MID_LIGHTS_COMMAND_FROM_PC, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionLightsOverride),   
            new JobListStruct(MID_DIRECTACCESS_TEST_STATION_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 300, Action_DirectAccess_TestStation),
            new JobListStruct(MID_DIRECTACCESS_STOPKEY_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 300, Action_DirectAccess_StopIrrigation),
            new JobListStruct(MID_SETPERCENTADJUST_BY_PTAG, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionPercentAdjustProgramTag),
            new JobListStruct(MID_SETHO_BY_PTAG, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionClearHoldoverProgramTag),
            new JobListStruct(MID_SETSTOPTIME_BY_PTAG, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSetStopTimesProgramTag),
            new JobListStruct(MID_SETSTARTTIME_BY_PTAG, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSetStartTimesProgramTag),
            new JobListStruct(MID_SETNOWDAYS_BY_PTAG, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionNoWaterDaysProgramTag),
            new JobListStruct(MID_LIGHTS_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSendLights),
            new JobListStruct(MID_LIGHTS_TO_PC , TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionGetLights),
            new JobListStruct(MID_MANUALSTUFF_TO_CONTROLLER , TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSendManualPrograms),      
            new JobListStruct(MID_MANUALSTUFF_TO_PC , TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionGetManualPrograms),               
            new JobListStruct(MID_PDATA_TO_CONTROLLER , TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 120, ActionSendProgramData),   
            new JobListStruct(MID_PDATA_TO_PC , TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 120, ActionGetProgramData),   
            new JobListStruct(MID_CMOS_TO_CONTROLLER , TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSendCMOS),        
            new JobListStruct(MID_PASSWORDS_TO_CONTROLLER , TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSendPasswords),
            new JobListStruct(MID_GETALERTS, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionGetAlerts),
            new JobListStruct(MID_SETTIMEDATE, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionSetControllerClockByTimeZone),
            new JobListStruct(MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 250, ActionSendRainShutdown),            
            new JobListStruct(MID_LOGLINES_CONTROLLER_TO_PC, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionGetStationHistory),
            new JobListStruct(MID_CMOS_TO_PC, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 130, ActionGetCMOS),
            new JobListStruct(MID_CONTROLLER_REPORT_DATA_TO_PC, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 100, ActionGetReports),
            new JobListStruct(Constants.MID_TO_COMMSERVER_update_and_send_hub_list, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 15, Action2000HubChange), // low priority so other stuff finishes first
            new JobListStruct(MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 350, Action_DirectAccess_Start), // higher priority that'll start the direct access session before the other command
            new JobListStruct(MID_DIRECTACCESS_REFRESH_REQUEST_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false,300, Action_DirectAccess_Refresh),
            new JobListStruct(MID_DIRECTACCESS_CHANGESCREEN_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 300, Action_DirectAccess_ChangeScreen),
            new JobListStruct(MID_DIRECTACCESS_GOTO_STATION_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 300, Action_DirectAccess_GoToStation),
            new JobListStruct(MID_DIRECTACCESS_TURN_OFF_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 300, Action_DirectAccess_TurnOff),
            new JobListStruct(MID_DIRECTACCESS_TURN_ON_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 300, Action_DirectAccess_TurnOn),
            new JobListStruct(MID_DIRECTACCESS_MVOR_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 300, Action_DirectAccess_MVOR),
            new JobListStruct(MID_DIRECTACCESS_MANUAL_STATION_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 300, Action_DirectAccess_ManualByStation),
            new JobListStruct(MID_DIRECTACCESS_MANUAL_PROG_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 300, Action_DirectAccess_ManualByProgramStation),
            new JobListStruct(MID_DIRECTACCESS_MANUAL_ALL_TO_CONTROLLER, TimeSpan.FromHours(8.0), TimeSpan.FromHours(5.0),  false, 300, Action_DirectAccess_ManualAll),
         };

        //
        // separate conversion routines here - Note that Legacy controller
        // internal multibyte values are placed in messages in *reverse*
        // endian compared to CS3000 method.
        //
        //
        public static void LegacyUint16ToNetworkBytes(UInt16 val, MemoryStream mem)
        {
            if (!BitConverter.IsLittleEndian)
            {
                mem.WriteByte((byte)val);
                mem.WriteByte((byte)(val >> 8));
            }
            else
            {
                mem.WriteByte((byte)(val >> 8));
                mem.WriteByte((byte)val);
            }
            return;
        }
        public static void LegacyUint32ToNetworkBytes(UInt32 val, MemoryStream mem)
        {

            if (!BitConverter.IsLittleEndian)
            {
                mem.WriteByte((byte)val);
                mem.WriteByte((byte)(val >> 8));
                mem.WriteByte((byte)(val >> 16));
                mem.WriteByte((byte)(val >> 24));
            }
            else
            {
                mem.WriteByte((byte)(val >> 24));
                mem.WriteByte((byte)(val >> 16));
                mem.WriteByte((byte)(val >> 8));
                mem.WriteByte((byte)val);
            }
            return;
        }

        //
        // return 5 or 6 indicating series of given legacy controller
        //
        static int LegacyControllerSeries(uint cid)
        {
            int series = 0;
            // record in database
            ServerDB db = new ServerDB(_cts);

            AdsConnection conn = db.OpenDBConnectionNotAsync("");
            if (conn != null)
            {
                try
                {
                    AdsCommand cmd = new AdsCommand("SELECT Model from Controllers where Deleted = false and ControllerID = " + cid.ToString(), conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                    var result = cmd.ExecuteScalar();
                    cmd.Unprepare();

                    if(result != null)
                    {
                        series = Convert.ToInt32(result);
                    }
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "14394", "Database exception during Legacy query: " + e.ToString());
                }
                db.CloseDBConnection(conn, "");
                
            }
            return (series);
        }

        //
        // return true if controller has F interface option
        //
        public static bool LegacyControllerHasFInterface(uint cid)
        {
            bool FInterface = false;
            // record in database
            ServerDB db = new ServerDB(_cts);

            AdsConnection conn = db.OpenDBConnectionNotAsync("");
            if (conn != null)
            {
                try
                {
                    AdsCommand cmd = new AdsCommand("SELECT FInterface from CMOS where ControllerID = " + cid.ToString(), conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                    var result = cmd.ExecuteScalar();
                    cmd.Unprepare();

                    if (result != null)
                    {
                        FInterface = Convert.ToBoolean(result);
                    }
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "14391", "Database exception during Legacy query: " + e.ToString());
                }
                db.CloseDBConnection(conn, "");

            }
            return (FInterface);
        }

        //
        // get the newest item datetime field for given table
        //
        static DateTime GetLastDateTimeFromTable(String tablename, String colname, uint cid)
        {
            DateTime dt = DateTime.Parse("1900/01/01 00:00:00");

            // record in database
            ServerDB db = new ServerDB(_cts);

            AdsConnection conn = db.OpenDBConnectionNotAsync(cid.ToString());
            if (conn != null)
            {
                try
                {
                    AdsCommand cmd = new AdsCommand("SELECT TOP 1 " + colname + " from " + tablename + " WHERE ControllerID=" + cid.ToString() + " ORDER BY " + colname + " DESC", conn);
                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText, cid.ToString());
                    var result = cmd.ExecuteScalar();
                    cmd.Unprepare();

                    if (result != null)
                    {
                        if (result != DBNull.Value)
                        {
                            dt = Convert.ToDateTime(result);
                        }
                    }
                }
                catch (Exception e)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "14391", "Database exception during Legacy query: " + e.ToString(), cid.ToString());
                }

                db.CloseDBConnection(conn, cid.ToString());
            }

            return (dt);
        }

        //
        // get the newest item datetime field for field in Controllers table
        //
        public static DateTime GetLastDateTime(String colname, uint cid)
        {
            DateTime dt = DateTime.Parse("1900/01/01 00:00:00");

            // record in database
            ServerDB db = new ServerDB(_cts);

            AdsConnection conn = db.OpenDBConnectionNotAsync(cid.ToString());
            if (conn != null)
            {
                try
                {
                    AdsCommand cmd = new AdsCommand("SELECT " + colname + " from Controllers WHERE ControllerID=" + cid.ToString(), conn);
                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText, cid.ToString());
                    var result = cmd.ExecuteScalar();
                    cmd.Unprepare();

                    if (result != null)
                    {
                        if (result != DBNull.Value)
                        {
                            dt = Convert.ToDateTime(result);
                        }
                    }
                }
                catch (Exception e)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "14391", "Database exception during Legacy query: " + e.ToString(), cid.ToString());
                }

                db.CloseDBConnection(conn, cid.ToString());
            }

            return (dt);
        }


        //
        // get one of the IHSFY_Jobs argument columns for given job
        //
        static int? GetJobArgumentInt(uint job, String colname)
        {
            int? arg = null;
            // record in database
            ServerDB db = new ServerDB(_cts);

            AdsConnection conn = db.OpenDBConnectionNotAsync(_log);
            if (conn != null)
            {
                try
                {
                    AdsCommand cmd = new AdsCommand("SELECT " + colname + " from IHSFY_jobs where Idx = " + job.ToString(), conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                    var result = cmd.ExecuteScalar();
                    cmd.Unprepare();

                    if (result != null)
                    {
                        arg = Convert.ToInt32(result);
                    }
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "14391", "Database exception during Legacy query: " + e.ToString());
                }

                db.CloseDBConnection(conn, _log);
            }
            return (arg);
        }


        //
        // get one of the IHSFY_Jobs argument columns for given job
        //
        static string GetJobArgumentString(uint job, String colname)
        {
            string arg = null;
            long len;
            // record in database
            ServerDB db = new ServerDB(_cts);

            AdsConnection conn = db.OpenDBConnectionNotAsync(_log);
            if (conn != null)
            {
                try
                {
                    AdsCommand cmd = new AdsCommand("SELECT " + colname + " from IHSFY_jobs where Idx = " + job.ToString(), conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                    AdsDataReader rdr = cmd.ExecuteReader();
                    if(rdr.HasRows)
                    {
                        rdr.Read();
                        len = rdr.GetChars(0, 0, null, 0, Int32.MaxValue);
                        if(len > 0)
                        {
                            char[] buf = new char[len];
                            rdr.GetChars(0, 0, buf, 0, (int)len);
                            arg = new string(buf);
                        }

                    }
                    rdr.Close();
                    cmd.Unprepare();
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "14391", "Database exception during Legacy query: " + e.ToString());
                }

                db.CloseDBConnection(conn, _log);
            }
            return (arg);
        }

        //
        // get one of the IHSFY_Jobs argument columns for given job
        //
        public static byte[] GetJobArgumentBytes(uint job, String colname)
        {
            byte[] arg = null;
            
            // record in database
            ServerDB db = new ServerDB(_cts);

            AdsConnection conn = db.OpenDBConnectionNotAsync(_log);
            if (conn != null)
            {
                try
                {
                    AdsCommand cmd = new AdsCommand("SELECT " + colname + " from IHSFY_jobs where Idx = " + job.ToString(), conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                    AdsDataReader rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        rdr.Read();

                        if (rdr[0] != DBNull.Value)
                        {
                            arg = rdr.GetBytes(0);
                        }
                    }
                    rdr.Close();
                    cmd.Unprepare();
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "14391", "Database exception during Legacy query: " + e.ToString());
                }

                db.CloseDBConnection(conn, _log);
            }
            return (arg);
        }

        //
        // load trigger times out of CS3000CommServerSettings table, one for each row in TimeTriggerData
        //
        // it builds a derived table containing company specific tasks
        //
        static async void LoadTriggerTimesFromDatabase()
        {
            AdsCommand cmd;
            DataTable dt = new DataTable();
            ServerDB db = new ServerDB(_cts);

            Task<AdsConnection> connTask = db.OpenDBConnection("");
            AdsConnection conn = await connTask;

            if (conn != null)
            {
                try
                {
                    // load max connection also
                    cmd = new AdsCommand("SELECT LegacyMaxConnections from CS3000CommServerSettings WHERE CommServerID=" + Constants.CommServerID, conn);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                    object MaxConnections = cmd.ExecuteScalar();
                    cmd.Unprepare();
                    if (MaxConnections != DBNull.Value)
                    {
                        if (LegacyMaxConnections != (Int32)MaxConnections)
                        {
                            LegacyMaxConnections = (Int32)MaxConnections;
                            _log.WriteMessage(NLog.LogLevel.Info, "171", "(re)Loaded LegacyMaxConnections as " + LegacyMaxConnections.ToString());
                        }
                    }

                    // rebuild our derived list, so first clear it
                    DerivedTriggerData.Clear();

                    // load the company info
                    using (var command = new AdsCommand("select c.CompanyID, LegacyGetAlerts, LegacyGetReport, LegacyGetStationHistory, s.Company_TimeZone from CompaniesCommServerSettings c join Companies s on c.CompanyID = s.CompanyID where s.Deleted = false and s.CommServerID = " + Constants.CommServerID, conn))
                    {
                        using (AdsDataReader dr = command.ExecuteReader())
                        {
                            dt.Load(dr);
                            //_log.WriteMessage(NLog.LogLevel.Info, "171", "(re)Loaded company info for " + dt.Rows.Count.ToString() + " companies on this commserver.");
                        }
                    }

                    // derive the periodic task table from base table, adding for company specific settings
                    for (int i = 0; i < BaseTimeTriggerData.Length; i++)
                    {

                        // if there is no company dependency, then we just copy this entry to our derived list
                        if (BaseTimeTriggerData[i].SqlCompanyColumnName == null || BaseTimeTriggerData[i].SqlCompanyColumnName.Equals(null))
                        {
                            //  tart time defined
                            object Start;
                            // get this start time directly ...
                            cmd = new AdsCommand("SELECT " + BaseTimeTriggerData[i].SqlStartTimeColumnName + " from CS3000CommServerSettings WHERE CommServerID=" + Constants.CommServerID, conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            Start = cmd.ExecuteScalar();
                            cmd.Unprepare();

                            DerivedTriggerDataClass val = new DerivedTriggerDataClass();
                            val.CompanyID = 0;
                            val.TimeZoneName = "n/a";
                            val.entry = BaseTimeTriggerData[i];
                            val.entry.StartTime = TimeSpan.Parse(Start.ToString());
                            DerivedTriggerData.Add(val);
                        }
                        else
                        {
                            var dud = BaseTimeTriggerData[i].SqlCompanyColumnName;
                            // derive an entry for each defined company
                            foreach (DataRow row in dt.Rows)
                            {
                                DerivedTriggerDataClass val = new DerivedTriggerDataClass();
                                val.CompanyID = int.Parse(row["CompanyID"].ToString());
                                val.TimeZoneName = String.Copy(row.Field<string>("Company_TimeZone"));
                                //_log.WriteMessage(NLog.LogLevel.Debug, "171", "Company Specific job time is  " + TimeSpan.Parse(row[BaseTimeTriggerData[i].SqlCompanyColumnName].ToString().ToString()));                                
                                // now set the start time for this company from the corresponding CompaniesCommServerSettings column, all other values the same as base
                                val.entry = new TimeTriggerClass(TimeSpan.Parse(row[BaseTimeTriggerData[i].SqlCompanyColumnName].ToString()), BaseTimeTriggerData[i].RepeatDuration,
                                    BaseTimeTriggerData[i].RepeatFrequency, BaseTimeTriggerData[i].DayOfWeek, BaseTimeTriggerData[i].SqlDayOfWeekColumnName,
                                    BaseTimeTriggerData[i].SqlStartTimeColumnName, BaseTimeTriggerData[i].SqlStopTimeColumnName, BaseTimeTriggerData[i].SqlFrequencyColumnName,
                                    BaseTimeTriggerData[i].SqlCompanyColumnName, BaseTimeTriggerData[i].Priority, BaseTimeTriggerData[i].ProcessingFunction,
                                    BaseTimeTriggerData[i].CIDQuery);
                                DerivedTriggerData.Add(val);
                            }
                        }
                    }

                    foreach (DerivedTriggerDataClass r in DerivedTriggerData)
                    {
                        object Start;
                        Start = r.entry.StartTime;

                        object dayOfWeek;
                        if (r.entry.SqlDayOfWeekColumnName != null)
                        {
                            cmd = new AdsCommand("SELECT " + r.entry.SqlDayOfWeekColumnName + " from CS3000CommServerSettings WHERE CommServerID=" + Constants.CommServerID, conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            dayOfWeek = cmd.ExecuteScalar();
                            cmd.Unprepare();
                        }
                        else
                        {
                            dayOfWeek = DBNull.Value;
                        }

                        object Stop;
                        if (r.entry.SqlStopTimeColumnName != null)
                        {
                            cmd = new AdsCommand("SELECT " + r.entry.SqlStopTimeColumnName + " from CS3000CommServerSettings WHERE CommServerID=" + Constants.CommServerID, conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            Stop = cmd.ExecuteScalar();
                            cmd.Unprepare();
                        }
                        else
                        {
                            Stop = DBNull.Value;
                        }

                        object Freq;
                        if (r.entry.SqlFrequencyColumnName != null)
                        {
                            cmd = new AdsCommand("SELECT " + r.entry.SqlFrequencyColumnName + " from CS3000CommServerSettings WHERE CommServerID=" + Constants.CommServerID, conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            Freq = cmd.ExecuteScalar();
                            cmd.Unprepare();
                        }
                        else 
                        {
                            Freq = DBNull.Value;
                        }

                        TimeSpan? trigtime;

                        if(Start == DBNull.Value)
                        {
                            trigtime = null;
                        }
                        else
                        {
                            trigtime = (TimeSpan?)Start;
                        }

                        if (r.entry.StartTime != trigtime)
                        {
                            _log.WriteMessage(NLog.LogLevel.Info, "171", "(re)Loaded start trigger time for action " + r.entry.SqlStartTimeColumnName + " as " + trigtime.ToString());
                            r.entry.StartTime = trigtime;
                        }

                        // check for change in duration
                        TimeSpan? duration = null;
                        TimeSpan temp;
                        if(Start != DBNull.Value && Stop != DBNull.Value)
                        {
                            if(TimeSpan.Compare((TimeSpan)Start, (TimeSpan)Stop) < 0)   // Start before Stop
                            {
                                duration = ((TimeSpan)Stop).Subtract((TimeSpan)Start);
                            }
                            else
                            {
                                duration = ((TimeSpan)Start).Subtract((TimeSpan)Stop);
                                temp = new TimeSpan(24,0,0);
                                duration = temp.Subtract((TimeSpan)duration);
                            }
                        }

                        if (r.entry.RepeatDuration != duration)
                        {
                            _log.WriteMessage(NLog.LogLevel.Info, "171", "(re)Loaded duration for action " + r.entry.SqlStartTimeColumnName + " as " + duration.ToString());
                            r.entry.RepeatDuration = duration;
                        }

                        if (dayOfWeek != DBNull.Value)
                        {
                            if (r.entry.DayOfWeek != (int)dayOfWeek)
                            {
                                _log.WriteMessage(NLog.LogLevel.Info, "171", "(re)Loaded day of week for action " + r.entry.SqlStartTimeColumnName + " as " + dayOfWeek.ToString());
                                r.entry.DayOfWeek = (int)dayOfWeek;
                            }
                        }

                        // check for change in frequency minutes
                        TimeSpan? f = null;
                        if(Freq != DBNull.Value)
                        {
                            f = (TimeSpan?) new TimeSpan(0, Convert.ToInt32(Freq.ToString()), 0);
                        }
                        if (r.entry.RepeatFrequency != f)
                        {
                            _log.WriteMessage(NLog.LogLevel.Info, "171", "(re)Loaded frequency for action " + r.entry.SqlStartTimeColumnName + " as " + f.ToString());
                            r.entry.RepeatFrequency = f;
                        }

                    }
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "169", "Database exception during query of periodic task times: " + e.ToString());
                }

                db.CloseDBConnection(conn, "");
            }

        }
        //
        // constructor
        //
        static LegacyTasks()
        {
            int workerThreads;
            int portThreads;

            _log = new MyLogger("LegacyTasks", "", "", Constants.ET2000Model);
            _cts = null;    // this will get set at runtime
            _jobpollrequest_cts = null;
            _combo_cts = null;
            _lastrun = null;
            // start a task to periodically scan for something to send based
            // on database flags getting set for a controller...
            Task.Factory.StartNew(PollLoop, TaskCreationOptions.LongRunning);
            Task.Factory.StartNew(JobLoop, TaskCreationOptions.LongRunning);

            ThreadPool.GetAvailableThreads(out workerThreads, out portThreads);
            _log.WriteMessage(NLog.LogLevel.Info, "154", "FYI: Max worker threads " + workerThreads.ToString() + " max async I/O threads " + portThreads.ToString());
        }
        // internal vars
        private static MyLogger _log;
        private static CancellationTokenSource _cts;
        private static CancellationTokenSource _jobpollrequest_cts;
        private static CancellationTokenSource _combo_cts;
        private static DateTime? _lastrun;
        private static Dictionary<int, String> TimeZoneTable = new Dictionary<int, String>();
        public static int LegacyMaxConnections = 1000;

        //
        // routine to remove job from IHSFY_jobs and place it in IHSFY_notifications with given completion status in 
        // the 'Status' column
        //
        async public static Task CompleteLegacyJob(int job_number, int completion_status, byte[] responsedata)
        {
            AdsCommand cmd;

            if (job_number <= 0)
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "154", "CompleteLegacyJob() called with invalid job number " + job_number.ToString());
                return;
            }

            try
            {
                ServerDB db = new ServerDB(_cts);

                Task<AdsConnection> connTask = db.OpenDBConnection("");
                AdsConnection conn = await connTask;

                if (conn != null)
                {
                    // insert final status into notification table
                    // note that the Param4 column is binary data which
                    // will come into this function as responsedata byte array
                    cmd = new AdsCommand("INSERT INTO IHSFY_notifications(Idx, Command_id, Controller_id, User_id, Date_created, Status, Param1, Param2, Param3, Param4, Param5 ) "
                        + " SELECT Idx, Command_id, Controller_id, User_id,'"
                        + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                        + completion_status.ToString() + ", Param1, Param2, Param3, :v , Param5"
                        + " FROM IHSFY_jobs WHERE Idx = " + job_number.ToString(), conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);
                    AdsParameter prm = new AdsParameter("v", DbType.Binary);
                    if (responsedata == null)
                    {
                        prm.Value = null;
                    }
                    else
                    {
                        _log.WriteMessage(NLog.LogLevel.Info, "155", "Completed job with binary data containing " + responsedata.Length.ToString() + " bytes of data");
                        prm.Value = responsedata;
                    }
                    cmd.Parameters.Add(prm);
                    cmd.ExecuteNonQuery();
                    cmd.Unprepare();
                    // and then delete the job
                    cmd = new AdsCommand("DELETE from IHSFY_jobs where Idx = " + job_number.ToString(), conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);
                    cmd.ExecuteNonQuery();
                    cmd.Unprepare();

                    //update task status table to notify user with the results
                    cmd = new AdsCommand("UPDATE TaskStatus SET Status=" + (completion_status == Constants.JOB_SUCCESSFUL ? 1 : 2) +
                                         ",HasViewed=false,arg3=:v WHERE CommServerJobIdx = " + job_number.ToString(), conn);
                    cmd.Parameters.Add(prm);
                    _log.WriteMessage(NLog.LogLevel.Debug, "156", "SQL: " + cmd.CommandText);
                    cmd.ExecuteNonQuery();
                    cmd.Unprepare();

                    db.CloseDBConnection(conn, "");
                }
            }
            catch (Exception e)
            {
                _log.WriteMessage(NLog.LogLevel.Error, "178", "Error setting job status/completion in IHSFY_jobs/IHSFY_notifications table: " + e.ToString());
            }

            return;
        }

 
        public static void CheckJobsNow()
        {
            if (_jobpollrequest_cts != null)
            {
                _jobpollrequest_cts.Cancel();
            }
            return;
        }

        async private static void JobLoop()
        {
            int delay;
            AdsCommand cmd;
            AdsDataReader rdr;
            int job_idx;
            Random rand = new Random();

#if MOBILEJOBSPEEDUP
            bool MobileJobFound;
            int MobilePhaseCounter;
#endif


            while (_cts == null)
            {
                await Task.Delay(50);  // wait for somebody to tell us the cancellation token
            }

            SequenceNumber = (short)rand.Next(1,65535);         // start at this sequence number

            _jobpollrequest_cts = new CancellationTokenSource();
            _combo_cts = CancellationTokenSource.CreateLinkedTokenSource(_jobpollrequest_cts.Token, _cts.Token);

            await Task.Delay(2500);       // let everything spin up...

            _log.WriteMessage(NLog.LogLevel.Info, "150", "Legacy Job Loop process starting...");

            // at startup up, the PeriodicTasks class will "flush" any existing jobs, so we
            // don't need to do that here.

            while (true)
            {
                //
                // query for job to perform
                //

#if MOBILEJOBSPEEDUP
                MobileJobFound = false;
                MobilePhaseCounter = 0;
#endif

                try
                {
                    ServerDB db = new ServerDB(_cts);

                    Task<AdsConnection> connTask = db.OpenDBConnection("");
                    AdsConnection conn = await connTask;

                    if (conn != null)
                    {
                        // query for only legacy controllers
                        //
                        //
                        cmd = new AdsCommand("SELECT I.Idx, I.Command_id, c.ControllerID, IIF(c.PhoneNumber='' OR c.PhoneNumber IS NULL, '0.0.0.0',c.PhoneNumber), I.User_id, I.Date_created, I.Status, " +
                            " I.Processing_start, I.Param1, I.Param2, I.Param3, I.Param5, c.ConnectionType, c.Address, s.CompanyID, NoOfControllersInLegacyChain3(c.PhoneNumber,c.Address,c.HubID), c.UseHubType, ISNULL(Left(c.SoftwareVersion,1),'0'),ISNULL(RepeaterInUse,false),ISNULL(TxTimerAddon,0) "
                            + " from IHSFY_jobs I JOIN Controllers c ON I.Controller_ID = c.ControllerID " +
                            " JOIN ControllerSites s ON c.SiteID = s.SiteID WHERE c.Model in (1,5,6) and isnull(I.Controller_ID,0) > 0" + 
                            " AND s.CompanyID IN (SELECT CompanyID FROM Companies WHERE CommServerID=" + Constants.CommServerID + ")" +
                            " ORDER BY I.idx", conn);
                        //_log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);

                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                // first find the parameters for this type of commands...
                                for (job_idx = 0; job_idx < JobListData.Length; job_idx++)
                                {
                                    if (JobListData[job_idx].MessageID == rdr.GetInt32(1))   // Command_id column index is 1
                                    {
                                        break;
                                    }
                                }
                                if (job_idx >= JobListData.Length)
                                {
                                    _log.WriteMessage(NLog.LogLevel.Error, "155", "Table IHSFY_jobs contains unknown command_id: " + rdr.GetInt32(1).ToString());
                                    // error this job out
                                    await CompleteLegacyJob(rdr.GetInt32(0), Constants.JOB_UNKNOWN_COMMAND_ID, null);
                                }
                                // first see if this job has expired - by comparing the Date_created + lifetime to current time
                                else if (rdr.GetDateTime(5) + JobListData[job_idx].Lifetime <= DateTime.Now)
                                {
                                    await CompleteLegacyJob(rdr.GetInt32(0), Constants.JOB_EXPIRED, null);
                                }
                                //
                                // special case check for rain shutoff expiring at 8pm controller time - param1 contains minutes til that time based
                                // on time job was created.
                                //
                                else if (JobListData[job_idx].MessageID == Constants.MID_FROM_COMMSERVER_RAIN_SHUTDOWN_packet && rdr.GetDateTime(5).AddMinutes(rdr.GetInt32(8)) <= DateTime.Now)
                                {
                                    await CompleteLegacyJob(rdr.GetInt32(0), Constants.JOB_EXPIRED, null);
                                }
                                else
                                {
                                    // at this point this is a command we know about... see if time to run                             
                                    // see if the processing_start datetime column is null, indicating a new command
                                    DateTime? lastrun = null;
                                    Boolean runit = false;
                                    Int32 cid;
                                    String host;
                                    String adr;
                                    Int32 hubtype = 0;

                                    cid = rdr.GetInt32(2);

                                    if (!rdr.IsDBNull(16))
                                    {
                                        hubtype = rdr.GetInt32(16);
                                    }

                                    if(hubtype == 9 || hubtype == 10)
                                    {
                                        // we fake a hostname for Inbound 2000 controllers
                                        host = cid.ToString();
                                    }
                                    else if(!rdr.IsDBNull(3))
                                    {
                                        host = rdr.GetString(3);
                                    }
                                    else
                                    {
                                        // we have to know a host to connect to!
                                        _log.WriteMessage(NLog.LogLevel.Error, "155", "Legacy Controller has null phonenumber. Controller ID: " + cid.ToString());
                                        // error this job out
                                        await CompleteLegacyJob(rdr.GetInt32(0), Constants.JOB_UNKNOWN_COMMAND_ID, null);
                                        continue;
                                    }
                                    if (!rdr.IsDBNull(13))
                                    {
                                        adr = rdr.GetString(13);    // controller address, 3 characters
                                    }
                                    else
                                    {
                                        // we have to know an address to send to!
                                        _log.WriteMessage(NLog.LogLevel.Error, "155", "Legacy Controller has null address. Controller ID: " + cid.ToString());
                                        // error this job out
                                        await CompleteLegacyJob(rdr.GetInt32(0), Constants.JOB_UNKNOWN_COMMAND_ID, null);
                                        continue;
                                    }
                                    if (!rdr.IsDBNull(7))
                                    {
                                        lastrun = rdr.GetDateTime(7);
                                        if (lastrun + JobListData[job_idx].RetryPeriod <= DateTime.Now)
                                        {
                                            runit = true;       // it's time to try this job again
                                        }
                                    }
#if MOBILEJOBSPEEDUP
                                    if (lastrun == null && JobListData[job_idx].MobileJobFunction)
                                    {
                                        MobileJobFound = true;  // flag that we have seen a new "mobile" job
                                        MobilePhaseCounter = 24;    // poll at faster rate this many times before dropping back to default
                                    }
#endif
                                    if (lastrun == null || runit)
                                    {
                                        // there is a connection, job is good to go - call function (if there is one defined)
                                        if (JobListData[job_idx].JobFunction != null)
                                        {
                                            _log.WriteMessageWithId(NLog.LogLevel.Info, "167", "Executing job number " + rdr.GetInt32(0).ToString() + " of MID " + rdr.GetInt32(1).ToString() + " on Legacy controller", cid.ToString());
                                            // call the helper function with arguments:
                                            // ControllerID, hostname (could be IP or phone number), JobID, and ConnectionType
                                            // this will connect (if needed) and queue up given job for connection using
                                            // priority to decide which happens next on connection
                                            var jobidx = rdr.GetInt32(0);
                                            var conntype = rdr.GetInt32(12);
                                            var companyid = rdr.GetInt32(14);
                                            var multiplier = 0;
                                            if (!rdr.IsDBNull(15))multiplier = rdr.GetInt32(15);
                                            var fwversion = 0;
                                            if (!rdr.IsDBNull(17)) fwversion = Convert.ToInt32(rdr.GetString(17)[0]) - '0';
                                            var repeaterInUse = false;
                                            if (!rdr.IsDBNull(18)) repeaterInUse = rdr.GetBoolean(18);
                                            var txTimerAddon = 0;
                                            if (!rdr.IsDBNull(19)) txTimerAddon = rdr.GetInt32(19);

                                            // pragma to disable "Because this job is not awaited..."
#pragma warning disable 4014
                                            // TBD - if any job needs accurate timezone for controller then can't hardcode here as 0
                                            //  HelperAction(int JobIdx, uint cid, String hostname, uint connectiontype, uint timezonekey, int priority, String address, uint companyID, Action<ActionArgs> a)
                                            int tempnum = job_idx;
                                            Task.Run(() => HelperAction(jobidx, (uint)cid, host, (uint)conntype, 0, JobListData[tempnum].Priority, adr, (uint)companyid, (int)multiplier, (int)hubtype, (int)fwversion, (bool)repeaterInUse, (int)txTimerAddon, JobListData[tempnum].JobFunction), _cts.Token).ConfigureAwait(false);

                                            // update the processing time to now
                                            AdsCommand c2 = new AdsCommand("UPDATE IHSFY_jobs SET Processing_start = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' where Idx = " + jobidx.ToString(), conn);
                                            await c2.ExecuteNonQueryAsync();
                                            c2.Unprepare();
                                        }
                                    }
                                }
                            }
                        }
                        rdr.Close();
                        cmd.Unprepare();

                        db.CloseDBConnection(conn, "");
                    }
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "178", "Error querying/updating IHSFY_jobs table: " + e.ToString());
                }

#if MOBILEJOBSPEEDUP
                // if any new mobile command job found, look again in 5 seconds
                //
                if (MobileJobFound || MobilePhaseCounter > 0)
                {
                    delay = 5;
                    if (MobilePhaseCounter > 0) MobilePhaseCounter--;   // stay at faster rate until this drops to 0
                }
                else
                {
                    delay = 60;
                }
#endif

                delay = Convert.ToInt32(ConfigurationManager.AppSettings["JobLookLoopSeconds"]);

                try
                {
                    // either a QUIT signal, or a CHECK JOBS NOW signal will get us out of delay early
                    await Task.Delay(Convert.ToInt32(delay * 1001), _combo_cts.Token);        // run every X seconds
                }
                catch (TaskCanceledException ex)
                {
                    if (_cts.IsCancellationRequested)
                    {
                        _log.WriteMessage(NLog.LogLevel.Debug, "190", "Exiting Legacy Job Loop task " + ex.ToString());
                    }
                    else
                    {
                        _log.WriteMessage(NLog.LogLevel.Debug, "190", "Web signal to check jobs now.");
                        // recreate the tokens needed to look next time
                        _jobpollrequest_cts.Dispose();
                        _combo_cts.Dispose();
                        _jobpollrequest_cts = new CancellationTokenSource();
                        _combo_cts = CancellationTokenSource.CreateLinkedTokenSource(_jobpollrequest_cts.Token, _cts.Token);
                    }
                }

                if (_cts.IsCancellationRequested) break; // program is exiting
            }
        }

        //
        // method to return the current time at a Controller in the given Calsense timezone number
        //
        //
        static public DateTime CurrentTimeAtControllerInTz(int z)
        {
            foreach (var tz in TimeZoneTable)
            {
                if (tz.Key == z)
                {
                    return (TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(tz.Value)));
                }
            }
            return (DateTime.Now);
        }

        //
        // Look for periodic tasks to perform on legacy controllers
        // Note that different controllers may be in different time zones, and the job time is
        // based on the controller time.
        //
        async private static void PollLoop()
        {
            DateTime currentcheck;
            DateTime dtatcontroller;
            DateTime lastatcontroller;
            TimeZoneInfo currentzone;
            AdsCommand cmd;
            AdsDataReader rdr;
            uint i;
            int j;
            IntPtr retptr;
            byte b;

            while (_cts == null)
            {
                await Task.Delay(50);  // wait for somebody to tell us the cancellation token
            }

            await Task.Delay(2500);       // let everything spin up...

            _log.WriteMessage(NLog.LogLevel.Info, "150", "Legacy Periodic Task process starting...");

            LoadTriggerTimesFromDatabase();

            // gather the time zone index to name mapping by calling into the shared
            // controller code
            // get timezone name strings using call to shared controller function
            for (i = 0; i <= 300; i++)
            {
                // UKNOWN string is returned for unknown mapping
                retptr = GetTimeZoneStr(i);
                if (retptr != IntPtr.Zero)
                {
                    // get returned bytes which should be an ascii character string
                    j = 0;
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    while ((b = System.Runtime.InteropServices.Marshal.ReadByte(retptr, j)) != 0)
                    {
                        sb.Append((char)b);
                        j++;
                    }
                    if (sb.ToString() == "UNKNOWN")
                    {
                        // not known
                    }
                    else
                    {
                        // verify this computer has information about the given time zone
                        try
                        {
                            TimeZoneInfo.FindSystemTimeZoneById(sb.ToString());
                            TimeZoneTable.Add((int)i, sb.ToString());
                            _log.WriteMessage(NLog.LogLevel.Info, "177", "Retrieved time zone name for zone id " + i + " as " + sb.ToString());
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessage(NLog.LogLevel.Error, "178", "Error looking up time zone " + sb.ToString() + " of " + e.ToString());
                        }

                    }
                }
            }

            while (true)
            {
                //
                // see if a trigger time has been hit since we last looked...
                //
                currentcheck = DateTime.Now;      // timestamp of this check
                currentzone = TimeZoneInfo.Local;           // our timezone
                _log.WriteMessage(NLog.LogLevel.Debug, "151", "Check for legacy task to run at commserver time-of-day: " + currentcheck.ToString());

                if (_lastrun != null)
                {
                    // separately process each timezone we know about
                    foreach (var tz in TimeZoneTable)
                    {
                        //
                        // need to know time of day at controller(s) in the given timezone
                        //
                        dtatcontroller = TimeZoneInfo.ConvertTime(currentcheck, TimeZoneInfo.FindSystemTimeZoneById(tz.Value));
                        lastatcontroller = TimeZoneInfo.ConvertTime((DateTime)_lastrun, TimeZoneInfo.FindSystemTimeZoneById(tz.Value));

                        //
                        // the TimeTriggerData now contains the processing times for
                        // the company we are working on
                        //
                        foreach (DerivedTriggerDataClass task in DerivedTriggerData)
                        {
                            if (task.entry.StartTime != null)
                            {
                                // we have the required start time...
                                // this could be:
                                // a) a once per day non-repeating job
                                // b) a once per month non-repeating job
                                // c) one of the above that repeats
                                // we have to check each repeating possibility for the command

                                long NumChecks = 1;

                                if (task.entry.RepeatDuration != null && task.entry.RepeatFrequency != null)
                                {
                                    long ticks1;
                                    long ticks2;
                                    ticks1 = task.entry.RepeatDuration.Value.Ticks;
                                    ticks2 = task.entry.RepeatFrequency.Value.Ticks;
                                    if (ticks1 > 0 && ticks2 > 0)
                                    {
                                        if (ticks1 / ticks2 > 1)
                                        {
                                            NumChecks = ticks1 / ticks2;
                                        }
                                    }
                                }

                                while (NumChecks-- > 0)
                                {
                                    TimeSpan CheckTime = task.entry.StartTime.Value;
                                    for (i = 0; i < NumChecks; i++)
                                    {
                                        // check each possible frequency of command 
                                        CheckTime += task.entry.RepeatFrequency.Value;
                                    }

                                    // see if the trigger time for given entry falls within current checking window
                                    // for weekly jobs, make sure day of week matches
                                    // or if a repeating function check for a repeat time falling in window
                                    bool TimeToRunJob = false;
                                    if (task.entry.DayOfWeek != null)
                                    {
                                        //_log.WriteMessage(NLog.LogLevel.Debug, "152", "Checking to see if weekly legacy task scheduled for " + Enum.GetName(typeof(DayOfWeek),task.DayOfWeek) + " at " + task.StartTime.ToString() + " is within window of " + lastatcontroller.ToString() + " to " + dtatcontroller.ToString());
                                        // weekly job

                                        // just in case a repeating daily job runs into the next day,
                                        // subtract out the days when comparing time to run
                                        TimeSpan tod;
                                        tod = CheckTime.Subtract(TimeSpan.FromDays(CheckTime.Days));
                                        if (tod > lastatcontroller.TimeOfDay && tod <= dtatcontroller.TimeOfDay && ((int)dtatcontroller.DayOfWeek == (int)task.entry.DayOfWeek))
                                        {
                                            TimeToRunJob = true;
                                        }
                                    }
                                    else
                                    {
                                        //_log.WriteMessage(NLog.LogLevel.Debug, "152", "Checking to see if legacy task scheduled for " + task.StartTime.ToString() + " is within window of " + lastatcontroller.ToString() + " to " + dtatcontroller.ToString());
                                        // just in case a repeating daily job runs into the next day,
                                        // subtract out the days when comparing time to run
                                        TimeSpan tod;
                                        tod = CheckTime.Subtract(TimeSpan.FromDays(CheckTime.Days));
                                        if (tod > lastatcontroller.TimeOfDay && tod <= dtatcontroller.TimeOfDay)
                                        {
                                            TimeToRunJob = true;
                                        }
                                    }
                                    if (TimeToRunJob == true)
                                    {
                                        if (task.CompanyID != 0 && !String.Equals(task.TimeZoneName, tz.Value.ToString(), StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            _log.WriteMessage(NLog.LogLevel.Debug, "153", "Not bothering with controller query for Company " + task.CompanyID.ToString() + " since check time zone " + tz.Value.ToString() + " not same as Company time zone " + task.TimeZoneName.ToString());
                                        }
                                        else
                                        {

                                            _log.WriteMessage(NLog.LogLevel.Debug, "153", "Legacy Controllers in time zone " + tz.Value.ToString() + " have a task to be performed");
                                            //
                                            // make a list of all the Legacy Controllers from database in this timezone
                                            //
                                            ServerDB db = new ServerDB(_cts);

                                            Task<AdsConnection> connTask = db.OpenDBConnection("");
                                            AdsConnection conn = await connTask;

                                            // list of the controller IDs / hostnames in database for this timezone
                                            List<Tuple<uint, String, uint, String, uint, int, int, Tuple<int, bool, int>>> cids = new List<Tuple<uint, String, uint, String, uint, int, int, Tuple<int, bool, int>>>();   // only 7 regular elements allowed, 8th must be a Tuple
                                            
                                            if (conn != null)
                                            {
                                                try
                                                {
                                                    // use the query specific to this task to get all the applicable Controller IDs (CIDs) / IP addresses / ConnectionType / Address / Company ID / Multiplier, appending the timezone to query ending in AND Timezone = "
                                                    if (task.CompanyID != 0)
                                                    {
                                                        // company specific job
                                                        cmd = new AdsCommand(task.entry.CIDQuery + "'" + tz.Value.ToString() + "'" + " and s.CompanyID = " + task.CompanyID.ToString(), conn);
                                                    }
                                                    else
                                                    {
                                                        // all companies
                                                        cmd = new AdsCommand(task.entry.CIDQuery + "'" + tz.Value.ToString() + "'", conn);
                                                    }
                                                    _log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);
                                                    rdr = cmd.ExecuteReader();
                                                    if (rdr.HasRows)
                                                    {
                                                        var multiplier = 0;
                                                        var hubtype = 0;
                                                        var fwversion = 0;
                                                        var repeaterInUse = false;
                                                        var txTimerAddon = 0;
                                                        String hostname = null;
                                                        uint cid = 0;

                                                        while (rdr.Read())
                                                        {
                                                            cid = (uint)rdr.GetInt32(0);

                                                            if (rdr.IsDBNull(5)) multiplier = 0;
                                                            else multiplier = rdr.GetInt32(5);

                                                            if (rdr.IsDBNull(6)) hubtype = 0;
                                                            else hubtype = rdr.GetInt32(6);

                                                            if (hubtype == 9 || hubtype == 10)
                                                            {
                                                                // for Inbound 2000's use controller id as hostname
                                                                hostname = cid.ToString();
                                                            }
                                                            else
                                                            {
                                                                hostname = rdr.GetString(1);
                                                            }

                                                            if (rdr.IsDBNull(7)) fwversion = 0;
                                                            else fwversion = Convert.ToInt32(rdr.GetString(7)[0]) - '0';

                                                            if (rdr.IsDBNull(8)) repeaterInUse = false;
                                                            else repeaterInUse = rdr.GetBoolean(8);

                                                            if (rdr.IsDBNull(9)) txTimerAddon = 0;
                                                            else txTimerAddon = rdr.GetInt32(9);

                                                            cids.Add(new Tuple<uint, String, uint, String, uint, int, int, Tuple<int, bool, int>>(cid, hostname, (uint)rdr.GetInt32(2), rdr.GetString(3), (uint)rdr.GetInt32(4), (int)multiplier, (int)hubtype, new Tuple<int, bool, int>((int)fwversion, (bool)repeaterInUse, (int)txTimerAddon)));    // put the next controller ID and hostname/IP and connectiontype and address into a list
                                                        }
                                                        _log.WriteMessage(NLog.LogLevel.Info, "156", "Number of legacy controllers in DB to process in time zone " + tz.Value.ToString() + " is " + cids.Count.ToString());
                                                    }
                                                    rdr.Close();
                                                    cmd.Unprepare();

                                                }
                                                catch (Exception e)
                                                {
                                                    _log.WriteMessage(NLog.LogLevel.Error, "166", "Database exception during query of legacy controllers by periodic task: " + e.ToString());
                                                }

                                                db.CloseDBConnection(conn, "");
                                            }

                                            //
                                            // setup a task to call given function
                                            //
                                            foreach (Tuple<uint, String, uint, String, uint, int, int, Tuple<int, bool, int>> e in cids)
                                            {
                                                // TODO change this to .Debug once working I think...
                                                _log.WriteMessageWithId(NLog.LogLevel.Debug, "160", "Queuing up " + task.entry.StartTime.ToString() + " task for legacy controller", e.Item1.ToString());
                                                // pragma to remove build warning about not await these "fire and forget tasks"
#pragma warning disable 4014
                                                //         static void HelperAction(int JobIdx, uint cid, String hostname, uint connectiontype, uint timezonekey, int priority, Action<uint, String, uint, uint, NetworkStream> a)
                                                Task.Run(() => HelperAction(-1, e.Item1, e.Item2, e.Item3, (uint)tz.Key, task.entry.Priority, e.Item4, e.Item5, e.Item6, e.Item7, e.Rest.Item1, e.Rest.Item2, e.Rest.Item3, task.entry.ProcessingFunction), _cts.Token).ConfigureAwait(false);
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }

                //
                // save when we ran
                //
                _lastrun = currentcheck;

                try
                {
                    await Task.Delay(Convert.ToInt32(60 * 1000), _cts.Token);        // run every minute
                }
                catch (TaskCanceledException ex)
                {
                    _log.WriteMessage(NLog.LogLevel.Debug, "190", "Exiting Legacy Periodic Task process " + ex.ToString());
                }

                LoadTriggerTimesFromDatabase();     // check for any changes to run at times...

                if (_cts.IsCancellationRequested) break; // program is exiting
            }
        }

        //
        // Set the cancellation token for other legacy tasks
        //
        public static void SetCts(CancellationTokenSource c)
        {
            _cts = c;
            LegacyConnectionTracker.SetCts(c);  // also tell our connectiokn tracker
            LegacyTransport.SetCts(c);
        }

        //
        // Common action that gets an actual action queued up/connected.
        // Called via Task.Run so that multiple outbound connections can
        // be attempted at once instead of being limited serially.
        // Queue up the given action for the connection
        //
        // JobIdx can be 0/negative if a time periodic action
        static void HelperAction(int JobIdx, uint cid, String hostname, uint connectiontype, uint timezonekey, int priority, String address, uint companyID, int Multiplier, int HubType, int Series, bool RepeaterInUse, int TxTimerAddon, Action<ActionArgs> a)
        {
            ActiveLegacyConnection ac;
            ActionArgs args = new ActionArgs();
            Boolean internal_only = false;

            // special case for jobs that don't actually communicate, but are used for internal processing
            if(a == Action2000HubChange)
            {
                // we just need an AC structure, no connection etc.
                internal_only = true;
            }

            // FYI: "short-circuit" evaluation allows us to use ac in second condition knowing it won't be null
            //
            // when the 2000 is attached via a 3000, the ac.sock may not be set to the 3000 socket if it is
            // not currently busy with a job.
            //
            if ((ac = LegacyConnectionTracker.AddOrReuseConnection(hostname, cid, connectiontype, address, companyID, Multiplier, HubType, internal_only,RepeaterInUse, TxTimerAddon)) != null && (internal_only || ac.AddReuseGoodToGo || (ac.sock != null && ac.sock.Connected)))
            {
                // connection is now established. grab the lock so we can add the given function to
                // be performed. Also see if a GET CMOS is required... We do that for a controller
                // the first time a Get Program Data or Get Alerts is going out on a connection
                ac.acsem.Wait(_cts.Token);
                args.adr = address;
                args.cid = cid;
                args.connectiontype = connectiontype;
                args.hostname = internal_only ? "INTERNAL" : hostname;
                args.timezonekey = timezonekey;
                args.jobid = JobIdx > 0 ? (uint) JobIdx : 0;
                args.cityid = companyID;
                ac.queued_functions.Add(new Tuple<int, Action<ActionArgs>, ActionArgs>(priority, a, args));

                var controllerobj = ac.controllers.Find(x => x.CID == cid);
                if (controllerobj != null)
                {
                    // if we haven't yet received a CMOS response from this controller during this connection, then...
                    if (controllerobj.GetCMOSRecv == false)
                    {
                        controllerobj.FWVersion = Series;   // set the version of this controller based on existing database value
                    }
                    //
                    // we send a GetCMOS first if either of:
                    // a) asked to send a Get Program Data
                    // b) database doesn't have a SoftwareVersion value yet for this controller
                    //
                    if (a == ActionGetProgramData || Series == 0)
                    {
                        if (a == ActionGetProgramData || controllerobj.GetCMOSSent == false)
                        {
                            controllerobj.GetCMOSSent = true;
                            // build and queue high priority Get CMOS
                            //
                            // we will use a 1000 priority (higher number = higher priority) to be first to dequeue.
                            //
                            args.cid = cid;
                            args.hostname = hostname;
                            args.connectiontype = connectiontype;
                            args.timezonekey = timezonekey;
                            args.adr = address;
                            args.cityid = companyID;
                            args.jobid = 0; // internally generated job
                            ac.queued_functions.Add(new Tuple<int, Action<ActionArgs>, ActionArgs>(1000, LegacyTasks.ActionGetCMOS, args));
                        }
                    
                    }
                }

                ac.acsem.Release();
                // job now queued

                // this will pull the next command to go on the connection - which
                // we have to do when connection first made to get things happening.
                // (as functions complete, they will kick off any subsequent commands)
                LegacyConnectionTracker.RemoveTrackingEntryIfDone(ac);  
            }
            else
            {
                _log.WriteMessageWithId(NLog.LogLevel.Warn, "162", "Unable to connect to Legacy Controller: " + cid.ToString(), cid.ToString());
                // IHSFY_Jobs completition for error case
                if (JobIdx > 0)
                {
                    Misc.AddET2000Alert(_cts, cid, "Unable to connect to the controller", true);
                    CompleteLegacyJob(JobIdx, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                }
            }

            return;
        }

        //
        // perform a GetCMOS (MID = MID_CMOS_TO_PC) to given controller over given network stream
        // When this function is called, we have already been selected and
        // allowed to send out over established socket connection.
        //
        static public void ActionGetCMOS(ActionArgs args)
        {
            ActiveLegacyConnection ac;

            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Get CMOS for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_CMOS_TO_PC, null, args.cid, args.jobid);    // no application data
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above to set a given controllers clock.
        // (Controller specified by cid=Controller ID argument)
        // build a packet stream containing current time and transmit out
        // on network stream for given controller
        // Content: DATE_TIME: The current date/time based upon the company’s time zone, available in the Company_TimeZone field of the Companies table.
        //
        static void ActionSetControllerClock(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            DateTime timeatcontroller;
            UInt16 d;
            UInt32 s;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Set Clock for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                timeatcontroller = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(TimeZoneTable[(int)args.timezonekey]));
                // now build 6 byte Calsense date time structure from above c# DateTime object
                // first four bytes are 32 bit time
                // next two bytes are 16 bit date
                d = Misc.PackedDatefromDateTime(timeatcontroller);
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "Time at controller is " + timeatcontroller.ToString(), args.cid.ToString());
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "2-byte date is " + d.ToString("X"), args.cid.ToString());
                s = (UInt32)timeatcontroller.TimeOfDay.TotalSeconds;
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "4-byte time is " + s.ToString("X"), args.cid.ToString());
                var mem = new MemoryStream(0);
                LegacyUint16ToNetworkBytes(d, mem);
                LegacyUint32ToNetworkBytes(s, mem);
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETTIMEDATE, data, args.cid, args.jobid); 
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above to set a given controllers clock.
        // (Controller specified by cid=Controller ID argument)
        // build a packet stream containing current time and transmit out
        // on network stream for given controller
        // Content: DATE_TIME: The current date/time based upon the company’s time zone, available in the Company_TimeZone field of the Companies table.
        //
        static void ActionSetControllerClockByTimeZone(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            DateTime timeatcontroller;
            UInt16 d;
            UInt32 s;
            string timeZone = "";
            bool forceToEndTxMessage = true;


            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Set Clock for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {

                // record in database
                ServerDB db = new ServerDB(_cts);

                AdsConnection conn = db.OpenDBConnectionNotAsync(_log);
                if (conn != null)
                {
                    try
                    {
                        AdsCommand cmd = new AdsCommand("SELECT Company_TimeZone from Companies where CompanyID = " + args.cityid, conn);
                        _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                        AdsDataReader rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            timeZone = rdr.GetString(0);
                        }
                        rdr.Close();
                        cmd.Unprepare();
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "14391", "Database exception during Legacy query: " + e.ToString());
                    }

                    db.CloseDBConnection(conn, _log);
                }

                if (!string.IsNullOrEmpty(timeZone))
                {
                    timeatcontroller = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(timeZone));
                    // now build 6 byte Calsense date time structure from above c# DateTime object
                    // first four bytes are 32 bit time
                    // next two bytes are 16 bit date
                    d = Misc.PackedDatefromDateTime(timeatcontroller);
                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "Time at controller is " + timeatcontroller.ToString(), args.cid.ToString());
                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "2-byte date is " + d.ToString("X"), args.cid.ToString());
                    s = (UInt32)timeatcontroller.TimeOfDay.TotalSeconds;
                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "4-byte time is " + s.ToString("X"), args.cid.ToString());
                    var mem = new MemoryStream(0);
                    LegacyUint16ToNetworkBytes(d, mem);
                    LegacyUint32ToNetworkBytes(s, mem);
                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETTIMEDATE, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;

                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above to get alerts
        //
        // content - DATE_TIME, the last timestamp stored in database for this controller
        //
        static void ActionGetAlerts(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            DateTime timesince;
            UInt16 d;
            UInt32 s;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Get Alerts for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                timesince = GetLastDateTime("LastTimestamp_Alerts", args.cid);
                // now build 6 byte Calsense date time structure from above c# DateTime object
                // first four bytes are 32 bit time
                // next two bytes are 16 bit date
                d = Misc.PackedDatefromDateTime(timesince);
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "Getting alerts since " + timesince.ToString(), args.cid.ToString());
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "2-byte date is " + d.ToString("X"), args.cid.ToString());
                s = (UInt32)timesince.TimeOfDay.TotalSeconds;
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "4-byte time is " + s.ToString("X"), args.cid.ToString());
                var mem = new MemoryStream(0);
                LegacyUint16ToNetworkBytes(d, mem);
                LegacyUint32ToNetworkBytes(s, mem);
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_GETALERTS, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above to get reports
        //
        // content - DATE_TIME, the last timestamp stored in database for this controller
        //
        static void ActionGetReports(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            DateTime timesince;
            UInt16 d;
            UInt32 s;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Get Reports for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                timesince = GetLastDateTime("LastTimestamp_DailyData", args.cid);

                // now build 6 byte Calsense date time structure from above c# DateTime object
                // first four bytes are 32 bit time
                // next two bytes are 16 bit date
                d = Misc.PackedDatefromDateTime(timesince);
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "Getting reports since " + timesince.ToString(), args.cid.ToString());
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "2-byte date is " + d.ToString("X"), args.cid.ToString());
                s = (UInt32)timesince.TimeOfDay.TotalSeconds;
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "4-byte time is " + s.ToString("X"), args.cid.ToString());
                var mem = new MemoryStream(0);
                LegacyUint16ToNetworkBytes(d, mem);
                LegacyUint32ToNetworkBytes(s, mem);
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_CONTROLLER_REPORT_DATA_TO_PC, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }


        
        //
        // Action spun up by Task.Run() above to get reports
        //
        // content - unsigned short: The number of lines to send; ranges from 1 to 20
        //
        static void ActionGetStationHistory(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Get Station History for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                ushort lines = (ushort)Convert.ToInt16(ConfigurationManager.AppSettings["LegacyLogLinesRequestNumber"]);
                if (lines <= 0) lines = 2;  // default to 2
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "819", "Number of lines requested is " + lines.ToString(), args.cid.ToString());
                var mem = new MemoryStream(0);
                LegacyUint16ToNetworkBytes(lines, mem);
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_LOGLINES_CONTROLLER_TO_PC, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionSendTurnOn(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Turn On for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_TURNON, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionSendTurnOff(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Turn Off for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_TURNOFF, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionSendPasswordReset(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Password Reset for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_PASSWORDS_RESET, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionSendPasswordUnlock(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Password Unlock for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_PASSWORDS_UNLK, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }
        //
        // Action spun up by Task.Run() above 
        //
        // content - none
        //
        static void ActionSendClearMLB(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Clear MLB for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");

                if (param1 != null)
                {
                    var mem = new MemoryStream(0);
                    // content depends on 500 vs 600 series - for 600 unsigned char index of POC to clear 0,1,2
                    if (LegacyControllerSeries(args.cid) == 6)
                    {
                        mem.WriteByte((byte)param1);    // get 1st argument for this job
                    }
                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_CLEARMLB, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }
        //
        // Action spun up by Task.Run() above 
        //
        // content - none
        //
        static void ActionSetHOAllStations(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Set Holdover All Stations for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);

                //	long: The amount of time to apply to the hold-over (hard coded to 0)
                mem.WriteByte(0);
                mem.WriteByte(0);
                mem.WriteByte(0);
                mem.WriteByte(0);

                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETHO_ALL_STATIONS, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }
        //
        // Action spun up by Task.Run() above 
        //
        // content - none
        //
        static void ActionSetHOByProgram(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Set Holdover By Program for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");

                if (param1 != null)
                {
                    var mem = new MemoryStream(0);
                    // content is 	
                    //  unsigned char: The program to clear hold-over for; ranges from 0 to 6
                    //	long: The amount of time to apply to the hold-over (hard coded to 0)
                    mem.WriteByte((byte)param1);    // get 1st argument for this job

                    mem.WriteByte(0);
                    mem.WriteByte(0);
                    mem.WriteByte(0);
                    mem.WriteByte(0);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETHO_BY_PROG, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }
        //
        // Action spun up by Task.Run() above 
        //
        // content - none
        //
        static void ActionSetHOByStation(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Set Holdover By Station for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");

                if (param1 != null)
                {
                    var mem = new MemoryStream(0);
                    // content is 	
                    //  unsigned short: The station to clear hold-over for; ranges from 0 to 47
                    //	long: The amount of time to apply to the hold-over (hard coded to 0)
                    LegacyUint16ToNetworkBytes((ushort)param1, mem);

                    mem.WriteByte(0);
                    mem.WriteByte(0);
                    mem.WriteByte(0);
                    mem.WriteByte(0);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETHO_BY_STATION, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }
        //
        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionSetNOWAllStations(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Set No Water Days All Stations for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");

                if (param1 != null)
                {
                    var mem = new MemoryStream(0);
                    // content is 	
                    //  unsigned short: The number of no water days to set; ranges from 0 to 31
                    LegacyUint16ToNetworkBytes((ushort)param1, mem);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETNOWDAYS_ALL_STATIONS, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }
        //
        // Action spun up by Task.Run() above 
        //
        // content - none
        //
        static void ActionSetNOWByProgram(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;
            int? param2;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Set No Water Days By Program for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");
                param2 = GetJobArgumentInt(args.jobid, "Param2");

                if (param1 != null && param2 != null)
                {
                    var mem = new MemoryStream(0);
                    // content is 	
                    //  unsigned char: The program to set no water days for; ranges from 0 to 6
                    //	unsigned short: The number of no water days to set; ranges from 0 to 31

                    mem.WriteByte((byte)param1);    // get 1st argument for this job
                    LegacyUint16ToNetworkBytes((ushort)param2, mem);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETNOWDAYS_BY_PROG, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 or 2 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }
        //
        // Action spun up by Task.Run() above 
        //
        // content - none
        //
        static void ActionSetNOWByStation(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;
            int? param2;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Set No Water Days By Stations for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");
                param2 = GetJobArgumentInt(args.jobid, "Param2");

                if (param1 != null && param2 != null)
                {
                    var mem = new MemoryStream(0);
                    // content is 	
                    //  unsigned short: The station to set no water days for; ranges from 0 to 47
                    //  unsigned short: The number of no water days to set; ranges from 0 to 31

                    LegacyUint16ToNetworkBytes((ushort)param1, mem);
                    LegacyUint16ToNetworkBytes((ushort)param2, mem);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETNOWDAYS_BY_STATION, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 or 2 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }
        //
        // Action spun up by Task.Run() above 
        //
        // content - none
        //
        static void ActionGetFlowRecords(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Get Flow Records for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);

                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SEND_FLOW_RECORDER_RECORDS_TO_PC, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }     
        //
        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionMasterValveOverride(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;
            int? param2;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Master Valve Override for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");
                param2 = GetJobArgumentInt(args.jobid, "Param2");

                if (param1 != null && param2 != null)
                {
                    var mem = new MemoryStream(0);
                    // content
                    //  unsigned char: Whether to open (0), close (1) or clear (2) the master valve override
                    //  Note: Only controllers with 600 series firmware support closing (1) the master valve
                    // unsigned long: The length of time, in seconds, to open or close the master valve; ranges from 306-172800
                    mem.WriteByte((byte)param1);    // get 1st argument for this job
                    LegacyUint32ToNetworkBytes((ushort)param2, mem);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_MVOR_TO_CONTROLLER, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 or 2 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }             
        
        //
        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionResetFlowTable(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            int i;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Reset Flow Table for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                // content
                //   long[1350]: The table values hard-coded to 0's

                for (i = 0; i < 1350 * 4; i++ )
                {
                    mem.WriteByte((byte)0);    // bunch of zeros
                }

                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DERATE_TABLE_TO_CONTROLLER, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        } 

        //
        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionLightsOverride(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] s;
            byte[] data;
            int i;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Lights Override for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                // content
                //  LIGHTS_COMMAND_BITFIELD: 1-byte bit field indicating what to do
                // data is in blob Param4
                s = GetJobArgumentBytes(args.jobid, "Param4");

                if (s != null)
                {
                    var mem = new MemoryStream(0);

                    i = 0;
                    while (s != null && i < s.Length)
                    {
                        mem.WriteByte((byte)s[i++]);
                    }

                    data = mem.ToArray();

                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_LIGHTS_COMMAND_FROM_PC, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 4 to be byte[], null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        } 

        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionPercentAdjustProgramTag(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            int i;
            byte[] s;
            byte[] data;
            bool nullseen = false;
            bool forceToEndTxMessage = true;
            int? param1;
            int? param2;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Percent Adj. Pgm Tag for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                // content
                //	unsigned char *: The program tag to set percent adjust for; max string length of 20 chars
                // (this is a fixed 20 bytes - null padded)
                //  unsigned short: The percentage to set; ranges from 20 to 200
                //  unsigned short: The number of days to set; ranges from 0 to 366 OR 65535
                // Possible Responses:
                // MID_SETPERCENTADJUST_BY_PTAG_RESP = 1471
                // MID_SETPERCENTADJUST_PERCENTAGE_OUT_OF_RANGE_RESP = 1472
                // MID_SETPERCENTADJUST_DAYS_OUT_OF_RANGE_RESP = 1473
                // MID_PTAG_DOES_NOT_EXIST_RESP = 1475

                // program tag is in blob Param4
                s = GetJobArgumentBytes(args.jobid, "Param4");
                param1 = GetJobArgumentInt(args.jobid, "Param1");
                param2 = GetJobArgumentInt(args.jobid, "Param2");

                if (param1 != null && param2 != null && s != null)
                {
                    var mem = new MemoryStream(0);

                    for (i = 0; i < 20; i++)
                    {
                        if (s != null && i < s.Length)   // n.b. second argument not evaluated unless first && check is true
                        {
                            mem.WriteByte(nullseen ? (byte)s[i] : (byte)0);
                            if (s[i] == 0)
                            {
                                nullseen = true;   // once we see a null stop using blob data just in case
                            }
                        }
                        else
                        {
                            mem.WriteByte(0);
                        }
                    }
                    LegacyUint16ToNetworkBytes((ushort)param1, mem);   // percentage 20-200
                    LegacyUint16ToNetworkBytes((ushort)param2, mem);   // number of days 0-366 or 65535


                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETPERCENTADJUST_BY_PTAG, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameters to be int or byte[], null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        } 


        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionClearHoldoverProgramTag(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            int i;
            byte[] s;
            byte[] data;
            bool nullseen = false;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Clear Hold-over by Prog. Tag for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                // content
                //		unsigned char *: The program tag to clear hold-over for; max string length of 20 chars
                //  long: The amount of time to apply to the hold-over (Wissam: always pass a 0 from the UI)


                // program tag is in blob Param4
                s = GetJobArgumentBytes(args.jobid, "Param4");

                if (s != null)
                {
                    var mem = new MemoryStream(0);

                    for (i = 0; i < 20; i++)
                    {
                        if (s != null && i < s.Length)   // n.b. second argument not evaluated unless first && check is true
                        {
                            mem.WriteByte(nullseen ? (byte)s[i] : (byte)0);
                            if (s[i] == 0)
                            {
                                nullseen = true;   // once we see a null stop using blob data just in case
                            }
                        }
                        else
                        {
                            mem.WriteByte(0);
                        }
                    }
                    LegacyUint32ToNetworkBytes((uint)0, mem);   // hard-coded 0 for amount of time

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETHO_BY_PTAG, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 4 to be byte[], null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        } 


        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionSetStopTimesProgramTag(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            int i;
            byte[] s;
            byte[] data;
            bool nullseen = false;
            bool forceToEndTxMessage = true;
            int? param1;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Set Stop Times by Prog. Tag for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                // content
                //			unsigned char *: The program tag to set the stop time for; max string length of 20 chars
                // unsigned long: The stop time to set; ranges from 0 to 86400 (86400 = OFF)


                // program tag is in blob Param4
                s = GetJobArgumentBytes(args.jobid, "Param4");

                param1 = GetJobArgumentInt(args.jobid, "Param1");

                if (param1 != null && s != null)
                {
                    var mem = new MemoryStream(0);

                    for (i = 0; i < 20; i++)
                    {
                        if (s != null && i < s.Length)   // n.b. second argument not evaluated unless first && check is true
                        {
                            mem.WriteByte(nullseen ? (byte)s[i] : (byte)0);
                            if (s[i] == 0)
                            {
                                nullseen = true;   // once we see a null stop using blob data just in case
                            }
                        }
                        else
                        {
                            mem.WriteByte(0);
                        }
                    }
                    LegacyUint32ToNetworkBytes((uint)param1, mem);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETSTOPTIME_BY_PTAG, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameters to be integer or byte[], null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        } 

        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionSetStartTimesProgramTag(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            int i;
            byte[] s;
            byte[] data;
            bool nullseen = false;
            bool forceToEndTxMessage = true;
            int? param1;
            int? param2;
            int? param3;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Set Start Times by Prog. Tag for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                // content
                // unsigned char *: The program tag to set the start time for; max string length of 20 chars
                // unsigned long: The start time to set; ranges from 0 to 86400 (86400 = OFF)
                // unsigned short: The start month; ranges from 0 to 12 (default to 0 unless in 12 month)
                // unsigned short: The end month; ranges from 0 to 12 (default to 0 unless in 12 month)


                // program tag is in blob Param4
                s = GetJobArgumentBytes(args.jobid, "Param4");
                param1 = GetJobArgumentInt(args.jobid, "Param1");
                param2 = GetJobArgumentInt(args.jobid, "Param2");
                param3 = GetJobArgumentInt(args.jobid, "Param3");

                if (param1 != null && param2 != null && param3 != null && s != null)
                {

                    var mem = new MemoryStream(0);

                    for (i = 0; i < 20; i++)
                    {
                        if (s != null && i < s.Length)   // n.b. second argument not evaluated unless first && check is true
                        {
                            mem.WriteByte(nullseen ? (byte)s[i] : (byte)0);
                            if (s[i] == 0)
                            {
                                nullseen = true;   // once we see a null stop using blob data just in case
                            }
                        }
                        else
                        {
                            mem.WriteByte(0);
                        }
                    }
                    LegacyUint32ToNetworkBytes((uint)param1, mem);
                    LegacyUint16ToNetworkBytes((ushort)param2, mem);
                    LegacyUint16ToNetworkBytes((ushort)param3, mem);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETSTARTTIME_BY_PTAG, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameters to be int or byte[], null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        } 

        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionNoWaterDaysProgramTag(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            int i;
            byte[] s;
            byte[] data;
            bool nullseen = false;
            bool forceToEndTxMessage = true;
            int? param1;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send No Water Days by Prog. Tag for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                // content
                // unsigned char *: The program tag to set no water days for; max string length of 20 chars
                // unsigned short: The number of no water days to set; ranges from 0 to 31


                // program tag is in blob Param4
                s = GetJobArgumentBytes(args.jobid, "Param4");
                param1 = GetJobArgumentInt(args.jobid, "Param1");

                if (param1 != null && s != null)
                {
                    var mem = new MemoryStream(0);

                    for (i = 0; i < 20; i++)
                    {
                        if (s != null && i < s.Length)   // n.b. second argument not evaluated unless first && check is true
                        {
                            mem.WriteByte(nullseen ? (byte)s[i] : (byte)0);
                            if (s[i] == 0)
                            {
                                nullseen = true;   // once we see a null stop using blob data just in case
                            }
                        }
                        else
                        {
                            mem.WriteByte(0);
                        }
                    }
                    LegacyUint16ToNetworkBytes((ushort)param1, mem);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SETNOWDAYS_BY_PTAG, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameters to be int or byte[], null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        } 


        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionSendLights(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            int i;
            byte[] s;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Lights for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                // content
                // Content:  unsigned char *: a block of data containing the lights data.

                // data is in blob Param4
                s = GetJobArgumentBytes(args.jobid, "Param4");

                if (s != null)
                {
                    var mem = new MemoryStream(0);

                    i = 0;
                    while (s != null && i < s.Length)
                    {
                        mem.WriteByte((byte)s[i++]);
                    }

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_LIGHTS_TO_CONTROLLER, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 4 to be byte[], null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionGetLights(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Get Lights for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                // content
                // none
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_LIGHTS_TO_PC, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        } 


        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionSendManualPrograms(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            int i;
            byte[] s;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Manual Programs for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                // content
                // Content:  unsigned char *: a block of data containing the CMOS.

                // data is in blob Param4
                s = GetJobArgumentBytes(args.jobid, "Param4");
                if (s != null)
                {
                    var mem = new MemoryStream(0);

                    i = 0;
                    while (s != null && i < s.Length)
                    {
                        mem.WriteByte((byte)s[i++]);
                    }

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_MANUALSTUFF_TO_CONTROLLER, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 4 to be byte[], null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionGetManualPrograms(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Get Manual Programs for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                // content
                // 0x1F to get all manual program stuff (manual program 1,manual program 2, lights , MVOR ..)
                mem.WriteByte((byte)0x1F);

                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_MANUALSTUFF_TO_PC, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionSendProgramData(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            DateTime? pgmTimestamp;

            byte[] s;
            byte[] data;
            bool forceToEndTxMessage = true;
            pgmTimestamp = null;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Program Data for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                int model = LegacyControllerSeries(args.cid);

                if (model != 6 && model != 5)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unknown legacy controller version !", args.cid.ToString());
                }
                else
                {
                    var mem = new MemoryStream(0);

                    s = GetJobArgumentBytes(args.jobid, "Param4");

                    try
                    {
                        pgmTimestamp = DateTime.FromBinary(BitConverter.ToInt64(s, 0)); // pgmTimestamp stored in bytes
                    }
                    catch (Exception ex)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "989", "Error while reading PgmTimestamp from IHSFY_Jobs, cid: " + args.cid + "   ex: " + ex.Message, args.cid.ToString());
                    }

                    if (pgmTimestamp.HasValue)
                    {
                        if (!BuildPdata(mem, (int)args.cid, (model == 6), (DateTime)pgmTimestamp))
                        {
                            data = mem.ToArray();
                            LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_PDATA_TO_CONTROLLER, data, args.cid, args.jobid);
                            mem.Dispose();
                            forceToEndTxMessage = false;
                        }
                    }
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionGetProgramData(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Get Program Data for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                // content
                // Content:  none

                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_PDATA_TO_PC, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }


        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionSendCMOS(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            byte[] s;
            int i;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send CMOS for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                // content
                // Content:  unsigned byte[]: a block of data containing the CMOS. A function will be provided to build this data.

                // data is in blob Param4
                s = GetJobArgumentBytes(args.jobid, "Param4");

                if (s != null)
                {
                    var mem = new MemoryStream(0);

                    i = 0;
                    while (s != null && i < s.Length)
                    {
                        mem.WriteByte((byte)s[i++]);
                    }

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_CMOS_TO_CONTROLLER, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 4 to be byte[], null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void ActionSendPasswords(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            int i;
            byte[] s;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Passwords for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                // content
                // Content: 
                // unsigned char [100]: a block of data containing the access profile. A function will be provided to build this data.
                // LOGIN_STRUCT [10]: a block of data containing the login codes. A function will be provided to build this data.

                // data is in blob Param4
                s = GetJobArgumentBytes(args.jobid, "Param4");

                if (s != null)
                {
                    var mem = new MemoryStream(0);

                    i = 0;
                    while (s != null && i < s.Length)
                    {
                        mem.WriteByte((byte)s[i++]);
                    }

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_PASSWORDS_TO_CONTROLLER, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 4 to be byte[], null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        private static bool BuildPdata(System.IO.MemoryStream mem, int cid, bool isET2000e, DateTime PgmTimeStamp)
        {
            string[] sqlQuers;
            bool abort = false;

            AdsCommand cmd;
            AdsDataReader rdr;

            if (isET2000e)
            {
                sqlQuers = LegacyProgramData.PDATA_600;
            }
            else
            {
                sqlQuers = LegacyProgramData.PDATA_500;
            }
            
            ServerDB db = new ServerDB(_cts);

            AdsConnection conn = db.OpenDBConnectionNotAsync(cid.ToString());
            
            if (conn != null)
            {
                foreach (var s in sqlQuers)
                {
                    do
                    {
                        try
                        {
                            cmd = new AdsCommand(s, conn);
                            AdsParameter cidParam = new AdsParameter("ControllerID", DbType.Int32);
                            AdsParameter dateParam = new AdsParameter("PgmTimeStamp", DbType.DateTime);
                            
                            cidParam.Value = cid;
                            dateParam.Value = PgmTimeStamp;

                            cmd.Parameters.Add(cidParam);
                            cmd.Parameters.Add(dateParam);

                            rdr = cmd.ExecuteReader();

                            DataTable tabledata = new DataTable();

                            if (rdr.HasRows)
                            {
                                tabledata.Load(rdr);
                            }

                            rdr.Close();

                            if (tabledata.Rows.Count > 0)
                            {
                                abort = BuildET2000Message(mem, tabledata, cid, LegacyProgramData.PDATA_SIZE, isET2000e);
                            }

                            cmd.Unprepare();
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "11791", "Database exception during building Legacy Program Data: " + e.ToString(), cid.ToString());

                            abort = true;
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    if (abort == true)
                    {
                        break;
                    }
                }

                db.CloseDBConnection(conn, cid.ToString());

            }

            return abort;
        }

        public static bool BuildET2000Message(System.IO.MemoryStream str, DataTable tabledata, int ControllerID, Dictionary<string, int> structure, bool checkPaddingChar, bool logIt = false)
        {
            bool abort;

            int offset;
            char paddingChar;

            UInt16 bitsHolder;
            int bitNumber;

            abort = false;
            offset = 0;
            paddingChar = (char)0x20;

            bitsHolder = 0;
            bitNumber = 0;

            foreach (DataColumn column in tabledata.Columns)
            {
                int colSize = structure[column.ColumnName];

                switch (Type.GetTypeCode(column.DataType))
                {
                    case TypeCode.String:
                        string val = tabledata.Rows[0][column].ToString();

                        if (logIt)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Debug, "11182", "column: " + column + " is string, size: " + colSize + " val: " + val, ControllerID.ToString());
                        }

                        if (checkPaddingChar)
                        {
                            paddingChar = LegacyProgramData.PDATA_ZERO_CHAR_PADDING.Any(v => v == column.ColumnName) ? (char)0 : (char)0x20;
                        }

                        str.Write(System.Text.ASCIIEncoding.Default.GetBytes(val.PadRight(colSize, paddingChar)), 0, colSize);

                        break;
                    case TypeCode.Boolean:
                        if (logIt)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Debug, "11182", "column: " + column + " is boolean, val: " + tabledata.Rows[0][column].ToString(), ControllerID.ToString());
                        }

                        str.WriteByte((Byte)((Boolean)tabledata.Rows[0][column] ? 1 : 0));

                        break;
                    case TypeCode.Object:
                        if (column.DataType.FullName == "System.Byte[]")
                        {
                            byte[] rawArray = (byte[])tabledata.Rows[0][column];

                            if (logIt)
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Debug, "11182", "column: " + column + " is byte[], val: " + BitConverter.ToString(rawArray), ControllerID.ToString());
                            }

                            str.Write(rawArray, 0, rawArray.Length);
                        }
                        else
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "11182", "Unknown column type of object while parsing legacy data, cid: " + ControllerID + " column name: " + column.ColumnName, ControllerID.ToString());

                            abort = true;
                        }
                        break;
                    case TypeCode.Double:
                    case TypeCode.Decimal:
                    case TypeCode.Int32:
                    case TypeCode.Int16:

                        byte[] workingarray;

                        if (colSize == 8)
                        {
                            workingarray = BitConverter.GetBytes(Convert.ToDouble(tabledata.Rows[0][column]));
                        }
                        else if (colSize == 4)
                        {
                            workingarray = BitConverter.GetBytes(Convert.ToUInt32(tabledata.Rows[0][column]));
                        }
                        else if (colSize == 2)
                        {
                            workingarray = BitConverter.GetBytes(Convert.ToUInt16(tabledata.Rows[0][column]));
                        }
                        else if (colSize == 1)
                        {
                            workingarray = new byte[] { Convert.ToByte(tabledata.Rows[0][column]) };
                        }
                        else if (colSize == 0) // bit stack them as UInt16
                        {
                            bitsHolder = (UInt16)(bitsHolder | (Convert.ToUInt16(tabledata.Rows[0][column]) << bitNumber));
                            bitNumber++;

                            // do we have the full 16 bits?
                            if (bitNumber == 16)
                            {
                                workingarray = BitConverter.GetBytes(bitsHolder);
                                bitNumber = 0;
                                bitsHolder = 0;
                                colSize = 2;
                            }
                            else
                            {
                                // don't go further, we didn't get the full 16 bits
                                continue;
                            }
                        }
                        else
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "11182", "Unknown column size while parsing legacy data, cid: " + ControllerID + " column name: " + column.ColumnName, ControllerID.ToString());

                            abort = true;

                            break;
                        }

                        if (logIt)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Debug, "11182", "column: " + column + " is a number, size: " + colSize + " val: " + tabledata.Rows[0][column].ToString(), ControllerID.ToString());
                        }

                        if (BitConverter.IsLittleEndian)
                        {
                            Array.Reverse(workingarray, 0, workingarray.Length);
                        }

                        str.Write(workingarray, 0, workingarray.Length);

                        break;
                    default:
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "11182", "Unknown column type while parsing legacy data, cid: " + ControllerID + " column name: " + column.ColumnName, ControllerID.ToString());

                        abort = true;
                        break;
                }

                offset += colSize;

                if (abort)
                {
                    break;
                }
            }


            return abort;
        }

        static void ActionGetRain(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Get Rain for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                // content
                // none
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SHARING_GET_RAIN, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        static void ActionGetET(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Get ET for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                // content
                // none
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SHARING_GET_ET, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        // 
        // SendWeatherData - called once per day for controllers that need weather data sent to them.
        // the data may come from RainShare/ETShar, or from WeatherSense API
        //
        static void ActionSendWeatherData(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            ControllerWeatherInfo wi = new ControllerWeatherInfo();
            byte[] data;
            bool forceToEndTxMessage = true;
            Int32 ETStatus = -1;     // values to send to controller
            Int32 RainStatus = -1;
            Int32 RainValue = 0;
            Int32 ETValue = 0;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Weather Data for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);

                // get controller weather info from DB
                if(LookupLegacyControllerWeatherShare(args.cid, wi) == 0)
                {
                    // we have possible lat/lon/ETshareid/Rainshareid... decide where to get weather based on those
                    if ((wi.ETID == null || wi.ETID == -2 || wi.ETID == -1) && (wi.rainID == null || wi.rainID == -2 || wi.rainID == -1))
                    {
                        // no weather info to send.
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "185", "No ET/Rain configured for Legacy Controller", args.cid.ToString());
                        LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_SUCCESSFUL, null, args.cid);
                        return;
                    }
                    else
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "186", "ET/Rain configured for Legacy Controller, gathering... ", args.cid.ToString());
                        if (wi.ETID >= 0)
                        {
                            if (wi.ETStatus != -1)
                            {
                                ETStatus = wi.ETStatus;
                                ETValue = wi.ETValue;
                            }
                            else
                            {
                                var msg = "No ET data available (WeatherSense)";
                                Misc.AddAlert(_cts, args.cid, msg, true, false);
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "2888", msg, args.cid.ToString());
                            }
                        }
                        if (wi.rainID >= 0)
                        {
                            if (wi.RainStatus != -1)
                            {
                                RainStatus = wi.RainStatus;
                                RainValue = wi.RainValue;
                            }
                            else
                            {
                                var msg = "No Rain data available (WeatherSense)";
                                Misc.AddAlert(_cts, args.cid, msg, true, false);
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "2888", msg, args.cid.ToString());
                            }
                        }

                        // see if we have something to send
                        if (RainStatus >= 0 || ETStatus >= 0)
                        {
                                //    •	unsigned char whats_included – Either 0, 2, 4, or 6 (nothing, ET, Rain, Both)
                                // •	ET_TABLE_ENTRY – (if whats_included is 2 or 6)
                                // •	RAIN_TABLE_ENTRY – (if whats_included is 4 or 6)
                            byte val1 = 0;
                            if (RainStatus >= 0) val1 |= 4;
                            if (ETStatus >= 0) val1 |= 2;

                            mem.WriteByte(val1);        // whats included byte

                            if(ETStatus >= 0)
                            {
                                LegacyUint16ToNetworkBytes((ushort)ETValue, mem);
                                mem.WriteByte((byte)ETStatus);
                                mem.WriteByte((byte)0);
                                _log.WriteMessageWithId(NLog.LogLevel.Info, "197", "Sending ET with value: " + ETValue.ToString(), args.cid.ToString());
                            }

                            if (RainStatus >= 0)
                            {
                                LegacyUint16ToNetworkBytes((ushort)RainValue, mem);
                                mem.WriteByte((byte)RainStatus);
                                mem.WriteByte((byte)0);
                                _log.WriteMessageWithId(NLog.LogLevel.Info, "197", "Sending Rain with value: " + RainValue.ToString(), args.cid.ToString());
                            }

                            data = mem.ToArray();
                            LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SHARING_ETRAIN_TO_CONTROLLER, data, args.cid, args.jobid);
                            mem.Dispose();
                            forceToEndTxMessage = false;
                        }
                        else
                        {
                            // no data for this request, terminate the job now
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "199", "No shared Rain or ET data available to be sent.", args.cid.ToString());

                            LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_NO_WEATHERSENSE_DATA, null, args.cid);
                            return;
                        }
                    }
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }


        static void ActionPollRain(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Rain Poll for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                // content
                // none
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SHARING_RAIN_POLL, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Job driven by IHSFY entries as a result of rain polling
        //
        static void ActionSendRainShutdown(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Rain Shutdown for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");

                if (param1 != null)
                {

                    var mem = new MemoryStream(0);
                    // content
                    // unsigned short: The date returned in rain poll, placed in Param1 when job created
                    LegacyUint16ToNetworkBytes((ushort)param1, mem);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        } 

        static void Action2000HubChange(ActionArgs args)
        {
            int i;

            // tell hub handler about a change in controllers
            i = HubConnectionTracker.Process2000HubChange(args.hostname, args.cityid, args.cid);

            // based on what hub handler returns, complete job with status
            if (i == 0) CompleteLegacyJob((int)args.jobid, Constants.JOB_SUCCESSFUL, null);
            else CompleteLegacyJob((int)args.jobid, Constants.JOB_INVALID_RESPONSE, null);

            return;
        }

        //
        // return 0 means success, otherwise error
        //
        static int LookupLegacyControllerWeatherShare(uint cid, ControllerWeatherInfo returninfo)
        {
            int ret = -1;   // assume error
            // record in database
            ServerDB db = new ServerDB(_cts);

            returninfo.rainIsNID = false;   // assume legacy sharing
            returninfo.ETIsNID = false;

            AdsConnection conn = db.OpenDBConnectionNotAsync("");
            if (conn != null)
            {
                try
                {
                    AdsCommand cmd = new AdsCommand("SELECT IIF(Latitude='',NULL,Latitude), IIF(Longitude='',NULL,Longitude), ETSharingControllerID, RainSharingControllerID, ETSharingNetworkID, RainSharingNetworkID from Controllers where Deleted = false and ControllerID = " + cid.ToString(), conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                    AdsDataReader rdr = cmd.ExecuteReader();
                    if(rdr.HasRows)
                    {
                        rdr.Read();
                        if (!(rdr[0] is DBNull))
                        {
                            returninfo.lat = double.Parse(rdr.GetString(0));
                        }
                        else
                        {
                            returninfo.lat = null;
                        }
                        if (!(rdr[1] is DBNull))
                        {
                            returninfo.lon = double.Parse(rdr.GetString(1));
                        }
                        else
                        {
                            returninfo.lon = null;
                        }
                        if (!(rdr[2] is DBNull))
                        {
                            returninfo.ETID = rdr.GetInt32(2);
                        }
                        else
                        {
                            returninfo.ETID = null;
                        }
                        if (!(rdr[3] is DBNull))
                        {
                            returninfo.rainID = rdr.GetInt32(3);
                        }
                        else
                        {
                            returninfo.rainID = null;
                        }
                        if (!(rdr[4] is DBNull))
                        {
                            if (rdr.GetInt32(4) > 0)
                            {
                                // ET is shared from a network ID
                                returninfo.ETID = rdr.GetInt32(4);
                                returninfo.ETIsNID = true;
                            }
                        }
                        if (!(rdr[5] is DBNull))
                        {
                            if (rdr.GetInt32(5) > 0)
                            {
                                // rain is shared from a network ID
                                returninfo.rainID = rdr.GetInt32(5);
                                returninfo.rainIsNID = true;
                            }
                        }

                        ret = 0; // valid data in struct we are returning
                    }
                    rdr.Close();
                    cmd.Unprepare();

                    // if controller gets rain / ET from another controller, load that up now
                    if(ret == 0 && ((!returninfo.rainIsNID && returninfo.rainID > 0) || (!returninfo.ETIsNID && returninfo.ETID > 0)))
                    {
                        ret = -1;
                        if (!returninfo.ETIsNID && returninfo.ETID > 0)
                        {
                            returninfo.ETStatus = -1;
                            returninfo.ETValue = -1;
                            cmd = new AdsCommand("SELECT ifnull(ETStatus, 0), ifnull(ET, 0) FROM ETShare WHERE " +
                                                  " (ControllerID=(SELECT ETSharingControllerID FROM Controllers WHERE ControllerID=" + cid.ToString() + ") " +
                                                  " AND ETDate=CURDATE())", conn);
                            _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            rdr = cmd.ExecuteReader();
                            if(rdr.HasRows)
                            {
                                rdr.Read();
                                if (!(rdr[0] is DBNull))
                                {
                                    returninfo.ETStatus = rdr.GetInt32(0);
                                }
                                if (!(rdr[1] is DBNull))
                                {
                                    returninfo.ETValue = rdr.GetInt32(1);
                                }
                            }
                            rdr.Close();
                            cmd.Unprepare();
                        }
                        if (!returninfo.rainIsNID && returninfo.rainID > 0)
                        {
                            returninfo.RainStatus = -1;
                            returninfo.RainValue = -1;
                            cmd = new AdsCommand("SELECT ifnull(RainStatus, 0), ifnull(Rain, 0) FROM RainShare WHERE " +
                                                  " (ControllerID=(SELECT RainSharingControllerID FROM Controllers WHERE ControllerID=" + cid.ToString() + ") " +
                                                  " AND RainDate=CURDATE())", conn);
                            _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            rdr = cmd.ExecuteReader();
                            if (rdr.HasRows)
                            {
                                rdr.Read();
                                if (!(rdr[0] is DBNull))
                                {
                                    returninfo.RainStatus = rdr.GetInt32(0);
                                }
                                if (!(rdr[1] is DBNull))
                                {
                                    returninfo.RainValue = rdr.GetInt32(1);
                                }
                            }
                            rdr.Close();
                            cmd.Unprepare();
                        }
                        ret = 0;
                    }
                    // if controller gets rain / ET from another **network ID**, load that up now
                    if (ret == 0 && ((returninfo.rainIsNID && returninfo.rainID > 0) || (returninfo.ETIsNID && returninfo.ETID > 0)))
                    {
                        ret = -1;
                        if (returninfo.ETIsNID && returninfo.ETID > 0)
                        {
                            returninfo.ETStatus = -1;
                            returninfo.ETValue = -1;
                            cmd = new AdsCommand("SELECT ifnull(ETStatus, 0), ifnull(ETValue, 0) FROM CS3000WeatherData WHERE " +
                                                  " ETStatus<>0 AND " + // don't share Historical et
                                                  " (NetworkID=" + returninfo.ETID.ToString() +
                                                  " AND CAST(WeatherTimeStamp as SQL_DATE) = CURDATE())  ", conn);
                            _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            rdr = cmd.ExecuteReader();
                            if (rdr.HasRows)
                            {
                                rdr.Read();
                                if (!(rdr[0] is DBNull))
                                {
                                    returninfo.ETStatus = rdr.GetInt32(0);

                                    // @WB: we need to modify the 3000 ET status to match with the 2000 ET status.
                                    if (returninfo.ETStatus == 4) returninfo.ETStatus = 2; // 4 "ET_STATUS_SHARED_FROM_THE_CENTRAL" = (2000)FROM_ET_GAGE = 2, 
                                }
                                if (!(rdr[1] is DBNull))
                                {
                                    returninfo.ETValue = (Int32)(rdr.GetInt32(1) / 100.0); // @wb: divided by 100.. we store the ET a 10000 up for 3000
                                }
                            }
                            rdr.Close();
                            cmd.Unprepare();
                        }
                        if (returninfo.rainIsNID && returninfo.rainID > 0)
                        {
                            returninfo.RainStatus = -1;
                            returninfo.RainValue = -1;
                            cmd = new AdsCommand("SELECT ifnull(RainStatus, 0), ifnull(RainValue, 0) FROM CS3000WeatherData WHERE " +
                                                  " (NetworkID=" + returninfo.rainID.ToString() +
                                                  " AND CAST(WeatherTimeStamp as SQL_DATE) = CURDATE())  ", conn);
                            _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            rdr = cmd.ExecuteReader();
                            if (rdr.HasRows)
                            {
                                rdr.Read();
                                if (!(rdr[0] is DBNull))
                                {
                                    returninfo.RainStatus = rdr.GetInt32(0);

                                    // @WB: we need to modify the 3000 ET status to match with the 2000 ET status.
                                    if (returninfo.RainStatus == 4) returninfo.RainStatus = 6; //RAIN_STATUS_R = 6
                                    else if (returninfo.RainStatus == 0) returninfo.RainStatus = 7; // RAIN_STATUS_M = 7
                                }
                                if (!(rdr[1] is DBNull))
                                {
                                    returninfo.RainValue = rdr.GetInt32(1);
                                }
                            }
                            rdr.Close();
                            cmd.Unprepare();
                        }
                        ret = 0;
                    }
                    // if controller gets weather from new source (0) do that now
                    // if controller gets rain / ET from another **network ID**, load that up now
                    if (ret == 0 && (returninfo.rainID == 0) || (returninfo.ETID == 0))
                    {
                        ret = -1;
                        if (returninfo.ETID == 0)
                        {
                            _log.WriteMessage(NLog.LogLevel.Info, "182", "Using new WeatherSenseState for ET on " + cid.ToString());
                            returninfo.ETStatus = -1;
                            returninfo.ETValue = -1;
                            cmd = new AdsCommand("SELECT ifnull(ETStatus, -1), ifnull(ETValue, 0)  FROM WeatherSenseData  WHERE " +
                                                " (ControllerID=" + cid.ToString() + 
                                                " AND ControllerModel < 7 AND WeatherDate=CURDATE()) ", conn);
                            _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            rdr = cmd.ExecuteReader();
                            if (rdr.HasRows)
                            {
                                rdr.Read();
                                if (!(rdr[0] is DBNull))
                                {
                                    returninfo.ETStatus = rdr.GetInt32(0);
                                }
                                if (!(rdr[1] is DBNull))
                                {
                                    returninfo.ETValue = (Int32)(rdr.GetDouble(1) * 100.0); // @wb: convert from double to hundredths of inches
                                }
                            }
                            rdr.Close();
                            cmd.Unprepare();
                        }
                        if (returninfo.rainID == 0)
                        {
                            _log.WriteMessage(NLog.LogLevel.Info, "182", "Using new WeatherSenseState for Rain on " + cid.ToString());
                            returninfo.RainStatus = -1;
                            returninfo.RainValue = -1;
                            cmd = new AdsCommand("SELECT ifnull(RainStatus, -1), ifnull(RainValue, 0) FROM WeatherSenseData  WHERE " +
                                                " (ControllerID=" + cid.ToString() +
                                                " AND ControllerModel < 7 AND WeatherDate=CURDATE()) ", conn);
                            _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            rdr = cmd.ExecuteReader();
                            if (rdr.HasRows)
                            {
                                rdr.Read();
                                if (!(rdr[0] is DBNull))
                                {
                                    returninfo.RainStatus = rdr.GetInt32(0);
                                }
                                if (!(rdr[1] is DBNull))
                                {
                                    returninfo.RainValue = (Int32)(rdr.GetDouble(1) * 100.0); // @wb: convert from double to hundredths of inches
                                }
                            }
                            rdr.Close();
                            cmd.Unprepare();
                        }
                        ret = 0;
                    }

                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "14394", "Database exception during Legacy query: " + e.ToString());
                }
                db.CloseDBConnection(conn, "");

            }
            return (ret);
        }

        /*************************************************************
         * 
         * Direct Access
         * 
         *************************************************************/

        //
        // Action spun up by Task.Run() above
        //
        // content - TRUE (0x01) screen dumps will be happening
        //
        static void Action_DirectAccess_Start(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Direct Access-Start command for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);

                // when TRUE (0x01) screen dumps will be happening
                mem.WriteByte(0x01);

                // DON'T forget about the exsistence of this guy
                mem.WriteByte(0x00);

                // no_longer_used;
                mem.WriteByte(0x00);
                mem.WriteByte(0x00);

                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void Action_DirectAccess_Refresh(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Direct Access-Refresh Screen command for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DIRECTACCESS_REFRESH_REQUEST_TO_CONTROLLER, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above
        //
        // content - unsigned short: An index for the screen to display.
        //
        static void Action_DirectAccess_ChangeScreen(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Direct Access-Change Screen command for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");

                if (param1 != null)
                {
                    var mem = new MemoryStream(0);

                    // unsigned short: An index for the screen to display.
                    LegacyUint16ToNetworkBytes((ushort)param1, mem);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DIRECTACCESS_CHANGESCREEN_TO_CONTROLLER, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        
        // Action spun up by Task.Run() above
        //
        // content - unsigned short: 0-based station number to view. For ET2000es, value may range from 0-47
        //
        static void Action_DirectAccess_GoToStation(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Direct Access-Go to Station command for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");

                if (param1 != null)
                {
                    var mem = new MemoryStream(0);

                    // unsigned short: 0-based station number to view. For ET2000es, value may range from 0-47
                    LegacyUint16ToNetworkBytes((ushort)param1, mem);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DIRECTACCESS_GOTO_STATION_TO_CONTROLLER, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void Action_DirectAccess_TurnOff(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Direct Access-Turn Off command for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DIRECTACCESS_TURN_OFF_TO_CONTROLLER , data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void Action_DirectAccess_TurnOn(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Direct Access-Turn On command for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DIRECTACCESS_TURN_ON_TO_CONTROLLER, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above
        //
        // content - unsigned char: A flag which indicates the operation to perform
        //
        static void Action_DirectAccess_MVOR(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;
            int? param2;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Direct Access-MOVR command for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");
                param2 = GetJobArgumentInt(args.jobid, "Param2");

                if (param1 != null && param2 != null)
                {
                    var mem = new MemoryStream(0);

                    // content
                    //  unsigned char: Whether to open (0), close (1) or clear (2) the master valve override
                    //  Note: Only controllers with 600 series firmware support closing (1) the master valve
                    // unsigned long: The length of time, in seconds, to open or close the master valve; ranges from 306-172800
                    mem.WriteByte((byte)param1);    // get 1st argument for this job
                    LegacyUint32ToNetworkBytes((ushort)param2, mem);
                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DIRECTACCESS_MVOR_TO_CONTROLLER, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 or 2 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above
        //
        // content - none
        //
        static void Action_DirectAccess_TestStation(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Direct Access-Test Station for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {

                param1 = GetJobArgumentInt(args.jobid, "Param1");

                if (param1 != null)
                {
                    var mem = new MemoryStream(0);
                    // content
                    // unsigned short: The station to test; ranges from 0-47
                    LegacyUint16ToNetworkBytes((ushort)param1, mem);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DIRECTACCESS_TEST_STATION_TO_CONTROLLER, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // content - none
        //
        static void Action_DirectAccess_StopIrrigation(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Direct Access-Stop Irrigation for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);

                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DIRECTACCESS_STOPKEY_TO_CONTROLLER, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }

        //
        // Action spun up by Task.Run() above
        //
        // content - 
        //
        static void Action_DirectAccess_ManualByStation(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Direct Access-Manual by Station command for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");

                if (param1 != null)
                {
                    var mem = new MemoryStream(0);

                    // content is 	
                    //  unsigned short: The station to send manual watering for; ranges from 0 to 47 or to 40 in 500 series
                    LegacyUint16ToNetworkBytes((ushort)param1, mem);

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DIRECTACCESS_MANUAL_STATION_TO_CONTROLLER, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }


        //
        // Action spun up by Task.Run() above
        //
        // content - 
        //
        static void Action_DirectAccess_ManualByProgramStation(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;
            int? param1;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Direct Access-Manual by Program command for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                param1 = GetJobArgumentInt(args.jobid, "Param1");

                if (param1 != null)
                {
                    var mem = new MemoryStream(0);

                    // content is 	
                    //  unsigned char: The program to send manual watering for;
                    mem.WriteByte((byte)param1);    // get 1st argument for this job

                    data = mem.ToArray();
                    LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DIRECTACCESS_MANUAL_PROG_TO_CONTROLLER, data, args.cid, args.jobid);
                    mem.Dispose();
                    forceToEndTxMessage = false;
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "33371", "Expects parameter 1 to be integer, null given. jobid: " + args.jobid, args.cid.ToString());
                }
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }


        //
        // Action spun up by Task.Run() above
        //
        // content - 
        //
        static void Action_DirectAccess_ManualAll(ActionArgs args)
        {
            ActiveLegacyConnection ac;
            byte[] data;
            bool forceToEndTxMessage = true;

            _log.WriteMessageWithId(NLog.LogLevel.Info, "182", "Send Direct Access-Manual to all Stations command for Legacy Controller: " + args.cid.ToString(), args.cid.ToString());

            ac = LegacyConnectionTracker.GetTrackingEntry(args.hostname, args.cityid);

            if (ac == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "987", "Unexpected error finding logical connection for controller! ", args.cid.ToString());
            }
            else
            {
                var mem = new MemoryStream(0);
                data = mem.ToArray();
                LegacyTransport.AddTxMessage(ac, args.hostname, args.adr, MID_DIRECTACCESS_MANUAL_ALL_TO_CONTROLLER, data, args.cid, args.jobid);
                mem.Dispose();
                forceToEndTxMessage = false;
            }

            if (forceToEndTxMessage)
            {
                // Call EndTxMessageProcessing to make sure we close the connection and flush the job, 
                // that's should be called only if we didn't call AddTxMessage function
                LegacyTransport.EndTxMessageProcessing(ac, null, args.jobid, Constants.JOB_ERROR_WHILE_SENDING, null, args.cid);
            }
        }
    }
}
