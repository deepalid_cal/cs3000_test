//
//
// HubInternal class 
//
// Handles the reception of TCP/IP responses from hub connected controllers.
// One instance per remote connection (IP address).
//
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using ServerApp;
using Advantage.Data.Provider;

namespace Calsense.CommServer
{
    public enum HubHandoffCode
    {
        HANDOFF_NO,
        HANDOFF_YES,
        HANDOFF_FATAL_ERROR
    }

    internal class HubInternal
    {
        public const int PACKET_DECIDE_SIZE = 12;            // number of bytes we receive to decide if 2000 or 3000
        private ActiveLegacyConnection _ac;
        private readonly Socket _socket;
        private readonly NetworkStream _networkStream;
        private CancellationTokenSource _cts;
        private MyLogger _logger;
        private int _rxBytecount;
        private StringBuilder _from;   // three bytes from
        private StringBuilder _to;     // three bytes to
        private UInt32 _this_block;
        private UInt32 _total_blocks;
        private UInt16 _seq;
        // 3000 stuff
        private UInt32 _sn;
        private UInt32 _nid;
        private UInt32 _len;
        private UInt16 _mid;
        private UInt32 _expected_size;
        private UInt32 _expected_crc;
        private UInt16 _expected_packets;
        private UInt32 _crc;
        private UInt16 _mid_dupe;
        private int _txBytecount;
        private String _ackey;

        private Int32 _hub_type;
        private UInt32 _hubid;

        private byte[] initialpartialpacket;        

        public enum ReceiveState
        {
            // common
            RX_PREAMBLE1 = 0,       // preamble bytes
            RX_PREAMBLE2 = 1,
            RX_PREAMBLE3 = 2,
            RX_PREAMBLE4 = 3,
            RX_PACKET_DECIDE,   // enought bytes to know if 2000 or 3000
            // 2000's
            RX_HEADER_OPEN,     // rest of tpl data header, mid
            RX_REMAINDER,       // minimum 0 data content of message plus crc, post amble
            RX_STATUS_REMAINDER,// crc/postamble of status
            RX_DATA,            // remaining data if any
            RX_STATUS_CHECK,    // checking crc on status
            // 3000's
            RX_BEGIN_HEADER,
            RX_FINISH_HEADER,
            RX_3000DATA,
            RX_IGNOREDATA1,
        }

        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int MaxSizeMessage(uint mid);
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int MinSizeMessage(uint mid);
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int AckOrNotMessage(uint mid);

        public bool IsDataPacketMID(int mid)
        {
            bool ret = false;

            switch (mid)
            {
                case Constants.MID_TO_COMMSERVER_REGISTRATION_DATA_data_packet:
                case Constants.MID_TO_COMMSERVER_ENGINEERING_ALERTS_data_packet:
                case Constants.MID_TO_COMMSERVER_FLOW_RECORDING_data_packet:
                case Constants.MID_TO_COMMSERVER_STATION_HISTORY_data_packet:
                case Constants.MID_TO_COMMSERVER_STATION_REPORT_DATA_data_packet:
                case Constants.MID_CODE_DISTRIBUTION_kickstart_data_packet:
                case Constants.MID_CODE_DISTRIBUTION_main_data_packet:
                case Constants.MID_CODE_DISTRIBUTION_tpmicro_data_packet:
                case Constants.MID_TO_COMMSERVER_POC_REPORT_DATA_data_packet:
                case Constants.MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_data_packet:
                case Constants.MID_TO_COMMSERVER_CHECK_FOR_UPDATES_data_packet:
                case Constants.MID_TO_COMMSERVER_WEATHER_DATA_data_packet:
                case Constants.MID_TO_COMMSERVER_CHECK_FOR_WEATHER_data_packet:
                case Constants.MID_FROM_COMMSERVER_WEATHER_DATA_packet:
                case Constants.MID_FROM_COMMSERVER_WEATHER_DATA_data_packet:
                case Constants.MID_TO_COMMSERVER_RAIN_INDICATION_data_packet:
                case Constants.MID_TO_COMMSERVER_PROGRAM_DATA_data_packet:
                case Constants.MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_data_packet:
                case Constants.MID_FROM_COMMSERVER_PROGRAM_DATA_data_packet:
                case Constants.MID_TO_COMMSERVER_VERIFY_FIRMWARE_VERSION_data_packet:
                case Constants.MID_TO_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_data_packet:
                case Constants.MID_TO_COMMSERVER_status_screen_data_packet:
                case Constants.MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_data_packet:
                case Constants.MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_data_packet:
                case Constants.MID_TO_COMMSERVER_BUDGET_REPORT_DATA_data_packet:
                case Constants.MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_data_packet:
                case Constants.MID_TO_COMMSERVER_no_more_messages_data_packet:
                case Constants.MID_TO_COMMSERVER_hub_is_busy_data_packet:
                case Constants.MID_TO_COMMSERVER_BUDGET_REPORT_DATA_data_packet2:
                    ret = true;
                    break;
                default:
                    break;
            }

            return (ret);
        }



        //
        // create a new receiver on already established socket/networkstream
        //
        public HubInternal(byte [] initialpkt, Socket socket, CancellationTokenSource cts, NetworkStream stream, uint nid, int hub_type)
        {
            int i;

            _hub_type = hub_type;
            _socket = socket;
            _cts = cts;
            _rxBytecount = initialpkt.Length;
            _networkStream = stream;
            _nid = nid;
            initialpartialpacket = new byte[ServerInternal.THRU_MID_SIZE];
            // set up the initial data that has already been received for first time thru Do()
            for(i = 0; i < initialpkt.Length && i < ServerInternal.THRU_MID_SIZE; i++)
            {
                initialpartialpacket[i] = initialpkt[i];
            }
            _logger = new MyLogger(this.GetType().Name, nid.ToString(), nid.ToString(), Constants.CS3000Model);
            _logger.WriteMessageWithId(NLog.LogLevel.Info, "3001", "Starting Hub listen task for " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), nid.ToString());
        }

        // See if we should 
        // transfer an existing connection to this hub processing class. 
        // At this point, the ServerInternal.cs logic has already been 
        // fired and received 18 bytes of packet on the open socket/stream.
        //
        // we are told the values of the first 18 bytes so that we can pick
        // up and continue receiving remainder of message, and further
        // messages that may be either 2000 or 3000 series controllers.
        // 
        //
        // pkt contains bytes received so far
        // socket contains established TCP/IP socket connected to hub
        // cts is existing cancellation token
        // stream is a NetworkStream already set up
        //
        public static HubHandoffCode HubHandoff(byte[] pkt, Socket socket, CancellationTokenSource cts, NetworkStream stream, uint nid, uint sn)
        {
            bool isahub = false;
            AdsCommand cmd;
            int hubtype = 0;
            ActiveLegacyConnection ac;

            try
            {
                ServerDB db = new ServerDB(cts);
                AdsConnection conn = db.OpenDBConnectionNotAsync("");
                if(conn != null)
                {

                    //SELECT 1 FROM CS3000Networks WHERE UseHubType IN (7,8) AND NetworkID=XX
                    //
                    //7 is LR hub
                    //8 is SR hub
                    cmd = new AdsCommand("SELECT ISNULL(UseHubType,0) FROM CS3000Networks n " + 
                                         "JOIN ControllerSites s ON s.SiteID=n.NetworkID " +
                                         "WHERE s.Deleted = false AND s.CompanyID <> -1 AND UseHubType IN (7,8) AND NetworkId=" + nid.ToString(), conn);
                    var existsID = cmd.ExecuteScalar();
                    cmd.Unprepare();
                    if (existsID != null)
                    {
                        isahub = true;
                        hubtype = Convert.ToInt32(existsID);
                    }

                    db.CloseDBConnection(conn, "");
                }
            }
            catch
            {
                return (HubHandoffCode.HANDOFF_FATAL_ERROR);
            }

            // use the NID to look in database and determine if this message is from a hub
            if (isahub)
            {
                // establish connection data structures, if we can't, don't handoff
                ac = HubConnectionTracker.EstablishConnectionTracking(nid, sn, hubtype, socket, stream);
                if(ac != null )
                {
                    var receiver = new HubInternal(pkt, socket, cts, stream, nid, hubtype);
                    receiver._ac = ac;
                    receiver._ackey = ac.key;
                    receiver._hubid = nid;  // hub id is who established the connection to commserver
                    // Create a task to handle this connection
                    Task.Factory.StartNew(receiver.Do);
                    // let caller know we have taken over this connection
                    return (HubHandoffCode.HANDOFF_YES);
                }
                else
                {
                    return (HubHandoffCode.HANDOFF_FATAL_ERROR);
                }
            }
            else
            {
                // let caller know he needs to keep handling this connection
                return (HubHandoffCode.HANDOFF_NO);
            }
        }

        private void OurSocketClose()
        {
            if (_nid != 0)
            {
                ServerSocketTracker.RemoveTrackingEntry(_nid, _networkStream);
            }
            _socket.Shutdown(SocketShutdown.Both);  // ensure all data sent/received
            _socket.Disconnect(false);
        }

        //
        // Copy of this spun up for each connection we have to legacy controller(s)
        // Note that there can be more than one controller at the end of each connection
        //
        public async void Do()
        {
            Int32 amount;
            Int32 got;
            Int32 rxmax;
            byte[] pkt = new byte[Math.Max(LegacyInternal.MAX_PACKET_SIZE,ServerInternal.MAX_PACKET_SIZE)];     // receive raw init/data/ack packets into this buffer
            Int32 offset;
            ReceiveState _state;
            LegacyClientInfoStruct client2000;
            ClientInfoStruct client3000;
            int i,j;
            bool postambleseen;
            bool crcgood;
            int timeout = 0;
            int ignore_count = 0;

            CancellationTokenSource comboCts = null;

            // 3000 specific variables
            Int32 expected_packet_number;
            byte[] buf = new byte[ServerInternal.MAX_APP_BUFFER_SIZE];  // assemble 3000 application data into this buffer
            ServerInternal.MsgLen msglen;
            Int32 app_offset;
            List<byte[]> responseList = new List<byte[]>();
            bool terminate_connection;

            bool initialize = true;


            if (System.Threading.Thread.CurrentThread.Name == null)
            {
                System.Threading.Thread.CurrentThread.Name = "HTCP Do()";
            }

            client3000.sessioninfo = null;  // for repeated passes, will get filled in by return value from client
            client3000.internalstate = 0;   // ditto
            client3000.remoteport = (uint)((IPEndPoint)(_socket.RemoteEndPoint)).Port;
            _from = new StringBuilder("");
            _to = new StringBuilder("");

            // 3000 vars
            _sn = 0;
            app_offset = 0;
            msglen.max = 0;
            msglen.min = 0;
            msglen.ack = false;

            expected_packet_number = 0;

            //
            // continue to listen for packets until someone else cancels us, or connection goes away
            //
            while (true)
            {
                // here when looking for beginning of a packet 
                if (initialize)
                {
                    // at startup, THRU_MID_SIZE bytes have already been received by ServerInternal.cs
                    // so we setup here to continue from that point
                    offset = 0;
                    amount = ServerInternal.THRU_MID_SIZE;
                    rxmax = ServerInternal.THRU_MID_SIZE;          // receive the first byte of preamble
                    Buffer.BlockCopy(initialpartialpacket, 0, pkt, 0, ServerInternal.THRU_MID_SIZE);
                    _rxBytecount = ServerInternal.THRU_MID_SIZE;
                    _state = ReceiveState.RX_BEGIN_HEADER;
                    got = ServerInternal.THRU_MID_SIZE;
                }
                else
                {
                    _state = ReceiveState.RX_PREAMBLE1;
                    offset = 0;
                    got = 0;
                    amount = 0;
                    rxmax = 1;          // receive the first byte of preamble
                }

                _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3002", "Ready to receive packet from " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());

                // Executed on a separate thread from listener
                for (; ; )
                {
                    DateTime beginstamp = DateTime.Now;

                    //
                    // we skip the receive at startup, because the handoff has already provided us with
                    // the first 18 bytes from the 3000 hub. (first message guaranteed to be from hub)
                    //
                    if (!initialize)
                    {
                        try
                        {
                            // setup our cancellation tokens if necessary
                            if(comboCts == null)
                            {
                                if (_ac.controllers3000.Exists(x => x.NID == _hubid))
                                {
                                    if (_ac.controllers3000.Find(x => x.NID == _hubid).ResetRxToken == null)
                                    {
                                        _ac.controllers3000.Find(x => x.NID == _hubid).ResetRxToken = new CancellationTokenSource();
                                    }
                                    comboCts = CancellationTokenSource.CreateLinkedTokenSource(_cts.Token, _ac.controllers3000.Find(x => x.NID == _hubid).ResetRxToken.Token);
                                }
                                else
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Error, "3099", "Hub went away from internal AC structure?");
                                }
                            }

                            // receive next chunk of data into our packet buffer
                            got = 0;
                            if(_networkStream.DataAvailable)
                            {
                                // there is something, go get it, up to the amount we can handle
                                _logger.WriteMessage(NLog.LogLevel.Debug, "3099", "Asking to receive " + (rxmax - amount).ToString() + " bytes at offset " + offset.ToString());
                                got = _networkStream.Read(pkt, offset, rxmax - amount);
                                timeout = 0;    // reset idle timeout
                            }
                            else
                            {
                                // delay
                                await Task.Delay(250, comboCts.Token);
                                timeout += 250;

                                if (timeout > (8 * 60 * 60 * 1000))
                                {
                                    _logger.WriteMessageWithId(NLog.LogLevel.Warn, "3003", "Listener timed out waiting for any RX data from " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                    return;
                                }
                                continue;
                            }


                            //Task<Int32> rx = _networkStream.ReadAsync(pkt, offset, rxmax - amount);
                            //TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
                            //_cts.Token.Register(() => tcs.TrySetCanceled(),
                            //    useSynchronizationContext: false);

                            // timeout used below in Delay() call - remote client needs to send
                            // something within this amount of time or we give up on him (90000 = 90 seconds)
                            //
                            // Timeout should be greater than all the transmit retries + receive timeouts
                            //
                            // so we don't time out quicker than transport layer above us which manages retries
                            // and since multiple controllers on one connection could all be timing out,
                            // this really needs to be grossly big, like hours long to never interfere
                            // with normal operations - just catching a hanging listener, if that could ever occur.
                            //
                            //timeout = 8 * 60 * 60;  // seconds in 8 hours

                            //timeout *= 1000;    // convert seconds to milliseconds

                            if (got > 0/*rx == await Task.WhenAny(rx, tcs.Task, Task.Delay(timeout)).ConfigureAwait(false)*/)
                            {
                                //got = await rx;         // bytes received this call
                                _logger.WriteMessage(NLog.LogLevel.Debug, "3099", "Got " + got.ToString() + " bytes at offset " + offset.ToString());
                                // since controllers could have changed (when talking to hub) update logging info just in case
                                _logger.UpdateControllerId(_ac.activecid.ToString());
                                if (got == 0)
                                {
                                    // dead socket, nothing actually received
                                    _logger.WriteMessageWithId(NLog.LogLevel.Info, "3003", "Controller closed socket. IP: " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                    // it seems like the .Connected property is not yet false at this
                                    // time, as future transmit attempts that check it still proceed.
                                    // so we will force socket closed ourselves...
                                    try
                                    {
                                        // close immediately don't linger, toss pending data
                                        LingerOption lingerOption = new LingerOption(false, 0);
                                        _socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, lingerOption);
                                        _socket.Disconnect(false);
                                    }
                                    catch
                                    {
                                        // don't care if we get an exception closing socket
                                    }
                                    return;
                                }
                                amount += got;          // total this state
                                _rxBytecount += got;    // total this client
                                if (_from.Length == 3)
                                {
                                    // update stats tracking table
                                    // TBD CalsenseControllers.BytesReceived((int)_sn, got);
                                }
                            }
                            else if (_cts.IsCancellationRequested)
                            {
                                //_logger.WriteMessageWithId(NLog.LogLevel.Info, "3002", "Request to stop listening on " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                //return;
                            }
                            else
                            {
                                //_logger.WriteMessageWithId(NLog.LogLevel.Warn, "3003", "Listener timed out waiting for any RX data from " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                //return;
                            }
                        }
                        catch (Exception e)
                        {
                            if (_cts.IsCancellationRequested)
                            {
                                _logger.WriteMessageWithId(NLog.LogLevel.Info, "3002", "Request to stop listening on " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                return;
                            }
                            if (e is TaskCanceledException)
                            {
                                if (_ac.controllers3000.Exists(x => x.NID == _hubid))
                                {
                                    if (_ac.controllers3000.Find(x => x.NID == _hubid).ResetRxToken.IsCancellationRequested)
                                    {
                                        // need to recreate tokens to be able to use them again
                                        comboCts.Dispose();
                                        comboCts = null;
                                        _ac.controllers3000.Find(x => x.NID == _hubid).ResetRxToken.Dispose();
                                        _ac.controllers3000.Find(x => x.NID == _hubid).ResetRxToken = null;
                                        _logger.WriteMessageWithId(NLog.LogLevel.Info, "3002", "Since Poll timeout, restarting receive on " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                        _state = ReceiveState.RX_PREAMBLE1;
                                        offset = 0;
                                        amount = 0;
                                        rxmax = 1;          // receive the first byte of preamble
                                        continue;
                                    }
                                }
                                else
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Error, "3099", "Hub went away from internal AC structure?");
                                }
                            }

                            if (e is SocketException)
                            {
                                var ex = (SocketException)e;
                                if (ex.SocketErrorCode == SocketError.Interrupted)
                                {
                                    _logger.WriteMessageWithId(NLog.LogLevel.Info, "3005", "Socket terminating normally for " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                    return;
                                }
                                else if (ex.SocketErrorCode == SocketError.ConnectionReset)
                                {
                                    _logger.WriteMessageWithId(NLog.LogLevel.Info, "3005", "Socket closed " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) + ", remote port " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString(), _ac.activecid.ToString());
                                    return;
                                }
                                else
                                {
                                    _logger.WriteMessageWithId(NLog.LogLevel.Error, "3005", "Unexpected Socket exception during network read " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) + " " + ex.ToString(), _ac.activecid.ToString());
                                    return;
                                }

                            }
                            else if (e is IOException)
                            {
                                if (_state == ReceiveState.RX_PREAMBLE1)
                                {
                                    _logger.WriteMessageWithId(NLog.LogLevel.Info, "3005", "Socket closed " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) + ", remote port " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString(), _ac.activecid.ToString());
                                }
                                else
                                {
                                    _logger.WriteMessageWithId(NLog.LogLevel.Warn, "3005", "Unexpected I/O exception during network read " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) + " " + e.ToString(), _ac.activecid.ToString());
                                }
                                return;
                            }
                            else
                            {
                                if (e is TaskCanceledException)
                                {
                                    _logger.WriteMessageWithId(NLog.LogLevel.Info, "3005", "Old/stale hub listener task exiting for connection from " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                }
                                else
                                {
                                    _logger.WriteMessageWithId(NLog.LogLevel.Error, "3005", "Unexpected exception type during network read " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) + " " + e.ToString(), _ac.activecid.ToString());
                                }
                                // in case socket still alive, kill it, we don't want to leave hub connected with no listener
                                try
                                {
                                    if (_socket != null && _socket.Connected)
                                    {
                                        LingerOption lingerOption = new LingerOption(false, 0);
                                        _socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, lingerOption);
                                        _socket.Disconnect(false);
                                    }
                                }
                                catch
                                {

                                }
                                return;
                            }
                        }

                        if (!_socket.Connected || amount == 0)
                        {
                            _logger.WriteMessageWithId(NLog.LogLevel.Info, "3095", "Socket terminating, not connected anymore.", _ac.activecid.ToString());
                            return;
                        }
                    }

                    initialize = false;

                    switch (_state)
                    {                           
                        case ReceiveState.RX_PREAMBLE1:
                        case ReceiveState.RX_PREAMBLE2:
                        case ReceiveState.RX_PREAMBLE3:
                        case ReceiveState.RX_PREAMBLE4:
                            // if we got here, we received one more byte
                            if (pkt[LegacyInternal.OFFSET_PRE + (int)_state] != Constants.PREAMBLE[(int)_state])
                            {
                                ignore_count++;
                                if ((ignore_count % 100) == 0 || ignore_count == 1)
                                {
                                    // chatter a little less often than every byte if we are waiting for preamble thru partial messages
                                    _logger.WriteMessageWithId(NLog.LogLevel.Warn, "3095", "While waiting for preamble have ignored " + ignore_count.ToString() + " bytes so far", _ac.activecid.ToString());
                                }
                                break;  // start over, not a good packet opening
                            }
                            offset += amount;    // keep appending
                            if (_state == ReceiveState.RX_PREAMBLE4)
                            {
                                rxmax = PACKET_DECIDE_SIZE - LegacyInternal.OFFSET_PID;  // rest of header after preamble
                                _state = ReceiveState.RX_PACKET_DECIDE;
                                amount = 0;
                            }
                            else
                            {
                                _state++;   // next preamble byte
                                amount = 0;
                            }
                            continue;

                        case ReceiveState.RX_PACKET_DECIDE:
                            offset += got;   // keep appending no matter what we decide
                            if(amount == rxmax)
                            {
                                // we have enough bytes to know if this is coming from a 2000 or 3000 type controller
                                if (pkt[LegacyInternal.OFFSET_FROMTO + 3] == 0xff && pkt[LegacyInternal.OFFSET_FROMTO + 4] == 0xff && pkt[LegacyInternal.OFFSET_FROMTO + 5] == 0xff)
                                {
                                    // 2000 type controller
                                    rxmax = LegacyInternal.THRU_HEADER_SIZE - PACKET_DECIDE_SIZE;
                                    _state = ReceiveState.RX_HEADER_OPEN;
                                    amount = 0;
                                }
                                else
                                {
                                    // 3000 type controller
                                    rxmax = ServerInternal.THRU_MID_SIZE - PACKET_DECIDE_SIZE;
                                    _state = ReceiveState.RX_BEGIN_HEADER;
                                    amount = 0;
                                }
                            }
                            continue;


                        #region 2000CONTROLLERRECEIVE

                        case ReceiveState.RX_HEADER_OPEN:
                            // see if we got all the bytes we want up to and including all of header
                            if (amount == rxmax)
                            {
                                // verify message class is 2000 to commserver
                                if (((pkt[LegacyInternal.OFFSET_PID] >> 1) & 0x0f) != LegacyTransport.MSG_CLASS_2000e_TO_CENTRAL)
                                {
                                    _logger.WriteMessageWithId(NLog.LogLevel.Warn, "3087", "Received non-2000 Message Class?! from IP: " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) + ", packet dropped", _ac.activecid.ToString());
                                    break;      // break to start new packet receive
                                }

                                // because we gotup thru mid, it is safe to pull data out
                                // from these various offsets before/including mid
                                _from.Clear();
                                _from.Append((char)pkt[LegacyInternal.OFFSET_FROMTO]);   // 3 byte from, should be controller character address
                                _from.Append((char)pkt[LegacyInternal.OFFSET_FROMTO + 1]);
                                _from.Append((char)pkt[LegacyInternal.OFFSET_FROMTO + 2]);

                                _to.Clear();
                                _to.Append((char)pkt[LegacyInternal.OFFSET_FROMTO + 3]); // 3 byte to, should be 0xff 0xff 0xff
                                _to.Append((char)pkt[LegacyInternal.OFFSET_FROMTO + 4]);
                                _to.Append((char)pkt[LegacyInternal.OFFSET_FROMTO + 5]);


                                // @wjb: I comment out this line, we don't need to update the logger ID to the controller address
                                // keep the same id "hostname"
                                // _logger.UpdateId(_from.ToString()); // now we can log with an ID = controller address

#if SWAP_PROCESS_FOR_LEGACY_CONTROLLERS
                            // Check if the controller was a part of swapping process and the process didn't complete 
                            // either because the controller was powered down or because a communication failure. 
                            // if so we need to send a factory reset command 
                            if (_to[0] != 0)
                            {
                                int s = await Misc.NeedToDoFactoryReset(_logger, _cts, _cid, _sn);

                                if (s == Constants.FACTORY_RESET_IS_REQIURED)
                                {
                                    // send a factory reset command
                                    await FactoryResetToOldControllerRequest(_logger, _cid, _sn);

                                    // Close connection
                                    OurExchangeFinish();
                                    return;
                                }
                                else if (s == Constants.FACTORY_RESET_ERROR_WHILE_CHECKING)
                                {
                                    // some error occured, close connection
                                    OurExchangeFinish();
                                    return;
                                }
                            }
#endif

                                // note contact in our stats tracking table
                                // TBD CalsenseControllers.Contacted((int)_sn, ((IPEndPoint)_socket.RemoteEndPoint).Address);
                                // update Last Communicated field in database
                                // Misc.LegacyLastCommunicated(_logger, _cts, _ac.activecid);

                                // check that from address is who we think we are talking to
                                if (_from.ToString() != _ac.activeadr.ToString())
                                {
                                    // some kind of problem, address changing or something...
                                    _logger.WriteMessageWithId(NLog.LogLevel.Warn, "3099", "Mismatch on controller address, expected " + _ac.activeadr.ToString() + " got " + _from.ToString() + ", packet dropped", _ac.activecid.ToString());
                                    break;      // break to start new packet receive
                                }

                                _seq = pkt[LegacyInternal.OFFSET_SEQ];
                                _seq <<= 8;
                                _seq |= pkt[LegacyInternal.OFFSET_SEQ + 1];
                                // legacy reverses bytes, so can't do
                                // _seq = Misc.NetworkBytesToUint16(pkt, OFFSET_SEQ);

                                _total_blocks = pkt[LegacyInternal.OFFSET_TOTAL_BLOCKS];   // for a status packet this byte is not used
                                _this_block = pkt[LegacyInternal.OFFSET_THIS_BLOCK];       // for a status packet this byte is the AcksLength


                                if ((pkt[LegacyInternal.OFFSET_PID] & LegacyTransport.LEGACY_STATUS_BIT) != 0)
                                {
                                    // this is a status packet... there will be no MID, only possible application data is AcksLength bytes
                                    rxmax = LegacyInternal.STATUS_PKT_SIZE - LegacyInternal.THRU_HEADER_SIZE;   // typically 8 more bytes (CRC and postamble)
                                    rxmax += (int)_this_block;                         // AcksLength tells us how many bytes of retry bits set
                                    offset += got;    // keep appending 
                                    _state = ReceiveState.RX_STATUS_REMAINDER;
                                    amount = 0;
                                    _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3021", "Status Pkt from " + _from.ToString() + " still to go " + rxmax.ToString(), _ac.activecid.ToString());
                                }
                                else
                                {
                                    // not a status packet
                                    // how much to receive in remainder of header
                                    //
                                    if (_total_blocks != _this_block)
                                    {
                                        // non-last packet - will be full 520 bytes
                                        rxmax = (LegacyInternal.MAX_APPDATA_IN_PACKET + LegacyInternal.DATA_PACKET_OVERHEAD) - LegacyInternal.THRU_HEADER_SIZE;
                                    }
                                    else
                                    {
                                        // last packet, could have any amount of application data... at a minimum
                                        // there is a CID, CRC, and postamble
                                        rxmax = LegacyInternal.DATA_PACKET_OVERHEAD - LegacyInternal.THRU_HEADER_SIZE;
                                    }
                                    offset = LegacyInternal.THRU_HEADER_SIZE;
                                    _state = ReceiveState.RX_REMAINDER;
                                    amount = 0;
                                }
                            }
                            else
                            {
                                _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3021", "Did not get full packet yet, instead got so far:" + amount.ToString(), _ac.activecid.ToString());
                                offset = LegacyInternal.OFFSET_PID + amount;
                            }
                            continue;   // always keep trying while working on header...

                        case ReceiveState.RX_STATUS_REMAINDER:
                        case ReceiveState.RX_REMAINDER:
                            // see if we got all we are asking for
                            if (amount == rxmax)
                            {
                                //
                                // at this point we may have a full packet...
                                // for a status packet - this should be it as we set rxmax in previous state to exact amount we need
                                // for a non-last data packet - it should be full up
                                //
                                postambleseen = true;
                                crcgood = false;
                                if (_state == ReceiveState.RX_STATUS_REMAINDER || _this_block != _total_blocks)
                                {
                                    // see if post amble correct
                                    // see if CRC correct
                                    for (i = 0; i < Constants.POSTAMBLE.Length; i++)
                                    {
                                        if (pkt[i + got + offset - Constants.POSTAMBLE.Length] != Constants.POSTAMBLE[i])
                                        {
                                            postambleseen = false;  // nope, no end of message yet
                                        }
                                    }
                                    if (postambleseen == true)
                                    {
                                        // data, offset, length
                                        if (Misc.OrderedCRC(pkt, LegacyInternal.OFFSET_PID, offset + got - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))) == Misc.NetworkBytesToUint32(pkt, got + offset - (Constants.POSTAMBLE.Length + sizeof(UInt32))))   // CRC is right before post amble
                                        {
                                            crcgood = true;
                                        }
                                    }
                                }
                                else
                                {
                                    // since we don't know how many application bytes might be present, we have to look
                                    // for postamble and good CRC byte by byte on the last data packet
                                    for (i = 0; i < Constants.POSTAMBLE.Length; i++)
                                    {
                                        if (pkt[i + got + offset - Constants.POSTAMBLE.Length] != Constants.POSTAMBLE[i])
                                        {
                                            postambleseen = false;  // nope, no end of message yet
                                        }
                                    }
                                    if (postambleseen == true)
                                    {
                                        // check for good CRC
                                        // data, offset, length
                                        if (Misc.OrderedCRC(pkt, LegacyInternal.OFFSET_PID, offset + got - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))) == Misc.NetworkBytesToUint32(pkt, got + offset - (Constants.POSTAMBLE.Length + sizeof(UInt32))))   // CRC is right before post amble
                                        {
                                            crcgood = true;
                                        }
                                    }

                                    // if there is still room, and haven't seen end of packet yet, receive another byte
                                    if (!crcgood && ((offset + got) < (LegacyInternal.MAX_APPDATA_IN_PACKET + LegacyInternal.DATA_PACKET_OVERHEAD)))
                                    {
                                        offset += got;
                                        rxmax = 1;
                                        amount = 0;
                                        continue;
                                    }
                                }

                                if (!crcgood)
                                {
                                    // messed up packet.
                                    _logger.WriteMessageWithId(NLog.LogLevel.Warn, "3077", "Bad CRC/postamble/length, throwing away packet from " + _from.ToString() + " at IP: " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                    // break to start a new packet receive
                                    break;
                                }
                                else
                                {
                                    ignore_count = 0;   // reset ignore count for log output
                                    // we have a valid packet.
                                    // good packet (crc, postamble)
                                    // send to transport
                                    _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3099", "Well framed packet " + _this_block.ToString() + " of " + _total_blocks.ToString() + " received from: " + _from.ToString(), _ac.activecid.ToString());
                                    // pkt has byte array
                                    client2000.rx = pkt;
                                    client2000.rxlen = (offset + got); // total bytes received
                                    client2000.sequence = _seq;
                                    client2000.adr = _from;
                                    LegacyTransport.RxPacketReceived(_ac, client2000);
                                }
                            }
                            else
                            {
                                _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3021", "Did not get full packet yet, instead got so far:" + (LegacyInternal.THRU_HEADER_SIZE + amount).ToString(), _ac.activecid.ToString());
                                offset = LegacyInternal.THRU_HEADER_SIZE + amount; // keep appending after the header we already received 
                                continue;
                            }
                            // hitting this starts a new packet receive
                            break;

                        #endregion      // 2000
                        #region 3000CONTROLLERRECEIVE
                        case ReceiveState.RX_BEGIN_HEADER:
                            // see if we got all the bytes we want up to and including MID
                            if (amount == rxmax)
                            {
                                // preamble known good by common receive logic in initial state

                                // because we got a full packet worth of data, it is safe to pull data out
                                // from these various offsets
                                _sn = Misc.NetworkBytesToUint32(pkt, ServerInternal.OFFSET_SN);
                                client3000.sn = _sn;    // go ahead and set this, so available for logging
                                _nid = Misc.NetworkBytesToUint32(pkt, ServerInternal.OFFSET_NID);
                                _logger.UpdateId(_nid.ToString()); // now we can log with an ID = network ID
                                _logger.UpdateControllerId(_nid.ToString()); // now we can log into databse table with an ID = network ID

                                _len = Misc.NetworkBytesToUint32(pkt, ServerInternal.OFFSET_LEN);
                                _mid = Misc.NetworkBytesToUint16(pkt, ServerInternal.OFFSET_MID);

                                // Check if the controller was a part of swapping process and the process didn't complete 
                                // either because the controller was powered down or because a communication failure. 
                                // if so we need to send a factory reset command 
                                if (_nid != 0)
                                {
                                    int s = await Misc.NeedToDoFactoryReset(_logger, _cts, _nid, _sn);

                                    if (s == Constants.FACTORY_RESET_IS_REQIURED)
                                    {
                                        // send a factory reset command
                                        await FactoryResetToOldControllerRequest(_logger, _nid, _sn);

                                        // Close connection
                                        OurSocketClose();
                                        return;
                                    }
                                    else if (s == Constants.FACTORY_RESET_ERROR_WHILE_CHECKING)
                                    {
                                        _state = ReceiveState.RX_PREAMBLE1;
                                        offset = 0;
                                        got = 0;
                                        amount = 0;
                                        rxmax = 1;          // receive the first byte of preamble
                                        break;  // break to start new packet
                                    }
                                }

                                // note contact in our stats tracking table
                                CalsenseControllers.Contacted((int)_sn, ((IPEndPoint)_socket.RemoteEndPoint).Address);
                                // update Last Communicated field in database
                                await Misc.LastCommunicated(_logger, _cts, _nid);

                                // check for error condition of bad nid on hub
                                if (_nid != 0)
                                {
                                    // look up the new nid in socket tracking table
                                    if (HubConnectionTracker.DoubleCheckNid(_ackey, _nid, _sn, _networkStream, _logger) != 0)
                                    {
                                        // some kind of problem, NID not one we think is on this hub
                                        _logger.WriteMessage(NLog.LogLevel.Warn, "3099", "Message from NID not in database for this hub?" + _nid.ToString());
                                        _state = ReceiveState.RX_PREAMBLE1;
                                        offset = 0;
                                        got = 0;
                                        amount = 0;
                                        rxmax = 1;          // receive the first byte of preamble
                                        break;  // break to start new packet
                                    }
                                }

                                // if we are out of sequence, this could be a data packet structure, check for that now...
                                if(IsDataPacketMID(_mid))
                                {
                                    _state = ReceiveState.RX_IGNOREDATA1;
                                    offset += got;
                                    got = 0;
                                    rxmax =   ServerInternal.INIT_SIZE - ServerInternal.THRU_MID_SIZE;  // remainder of init packet header, but this is a data packet instead
                                    rxmax -= 8;     // so subtract off the extra init bytes
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3023", "Out of sequence data packet coming in. MID: " + _mid.ToString());
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "3023", "Adjusting length for  " + (_len - 18).ToString() + " bytes of data");
                                    rxmax += (int)(_len-18);  // and add in reported data bytes
                                    amount = 0;
                                    continue;
                                }

                                // see how many application bytes could be possible for this message type
                                if (ServerInternal.MessageLimits.TryGetValue(_mid, out msglen))
                                {
                                    // Key was in dictionary, msglen set to range we could receive max, min
                                }
                                else
                                {
                                    msglen.max = MaxSizeMessage(_mid);
                                    // override max - allow 1 megabyte for all messages with data now
                                    if (msglen.max > 0) msglen.max = 1024 * 1024;
                                    msglen.min = MinSizeMessage(_mid);
                                    // check for a message we don't know about
                                    if (msglen.max == -1 || msglen.min == -1)
                                    {
                                        _logger.WriteMessage(NLog.LogLevel.Error, "3023", "Unknown MID received " + _mid.ToString());
                                        _state = ReceiveState.RX_PREAMBLE1;
                                        offset = 0;
                                        got = 0;
                                        amount = 0;
                                        rxmax = 1;          // receive the first byte of preamble
                                        break;  // break to start new packet
                                    }
                                    if (msglen.max > ServerInternal.MAX_APP_BUFFER_SIZE)
                                    {
                                        _logger.WriteMessage(NLog.LogLevel.Error, "3043", "Internal sizing error, msg maximum " + msglen.max.ToString() + " too big for buffer of " + ServerInternal.MAX_APP_BUFFER_SIZE);
                                        _state = ReceiveState.RX_PREAMBLE1;
                                        offset = 0;
                                        got = 0;
                                        amount = 0;
                                        rxmax = 1;          // receive the first byte of preamble
                                        break;  // break to start new packet
                                    }
                                    // see if this is an ACK structure message or not...
                                    msglen.ack = AckOrNotMessage(_mid) == 0 ? false : true;
                                    // add lookup to dictionary for later use to avoid trip in C DLL
                                    ServerInternal.MessageLimits.Add(_mid, msglen);
                                }

                                // for ACK messages we receive remaining data (rare, only mobile on/off at this time), CRC and postamble.
                                // for all other messages we receive that plus Init struct
                                if (msglen.ack)
                                {
                                    // how much to receive in remainder of header - note for some special case ACKs there is a bit of data included (e.g. mobile ON/OFF)
                                    rxmax = ServerInternal.ACK_SIZE - ServerInternal.THRU_MID_SIZE + msglen.min;
                                }
                                else
                                {
                                    // how much to receive in remainder of header
                                    rxmax = ServerInternal.INIT_SIZE - ServerInternal.THRU_MID_SIZE;
                                }
                                offset += got;    // keep appending 
                                _state = ReceiveState.RX_FINISH_HEADER;
                                amount = 0;
                            }
                            else
                            {
                                _logger.WriteMessage(NLog.LogLevel.Debug, "3021", "Did not get full INIT packet yet, instead got so far:" + amount.ToString());
                                offset += got;   // keep appending
                            }
                            continue;   // always keep trying while working on header...

                        case ReceiveState.RX_IGNOREDATA1:
                            if(amount == rxmax)
                            {
                                // reset poll timer and timeout if active
                                if (_ac.controllers3000 != null && _ac.controllers3000.Count > 0)
                                {
                                    if (_ac.controllers3000.Exists(x => x.NID == _nid))
                                    {
                                        if (_ac.controllers3000.Find(x => x.NID == _nid).pollstate > 0)
                                        {
                                            _ac.controllers3000.Find(x => x.NID == _nid).pollstate = 1;
                                        }
                                    }
                                    if (_ac.pollTimeout > 0 && _ac.pollTimeout < HubConnectionTracker.GetPollTimeout()) // never shorten, only extend
                                    {
                                        _ac.pollTimeout = HubConnectionTracker.GetPollTimeout();   // ticks down from here
                                        _logger.WriteMessage(NLog.LogLevel.Info, "3044", "Restarting 3000 Poll timeout since ignored data packet.");
                                    }
                                }
                                _state = ReceiveState.RX_PREAMBLE1;
                                offset = 0;
                                got = 0;
                                amount = 0;
                                rxmax = 1;          // receive the first byte of preamble
                                break;  // break to start new packet
                            }
                            else
                            {
                                _logger.WriteMessage(NLog.LogLevel.Debug, "3021", "Did not get full Data being ignored packet yet, instead got so far:" + amount.ToString());
                                offset += got; // keep appending
                            }
                            continue;   // always keep trying while working on header, no parsing until body

                        case ReceiveState.RX_FINISH_HEADER:
                            // see if we got a full packet...
                            if (amount == rxmax)
                            {
                                // check post amble, pre amble already good
                                var err = false;
                                for (i = 0; i < Constants.POSTAMBLE.Length; i++)
                                {
                                    if (pkt[(msglen.ack ? ServerInternal.ACK_SIZE + msglen.min : ServerInternal.INIT_SIZE) + i - Constants.POSTAMBLE.Length] != Constants.POSTAMBLE[i])
                                    {
                                        _logger.WriteMessage(NLog.LogLevel.Warn, "3087", "Expected Postamble from controller, but got:" +
                                            pkt[(msglen.ack ? ServerInternal.ACK_SIZE + msglen.min : ServerInternal.INIT_SIZE) + 0 - Constants.POSTAMBLE.Length].ToString() + " " +
                                            pkt[(msglen.ack ? ServerInternal.ACK_SIZE + msglen.min : ServerInternal.INIT_SIZE) + 1 - Constants.POSTAMBLE.Length].ToString() + " " +
                                            pkt[(msglen.ack ? ServerInternal.ACK_SIZE + msglen.min : ServerInternal.INIT_SIZE) + 2 - Constants.POSTAMBLE.Length].ToString() + " " +
                                            pkt[(msglen.ack ? ServerInternal.ACK_SIZE + msglen.min : ServerInternal.INIT_SIZE) + 3 - Constants.POSTAMBLE.Length].ToString());
                                        err = true;

                                    }
                                }
                                if(err)
                                {
                                    _state = ReceiveState.RX_PREAMBLE1;
                                    offset = 0;
                                    got = 0;
                                    amount = 0;
                                    rxmax = 1;          // receive the first byte of preamble
                                    break;  // break to start new packet
                                }

                                // for everything except ACKs process the MSG_INITIALIZATION_STRUCT stuff
                                if (!msglen.ack)
                                {
                                    _expected_size = Misc.NetworkBytesToUint32(pkt, ServerInternal.INIT_OFFSET_EXP_SIZE);
                                    _expected_crc = Misc.NetworkBytesToUint32(pkt, ServerInternal.INIT_OFFSET_EXP_CRC);
                                    _expected_packets = Misc.NetworkBytesToUint16(pkt, ServerInternal.INIT_OFFSET_EXP_PKTS);
                                    _mid_dupe = Misc.NetworkBytesToUint16(pkt, ServerInternal.INIT_OFFSET_MID_DUP);
                                    _crc = Misc.NetworkBytesToUint32(pkt, ServerInternal.INIT_OFFSET_CRC);

                                    // sanity check that both MIDs received in init packet are the same
                                    if (_mid_dupe != _mid)
                                    {
                                        _logger.WriteMessage(NLog.LogLevel.Warn, "3087", "Expected MIDs to match in Init packet but got 2 different: " + _mid.ToString() + " " + _mid_dupe.ToString());
                                        _state = ReceiveState.RX_PREAMBLE1;
                                        offset = 0;
                                        got = 0;
                                        amount = 0;
                                        rxmax = 1;          // receive the first byte of preamble
                                        break;  // break to start new packet
                                    }


                                    // check CRC - it is calc'd across packet less pre/postambles and 4-byte CRC itself
                                    if (Misc.OrderedCRC(pkt, ServerInternal.OFFSET_SN, ServerInternal.INIT_SIZE - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))) != _crc)
                                    {
                                        _logger.WriteMessage(NLog.LogLevel.Warn, "3044", "CRC mismatch on init packet, calculated: " + Misc.OrderedCRC(pkt, ServerInternal.OFFSET_SN, ServerInternal.INIT_SIZE - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))).ToString("X") + " recv'd: " + _crc.ToString("X"));
                                        _state = ReceiveState.RX_PREAMBLE1;
                                        offset = 0;
                                        got = 0;
                                        amount = 0;
                                        rxmax = 1;          // receive the first byte of preamble
                                        break;  // break to start new packet
                                    }

                                    _logger.WriteMessage(NLog.LogLevel.Debug, "3020", "Rx'd full init packet: SN:" + _sn.ToString() +
                                        " NID:" + _nid.ToString() + " Len:" + _len.ToString() + " MID:" + _mid.ToString() +
                                        " Exp Size:" + _expected_size.ToString() + " Exp CRC: 0x" + _expected_crc.ToString("X") +
                                        " Exp Packets:" + _expected_packets.ToString() + " CRC: 0x" + _crc.ToString("X"));
                                }
                                // ACKs have no MSG_INITIALIZATION_STRUCT, so just skip to the CRC
                                else
                                {

                                    _crc = Misc.NetworkBytesToUint32(pkt, msglen.min + ServerInternal.INIT_OFFSET_CRC - ServerInternal.MSG_INITIALIZATION_STRUCT_SIZE);
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "3020", "Rx'd ACK packet: SN:" + _sn.ToString() +
                                        " NID:" + _nid.ToString() + " Len:" + _len.ToString() + " MID:" + _mid.ToString() +
                                        " CRC: 0x" + _crc.ToString("X"));
                                    if (Misc.OrderedCRC(pkt, ServerInternal.OFFSET_SN, msglen.min + ServerInternal.ACK_SIZE - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))) != _crc)
                                    {
                                        _logger.WriteMessage(NLog.LogLevel.Warn, "3044", "CRC mismatch on ack packet, calculated: " + Misc.OrderedCRC(pkt, ServerInternal.OFFSET_SN, msglen.min + ServerInternal.ACK_SIZE - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))).ToString("X") + " recv'd: " + _crc.ToString("X"));
                                        _state = ReceiveState.RX_PREAMBLE1;
                                        offset = 0;
                                        got = 0;
                                        amount = 0;
                                        rxmax = 1;          // receive the first byte of preamble
                                        break;  // break to start new packet
                                    }
                                }
                                
                                ignore_count = 0;   // reset ignore count for log output

                                // reset poll timer and timeout if active
                                if (_ac.controllers3000 != null && _ac.controllers3000.Count > 0)
                                {
                                    if (_ac.controllers3000.Exists(x => x.NID == _nid))
                                    {
                                        if (_ac.controllers3000.Find(x => x.NID == _nid).pollstate > 0)
                                        {
                                            _ac.controllers3000.Find(x => x.NID == _nid).pollstate = 1;
                                        }
                                    }
                                    if (_ac.pollTimeout > 0 && _ac.pollTimeout < HubConnectionTracker.GetPollTimeout()) // never shorten, only extend
                                    {
                                        _ac.pollTimeout = HubConnectionTracker.GetPollTimeout();   // ticks down from here
                                        _logger.WriteMessage(NLog.LogLevel.Debug, "3044", "Restarting 3000 Poll timeout since good packet.");
                                    }
                                }

                                // for ACK messages - we are done, move on to parsing
                                if (msglen.ack)
                                {
                                    // indicate any size of data contained in the ACK (normally 0, but Mobile ON/OFF has a job number here)
                                    app_offset = msglen.min;
                                    // if there is data in the ACK, move it into the final buffer that holds data for Parse routine
                                    for (i = 0; i < app_offset; i++)
                                    {
                                        buf[i] = pkt[i + ServerInternal.INIT_OFFSET_CRC - ServerInternal.MSG_INITIALIZATION_STRUCT_SIZE]; // any ACK data is where CRC would normally start in an ACK
                                    }
                                    break;
                                }

                                // safety check length we have been told makes sense
                                // against what we know of message
                                if (_expected_size > msglen.max)
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Error, "3023", "Received expected size of " + _expected_size.ToString() + " expecting no more than " + msglen.max.ToString());
                                    _state = ReceiveState.RX_PREAMBLE1;
                                    offset = 0;
                                    got = 0;
                                    amount = 0;
                                    rxmax = 1;          // receive the first byte of preamble
                                    break;  // break to start new packet
                                }
                                if (_expected_size < msglen.min)
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Error, "3023", "Received expected size of " + _len.ToString() + " expecting at least " + msglen.min.ToString());
                                    _state = ReceiveState.RX_PREAMBLE1;
                                    offset = 0;
                                    got = 0;
                                    amount = 0;
                                    rxmax = 1;          // receive the first byte of preamble
                                    break;  // break to start new packet
                                }


                                // now start receiving application data, MAX_PACKET_SIZE is most in one packet

                                _state = ReceiveState.RX_3000DATA;
                                offset = 0;
                                app_offset = 0;
                                expected_packet_number = 1;
                                // how much to receive, one data packet worth
                                rxmax = (_expected_size > ServerInternal.MAX_DATA_IN_PACKET) ? ServerInternal.MAX_PACKET_SIZE : (Int32)((_expected_size + ServerInternal.PACKET_OVERHEAD));
                                amount = 0;
                            }
                            else
                            {
                                _logger.WriteMessage(NLog.LogLevel.Debug, "3021", "Did not get full INIT packet yet, instead got so far:" + amount.ToString());
                                offset += got; // keep appending
                            }
                            continue;   // always keep trying while working on header, no parsing until body

                        case ReceiveState.RX_3000DATA:
                            if (amount == rxmax)
                            {
                                // reset poll timer and timeout if active
                                if (_ac.controllers3000 != null && _ac.controllers3000.Count > 0)
                                {
                                    if (_ac.controllers3000.Exists(x => x.NID == _nid))
                                    {
                                        if (_ac.controllers3000.Find(x => x.NID == _nid).pollstate > 0)
                                        {
                                            _ac.controllers3000.Find(x => x.NID == _nid).pollstate = 1;
                                        }
                                    }
                                    if (_ac.pollTimeout > 0 && _ac.pollTimeout < HubConnectionTracker.GetPollTimeout()) // never shorten, only extend
                                    {
                                        _ac.pollTimeout = HubConnectionTracker.GetPollTimeout();   // ticks down from here
                                        _logger.WriteMessage(NLog.LogLevel.Debug, "3044", "Restarting 3000 Poll timeout since data packet.");
                                    }
                                }

                                var err = false;
                                // we have a full data packet ... verify and pull the application data out into our buffer
                                // check pre and post amble
                                for (i = 0; i < Constants.PREAMBLE.Length; i++)
                                {
                                    if (pkt[ServerInternal.OFFSET_PRE + i] != Constants.PREAMBLE[i])
                                    {
                                        _logger.WriteMessage(NLog.LogLevel.Warn, "3097", "Expected Preamble from controller, but got:" + pkt[0 + ServerInternal.OFFSET_PRE].ToString() + " " + pkt[1 + ServerInternal.OFFSET_PRE].ToString() + " " + pkt[2 + ServerInternal.OFFSET_PRE].ToString() + " " + pkt[3 + ServerInternal.OFFSET_PRE].ToString());
                                        err = true;

                                    }
                                }
                                if(err)
                                {
                                    _state = ReceiveState.RX_PREAMBLE1;
                                    offset = 0;
                                    got = 0;
                                    amount = 0;
                                    rxmax = 1;          // receive the first byte of preamble
                                    break;  // break to start new packet
                                }

                                for (i = 0; i < Constants.POSTAMBLE.Length; i++)
                                {
                                    if (pkt[i + amount - Constants.POSTAMBLE.Length] != Constants.POSTAMBLE[i])
                                    {
                                        _logger.WriteMessage(NLog.LogLevel.Warn, "3097", "Expected Postamble from controller, but got:" + pkt[0 + amount - Constants.POSTAMBLE.Length].ToString() + " " + pkt[1 + amount - Constants.POSTAMBLE.Length].ToString() + " " + pkt[2 + amount - Constants.POSTAMBLE.Length].ToString() + " " + pkt[3 + amount - Constants.POSTAMBLE.Length].ToString());
                                        err = true;
                                    }
                                }
                                if (err)
                                {
                                    _state = ReceiveState.RX_PREAMBLE1;
                                    offset = 0;
                                    got = 0;
                                    amount = 0;
                                    rxmax = 1;          // receive the first byte of preamble
                                    break;  // break to start new packet
                                }

                                // sanity check for matching header values
                                if (_sn != Misc.NetworkBytesToUint32(pkt, ServerInternal.OFFSET_SN))
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3098", "Got mismatched SN in Data Pkt vs. Init Pkt.");
                                    _state = ReceiveState.RX_PREAMBLE1;
                                    offset = 0;
                                    got = 0;
                                    amount = 0;
                                    rxmax = 1;          // receive the first byte of preamble
                                    break;  // break to start new packet
                                }
                                if (_nid != Misc.NetworkBytesToUint32(pkt, ServerInternal.OFFSET_NID))
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3098", "Got mismatched Network ID in Data Pkt vs. Init Pkt.");
                                    _state = ReceiveState.RX_PREAMBLE1;
                                    offset = 0;
                                    got = 0;
                                    amount = 0;
                                    rxmax = 1;          // receive the first byte of preamble
                                    break;  // break to start new packet
                                }
                                if (expected_packet_number != Misc.NetworkBytesToUint16(pkt, ServerInternal.DATA_OFFSET_PKT_NUM))
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3099", "Got out of sequence Packet Number in Data Pkt. Got " + Misc.NetworkBytesToUint16(pkt, ServerInternal.DATA_OFFSET_PKT_NUM).ToString() + " expected " + expected_packet_number.ToString());
                                    _state = ReceiveState.RX_PREAMBLE1;
                                    offset = 0;
                                    got = 0;
                                    amount = 0;
                                    rxmax = 1;          // receive the first byte of preamble
                                    break;  // break to start new packet
                                }
                                if (Misc.OrderedCRC(pkt, ServerInternal.OFFSET_SN, amount - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))) != Misc.NetworkBytesToUint32(pkt, amount - (Constants.POSTAMBLE.Length + sizeof(UInt32))))   // CRC is right before post amble
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Warn, "3044", "CRC mismatch on data packet, calculated: " + Misc.OrderedCRC(pkt, ServerInternal.OFFSET_SN, amount - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))).ToString("X") + " recv'd: " + Misc.NetworkBytesToUint32(pkt, amount - (Constants.POSTAMBLE.Length + sizeof(UInt32))).ToString("X"));
                                    _state = ReceiveState.RX_PREAMBLE1;
                                    offset = 0;
                                    got = 0;
                                    amount = 0;
                                    rxmax = 1;          // receive the first byte of preamble
                                    break;  // break to start new packet
                                }

                                ignore_count = 0;   // reset ignore count for log output

                                // append application data into the big buffer
                                Array.Copy(pkt, ServerInternal.DATA_OFFSET_DATA, buf, app_offset, amount - ServerInternal.PACKET_OVERHEAD);
                                app_offset += (amount - ServerInternal.PACKET_OVERHEAD);

                                if (app_offset >= _expected_size)
                                {
                                    // we have copied into buf all the application data that was expected...move on to parsing that
                                    // will hit break for this case
                                }
                                else
                                {
                                    // receive next data packet
                                    offset = 0;
                                    rxmax = ((_expected_size - app_offset) > ServerInternal.MAX_DATA_IN_PACKET) ? ServerInternal.MAX_PACKET_SIZE : (Int32)(((_expected_size - app_offset) + ServerInternal.PACKET_OVERHEAD));
                                    amount = 0;
                                    expected_packet_number++;
                                    continue;
                                }
                            }
                            else
                            {
                                _logger.WriteMessage(NLog.LogLevel.Debug, "3027", "Received data, more expected in this DATA packet...receiving again...so far:" + amount.ToString());
                                offset = amount;
                                continue;   // more to receive
                            }
                            break;

                        #endregion

                    }

                    _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3020", "Total Rx'd so far on socket is " + _rxBytecount.ToString(), _ac.activecid.ToString());

                    if (_state == ReceiveState.RX_FINISH_HEADER || _state == ReceiveState.RX_3000DATA)
                    {
                        _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3020", "3000 Hub message ACK/Data to parse...", _nid.ToString());

                        client3000.ip = ((IPEndPoint)(_socket.RemoteEndPoint)).Address;
                        client3000.sn = _sn;
                        client3000.nid = _nid;
                        client3000.rx = buf;
                        client3000.rxlen = (UInt32)app_offset;
                        client3000.mid = _mid;
                        if (responseList != null)
                        {
                            responseList.Clear();
                        }
                        terminate_connection = true;    // assume we will drop connection

                        if (_sn != 0)
                        {
                            // update stats tracking table
                            CalsenseControllers.MsgsReceived((int)_sn, 1);
                        }

                        //
                        // call the appropriate parsing function based on received mid
                        //
                        if (ServerInternal.ParseFunctions.ContainsKey(_mid))
                        {
                            int originalTimeout;

                            beginstamp = DateTime.Now;
                            
                            // since the DB updates in the parse routine could take a long time using Advantage DB (we're talking many minutes!)
                            // we will artifically extend the poll timeout if active while we wait for this function to complete...
                            // we will save the original value and restore it after Parse returns.
                            // reset poll timer and timeout if active
                            originalTimeout = -1;
                            if (_ac.controllers3000 != null && _ac.controllers3000.Count > 0)
                            {
                                if (_ac.pollTimeout > 0) 
                                {
                                    originalTimeout = _ac.pollTimeout;
                                    _ac.pollTimeout = 30*60;   // 30 minutes
                                    _logger.WriteMessage(NLog.LogLevel.Info, "3044", "Pausing 3000 Poll timeout during Database parse/store...");
                                }
                            }

                            Task<Tuple<List<byte[]>, String, bool, int>> tx = ServerInternal.ParseFunctions[_mid](client3000, _cts);
                            Tuple<List<byte[]>, String, bool, int> result;
                            result = await tx;
                            responseList = result.Item1;
                            terminate_connection = result.Item3;
                            // if Parser routine is "long-lived" i.e. multi-pass messaging, store
                            // any state/session data it has.
                            if (result.Item4 != Constants.NO_STATE_DATA)
                            {
                                client3000.sessioninfo = result.Item2;
                                client3000.internalstate = result.Item4;
                            }
                            // if Parser routine is done with it's state information, re-initialize it
                            // so okay for next message sequence that might look at it.
                            if (result.Item4 == Constants.LAST_STATE_COMPLETE)
                            {
                                client3000.sessioninfo = null;
                                client3000.internalstate = 0;
                            }

                            if(originalTimeout != -1 && _ac.pollTimeout > 0)
                            {
                                // in a case of nested messages due to controller retry, don't restore to a big value
                                _ac.pollTimeout = (originalTimeout > HubConnectionTracker.HUB_POLL_TIMEOUT) ? HubConnectionTracker.HUB_POLL_TIMEOUT : originalTimeout;
                                _logger.WriteMessage(NLog.LogLevel.Debug, "3044", "Restoring 3000 Poll after Database parse/store.");
                            }
                        }
                        else
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "3030", "No parsing routine available for MID " + _mid);
                        }

                        // if response generated for controller, send it
                        if (responseList != null)
                        {
#if FLUSHPOSSIBLEGARBAGEDATA
                    //
                    // flush any extra incoming data...
                    // i have seen extra garbage data on the end of some controller requests. this clears that out of the way
                    // 
                    while (_networkStream.DataAvailable)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "3031", "Unexpected data present from controller:");
                        await _networkStream.ReadAsync(buf, 0, 8);
                        Misc.LogMsg(_logger, buf, 8);
                    }
#endif

                            int totalmsglen = 0;
                            i = 0;
                            j = 0;
                            while (i < responseList.Count)
                            {
                                if (responseList[i].Length > 0)
                                {
                                    totalmsglen += responseList[i].Length + ServerApp.Constants.PREAMBLE.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32);
                                    j++;
                                }
                                i++;
                            }
                            if (totalmsglen > ServerInternal.MAX_DATA_IN_PACKET)
                            {
                                // more than one packet, lets extend the poll timer by 15 seconds per packet
                                int extendtime = HubConnectionTracker.GetPollTimeout() + (15 * ((totalmsglen) / ServerInternal.MAX_DATA_IN_PACKET));
                                if (extendtime > 200) extendtime = 200; // cap at 200
                                _logger.WriteMessage(NLog.LogLevel.Info, "3040", "Since multiple packet message for hub, extending poll timeout to " + extendtime.ToString() + " secs.");
                                _ac.pollTimeout = extendtime;   // ticks down from here
                            }

                            i = 0;
                            j = 0;
                            totalmsglen = 0;
                            while (i < responseList.Count)
                            {
                                if (responseList[i].Length > 0)
                                {
                                    UInt32 crc;
                                    crc = Misc.CRC(responseList[i]);
                                    //_logger.WriteMessage(NLog.LogLevel.Debug, "3040", "Calculated CRC: 0x" + crc.ToString("X"));
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "3028", "Response to be transmitted to SN:" + client3000.sn.ToString());

                                    // combine preamble, data, CRC and then postamble
                                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + responseList[i].Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                                    System.Buffer.BlockCopy(responseList[i], 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, responseList[i].Length);

                                    byte[] crcbytes;
                                    crcbytes = System.BitConverter.GetBytes(crc);
                                    if (BitConverter.IsLittleEndian)
                                    {
                                        //_logger.WriteMessage(NLog.LogLevel.Debug, "3029", "FYI: CRC Bytes swapped to send ");
                                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                                    }
                                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + responseList[i].Length, crcbytes.Length);
                                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + responseList[i].Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);


                                    //_logger.WriteMessage(NLog.LogLevel.Info, "3044", "Dump of response bytes raw:" + finalpacket.Length.ToString());
                                    //Misc.LogMsg(_logger, finalpacket, finalpacket.Length);

                                    try
                                    {
                                        totalmsglen += finalpacket.Length;
                                        await SendResponseAsync(finalpacket, (uint)finalpacket.Length);
                                    }
                                    catch
                                    {
                                        // bail out for any exception
                                        _logger.WriteMessage(NLog.LogLevel.Error, "3075", "Exception during transmit to controller SN" + client3000.sn.ToString());
                                        terminate_connection = true;
                                        break;
                                    }
                                    _txBytecount += (int)finalpacket.Length;
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "3029", "Total bytes TX'd to client this connection " + _txBytecount);
                                    j++;    // number of messages this pass
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "3029", "Total messages this sequence " + j.ToString());
                                    if (_sn != 0)
                                    {
                                        // update stats tracking table
                                        CalsenseControllers.BytesSent((int)_sn, (int)finalpacket.Length);
                                        CalsenseControllers.MsgsSent((int)_sn, 1);
                                    }
                                }
                                i++;    // next response if any
                            }

                            DateTime endstamp = DateTime.Now;
                            TimeSpan msgTime = endstamp - beginstamp;

                            _logger.WriteMessage(NLog.LogLevel.Debug, "3033", "Message time total: " + msgTime.TotalSeconds.ToString() + " seconds , mid: " + _mid);
                        }

                        if (terminate_connection)
                        {
                            // okay to move on to next thing on this hub.
                            _logger.WriteMessage(NLog.LogLevel.Info, "3030", "End of this NID message sequence");
                            //
                            // don't call this here - only if a Poll response is received
                            // HubConnectionTracker.EndOfPoll(_ackey);
                            //
                        }

                    }
#if STATSDONE
                if (_sn != 0)
                {
                    // update stats tracking table
                    CalsenseControllers.MsgsReceived((int)_sn, 1);
                }

#endif
                    // break out of for loop, restart while(true) loop to begin new packet
                    break;
                }
            }
        }

        public async Task SendResponseAsync(byte[] data, UInt32 len)
        {
            await _networkStream.WriteAsync(data, 0, (int)len).ConfigureAwait(false);
            await _networkStream.FlushAsync().ConfigureAwait(false);
        }

        public async Task FactoryResetToOldControllerRequest(MyLogger _log, uint nid, uint serial_number)
        {
            byte[] data;

            _log.WriteMessage(NLog.LogLevel.Info, "382", "Sending a factory reset to hub connected NID " + nid.ToString());

            var mem = new MemoryStream(0);
            // PID byte
            mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
            // message class byte
            mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
            // to serial number
            Misc.Uint32ToNetworkBytes(serial_number, mem);
            // network id
            Misc.Uint32ToNetworkBytes(nid, mem);
            // mid
            Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_panel_swap_factory_reset_to_old_panel, mem);

            data = mem.ToArray();

            mem.Dispose();

            UInt32 crc;
            crc = Misc.CRC(data);
            //_log.WriteMessage(NLog.LogLevel.Debug, "3040", "Calculated CRC: 0x" + crc.ToString("X"));
            _log.WriteMessage(NLog.LogLevel.Debug, "3028", "Response to be transmitted to SN:" + _sn.ToString());

            // combine preamble, data, CRC and then postamble
            byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
            System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
            System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

            byte[] crcbytes;
            crcbytes = System.BitConverter.GetBytes(crc);
            if (BitConverter.IsLittleEndian)
            {
                //_logger.WriteMessage(NLog.LogLevel.Debug, "3029", "FYI: CRC Bytes swapped to send ");
                Array.Reverse(crcbytes, 0, crcbytes.Length);
            }
            System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
            System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

            try
            {
                await SendResponseAsync(finalpacket, (uint)finalpacket.Length);
            }
            catch
            {
                // bail out for any exception
                _logger.WriteMessage(NLog.LogLevel.Error, "3075", "Exception during transmit to controller SN " + _sn.ToString());
            }
        }
    }
}
