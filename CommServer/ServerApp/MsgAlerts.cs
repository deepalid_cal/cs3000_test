﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using Advantage.Data.Provider;



namespace Calsense.CommServer
{
    public class MsgAlerts
    {
        // common C code that provides alert parsing capability
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint nm_ALERT_PARSING_parse_alert_and_return_length(uint paid,
            [In, Out] byte[] ppile,
            uint pparse_why,
            [In, Out] byte[] pdest_ptr,
            uint pallowable_size,
            int pindex);

        // common C code that provides visibility boolean for given alert, pass in alert id,true,true returns true or false
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint ALERTS_alert_is_visible_to_the_user(uint paid,
            uint pinclude_alerts,
            uint pinclude_changes);

        // if it throws access exception (not normally catchable in C#)
        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptionsAttribute]
        [System.Security.SecurityCriticalAttribute]
        static uint alerts_wrapper(MyLogger _logger, ushort diag_code, byte[] receiveddata, int offset, byte[] raw_string)
        {
            uint alert_size;

            try
            {
                alert_size = nm_ALERT_PARSING_parse_alert_and_return_length(diag_code,
                  receiveddata,
                  Constants.PARSE_FOR_LENGTH_AND_TEXT,
                  raw_string,
                  Constants.MAX_ALERT_STRING - 1,
                  offset);
            }
            catch (Exception e)
            {
                alert_size = 0;
                _logger.WriteMessage(NLog.LogLevel.Error, "7093", "Alerts parse exception in nm_ALERT_PARSING_parse_alert_and_return_length " + 
                                     "(Diag Code: " + diag_code.ToString() + ") : " + e.ToString());
            }

            return (alert_size);
        }

        //
        // Parses Engineering Alerts Message inbond message from controller
        // returns via Tuple:
        //   List of byte arrays, which are 0 to n messages to send as a response
        //   String that will be passed back in if this connection stays up
        //   Boolean as a flag to terminate connection or not
        //   int as an internal state to track thru session (if needed)
        //
        public static async Task<Tuple<List<byte[]>,String, bool, int>> Parse(ClientInfoStruct client, CancellationTokenSource cts)
        {
            int i;
            int cc;
            byte[] data;
            byte[] raw_string = new byte[Constants.MAX_ALERT_STRING];
            MyLogger _logger;
            uint size;
            byte[] Buffer = null;
            AdsCommand cmd;
            int zero;
            bool abort = true;


            int offset;
            uint alert_size;

            DateTime? lastReportDateTime = null;
            DateTime newestAlertDT = DateTime.Parse("1900/01/01 00:00:00");

            AlertList l = new AlertList();

            _logger = new MyLogger(typeof(MsgAlerts).Name, Misc.ClientLogString(client), Misc.ClientLogDB(client), Constants.CS3000Model);
            _logger.WriteMessage(NLog.LogLevel.Info, "7004", "Processing Alerts message...");

            size = client.rxlen;
            Buffer = client.rx;

            offset = 0;
            i = 0;
            // data consists of variable sized alerts, where each alerts contains:
            //  Alert ID: 16-bit unsigned (used to identify how to parse the alert)
            //  Time Stamp: The DATE_TIME when the alert was generated
            //  Raw bytes based upon the alert ID. For example, some alerts have no associated bytes and 
            // simply display a text string based upon the alert ID. Others may include the entire text 
            // string. The rest usually contain a mix of 8-bit, 16-bit, and 32-bit variables which contain 
            // information necessary for the alert.
            //  One byte length, length of all the bytes in alert including length byte

            while(offset < size)
            {
                // empty our string buffer to start with
                Array.Clear(raw_string, 0, raw_string.Length);

                l.records.Add(new Alert());
                l.records[i].diag_code = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                l.records[i].time = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                l.records[i].date = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                
                // parse the alert getting the descriptive string that we want and knowledge of length to get us to next alert
                alert_size = alerts_wrapper(_logger, l.records[i].diag_code, Buffer, offset, raw_string);

                // check for error return from parse routine
                if(alert_size == 0)
                {
                    // stop processing if routine returns 0 indicating error
                    _logger.WriteMessage(NLog.LogLevel.Error, "7094", "Unexpected alert parsing length of 0 on an Alert of ID " + l.records[i].diag_code.ToString());
                    l.records[i].description = "ALERT PARSE ZERO LENGTH MISMATCH (Alert " + l.records[i].diag_code + ")";
                    break;
                }
                // check for mismatch between computed and returned length which probably indicates structure change
                else if(alert_size != (Buffer[offset + alert_size -1]-8))
                {
                    // stop processing on a mismatch, log error
                    _logger.WriteMessage(NLog.LogLevel.Error, "7099", "Alert " + l.records[i].diag_code.ToString());
                    _logger.WriteMessage(NLog.LogLevel.Error, "7099", "Mismatch on parsed length " + alert_size.ToString() + " vs calc using length byte value " + (8 + Buffer[offset + alert_size - 1]).ToString());
                    l.records[i].description = "ALERT PARSE SIZE MISMATCH (Alert " + l.records[i].diag_code + ")";
                    break;
                }
                else
                {
                    //_logger.WriteMessage(NLog.LogLevel.Info, "7055", "Parsed alert " + l.records[i].diag_code.ToString() + " length: " + alert_size.ToString());
                }
                offset += (int) alert_size;
                l.records[i].description = System.Text.Encoding.UTF8.GetString(raw_string, 0, Constants.MAX_ALERT_STRING);
                zero = l.records[i].description.IndexOf('\0');
                if (zero >= 0) l.records[i].description = l.records[i].description.Remove(zero);
                if(l.records[i].description.Length > 0)
                {
                    //_logger.WriteMessage(NLog.LogLevel.Info, "7056", "Desc: " + l.records[i].description.ToString());
                }
                else
                {
                    // put in an empty description so SQL not upset on insert
                    _logger.WriteMessage(NLog.LogLevel.Warn, "7057", "Empty Description returned from parser on alert: " + l.records[i].diag_code.ToString());
                }
                // remove any non-ASCII characters so does not mess up SQL
                l.records[i].description = Regex.Replace(l.records[i].description, @"[^\u0000-\u007F]", string.Empty);

                i++;

            }

#if DUMPRAWBYTES
            _logger.WriteMessage(NLog.LogLevel.Info, "7004", "Dump of bytes raw:" + size.ToString());
            for (i = 0, j = (int)size; i < size; i += 8)
            {
                if (j >= 8)
                {
                    s = Buffer[i].ToString("x2") + " " + Buffer[i + 1].ToString("x2") + " " + Buffer[i + 2].ToString("x2") + " " + Buffer[i + 3].ToString("x2") + " " + Buffer[i + 4].ToString("x2") + " " + Buffer[i + 5].ToString("x2") + " " + Buffer[i + 6].ToString("x2") + " " + Buffer[i + 7].ToString("x2");
                    j -= 8;
                }
                else
                {
                    s = "";
                    while (j > 0)
                    {
                        s = s + Buffer[i].ToString("x2") + " ";
                        j--;
                        i++;
                    }
                }
                _logger.WriteMessage(NLog.LogLevel.Debug, "8001", s);
                if (j <= 0) break;
                Thread.Sleep(10);       // allow UDP logger to catch up
            }
#endif

            DateTime? dt = await Misc.GetReportLastDateTime(_logger, cts, "LastTimestamp_Alerts", client.nid);

            if (dt != null)
            {
                lastReportDateTime = (DateTime)dt;
            }
            else
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "14337", "Couldn't get the last timestamp for alerts.");
            }

            // record alerts in database
            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                // @WB Check if the Network id in DB before insert records.
                cmd = new AdsCommand("SELECT ISNULL(NetworkID,0) FROM CS3000Networks  n join controllersites s on s.siteid=n.networkid " +
                            " WHERE s.deleted = false and  n.NetworkId=" + client.nid, conn);
                var existsID = cmd.ExecuteScalar();
                cmd.Unprepare();
                if (existsID == null)
                {
                    // we won't insert alert
                    _logger.WriteMessage(NLog.LogLevel.Error, "7019", "Network id is not in DB. SN:" + client.sn.ToString() + ", nid:" + client.nid.ToString());
                }
                else
                {
                    _logger.WriteMessage(NLog.LogLevel.Info, "7020", "Inserting " + l.records.Count.ToString() + " Alerts row(s) into DB...");
                    // NOTE: Transactions not supported on local server, so ignore that exception
                    // insert data into DB and retrieve new network id to assign to controller
                    AdsTransaction txn = null;

                    try
                    {
                        txn = conn.BeginTransaction();
                    }
                    catch (System.NotSupportedException)
                    {

                    }
                    do
                    {
                        try
                        {
                            DateTime beginstamp = DateTime.Now;
                            for (cc = 0; cc < l.records.Count; cc++)
                            {
                                DateTime alertDT = Misc.PackedTimeMilliDatetoDatetime(l.records[cc].time, l.records[cc].date);

                                if (lastReportDateTime == null || (alertDT <= lastReportDateTime))
                                {
                                    cmd = new AdsCommand("MERGE CS3000Alerts ON " +
                                        " (NetworkID=" + client.nid.ToString() + " AND AlertDiagCode=" + l.records[cc].diag_code.ToString() +
                                        " AND AlertTimeStamp=" + Misc.PackedTimeMilliDatetoSQLtimestamp(l.records[cc].time, l.records[cc].date).ToString() +
                                        " ) WHEN NOT MATCHED THEN " +
                                        " insert (NetworkID, AlertTimestamp,  " +
                                        " AlertDiagCode, Description, UserVisible) " +
                                        " values(" + client.nid.ToString() + ","
                                        + Misc.PackedTimeMilliDatetoSQLtimestamp(l.records[cc].time, l.records[cc].date).ToString() + ","
                                        + l.records[cc].diag_code.ToString() + "," + "'" + l.records[cc].description.ToString().Replace("'", "''") + "'" + ","
                                        + (ALERTS_alert_is_visible_to_the_user((uint)l.records[cc].diag_code, 1, 1) != 0 ? "TRUE" : "FALSE")
                                        + ")", conn, txn);
                                }
                                else
                                {
                                    cmd = new AdsCommand("INSERT INTO CS3000Alerts (NetworkID, AlertTimestamp,  " +
                                        " AlertDiagCode, Description, UserVisible) " +
                                        " values(" + client.nid.ToString() + ","
                                        + "'" + alertDT.ToString("d") + " " + alertDT.ToString("HH:mm:ss.fff") + "',"
                                        + l.records[cc].diag_code.ToString() + "," + "'" + l.records[cc].description.ToString().Replace("'", "''") + "'" + ","
                                        + (ALERTS_alert_is_visible_to_the_user((uint)l.records[cc].diag_code, 1, 1) != 0 ? "TRUE" : "FALSE")
                                        + ")", conn, txn);
                                }

                                if (alertDT > newestAlertDT)
                                {
                                    newestAlertDT = alertDT;
                                }

                                //_logger.WriteMessage(NLog.LogLevel.Debug, "7053", "SQL: " + cmd.CommandText);

                                // don't insert empty or null description.
                                if (!String.IsNullOrEmpty(l.records[cc].description))
                                {
                                    cmd.ExecuteNonQuery();
                                }
                                cmd.Unprepare();
                            }

                            // 1/11/2017 wjb : update the lastTimestamp_Alerts in CS3000NetworkSettings table
                            cmd = new AdsCommand("UPDATE CS3000NetworkSettings SET LastTimestamp_Alerts='" +
                                                 newestAlertDT.ToString("d") + " " + newestAlertDT.ToString("HH:mm:ss.fff") + "'" +
                                                 " WHERE NetworkID=" + client.nid.ToString(), conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();
                            
#if COMBINEALERTSINTOONESQLSTATEMENT
                            String bigcommand = "";
                            for( cc = 0; cc < l.records.Count; cc++)
                            {
                                bigcommand = bigcommand + "MERGE CS3000Alerts ON " +
                                    " (NetworkID=" + client.nid.ToString() + " AND AlertDiagCode=" + l.records[cc].diag_code.ToString() +
                                    " AND AlertTimeStamp=" + Misc.PackedTimeMilliDatetoSQLtimestamp(l.records[cc].time, l.records[cc].date).ToString() +
                                    " ) WHEN NOT MATCHED THEN " +
                                    " insert (NetworkID, AlertTimestamp,  " +
                                    " AlertDiagCode, Description, UserVisible) " +
                                    " values(" + client.nid.ToString() + ","
                                    + Misc.PackedTimeMilliDatetoSQLtimestamp(l.records[cc].time, l.records[cc].date).ToString() + ","
                                    + l.records[cc].diag_code.ToString() + "," + "'" + l.records[cc].description.ToString().Replace("'", "''") + "'" + ","
                                    + (ALERTS_alert_is_visible_to_the_user((uint)l.records[cc].diag_code, 1, 1) != 0 ? "TRUE" : "FALSE")
                                    + ");";
                            }
                            cmd = new AdsCommand(bigcommand, conn, txn);
                            cmd.ExecuteNonQuery();
                            _logger.WriteMessage(NLog.LogLevel.Debug, "7053", "SQL: " + cmd.CommandText);                            
                            cmd.Unprepare();
#endif
                            DateTime endstamp = DateTime.Now;
                            TimeSpan sqltime = endstamp - beginstamp;
                            _logger.WriteMessage(NLog.LogLevel.Debug, "7777", "SQL time total for " + l.records.Count.ToString() + " alerts is " + sqltime.ToString());
                            // if we made it to here, we are good to commit all our changes
                            abort = false;
                        }
                        catch (Exception e)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "7091", "Database exception during alerts insert(s): " + e.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    if (abort == true)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "7091", "Database Error on alerts inserts.");
                        txn.Rollback();
                    }
                    else
                    {
                        txn.Commit();
                    }
                }
                db.CloseDBConnection(conn, _logger);
            }

            // if nothing saved to database, do not ACK
            if (abort == true)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "9901", "Not acking, problem with DB update.");

                _logger.WriteMessage(NLog.LogLevel.Info, "14333", "Build Alerts Nak response.");

                return (Tuple.Create(Misc.buildNakResponse(client.nid, client.sn, client.mid), (String)null, false, Constants.NO_STATE_DATA));
            }

            var mem = new MemoryStream(0);
            // build ack response
            if (client.nid != 0)
            {
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(client.sn, mem);
                // network id
                Misc.Uint32ToNetworkBytes(client.nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_ENGINEERING_ALERTS_full_receipt_ack, mem);
                _logger.WriteMessage(NLog.LogLevel.Info, "7071", "Build Engineering Alerts Ack response.");
            }

            // get the built response to return and be sent
            data = mem.ToArray();
            List<byte[]> rsplist = new List<byte[]>();
            rsplist.Add(data);
            // if we are sending Ack, controller will disconnect socket
            return (Tuple.Create(rsplist, (String)null, false, Constants.NO_STATE_DATA));
        }
    }
}
