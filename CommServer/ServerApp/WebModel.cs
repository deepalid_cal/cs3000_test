﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace ServerApp
{
    // class to map to web control page, variables controlled via HTML form
    public sealed class WebFormData
    {
        public bool IgnoreMode { get; set; }
        public bool ForceFirmwareUpdate { get; set; }
        public bool ForceTPUpdate { get; set; }
        private static readonly WebFormData instance = new WebFormData();

        // private initializer, called only once on this singleton
        // set our vars to their default startup value for normal operation
        private WebFormData(){}

        public static WebFormData Instance
        {
            get 
            { 
                return instance;
            }
        }

    }


    // these classes use to internally track statistics for the controllers we see
    // they are displayed on internal web server page
    public class CalsenseController
    {
        public int ControllerID { get; set; }
        public string ControllerName { get; set; }

        public int LocationID { get; set; }
        public string LocationName { get; set; }

        public IPAddress IP { get; set; }

        public long BytesSent { get; set; }
        public long BytesReceived { get; set; }
        public long MessagesSent { get; set; }
        public long MessagesReceived { get; set; }

        public DateTime? FirstHeard { get; set; }
        public DateTime? LastHeard { get; set; }

        public CalsenseController(int cid)
        {
            ControllerID = cid;
            FirstHeard = DateTime.UtcNow;
            LastHeard = FirstHeard;
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            CalsenseController objAsPart = obj as CalsenseController;
            if (objAsPart == null) return false;
            else return Equals(objAsPart);
        }
        public override int GetHashCode()
        {
            return ControllerID;
        }
        public bool Equals(CalsenseController other)
        {
            if (other == null) return false;
            return (this.ControllerID.Equals(other.ControllerID));
        }
        public bool Equals(int ID)
        {
            return (this.ControllerID.Equals(ID));
        }
    }

    public sealed class CalsenseControllers
    {
        private IList<CalsenseController> Controllers;

        private static readonly  Lazy<CalsenseControllers> lazy =
        new Lazy<CalsenseControllers>(() => new CalsenseControllers());

        public static CalsenseControllers Instance { get { return lazy.Value; } }

        private CalsenseControllers()
        {
            Controllers = new List<CalsenseController>();
        }

        public static IList<CalsenseController> GetIList()
        {
            return (Instance.Controllers);
        }

        public static int NumSeen()
        {
            return Instance.Controllers.Count();
        }
        public static int Find(int ID)
        {
            if (Instance.Controllers.Count() == 0) return (-1);  // nothing to find
            for (int i = 0; i < Instance.Controllers.Count; i++)
            {
                if (Instance.Controllers[i].Equals(ID))
                {
                    return i;
                }
            }
            return -1;
        }
        public static int AddifNew(int ControllerID)
        {
            int idx = 01;

            idx = Find(ControllerID);
            if (idx == -1)
            {
                Instance.Controllers.Add(new CalsenseController(ControllerID));
                idx = Find(ControllerID);
            }
            return (idx);
        }

        public static bool Contacted(int ControllerID, IPAddress IP)
        {
            int idx = AddifNew(ControllerID);
            if(idx != -1)
            {
                Instance.Controllers[idx].IP = IP;
                Instance.Controllers[idx].LastHeard = DateTime.UtcNow;
                return (true);
            }
            return (false);
        }

        public static bool BytesSent(int ControllerID, int txcount)
        {
            int idx = AddifNew(ControllerID);

            if (idx != -1)
            {
                Instance.Controllers[idx].BytesSent += txcount;
                return (true);
            }
            return (false);
        }

        public static bool BytesReceived(int ControllerID, int rxcount)
        {
            int idx = AddifNew(ControllerID);

            if (idx != -1)
            {
                Instance.Controllers[idx].BytesReceived += rxcount;
                return (true);
            }
            return (false);
        }

        public static bool MsgsSent(int ControllerID, int msgcount)
        {
            int idx = AddifNew(ControllerID);

            if (idx != -1)
            {
                Instance.Controllers[idx].MessagesSent += msgcount;
                return (true);
            }
            return (false);
        }

        public static bool MsgsReceived(int ControllerID, int msgcount)
        {
            int idx = AddifNew(ControllerID);

            if (idx != -1)
            {
                Instance.Controllers[idx].MessagesReceived += msgcount;
                return (true);
            }
            return (false);
        }
    }

    public class About
    {
        public string BuildStamp { get; set; }
        public string Path { get; set; }
        public About()
        {
            BuildStamp = RetrieveLinkerTimestamp().ToString();
            Path = System.Reflection.Assembly.GetCallingAssembly().Location.ToString();
        }
        private DateTime RetrieveLinkerTimestamp()
        {
            string filePath = System.Reflection.Assembly.GetCallingAssembly().Location;
            const int c_PeHeaderOffset = 60;
            const int c_LinkerTimestampOffset = 8;
            byte[] b = new byte[2048];
            System.IO.Stream s = null;

            try
            {
                s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                s.Read(b, 0, 2048);
            }
            finally
            {
                if (s != null)
                {
                    s.Close();
                }
            }

            int i = System.BitConverter.ToInt32(b, c_PeHeaderOffset);
            int secondsSince1970 = System.BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0);
            dt = dt.AddSeconds(secondsSince1970);
            dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours);
            return dt;
        }
           
    }
}
