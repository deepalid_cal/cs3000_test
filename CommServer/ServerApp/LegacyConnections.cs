//
// LegacyConnections - manages table to track network connections to legacy controllers,
//                     connections establish *from* server *to* legacy controller.
// (Static class, as only one copy ever exists in the CommServer)
//
//
// A "TrackingTable" stores an entry for each IP address that is currently has
// communications active or queued up to be active.
//
// Each entry in the table points to a "ActiveLegacyConnection" object. One
// of these objects will exist per IP address for non-RF-hub controllers or
// one per RF-hubs (controllers that can interfere with each other).
//
// This means commands are held in a queue either:
// a) queue for all commands directed to a single IP address (which could
//    be a single controller, or multiple controllers behind a hub)
// b) queue for all commmands directed to multiple IP address that make
//    up a customer "site".
//
// The command queues contain priority information for each message to
// be sent, so highest priority per queue becomes the next message to send.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Calsense.Logging;
using ServerApp;
using System.Collections.Concurrent;
using System.Configuration;
using Advantage.Data.Provider;
using System.IO;

namespace Calsense.CommServer
{
    // 
    // store a one of these for each active Legacy Controller we are talking to
    class ActiveLegacyConnection
    {

        public class ControllerInfo<T1, T2, T3, T4>
        {
            public T1 CID { get; set; }
            public T2 FWVersion { get; set; }   // 0 = unknown, 5= series 500 6= series 600
            public T3 Hostname { get; set; }
            public bool GetCMOSSent { get; set; }   // true if we've already queued/sent a GetCMOS to this controller
            public bool GetCMOSRecv { get; set; }
            public int TransportTimerMultiplier;    // value returned from SQL NoOfControllersInLegacyChain(), -1 ,0, or n
            public bool imAhub { get; set; }
            public int myhubtype { get; set; }
            public bool imBehind3000 { get; set; }
            public bool repeaterInUse { get; set; }
            public int dbTimerAddon { get; set; }

            public ControllerInfo(T1 a, T2 b, T3 c, int Multiplier, int hubtype, bool RepeaterInUse, int DBTimerAddon)
            {
                CID = a;
                FWVersion = b;
                Hostname = c;
                TransportTimerMultiplier = Multiplier;
                GetCMOSSent = false;
                GetCMOSRecv = false;
                imAhub = false;
                myhubtype = hubtype;
                imBehind3000 = false;
                repeaterInUse = RepeaterInUse;
                dbTimerAddon = DBTimerAddon;
                if (myhubtype == 5 || myhubtype == 6 || myhubtype == 9) imAhub = true;
                if (myhubtype == 3 || myhubtype == 4) imBehind3000 = true;
            }
        }

        public class Controller3000Info<T1, T2>
        {
            public T1 NID { get; set; }
            public T2 Hostname { get; set; }
            public Socket sock_3000 { set; get; }
            public Int32 pollstate {set; get; }
            public NetworkStream net_stream3000 { set; get; }
            public T1 myHubID { get; set; }     // to know which controller on which hub
            public bool imAhub { get; set; }
            public UInt32 sn { get; set; }
            public Int32 mobilestate { get; set; }
            public Int32 mobileduration { get; set; }
            public Int32 Status10Seen { set; get; }
            public Boolean needsHublist { set; get; }
            public UInt32 consecutiveErrors {set; get; }
            // this next value is only needed one per hub, but we'll have to make space in each of our 3000 structures
            // since we don't differentiate a hub
            public CancellationTokenSource ResetRxToken { set; get; }   // used to signal receiver a poll timeout occurred
            public Boolean ignoreNextNoMoreMessages { set; get; }       // set if hub connects while other hubs on company already in poll sequence
            public UInt32 demandPollJobId { set; get; }                 // when a demand poll happens to a 3000, save job id here
            public Boolean ignoreUntilHubListAcked { set; get; }        // can't talk to controllers behind hub until hub list has been received by hub

            public Controller3000Info(T1 a, T2 b)
            {
                int i;
                NID = a;
                Hostname = b;
                i = Int32.Parse(ConfigurationManager.AppSettings["HUB_POLL_STANDARD"]);
                i -= 10;    // poll 10 seconds after creation
                if (i < 1) i = 1;
                pollstate = i; // 0 = poll queued, 1+ = seconds since poll / pseudopoll transmitted, big number will make poll go next
                mobilestate = -1;   // not in a mobile condition
                mobileduration = 0;
                imAhub = false; // default to not being a hub
                Status10Seen = 0;
                needsHublist = false;
                consecutiveErrors = 0;
                ResetRxToken = null;
                ignoreNextNoMoreMessages = false;
                demandPollJobId = 0;
                ignoreUntilHubListAcked = false;
            }
        }

        public ActiveLegacyConnection(String hostname, UInt32 ctype, UInt32 site_id)
        {
            net_stream = null;
            sock = null;
            connection_type = ctype;
            controllers = new List<ControllerInfo<UInt32, Int32, String, int>>();    // list of controllers, their firmware version, and their hostnames on this connection
            controllers3000 = new List<Controller3000Info<UInt32, String>>();        // list of 3000 series controllers
            queued_functions = new List<Tuple<int, Action<ActionArgs>, ActionArgs>>();
            acsem = new SemaphoreSlim(1);
            // since we are make a new "connection" the first host is the first active address we use
            activehost = new StringBuilder(hostname);
            activeadr = null;
            activecid = 0;
            site = site_id;
            key = null;
            listenCts = new CancellationTokenSource(); // cancellation token for closing listener
            activemultiplier = 0;
            hubhandler = false;
            codedownloadinprogress = false;
            downloadingNID = 0;
            lastConnectattempt = new DateTime(2000, 1, 1);
            NeedsReconnect = false;
            TaskActive = false;
            pollTimeout = -1;
            AddReuseGoodToGo = false;
            Slave3000MainOutofdate = false;
            Slave3000TPOutofdate = false;
            activeDBaddon = 0;
            activeRepeater = false;
            inbound2000 = false;
        }
        public NetworkStream net_stream { set; get;}
        public Socket sock { set; get; }
        public UInt32 connection_type { set; get; }
        public List<Tuple<int, Action<ActionArgs>, ActionArgs>> queued_functions;   // priority and function
        public List<ControllerInfo<UInt32, Int32, String, int>> controllers;  // info for all the controllers on this connection
        public List<Controller3000Info<UInt32, String>> controllers3000;  // info for all the controllers on this connection
        public SemaphoreSlim acsem;   // protect access to our lists/queues on this connection
        public StringBuilder activehost { set; get; }   // the current hostname/IP we are communicating with
        public StringBuilder activeadr { set; get; }    // the current 3 character address for controller we are communicating with
        public UInt32 activecid { set; get; }    // the current controller id for controller we are communicating with
        public UInt32 site { set; get; }    // the site id for this controller/controllers
        public String key { set; get; } // the dictionary key we are stored under
        public CancellationTokenSource listenCts {set; get;}
        public int activemultiplier;
        public int activeDBaddon;   // add additional second(s) to TX timer from "TxTimerAddon" field in controllers table 
        public bool activeRepeater; // we add additional second(s) to TX timer if system set to use a repeater
        public Boolean hubhandler { set; get; }
        public Boolean codedownloadinprogress { set; get; }
        public UInt32 downloadingNID { set; get; }  // the NID that is doing a code distribution. only poll him until complete
        public DateTime lastConnectattempt { set; get; }
        public Boolean NeedsReconnect { set; get; }
        public Boolean TaskActive { set; get; }
        public Int32 pollTimeout {set; get; }             // to catch any 3000 poll that does not come back
        public Boolean AddReuseGoodToGo { set; get; }       // flag to AddorReuse that socket for 3000 hub is good to go, don't check      
        public Boolean Slave3000MainOutofdate { set; get; } // at least one slave 3000 is out of date on Main firmware
        public Boolean Slave3000TPOutofdate { set; get; }   // at least one slave 3000 is out of date on TP Micro firmware
        public Boolean inbound2000 { set; get; }        // inbound 2000 situation
    }

    //
    // class that handles populating and manipulating lookup table containing ActiveLegacyController
    //
    static class LegacyConnectionTracker
    {
        // constructor
        static LegacyConnectionTracker() 
        {
            TrackingTable = new ConcurrentDictionary<String, ActiveLegacyConnection>();     // Dictionary of hostnames to Connection objects
            _log = new MyLogger("LegacyTracker", "", "", Constants.ET2000Model);
            _sem = new SemaphoreSlim(1);
            _cts = null;        // this will get set at runtime
            rand = new Random();
            throttle = null;
            throttleCts = new CancellationTokenSource();    // cancellation token for throttle semaphore
            throttleCurrentSize = 0;
            throttleCurrentWaiters = 0;
            throttleMaxWaiters = 0;
            LockoutTable = new ConcurrentDictionary<String, DateTime>();
        }

        // properties
        private static ConcurrentDictionary<String, ActiveLegacyConnection> TrackingTable;
        private static MyLogger _log;
        private static SemaphoreSlim _sem;  // mutex to protect access to our dictionary
        private static CancellationTokenSource _cts;
        private static Random rand;         // for random delay time
        public static SemaphoreSlim throttle;  // throttle connections
        public static CancellationTokenSource throttleCts { set; get; }
        private static CancellationTokenSource comboCts { set; get; }
        public static int throttleCurrentSize;
        public static int throttleCurrentWaiters;
        public static int throttleMaxWaiters;
        public static ConcurrentDictionary<String, DateTime> LockoutTable;

        public static int totalThrottleFrees;
        public static int totalThrottleSets;
        public static int totalTaskActiveSets;
        public static int totalTaskActiveFrees;

        public static void SetCts(CancellationTokenSource c)
        {
            _cts = c;
            comboCts = CancellationTokenSource.CreateLinkedTokenSource(_cts.Token, throttleCts.Token);
            HubConnectionTracker.SetCts(c);
        }

        private static void DisconnectController(ActiveLegacyConnection ac)
        {
            // signal listener to close
            // we let the socket exception close the listener...if we do a ac.listenCts.Cancel();
            // we can accidently close the old *and* the new socket when switching between controllers
            // on a hub.
            // close socket
            if (ac.sock == null) return;
            if (!ac.sock.Connected) return;
            ac.sock.Shutdown(SocketShutdown.Both);
            ac.sock.Disconnect(false);
        }
        private static Boolean ConnectController(ActiveLegacyConnection ac)
        {
            IPAddress ip;
            int port = 0;
            int retries;
            int delay;
            bool fail = true;

            retries = Convert.ToInt32(ConfigurationManager.AppSettings["LegacyConnectionRetryCount"]);
            delay = Convert.ToInt32(ConfigurationManager.AppSettings["LegacyConnectionFailSquelchSeconds"]);
            if (delay < 0) delay = 0;

            String connectname = ac.activehost.ToString();

            // if there is a colon, then we have a hard coded port number to deal with
            if(connectname.Contains(':'))
            {
                port = int.Parse(connectname.Split(':')[1]);
                connectname = connectname.Split(':')[0];
            }

            if (!IPAddress.TryParse(connectname, out ip))
            {
                connectname = ac.activehost + ".eairlink.com";   // if not already an IP address, assume a phone number, convert to hostname
            }
            if (ac.connection_type == 5 && port == 0)
            {
                port = 2000;
            }
            if (ac.connection_type == 7 && port == 0)
            {
                port = 12345;
            }

            // if this host has had connection problems recently, don't even bother until
            // timeout expires
            DateTime checktime;
            if (LockoutTable.TryGetValue(connectname, out checktime))
            {
                // this host may be locked out, see if that is the case and bail if so
                if (DateTime.Now < checktime)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "199", "Not attempting connection as controller recently failed connection attempt.", ac.activecid.ToString());
                    return (false);
                }
            }

            _log.WriteMessageWithId(NLog.LogLevel.Info, "3005", "Attempting connection attempt to " + connectname, ac.activecid.ToString());

            try
            {
                ac.sock = new Socket(AddressFamily.InterNetwork,
                                    SocketType.Stream,
                                    ProtocolType.Tcp);
            }
            catch (Exception e)
            {
                if (e is SocketException)
                {
                    var ex = (SocketException)e;
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "3045", "Socket error " + e.ToString() + " during adding new connection to " + connectname, ac.activecid.ToString());
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "3045", "Unexpected exception during creating a new connection to " + connectname, ac.activecid.ToString());
                }

                return (false);
            }

            while (true)
            {

                try
                {

                    fail = false;

                    IPAddress[] IPs = Dns.GetHostAddresses(connectname);

                    ac.lastConnectattempt = DateTime.Now;

                    ac.sock.Connect(IPs[0], port);
                }
                catch (Exception e)
                {
                    if (e is SocketException)
                    {
                        var ex = (SocketException)e;
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "3005", "Socket error " + e.ToString() + " during connection attempt to " + connectname, ac.activecid.ToString());
                    }
                    else
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "3005", "Unexpected exception during connection attempt to " + connectname, ac.activecid.ToString());
                    }

                    fail = true;
                }

                if (fail != true)
                {
                    // connection successful
                    break;
                }


                // connection failure, see if we should retry...
                retries--;

                if(retries >= 0)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "3005", "Attempt to connect failed, will retry in Will retry this connection in " + delay.ToString() + " secs.", ac.activecid.ToString());
                    try
                    {
                        Task.Delay(delay * 1000, _cts.Token).Wait();
                    }
                    catch
                    {
                        return(false); // exiting program
                    }
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "3005", "*Retrying* connection attempt to " + connectname, ac.activecid.ToString());
                }
                else
                {
                    // done retrying
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "3005", "Failed connection attempt, after retries, to " + connectname, ac.activecid.ToString());

#if LOCKOUTLEGACYCONTROLLERIMPLEMENTED
                    // flag this controller as a problem 
                    DateTime when;
                    int lockouttime;
                    lockouttime = Convert.ToInt32(ConfigurationManager.AppSettings["LegacyConnectionFailSquelchSeconds"]);

                    if (!LockoutTable.TryGetValue(connectname, out when))
                    {
                        if (lockouttime != 0)
                        {
                            when = DateTime.Now;
                            LockoutTable.TryAdd(connectname, when.AddSeconds((double)lockouttime));
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "3005", "Since connect failed, locking out connection attempts for  " + lockouttime + " secs, until " + when.AddSeconds((double)lockouttime).ToString(), ac.activecid.ToString());
                        }                        
                    }
                    else
                    {
                        // entry already exists, update time
                        if(lockouttime != 0)
                        {
                            when = DateTime.Now;
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "3005", "Connect failed, locking out connection attempts for  " + lockouttime + " secs, until " + when.AddSeconds((double)lockouttime).ToString(), ac.activecid.ToString());
                            LockoutTable[connectname] = when.AddSeconds((double)lockouttime);
                        }
                        else
                        {
                            // no more lockout, remove
                            LockoutTable.TryRemove(connectname, out when);
                        }
                    }
#endif
                    return (false); 
                }
            }


            // we've connected... set up the network stream object
            ac.net_stream = new NetworkStream(ac.sock, true);

            ac.NeedsReconnect = false;

            // set up a legacy listening task to handle responses for controller(s) on this connection
            // note: we use a linked cancellation token so listener can be terminated individually when
            // we want to close connection - or overall when program exiting.
            var client = new LegacyInternal(ac, CancellationTokenSource.CreateLinkedTokenSource(ac.listenCts.Token, _cts.Token));
            Task.Run(() => client.Do());

            return (true);  // success
        }

        //
        // used by LegacyTransport at end of a reception so we can
        // either:
        // a) move to next queue thing to do on connection
        // or
        // b) close connection
        //
        public static void RemoveTrackingEntryIfDone(ActiveLegacyConnection ac)
        {            
            if(ac == null)return;

            // if this ac handled by hub logic, go there

            if(ac.hubhandler)
            {
                HubConnectionTracker.RemoveTrackingEntryIfDone(ac);
                return;
            }

            reenter:

            try
            {
                // lock the priority list so no one messes with it while we search for function to call
                ac.acsem.Wait(_cts.Token);
                // if there is a transport message in progress, we don't need to do anything, the
                // finishing of that transport message will move to next message in queue.
                if(ac.TaskActive)
                {
                    ac.acsem.Release();
                    return;
                }
                // see if anyone pending on queue
                if(ac.queued_functions.Count == 0)
                {
                    // nothing else to process...okay to finish and close connection
                    DisconnectController(ac);
                    // remove any tracking table entry that references this connection
                    _sem.Wait(_cts.Token);

                    // remove the tracking dictionary entry
                    {
                        ActiveLegacyConnection tc;
                        TrackingTable.TryRemove(ac.key, out tc);
                        if (tc == null)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "19", "Couldn't find expected legacy tracking entry for key: " + ac.key.ToString(), ac.activecid.ToString());
                        }
                        else
                        {
                            // give up semaphore
                            throttle.Release();
                            totalThrottleFrees++;
                        }
                    }
                    // clear the lists of controllers/functions (which should already be empty)
                    ac.controllers.Clear();
                    ac.queued_functions.Clear();
                    ac.NeedsReconnect = true;   // needs to reconnect next time using
                    // at this point should no longer be any references to this connection, so should garbage collect automatically
                    _sem.Release();
                }
                else
                {
                    int highest_priority;
                    int idx_to_work;
                recycle:
                    // there are still things to so, so 
                    // look for next highest priority thing to send
                    highest_priority = -1;  // start at impossibly low priority, we know there is at least one higher than this present
                    idx_to_work = -1;
                    ActionArgs args = new ActionArgs();
                    Action<ActionArgs> func = null;
                    foreach (Tuple<int, Action<ActionArgs>, ActionArgs> tf in ac.queued_functions)
                    {
                        if(tf.Item1 > highest_priority)
                        {
                            // note the highest priority we have found so far
                            highest_priority = tf.Item1;
                            // index of this function in our list, so we can access/call it
                            idx_to_work = ac.queued_functions.IndexOf(tf);
                            // get the arguments to pass into action when calling it
                            args.adr = tf.Item3.adr;
                            args.cid = tf.Item3.cid;
                            args.hostname = tf.Item3.hostname;
                            args.timezonekey = tf.Item3.timezonekey;
                            args.connectiontype = tf.Item3.connectiontype;
                            args.jobid = tf.Item3.jobid;
                            args.cityid = tf.Item3.cityid;
                            args.periodicaction = tf.Item3.periodicaction;
                            args.ihsfyaction = tf.Item3.ihsfyaction;
                            args.legacyaction = tf.Item3.legacyaction;
                            func = tf.Item2;
                        }
                    }
                    // if switching to different IP address/port, we need to close current connection and connect to new connection
                    // also, if connection has spontaneously dropped, make a reconnect attempt here
                    if(args.hostname == "INTERNAL")
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "201", "Processing internal command.", args.cid.ToString());
                    }
                    else if(ac.activehost.ToString() != args.hostname || ac.connection_type != args.connectiontype || ac.sock == null || !ac.sock.Connected)
                    {
                        // disconnect from current controller
                        DisconnectController(ac);
                        ac.sock = null; // this socket not around anymore
                        // make new controller the active one
                        ac.activehost.Replace(ac.activehost.ToString(), args.hostname);
                        ac.activeadr.Replace(ac.activeadr.ToString(), args.adr);
                        ac.activecid = args.cid;
                        ac.connection_type = args.connectiontype;
                        ac.activemultiplier = ac.controllers.Find(x => x.CID == ac.activecid).TransportTimerMultiplier;
                        ac.activeDBaddon = ac.controllers.Find(x => x.CID == ac.activecid).dbTimerAddon;
                        ac.activeRepeater = ac.controllers.Find(x => x.CID == ac.activecid).repeaterInUse;
                        ac.NeedsReconnect = false;  // assume connect will work, will get set again if fails
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "201", "Switching controllers/connections to " + args.cid.ToString() + ", Multiplier now " + ac.activemultiplier.ToString() +
                        " , system has a repeater? " + ac.activeRepeater.ToString() +
                        " , additional timer in DB is " + ac.activeDBaddon.ToString(), ac.activecid.ToString());

                        // connect to new controller
                        if(!ConnectController(ac))
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "201", "Could not connect to controller when switching from one to another.", ac.activecid.ToString());
                            // we couldn't connect, so flush pending messages
                            // there can be several different controllers with things queued up, in the case of an LR hub

                            // mark complete with error any tasks queued up
                            if (ac.queued_functions.Count > 0)
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "190", "Ending other jobs queued for same IP/port", ac.activecid.ToString());
                                for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                                {
                                    Tuple<int, Action<ActionArgs>, ActionArgs> tf = ac.queued_functions[i];
                                    if (tf.Item3.hostname == args.hostname)
                                    {
                                        ac.queued_functions.RemoveAt(i);
                                        if (tf.Item3.jobid > 0)
                                        {
#pragma warning disable 4014
                                            Misc.AddET2000Alert(_cts, tf.Item3.cid, "Unable to connect to the controller", true);
                                            LegacyTasks.CompleteLegacyJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
#pragma warning restore 4014
                                        }
                                    }
                                }
                            }
                            if(ac.queued_functions.Count > 0)
                            {
                                // if there are still functions left to process, give next one a try
                                _log.WriteMessageWithId(NLog.LogLevel.Info, "190", "Still some queued functions to work on, looking for next one", ac.activecid.ToString());
                                goto recycle;
                            }
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "190", "No remaining functions, so ending this connection attempt.", ac.activecid.ToString());
                            ac.NeedsReconnect = true;   // needs a connection if used again
                            ac.acsem.Release();
                            // give up semaphore
                            throttle.Release();
                            totalThrottleFrees++;
                            return;
                        }
                    }
                    else if (ac.activecid != 0 && args.cid != ac.activecid)
                    {
                        // changing between controllers on existing connection
                        // update to the new valiues including controller address
                        ac.activecid = args.cid;
                        ac.activeadr.Replace(ac.activeadr.ToString(), args.adr);
                        ac.activemultiplier = ac.controllers.Find(x => x.CID == ac.activecid).TransportTimerMultiplier;
                        ac.activeDBaddon = ac.controllers.Find(x => x.CID == ac.activecid).dbTimerAddon;
                        ac.activeRepeater = ac.controllers.Find(x => x.CID == ac.activecid).repeaterInUse;
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "202", "Switching controllers, Multiplier now: " + ac.activemultiplier.ToString() +
                        " , system has a repeater? " + ac.activeRepeater.ToString() +
                        " , additional timer in DB is " + ac.activeDBaddon.ToString(), ac.activecid.ToString());
                    }

                    // fire up task with this work
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "999", "TASK Start", ac.activecid.ToString());
                    ac.TaskActive = true;
                    LegacyConnectionTracker.totalTaskActiveSets++;
// pragma to remove build warning about not await these "fire and forget tasks"
#pragma warning disable 4014
                    Task.Run(() => func(args), _cts.Token).ConfigureAwait(false);
                    // remove from list
                    ac.queued_functions.RemoveAt(idx_to_work);
                }
                ac.acsem.Release();
            }
            catch(OperationCanceledException)
            {
                // program exiting
                return;
            }
            catch(SocketException)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Warn, "202", "Socket exception during close.", ac.activecid.ToString());
                ac.NeedsReconnect = true;   // needs a connection if used again
                // error during socket closure
                ac.acsem.Release(); // in case not already performed
                // give up semaphore
                throttle.Release();
                totalThrottleFrees++;
                goto reenter;
            }
            catch(IOException)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Warn, "202", "I/O exception during close.", ac.activecid.ToString());
                ac.NeedsReconnect = true;   // needs a connection if used again
                // error during socket closure
                ac.acsem.Release(); // in case not already performed
                // give up semaphore
                throttle.Release();
                totalThrottleFrees++;
                goto reenter;
            }
        }

        public static ActiveLegacyConnection AddOrReuseConnection(String hostname, UInt32 controller_id, UInt32 connection_type, String adr, UInt32 SiteID, int Multiplier, int Hubtype, Boolean InternalOnly, bool RepeaterInUse, int DBTimerAddon)
        {
            ActiveLegacyConnection ac = null;
            Boolean connection_required = false;
            int invalid_arg = 0;
            String key;
            Boolean failed = true;

            // check for a resize
            if (throttle != null)
            {
                if (throttleCurrentSize != LegacyTasks.LegacyMaxConnections)
                {
                    // user has changed throttle setting at run time.
                    // cancel all current waiters and make a new semaphore with new count
                    _sem.Wait(_cts.Token);
                    throttleCts.Cancel();   // cancel so all pending wait tasks free up semaphore
                    while (throttleCurrentWaiters > 0)
                    {
                        try
                        {
                            Task.Delay(100, _cts.Token);    // wait for all the cancels to happen
                        }
                        catch (OperationCanceledException)
                        {
                            // program exiting
                            return(null);
                        }
                    }
                    throttle.Dispose();
                    throttle = null;
                    _sem.Release();
                }
            }
            // if throttle needs creating, make it
            if (throttle == null)
            {
                _sem.Wait(_cts.Token);
                throttleCurrentSize = LegacyTasks.LegacyMaxConnections;
                throttle = new SemaphoreSlim(throttleCurrentSize);
                _sem.Release();
            }

            _log.WriteMessageWithId(NLog.LogLevel.Debug, "29", "FYI: just before this use/reuse of connection, we think there are this many connections throttling: " + throttleCurrentWaiters.ToString(), controller_id.ToString());                                   

            // UseHubType types:
            // 0 or NULL: Controller is not using a hub to communicate.
            // 1: Controller using Legacy LR hub.
            // 2: Controller using Legacy SR hub.
            // 3: Controller using CS3000 LR hub.
            // 4: Controller using CS3000 SR hub.
            // 5: Controller is an LR - HUB
            // 6: Controller is an SR - HUB
            // 7: Controller is a CS3000 LR HUB
            // 8: Controller is a CS3000 SR HUB
            // 9: Controller is an Inbound 2000 hub
            // 10: Controller is a 2000 behind an Inbound 2000 hub
            // any potentional hub interfering connection, and 2000's behind 3000 hubs, are handled via HubConnection now
            if (Hubtype == 1 || Hubtype == 3 || Hubtype == 4 || Hubtype == 5 || Hubtype == 7 || Hubtype == 8 || Hubtype == 9 || Hubtype == 10)
            {
                return(HubConnectionTracker.AddOrReuseConnection(hostname, controller_id, connection_type, adr, SiteID, Multiplier, Hubtype, InternalOnly, RepeaterInUse, DBTimerAddon));
            }

            if (connection_type != 5 && connection_type != 7) invalid_arg = -1;
            if (controller_id == 0) invalid_arg = -2;
            if (hostname == null) invalid_arg = -3;
            if (hostname.Length < 7) invalid_arg = -4;
            if (adr == null) invalid_arg = -5;
            if (adr.Length != 3) invalid_arg = -6;  // legacy controllers use 3 ascii character addresses
            if (SiteID <= 0) invalid_arg = -7;   // must have a site id

            if (invalid_arg != 0)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "9", "Attempt to connect to a legacy controller using invalid IP/name/network_type/address. Error = " + invalid_arg.ToString(), controller_id.ToString());
                return (null);
            }

            // set rf_hub to true if this is an RF shared site thingy
            if(Hubtype == 1 || Hubtype == 5)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Info, "9", "This controller attached via legacy hub, queueing around company ID.", controller_id.ToString());
                // for a legacy RF Hub, we group/queue by Site ID
                key = SiteID.ToString();
            }
            else
            {
                // for all other situations, we group/queue by host/IP
                _log.WriteMessageWithId(NLog.LogLevel.Info, "9", "This controller " + controller_id.ToString() + " using normal IP based queueing.", controller_id.ToString());
                key = hostname;
            }

            // grab the mutex while we mess with things so two players don't try adding
            // same connection 
            try
            {
                _sem.Wait(_cts.Token);

                // if we already have a connection entry for this host, don't need a new object
                if (!TrackingTable.TryGetValue(key, out ac))
                {
                    TrackingTable.TryAdd(key, new ActiveLegacyConnection(hostname, connection_type, SiteID));
                    TrackingTable.TryGetValue(key, out ac);
                    if (ac != null && !InternalOnly)
                    {
                        ac.AddReuseGoodToGo = false;
                        // we know we need a connection, we just created this object fresh
                        connection_required = true;
                        ac.controllers.Add(new ActiveLegacyConnection.ControllerInfo<UInt32, Int32, String, int>(controller_id, 0, hostname, Multiplier, Hubtype, RepeaterInUse, DBTimerAddon));    // this controller is on the connection, with unknown version number
                        ac.key = key;
                        // the active controller will be this first one 
                        ac.activehost = new StringBuilder(hostname);
                        ac.activeadr = new StringBuilder(adr);
                        ac.activecid = controller_id;
                        ac.activemultiplier = ac.controllers.Find(x => x.CID == ac.activecid).TransportTimerMultiplier;
                        ac.activeDBaddon = ac.controllers.Find(x => x.CID == ac.activecid).dbTimerAddon;
                        ac.activeRepeater = ac.controllers.Find(x => x.CID == ac.activecid).repeaterInUse;

                        _log.WriteMessageWithId(NLog.LogLevel.Debug, "199", "Transport Timer multiplier is " + Multiplier.ToString() +
                        " , system has a repeater? " + RepeaterInUse.ToString() +
                        " , additional timer in DB is " + DBTimerAddon.ToString(), controller_id.ToString());
                    }
                }
                else
                {
                    ac.AddReuseGoodToGo = false;
                    // existing connection, see if we already know this controller is on this connection
                    if (!ac.controllers.Any(x => x.CID == controller_id))
                    {
                        // no, so add it
                        ac.controllers.Add(new ActiveLegacyConnection.ControllerInfo<UInt32, Int32, String, int>(controller_id, 0, hostname, Multiplier, Hubtype, RepeaterInUse, DBTimerAddon));    // new controller on existing connection
                        _log.WriteMessageWithId(NLog.LogLevel.Debug, "199", "Transport Timer multiplier is " + Multiplier.ToString() +
                        " , system has a repeater? " + RepeaterInUse.ToString() +
                        " , additional timer in DB is " + DBTimerAddon.ToString(), controller_id.ToString());
                    }
                    if(ac.NeedsReconnect && !InternalOnly)
                    {
                        // re-using, and needs to re-connect
                        connection_required = true;
                        ac.NeedsReconnect = false;
                        ac.activehost.Replace(ac.activehost.ToString(), hostname);
                        ac.activeadr.Replace(ac.activeadr.ToString(), adr);
                        ac.activecid = controller_id;
                        ac.activemultiplier = ac.controllers.Find(x => x.CID == ac.activecid).TransportTimerMultiplier;
                        ac.activeDBaddon = ac.controllers.Find(x => x.CID == ac.activecid).dbTimerAddon;
                        ac.activeRepeater = ac.controllers.Find(x => x.CID == ac.activecid).repeaterInUse;
                    }
                }

                // done manipulating dictionary/list
                _sem.Release();
            }
            catch (OperationCanceledException)
            {
                // program exiting
                return (null);
            }

            // if there is no data to be sent, we are done
            if (InternalOnly) return (ac);

            if (connection_required)
            {
                // if this host has had connection problems recently, don't even bother until
                // timeout expires
                DateTime when;
                if (LockoutTable.TryGetValue(ac.activehost.ToString(), out when))
                {
                    // this host may be locked out, see if that is the case and bail if so
                    if (DateTime.Now < when)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "199", "Not attempting connection as this controller recently failed connection attempt.", controller_id.ToString());
                        ac.acsem.Wait(_cts.Token);
                        // mark complete with error any tasks queued up
                        if (ac.queued_functions.Count > 0)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "190", "Since no connection made, ending other jobs queued for same connection.", controller_id.ToString());
                            for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                            {
                                Tuple<int, Action<ActionArgs>, ActionArgs> tf = ac.queued_functions[i];
                                if (tf.Item3.hostname == ac.activehost.ToString())
                                {
                                    ac.queued_functions.RemoveAt(i);
                                    if (tf.Item3.jobid > 0)
                                    {
#pragma warning disable 4014
                                        Misc.AddET2000Alert(_cts, tf.Item3.cid, "Unable to connect to the controller", true);
                                        LegacyTasks.CompleteLegacyJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
#pragma warning restore 4014
                                    }
                                }
                            }
                        }
                        ac.NeedsReconnect = true;
                        ac.acsem.Release();
                        // if there are still pending functions queued on this group, we need to kick start them now
                        if (ac.queued_functions.Count > 0)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "190", "Still some other jobs queued, so giving them a chance to go after no connection.", controller_id.ToString());
                            RemoveTrackingEntryIfDone(ac);
                        }
                        return (null);
                    }
                }

                // use the semaphore on this connection to hold off other messages
                // until connection completes (may be several seconds for a radio controller)
                ac.acsem.Wait(_cts.Token);
                _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Calling initial connect for" + controller_id.ToString(), controller_id.ToString());
                ac.NeedsReconnect = false;  // assume attempt will succeed, if it doesn't will get set again
                if (CallConnectSub(ac)) failed = false; 
                ac.acsem.Release();
            }
            else
            {
                // it's possible on slow connections (say radio) for connection to take several seconds
                // to establish ... when multiple commands to the same controller occur, we need to
                // make sure the first one that is establishing the connection happens before the
                // other commands try to go - otherwise, they can try and go out before the connection
                // is even made.
                bool kick = false;
                ac.acsem.Wait(_cts.Token);
                // if we didn't actually get a connection via the original attempt, we can try the connection for this call
                if (ac.TaskActive == false && (ac.sock == null || !ac.sock.Connected))
                {
                    if (ac.activehost.ToString() != hostname || (DateTime.Now - ac.lastConnectattempt).TotalSeconds > 299)
                    {
                        // don't bother trying to connect if the connection we are waiting on is the same that just failed, unless enough time has gone by
                        connection_required = true;
                        ac.activehost = new StringBuilder(hostname);
                        ac.activeadr = new StringBuilder(adr);
                        ac.activecid = controller_id;
                        ac.connection_type = connection_type;
                        ac.activemultiplier = ac.controllers.Find(x => x.CID == ac.activecid).TransportTimerMultiplier;
                        ac.activeDBaddon = ac.controllers.Find(x => x.CID == ac.activecid).dbTimerAddon;
                        ac.activeRepeater = ac.controllers.Find(x => x.CID == ac.activecid).repeaterInUse;
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Transport Timer multiplier is " + Multiplier.ToString() +
                        " , system has a repeater? " + RepeaterInUse.ToString() +
                        " , additional timer in DB is " + DBTimerAddon.ToString(), controller_id.ToString());
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "Calling secondary connect for " + controller_id.ToString(), controller_id.ToString());
                        if (CallConnectSub(ac)) failed = false;
                        else kick = true;       // couldn't connect, may be other functions still queued to move to
                    }
                    else
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "No connect call made for " + controller_id.ToString() + " as too soon or recently failed. ", controller_id.ToString());
                        kick = true;
                    }
                }
                else
                {
                    failed = false;
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "199", "No connect call for " + controller_id.ToString() + " needed as already connected. " , controller_id.ToString());
                }
                ac.acsem.Release();
                if(kick)
                {
                    if (ac.queued_functions.Count > 0)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "190", "Still some other jobs queued so giving them a chance to go after no connection.", controller_id.ToString());
                        RemoveTrackingEntryIfDone(ac);
                    }
                }
            }
            if (failed)
            {
                return (null);
            }
            return (ac);
        }

        public static bool CallConnectSub(ActiveLegacyConnection ac)
        {
            _log.WriteMessage(NLog.LogLevel.Debug, "190", "THROTTLE STATS: " + totalThrottleSets.ToString() + "," + totalThrottleFrees.ToString() + "," + (totalThrottleSets - totalThrottleFrees).ToString());
            _log.WriteMessage(NLog.LogLevel.Debug, "190", "TASKFLAG STATS: " + totalTaskActiveSets.ToString() + "," + totalTaskActiveFrees.ToString() + "," + (totalTaskActiveSets - totalTaskActiveFrees).ToString());

            // here we wait for a free semaphore entry that could throttle us down
            if (++throttleCurrentWaiters > throttleMaxWaiters)
            {
                throttleMaxWaiters = throttleCurrentWaiters;
                if (throttleMaxWaiters > 1)
                {
                    _log.WriteMessage(NLog.LogLevel.Info, "190", "Maximum throttled connections seen now " + (throttleMaxWaiters - 1).ToString());
                }
            }

            DateTime beginstamp = DateTime.Now;
            try
            {
                _log.WriteMessage(NLog.LogLevel.Debug, "190", "Current free throttles " + throttle.CurrentCount.ToString());

                LegacyConnectionTracker.totalThrottleSets++;
                // @wb I changed it to -1 for testing
                throttle.Wait(-1, comboCts.Token);   // never wait more than 5 minutes
            }
            catch (OperationCanceledException)
            {
                // someone changing throttle size, go ahead and go
            }
            throttleCurrentWaiters--;
            DateTime endstamp = DateTime.Now;

            TimeSpan semtime = endstamp - beginstamp;
            _log.WriteMessageWithId(NLog.LogLevel.Debug, "9777", "FYI: waited on throttle semaphore for " + semtime.ToString(), ac.activecid.ToString());

            if (!ConnectController(ac))
            {
                // give up semaphore
                throttle.Release();
                totalThrottleFrees++;
                // if we couldn't connect, flush some stuff
                // mark complete with error any tasks queued up
                if (ac.queued_functions.Count > 0)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "190", "Since no connection made, ending other jobs queued for same connection.", ac.activecid.ToString());
                    for (int i = ac.queued_functions.Count - 1; i >= 0; i--)
                    {
                        Tuple<int, Action<ActionArgs>, ActionArgs> tf = ac.queued_functions[i];
                        if (tf.Item3.hostname == ac.activehost.ToString())
                        {
                            ac.queued_functions.RemoveAt(i);
                            if (tf.Item3.jobid > 0)
                            {
#pragma warning disable 4014
                                Misc.AddET2000Alert(_cts, tf.Item3.cid, "Unable to connect to the controller", true);
                                LegacyTasks.CompleteLegacyJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
#pragma warning restore 4014
                            }
                        }
                    }
                }
                ac.NeedsReconnect = true;
                return (false);
            }
            return (true);
        }

        public static ActiveLegacyConnection GetTrackingEntry(String hostname, UInt32 SiteID)
        {
            ActiveLegacyConnection ac;
            // hostname is either the key, or for RF hubs, SiteID as a string
            // check in our table if pure 2000
            if (TrackingTable.TryGetValue(hostname, out ac))
            {
                return (ac);
            }
            // or possibly connected via hub
            if ((ac = HubConnectionTracker.GetTrackingEntry(hostname, SiteID)) != null)
            {
                return (ac);
            }
            if (TrackingTable.TryGetValue(SiteID.ToString(), out ac))
            {
                return (ac);
            }
            return (null);  // couldn't find it.
        }

    }
}