﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using System.Threading;



//
// our representation of a Station History Record
//
namespace Calsense.CommServer
{
    class StationHistory
    {
        public UInt32 record_start_time { get; set; }        // seconds past midnight

        // ------------------------

        //	Time stamp for the start of the first cycle.
        public UInt32 pi_first_cycle_start_time { get; set; }

        //	Time stamp when the irrigation ended.
        public UInt32 pi_last_cycle_end_time { get; set; }

        // ------------------------

        //	In seconds. A long. As a short is too ... short. May need to accomodate 24+ hours.
        public UInt32 pi_seconds_irrigated_ul { get; set; }

        // ------------------------

        // We store it here as a float in order to accumulate fractional amounts of gallons on a
        // second by second basis. We will ship it to the central as an ul_100u.
        public Single pi_gallons_irrigated_fl { get; set; }

		public Boolean pi_flow_data_has_been_stamped { get; set; }

			// -------------------------------

		public Boolean controller_turned_off { get; set; }

		public Boolean hit_stop_time { get; set; }

		public Boolean stop_key_pressed { get; set; }

		public Boolean current_short { get; set; }
		public Boolean current_none { get; set; }
		public Boolean current_low { get; set; }
		public Boolean current_high { get; set; }

		public Boolean tailends_modified_the_cycle_time { get; set; }
		public Boolean tailends_zeroed_the_irrigation_time { get; set; }

		public Boolean flow_low { get; set; }
		public Boolean flow_high { get; set; }

			// 6/5/2012 rmd : This gets stamped only for the case of programmed irrigation if the system
			// is all set to check flow and the valve is too. But never does. We won't know the exact
			// reason. Could've have too short a cycle time (as compared to say the line fill time).
			// Flow may never have gone stable. And more.
		public Boolean flow_never_checked { get; set; }

		public Boolean no_water_by_manual_prevented { get; set; }
		public Boolean no_water_by_calendar_prevented { get; set; }

		public Boolean mlb_prevented_or_curtailed { get; set; }
		public Boolean mvor_closed_prevented_or_curtailed { get; set; }

		public Boolean rain_as_negative_time_prevented_irrigation { get; set; }
		public Boolean rain_as_negative_time_reduced_irrigation { get; set; }

			// 10/12/2012 rmd : At the start time either there was an R in the table, we had crossed the
			// minimum, or we were polled to. Or during irrigation we crossed the minimum or were polled
			// to. Any of those conditions set this flag.
		public Boolean rain_table_R_M_or_Poll_prevented_or_curtailed { get; set; }

		public Boolean switch_rain_prevented_or_curtailed { get; set; }
		public Boolean switch_freeze_prevented_or_curtailed { get; set; }
		public Boolean wind_conditions_prevented_or_curtailed { get; set; }

		public Boolean mois_cause_cycle_skip { get; set; }
		public Boolean mois_max_water_day { get; set; }

			// 8/27/2012 rmd : When a MV or PUMP short is reported by a TPMicro such a short happened at
			// a POC. That poc belongs to a system. All irrigation for that system is in turn cancelled.
			// Stations in the list for ProgrammedIrrigation at the time of the short have this flag set
			// when they are removed.
		public Boolean poc_short_cancelled_irrigation { get; set; }

		public Boolean mow_day { get; set; }


			// 11/1/2013 rmd : Even if this is not a two-wire station. When there is a two-wire cable
			// anomaly (excessive current or overheated terminal) we kill all irrigation for all systems
			// at all controllers. This bit indicates this station was enveloped in that process.
        public Boolean two_wire_cable_problem { get; set; }

        // ----------
			
		// 11/19/2014 rmd : Station history lines are closed and the rip initialized at that time.
		// If we were to show the rip BEFORE the next start time water day combination we would see
		// the ZEROED line. And it is confusing. We will set this flag when the rip is valid to
		// show. And that of course would be when the start time is crossed on a water day.
        public Boolean rip_valid_to_show { get; set; }

        // ----------

        public Boolean two_wire_station_decoder_inoperative { get; set; }

        public Boolean two_wire_poc_decoder_inoperative { get; set; }

        // ------------------------

        // 6/28/2012 rmd : What group ids do we need to keep? Running reports about say the test use
        // for the turf stations belonging to a particular system over a period of time sounds
        // reasonable. So here they are.

        // 6/28/2012 rmd : The log lines need the system and the schedule GID's and that's all. The
        // station by station needs 'em all as you see below.

        public UInt16 GID_irrigation_system { get; set; }

        public UInt16 GID_irrigation_schedule { get; set; }

        // ------------------------

        //  Date associated with record start. This is ALSO the date of the programmed irrigation.
        //  BECAUSE we run these records from start time to start time. And only make a new one at
        //  the start time ON A WATERING DAY.
        public UInt16 record_start_date { get; set; }			//	Date associated with record start. Should occur each and every day!

        // ------------------------

        public UInt16 pi_first_cycle_start_date { get; set; }	//	Date stamp for the start of the first cycle

        public UInt16 pi_last_cycle_end_date { get; set; }		//	Date stamp when the irrigation ended

        // ------------------------

        // We are storing the total requested run time as minutes_us_10u. First of all we do want it
        // a short to manage storage space. And at 10u will provide 6 seconds resolution.
        public UInt16 pi_total_requested_minutes_us_10u { get; set; }

        // -----------------------------

        // 6/28/2012 rmd : Because there has been confusion about was this the amount at the start
        // time or the amount after the irrigation ended we'll keep both. Same goes for the rain.

        public UInt16 pi_left_over_at_start__minutes_10u { get; set; }

        public UInt16 pi_left_over_at_conclusion__minutes_us_10u { get; set; }

        // -----------------------------

        // Kept as a positive number. But it does represent in a sense negative time. It is time
        // that we will not be irrigating!
        public UInt16 pi_rain_at_start_time_before_working_down__minutes_10u { get; set; }

        public UInt16 pi_rain_at_start_time_after_working_down__minutes_10u { get; set; }

        // ------------------------

        // 9/16/2013 ajv : Include the last measured current as a diagnostic tool.
        // Since this is stamped at the start time, it should not be considered
        // as the current for this irrigation, but rather what it was the last time
        // it ran, similar to rain minutes.
        public UInt16 pi_last_measured_current_ma { get; set; }

        // -----------------------------

        // 6/28/2012 rmd : This group is stamped after the line fill, valve close, etc timers
        // expire. And the station is ready to check flow (or update the flow table).
        public UInt16 pi_flow_check_share_of_actual_gpm { get; set; }

        public UInt16 pi_flow_check_share_of_hi_limit_gpm { get; set; }

        public UInt16 pi_flow_check_share_of_lo_limit_gpm { get; set; }

        // -----------------------------

        // 6/28/2012 rmd : From 0 to 127 representing station 1 through 128 at a box.
        public byte station_number { get; set; }

        public byte pi_number_of_repeats { get; set; }

        // 4/18/2014 rmd : This is the 0..11 index of the box the station belongs to.
        public byte box_index_0 { get; set; }

        public Int16 pi_moisture_balance_percentage_after_schedule_completes_100u { get; set; }

        public UInt16 expansion_u16 { get; set; }
    }
    class StationHistoryList
    {
        public IList<StationHistory> records;
        public StationHistoryList()
        {
            records = new List<StationHistory>();
        }
        public void LogIt(MyLogger l, NLog.LogLevel lev)
        {
#if LOGGING_STATION_HISTORY
            int i;

            l.WriteMessage(lev, "8001", "-----Dumping Structure from Station History Message-----");
            l.WriteMessage(lev, "8001", "System GID:" + this.system_gid.ToString());
            for (i = 0; i < this.records.Count; i++ )
            {
                l.WriteMessage(lev, "8001", " ** Record " + i.ToString() + " ** ");
                l.WriteMessage(lev, "8001", "Time:" + this.records[i].time.ToString());
                l.WriteMessage(lev, "8001", "Date:" + this.records[i].date.ToString());
                l.WriteMessage(lev, "8001", " Station Number:" + this.records[i].station_num.ToString());
                l.WriteMessage(lev, "8001", " Box Index0:" + this.records[i].box_index_0.ToString());
                l.WriteMessage(lev, "8001", " Expected Flow:" + this.records[i].expected_flow.ToString());
                l.WriteMessage(lev, "8001", " Derated Expected Flow:" + this.records[i].derated_expected_flow.ToString());
                l.WriteMessage(lev, "8001", " High Limit:" + this.records[i].hi_limit.ToString());
                l.WriteMessage(lev, "8001", " Low Limit:" + this.records[i].lo_limit.ToString());
                l.WriteMessage(lev, "8001", " Portion of actual:" + this.records[i].portion_of_actual.ToString());
                l.WriteMessage(lev, "8001", " Flow check status:" + this.records[i].flow_check_status.ToString());

                l.WriteMessage(lev, "8001", " indicies_out_of_range: " + this.records[i].FRF_NO_CHECK_REASON_indicies_out_of_range.ToString());
                l.WriteMessage(lev, "8001", " station_cycles_too_low: " + this.records[i].FRF_NO_CHECK_REASON_station_cycles_too_low.ToString());
                l.WriteMessage(lev, "8001", " cell_iterations_too_low: " + this.records[i].FRF_NO_CHECK_REASON_cell_iterations_too_low.ToString());
                l.WriteMessage(lev, "8001", " no_flow_meter: " + this.records[i].FRF_NO_CHECK_REASON_no_flow_meter.ToString());
                l.WriteMessage(lev, "8001", " not_enabled_by_user_setting: " + this.records[i].FRF_NO_CHECK_REASON_not_enabled_by_user_setting.ToString());
                l.WriteMessage(lev, "8001", " acquiring_expected: " + this.records[i].FRF_NO_CHECK_REASON_acquiring_expected.ToString());
                l.WriteMessage(lev, "8001", " combo_not_allowed_to_check: " + this.records[i].FRF_NO_CHECK_REASON_combo_not_allowed_to_check.ToString());
                l.WriteMessage(lev, "8001", " unstable_flow: " + this.records[i].FRF_NO_CHECK_REASON_unstable_flow.ToString());
                l.WriteMessage(lev, "8001", " cycle_time_too_short: " + this.records[i].FRF_NO_CHECK_REASON_cycle_time_too_short.ToString());
                l.WriteMessage(lev, "8001", " irrigation_restart: " + this.records[i].FRF_NO_CHECK_REASON_irrigation_restart.ToString());
                l.WriteMessage(lev, "8001", " not_supposed_to_check: " + this.records[i].FRF_NO_CHECK_REASON_not_supposed_to_check.ToString());
                l.WriteMessage(lev, "8001", " zero_flow_rate: " + this.records[i].FRF_NO_UPDATE_REASON_zero_flow_rate.ToString());
                l.WriteMessage(lev, "8001", " thirty_percent: " + this.records[i].FRF_NO_UPDATE_REASON_thirty_percent.ToString());
            }
#endif
        }
    }

}
