﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Calsense.Logging;
using ServerApp;
using System.Collections.Concurrent;
using System.Runtime.ExceptionServices;

namespace Calsense.CommServer
{
    public class Server : IDisposable
    {
        public readonly int Port;
        private readonly TcpListener _tcpListener;
        private readonly Task _listenTask;
        public string ServerName = "Calsense.Master.CommServer";
        public bool IsRunning { get; private set; }
        Socket _nextSocket;
        private MyLogger _logger;

        public Server(int port, CancellationTokenSource cts, ConcurrentBag<Task>tl)
            : this(IPAddress.Any, port, cts, tl)
        {
        }

        public Server(IPAddress listeningAddress, int port, CancellationTokenSource cts, ConcurrentBag<Task>tl)
        {
            Port = port;

            _logger = new MyLogger(this.GetType().Name, "", "", Constants.CS3000Model);

            AppDomain.CurrentDomain.FirstChanceException += FirstChanceHandler;

            IsRunning = true;
            _tcpListener = null;
            _listenTask = null;

            ServerSocketTracker.SetCts(cts);    // tell the static socket tracker what the cancellation token is
            PeriodicTasks.SetCts(cts);          // tell periodic tasks what the cancellation token is
            LegacyTasks.SetCts(cts);            // tell legacy controller tasks what the cancellation token is

            // Start listening
            _logger.WriteMessage(NLog.LogLevel.Debug, "2001", "Starting...");
            _tcpListener = new TcpListener(listeningAddress, Port);
            _tcpListener.Start();
            _logger.WriteMessage(NLog.LogLevel.Debug, "2002", "Started.");
            // Start a background thread to listen for incoming
            _listenTask = Task.Factory.StartNew(() =>
                {
                    ListenLoop(cts, tl);
                }
                );
        }

        private void ListenLoop(CancellationTokenSource cts, ConcurrentBag<Task>tl)
        {
            // get access to runtime control settings via singleton
            WebFormData wf = WebFormData.Instance;

            if (System.Threading.Thread.CurrentThread.Name == null)
            {
                System.Threading.Thread.CurrentThread.Name = "Connection Lstnr";
            }

            while (_tcpListener.Server.IsBound && !cts.IsCancellationRequested)
            {

                try
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "2003", "Awaiting connection...");
                    // Wait for connection
                    _nextSocket = null;
                    //_nextSocket = await _tcpListener.AcceptSocketAsync().ConfigureAwait(false);
                    _nextSocket = _tcpListener.AcceptSocket();
                    if (_nextSocket == null)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "2020", "NULL Socket returned from AcceptSocket??");
                        break;
                    }

                    if (wf.IgnoreMode)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "2009", "Ignoring inbound request from " + IPAddress.Parse(((IPEndPoint)_nextSocket.RemoteEndPoint).Address.ToString()) + " because Ignore Mode enabled.");
                        continue;
                    }
                    
                    CancellationTokenSource _child_cts = CancellationTokenSource.CreateLinkedTokenSource(cts.Token);

                    // Got new connection, create a client handler for it
                    _logger.WriteMessage(NLog.LogLevel.Info, "2004", "Connection made from " + IPAddress.Parse(((IPEndPoint)_nextSocket.RemoteEndPoint).Address.ToString()) +
                        " on remote port: " + ((IPEndPoint)_nextSocket.RemoteEndPoint).Port.ToString());
                    var client = new ServerInternal(this, _nextSocket, _child_cts);

                    // Create a task to handle new connection
                    if(tl != null){     // if someone keeping a list of tasks active, add created task to list
                        tl.Add(Task.Factory.StartNew(client.Do));
                    }
                    else {              // otherwise just create the task
                        Task.Factory.StartNew(client.Do);
                    }
                }
                catch (SocketException e)
                {
                    if (e.SocketErrorCode == SocketError.Interrupted)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Debug, "2005", "Socket terminating normally.");

                        break;
                    }
                    else throw;
                }
                finally
                {

                }
            }

        }

        public void Dispose()
        {
            if (_tcpListener != null)
            {
                _logger.WriteMessage(NLog.LogLevel.Debug, "2009", "Stopping...");
                _tcpListener.Stop();
                _logger.WriteMessage(NLog.LogLevel.Debug, "2010", "Stopped.");
            }
        }
        private volatile bool _insideFirstChanceExceptionHandler = false; 
        private void FirstChanceHandler(object source, FirstChanceExceptionEventArgs e)
        {
               if (_insideFirstChanceExceptionHandler)
                {
                    // Prevent recursion if an exception is thrown inside this method
                    return;
                }
                _insideFirstChanceExceptionHandler = true;
                if (e.Exception.Message != null)
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "2017", "Exception is being generated (could be normal). Desc: " + e.Exception.Message.Substring(0, Math.Min(e.Exception.Message.Length - 1, 120)));
                }
                if (e.Exception.StackTrace != null)
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "2017", "Stack trace: " + e.Exception.StackTrace.Substring(0, Math.Min(e.Exception.StackTrace.Length - 1, 120)));
                }

                _insideFirstChanceExceptionHandler = false;

        }
    }
}
