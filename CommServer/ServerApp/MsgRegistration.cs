﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using Advantage.Data.Provider;



namespace Calsense.CommServer
{

    public class MsgRegistration
    {
        // test call unmanged C code
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint Dummy(ref DUMMY_STRUCT pp,
        uint why,
        [In, Out] byte[] dest,
        uint sz,
        uint idx);


        //
        // Parses Registration Message inbond message from controller
        // returns via Tuple:
        //   List of byte arrays, which are 0 to n messages to send as a response
        //   String that will be passed back in if this connection stays up
        //   Boolean as a flag to terminate connection or not
        //   int as an internal state to track thru session (if needed)
        //
        public static async Task<Tuple<List<byte[]>, String, bool, int>> Parse(ClientInfoStruct client, CancellationTokenSource cts)
        {
            uint i;
            int j;
            int ci;
            int cc;
            byte[] data;
            MyLogger _logger;
            uint size;
            byte[] Buffer = null;
            bool abort = true;
            
            int hub_type = 0;
            bool hubChange = false;

            int offset;

            UInt16 _poflags;
            UInt32 _temp32;
            Controller c = new Controller();
            Controller db_c = new Controller();

            _logger = new MyLogger(typeof(MsgRegistration).Name, Misc.ClientLogString(client), Misc.ClientLogDB(client), Constants.CS3000Model);
            _logger.WriteMessage(NLog.LogLevel.Info, "3004", "Processing registration message...");

            size = client.rxlen;
            Buffer = client.rx;

            offset = 0;

            // structure version 3 is the original
            // structure version 4 adds 
            // UNS_32		expansion[ 4 ];
            // to the end of CHAIN_MEMBERS_SHARED_STRUCT
            try
            {
                c.structure_version = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);

                // check for newer version than we support
                if (c.structure_version > 6)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "3667", "Version of registration structure higher than 5, unsupported by commserver.");
                    List<byte[]> nulllist = new List<byte[]>();
                    return (Tuple.Create(nulllist, (String)null, true, 0));
                }

                c.time = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                c.date = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                c.code_revision_string = Misc.NetworkBytesToString(Buffer, offset, Constants.CODE_REV_STR_LEN); offset += Constants.CODE_REV_STR_LEN;
                c.TP_code_revision_string = Misc.NetworkBytesToString(Buffer, offset, Constants.CODE_REV_STR_LEN); offset += Constants.CODE_REV_STR_LEN;

                for (j = 0, ci = 0; j < Constants.MAX_CHAIN_LENGTH; j++)
                {

                    // if this controller is not seen then skip past its data area...read the saw during scan boolean
                    _temp32 = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                    if (_temp32 == 0)
                    {
                        // for structures before than 6 extract the Box Names from the box size.
                        offset += (c.structure_version < 6 ? Constants.NUMBER_OF_CHARS_IN_A_NAME : 0) + (sizeof(UInt32) * 18);
                        if (c.structure_version > 3)
                        {
                            offset += Constants.REGVER4_SIZE_DELTA_PER_CONTROLLER;
                        }
                    }
                    // active controller area, load up a controller object for our use
                    else
                    {
                        // we will use variable "ci" to index controllers we add as
                        // they may not be contiguously present in the array reported in registration msg
                        c.details.Add(new ControllerDetails());
                        c.details[ci].saw_during_scan = true;

                        // for structures before than 6 extract the Box Names but don't store it
                        if (c.structure_version < 6) offset += Constants.NUMBER_OF_CHARS_IN_A_NAME;

                        c.details[ci].serial_number = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                        _poflags = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                        c.details[ci].option_FL = ((_poflags & Constants.option_fl_bit) != 0) ? true : false;
                        c.details[ci].option_SSE = ((_poflags & Constants.option_SSE_bit) != 0) ? true : false;
                        c.details[ci].option_SSE_D = ((_poflags & Constants.option_SSE_D_bit) != 0) ? true : false;
                        c.details[ci].option_HUB = ((_poflags & Constants.option_HUB_bit) != 0) ? true : false;

                        // EXTRA 2 BYTES OF PADDING in MESSAGE - eat it
                        _poflags = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);

                        // card array
                        for (i = 0; i < Constants.STATION_CARD_COUNT; i++)
                        {
                            _temp32 = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                            c.details[ci].i2c_cards.Add(new I2C_Card_Details(i, _temp32 & Constants.card_present_bit, _temp32 & Constants.tb_present_bit));
                        }

                        // poc
                        _temp32 = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                        c.details[ci].poc.Initialize(0, _temp32 & Constants.card_present_bit, _temp32 & Constants.tb_present_bit);

                        // lights
                        _temp32 = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                        c.details[ci].lights.Initialize(0, _temp32 & Constants.card_present_bit, _temp32 & Constants.tb_present_bit);

                        // misc installed
                        _temp32 = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                        c.details[ci].weather_card_present = (_temp32 != 0) ? true : false;
                        _temp32 = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                        c.details[ci].weather_terminal_present = (_temp32 != 0) ? true : false;
                        _temp32 = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                        c.details[ci].dash_m_card_present = (_temp32 != 0) ? true : false;
                        _temp32 = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                        c.details[ci].dash_m_terminal_present = (_temp32 != 0) ? true : false;
                        _temp32 = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                        c.details[ci].dash_m_card_type = (_temp32 != 0) ? true : false;
                        _temp32 = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                        c.details[ci].two_wire_terminal_present = (_temp32 != 0) ? true : false;

                        // device idx's
                        c.details[ci].port_A_device_index = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);
                        c.details[ci].port_B_device_index = Misc.NetworkBytesToUint32(Buffer, offset); offset += sizeof(UInt32);

                        c.details[ci].box_index = (uint)j; // box index stores the original index into array of this set of data

                        // hub type can be determined based on port B index (SR or LR) 
                        // hub option should be in the first box in this chain.
                        if (c.details[ci].option_HUB && ci == 0)
                        {
                            if (c.details[ci].port_B_device_index == 1 || c.details[ci].port_B_device_index == 2) // 1 or 2 is LR 
                            {
                                hub_type = Constants.Hubs.CS3000_LR_Hub;
                            }
                            else if (c.details[ci].port_B_device_index == 3) // 3 is SR
                            {
                                hub_type = Constants.Hubs.CS3000_SR_Hub;
                            }
                        }

                        // bump index
                        ci++;

                        // for structures later than 3 account for extra bytes 
                        if (c.structure_version > 3)
                        {
                            offset += Constants.REGVER4_SIZE_DELTA_PER_CONTROLLER;
                        }
                    }
                }

                // 9/4/2015 rmd : Indication to comm server to clear the pdata associated with this
                // controller. Maybe also clears report data. Added in the transition from revision 4 to
                // 5.
                if (c.structure_version > 4)
                {
                    c.commserver_to_clear_data = (Buffer[offset] != 0); offset++;
                }
            }
            catch (Exception e)
            {
                // we don't expect any exceptions pulling out registration data, if so, log and return error
                _logger.WriteMessage(NLog.LogLevel.Error, "3000", "Exception parsing Registration Data: " + e.ToString());
                List<byte[]> nulllist = new List<byte[]>();

                return (Tuple.Create(nulllist, (String)null, true, 0));
            }
            //c.post_test_string = Misc.NetworkBytesToString(Buffer, offset, Constants.TEST_STRING_LEN); offset += Constants.TEST_STRING_LEN;

#if DUMPRAWDATA
            _logger.WriteMessage(NLog.LogLevel.Info, "3004", "Dump of bytes raw:" + size.ToString());
            for (i = 0, j = (int)size; i < size; i += 8)
            {
                if (j >= 8)
                {
                    s = Buffer[i].ToString("x2") + " " + Buffer[i + 1].ToString("x2") + " " + Buffer[i + 2].ToString("x2") + " " + Buffer[i + 3].ToString("x2") + " " + Buffer[i + 4].ToString("x2") + " " + Buffer[i + 5].ToString("x2") + " " + Buffer[i + 6].ToString("x2") + " " + Buffer[i + 7].ToString("x2");
                    j -= 8;
                }
                else
                {
                    s = "";
                    while (j > 0)
                    {
                        s = s + Buffer[i].ToString("x2") + " ";
                        j--;
                        i++;
                    }
                }
                _logger.WriteMessage(NLog.LogLevel.Debug, "8001", s);
                if (j <= 0) break;
                Thread.Sleep(10);       // allow UDP logger to catch up
            }
#endif

#if TEST_CLONING_COMPARISON
            // test controller comparison
            clone = c.Clone();
            if(clone == c)
            {
                _logger.WriteMessage(NLog.LogLevel.Info, "3011", "Cloned controller is equivalent as expected :)");
                clone.details[5].i2c_cards[2].tb_present = true;
                if (clone == c)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "3011", "Cloned controller is same!");
                    clone.LogIt(_logger, NLog.LogLevel.Error);
                }
            }
            else
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "3011", "Cloned controller is different!");
                clone.LogIt(_logger, NLog.LogLevel.Error);

            }
#endif

            // log our interpretation of the registration data
            c.LogIt(_logger, NLog.LogLevel.Info);

            // check for case of info with no controllers, board with no TP board etc
            // ignore these
            if (c.details.Count == 0)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "3066", "No controllers in registration message, ignoring");

                List<byte[]> nulllist = new List<byte[]>();

                return (Tuple.Create(nulllist, (String)null, true, 0));
            }

            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                // see if we already have an entries

                if (client.nid == 0)
                {
                    var cmd = new AdsCommand("select isnull(NetworkID, 0) from CS3000Networks where MasterSerialNumber = " + client.sn.ToString(), conn);
                    _logger.WriteMessage(NLog.LogLevel.Debug, "3050", "SQL: " + cmd.CommandText);
                    var existsID = cmd.ExecuteScalar();
                    if (existsID != null)
                    {
                        if (Convert.ToUInt32(existsID) > 0)
                        {
                            // reuse network id that is already in place
                            client.nid = Convert.ToUInt32(existsID);
                            _logger.WriteMessage(NLog.LogLevel.Warn, "3085", "Registration message received with 0 Network ID already in DB as Network ID:" + existsID.ToString());
                        }
                    }
                    cmd.Unprepare();
                }

                if (client.nid != 0)
                {
                    var cmd = new AdsCommand("select isnull(NetworkID, 0) from CS3000Networks where MasterSerialNumber = " + client.sn.ToString(), conn);
                    _logger.WriteMessage(NLog.LogLevel.Debug, "3052", "SQL: " + cmd.CommandText);
                    var existsID = cmd.ExecuteScalar();
                    cmd.Unprepare();
                    if (existsID == null)
                    {
                        // we ACK this case:
                        _logger.WriteMessage(NLog.LogLevel.Error, "3083", "Registration with non-zero Network ID is not in DB. SN:" + client.sn.ToString() + ", nid:" + client.nid.ToString());

                        // set nid to zero to accept the registration message
                        client.nid = 0;
                        _logger.WriteMessage(NLog.LogLevel.Warn, "3086", "Change the Network ID to zero to accept the registration message");
                    }
                    else
                    {
                        uint db_nid = uint.Parse(existsID.ToString());

                        if (db_nid != client.nid)
                        {
                            // we ACK this case
                            _logger.WriteMessage(NLog.LogLevel.Error, "3083", "Registration with non-zero Network ID that does not match what is already in DB. SN:" + client.sn.ToString() + ", nid:" + client.nid.ToString());

                            // change back the network id
                            client.nid = db_nid;
                            _logger.WriteMessage(NLog.LogLevel.Warn, "3086", "Change the Network ID to match what is in DB. nid:" + client.nid.ToString());
                        }
                        
                        {
                            // update database to contents of this new registration message
                            _logger.WriteMessage(NLog.LogLevel.Info, "3020", "Controller registration with existing SN or networkID, updating rows into DB...");
                            // NOTE: Transactions not supported on local server, so ignore that exception
                            // insert data into DB and retrieve new network id to assign to controller
                            AdsTransaction txn2 = null;

                            try
                            {
                                txn2 = conn.BeginTransaction();
                            }
                            catch (System.NotSupportedException)
                            {

                            }
                            do
                            {
                                try
                                {
                                    // ControllerSites already should be good, and the SiteID is our networkID btw
                                    // CS3000Systems is already good too, with our NetworkID
                                    // CS3000Networks already okay too.

                                    int? oldRecord = 0;
                                    int? oldCID = 0;
                                    int newCID;
                                    bool still_present;
                                    int matched_idx = 0;

                                    for (cc = 0; cc < Constants.MAX_CHAIN_LENGTH; cc++)
                                    {
                                        // scan thru the reported boxes to see if this box index is reported
                                        for (ci = 0, still_present = false; ci < c.details.Count; ci++)
                                        {
                                            if (c.details[ci].box_index == cc)
                                            {
                                                still_present = true;
                                                matched_idx = ci;
                                            }
                                        }
                                        // check for controllers going away
                                        if (still_present == false)
                                        {
                                            // query for networkid, boxindex = cc - if found, mark not present as box_index = cc no longer in registration msg
                                            cmd = new AdsCommand("select ControllerID from CS3000Controllers where NetworkID = " + client.nid.ToString() + " and BoxIndex = " + cc.ToString(), conn, txn2);
                                            _logger.WriteMessage(NLog.LogLevel.Debug, "3076", "SQL: " + cmd.CommandText);
                                            oldCID = (int?)cmd.ExecuteScalar();
                                            cmd.Unprepare();
                                            if (oldCID != null)
                                            {
                                                if ((int)oldCID > 0)
                                                {
                                                    // there is a Controller that used to be attached that should now be marked deleted
                                                    cmd = new AdsCommand("update Controllers set Deleted = TRUE where ControllerID = " + oldCID.ToString() + " and SiteID = " + client.nid.ToString(), conn, txn2);
                                                    _logger.WriteMessage(NLog.LogLevel.Debug, "3076", "SQL: " + cmd.CommandText);
                                                    cmd.ExecuteNonQuery();
                                                    cmd.Unprepare();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            // box_index = cc is present in registration
                                            // array index = matched_idx is the item that has that box_index

                                            // see if this box index already exists in DB, in which case we will do an update
                                            // otherwise we will do an insert for a new controller.
                                            cmd = new AdsCommand("select (1) from CS3000Controllers where NetworkID = " + client.nid.ToString() + " and BoxIndex = " + c.details[matched_idx].box_index.ToString(), conn, txn2);

                                            _logger.WriteMessage(NLog.LogLevel.Debug, "3076", "SQL: " + cmd.CommandText);
                                            oldRecord = (int?)cmd.ExecuteScalar();
                                            cmd.Unprepare();

                                            if (oldRecord == null)   // nothing from DB
                                            {
                                                // no pre-existing controller for this network/boxid - insert all new stuff.
                                                // insert into old Controllers table
                                                cmd = new AdsCommand("insert into Controllers(SiteID, ControllerName, ConnectionType, Installed, CompanyID, Model, SoftwareVersion,TP_Version) " +
                                                    " values(" + client.nid.ToString() + ",'This Controller (not yet named)',0,'" +
                                                    DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "', -1, 7,'" + c.structure_version.ToString() + ' ' + c.code_revision_string + "','" + c.TP_code_revision_string + "')", conn, txn2);
                                                _logger.WriteMessage(NLog.LogLevel.Debug, "3076", "SQL: " + cmd.CommandText);
                                                cmd.ExecuteNonQuery();
                                                newCID = cmd.LastAutoinc;   // ControllerID was just generated
                                                cmd.Unprepare();
                                                _logger.WriteMessage(NLog.LogLevel.Info, "3059", "New ControllerID generated is " + newCID.ToString());

                                                cmd = new AdsCommand("insert into CS3000Controllers(ControllerID, NetworkID, Name, SerialNumber, FLOption, SSEOption, SSE_DOption, "
                                                                        + "WeatherCard, WeatherTerminal, DashM_Card, DashM_Terminal, DashM_CardType, TwoWireTerminal, PortA_DeviceIndex, PortB_DeviceIndex, BoxIndex, Hub_Option) values("
                                                                        + newCID.ToString() + "," + client.nid.ToString() + ",'This Controller (not yet named)',"
                                                                        + c.details[matched_idx].serial_number.ToString() + "," + c.details[matched_idx].option_FL.ToString() + ","
                                                                        + c.details[matched_idx].option_SSE.ToString() + "," + c.details[matched_idx].option_SSE_D.ToString() + ","
                                                                        + c.details[matched_idx].weather_card_present.ToString() + "," + c.details[matched_idx].weather_terminal_present.ToString() + ","
                                                                        + c.details[matched_idx].dash_m_card_present.ToString() + "," + c.details[matched_idx].dash_m_terminal_present.ToString() + ","
                                                                        + c.details[matched_idx].dash_m_card_type.ToString() + "," + c.details[matched_idx].two_wire_terminal_present.ToString() + ","
                                                                        + c.details[matched_idx].port_A_device_index.ToString() + "," + c.details[matched_idx].port_B_device_index.ToString() + "," +
                                                                        c.details[matched_idx].box_index.ToString() + "," + c.details[matched_idx].option_HUB.ToString()
                                                                        + ")", conn, txn2);
                                                _logger.WriteMessage(NLog.LogLevel.Debug, "3053", "SQL: " + cmd.CommandText);
                                                cmd.ExecuteNonQuery();
                                                cmd.Unprepare();

                                                // for each controller add its cards to CS3000Cards
                                                for (j = 0; j < Constants.STATION_CARD_COUNT; j++)
                                                {
                                                    cmd = new AdsCommand("insert into CS3000Cards(Slot, CardPresent, TBPresent, ControllerSN, NetworkID, BoxIndex) values(" +
                                                        c.details[matched_idx].i2c_cards[j].idx.ToString() + ","
                                                        + c.details[matched_idx].i2c_cards[j].card_present.ToString() + ","
                                                        + c.details[matched_idx].i2c_cards[j].tb_present.ToString() + ","
                                                        + c.details[matched_idx].serial_number.ToString() + ","
                                                        + client.nid.ToString() + ","
                                                        + c.details[matched_idx].box_index.ToString() + ")", conn, txn2);
                                                    _logger.WriteMessage(NLog.LogLevel.Debug, "3054", "SQL: " + cmd.CommandText);
                                                    cmd.ExecuteNonQuery();
                                                    cmd.Unprepare();
                                                }
                                                // add POC card
                                                cmd = new AdsCommand("insert into CS3000Cards(Slot, CardPresent, TBPresent, ControllerSN, NetworkID, BoxIndex) values(" +
                                                    Constants.POC_SLOT_NUMBER.ToString() + ","
                                                    + c.details[matched_idx].poc.card_present.ToString() + ","
                                                    + c.details[matched_idx].poc.tb_present.ToString() + ","
                                                    + c.details[matched_idx].serial_number.ToString() + ","
                                                    + client.nid.ToString() + ","
                                                    + c.details[matched_idx].box_index.ToString() + ")", conn, txn2);
                                                _logger.WriteMessage(NLog.LogLevel.Debug, "3055", "SQL: " + cmd.CommandText);
                                                cmd.ExecuteNonQuery();
                                                cmd.Unprepare();

                                                // add lights card
                                                cmd = new AdsCommand("insert into CS3000Cards(Slot, CardPresent, TBPresent, ControllerSN, NetworkID, BoxIndex) values(" +
                                                    Constants.LIGHT_SLOT_NUMBER.ToString() + ","
                                                    + c.details[matched_idx].lights.card_present.ToString() + ","
                                                    + c.details[matched_idx].lights.tb_present.ToString() + ","
                                                    + c.details[matched_idx].serial_number.ToString() + ","
                                                    + client.nid.ToString() + ","
                                                    + c.details[matched_idx].box_index.ToString() + ")", conn, txn2);
                                                _logger.WriteMessage(NLog.LogLevel.Debug, "3056", "SQL: " + cmd.CommandText);
                                                cmd.ExecuteNonQuery();
                                                cmd.Unprepare();

                                            }
                                            else
                                            {
                                                // there already is a CS3000Controllers row, and presumably Controllers row for this unit.
                                                // update them to the new contents.

                                                // get the controller ID for existing controller based on box index and network ID
                                                cmd = new AdsCommand("select ControllerID from CS3000Controllers where NetworkID = " + client.nid.ToString() +
                                                " AND BoxIndex= " + c.details[matched_idx].box_index.ToString(), conn, txn2);
                                                _logger.WriteMessage(NLog.LogLevel.Debug, "3056", "SQL: " + cmd.CommandText);
                                                newCID = (int)cmd.ExecuteScalar();
                                                cmd.Unprepare();

                                                // update the Controllers data
                                                cmd = new AdsCommand("update Controllers set Deleted = FALSE, SoftwareVersion='" + c.structure_version.ToString() + ' ' + c.code_revision_string  +
                                                    "', TP_Version='"      + c.TP_code_revision_string + "' " + 
                                                    " where SiteID = " + client.nid.ToString() + " and ControllerID = " + newCID.ToString() , conn, txn2);
                                                _logger.WriteMessage(NLog.LogLevel.Debug, "3076", "SQL: " + cmd.CommandText);
                                                cmd.ExecuteNonQuery();
                                                cmd.Unprepare();

                                                // update CS3000Controllers data
                                                cmd = new AdsCommand("update CS3000Controllers set FLOption = " + c.details[matched_idx].option_FL.ToString() +
                                                    ", SSEOption = " + c.details[matched_idx].option_SSE.ToString() +
                                                    ", SSE_DOption = " + c.details[matched_idx].option_SSE_D.ToString() +
                                                    ", Hub_Option = " + c.details[matched_idx].option_HUB.ToString() +
                                                    ", WeatherCard = " + c.details[matched_idx].weather_card_present.ToString() +
                                                    ", WeatherTerminal = " + c.details[matched_idx].weather_terminal_present.ToString() +
                                                    ", DashM_Card = " + c.details[matched_idx].dash_m_card_present.ToString() +
                                                    ", DashM_Terminal = " + c.details[matched_idx].dash_m_terminal_present.ToString() +
                                                    ", DashM_CardType = " + c.details[matched_idx].dash_m_card_type.ToString() +
                                                    ", TwoWireTerminal = " + c.details[matched_idx].two_wire_terminal_present.ToString() +
                                                    ", PortA_DeviceIndex = " + c.details[matched_idx].port_A_device_index.ToString() +
                                                    ", PortB_DeviceIndex = " + c.details[matched_idx].port_B_device_index.ToString() +
                                                    ", SerialNumber = " + c.details[matched_idx].serial_number.ToString() +
                                                    " where BoxIndex = " + c.details[matched_idx].box_index.ToString() +
                                                    " and NetworkID = " + client.nid.ToString(), conn, txn2);
                                                _logger.WriteMessage(NLog.LogLevel.Debug, "3053", "SQL: " + cmd.CommandText);
                                                cmd.ExecuteNonQuery();
                                                cmd.Unprepare();

                                                // update the cards data
                                                // for each controller add its cards to CS3000Cards
                                                for (j = 0; j < Constants.STATION_CARD_COUNT; j++)
                                                {
                                                    cmd = new AdsCommand("update CS3000Cards set CardPresent = " + c.details[matched_idx].i2c_cards[j].card_present.ToString() +
                                                        ", TBPresent = " + c.details[matched_idx].i2c_cards[j].tb_present.ToString() +
                                                        ", ControllerSN = " + c.details[matched_idx].serial_number.ToString() +
                                                        " where Slot = " + c.details[matched_idx].i2c_cards[j].idx.ToString() +
                                                        " and NetworkID = " + client.nid.ToString() +
                                                        " and BoxIndex = " + c.details[matched_idx].box_index.ToString(),
                                                        conn, txn2);
                                                    _logger.WriteMessage(NLog.LogLevel.Debug, "3054", "SQL: " + cmd.CommandText);
                                                    cmd.ExecuteNonQuery();
                                                    cmd.Unprepare();
                                                }
                                                // update POC card
                                                cmd = new AdsCommand("update CS3000Cards set CardPresent = " + c.details[matched_idx].poc.card_present.ToString() +
                                                    ", TBPresent = " + c.details[matched_idx].poc.tb_present.ToString() +
                                                    ", ControllerSN = " + c.details[matched_idx].serial_number.ToString() +
                                                    " where Slot = " + Constants.POC_SLOT_NUMBER.ToString() +
                                                    " and NetworkID = " + client.nid.ToString() +
                                                    " and BoxIndex = " + c.details[matched_idx].box_index.ToString(),
                                                    conn, txn2);
                                                _logger.WriteMessage(NLog.LogLevel.Debug, "3055", "SQL: " + cmd.CommandText);
                                                cmd.ExecuteNonQuery();
                                                cmd.Unprepare();

                                                // update lights card
                                                cmd = new AdsCommand("update CS3000Cards set CardPresent = " + c.details[matched_idx].lights.card_present.ToString() +
                                                    ", TBPresent = " + c.details[matched_idx].lights.tb_present.ToString() +
                                                    ", ControllerSN = " + c.details[matched_idx].serial_number.ToString() +
                                                    " where Slot = " + Constants.LIGHT_SLOT_NUMBER.ToString() +
                                                    " and NetworkID = " + client.nid.ToString() +
                                                    " and BoxIndex = " + c.details[matched_idx].box_index.ToString(),
                                                    conn, txn2);
                                                _logger.WriteMessage(NLog.LogLevel.Debug, "3056", "SQL: " + cmd.CommandText);
                                                cmd.ExecuteNonQuery();
                                                cmd.Unprepare();
                                            }

                                        }
                                    }

                                    if (c.structure_version > 4)
                                    {
                                        if (c.commserver_to_clear_data)
                                        {
                                            _logger.WriteMessage(NLog.LogLevel.Info, "3065", "Controller reset issued, deleting program data from database....");
                                            await Misc.AddAlert(cts, client.nid, "Controller reset issued, program data deleted from cloud database", false, true);
                                            cmd = new AdsCommand("EXECUTE PROCEDURE DeletePData(" + client.nid.ToString() + ");", conn, txn2);
                                            _logger.WriteMessage(NLog.LogLevel.Debug, "14357", "SQL: " + cmd.CommandText);
                                            cmd.ExecuteNonQuery();
                                            cmd.Unprepare();
                                        }
                                    }

                                    // set hub type in CS3000Networks table
                                    cmd = new AdsCommand("SELECT ISNULL(UseHubType,0) FROM CS3000Networks n join controllersites s on s.siteid=n.networkid  WHERE n.NetworkID=" + client.nid.ToString(), conn, txn2);
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "3076", "SQL: " + cmd.CommandText);
                                    var useHubTypeIndb = (int?)cmd.ExecuteScalar();
                                    cmd.Unprepare();

                                    if (useHubTypeIndb != null)
                                    {
                                        if (hub_type == Constants.Hubs.CS3000_LR_Hub ||
                                            hub_type == Constants.Hubs.CS3000_SR_Hub)
                                        {
                                            // controller is a hub update UseHubType column
                                            cmd = new AdsCommand("UPDATE CS3000Networks SET UseHubType=" + hub_type.ToString() + ",HubID=" + client.nid.ToString() +
                                                " WHERE NetworkID=" + client.nid.ToString(), conn, txn2);
                                            _logger.WriteMessage(NLog.LogLevel.Debug, "14357", "SQL: " + cmd.CommandText);
                                            cmd.ExecuteNonQuery();
                                            cmd.Unprepare();

                                            if (useHubTypeIndb != hub_type) hubChange = true;
                                        }
                                        else // controller is not a hub
                                        {

                                            if (useHubTypeIndb == Constants.Hubs.CommunicateUsingCS3000_LR_Hub ||
                                                useHubTypeIndb == Constants.Hubs.CommunicateUsingCS3000_SR_Hub ||
                                                useHubTypeIndb == Constants.Hubs.NoHubInvolved)
                                            {
                                                // we're okay, don't change the settings.
                                            }
                                            else if (useHubTypeIndb == Constants.Hubs.CS3000_LR_Hub ||
                                                     useHubTypeIndb == Constants.Hubs.CS3000_SR_Hub)
                                            {
                                                // controller was a hub, update settings
                                                cmd = new AdsCommand("UPDATE CS3000Networks SET UseHubType=0,HubID=0 WHERE NetworkID=" + client.nid.ToString(), conn, txn2);
                                                _logger.WriteMessage(NLog.LogLevel.Debug, "14357", "SQL: " + cmd.CommandText);
                                                cmd.ExecuteNonQuery();
                                                cmd.Unprepare();

                                                hubChange = true;
                                            }
                                            else
                                            {
                                                // error in UseHubType setting correct it. we shouldn't see it!!
                                                _logger.WriteMessage(NLog.LogLevel.Error, "14353", "Incorrect setting for UseHubType in the db, set it back to Zero from " + useHubTypeIndb.ToString());

                                                cmd = new AdsCommand("UPDATE CS3000Networks SET UseHubType=0,HubID=0 WHERE NetworkID=" + client.nid.ToString(), conn, txn2);
                                                _logger.WriteMessage(NLog.LogLevel.Debug, "14357", "SQL: " + cmd.CommandText);
                                                cmd.ExecuteNonQuery();
                                                cmd.Unprepare();

                                                hubChange = true;
                                            }
                                        }

                                        if (hubChange)
                                        {
                                            _logger.WriteMessage(NLog.LogLevel.Info, "14357", "Contoller HUB type has been changed");

                                            // hub has been changed, tell hub handler about a change in controllers
                                            cmd = new AdsCommand("INSERT INTO IHSFY_jobs(Command_ID, Network_id, User_id, Date_created, Status) "
                                                    + " VALUES(" + Constants.MID_TO_COMMSERVER_update_and_send_hub_list.ToString() + ","
                                                    + client.nid.ToString() + ","
                                                    + "-1,'"
                                                    + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                                                    + "0)", conn, txn2);

                                            cmd.ExecuteNonQuery();
                                            cmd.Unprepare();
                                        }
                                    }

                                    // if we made it to here, we are good to commit all our changes
                                    abort = false;
                                }
                                catch (Exception e)
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Error, "3091", "Database exception during existing controller update(s)/insert(s): " + e.ToString());
                                }
                            } while (false);    // allows us to break out early if SQL problem

                            if (abort == true)
                            {
                                _logger.WriteMessage(NLog.LogLevel.Error, "3091", "Database Error on existing controller reregistration.");
                                txn2.Rollback();
                            }
                            else
                            {
                                txn2.Commit();
                            }
                        }
                    }
                }


                if (client.nid == 0)
                {
                    _logger.WriteMessage(NLog.LogLevel.Info, "3020", "New controller, inserting rows into DB...");
                    // NOTE: Transactions not supported on local server, so ignore that exception
                    // insert data into DB and retrieve new network id to assign to controller
                    AdsTransaction txn = null;

                    try
                    {
                        txn = conn.BeginTransaction();
                    }
                    catch (System.NotSupportedException)
                    {

                    }
                    do
                    {
                        try
                        {
                            // insert into ControllerSites
                            var cmd = new AdsCommand("insert into ControllerSites(CompanyID, SiteName) values(-1, 'Site name')", conn, txn);
                            _logger.WriteMessage(NLog.LogLevel.Debug, "3077", "SQL: " + cmd.CommandText);
                            cmd.ExecuteNonQuery();
                            var SiteID = cmd.LastAutoinc;
                            cmd.Unprepare();
                            _logger.WriteMessage(NLog.LogLevel.Info, "3078", "Site ID (new Network ID) is:" + SiteID.ToString());


                            // select isnull(max(NetworkID), -1)+1 from CS3000Systems
                            var newID = SiteID;
                            // cmd = new AdsCommand("select isnull(max(NetworkID), 0)+1 from CS3000Systems", conn, txn);
                            // _logger.WriteMessage(NLog.LogLevel.Debug, "3050", "SQL: " + cmd.CommandText);
                            // var newID = cmd.ExecuteScalar();
                            // if (newID == null) break;
                            _logger.WriteMessage(NLog.LogLevel.Info, "3021", "Network ID is:" + newID.ToString());
                            client.nid = Convert.ToUInt32(newID); // save new ID in client structure for response
                            // add CS3000Systems row
                            // cmd.Unprepare();

                            // add CS3000Networks row including setting the LastCommunicated for the first time to now and MasterTimeStamp to controller reported value
                            cmd = new AdsCommand("insert into CS3000Networks(NetworkID, Deleted, CompanyID, LastCommunicated, MasterTimeStamp, MasterSerialNumber, UseHubType, HubID) values(" +
                                newID.ToString() + ",FALSE,-1,'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'," + Misc.PackedTimeMilliDatetoSQLtimestamp(c.time, c.date).ToString() +
                                "," + client.sn.ToString() + "," + hub_type.ToString() + "," + (hub_type == 0 ? "0" : newID.ToString()) + ")", conn, txn);
                            _logger.WriteMessage(NLog.LogLevel.Debug, "3052", "SQL: " + cmd.CommandText);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();

                            // add CS3000NetworkSettings row 
                            cmd = new AdsCommand("insert into CS3000NetworkSettings(NetworkID) values(" +
                                newID.ToString() + ")", conn, txn);
                            _logger.WriteMessage(NLog.LogLevel.Debug, "3052", "SQL: " + cmd.CommandText);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();

                            // add CS3000Controllers rows
                            int newCID = 0;
                            for (cc = 0; cc < c.details.Count; cc++)
                            {
                                // insert into old Controllers table
                                cmd = new AdsCommand("insert into Controllers(SiteID, ControllerName, ConnectionType, Installed, CompanyID, Model, SoftwareVersion, TP_Version) " +
                                    " values(" + SiteID.ToString() + ",'This Controller (not yet named)',0,'" +
                                    DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "', -1, 7,'" + c.structure_version.ToString() + ' ' + c.code_revision_string +
                                    "', '" + c.TP_code_revision_string + "')", conn, txn);
                                _logger.WriteMessage(NLog.LogLevel.Debug, "3076", "SQL: " + cmd.CommandText);
                                cmd.ExecuteNonQuery();
                                newCID = cmd.LastAutoinc;   // ControllerID was just generated
                                cmd.Unprepare();
                                _logger.WriteMessage(NLog.LogLevel.Info, "3059", "New ControllerID generated is " + newCID.ToString());

                                cmd = new AdsCommand("insert into CS3000Controllers(ControllerID, NetworkID, Name, SerialNumber, FLOption, SSEOption, SSE_DOption, "
                                                        + "WeatherCard, WeatherTerminal, DashM_Card, DashM_Terminal, DashM_CardType, TwoWireTerminal, PortA_DeviceIndex, PortB_DeviceIndex, BoxIndex, Hub_Option) values("
                                                        + newCID.ToString() + "," + newID.ToString() + ",'This Controller (not yet named)',"
                                                        + c.details[cc].serial_number.ToString() + "," + c.details[cc].option_FL.ToString() + ","
                                                        + c.details[cc].option_SSE.ToString() + "," + c.details[cc].option_SSE_D.ToString() + ","
                                                        + c.details[cc].weather_card_present.ToString() + "," + c.details[cc].weather_terminal_present.ToString() + ","
                                                        + c.details[cc].dash_m_card_present.ToString() + "," + c.details[cc].dash_m_terminal_present.ToString() + ","
                                                        + c.details[cc].dash_m_card_type.ToString() + "," + c.details[cc].two_wire_terminal_present.ToString() + ","
                                                        + c.details[cc].port_A_device_index.ToString() + "," + c.details[cc].port_B_device_index.ToString() + ","
                                                        + c.details[cc].box_index.ToString() + "," + c.details[cc].option_HUB.ToString()
                                                        + ")", conn, txn);
                                _logger.WriteMessage(NLog.LogLevel.Debug, "3053", "SQL: " + cmd.CommandText);
                                cmd.ExecuteNonQuery();
                                cmd.Unprepare();

                                // for each controller add its cards to CS3000Cards
                                for (j = 0; j < Constants.STATION_CARD_COUNT; j++)
                                {
                                    cmd = new AdsCommand("insert into CS3000Cards(Slot, CardPresent, TBPresent, ControllerSN, NetworkID, BoxIndex) values(" +
                                        c.details[cc].i2c_cards[j].idx.ToString() + ","
                                        + c.details[cc].i2c_cards[j].card_present.ToString() + ","
                                        + c.details[cc].i2c_cards[j].tb_present.ToString() + ","
                                        + c.details[cc].serial_number.ToString() + ","
                                        + client.nid.ToString() + ","
                                        + c.details[cc].box_index.ToString() + ")", conn, txn);
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "3054", "SQL: " + cmd.CommandText);
                                    cmd.ExecuteNonQuery();
                                    cmd.Unprepare();
                                }
                                // add POC card
                                cmd = new AdsCommand("insert into CS3000Cards(Slot, CardPresent, TBPresent, ControllerSN, NetworkID, BoxIndex) values(" +
                                    Constants.POC_SLOT_NUMBER.ToString() + ","
                                    + c.details[cc].poc.card_present.ToString() + ","
                                    + c.details[cc].poc.tb_present.ToString() + ","
                                    + c.details[cc].serial_number.ToString() + ","
                                    + client.nid.ToString() + ","
                                    + c.details[cc].box_index.ToString() + ")", conn, txn);
                                _logger.WriteMessage(NLog.LogLevel.Debug, "3055", "SQL: " + cmd.CommandText);
                                cmd.ExecuteNonQuery();
                                cmd.Unprepare();

                                // add lights card
                                cmd = new AdsCommand("insert into CS3000Cards(Slot, CardPresent, TBPresent, ControllerSN, NetworkID, BoxIndex) values(" +
                                    Constants.LIGHT_SLOT_NUMBER.ToString() + ","
                                    + c.details[cc].lights.card_present.ToString() + ","
                                    + c.details[cc].lights.tb_present.ToString() + ","
                                    + c.details[cc].serial_number.ToString() + ","
                                    + client.nid.ToString() + ","
                                    + c.details[cc].box_index.ToString() + ")", conn, txn);
                                _logger.WriteMessage(NLog.LogLevel.Debug, "3056", "SQL: " + cmd.CommandText);
                                cmd.ExecuteNonQuery();
                                cmd.Unprepare();
                            }

                            if (c.structure_version > 4)
                            {
                                if (c.commserver_to_clear_data)
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Info, "3065", "Controller reset issued, deleting program data from database....");
                                    await Misc.AddAlert(cts, client.nid, "Controller reset issued, program data deleted from cloud database", false, true);
                                    cmd = new AdsCommand("EXECUTE PROCEDURE DeletePData(" + client.nid.ToString() + ");", conn, txn);
                                    _logger.WriteMessage(NLog.LogLevel.Debug, "14357", "SQL: " + cmd.CommandText);
                                    cmd.ExecuteNonQuery();
                                    cmd.Unprepare();
                                }
                            }

                            if (hub_type>0)
                            {
                                // new hub just got registered, tell hub handler about the new controller.
                                cmd = new AdsCommand("INSERT INTO IHSFY_jobs(Command_ID, Network_id, User_id, Date_created, Status) "
                                        + " VALUES(" + Constants.MID_TO_COMMSERVER_update_and_send_hub_list.ToString() + ","
                                        + client.nid.ToString() + ","
                                        + "-1,'"
                                        + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                                        + "0)", conn, txn);

                                cmd.ExecuteNonQuery();
                                cmd.Unprepare();
                            }

                            // if we made it to here, we are good to commit all our changes
                            abort = false;
                        }
                        catch (Exception e)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "3091", "Database exception during new controller insert(s): " + e.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    if (abort == true)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "3091", "Database Error on new controller addition.");
                        txn.Rollback();
                    }
                    else
                    {
                        txn.Commit();
                    }
                }
                db.CloseDBConnection(conn, _logger);
            }
            

            // if nothing saved to database, do not ACK
            if (abort == true)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "9901", "Not acking, problem with DB update or registration content.");

                _logger.WriteMessage(NLog.LogLevel.Info, "14333", "Build Registration Data Nak response.");

                return (Tuple.Create(Misc.buildNakResponse(client.nid, client.sn, client.mid), (String)null, false, Constants.NO_STATE_DATA));
            }

            var mem = new MemoryStream(0);
            // build ack response
            if (client.nid != 0)
            {
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(client.sn, mem);
                // network id
                Misc.Uint32ToNetworkBytes(client.nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_REGISTRATION_DATA_full_receipt_ack, mem);
                _logger.WriteMessage(NLog.LogLevel.Info, "3071", "Build Registration Data Ack response.");
            }

            // get the built response to return and be sent
            data = mem.ToArray();

            List<byte[]> rsplist = new List<byte[]>();
            rsplist.Add(data);

            return (Tuple.Create(rsplist, (String)null, false, Constants.NO_STATE_DATA));
        }
    }
}
