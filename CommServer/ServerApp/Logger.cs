﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NLog;
using NLog.Config;
using NLog.Targets;
using System.Configuration;

namespace Calsense.Logging
{
    class MyLogger
    {
        private Logger _logger;
        private String _id;

        // for db logs
        private String _cid;
        private String _model;
                
        public MyLogger(string name, string id)
        {
           _logger = LogManager.GetLogger(name);
           _id = id;
           _cid = "";
           _model = "";
        }

        public MyLogger(string name, string id, string controllerId, string controllerModel)
        {
            _logger = LogManager.GetLogger(name);
            _id = id;
            _cid = controllerId;
            _model = controllerModel;
        }

        public String MyId()
        {
            return (_id);
        }

        public void UpdateId(string id)
        {
            _id = id;
        }

        public void UpdateControllerId(string controllerId)
        {
            _cid = controllerId;
        }

        public void WriteMessage(LogLevel level, string eventID, string message, bool userVisible = true)
        {
            ///
            /// create log event from the passed message
            /// 

            // old way message
            //LogEventInfo logEvent = new LogEventInfo(level, _logger.Name, "@" + _id + ":" + message);

            LogEventInfo logEvent = new LogEventInfo(level, _logger.Name, message);

            // set up the dynamic filename
            NLog.GlobalDiagnosticsContext.Set("DataDir", _id);

            // set database fields
            logEvent.Properties["new_id"] = _id;
            logEvent.Properties["model"] = _model;
            logEvent.Properties["userVisible"] = (userVisible ? "1" : "0");
            logEvent.Properties["logLevels"] = getLogLevelInteger(level).ToString();

            //
            // set event-specific context parameter
            // this context parameter can be retrieved using ${event-context:EventID}
            // @wjb: I comment out the EventID for now as long as we don't store it any where
            // logEvent.Properties["EventID"] = eventID;

            // 
            // Call the Log() method. It is important to pass typeof(MyLogger) as the
            // first parameter. If you don't, ${callsite} and other callstack-related 
            // layout renderers will not work properly.
            //

            _logger.Log(typeof(MyLogger), logEvent);

            // also event log warnings and errors
            if (level == LogLevel.Error || level == LogLevel.Warn)
            {
                EventLog.WriteEntry(Program.MyName, "@" + _id + ":" + message, level == LogLevel.Error ? EventLogEntryType.Error : EventLogEntryType.Warning, Convert.ToInt32(eventID));
            }
        }

        public void WriteMessageWithId(LogLevel level, string eventID, string message, string new_id, bool userVisible = true)
        {
            ///
            /// create log event from the passed message
            /// 
            LogEventInfo logEvent = new LogEventInfo(level, _logger.Name, message);

            // set up the dynamic filename
            NLog.GlobalDiagnosticsContext.Set("DataDir", new_id);
            

            // set database fields            
            logEvent.Properties["new_id"] = new_id;
            logEvent.Properties["model"] = _model;
            logEvent.Properties["userVisible"] = (userVisible ? "1" : "0");
            logEvent.Properties["logLevels"] = getLogLevelInteger(level).ToString();

            //
            // set event-specific context parameter
            // this context parameter can be retrieved using ${event-context:EventID}
            // 
            // logEvent.Properties["EventID"] = eventID;

            // 
            // Call the Log() method. It is important to pass typeof(MyLogger) as the
            // first parameter. If you don't, ${callsite} and other callstack-related 
            // layout renderers will not work properly.
            //

            _logger.Log(typeof(MyLogger), logEvent);

            // also event log warnings and errors
            if (level == LogLevel.Error || level == LogLevel.Warn)
            {
                EventLog.WriteEntry(Program.MyName, "@" + new_id + ":" + message, level == LogLevel.Error ? EventLogEntryType.Error : EventLogEntryType.Warning, Convert.ToInt32(eventID));
            }
        }

        public static int getLogLevelInteger(LogLevel level)
        {
            if (level == LogLevel.Trace)
            {
                return 0;
            }
            else if (level == LogLevel.Debug)
            {
                return 1;
            }
            else if (level == LogLevel.Info)
            {
                return 2;
            }
            else if (level == LogLevel.Warn)
            {
                return 3;
            }
            else if (level == LogLevel.Error)
            {
                return 4;
            }
            else if (level == LogLevel.Fatal)
            {
                return 5;
            }

            return -1;
        }
    }
}