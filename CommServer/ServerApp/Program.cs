﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using Calsense.Logging;
using ServerApp;
using System.Collections.Concurrent;
using Topshelf;         // used to make program a service (NuGet package)

namespace Calsense
{
    class Program
    {
        public static readonly string MyName = ConfigurationManager.AppSettings["ServerName"];  // for service and event log use, cannot contain spaces or / or \
        public static readonly string Varsion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

        static void Main(String[] args)
        {
            // call into the Topshelf service class
            HostFactory.Run(host =>
            {
                host.SetServiceName(MyName); //cannot contain spaces or / or \
                host.SetDisplayName(ConfigurationManager.AppSettings["ServerDisplayName"] + " " + Varsion);
                host.SetDescription("CS3000 Controller Communications Server");
                host.StartAutomatically();

                host.RunAsLocalSystem();    // other options: .RunAsNetworkService(), .RunAsLocalService()

                host.Service<CommServerService>();
            });
        }
    }
    // Service relate items:

    public class CommServerService : ServiceControl
    {
        public CancellationTokenSource cts;
        private MyLogger logger;
        //public ConcurrentBag<Task> tasklist;
        public WebFormData wf;
        public CalsenseControllers conts;
        private WebControl web;
        private Calsense.CommServer.Server server;
        private Calsense.CommServer.Server2000 server2000;
        //private int max_simultaneous;

        // start up Service
        public bool Start(HostControl hostControl)
        {
            try
            {
                // create a cancellation token
                cts = new CancellationTokenSource();

                if (System.Threading.Thread.CurrentThread.Name == null)
                {
                    System.Threading.Thread.CurrentThread.Name = "Main";
                }

                // create our web controllable settings class (singleton)
                // (created here so don't have to worry about multi-threaded singleton issues)
                wf = WebFormData.Instance;
                // controllable values to normal operation:
                wf.IgnoreMode = false;
                wf.ForceFirmwareUpdate = false;
                wf.ForceTPUpdate = false;

                // create a place to track concurrent socket tasks we spawn
                //tasklist = new ConcurrentBag<Task>();

                // create an Nlog logger
                logger = new MyLogger("Main", "");

                // create an Event log item
                EventLog.WriteEntry(Program.MyName, "Starting up CommServer", EventLogEntryType.Information, 1);

                var port = Convert.ToInt32(ConfigurationManager.AppSettings["TcpPort"]);

                var port2000 = Convert.ToInt32(ConfigurationManager.AppSettings["TcpPort2000"]);

                // instantiate our class that holds statistics for each controller we hear from
                conts = CalsenseControllers.Instance;
                //logger.WriteMessage(NLog.LogLevel.Warn, "1223", "Number controllers seen is " + CalsenseControllers.NumSeen().ToString());       

                //max_simultaneous = 0;
                // start up commserver code
                server = new Calsense.CommServer.Server(port, cts, null /* tasklist */);
                // start up Inbound 2000 stuff
                server2000 = new Calsense.CommServer.Server2000(port2000, cts, null);
                // start up web server control/monitor code
                web = new ServerApp.WebControl();
                web.Start();
                logger.WriteMessage(NLog.LogLevel.Info, "1000", "Server running on port:" + port.ToString());
                logger.WriteMessage(NLog.LogLevel.Info, "1000", "2000 Inbound on port:" + port2000.ToString());
                logger.WriteMessage(NLog.LogLevel.Info, "1100", "Web server controll on port:" + ConfigurationManager.AppSettings["ControlPort"].ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Stop(HostControl hostControl)
        {
            try
            {
                logger.WriteMessage(NLog.LogLevel.Info, "1010", "Server shutting down.");
                EventLog.WriteEntry(Program.MyName, "Stopping up CommServer", EventLogEntryType.Information, 2);
                cts.Cancel();   // should end our stuff
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}

#if XYX
                    while(!Console.KeyAvailable)
                {
                    Thread.Sleep(100);
                    // run thru the task list and remove ones that are done
                    int i = 0;
                    foreach(Task t in tasklist)
                    {
                        i++;
                        if(t.Status == TaskStatus.RanToCompletion)
                        {
                            Task finished;
                            tasklist.TryTake(out finished);
                        }
                    }
                    if(i > max)
                    {
                        Console.WriteLine("New maximum concurrent connections is: " + i.ToString());
                        max = i;
                    }
                }
#endif