//
// LegacyTransport - manages transport layer pack/unpack of messages
// to/from legacy controllers
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Calsense.Logging;
using ServerApp;
using System.Collections.Concurrent;
using System.Configuration;
using Advantage.Data.Provider;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace Calsense.CommServer
{
    //
    // information for each message we are transmitting
    //
    class TransportTxMsgStruct
    {
        public TransportTxMsgStruct(String h, String a, short m, byte[] d)
        {
            data = d;
            hostname = h;
            adr = a;
            MID = m;
        }
        public byte[] data { get; set; }
        public String hostname { get; set; }
        public String adr { get; set; }
        public short MID { get; set; }
    }

    class AlreadyRainShutdownController
    {
        public AlreadyRainShutdownController(uint cid, UInt16 d)
        {
            controllerId = cid;
            dateofinterest = d;
        }
        public uint controllerId { get; set; }
        public UInt16 dateofinterest { get; set; }
    }
    // 
    // store a one of these for each total message we are working on
    //
    // a "Message" consists of a linked list of packets
    // not all packets may necessarily be present until
    // a complete message is received.
    //
    class TransportMessage
    {
        //
        // initialize a transport session for the given connection
        //
        public TransportMessage(ActiveLegacyConnection a)
        {
            ac = a;                                 // this is the connection we transmit/receive on, will already be established
            tx_retry_timer = -1;                    // set when we expect a status packet back
            rx_expect_timer = -1;
            rx_existence_timer = -1;
            txmsg = null;                          // will get filled in when message is known
            rxmsg = new LinkedList<byte[]>();       // each entry is a received RX packet, we're ready to link them with this LinkedList
            bitmask = 0;
            incomingseq = 0;
            cid = 0;
        }

        public TransportTxMsgStruct txmsg { get; set; }   // only one message outstanding at a time (queued at higher level)
        public LinkedList<byte[]> rxmsg { get; set; }   // packets in message we are receiving
        public int tx_retry_count{ get; set; }
        public int tx_retry_timer { get; set; }
        public int rx_expect_count { get; set; }
        public int rx_expect_timer { get; set; }
        public int rx_existence_timer { get; set; }
        public ActiveLegacyConnection ac { get; set; }  // the connection associated with this message
        public ulong bitmask { get; set; }
        public ushort sequence { get; set; }
        public ushort incomingseq { get; set; }
        public uint cid { get; set; }
        public uint jobid { get; set; }                 // if non-zero, means there is a job entry to mark complete when message finishes
    }

    //
    // these transport timing values are configurable in the App.config file
    // we reload that periodically to get any updates user might make at
    // runtime.
    //
    static class TransportVars 
    {
        public static int TransportTxRetryCount = int.Parse(ConfigurationManager.AppSettings["TransportTxRetryCount"]);        // how many times we wait for a status packet back before giving up a transmit
        public static int TransportTxRetryTimerBase = int.Parse(ConfigurationManager.AppSettings["TransportTxRetryTimerBase"]);        // how long we wait for a status packet back before retrying, base component in formula
        public static int TransportTxRetryTimerAddon = int.Parse(ConfigurationManager.AppSettings["TransportTxRetryTimerAddon"]);        // additional seconds to wait added to above for some controllers
        public static int TransportTxRetryTimerHubAddon = int.Parse(ConfigurationManager.AppSettings["TransportTxRetryTimerHubAddon"]);        // additional seconds to wait added to above for controllers behind a hub
        public static int TransportTxRetryTimerRepeatersAddon = int.Parse(ConfigurationManager.AppSettings["TransportTxRetryTimerRepeatersAddon"]);        // additional seconds to wait added to above for controllers have repeaters
        public static int TransportTxRetryTimerDefault = int.Parse(ConfigurationManager.AppSettings["TransportTxRetryTimerDefault"]);        // how long to wait for controllers in unknown chains       
        public static int TransportRxExistTimer = int.Parse(ConfigurationManager.AppSettings["TransportRxExistTimer"]);        // set each time we receive a packet, if times out, inbound message destroyed
        public static int TransportRxExpectTimer = int.Parse(ConfigurationManager.AppSettings["TransportRxExpectTimer"]);        // we expect to see a packet this often while receiving
        public static int TransportRxExpectCount = int.Parse(ConfigurationManager.AppSettings["TransportRxExpectCount"]);        // after this many times of expect timer expiring we give up on receiving        
    }

    //
    // class that handles managing ongoing messages including timers
    static class LegacyTransport
    {       
        public const int TRANSPORT_TIMER_LOOP_SECS = 5;         // how often to process timers/retries
        public const int MAX_USER_DATA_PER_LEGACY_PACKET = 496; // number of bytes we can fit in a 520 byte packet (overhead of ambles, CRC, etc)
        public const byte LEGACY_SEND_ACK_BIT = (0x40);
        public const byte LEGACY_STATUS_BIT = (0x80);
        public const byte MSG_CLASS_2000e_FROM_CENTRAL = (0);
        public const byte MSG_CLASS_2000e_TO_CENTRAL = (1);
        public const int CMOS_VERSION_OFFSET_IN_APPDATA = (6);  // offset to ASCII version byte in CMOS reponse
        public const int INBOUND_MESSAGE_SN_OFFSET = (0);   // offset of 2 byte serial number in inbound 2000 message response

        //
        // structure to match C struct in SQL parsing routines defined as:
        //      typedef struct { unsigned short  D;
        //               unsigned long   T;
        //      } DATE_TIME;
        //
        [StructLayout(LayoutKind.Sequential, Pack = 8)]
        public unsafe struct LEGACY_DATE_TIME_STRUCT           // unsafe require if buffers inside are fixed
        {
            public ushort D;
            public uint T;
        }

        // this function handles:
        // MID_CONTROLLER_REPORT_DATA_TO_PC 1210 (response is 1211)
        // MID_LEGACY_STATION_REPORT_DATA_TO_PC 1220 (response is 1221)
        // MID_LOGLINES_CONTROLLER_TO_PC    1200 (response is 1201)
        // MID_SEND_FLOW_RECORDER_RECORDS_TO_PC 1110 (response is 1111)
        // MID_SEND_DMC_RECORDER_RECORDS_TO_PC 1490 (response is 1491)
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint LEGACY_REPORTS_extract_and_store_data(
            uint mid,
            byte[] receiveddata,
            int rx_len,
            uint controller_id,
            byte pis_a_600_series,
            uint bool_has_a_dash_f_interface,
            [In, Out] byte[] sqlBuffer,             // 10*1024*1024
            [In] byte[] pSQL_search_condition_str,  
            [In] byte[] pSQL_update_str,            // 4*1024
            [In] byte[] pSQL_insert_fields_str,     // 4*1024
            [In] byte[] pSQL_insert_values_str,
            [In] byte[] pSQL_single_merge_statement_buf);   // 8*1024

        // this function handles:
        // MID_CMOS_TO_PC 1002 response is 1003
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint LEGACY_CMOS_extract_and_store_data(
            byte[] receiveddata,
            uint controller_id,
            byte pis_a_600_series,
            [In, Out] byte[] sqlBuffer,             // 10*1024*1024
            [In] byte[] pSQL_search_condition_str,  
            [In] byte[] pSQL_update_str,            // 4*1024
            [In] byte[] pSQL_insert_fields_str,     // 4*1024
            [In] byte[] pSQL_insert_values_str,
            [In] byte[] pSQL_single_merge_statement_buf);   // 8*1024

        // this function handles MID_PDATA_TO_PC = 1006 response 1007
        //
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint LEGACY_PDATA_extract_and_store_data(
            byte[] receiveddata,
            uint controller_id,
            byte pis_a_600_series,
            [In, Out] byte[] sqlBuffer,             // 10*1024*1024
            [In] byte[] pSQL_update_str,            // 4*1024
            [In] byte[] pSQL_insert_fields_str,     // 4*1024
            [In] byte[] pSQL_insert_values_str,
            [In] byte[] pSQL_single_merge_statement_buf);   // 8*1024

        // this function handles MID_MANUALSTUFF_TO_PC 1250, response 1251 (1252 is no data)
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint LEGACY_MANUAL_extract_and_store_data(
            byte[] receiveddata,
            uint controller_id,
            byte pis_a_600_series,
            [In, Out] byte[] sqlBuffer,             // 10*1024*1024
            [In] byte[] pSQL_search_condition_str,  
            [In] byte[] pSQL_update_str,            // 4*1024
            [In] byte[] pSQL_insert_fields_str,     // 4*1024
            [In] byte[] pSQL_insert_values_str,
            [In] byte[] pSQL_single_merge_statement_buf);   // 8*1024

        // this function handles MID_LIGHTS_TO_PC = 1240 (response 1241)
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint LEGACY_LIGHTS_extract_and_store_data(
            byte[] receiveddata,
            uint controller_id,
            [In, Out] byte[] sqlBuffer,             // 10*1024*1024
            [In] byte[] pSQL_search_condition_str,  
            [In] byte[] pSQL_update_str,            // 4*1024
            [In] byte[] pSQL_insert_fields_str,     // 4*1024
            [In] byte[] pSQL_insert_values_str,
            [In] byte[] pSQL_single_merge_statement_buf);   // 8*1024

        // this function handles:
        // MID_GETALERTS 1010 (response is 1011)
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint LEGACY_ALERTS_parse_alert_and_return_length(
            uint paid,
            [In, Out] byte[] ppile,
            uint pparse_why,
            [In, Out] byte[] pdest_ptr,
            uint pallowable_size,
            int pindex,
            byte pis_a_600_series);

        // wrapper function for above DLL call with attribute to allow exception catching
        // if it throws access exception (not normally catchable in C#)
        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptionsAttribute]
        [System.Security.SecurityCriticalAttribute]
        static uint reports_extract_wrapper( 
            uint mid, 
            byte[] receiveddata, 
            uint controller_id,
            byte pis_a_600_series,
            uint bool_has_a_dash_f_interface,
            byte[] sqlBuffer)
        {
            uint i = 0;
            try
            {
                // allocate the buffers that the parsing function uses to build SQL statements internally
                byte[] a1 = new byte[4 * 1024];
                byte[] a2 = new byte[4 * 1024];
                byte[] a3 = new byte[4 * 1024];
                byte[] a4 = new byte[4 * 1024];
                byte[] a5 = new byte[8 * 1024];
                i = LEGACY_REPORTS_extract_and_store_data(mid, receiveddata, receiveddata.Length, controller_id, pis_a_600_series, bool_has_a_dash_f_interface, sqlBuffer, a1, a2, a3, a4, a5);
            }
            catch (Exception e)
            {
                i = 0;
                _log.WriteMessageWithId(NLog.LogLevel.Error, "14444", "Report Data parse exception in LEGACY_REPORTS_extract_and_store_data: " + e.ToString(), controller_id.ToString());
            }
            return (i);
        }

        // wrapper function for above DLL call with attribute to allow exception catching
        // if it throws access exception (not normally catchable in C#)
        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptionsAttribute]
        [System.Security.SecurityCriticalAttribute]
        static uint cmos_extract_wrapper(
            byte[] receiveddata,
            uint controller_id,
            byte pis_a_600_series,
            byte[] sqlBuffer)
        {
            uint i = 0;
            try
            {
                // allocate the buffers that the parsing function uses to build SQL statements internally
                byte[] a1 = new byte[4 * 1024];
                byte[] a2 = new byte[4 * 1024];
                byte[] a3 = new byte[4 * 1024];
                byte[] a4 = new byte[4 * 1024];
                byte[] a5 = new byte[8 * 1024];
                i = LEGACY_CMOS_extract_and_store_data(receiveddata, controller_id, pis_a_600_series, sqlBuffer, a1, a2, a3, a4, a5);
            }
            catch (Exception e)
            {
                i = 0;
                _log.WriteMessageWithId(NLog.LogLevel.Error, "14444", "CMOS Data parse exception in LEGACY_CMOS_extract_and_store_data: " + e.ToString(), controller_id.ToString());
            }
            return (i);
        }

        // wrapper function for above DLL call with attribute to allow exception catching
        // if it throws access exception (not normally catchable in C#)
        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptionsAttribute]
        [System.Security.SecurityCriticalAttribute]
        static uint manual_extract_wrapper(
            byte[] receiveddata,
            uint controller_id,
            byte pis_a_600_series,
            byte[] sqlBuffer)
        {
            uint i = 0;

            try
            {
                // allocate the buffers that the parsing function uses to build SQL statements internally
                byte[] a1 = new byte[4 * 1024];
                byte[] a2 = new byte[4 * 1024];
                byte[] a3 = new byte[4 * 1024];
                byte[] a4 = new byte[4 * 1024];
                byte[] a5 = new byte[8 * 1024];
                i = LEGACY_MANUAL_extract_and_store_data(receiveddata, controller_id, pis_a_600_series, sqlBuffer, a1, a2, a3, a4, a5);
            }
            catch (Exception e)
            {
                i = 0;
                _log.WriteMessageWithId(NLog.LogLevel.Error, "14444", "Manual Program Data parse exception in LEGACY_MANUAL_extract_and_store_data: " + e.ToString(), controller_id.ToString());
            }
            return (i);
        }

        // wrapper function for above DLL call with attribute to allow exception catching
        // if it throws access exception (not normally catchable in C#)
        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptionsAttribute]
        [System.Security.SecurityCriticalAttribute]
        static uint lights_extract_wrapper(
            byte[] receiveddata,
            uint controller_id,
            byte[] sqlBuffer)
        {
            uint i = 0;

            try
            {
                // allocate the buffers that the parsing function uses to build SQL statements internally
                byte[] a1 = new byte[4 * 1024];
                byte[] a2 = new byte[4 * 1024];
                byte[] a3 = new byte[4 * 1024];
                byte[] a4 = new byte[4 * 1024];
                byte[] a5 = new byte[8 * 1024];
                i = LEGACY_LIGHTS_extract_and_store_data(receiveddata, controller_id, sqlBuffer, a1, a2, a3, a4, a5);
            }
            catch (Exception e)
            {
                i = 0;
                _log.WriteMessageWithId(NLog.LogLevel.Error, "14444", "Lights Data parse exception in LEGACY_LIGHTS_extract_and_store_data: " + e.ToString(), controller_id.ToString());
            }
            return (i);
        }

        // wrapper function for above DLL call with attribute to allow exception catching
        // if it throws access exception (not normally catchable in C#)
        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptionsAttribute]
        [System.Security.SecurityCriticalAttribute]
        static uint pdata_extract_wrapper(
            byte[] receiveddata,
            uint controller_id,
            byte pis_a_600_series,
            byte[] sqlBuffer)
        {
            uint i = 0;

            try
            {
                // allocate the buffers that the parsing function uses to build SQL statements internally
                byte[] a1 = new byte[4 * 1024];
                byte[] a2 = new byte[4 * 1024];
                byte[] a3 = new byte[4 * 1024];
                byte[] a4 = new byte[8 * 1024];
                i = LEGACY_PDATA_extract_and_store_data(receiveddata, controller_id, pis_a_600_series, sqlBuffer, a1, a2, a3, a4);
            }
            catch (Exception e)
            {
                i = 0;
                _log.WriteMessageWithId(NLog.LogLevel.Error, "14444", "Program Data parse exception in LEGACY_PDATA_extract_and_store_data: " + e.ToString(), controller_id.ToString());
            }
            return (i);
        }

        // wrapper function for above DLL call with attribute to allow exception catching
        // if it throws access exception (not normally catchable in C#)
        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptionsAttribute]
        [System.Security.SecurityCriticalAttribute]
        static uint alerts_wrapper(uint diag_code, byte[] receiveddata, int offset, byte[] raw_string, byte pis_a_600_series, uint controller_id)
        {
            uint alert_size;
            
            try
            {
                alert_size = LEGACY_ALERTS_parse_alert_and_return_length(diag_code,
                  receiveddata,
                  Constants.PARSE_FOR_LENGTH_AND_TEXT,
                  raw_string,
                  Constants.MAX_ALERT_STRING - 1,
                  offset,
                  pis_a_600_series);
            }
            catch (Exception e)
            {
                alert_size = 0;
                _log.WriteMessageWithId(NLog.LogLevel.Error, "7093", "Alerts parse exception in nm_ALERT_PARSING_parse_alert_and_return_length " +
                                     "(Diag Code: " + diag_code.ToString() + ") : " + e.ToString(), controller_id.ToString());
            }

            return (alert_size);
        }
        //
        // parse a Get ET response and build SQL to store data in ETShare table
        //
        static uint parseLegacyET(uint controllerId, byte[] receiveddata, byte[] sqlBuffer)
        {
            WeatherData wd = new WeatherData();
            int offset = 0;
            UInt16 date;

            // ETShare table:
            // ControllerID: integer
            // ETDate: date
            // ET: double
            // ETStatus: shortint

            // received data
            // �	DATE_TIME � the current date/time
            // �	ET_TABLE_ENTRY: 	UNS_16	et_inches_u16_10000u, UNS_16 status;

            date = Misc.LegacyBytesToUint16(receiveddata, offset); offset += sizeof(UInt16); offset += sizeof(UInt32); // for datetime
            wd.stamp = Misc.PackedDatetoDate(date);
            wd.et_value = Misc.LegacyBytesToUint16(receiveddata, offset); offset += sizeof(UInt16);
            wd.et_status = receiveddata[offset];

            var etstatstr = wd.et_status == null ? "NULL" : wd.et_status.ToString();
            var etvalstr = wd.et_value == null ? "NULL" : wd.et_value.ToString();

            // build merge SQL and place it in the provided SQL command buffer
            String cmd = "MERGE ETShare ON (ControllerID=" + controllerId.ToString() +
                         " AND ETDate='" + wd.stamp.ToString("yyyy-MM-dd") +
                         "') WHEN NOT MATCHED THEN insert (ControllerID,ETDate,ETStatus,ET)" +
                         " values(" + controllerId.ToString() + ",'" + 
                         wd.stamp.ToString("yyyy-MM-dd") + "'," +
                              etstatstr + "," + etvalstr + ")";

            Encoding.ASCII.GetBytes(cmd, 0, cmd.Length, sqlBuffer, 0);
            return 1;
        }
        //
        // parse a Get Rain response and build SQL to store data in RainShare table
        //
        static uint parseLegacyRain(uint controllerId, byte[] receiveddata, byte[] sqlBuffer)
        {
            WeatherData wd = new WeatherData();
            int offset = 0;
            UInt16 date;

            // RainShare table:
            // ControllerID: integer
            // RainDate: date
            // Rain: double
            // RainStatus: shortint

            // received data
            // �	DATE_TIME � the current date/time
            // �	RAIN_TABLE_ENTRY: 		UNS_16	rain_inches_u16_100u;   UNS_16 status;

            date = Misc.LegacyBytesToUint16(receiveddata, offset); offset += sizeof(UInt16); offset += sizeof(UInt32); // for datetime
            wd.stamp = Misc.PackedDatetoDate(date);
            wd.rain_value = Misc.LegacyBytesToUint16(receiveddata, offset); offset += sizeof(UInt16);
            wd.rain_status = receiveddata[offset];

            if (wd.rain_status != null)
            {
                if (wd.rain_status == 8 || wd.rain_status == 9)
                {
                    var errorDesc = "Received unexpected rain status " + wd.rain_status + ", not sharing";
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "1754", errorDesc, controllerId.ToString());
                    Misc.AddET2000Alert(_cts, controllerId, errorDesc, true).Wait();
                    wd.rain_status = null;
                }
            }

            var rainstatstr = wd.rain_status == null ? "NULL" : wd.rain_status.ToString();
            var rainvalstr = wd.rain_value == null ? "NULL" : wd.rain_value.ToString();

            // build merge SQL and place it in the provided SQL command buffer
            String cmd = "MERGE RainShare ON (ControllerID=" + controllerId.ToString() +
                         " AND RainDate='" + wd.stamp.ToString("yyyy-MM-dd") +
                         "') WHEN NOT MATCHED THEN insert (ControllerID,RainDate,RainStatus,Rain)" +
                         " values(" + controllerId.ToString() + ",'" +
                         wd.stamp.ToString("yyyy-MM-dd") + "'," +
                              rainstatstr + "," + rainvalstr + ")";

            Encoding.ASCII.GetBytes(cmd, 0, cmd.Length, sqlBuffer, 0);
            return 1;
        }
        //
        // controller has received rain shutdown message correctly. this
        // function keeps track of the given controller/date so that
        // no more shutdowns are sent to it during the given period
        //
        static void MarkRainShutdownOK(uint cid, UInt16 dateofinterest)
        {
            // if this controller already exists...don't need to add new one, just update date of interest
            AlreadyRainShutdownController c = AlreadyShutdown.Find(x => x.controllerId == cid);
            if(c != null)
            {
                c.dateofinterest = dateofinterest;  // change to this date
            }
            else
            {
                AlreadyShutdown.Add(new AlreadyRainShutdownController(cid, dateofinterest));
            }
            _log.WriteMessage(NLog.LogLevel.Debug, "154", "Rain Shutdown OK, remembering CID: " + cid.ToString());
        }
        //
        // generate outbound rain shutdown commands for controllers that match
        // SELECT ControllerID FROM Controllers WHERE RainSharingControllerID = :ControllerID AND Communicate=1 AND Deleted=0"
        //
        // ignore any controllers that have already given the OK to a shutdown message during this period
        //
        static bool parseLegacyRainPoll(uint controllerId, byte[] receiveddata, byte[] sqlBuffer)
        {
            bool abort = true;
            AdsCommand cmd;
            AdsDataReader rdr;
            ushort controllerdata;


            controllerdata = Misc.LegacyBytesToUint16(receiveddata, 0); // we will send this value in the shutdown message to each controller

            // for each controller insert a job into IHSFY_jobs table for a rain shutdown
            // record alerts in database

            ServerDB db = new ServerDB(_cts);

            AdsConnection conn = db.OpenDBConnectionNotAsync(controllerId.ToString());

            if (conn != null)
            {
                List<uint> cids = new List<uint>();   // we will build this up based on query results
                try
                {
                    // query all the controllers we need to build a job for
                    cmd = new AdsCommand("SELECT distinct ControllerID FROM Controllers WHERE RainSharingControllerID = " + controllerId.ToString() + " AND Communicate=1 AND Deleted=0", conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);
                    rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        var cid = 0;
                        while (rdr.Read())
                        {
                            cid = 0;
                            if (!rdr.IsDBNull(0))cid = rdr.GetInt32(0);

                            if (cid != 0)
                            {
                                var zz = AlreadyShutdown.Find(x => ((x.controllerId == cid) && (x.dateofinterest == controllerdata)));
                                if (zz == null)
                                {
                                    cids.Add((uint)cid);
                                }
                                else
                                {
                                    _log.WriteMessageWithId(NLog.LogLevel.Info, "154", "Skipping Rain Shutdown to controller since already OK'd it this period.", cid.ToString());
                                }
                            }
                        }
                    }
                    rdr.Close();
                    cmd.Unprepare();

                    abort = false;  // so far so good...
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "14394", "Database exception during Legacy query: " + e.ToString());
                    abort = true;
                }

                // only bother to insert jobs if we have something to insert :)
                if (abort == false && cids.Count > 0)
                {
                    abort = true;   // back to default mode of aborting until good

                    _log.WriteMessageWithId(NLog.LogLevel.Info, "9020", "Inserting " + cids.Count.ToString() + " Legacy Rain Shutdown jobs into DB...", controllerId.ToString());
                    // NOTE: Transactions not supported on local server, so ignore that exception
                    // insert data into DB and retrieve new network id to assign to controller
                    AdsTransaction txn = null;

                    try
                    {
                        txn = conn.BeginTransaction();
                    }
                    catch (System.NotSupportedException)
                    {

                    }
                    do
                    {
                        try
                        {
                            foreach (int c in cids)
                            {

                                cmd = new AdsCommand("INSERT INTO IHSFY_jobs(Command_ID, Controller_ID, User_id, Date_created, Status, Param1) "
                                                        + " VALUES(" + LegacyTasks.MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER.ToString() + ","
                                                        + c.ToString() + ","
                                                        + "-1,'"
                                                        + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                                                        + "0, " + controllerdata.ToString() + ")", conn);

                                _log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);

                                cmd.ExecuteNonQuery();
                                cmd.Unprepare();
                            }
      
                            // if we made it to here, we are good to commit all our changes
                            abort = false;
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "9091", "Database exception during Legacy rain shutdown jobs insert(s): " + e.ToString(), controllerId.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    if (abort == true)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "9091", "Database Error on Legacy alerts inserts.", controllerId.ToString());
                        txn.Rollback();
                    }
                    else
                    {
                        txn.Commit();
                    }
                }

                db.CloseDBConnection(conn, controllerId.ToString());
            }

            // stop now if problem
            if (abort == true) return (abort);

            // look for any 3000's that get their rain/ET from a legacy controller and queue up a IHSFY job for them
            conn = db.OpenDBConnectionNotAsync(controllerId.ToString());

            if (conn != null)
            {
                List<Tuple<uint,int>> nidstzs = new List<Tuple<uint,int>>();   // we will build this up based on query results
                try
                {
                    // query all the controllers we need to build a job for
                    cmd = new AdsCommand("SELECT distinct NetworkID, TimeZone FROM CS3000Networks WHERE RainSharingControllerID = " + controllerId.ToString() + " AND Deleted=0", conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);
                    rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        var nid = 0;
                        var tz = 0;
                        while (rdr.Read())
                        {
                            nid = 0;
                            tz = 0;
                            if (!rdr.IsDBNull(0)) nid = rdr.GetInt32(0);
                            if (!rdr.IsDBNull(1)) tz = rdr.GetInt32(1);

                            if (nid != 0)
                            {
                                nidstzs.Add(new Tuple<uint, int>((uint)nid, (int)tz));
                            }
                        }
                    }
                    rdr.Close();
                    cmd.Unprepare();

                    abort = false;  // so far so good...
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "14394", "Database exception during Legacy query: " + e.ToString());
                    abort = true;
                }

                // only bother to insert jobs if we have something to insert :)
                if (abort == false && nidstzs.Count > 0)
                {
                    abort = true;   // back to default mode of aborting until good

                    _log.WriteMessageWithId(NLog.LogLevel.Info, "9020", "Inserting " + nidstzs.Count.ToString() + " Legacy 3000 Rain Shutdown jobs into DB...", controllerId.ToString());
                    // NOTE: Transactions not supported on local server, so ignore that exception
                    // insert data into DB and retrieve new network id to assign to controller
                    AdsTransaction txn = null;

                    try
                    {
                        txn = conn.BeginTransaction();
                    }
                    catch (System.NotSupportedException)
                    {

                    }
                    do
                    {
                        try
                        {
                            var mins = 0;
                            foreach (Tuple<uint, int> nz in nidstzs)
                            {

                                DateTime ControllerNow = PeriodicTasks.CurrentTimeAtControllerInTz(nz.Item2);
                                DateTime ControllerStartOfDay = ControllerNow.Date;
                                DateTime Controller8pm = ControllerStartOfDay.AddHours(20);
                                DateTime ControllerNext8pm = ControllerNow <= Controller8pm ? Controller8pm : Controller8pm.AddDays(1);
                                TimeSpan RemainingMinutes = ControllerNext8pm - ControllerNow;
                                mins = (int)RemainingMinutes.TotalMinutes;

                                cmd = new AdsCommand("INSERT INTO IHSFY_jobs(Command_ID, Network_id, User_id, Date_created, Status, Param1) "
                                                        + " VALUES(" + Constants.MID_FROM_COMMSERVER_RAIN_SHUTDOWN_packet.ToString() + ","
                                                        + nz.Item1.ToString() + ","
                                                        + "-1,'"
                                                        + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                                                        + "0, " + mins.ToString() + ")", conn);

                                _log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);

                                cmd.ExecuteNonQuery();
                                cmd.Unprepare();
                            }

                            // if we made it to here, we are good to commit all our changes
                            abort = false;
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "9091", "Database exception during Legacy rain shutdown jobs insert(s): " + e.ToString(), controllerId.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    if (abort == true)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "9091", "Database Error on Legacy alerts inserts.", controllerId.ToString());
                        txn.Rollback();
                    }
                    else
                    {
                        txn.Commit();
                    }
                }

                db.CloseDBConnection(conn, controllerId.ToString());
            }

            return (abort);
        }
        //
        // parse & store legacy alerts
        // return abort status (true = problem, false = all good)
        //
        static bool ParseAlerts(byte[] data, uint controllerId, byte version)
        {
            int i;
            int cc;
            byte[] raw_string = new byte[Constants.MAX_ALERT_STRING];
            int size;
            byte[] Buffer = null;
            AdsCommand cmd;
            int zero;
            bool abort = true;

            DateTime beginstamp;

            int offset;
            uint alert_size;

            AlertList l = new AlertList();

            _log.WriteMessageWithId(NLog.LogLevel.Info, "7004", "Processing Legacy Alerts message...", controllerId.ToString());

            size = data.Length;
            Buffer = data;

            DateTime dt;
            string cmdSql = "";
            DateTime lastAlertTimestampInDB;

            offset = 0;
            i = 0;

            // data consists of variable sized alerts, where each alerts contains:
            //  Alert ID: 16-bit unsigned (used to identify how to parse the alert)
            //  Time Stamp: The DATE_TIME when the alert was generated
            //  Raw bytes based upon the alert ID. For example, some alerts have no associated bytes and 
            // simply display a text string based upon the alert ID. Others may include the entire text 
            // string. The rest usually contain a mix of 8-bit, 16-bit, and 32-bit variables which contain 
            // information necessary for the alert.
            //  One byte length, length of all the bytes in alert including length byte

            while (offset < size)
            {
                // empty our string buffer to start with
                Array.Clear(raw_string, 0, raw_string.Length);

                l.records.Add(new Alert());
                l.records[i].diag_code = Misc.LegacyBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                l.records[i].date = Misc.LegacyBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                l.records[i].time = Misc.LegacyBytesToUint32(Buffer, offset); offset += sizeof(UInt32);

                // parse the alert getting the descriptive string that we want and knowledge of length to get us to next alert
                alert_size = alerts_wrapper(l.records[i].diag_code, Buffer, offset, raw_string, version, controllerId);

                // check for error return from parse routine
                if (alert_size == 0)
                {
                    // stop processing if routine returns 0 indicating error
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "9094", "Unexpected legacy alert parsing length of 0 on an Alert of ID " + l.records[i].diag_code.ToString(), controllerId.ToString());
                    l.records[i].description = "ALERT PARSE ZERO LENGTH MISMATCH (Alert " + l.records[i].diag_code + ")";
                    break;
                }
                // check for mismatch between computed and returned length which probably indicates structure change
                else if (alert_size != (Buffer[offset + alert_size - 1] - 8))
                {
                    // stop processing on a mismatch, log error
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "9099", "Legacy Alert " + l.records[i].diag_code.ToString(), controllerId.ToString());
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "9099", "Legacy Alert mismatch on parsed length " + alert_size.ToString() + " vs calc using length byte value " + (8 + Buffer[offset + alert_size - 1]).ToString(), controllerId.ToString());
                    l.records[i].description = "ALERT PARSE SIZE MISMATCH (Alert " + l.records[i].diag_code + ")";
                    break;
                }

                offset += (int)alert_size;
                l.records[i].description = System.Text.Encoding.UTF8.GetString(raw_string, 0, Constants.MAX_ALERT_STRING);
                zero = l.records[i].description.IndexOf('\0');
                if (zero >= 0) l.records[i].description = l.records[i].description.Remove(zero);
                if (l.records[i].description.Length > 0)
                {
                    // we're okay
                }
                else
                {
                    // put in an empty description so SQL not upset on insert
                    l.records[i].description = " ";
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "9057", "Empty Description returned from parser on Legacy alert: " + l.records[i].diag_code.ToString(), controllerId.ToString());
                }
                // remove any non-ASCII characters so does not mess up SQL
                l.records[i].description = Regex.Replace(l.records[i].description, @"[^\u0000-\u007F]", string.Empty);

                i++;

            }

            lastAlertTimestampInDB = LegacyTasks.GetLastDateTime("LastTimestamp_Alerts", controllerId);
            
            // record alerts in database
            ServerDB db = new ServerDB(_cts);

            AdsConnection conn = db.OpenDBConnectionNotAsync(controllerId.ToString());

            if (conn != null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Info, "9020", "Inserting " + l.records.Count.ToString() + " Legacy Alerts row(s) into DB...", controllerId.ToString());
                // NOTE: Transactions not supported on local server, so ignore that exception
                // insert data into DB and retrieve new network id to assign to controller
                AdsTransaction txn = null;

                try
                {
                    txn = conn.BeginTransaction();
                }
                catch (System.NotSupportedException)
                {

                }
                do
                {
                    try
                    {
                        beginstamp = DateTime.Now;

                        for (cc = 0; cc < l.records.Count; cc++)
                        {
                            dt = Misc.PackedTimeDatetoDateTime(l.records[cc].time, l.records[cc].date);
                            
                            if (dt < DateTime.Now.AddDays(1)) // don't insert alerts if it was in the future
                            {
                                // 1/12/2017 wjb : add the Merge statement only if the alert timestamp was on the same second 
                                // for the last alert we've inserted in the db.
                                if (System.Math.Abs((dt - lastAlertTimestampInDB).TotalSeconds) < 1)
                                {
                                    cmdSql = "MERGE Alerts ON " +
                                    " (ControllerID=" + controllerId.ToString() + " AND AlertDiagCode=" + l.records[cc].diag_code.ToString() +
                                    " AND AlertTimeStamp='" + dt.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'" +
                                    " ) WHEN NOT MATCHED THEN " +
                                    " insert (ControllerID, AlertTimestamp," +
                                    " AlertDiagCode, Description) " +
                                    " values(" + controllerId.ToString() + ",'"
                                    + dt.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                                    + l.records[cc].diag_code.ToString() + "," + "'" + l.records[cc].description.ToString().Replace("'", "''") + "'"
                                    + ")";
                                }
                                else
                                {
                                    cmdSql = "INSERT INTO Alerts (ControllerID, AlertTimestamp,AlertDiagCode, Description) " +
                                    " VALUES(" + controllerId.ToString() + ",'"
                                    + dt.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                                    + l.records[cc].diag_code.ToString() + ",'" + l.records[cc].description.ToString().Replace("'", "''")
                                    + "')";
                                }

                                cmd = new AdsCommand(cmdSql, conn, txn);
                                //_log.WriteMessageWithId(NLog.LogLevel.Debug, "9053", "SQL: " + cmd.CommandText, controllerId.ToString());
                                cmd.ExecuteNonQuery();
                                cmd.Unprepare();

                                // 1/11/2017 wjb : update the lastTimestamp_Alerts in Controllers table
                                if (cc==0)
                                {
                                    cmd = new AdsCommand("UPDATE Controllers SET LastTimestamp_Alerts=" + Misc.PackedTimeMilliDatetoSQLtimestamp(l.records[cc].time, l.records[cc].date) +
                                    " WHERE ControllerID=" + controllerId.ToString(), conn, txn);
                                    cmd.ExecuteNonQuery();
                                    cmd.Unprepare();
                                }
                            }
                        }

                        DateTime endstamp = DateTime.Now;
                        TimeSpan sqltime = endstamp - beginstamp;
                        _log.WriteMessageWithId(NLog.LogLevel.Debug, "9777", "SQL time total for " + l.records.Count.ToString() + " legacy alerts is " + sqltime.ToString(), controllerId.ToString());

                        // if we made it to here, we are good to commit all our changes
                        abort = false;
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "9091", "Database exception during Legacy alerts insert(s): " + e.ToString(), controllerId.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "9091", "Database Error on Legacy alerts inserts.", controllerId.ToString());
                    txn.Rollback();
                }
                else
                {
                    txn.Commit();
                }

                db.CloseDBConnection(conn, controllerId.ToString());
            }
            return (abort);
        }

        
        private static bool UpdatePdata(uint controllerId, uint jobid)
        {
            bool abort = true;
            AdsCommand cmd;

            DateTime? pgmTimestamp;

            pgmTimestamp = null;

            var s = LegacyTasks.GetJobArgumentBytes(jobid, "Param4");

            try
            {
                pgmTimestamp = DateTime.FromBinary(BitConverter.ToInt64(s, 0)); // pgmTimestamp stored in bytes
            }
            catch (Exception ex)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "989", "Error while trying to save ET2000 pdata (error in reading PgmTimestamp from IHSFY_Jobs) ex: " + ex.Message, controllerId.ToString());
            }

            if (pgmTimestamp.HasValue)
            {
                // record alerts in database
                ServerDB db = new ServerDB(_cts);

                AdsConnection conn = db.OpenDBConnectionNotAsync(controllerId.ToString());

                if (conn != null)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "9020", "Updating legacy pdata after updating controller...", controllerId.ToString());
                    // NOTE: Transactions not supported on local server, so ignore that exception
                    // insert data into DB and retrieve new network id to assign to controller
                    AdsTransaction txn = null;

                    try
                    {
                        txn = conn.BeginTransaction();
                    }
                    catch (System.NotSupportedException)
                    {

                    }
                    do
                    {
                        try
                        {
                            string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                            string pgmTimestampStr = pgmTimestamp.Value.ToString("yyyy-MM-dd HH:mm:ss.fff");

                            // that will notify all users that there's a new copy of pdata
                            cmd = new AdsCommand("UPDATE BulkPgmData2 SET ExistsOnController=false WHERE ControllerID=" + controllerId.ToString(), conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();

                            cmd = new AdsCommand("INSERT INTO BulkPgmData1 SELECT " + controllerId.ToString() + ",'" + now + "',ETOption,ETCountyIndex," +
                                "ETCityIndex,RunWeek,PumpUsage,FlowMeter,TypeOfMV,LearnGPMs,IrrigMLB,NonIrrigMLB,MainLineBreak,FullYear,Password1,Password2," +
                            "Password3,Password,MasterUnit,MasterMLB,ControllerOff,CommAddr,LightsDef,RunTempProg,RunTempTill,UseBudget,BudgetOption,PercentOfETo," +
                            "UseDailyET,DailyETDate,UsingAnETGage,ComBaudRate,WS_InUse,WS_Pause,WS_P_Speed,WS_R_Speed,WS_P_Time,WS_R_Time,Jumpers_Demonstrator," +
                            "Jumpers_UsingPager,Jumpers_FM_FM,Jumpers_FM_ET,Jumpers_RG_ET,Jumpers_WS_ET,Jumpers_D1,Jumpers_D2,Jumpers_D3,Jumpers_D4,LogETPulses," +
                            "DLSavings FROM BulkPgmData1 b WHERE b.ControllerID=" + controllerId.ToString() +
                            " AND PgmTimeStamp='" + pgmTimestampStr + "'", conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();


                            cmd = new AdsCommand("INSERT INTO BulkPgmData2 SELECT " + controllerId.ToString() + ",'" + now + "'," +
                            "RainSwitchInUse,EnableHOTime,HOTime,RainInfo_InUse,RainInfo_HookedUp,RainInfo_MaxHourlyRate,RainInfo_Maximum,RainInfo_Minimum,RainInfo_Status," +
                            "RainInfo_Dummy2,RainInfo_RBTableDate,RainInfo_Dummy4,CentralRadio,XFlowMeter_1,XFlowMeter_2,MaxFlowNumber,StationOrder,ManCycleSoak,DailyETUse12," +
                            "AllowNegHO_1,AllowNegHO_2,AllowNegHO_3,AllowNegHO_4,AllowNegHO_5,AllowNegHO_6,AllowNegHO_7,MaxInchesOfRainHO,FME_K_1,FME_K_2,FME_K_3,FME_O_1,FME_O_2," +
                            "FME_O_3,FM_Connected,FMEnterOwn,VariableCCInUse,ReportsRollTimeEnabled,ReportsRollTime,LogAddress,DiagAddress,ActiveStations,EESize,ControllerDateTime," +
                            "CentralDateTime,SWVersion,CommunicationsType,true,UsePassword " +  
                            "FROM BulkPgmData2 b WHERE b.ControllerID=" + controllerId.ToString() +
                            " AND PgmTimeStamp='" + pgmTimestampStr + "'", conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();


                            cmd = new AdsCommand("INSERT INTO BulkPgmData3 SELECT " + controllerId.ToString() + ",'" + now + "'," +
                            "IncludeTime,UseETAveraging_1,UseETAveraging_2,UseETAveraging_3,UseETAveraging_4,UseETAveraging_5,UseETAveraging_6,UseETAveraging_7,DTMF_Mode1Addr,DTMF_Mode2Addr," +
                            "BacklightState,MSInUse,DTMFChannel1,DTMFChannel2,TrackEstimatedUsage,Deleted,UseYourOwnNumbers,POAFS,NOCIC,HighFlowMargin,LowFlowMargin,Capacity_NonPump,IrrigatingByCapacity," +
                            "TestingFlow,FlowRange1Top,FlowRange2Top,FlowRange3Top,FlowTolerance1Plus,FlowTolerance1Minus,FlowTolerance2Plus,FlowTolerance2Minus,FlowTolerance3Plus,FlowTolerance3Minus," +
                            "FlowTolerance4Plus,FlowTolerance4Minus,TailEndsThrowAwayUpperbound,TailEndsMakeAdjustmentUpperbound,IrriElectricalStationLimit,FlowLength,ChainDown,FlowMeterAvailableElsewhere," +
                            "MasterHasIrrigateByCapacity,MasterHasFlowChecking,FlowInfoValid,ProgramPrioritiesEnabled,UserDefinedETState,UserDefinedETCounty,UserDefinedETCity " +
                            " FROM BulkPgmData3 b WHERE b.ControllerID=" + controllerId.ToString() +
                            " AND PgmTimeStamp='" + pgmTimestampStr + "'", conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();


                            cmd = new AdsCommand("INSERT INTO BulkPgmData4 SELECT " + controllerId.ToString() + ",'" + now + "'," +
                                "CreatedByRReInterface,PTagsEditableAtController,RReChannelChangeMode,RReControllerChannel,RReUserChannel,DMCInUse,DMCLevels,DMCMaxFlowRateLevel1,DMCMaxFlowRateLevel2," +
                                "MLBMVOR,MaximumHoldoverFactor,SICFunctionalityName,SICContactType,SICIrrigationToAffect,SICActionToTake,SICSwitchName,YearlyBudget " +
                            " FROM BulkPgmData4 b WHERE b.ControllerID=" + controllerId.ToString() +
                            " AND PgmTimeStamp='" + pgmTimestampStr + "'", conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();


                            cmd = new AdsCommand("INSERT INTO MonthlyPrgmData SELECT " + controllerId.ToString() + ",'" + now + "'," +
                                "DataMonth,Budgets,ETNumber,FTimesMax,VariableCCs,Schedule,SX_ProgramA,SX_ProgramAEnabled,SX_ProgramB,SX_ProgramBEnabled,SX_ProgramC,SX_ProgramCEnabled,SX_ProgramD," +
                                "SX_ProgramDEnabled,SX_ProgramE,SX_ProgramEEnabled,SX_ProgramD1,SX_ProgramD1Enabled,SX_ProgramD2,SX_ProgramD2Enabled,UserETNumbers " +
                                " FROM MonthlyPrgmData b WHERE b.ControllerID=" + controllerId.ToString() +
                                " AND PgmTimeStamp='" + pgmTimestampStr + "'", conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();


                            cmd = new AdsCommand("INSERT INTO POCPrgmData SELECT " + controllerId.ToString() + ",'" + now + "'," +
                                "POCID,POC_Name,PartOfIrrigation,AllowIrriDuringMLB,MVType,FMInUse,FMUseKAndO,MLBDuringIdle,MLBDuringUse,MLBDuringIrri " +
                           " FROM POCPrgmData b WHERE b.ControllerID=" + controllerId.ToString() +
                           " AND PgmTimeStamp='" + pgmTimestampStr + "'", conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();


                            cmd = new AdsCommand("INSERT INTO POCSchedule SELECT " + controllerId.ToString() + ",'" + now + "'," +
                                "POCID,POCDay,OpenTimeAEnabled,OpenTimeA,DurationA,OpenTimeBEnabled,OpenTimeB,DurationB " +
                           " FROM POCSchedule b WHERE b.ControllerID=" + controllerId.ToString() +
                           " AND PgmTimeStamp='" + pgmTimestampStr + "'", conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();

                            cmd = new AdsCommand("INSERT INTO ProgramPrgmData SELECT " + controllerId.ToString() + ",'" + now + "'," +
                                "DataProgram,NoFlowShutOff,PumpByProgram,TripPercent,ValveTime,UseVariableCC,OverFlowShutOff,VariableCCByProg_Jan,VariableCCByProg_Feb,VariableCCByProg_Mar," +
                                "VariableCCByProg_Apr,VariableCCByProg_May,VariableCCByProg_June,VariableCCByProg_July,VariableCCByProg_Aug,VariableCCByProg_Sep,VariableCCByProg_Oct," +
                                "VariableCCByProg_Nov,VariableCCByProg_Dec,PTags,MSByProg,DailyETByProg,HoldOverEnabledByProg,HoldOverByProg,ValvesOnByProgram,SetExpected,ValveCloseTime," +
                                "ValvesOnInSystem,WindInUse,ProgramPriority,UserTag,PercentAdjust1,PercentAdjustNumberOfDays1 " +
                           " FROM ProgramPrgmData b WHERE b.ControllerID=" + controllerId.ToString() +
                           " AND PgmTimeStamp='" + pgmTimestampStr + "'", conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();

                            cmd = new AdsCommand("INSERT INTO StationPgmData SELECT " + controllerId.ToString() + ",'" + now + "'," +
                                "StationNumber,StationInUse,StationInfo_Size,StationInfo_Rate,LorL,FlowStatus,IStatus,NoWaterDays,NoWaterToday,EditSVar_Temp_PTime,EditSVar_Temp_HTime," +
                                "EditSVar_Temp_CTime,EditSVar_Jan_PTime,EditSVar_Jan_HTime,EditSVar_Jan_CTime,EditSVar_Feb_PTime,EditSVar_Feb_HTime,EditSVar_Feb_CTime,EditSVar_Mar_PTime," +
                                "EditSVar_Mar_HTime,EditSVar_Mar_CTime,EditSVar_Apr_PTime,EditSVar_Apr_HTime,EditSVar_Apr_CTime,EditSVar_May_PTime,EditSVar_May_HTime,EditSVar_May_CTime," +
                                "EditSVar_June_PTime,EditSVar_June_HTime,EditSVar_June_CTime,EditSVar_July_PTime,EditSVar_July_HTime,EditSVar_July_CTime,EditSVar_Aug_PTime,EditSVar_Aug_HTime," +
                                "EditSVar_Aug_CTime,EditSVar_Sep_PTime,EditSVar_Sep_HTime,EditSVar_Sep_CTime,EditSVar_Oct_PTime,EditSVar_Oct_HTime,EditSVar_Oct_CTime,EditSVar_Nov_PTime," +
                                "EditSVar_Nov_HTime,EditSVar_Nov_CTime,EditSVar_Dec_PTime,EditSVar_Dec_HTime,EditSVar_Dec_CTime,PAssign_Temp,PAssign_Norm,Name,DailyETFactor,LowFlowLimits, " +
                                "MSSensorAssignment,MSSetPoint,MSReading,MSReadingStatus,MSMaxWaterDays " +
                           " FROM StationPgmData b WHERE b.ControllerID=" + controllerId.ToString() +
                           " AND PgmTimeStamp='" + pgmTimestampStr + "'", conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();

                            cmd = new AdsCommand("INSERT INTO TagNames SELECT " + controllerId.ToString() + ",'" + now + "'," +
                                "TagIndex,TagNames " +
                           " FROM TagNames b WHERE b.ControllerID=" + controllerId.ToString() +
                           " AND PDataTimeStamp='" + pgmTimestampStr + "'", conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();

                            cmd = new AdsCommand("INSERT INTO TwentyEightDayPrgmData SELECT " + controllerId.ToString() + ",'" + now + "'," +
                                "DataDate,DailyET_ET,DailyET_Status,RainTable_Status,RainTable_Pad,RainTable_Rain,WDay_ProgA_Temp,WDay_ProgB_Temp,WDay_ProgC_Temp,WDay_ProgD_Temp,WDay_ProgE_Temp," +
                            "WDay_ProgD1_Temp,WDay_ProgD2_Temp,WDay_ProgA_Jan,WDay_ProgB_Jan,WDay_ProgC_Jan,WDay_ProgD_Jan,WDay_ProgE_Jan,WDay_ProgD1_Jan,WDay_ProgD2_Jan,WDay_ProgA_Feb,WDay_ProgB_Feb," +
                            "WDay_ProgC_Feb,WDay_ProgD_Feb,WDay_ProgE_Feb,WDay_ProgD1_Feb,WDay_ProgD2_Feb,WDay_ProgA_Mar,WDay_ProgB_Mar,WDay_ProgC_Mar,WDay_ProgD_Mar,WDay_ProgE_Mar,WDay_ProgD1_Mar," +
                            "WDay_ProgD2_Mar,WDay_ProgA_Apr,WDay_ProgB_Apr,WDay_ProgC_Apr,WDay_ProgD_Apr,WDay_ProgE_Apr,WDay_ProgD1_Apr,WDay_ProgD2_Apr,WDay_ProgA_May,WDay_ProgB_May,WDay_ProgC_May," +
                            "WDay_ProgD_May,WDay_ProgE_May,WDay_ProgD1_May,WDay_ProgD2_May,WDay_ProgA_June,WDay_ProgB_June,WDay_ProgC_June,WDay_ProgD_June,WDay_ProgE_June,WDay_ProgD1_June,WDay_ProgD2_June," +
                            "WDay_ProgA_July,WDay_ProgB_July,WDay_ProgC_July,WDay_ProgD_July,WDay_ProgE_July,WDay_ProgD1_July,WDay_ProgD2_July,WDay_ProgA_Aug,WDay_ProgB_Aug,WDay_ProgC_Aug,WDay_ProgD_Aug," +
                            "WDay_ProgE_Aug,WDay_ProgD1_Aug,WDay_ProgD2_Aug,WDay_ProgA_Sep,WDay_ProgB_Sep,WDay_ProgC_Sep,WDay_ProgD_Sep,WDay_ProgE_Sep,WDay_ProgD1_Sep,WDay_ProgD2_Sep,WDay_ProgA_Oct," +
                            "WDay_ProgB_Oct,WDay_ProgC_Oct,WDay_ProgD_Oct,WDay_ProgE_Oct,WDay_ProgD1_Oct,WDay_ProgD2_Oct,WDay_ProgA_Nov,WDay_ProgB_Nov,WDay_ProgC_Nov,WDay_ProgD_Nov,WDay_ProgE_Nov," +
                            "WDay_ProgD1_Nov,WDay_ProgD2_Nov,WDay_ProgA_Dec,WDay_ProgB_Dec,WDay_ProgC_Dec,WDay_ProgD_Dec,WDay_ProgE_Dec,WDay_ProgD1_Dec,WDay_ProgD2_Dec " +
                           " FROM TwentyEightDayPrgmData b WHERE b.ControllerID=" + controllerId.ToString() +
                           " AND PgmTimeStamp='" + pgmTimestampStr + "'", conn, txn);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();

                            // if we made it to here, we are good to commit all our changes
                            abort = false;
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Error, "9092", "Database exception during updating legacy pdata: " + e.ToString(), controllerId.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    if (abort == true)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "9092", "Database Error on Legacy updating pdata.", controllerId.ToString());
                        txn.Rollback();
                    }
                    else
                    {
                        txn.Commit();
                    }

                    db.CloseDBConnection(conn, controllerId.ToString());
                }
            }
            return (abort);
        }
        

        // constructor
        static LegacyTransport() 
        { 
            _log = new MyLogger("LegacyTransport", "" , "", Constants.ET2000Model);
            _sem = new SemaphoreSlim(1);
            _cts = null;        // this will get set at runtime
            ActiveTransports = new ConcurrentDictionary<String, TransportMessage>();
            Task.Factory.StartNew(TransportLoop, TaskCreationOptions.LongRunning);
            Random r = new Random();
            SequenceCounter = (ushort)r.Next(0, 0x0fff);    // start out at a random number, 12 bits
            AlreadyShutdown = new List<AlreadyRainShutdownController>();
        }

        // properties"T
        private static MyLogger _log;
        private static SemaphoreSlim _sem;  // mutex to protect access to our dictionary
        private static CancellationTokenSource _cts;
        private static ConcurrentDictionary<String, TransportMessage> ActiveTransports;
        public static ushort SequenceCounter;
        public static List<AlreadyRainShutdownController> AlreadyShutdown;

        public static void SetCts(CancellationTokenSource c)
        {
            _cts = c;
        }

        // methods
        public static void ReloadVars()
        {
            int old;

            ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");

            old = TransportVars.TransportTxRetryCount;
            TransportVars.TransportTxRetryCount = int.Parse(ConfigurationManager.AppSettings["TransportTxRetryCount"]);
            if (old != TransportVars.TransportTxRetryCount)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "7444", "Transport Tx Retry Count changed from " + old.ToString() + " to " + TransportVars.TransportTxRetryCount.ToString());
            }

            old = TransportVars.TransportTxRetryTimerBase;
            TransportVars.TransportTxRetryTimerBase = int.Parse(ConfigurationManager.AppSettings["TransportTxRetryTimerBase"]);
            if (old != TransportVars.TransportTxRetryTimerBase)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "7444", "Transport Tx Retry Base Timer changed from " + old.ToString() + " to " + TransportVars.TransportTxRetryTimerBase.ToString());
            }
            old = TransportVars.TransportTxRetryTimerAddon;
            TransportVars.TransportTxRetryTimerAddon = int.Parse(ConfigurationManager.AppSettings["TransportTxRetryTimerAddon"]);
            if (old != TransportVars.TransportTxRetryTimerAddon)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "7444", "Transport Tx Retry Addon Timer changed from " + old.ToString() + " to " + TransportVars.TransportTxRetryTimerAddon.ToString());
            }
            old = TransportVars.TransportTxRetryTimerHubAddon;
            TransportVars.TransportTxRetryTimerHubAddon = int.Parse(ConfigurationManager.AppSettings["TransportTxRetryTimerHubAddon"]);
            if (old != TransportVars.TransportTxRetryTimerHubAddon)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "7444", "Transport Tx Retry Hub Addon Timer changed from " + old.ToString() + " to " + TransportVars.TransportTxRetryTimerHubAddon.ToString());
            }
            old = TransportVars.TransportTxRetryTimerRepeatersAddon;
            TransportVars.TransportTxRetryTimerRepeatersAddon = int.Parse(ConfigurationManager.AppSettings["TransportTxRetryTimerRepeatersAddon"]);
            if (old != TransportVars.TransportTxRetryTimerRepeatersAddon)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "7444", "Transport Tx Retry Repeaters Addon Timer changed from " + old.ToString() + " to " + TransportVars.TransportTxRetryTimerRepeatersAddon.ToString());
            }
            old = TransportVars.TransportTxRetryTimerDefault;
            TransportVars.TransportTxRetryTimerDefault = int.Parse(ConfigurationManager.AppSettings["TransportTxRetryTimerDefault"]);
            if (old != TransportVars.TransportTxRetryTimerDefault)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "7444", "Transport Tx Retry Default Timer changed from " + old.ToString() + " to " + TransportVars.TransportTxRetryTimerDefault.ToString());
            }

            old = TransportVars.TransportRxExistTimer;
            TransportVars.TransportRxExistTimer = int.Parse(ConfigurationManager.AppSettings["TransportRxExistTimer"]);
            if (old != TransportVars.TransportRxExistTimer)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "7444", "Transport Rx Exist Timer changed from " + old.ToString() + " to " + TransportVars.TransportRxExistTimer.ToString());
            }

            old = TransportVars.TransportRxExpectTimer;
            TransportVars.TransportRxExpectTimer = int.Parse(ConfigurationManager.AppSettings["TransportRxExpectTimer"]);
            if (old != TransportVars.TransportRxExpectTimer)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "7444", "Transport Rx Expect Timer changed from " + old.ToString() + " to " + TransportVars.TransportRxExpectTimer.ToString());
            }

            old = TransportVars.TransportRxExpectCount;
            TransportVars.TransportRxExpectCount = int.Parse(ConfigurationManager.AppSettings["TransportRxExpectCount"]);
            if (old != TransportVars.TransportRxExpectCount)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "7444", "Transport Rx Expect Count changed from " + old.ToString() + " to " + TransportVars.TransportRxExpectCount.ToString());
            }
        }
        //
        // we have received enough packets to match TotalBlocks
        // note: they are in the LinkedList in random order
        // run thru the linked list and see if this makes a complete message (it should, right?)
        //
        public static void ProcessAppMessage(ActiveLegacyConnection ac, TransportMessage tm)
        {
            ushort mid;
            int app_length = 0;
            int blocknumber;
            char version = '5'; // assume series 5
            int jobStatus = Constants.JOB_SUCCESSFUL;

            // kill our rx timers, we have a complete app message to process
            tm.rx_existence_timer = -1;
            tm.rx_expect_timer = -1;

            // get the version number for parsing routines that care
            var controller = ac.controllers.Find(x => x.CID == tm.cid);

            if (controller != null)
            {
                if (controller.FWVersion == 6) version = '6';
                Misc.LegacyLastCommunicated(_log, _cts, tm.cid);
            }

            // we should be able to find all the blocks in our linked list
            // lets figure out how big the application message is.
            for (LinkedListNode<byte[]> item = tm.rxmsg.First; item != null; item = item.Next)
            {
                // sanity check - should all be 520 bytes all but last block
                if (item.Value[LegacyInternal.OFFSET_THIS_BLOCK] != item.Value[LegacyInternal.OFFSET_TOTAL_BLOCKS] && item.Value.Length != (LegacyInternal.DATA_PACKET_OVERHEAD + LegacyInternal.MAX_APPDATA_IN_PACKET))
                {
                    // non final packet that is not full???
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "970", "Transport layer sees non-full non-final packet. Won't process this response from " + ac.activeadr.ToString(), tm.cid.ToString());
                    // we can't process this response, data is not consistent
                    EndTxMessageProcessing(tm.ac, tm, tm.jobid, Constants.JOB_INVALID_RESPONSE, null, tm.cid);
                    return;
                }
                app_length += item.Value.Length - LegacyInternal.DATA_PACKET_OVERHEAD;  // application data portion of packet
                app_length += 2;    // for the MID bytes that are in the first packet (2 extra bytes in subsequent packets)
            }

            //
            // allocation an array to hold all the application data including MID from first packet
            //
            byte[] appmessage = new byte[app_length];

            try
            {

                // now fill in the bytes into that array by copying out of each packet
                for (LinkedListNode<byte[]> item = tm.rxmsg.First; item != null; item = item.Next)
                {
                    blocknumber = item.Value[LegacyInternal.OFFSET_THIS_BLOCK];
                    //
                    // copy the application data from each block into the overall appmessage array
                    // note: the first block in message has a Message ID in the first two bytes of
                    // application data. the remaining blocks do not.
                    //
                    Array.Copy(item.Value, LegacyInternal.OFFSET_MESSAGEID, appmessage,
                        (blocknumber - 1) * (LegacyInternal.MAX_APPDATA_IN_PACKET + 2), 2 + item.Value.Length - LegacyInternal.DATA_PACKET_OVERHEAD);
                }
            }
            catch(Exception e)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "970", "Exception processing app message during Array.Copy " +e.ToString(), tm.cid.ToString());
                for (LinkedListNode<byte[]> item = tm.rxmsg.First; item != null; item = item.Next)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "970", "Rx block # " + item.Value[LegacyInternal.OFFSET_THIS_BLOCK].ToString(), tm.cid.ToString());
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "970", "Rx block len " + item.Value.Length.ToString(), tm.cid.ToString());
                }
                EndTxMessageProcessing(tm.ac, tm, tm.jobid, Constants.JOB_INVALID_RESPONSE, null, tm.cid);
                return;
            }


            // based on message ID (first two bytes in application area), do something.
            mid = (ushort)((ushort)appmessage[0] << 8);
            mid |= appmessage[1];

            _log.WriteMessageWithId(NLog.LogLevel.Info, "987", "Processing legacy message, MID= " + mid.ToString() + ", bytes= " + appmessage.Length.ToString(), tm.cid.ToString());

#if DUMPRAWBYTES
            _log.WriteMessageWithId(NLog.LogLevel.Info, "7004", "Dump of app data bytes raw:" + appmessage.Length.ToString()), tm.cid.ToString();
            _log.WriteMessageWithId(NLog.LogLevel.Debug, "8002", "cid: " + tm.cid + "  mid: " + mid + "   bytes: " + BitConverter.ToString(appmessage), tm.cid.ToString());
            for (i = 0, j = (int)appmessage.Length; i < appmessage.Length; i += 8)
            {
                if (j >= 8)
                {
                    s = appmessage[i].ToString("x2") + " " + appmessage[i + 1].ToString("x2") + " " + appmessage[i + 2].ToString("x2") + " " + appmessage[i + 3].ToString("x2") + " " + appmessage[i + 4].ToString("x2") + " " + appmessage[i + 5].ToString("x2") + " " + appmessage[i + 6].ToString("x2") + " " + appmessage[i + 7].ToString("x2");
                    j -= 8;
                }
                else
                {
                    s = "";
                    while (j > 0)
                    {
                        s = s + appmessage[i].ToString("x2") + " ";
                        j--;
                        i++;
                    }
                }
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "8001", s, tm.cid.ToString());
                if (j <= 0) break;
                Thread.Sleep(10);       // allow UDP logger to catch up
            }
#endif

            // check for null application data (only message ID)
            if (appmessage.Length <= 2)
            {
                // add new insert for the pdata we just sent to the contoller, so it will be the latest for
                // user to view in the UI.
                if (mid == LegacyTasks.MID_PDATA_TO_CONTROLLER_RESP)
                {
                    UpdatePdata(tm.cid, tm.jobid);
                }

                _log.WriteMessageWithId(NLog.LogLevel.Info, "14329", "FYI message contains no application data to parse.", tm.cid.ToString());
                // we have finished a message sequence, now okay to move to next sequence
                EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, null, tm.cid);
                return;
            }

            {
                uint ret;
                byte[] sqlBuffer = new byte[Constants.SQL_BUFFER_SIZE];
                String sqlcommands = "";
                AdsCommand cmd;
                bool abort = true;

                // since our data has a 2 byte MID at the front, and the parse routines do not want that, 
                // we have to copy everything but first two bytes to new array
                byte[] parsearray = new byte[appmessage.Length - 2];
                Array.Copy(appmessage, 2, parsearray, 0, appmessage.Length - 2);

                // response contains application data...
                // for report type functions we
                // call the data extract SQL build function
                // for other functions, we can potentially place that data in the 
                // job status area.
                switch (mid)
                {
                    case LegacyTasks.MID_COMMSERVER_REQUEST_TO_SEND_SERIAL_NUMBER_RESP:
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "14446", "Initial Inbound 2000 Msg Recv'd", tm.cid.ToString());
                        HubConnectionTracker.InboundHookup(_cts, ac, tm, (parsearray[INBOUND_MESSAGE_SN_OFFSET] << 8) | parsearray[INBOUND_MESSAGE_SN_OFFSET + 1]);
                        return;
                    case LegacyTasks.MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER_RESP_START:
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "14446", "Direct Access Entered", tm.cid.ToString());
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER_RESP_STOP: 
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "14446", "Direct Access Stopped", tm.cid.ToString());
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_DIRECTACCESS_SCREEN_UPDATE_TO_PC:
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "14447", "Direct Access Dump Screen Status", tm.cid.ToString());
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER_RESP_OK:
                        // indicate we should no longer send rain shutdown messages to this controller during the current window
                        MarkRainShutdownOK(tm.cid, Misc.LegacyBytesToUint16(tm.txmsg.data, 0));
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "14444", "Rain Shutdown Response indicates okay", tm.cid.ToString());
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_SHARING_RAIN_SHUTDOWN_TO_CONTROLLER_DATES_DONT_MATCH:
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "14444", "Rain Shutdown Response indicates dates don't match?!", tm.cid.ToString());
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_SHARING_RAIN_POLL_RESP_NO_RAIN:
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "14444", "Rain Poll Response indicates no rain.", tm.cid.ToString());
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_SHARING_RAIN_POLL_RESP_NOT_RB:
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "14444", "Rain Poll Response indicates no -RB option.", tm.cid.ToString());
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_SHARING_RAIN_POLL_RESP_RAIN:
                        // �	unsigned short � the date the rain was collected for.
                        if(parseLegacyRainPoll(tm.cid, parsearray, sqlBuffer))
                        {
                            // something wrong updating database, flush any queued jobs to this same controller
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "14391", "Flushing any pending jobs for this controller after database problem.", tm.cid.ToString());
                            FlushControllerMessages(tm.ac, tm.cid);
                            jobStatus = Constants.JOB_INVALID_RESPONSE;
                        }
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_GETALERTS_RESP:
                        if (ParseAlerts(parsearray, tm.cid, version == '6' ? (byte)1 : (byte)0))
                        {
                            // something wrong updating database, flush any queued jobs to this same controller
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "14391", "Flushing any pending jobs for this controller after database problem.", tm.cid.ToString());
                            FlushControllerMessages(tm.ac, tm.cid);
                            jobStatus = Constants.JOB_INVALID_RESPONSE;
                        }
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_CLEARMLB_RESP:
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_SHARING_GET_ET_RESP_NOT_A_G:
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "14444", "Get ET Response indicates no -G option.", tm.cid.ToString());
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_SHARING_GET_ET_RESP_GAGE_NOT_IN_USE:
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "14444", "Get ET Response indicates G exists but not in use.", tm.cid.ToString());
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_SHARING_GET_ET_RESP_OK:
                        //�	DATE_TIME � the current date/time
                        //�	ET_TABLE_ENTRY
                        //Message Processing:
                        //�	The weather data you receive should be stored in the database for retrieval at the share time. For example, you can store the information in the ETShare table.
                        ret = parseLegacyET(tm.cid, parsearray, sqlBuffer);
                        break;

                    case LegacyTasks.MID_SHARING_GET_RAIN_RESP_NOT_AN_RB:
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "14444", "Get Rain Response indicates no -RB option.", tm.cid.ToString());
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_SHARING_GET_RAIN_RESP_OK:
                        //�	DATE_TIME � the current date/time
                        //�	RAIN_TABLE_ENTRY
                        //Message Processing:
                        //�	The weather data you receive should be stored in the database for retrieval at the share time. For example, you can store the information in the RainShare table.
                        ret = parseLegacyRain(tm.cid, parsearray, sqlBuffer);
                        break;

                    case LegacyTasks.MID_SHARING_ETRAIN_TO_CONTROLLER_RESP_OK:
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "14444", "Successfully Shared Rain/ET to controller.", tm.cid.ToString());
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_SHARING_ETRAIN_TO_CONTROLLER_RESP_ERROR:
                        byte errorBit = parsearray[0];

                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "14444", "Sharing Rain/ET Response indicates error.", tm.cid.ToString());

                        if ((errorBit & Constants.LEGACY_SHARED_BEFORE_ROLL_TIME) != 0)
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "14444", "Sharing Rain/ET before roll time.", tm.cid.ToString());
                        if ((errorBit & Constants.LEGACY_SHARED_AFTER_MID_NIGHT) != 0)
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "14444", "Sharing Rain/ET after mid night.", tm.cid.ToString());
                        if ((errorBit & Constants.LEGACY_SHARED_ET_TO_AN_ACTIVE_GAGE) != 0)
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "14444", "Sharing Rain/ET to an active gage.", tm.cid.ToString());
                        if ((errorBit & Constants.LEGACY_SHARED_RAIN_TO_AN_RB) != 0)
                            _log.WriteMessageWithId(NLog.LogLevel.Warn, "14444", "Sharing Rain/ET to an active rain bucket.", tm.cid.ToString());
                        jobStatus = Constants.JOB_INVALID_RESPONSE;
                        EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, parsearray, tm.cid);
                        return;

                    case LegacyTasks.MID_CONTROLLER_REPORT_DATA_TO_PC_RESP:
                        uint hasDashF_Interface = (uint)(LegacyTasks.LegacyControllerHasFInterface(tm.cid) ? 1 : 0);
                        ret = reports_extract_wrapper(LegacyTasks.MID_CONTROLLER_REPORT_DATA_TO_PC, parsearray, tm.cid, version == '6' ? (byte)1 : (byte)0, hasDashF_Interface, sqlBuffer);
                        break;

                    case LegacyTasks.MID_LEGACY_STATION_REPORT_DATA_TO_PC_RESP:
                        ret = reports_extract_wrapper(LegacyTasks.MID_LEGACY_STATION_REPORT_DATA_TO_PC, parsearray, tm.cid, version == '6' ? (byte)1 : (byte)0, 0, sqlBuffer);
                        break;
                    case LegacyTasks.MID_LOGLINES_CONTROLLER_TO_PC_RESP:
                        ret = reports_extract_wrapper(LegacyTasks.MID_LOGLINES_CONTROLLER_TO_PC, parsearray, tm.cid, version == '6' ? (byte)1 : (byte)0, 0, sqlBuffer);
                        break;
                    case LegacyTasks.MID_SEND_FLOW_RECORDER_RECORDS_TO_PC_RESP:
                        ret = reports_extract_wrapper(LegacyTasks.MID_SEND_FLOW_RECORDER_RECORDS_TO_PC, parsearray, tm.cid, version == '6' ? (byte)1 : (byte)0, 0, sqlBuffer);
                        break;
                    case LegacyTasks.MID_SEND_DMC_RECORDER_RECORDS_TO_PC_RESP:
                        ret = reports_extract_wrapper(LegacyTasks.MID_SEND_DMC_RECORDER_RECORDS_TO_PC, parsearray, tm.cid, version == '6' ? (byte)1 : (byte)0, 0, sqlBuffer);
                        break;

                    case LegacyTasks.MID_MANUALSTUFF_TO_CONTROLLER_RESP:
                        ret = manual_extract_wrapper(parsearray, tm.cid, version == '6' ? (byte)1 : (byte)0, sqlBuffer); // TBD need to get datetime
                        break;

                    case LegacyTasks.MID_MANUALSTUFF_TO_PC_RESP:
                        ret = manual_extract_wrapper(parsearray, tm.cid, version == '6' ? (byte)1 : (byte)0, sqlBuffer);
                        break;

                    case LegacyTasks.MID_LIGHTS_TO_PC_RESP:
                        ret = lights_extract_wrapper(parsearray, tm.cid, sqlBuffer);
                        break;

                    case LegacyTasks.MID_PDATA_TO_PC_RESP:
                        ret = pdata_extract_wrapper(parsearray, tm.cid, version == '6' ? (byte)1 : (byte)0, sqlBuffer); // TBD need to get datetime
                        break;

                    case LegacyTasks.MID_CMOS_TO_PC_RESP:
                        // the software version controller is running is contained in an ASCII string
                        // in the app message response, so we can distinguish series 6 from 5 controllers using that.
                        // if the first digit is an ASCII 6 we have a 6 series controller
                        if (parsearray.Length > CMOS_VERSION_OFFSET_IN_APPDATA)
                        {
                            if (parsearray[CMOS_VERSION_OFFSET_IN_APPDATA] == '6')
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Debug, "14329", "CMOS response indicates series 6 controller.", tm.cid.ToString());
                                var c = ac.controllers.Find(x => x.CID == tm.cid);

                                if (c != null)
                                {
                                    c.FWVersion = 6;
                                    c.GetCMOSRecv = true;   // flag that we have received a CMOS value setting firmware version
                                }
                            }
                            else if (parsearray[CMOS_VERSION_OFFSET_IN_APPDATA] == '5')
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Debug, "14329", "CMOS response indicates series 5 controller.", tm.cid.ToString());
                                var c = ac.controllers.Find(x => x.CID == tm.cid);

                                if (c != null)
                                {
                                    c.FWVersion = 5;
                                    c.GetCMOSRecv = true;
                                }
                            }
                            else
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "14329", "CMOS response contains neither 5 or 6 in version number string?", tm.cid.ToString());
                            }
                        }
                        ret = cmos_extract_wrapper(parsearray, tm.cid, 0, sqlBuffer);  // pis byte not used in parse routine, so 0 should be okay to pass in
                        break;

                    // unhandled message IDs
                    default:
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "14329", "Legacy message ID " + mid.ToString() + " unknown or not parsed for SQL.", tm.cid.ToString());
                        return;
                }
                if (ret != 0)
                {
                    // should have a valid SQL statement to process
                    sqlcommands = System.Text.Encoding.Default.GetString(sqlBuffer).TrimEnd('\0');  // remove extra nulls
                    // get rid of mysterious non-ascii characters
                    //if (Regex.Matches(sqlcommands, @"[^\u0000-\u007F]").Count > 0)
                    //{
                    //    _log.WriteMessage(NLog.LogLevel.Warn, "14329", "FYI: Replacing non-ASCII characters in SQL with question marks. Probably indicates an error.");
                    //    sqlcommands = Regex.Replace(sqlcommands, @"[^\u0000-\u007F]", "?");
                    // }
                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "14329", "Returned legacy SQL is " + sqlcommands.ToString(), tm.cid.ToString());

                    if (!String.IsNullOrEmpty(sqlcommands))
                    {
                        // record in database
                        ServerDB db = new ServerDB(_cts);

                        AdsConnection conn = db.OpenDBConnectionNotAsync(tm.cid.ToString());
                        if (conn != null)
                        {
                            _log.WriteMessageWithId(NLog.LogLevel.Info, "14320", "Inserting Legacy Data into DB...", tm.cid.ToString());
                            // NOTE: Transactions not supported on local server, so ignore that exception
                            // insert data into DB and retrieve new network id to assign to controller
                            AdsTransaction txn = null;

                            try
                            {
                                txn = conn.BeginTransaction();
                            }
                            catch (System.NotSupportedException)
                            {

                            }
                            do
                            {
                                try
                                {
                                    cmd = new AdsCommand(sqlcommands, conn, txn);
                                    cmd.ExecuteNonQuery();
                                    cmd.Unprepare();

                                    // if we made it to here, we are good to commit all our changes
                                    abort = false;
                                }
                                catch (Exception e)
                                {
                                    _log.WriteMessageWithId(NLog.LogLevel.Error, "14391", "Database exception during Legacy Data insert(s): " + e.ToString(), tm.cid.ToString());
                                }
                            } while (false);    // allows us to break out early if SQL problem

                            if (abort == true)
                            {
                                _log.WriteMessageWithId(NLog.LogLevel.Error, "14391", "Database Error on Legacy Data inserts.", tm.cid.ToString());
                                txn.Rollback();
                            }
                            else
                            {
                                txn.Commit();
                            }
                            db.CloseDBConnection(conn, tm.cid.ToString());

                            if (abort == true)
                            {
                                // something wrong updating database, flush any queued jobs to this same controller
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "14391", "Flushing any pending jobs for this controller after database problem. mid: " + mid.ToString(), tm.cid.ToString());
                                FlushControllerMessages(tm.ac, tm.cid);
                                jobStatus = Constants.JOB_INVALID_RESPONSE;
                            }
                        }
                    }
                    else
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "1982", "FYI: Parsing routine returned empty string. Probably indicates an error. mid: " + mid.ToString(), tm.cid.ToString());
                    }
                }
                else
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Error, "1981", "Error building SQL from Legacy response mid: " + mid.ToString() + ", flushing any queued commands.", tm.cid.ToString());
                    FlushControllerMessages(tm.ac, tm.cid);
                    jobStatus = Constants.JOB_INVALID_RESPONSE;
                }

            }

            // we have finished a message sequence, now okay to move to next sequence
            EndTxMessageProcessing(tm.ac, tm, tm.jobid, jobStatus, null, tm.cid);
        }
        //
        // remote end needs an ACK sent
        // see which blocks we are missing...
        //
        public static void BuildandTransmitAck(ActiveLegacyConnection ac, TransportMessage tm)
        {
            ulong mask = 0;
            byte block_number;
            byte total_blocks = 0;
            int acks_length;
            int i;
            byte[] data;
            UInt32 crc;
            byte val;

            if(tm.rxmsg == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Warn, "970", "Ignoring request to ack as message complete.", tm.cid.ToString());
                return;
            }

            // sanity check
            if(tm.rxmsg.Count == 0)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "970", "Internal request to send an ACK but no RX message in process??? for: " + ac.activeadr.ToString(), tm.cid.ToString());
                return;
            }

            // for each packet we have, set that bit
            for (LinkedListNode<byte[]> item = tm.rxmsg.First; item != null; item = item.Next)
            {
                block_number = item.Value[LegacyInternal.OFFSET_THIS_BLOCK];
                total_blocks = item.Value[LegacyInternal.OFFSET_TOTAL_BLOCKS];
                mask |= 0x01UL << (block_number - 1);
            }
            // number of bytes we need to send depends on total blocks in message

            var mem = new MemoryStream(0);
            mem.WriteByte(LEGACY_STATUS_BIT | (MSG_CLASS_2000e_FROM_CENTRAL << 1));

            mem.WriteByte(0);                                       // dummy word align byte

            // 3 byte from
            mem.WriteByte(0xff);                                    // central address is 0xff 0xff 0xff
            mem.WriteByte(0xff);
            mem.WriteByte(0xff);

            // 3 character controller address
            mem.WriteByte((byte)ac.activeadr[0]);
            mem.WriteByte((byte)ac.activeadr[1]);
            mem.WriteByte((byte)ac.activeadr[2]);

            // 2 byte sequence number matching what controller sent us
            LegacyTasks.LegacyUint16ToNetworkBytes(tm.incomingseq, mem);

            // not used in status packet
            mem.WriteByte((byte)0);
            // ack length in bytes
            if (total_blocks == tm.rxmsg.Count)
            {
                acks_length = 0;
            }
            else
            {
                acks_length = total_blocks / 8;                 // 8 block indicators per byte
                if (total_blocks % 8 != 0) acks_length++;       // one byte for leftover bits
            }
            mem.WriteByte((byte)acks_length);

            for (i = 0; i < acks_length; i++)
            {
                val = 0;
                for(int j = 0; j < 8; j++)
                {
                    if((mask & (0x01UL << (i*8)+j)) != 0UL)
                    {
                        val |= (byte)(0x01 << j);
                    }
                }
                mem.WriteByte(val);     // we have set the bit for blocks we have received
            }

            data = mem.ToArray();

            crc = Misc.CRC(data);
            //_log.WriteMessageWithId(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"), tm.cid.ToString());
            _log.WriteMessageWithId(NLog.LogLevel.Debug, "178", "Ack with " + acks_length.ToString() + " ack bytes to be transmitted to Legacy Controller adr " + ac.activeadr.ToString(), tm.cid.ToString());

            // combine preamble, data, CRC and then postamble
            byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
            System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
            System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

            byte[] crcbytes;
            crcbytes = System.BitConverter.GetBytes(crc);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(crcbytes, 0, crcbytes.Length);
            }
            System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
            System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

            try
            {
                ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                // update stats tracking table TBD
                //CalsenseControllers.BytesSent((int)ac., (int)finalpacket.Length);
                //CalsenseControllers.MsgsSent((int)args.cid, 1);
            }
            catch (Exception e)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "175", "Exception during transmit of ack to legacy controller " + ac.activeadr.ToString() + ", " + e.ToString(), tm.cid.ToString());
                EndTxMessageProcessing(ac, tm, tm.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null, tm.cid);
            }
            mem.Dispose();
        }
        //
        // process a data packet just received
        //
        public static void ProcessDataPacket(ActiveLegacyConnection ac, LegacyClientInfoStruct clientinfo, TransportMessage tm)
        {
            ushort seq;
            byte packet_number;
            bool ack_being_sent = false;

            if(tm.rxmsg == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Warn, "196", "Message processing already complete/timed out, ignoring data packet.", tm.cid.ToString());
                return;
            }
            
            seq = clientinfo.rx[LegacyInternal.OFFSET_SEQ];
            seq <<= 8;
            seq |= clientinfo.rx[LegacyInternal.OFFSET_SEQ + 1];

            // seems like legacy uses reverse byte ordering, so can't do
            // Misc.NetworkBytesToUint16(clientinfo.rx, LegacyInternal.OFFSET_MESSAGEID);
            // if we don't have an rx message ongoing, make one now
            if (tm.rxmsg.Count == 0)
            {
                // this is the message id we will see
                tm.incomingseq = seq;
            }
            else
            {
                // if we already have a partial app rx message, and this is part of a *different* message
                // ignore
                if(seq != tm.incomingseq)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "196", "Received changed sequence, ignoring packet, was " + tm.incomingseq.ToString() + " this one is " + seq.ToString(), tm.cid.ToString());
                    return;
                }
            }

            packet_number = clientinfo.rx[LegacyInternal.OFFSET_THIS_BLOCK];
            // if we already have this packet number for this mid, we can
            // throw this duplicate away
            for (LinkedListNode<byte[]> item = tm.rxmsg.First; item != null; item = item.Next )
            {
                if(item.Value[LegacyInternal.OFFSET_THIS_BLOCK] == packet_number)
                {
                    // received duplicate, ignore
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "19", "Received duplicate data packet, ignoring, from " + ac.activehost.ToString(), tm.cid.ToString());
                    // if asking for an ACK, we need to send an ACK indicating which pieces we
                    // have and have not received
                    if ((clientinfo.rx[LegacyInternal.OFFSET_PID] & LEGACY_SEND_ACK_BIT) != 0)
                    {
                        BuildandTransmitAck(ac, tm);
                    }
                    return;
                }
            }
            //
            // transport optimization - if we have an outstanding TX - this data packet acts as an ACK
            //
            if (tm.tx_retry_timer != -1)
            {
                // all blocks acked - yay! stop the retry timer now
                tm.tx_retry_timer = -1;
                _log.WriteMessageWithId(NLog.LogLevel.Info, "19", "*Data* packet indicates all blocks from " + ac.activehost.ToString() + " now received", tm.cid.ToString());
            }


            _log.WriteMessageWithId(NLog.LogLevel.Debug, "19", "Processing block number " + packet_number.ToString() + " from " + ac.activehost.ToString(), tm.cid.ToString());
            // add this new packet to the list of packets we have for current incoming message
            // we'll go ahead and truncate the max sized rx buffer to the actual rx length
            // so that the array length property reflects the actual number of received bytes

            if (tm.rxmsg == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Warn, "196", "Message processing already complete/timed-out, ignoring data packet.", tm.cid.ToString());
                return;
            }

            // (we make a new array since LegacyInternal will continue using this existing array for further receive data)
            byte[] newArray = (byte[])clientinfo.rx.Clone();
            // set the size of the array to the actual length of data received
            Array.Resize(ref newArray, clientinfo.rxlen);
            tm.rxmsg.AddLast(newArray);

            // if asking for an ACK, we need to send an ACK indicating which pieces we
            // have and have not received
            if ((newArray[LegacyInternal.OFFSET_PID] & LEGACY_SEND_ACK_BIT) != 0)
            {
                BuildandTransmitAck(ac, tm);
                //
                // if error sending Ack (socket closed), get out of here
                //
                ack_being_sent = true;

                // check to see if Transmission of ACK failed due to an exception, which ends message processing.
                if (tm.rxmsg == null)
                {
                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "19", "Exception during ACK transmit; dropping received packet.", tm.cid.ToString());
                    return;
                }
            }

            // existence timer reset each time we get a data packet
            tm.rx_existence_timer = TransportVars.TransportRxExistTimer;

            if (tm.rxmsg == null)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Warn, "196", "Message processing already done/timed out, ignoring data packet.", tm.cid.ToString());
                return;
            }

            // if we have enough unique blocks to make up the entire message then process it
            if (tm.rxmsg.Count == newArray[LegacyInternal.OFFSET_TOTAL_BLOCKS])
            {
                //
                // can cancel receive timers now 
                //
                // tm.rx_existence_timer = -1;  we leave existence timer and rx around so we don't get duplicate if controller resends
                tm.rx_expect_timer = -1;
                ProcessAppMessage(ac, tm);
                // above call takes care of calling EndTxMessageProcessing which will start next message (if any)
            }
            else
            {
                _log.WriteMessageWithId(NLog.LogLevel.Debug, "19", "Received " + tm.rxmsg.Count.ToString() + " so far, expecting total of " + newArray[LegacyInternal.OFFSET_TOTAL_BLOCKS].ToString(), tm.cid.ToString());
                // update receive timers as necessary
                if (ack_being_sent != true)
                {
                    // normal quick timer
                    tm.rx_expect_timer = TransportVars.TransportRxExpectTimer;
                }
                else
                {
                    // sending an ack and waiting for it to have an effect, we use the calculated no reply time
                    // (multiplier * multiplierTimer) + (HubTimer "if controller uses a hub") + (RepeatersTimer "if system use Repeaters") + (Additional timer in DB)
                    var multiplierTimer = tm.ac.activemultiplier == -1 ? TransportVars.TransportTxRetryTimerDefault : TransportVars.TransportTxRetryTimerBase + (TransportVars.TransportTxRetryTimerAddon * tm.ac.activemultiplier);
                    if (ac.hubhandler) multiplierTimer += TransportVars.TransportTxRetryTimerHubAddon;
                    if (ac.activeRepeater) multiplierTimer += TransportVars.TransportTxRetryTimerRepeatersAddon;
                    multiplierTimer += ac.activeDBaddon;

                    tm.rx_expect_timer = multiplierTimer;
                    //if (ac.hubhandler)
                }
                tm.rx_expect_count = TransportVars.TransportRxExpectCount;  // reset count also - we require consecutive failures to give up
            }
        }
        //
        // process a status packet just received
        //
        public static void ProcessStatusPacket(ActiveLegacyConnection ac, LegacyClientInfoStruct clientinfo, TransportMessage tm)
        {
            byte ackslength;
            ulong ackmask;
            int i, blocknumber, j;
            int alldone;

            // if we are waiting for ack to all our packets, see what this one acks...
            if(tm.tx_retry_timer != -1)
            {
                ackmask = 0;   // assume acks all blocks
                ackslength = clientinfo.rx[LegacyInternal.OFFSET_THIS_BLOCK];
                for(i = 0, blocknumber = 1; i < ackslength; i++)
                {
                    // each byte in application data message contains 8 bits representing up to 8 blocks
                    // LSB in first byte is block 1, MSB in first byte is block 8 etc.
                    // if a bit is not set, other end DID NOT get that block
                    // if a bit is set, that block was received
                    for(j = 0; j < 8; j++)
                    {
                        if((clientinfo.rx[LegacyInternal.OFFSET_MESSAGEID + i] & (0x01 << j)) == 0)
                        {
                            // bit not set 
                            ackmask |= 0x01UL << (blocknumber - 1);
                        }
                        blocknumber++;
                    }
                }
                // clear all our bits except what is set in ackmask
                tm.bitmask &= ackmask;

                if(tm.bitmask == 0UL)
                {
                    alldone = 1;
                }
                else
                {
                    alldone = 0;
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "19", "Status packet indicates not all blocks recv'd - resending missing blocks to " + ac.activehost.ToString(), tm.cid.ToString());
                    // not everything acked, resend with refresh retry count
                    tm.tx_retry_count = TransportVars.TransportTxRetryCount;
                    i = InternalXmit(ac, tm, false);
                    if(i == 0)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "19", "Weird status packet - acks length but no blocks to resend, from " + ac.activehost.ToString(), tm.cid.ToString());
                        alldone = 1;
                    }
                }

                if(alldone == 1)
                {

                    // all blocks acked - yay! stop the retry timer now
                    tm.tx_retry_timer = -1;
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "19", "Status packet indicates all blocks now received from " + ac.activehost.ToString(), tm.cid.ToString());

                    // if this is a message that has no response data packet expected, then we are done with this message now
                    switch(tm.txmsg.MID)
                    {
                        case LegacyTasks.MID_DIRECTACCESS_STRUCTURE_TO_CONTROLLER:
                        default:
                            // we now expect the first data packet to follow this ack. if it does not
                            // come, we need to time out.
                            tm.rx_expect_timer = TransportVars.TransportRxExpectTimer;
                            break;
                    }
                }
            }
        }
        //
        // RxPacketReceived is called by Legacy Internal when it has received
        // a validly framed (CRC=good, to address is us, ambles good) packet.
        //
        public static void RxPacketReceived(ActiveLegacyConnection ac, LegacyClientInfoStruct clientinfo)
        {
            TransportMessage tm = null;

            // verify we think we are talking to this controller at transport level
            if(!ActiveTransports.TryGetValue(ac.key, out tm))
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "19", "Received legacy packet when no outstanding transport TX ongoing? from " + ac.activehost.ToString() + ", packet dropped");
                return;
            }

#if POLL_TIMEOUT_RESTART_ON_2000_NECESSARY
            // if this is a 3000 hub connected controller, restart poll timeout on each
            // valid packet received
            // reset poll timer and timeout if active
            if (ac.controllers3000 != null && ac.controllers3000.Count > 0 && ac.controllers != null && ac.controllers.Count > 0 && ac.controllers.Exists(x => x.CID == tm.cid))
            {
                if(ac.controllers.Find(x => x.CID == tm.cid).imBehind3000)
                {
                    if (ac.pollTimeout > 0)
                    {
                        ac.pollTimeout = HubConnectionTracker.GetPollTimeout();   // ticks down from here
                        _log.WriteMessage(NLog.LogLevel.Info, "3044", "Restarting 3000 Poll timeout since RX of slave 2000 packet.");
                    }
                }

            }
#endif

            // if this is a status packet deal with it...
            if ((clientinfo.rx[LegacyInternal.OFFSET_PID] & LEGACY_STATUS_BIT) != 0)
            {
                // make sure status packet is for the outbound message we are currently dealing with
                if (clientinfo.sequence != tm.sequence)
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "19", "Received packet with mismatched sequence number from  " + ac.activehost.ToString() + ", packet dropped");
                    return;
                }
                ProcessStatusPacket(ac, clientinfo, tm);
            }
            else
            {
                // this is a data packet.
                ProcessDataPacket(ac, clientinfo, tm);
            }
        }
        //
        // Normally, a receive condition will complete a transmit operaiont. (i.e. getting ACK to what we sent)
        // In some cases, an error during transmit, or a timeout after retries could happen
        // 
        public static void EndTxMessageProcessing(ActiveLegacyConnection ac, TransportMessage tm, uint jobid, int jobstatus, byte[] sqldata, uint cid)
        {
            try
            {
                if (tm != null)
                {
                    _sem.Wait(_cts.Token);
                    TransportMessage tc;
                    ActiveTransports.TryRemove(ac.key, out tc);
                    if (tc == null)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "19", "Couldn't find expected legacy transport entry for key" + ac.key.ToString(), cid.ToString());
                    }
                    _sem.Release();
                    tm.txmsg = null;    // no reference to the tx msg anymore
                    tm.tx_retry_timer = -1;
                    if (tm.rxmsg != null)    // clear all receive references
                    {
                        tm.rx_existence_timer = -1;
                        tm.rx_expect_timer = -1;
                        tm.rxmsg.Clear();
                        tm.rxmsg = null;
                    }
                }
                _log.WriteMessageWithId(NLog.LogLevel.Info, "999", "TASK END", ac.activecid.ToString());
                ac.TaskActive = false;  // done processing
                LegacyConnectionTracker.totalTaskActiveFrees++;
                // tell connection tracker okay to move on to next message (or close connection if no more)
                LegacyConnectionTracker.RemoveTrackingEntryIfDone(ac);
                // mark job complete if there is a job
                if (jobid > 0)
                {
#pragma warning disable 4014
                    LegacyTasks.CompleteLegacyJob((int)jobid, jobstatus, sqldata);
#pragma warning restore 4014
                }
            }
            catch(Exception e)
            {
                _log.WriteMessage(NLog.LogLevel.Error, "1752", "Exception (will continue) during EndTxMessageProcessing: " + e.ToString());
            }
        }

        //
        // caller has another message to send.
        // buf contains:
        // all the bytes of an application message
        // (if transmit packetization is necessary, that will happen in this module)
        //
        public static void AddTxMessage(ActiveLegacyConnection ac, String hostname, String adr, short MID, byte[] buf, uint cid, uint jobid)
        {
            TransportMessage tm = null;

            // we should already be connected to remote controller - verify that is the case...
            if (ac.sock == null || !ac.sock.Connected)
            {
                _log.WriteMessageWithId(NLog.LogLevel.Error, "654", "Connection to legacy controller lost! Host is: " + ac.activehost.ToString(), cid.ToString());
                EndTxMessageProcessing(ac, null, jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null, cid);
                return;
            }

            // if this is the first message on this connection, we create Transport tracking data entry now
            if (!ActiveTransports.TryGetValue(ac.key, out tm))
            {
                // yup,  this is the first, so make an entry
                _sem.Wait(_cts.Token);
                ActiveTransports.TryAdd(ac.key, new TransportMessage(ac));
                ActiveTransports.TryGetValue(ac.key, out tm);
                _sem.Release();
            }
            if (tm == null)
            {
                // shouldn't ever happen
                _log.WriteMessageWithId(NLog.LogLevel.Error, "654", "Couldn't find/add Transport entry on connection to " + ac.activehost.ToString(), cid.ToString());
                EndTxMessageProcessing(ac, null, jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null, cid);
                return;
            }

            if (tm.txmsg != null)
            {
                // shouldn't happen - we should only be working one message at a time, and
                // setting this back to null when done.
                _log.WriteMessageWithId(NLog.LogLevel.Error, "654", "Transport TX message already in process when trying to send another? " + ac.activehost.ToString(), cid.ToString());
                EndTxMessageProcessing(ac, tm, jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null, cid);
                return;
            }

#if DUMPRAWBYTES
            _log.WriteMessage(NLog.LogLevel.Debug, "718", "Legacy Transport of message Command ID " + MID.ToString(), cid.ToString(), true);
            if (buf != null)
            {
                _log.WriteMessage(NLog.LogLevel.Debug, "718", "Application data bytes: ");
                {
                    int i = buf.Length;
                    int j = 0;
                    while (i-- > 0)
                    {
                        _log.WriteMessage(NLog.LogLevel.Debug, "719", "[" + buf[j].ToString("X") + "]" + " " + buf[j].ToString(), cid.ToString(), true);
                        j++;
                    }
                }
            }
#endif

            // add this message to end of the list
            tm.txmsg = new TransportTxMsgStruct(hostname, adr, MID, buf);
            tm.jobid = jobid;

            // initialize the retry count
            tm.tx_retry_count = TransportVars.TransportTxRetryCount;
            tm.rx_expect_count = TransportVars.TransportRxExpectCount;

            // set all bits to send all packets first time
            tm.bitmask = 0xffffffffffffffff;

            tm.sequence = SequenceCounter;    // next 16-bit number setup for this message
            if (++SequenceCounter > 0x0fff) SequenceCounter = 0;   // wrap at 12 bits

            tm.cid = cid;   // save controller id so we know which controller when time to process app message response

            // and kick off transmission
            InternalXmit(ac, tm, false);
        }


        //
        // called when we want bytes to hit the wire
        // tm.bitmask indicates which packet numbers to include in transmission
        // last packet will request status response
        //
        // return:
        // -1 = error
        // 0 = nothing to send since all acked
        // n = number of blocks sent
        //
        public static int InternalXmit(ActiveLegacyConnection ac, TransportMessage tm, bool last_packet_only)
        {
            int total_packets;
            int num_packets_sent = 0;
            int i;
            int lastpacket = 0;
            int packet_number;
            byte[] finaldata;
            byte[] data;
            UInt32 crc;
            int final_packet_size;

            var mem_data = new MemoryStream(0);

            // 2 byte MID, add it to the payload on the first packet only
            LegacyTasks.LegacyUint16ToNetworkBytes((ushort)tm.txmsg.MID, mem_data);

            // see how many total packets there are in complete message
            if (tm.txmsg.data == null)
            {
                // no application data per se
                total_packets = 1;
            }
            else if (tm.txmsg.data.Length == 0)
            {
                total_packets = 1;
            }
            else
            {
                mem_data.Write(tm.txmsg.data, 0, tm.txmsg.data.Length);
            }

            finaldata = mem_data.ToArray();

            
            //_log.WriteMessageWithId(NLog.LogLevel.Info, "178", "Data to be trasmited to mid: " + tm.txmsg.MID + " :: " + BitConverter.ToString(finaldata), tm.cid.ToString());

            total_packets = finaldata.Length / MAX_USER_DATA_PER_LEGACY_PACKET;
            if ((total_packets * MAX_USER_DATA_PER_LEGACY_PACKET) < finaldata.Length)
            {
                // last packet not completely full
                final_packet_size = finaldata.Length - (total_packets * MAX_USER_DATA_PER_LEGACY_PACKET);
                total_packets++;  
            }
            else
            {
                final_packet_size = MAX_USER_DATA_PER_LEGACY_PACKET;
            }

            //
            // a transport optimization is to only resend the last
            // packet when a retry occurs.
            //
            if(last_packet_only == true)
            {
                tm.bitmask = 1UL << (total_packets - 1);
            }


            // first see what the highest packet we need to send is...
            for (i = 0; i < total_packets; i++)
            {
                // see if this packet is one we need to send by looking at our bitmask
                if ((tm.bitmask & (1UL << i)) != 0)
                {
                    // yes packet "i"
                    lastpacket = i + 1; // keep track of the highest packet number (1 relative) we want to send
                }
            }
            _log.WriteMessageWithId(NLog.LogLevel.Debug, "178", "Command ID " + tm.txmsg.MID + " to be transmitted to Legacy Controller adr " + ac.activeadr.ToString(), tm.cid.ToString());

            // now transmit out the packets on already established socket connection
            // last packet needs SEND_ACK bit set to tell controller to acknowledge 
            for (i = 0; i < total_packets; i++)
            {
                // next packet to send
                if ((tm.bitmask & (1UL << i)) != 0)
                {
                    packet_number = (int)(i + 1); // this packet number, 1 relative
                    var mem = new MemoryStream(0);
                    // PID byte
                    if (packet_number == lastpacket)
                        mem.WriteByte(LEGACY_SEND_ACK_BIT | (MSG_CLASS_2000e_FROM_CENTRAL << 1));       // status bit is 0x80, send ack is 0x40
                    else
                        mem.WriteByte((MSG_CLASS_2000e_FROM_CENTRAL << 1));

                    mem.WriteByte(0);                                       // dummy word align byte

                    // 3 byte from
                    mem.WriteByte(0xff);                                    // central address is 0xff 0xff 0xff
                    mem.WriteByte(0xff);
                    mem.WriteByte(0xff);

                    // 3 character controller address
                    mem.WriteByte((byte)ac.activeadr[0]);
                    mem.WriteByte((byte)ac.activeadr[1]);
                    mem.WriteByte((byte)ac.activeadr[2]);

                    // 2 byte sequence number
                    LegacyTasks.LegacyUint16ToNetworkBytes(tm.sequence, mem);

                    // total number of blocks
                    mem.WriteByte((byte)total_packets);
                    // this block number
                    mem.WriteByte((byte)(i + 1));

                    // this packet data starts at i * MAX_USER_DATA_PER_LEGACY_PACKET
                    // and runs up to MAX_USER_DATA_PER_LEGACY_PACKET more bytes
                    if (packet_number == total_packets) // the ending packet could be partial
                    {
                        mem.Write(finaldata, i * MAX_USER_DATA_PER_LEGACY_PACKET, final_packet_size);
                    }
                    else
                    {
                        mem.Write(finaldata, i * MAX_USER_DATA_PER_LEGACY_PACKET, MAX_USER_DATA_PER_LEGACY_PACKET);
                    }

                    // 4 BYTE CRC
                    // 4 BYTE POSTAMBLE

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessageWithId(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"), tm.cid.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);
                    
                    try
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Info, "175", "Sending Packet: " + (i + 1).ToString() + " , Size: " + finalpacket.Length.ToString() + "  bytes to legacy controller " + ac.activeadr.ToString(), tm.cid.ToString());
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table TBD
                        //CalsenseControllers.BytesSent((int)ac., (int)finalpacket.Length);
                        //CalsenseControllers.MsgsSent((int)args.cid, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "175", "Exception during transmit of command " + tm.txmsg.MID.ToString() + " to legacy controller " + ac.activeadr.ToString() + ", " + e.ToString(), tm.cid.ToString());
                        mem.Dispose();
                        EndTxMessageProcessing(ac, tm, tm.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null, tm.cid);
                        return -1;
                    }

                    // start/restart the tx timer
                    // (multiplier * multiplierTimer) + (HubTimer "if controller uses a hub") + (RepeatersTimer "if system use Repeaters") + (Additional timer in DB)
                    var multiplierTimer = ac.activemultiplier == -1 ? TransportVars.TransportTxRetryTimerDefault : TransportVars.TransportTxRetryTimerBase + (TransportVars.TransportTxRetryTimerAddon * ac.activemultiplier);
                    if (ac.hubhandler) multiplierTimer += TransportVars.TransportTxRetryTimerHubAddon;
                    if (ac.activeRepeater) multiplierTimer += TransportVars.TransportTxRetryTimerRepeatersAddon;
                    multiplierTimer += ac.activeDBaddon;

                    _log.WriteMessageWithId(NLog.LogLevel.Debug, "175", "Computed transport: (RetryTimerBase) + (NOCIC * ChainTimerAddon) + (HubTimerAddon) + (RepeaterAddon) + (DBTimerAddon) = " +
                         TransportVars.TransportTxRetryTimerBase.ToString() + " + (" +
                         TransportVars.TransportTxRetryTimerAddon.ToString() + " * " + ac.activemultiplier.ToString() + ") + " + 
                         (ac.hubhandler ? TransportVars.TransportTxRetryTimerHubAddon.ToString() : "0") + " + " + 
                         (ac.activeRepeater ? TransportVars.TransportTxRetryTimerRepeatersAddon.ToString() : "0") + " + " + ac.activeDBaddon.ToString(), tm.cid.ToString());

                    tm.tx_retry_timer = multiplierTimer;
                    _log.WriteMessageWithId(NLog.LogLevel.Info, "175", "Computed transport retry time is " + tm.tx_retry_timer.ToString() + " secs.", tm.cid.ToString());
                    
                    mem.Dispose();
                    mem_data.Dispose();

                    num_packets_sent++;
                }
            }
            return (num_packets_sent);
        }

        //
        // Long running task/job that handles the active transport messages/timers/retries
        //
        async private static void TransportLoop()
        {
            int reloadSettingsTimer = 60;

            while (_cts == null)
            {
                await Task.Delay(50);  // wait for somebody to tell us the cancellation token
            }

            await Task.Delay(2500);       // let everything spin up...


            while(true)
            {
                reloadSettingsTimer -= TRANSPORT_TIMER_LOOP_SECS;
                if(reloadSettingsTimer <= 0)
                {
                    reloadSettingsTimer = 60;
                    ReloadVars();
                }

                try
                {
                    foreach (var item in ActiveTransports)
                    {
                        TransportMessage tm;

                        tm = item.Value;
                        // look for a transmit retry condition
                        if (tm.tx_retry_timer > 0)
                        {
                            tm.tx_retry_timer -= TRANSPORT_TIMER_LOOP_SECS;
                            if (tm.tx_retry_timer <= 0)
                            {
                                if (tm.tx_retry_count > 0 && tm.ac.sock != null && tm.ac.sock.Connected)
                                {
                                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "175", "TX retry on legacy transport for message  " + tm.txmsg.MID.ToString() + " to legacy controller " + tm.ac.activeadr.ToString(), tm.cid.ToString());
                                    tm.tx_retry_count--;
#pragma warning disable 4014    // no we don't want to await the retransmit - just go do it.
                                    // retransmit the message
                                    Task.Run(() =>
                                        {
                                            InternalXmit(tm.ac, tm, true);    // this will restart tx_retry_timer as part of its process, true indicates send last packet only (transport optimization)
                                        });
#pragma warning restore 4014
                                }
                                else
                                {
                                    // give up on this message, and flush any pending messages for this controller
                                    if (tm.ac.sock == null || !tm.ac.sock.Connected)
                                    {
                                        _log.WriteMessageWithId(NLog.LogLevel.Error, "175", "Detected controller closed socket so not processing Tx Retry timeout on " + tm.ac.activeadr.ToString(), tm.cid.ToString());
                                    }
                                    else
                                    {
                                        _log.WriteMessageWithId(NLog.LogLevel.Error, "175", "Timeout on TX at legacy transport for message  " + tm.txmsg.MID.ToString() + " to legacy controller " + tm.ac.activeadr.ToString(), tm.cid.ToString());
                                    }
                                    tm.tx_retry_timer = -1;
                                    FlushControllerMessages(tm.ac, tm.cid);
                                    EndTxMessageProcessing(tm.ac, tm, tm.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null, tm.cid);
                                }
                            }
                        }


                        // look for a receive timeout condition
                        if (tm.rx_expect_timer > 0)
                        {
                            tm.rx_expect_timer -= TRANSPORT_TIMER_LOOP_SECS;
                            if (tm.rx_expect_timer <= 0)
                            {
                                if (tm.rx_expect_count > 0 && tm.ac.sock != null && tm.ac.sock.Connected)
                                {
                                    _log.WriteMessageWithId(NLog.LogLevel.Warn, "175", "Timeout on RX expecting packet for MID: " + tm.txmsg.MID.ToString() + " to legacy controller " + tm.ac.activeadr.ToString(), tm.cid.ToString());

                                    tm.rx_expect_count--;
#pragma warning disable 4014
                                    // only transmit an ACK if there is something to ack 
                                    if (tm.rxmsg.Count > 0)
                                    {
                                        // we expected another data packet from controller and did not get one
                                        // send another status packet to kick it.
                                        // (multiplier * multiplierTimer) + (HubTimer "if controller uses a hub") + (RepeatersTimer "if system use Repeaters") + (Additional timer in DB)
                                        var multiplierTimer = tm.ac.activemultiplier == -1 ? TransportVars.TransportTxRetryTimerDefault : TransportVars.TransportTxRetryTimerBase + (TransportVars.TransportTxRetryTimerAddon * tm.ac.activemultiplier);
                                        if (tm.ac.hubhandler) multiplierTimer += TransportVars.TransportTxRetryTimerHubAddon;
                                        if (tm.ac.activeRepeater) multiplierTimer += TransportVars.TransportTxRetryTimerRepeatersAddon;
                                        multiplierTimer += tm.ac.activeDBaddon;

                                        tm.rx_expect_timer = multiplierTimer;
                                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "175", "Sending ACK to see if it helps, calc'd expect timer set to:" + tm.rx_expect_timer.ToString() + " secs.", tm.cid.ToString());
                                        Task.Run(() =>
                                        {
                                            BuildandTransmitAck(tm.ac, tm);
                                        });
                                    }
                                    else
                                    {
                                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "175", "Timeout, No Data packet received from controller following its ACK.", tm.cid.ToString());
                                        tm.rx_expect_timer = TransportVars.TransportRxExpectTimer; // look again in a bit
                                    }
#pragma warning restore 4014
                                }
                                else
                                {
                                    // guess we're not going to hear any more from this controller
                                    if (tm.ac.sock == null || !tm.ac.sock.Connected)
                                    {
                                        _log.WriteMessageWithId(NLog.LogLevel.Error, "175", "Detected controller closed socket so not processing Rx Expect timeout on " + tm.ac.activeadr.ToString(), tm.cid.ToString());
                                    }
                                    else
                                    {
                                        _log.WriteMessageWithId(NLog.LogLevel.Error, "175", "Timeout on RX packet at legacy transport for message  " + tm.txmsg.MID.ToString() + " to legacy controller " + tm.ac.activeadr.ToString(), tm.cid.ToString());

                                    }
                                    tm.rx_expect_timer = -1;
                                    FlushControllerMessages(tm.ac, tm.cid);
                                    EndTxMessageProcessing(tm.ac, tm, tm.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null, tm.cid);
                                }
                            }
                        }


                        // look for a receive existence timeout condition
                        if (tm.rx_existence_timer > 0)
                        {
                            tm.rx_existence_timer -= TRANSPORT_TIMER_LOOP_SECS;
                            if (tm.rx_existence_timer <= 0)
                            {
                                // guess we're not going to hear any more from this controller
                                if (tm.rxmsg.Count > 0) tm.rxmsg.Clear();   // throw away what we have
                                _log.WriteMessageWithId(NLog.LogLevel.Warn, "175", "Timeout on RX existence at legacy transport for message  " + tm.txmsg.MID.ToString() + " to legacy controller " + tm.ac.activeadr.ToString(), tm.cid.ToString());
                                tm.rx_expect_timer = -1;                    // probably already timed out anyway.
                                tm.rx_existence_timer = -1;
                                EndTxMessageProcessing(tm.ac, tm, tm.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null, tm.cid);
                            }
                        }

                    }
                }
                catch (Exception e)
                {
                    // possible exception would be an rx message arriving while timeout processing happening, resulting
                    // in tm (TransportMessage) being EndTxMessageProcessing()'ing while timeout processing happening
                    _log.WriteMessage(NLog.LogLevel.Warn, "190", "Exception during TransportLoop timeout processing." + e.ToString());
                }

                try
                {
                    // run every X seconds until exit signaled
                    await Task.Delay(Convert.ToInt32(TRANSPORT_TIMER_LOOP_SECS * 1000), _cts.Token);       
                }
                catch (TaskCanceledException ex)
                {
                    if (_cts.IsCancellationRequested)
                    {
                        _log.WriteMessage(NLog.LogLevel.Debug, "190", "Exiting Legacy Transport task " + ex.ToString());
                        break;
                    }
                }

            }
        }

        //
        // End immediately transmit processing for any queued messages on the given connection for the given controller.
        // Called with a TX timeout or RX timeout has occurred and we don't want to waste time trying to communicate
        // with a controller that isn't responding properly.
        //
        public static void FlushControllerMessages(ActiveLegacyConnection ac, uint ControllerID)
        {
            if(ac != null)
            {
                // lock the priority list so no one messes with it while we search for function to call
                ac.acsem.Wait(_cts.Token);

                //
                // iterate thru queue backwards since we will be removing matching items inside loop
                //
                for (int i = ac.queued_functions.Count - 1; i >= 0; i-- )
                {
                    Tuple<int, Action<ActionArgs>, ActionArgs> tf = ac.queued_functions[i];
                    if(tf.Item3.cid == ControllerID)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "190", "Flushing a message for a controller that did not respond.", ControllerID.ToString());
                        // remove item for this controller from queue
                        ac.queued_functions.RemoveAt(i);
                        // and mark it complete
                        // NOTE: since we know Flush is getting called prior to a call to EndTxMessageProcessing(), we don't
                        // call that here, just remote and mark jobs as complete with all the other EndTxMessageProcessing
                        // logic. That will happen next in the callers code and take care of connection things, since the
                        // original problem message for this controller will be completing there.
                        if (tf.Item3.jobid > 0)
                        {
#pragma warning disable 4014
                            Misc.AddET2000Alert(_cts, tf.Item3.cid, "Unable to connect to the controller", true);
                            LegacyTasks.CompleteLegacyJob((int)tf.Item3.jobid, Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
#pragma warning restore 4014
                        }
                    }
                }

                ac.acsem.Release(); 
            }
        }
    }
}