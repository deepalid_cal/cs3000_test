﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using System.Threading;



//
// our representation of an Alert
//
namespace Calsense.CommServer
{
    class Alert
    {
        public UInt32 time { get; set; }        // seconds past midnight
        public UInt16 date { get; set; }        // Julian day
        public String description { get; set; }     // at most 256 characters
        public UInt16 diag_code { get; set; }
    }
    class AlertList
    {
        public IList<Alert> records;
        public AlertList()
        {
            records = new List<Alert>();
        }
        public void LogIt(MyLogger l, NLog.LogLevel lev)
        {
            int i;

            l.WriteMessage(lev, "8071", "-----Dumping Alerts Message-----");
            for (i = 0; i < this.records.Count; i++ )
            {
                l.WriteMessage(lev, "8071", " ** Record " + i.ToString() + " ** ");
                l.WriteMessage(lev, "8071", "Time:" + this.records[i].time.ToString());
                l.WriteMessage(lev, "8071", "Date:" + this.records[i].date.ToString());
                l.WriteMessage(lev, "8071", "Alert ID:" + this.records[i].diag_code.ToString());
                l.WriteMessage(lev, "8071", "Desc:" + this.records[i].description.ToString());               
            }
        }
    }

}
