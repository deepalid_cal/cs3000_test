﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using Calsense.Logging;
using ServerApp;
using Advantage.Data.Provider;
using Microsoft.Win32;
using System.Configuration;
using System.Net.Sockets;
using System.Xml.Linq;  // for parsing weather response
using System.Net;

namespace Calsense.CommServer
{
    class Misc
    {
        // get timestamp of given binary file
        public static Tuple<DateTime, UInt32> GetBinaryTimeStamp(MyLogger _logger, String pathname, int timestamp_offset)
        {
            UInt32 fsize = 0;
            DateTime thisfile = DateTime.Now;

                try
                {
                    byte[] bytes = new byte[22];
                    String[] components;
                    int n;

                    using (FileStream fsSource = new FileStream(pathname, FileMode.Open, FileAccess.Read))
                    {
                        // 2/6/2014 rmd : We are using the __DATE__ macro in the assembly startup to embed the date
                        // string into the code image at a specific location. Happens to be a different location for
                        // the main code versus the tp micro code. We convert the date string to the number of days
                        // since a starting date. And that becomes the file revision number.

                        // 2/6/2014 rmd : The code revision string in memory has a very specific format. It is as
                        // follows: 0123456789012345678901234567890
                        //          Feb__6_2014_@_10:21:36
                        //          mmm_dd_yyyy_@_hh:mm:ss

                        // Read the source file into a byte array starting at date-time offset
                        fsSource.Seek((long)timestamp_offset, System.IO.SeekOrigin.Begin);
                        n = fsSource.Read(bytes, 0, 22);
                        if (n == 22)
                        {
                            components = System.Text.Encoding.Default.GetString(bytes).Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            if (components.Length != 5)
                            {
                                _logger.WriteMessage(NLog.LogLevel.Error, "6092", "Could not find date-time in .bin file: " + pathname.ToString());
                            }
                            else
                            {
                                _logger.WriteMessage(NLog.LogLevel.Info, "6050", "Date Time for " + pathname.ToString() + ":" + components[0] + ' ' + components[1] + ' ' + components[2] + ' ' + components[3] + ' ' + components[4]);
                                // convert the date-time components into a timestamp
                                int month = DateTime.ParseExact(components[0], "MMM", null).Month;
                                int day = Convert.ToInt32(components[1]);
                                int year = Convert.ToInt32(components[2]);
                                int seconds = (int)TimeSpan.Parse(components[4]).TotalSeconds;

                                thisfile = new DateTime(year, month, day);
                                thisfile = thisfile.AddSeconds((double)seconds);
                                fsize = (UInt32) fsSource.Length;
                            }

                        }

                    }
                }
                catch
                {

                }

        return(Tuple.Create(thisfile, fsize));
        }

        // get force firmware filename for TP Micro
        public static async Task<Tuple<String, DateTime?, UInt32>> GetTPMicroForcedFilename(MyLogger _logger, CancellationTokenSource cts, UInt32 nid)
        {
            String tpmicroname = null;
            DateTime? dt = null;
            bool abort = true;
            AdsCommand cmd;
            UInt32 fsize = 0;


            _logger.WriteMessage(NLog.LogLevel.Info, "9188", "Checking for any forced TP Micro firmware filename in database...");
            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {

                // NOTE: Transactions not supported on local server, so ignore that exception
                // insert data into DB and retrieve new network id to assign to controller
                AdsTransaction txn = null;

                try
                {
                    txn = conn.BeginTransaction();
                }
                catch (System.NotSupportedException)
                {

                }
                do
                {
                    try
                    {
                        cmd = new AdsCommand("SELECT TPMicroForceFirmware FROM CS3000Networks n join controllersites s on s.siteid=n.networkid " +
                            " WHERE s.deleted = false and  n.NetworkID=" + nid.ToString(), conn, txn);
                        _logger.WriteMessage(NLog.LogLevel.Debug, "9189", "SQL: " + cmd.CommandText);
                        var existsName = cmd.ExecuteScalar();
                        if (existsName == null || existsName == DBNull.Value || existsName.ToString().Length < 1)   // last bit shouldn't execute if either of first two true, so ok
                        {
                            // there is no forced filename
                            _logger.WriteMessage(NLog.LogLevel.Debug, "9186", "No forced TP Micro firmware for network id " + nid.ToString());
                        }
                        else
                        {
                            // there is something in database to use
                            tpmicroname = (String)existsName;
                            _logger.WriteMessage(NLog.LogLevel.Info, "9859", "There IS a forced TP Micro firmware name for network id " + nid.ToString() + " which is " + tpmicroname.ToString());
                            // get the timestamp and filesize of forced file
                            Tuple<DateTime, UInt32> result = GetBinaryTimeStamp(_logger, ConfigurationManager.AppSettings["TPFirmwareFolder"] + "\\" + ConfigurationManager.AppSettings["PreReleaseSubfolder"] + "\\" + tpmicroname, Constants.TPMICRO_BUILD_DT_OFFSET);
                            dt = result.Item1;
                            fsize = result.Item2;
                        }
                        cmd.Unprepare();
                        // if we made it to here, we are good to commit all our changes
                        abort = false;
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "9861", "Database exception during check of forced TP Micro firmware: " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "9791", "Database Error on check for forced TP Micro firmware.");
                    txn.Rollback();
                }
                else
                {
                    txn.Commit();
                }
                db.CloseDBConnection(conn, _logger);
            }

            return (Tuple.Create(tpmicroname, dt, fsize));

        }
        // get force firmware filename for mainboard
        public static async Task<Tuple<String, DateTime?, UInt32>> GetMainboardForcedFilename(MyLogger _logger, CancellationTokenSource cts, UInt32 nid)
        {
            String mainboardname = null;
            DateTime? dt = null;
            UInt32 fsize = 0;
            bool abort = true;
            AdsCommand cmd;


            _logger.WriteMessage(NLog.LogLevel.Info, "9188", "Checking for any forced Mainboard firmware filename in database...");
            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {

                // NOTE: Transactions not supported on local server, so ignore that exception
                // insert data into DB and retrieve new network id to assign to controller
                AdsTransaction txn = null;

                try
                {
                    txn = conn.BeginTransaction();
                }
                catch (System.NotSupportedException)
                {

                }
                do
                {
                    try
                    {
                        cmd = new AdsCommand("SELECT MainboardForceFirmware FROM CS3000Networks n join controllersites s on s.siteid=n.networkid " +
                            " WHERE s.deleted = false and  n.NetworkID=" + nid.ToString(), conn, txn);
                        _logger.WriteMessage(NLog.LogLevel.Debug, "9189", "SQL: " + cmd.CommandText);
                        var existsName = cmd.ExecuteScalar();
                        if (existsName == null || existsName == DBNull.Value || existsName.ToString().Length < 1)   // should be ok to use var in third check since first two passed
                        {
                            // there is no forced filename
                            _logger.WriteMessage(NLog.LogLevel.Debug, "9186", "No forced mainboard firmware for network id " + nid.ToString());
                        }
                        else
                        {
                            // there is something in database to use
                            mainboardname = (String)existsName;
                            _logger.WriteMessage(NLog.LogLevel.Info, "9859", "There IS a forced mainboard firmware name for network id " + nid.ToString() + " which is " + mainboardname.ToString());
                            // get the timestamp and filesize of forced file
                            Tuple<DateTime, UInt32> result = GetBinaryTimeStamp(_logger, ConfigurationManager.AppSettings["AppFirmwareFolder"] + "\\" + ConfigurationManager.AppSettings["PreReleaseSubfolder"] + "\\" + mainboardname, Constants.MAIN_APP_BUILD_DT_OFFSET);
                            dt = result.Item1;
                            fsize = result.Item2;

                        }
                        cmd.Unprepare();
                        // if we made it to here, we are good to commit all our changes
                        abort = false;
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "9861", "Database exception during check of forced main firmware: " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "9791", "Database Error on check for forced main firmware.");
                    txn.Rollback();
                }
                else
                {
                    txn.Commit();
                }
                db.CloseDBConnection(conn, _logger);
            }

            return (Tuple.Create(mainboardname, dt, fsize));

        }
        // update the LastCommunicated field in DB for given network ID
        public static async Task<int> LastCommunicated(MyLogger _logger, CancellationTokenSource cts, UInt32 nid)
        {
            bool abort = true;
            AdsCommand cmd;

            if (nid == 0)
            {
                // don't bother, no record to match on a network 0
                return (0);
            }

            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                _logger.WriteMessage(NLog.LogLevel.Info, "1111", "Last Communicated update for NID " + nid.ToString());
                do
                {
                    try
                    {

                        cmd = new AdsCommand("MERGE CS3000Networks ON " +
                            " (NetworkID=" + nid.ToString() +") WHEN MATCHED THEN " +
                            " update set LastCommunicated = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'", conn);
                        _logger.WriteMessage(NLog.LogLevel.Debug, "1115", "SQL: " + cmd.CommandText);
                        cmd.ExecuteNonQuery();
                        cmd.Unprepare();
                        // if we made it to here, we are good to commit all our changes
                        abort = false;
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "1112", "Database exception during Last Updated update " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "1113", "Database Error on Last Updated update.");
                }
                db.CloseDBConnection(conn, _logger);
            }
            return (0);
        }

        // update the LastCommunicated field in DB for given controller ID
        public static int LegacyLastCommunicated(MyLogger _logger, CancellationTokenSource cts, UInt32 cid)
        {
            bool abort = true;
            AdsCommand cmd;

            if (cid == 0)
            {
                // don't bother, no record to match on a network 0
                return (0);
            }

            ServerDB db = new ServerDB(cts);

            AdsConnection connTask = db.OpenDBConnectionNotAsync(_logger);
            AdsConnection conn = connTask;
            if (conn != null)
            {
                _logger.WriteMessageWithId(NLog.LogLevel.Info, "1111", "Last Communicated update", cid.ToString());
                do
                {
                    try
                    {

                        cmd = new AdsCommand("MERGE Controllers ON " +
                            " (ControllerID=" + cid.ToString() + ") WHEN MATCHED THEN " +
                            " update set LastCommunicated = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'", conn);
                        _logger.WriteMessageWithId(NLog.LogLevel.Debug, "1115", "SQL: " + cmd.CommandText, cid.ToString());
                        cmd.ExecuteNonQuery();
                        cmd.Unprepare();
                        // if we made it to here, we are good to commit all our changes
                        abort = false;
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessageWithId(NLog.LogLevel.Error, "1112", "Database exception during Last Updated update " + e.ToString(), cid.ToString(), false);
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessageWithId(NLog.LogLevel.Error, "1113", "Database Error on Last Updated update.", cid.ToString());
                }
                db.CloseDBConnection(conn, _logger);
            }
            return (0);
        }

        // Update the Polling fields in C3000NetworkSettings table for statistics purpose to use in the UI.
        // action: 0 : Update PollingNum when start polling controller
        //         1 : Update FailedPollingNum when we failed. 
        public static int UpdateCS3000PollingCounters(MyLogger _logger, CancellationTokenSource cts, UInt32 nid, UInt32 action)
        {
            bool abort = true;
            AdsCommand cmd;

            if (nid == 0)
            {
                // don't bother, no record to match on a network 0
                return (0);
            }

            ServerDB db = new ServerDB(cts);

            AdsConnection connTask = db.OpenDBConnectionNotAsync(_logger);
            AdsConnection conn = connTask;
            if (conn != null)
            {
                do
                {
                    try
                    {

                        cmd = new AdsCommand("EXECUTE PROCEDURE UpdatePollingsCounters (" + nid.ToString() + "," + action.ToString() + ");", conn);
                        cmd.ExecuteNonQuery();
                        cmd.Unprepare();
                        // if we made it to here, we are good to commit all our changes
                        abort = false;
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessageWithId(NLog.LogLevel.Error, "1112", "Database exception during Updated Polling Counters " + e.ToString(), nid.ToString(), false);
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessageWithId(NLog.LogLevel.Error, "1113", "Database Error on Updated CS3000 Polling Counters", nid.ToString());
                }
                db.CloseDBConnection(conn, _logger);
            }
            return (0);
        }

        // Add alerts message to CS3000Alerts table
        public static async Task<int> AddAlert(CancellationTokenSource cts, UInt32 nid, String description, bool userVisible, bool appendCloudToString)
        {
            MyLogger _logger;
            bool abort = true;
            AdsCommand cmd, ins;
            AdsDataReader rdr;
            int z;

            if (nid == 0)
            {
                // don't bother, no record to match on a network 0
                return (0);
            }

            _logger = new MyLogger("AddAlert", nid.ToString(), nid.ToString(), Constants.CS3000Model);
            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                do
                {
                    try
                    {

                        cmd = new AdsCommand("SELECT TimeZone FROM CS3000Networks n join controllersites s on s.siteid=n.networkid " +
                            " WHERE s.deleted = false and  n.NetworkID= " + nid.ToString(), conn);

                        _logger.WriteMessage(NLog.LogLevel.Debug, "3110", "SQL: " + cmd.CommandText);

                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                if (!rdr.IsDBNull(0))
                                {
                                    z = rdr.GetInt32(0);
                                }
                                else
                                {
                                    z = 0;
                                }
                                
                                // get current time at a Controller
                                DateTime ControllerNow = PeriodicTasks.CurrentTimeAtControllerInTz(z);

                                // insert alert message
                                ins = new AdsCommand("INSERT INTO CS3000Alerts " + 
                                                     "(NetworkID, AlertTimestamp, AlertDiagCode, Description, UserVisible) " +
                                                     "VALUES (" + nid + ",'" + ControllerNow.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',1000,'" +
                                                     description + (appendCloudToString ? " (cloud)'," : "',") + (userVisible ? "TRUE" : "FALSE") + ")", conn);

                                _logger.WriteMessage(NLog.LogLevel.Debug, "3111", "SQL: " + ins.CommandText);

                                ins.ExecuteNonQuery();
                                ins.Unprepare();
                            }
                        }
                        abort = false;
                        rdr.Close();
                        cmd.Unprepare();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "3112", "Database exception during Add Alert message " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "3113", "Database Error on Add Alert message.");
                }
                db.CloseDBConnection(conn, _logger);
            }
            return (0);
        }

        // Add alerts message to Alerts table "ET2000's"
        public static async Task<int> AddET2000Alert(CancellationTokenSource cts, UInt32 cid, String description, bool appendCloudToString)
        {
            MyLogger _logger;
            bool abort = true;
            AdsCommand cmd, ins;
            AdsDataReader rdr;
            string z;

            _logger = new MyLogger("AddET2000Alert", cid.ToString(), cid.ToString(), Constants.CS3000Model);
            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                do
                {
                    try
                    {

                        cmd = new AdsCommand("SELECT Company_TimeZone from Companies where CompanyID = " +
                            "(SELECT s.CompanyID FROM ControllerSites s " +
                            "JOIN Controllers c ON s.SiteID=c.SiteID " +
                            "WHERE ControllerID=" + cid.ToString() + ")", conn);

                        _logger.WriteMessage(NLog.LogLevel.Debug, "3110", "SQL: " + cmd.CommandText);

                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                if (!rdr.IsDBNull(0))
                                {
                                    z = rdr.GetString(0);
                                }
                                else
                                {
                                    _logger.WriteMessage(NLog.LogLevel.Error, "3112", "Couldn't found time zone for legacy controller");
                                    return (-1);
                                }

                                // get current time at a Controller
                                DateTime ControllerNow = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(z));

                                // insert alert message
                                ins = new AdsCommand("INSERT INTO Alerts " +
                                                     "(ControllerID, AlertTimestamp, AlertDiagCode, Description) " +
                                                     "VALUES (" + cid + ",'" + ControllerNow.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',1000,'" +
                                                     description + (appendCloudToString ? " (cloud)')" : "')"), conn);

                                //_logger.WriteMessage(NLog.LogLevel.Debug, "3111", "SQL: " + ins.CommandText);

                                ins.ExecuteNonQuery();
                                ins.Unprepare();
                            }
                        }
                        abort = false;
                        rdr.Close();
                        cmd.Unprepare();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "3112", "Database exception during Add Alert message for legacy controller, ex: " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "3113", "Database Error on Add Alert message for legacy controller.");
                }
                db.CloseDBConnection(conn, _logger);
            }
            return (0);
        }

        // Get company/customer name
        public static async Task<string> getCompanyName(MyLogger _logger, CancellationTokenSource cts, UInt32 id, bool isCS3000)
        {
            bool abort = true;
            AdsCommand cmd;
            AdsDataReader rdr;
            String c = "Uknown";

            if (id == 0)
            {
                // don't bother, no record to match on a network 0
                return c;
            }

            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                do
                {
                    try
                    {
                        if (isCS3000)
                        {
                            cmd = new AdsCommand("SELECT IFNULL(Name,'Unknown') FROM Companies " +
                                                 "WHERE CompanyID IN (SELECT CompanyID FROM ControllerSites WHERE SiteID= " + id.ToString() + ")", conn);
                        }
                        else // ET2000
                        {
                            cmd = new AdsCommand("SELECT IFNULL(Name,'Unknown') FROM Companies " +
                                                 "WHERE CompanyID IN (SELECT s.CompanyID FROM ControllerSites s JOIN Controllers c ON c.SiteID=s.SiteID " +
                                                 "WHERE c.ControllerID=" + id.ToString() + ")", conn);
                        }

                        _logger.WriteMessage(NLog.LogLevel.Debug, "5110", "SQL: " + cmd.CommandText);

                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                c = rdr.GetString(0);
                            }
                        }
                        abort = false;
                        rdr.Close();
                        cmd.Unprepare();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "5111", "Database exception during get company name " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "5112", "Database Error on get company name.");
                }
                db.CloseDBConnection(conn, _logger);
            }
            return (c);
        }

        // Get site name
        public static async Task<string> getControllerName(MyLogger _logger, CancellationTokenSource cts, UInt32 id, bool isCS3000)
        {
            bool abort = true;
            AdsCommand cmd;
            AdsDataReader rdr;
            String c = "Uknown";

            if (id == 0)
            {
                // don't bother, no record to match on a network 0
                return c;
            }

            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                do
                {
                    try
                    {
                        if (isCS3000)
                        {
                            cmd = new AdsCommand("SELECT IFNULL(SiteName,'Unknown') FROM ControllerSites WHERE SiteID= " + id.ToString(), conn);
                        }
                        else // ET2000
                        {
                            cmd = new AdsCommand("SELECT IFNULL(ControllerName,'Unknown') FROM Controllers WHERE ControllerID=" + id.ToString(), conn);
                        }

                        _logger.WriteMessage(NLog.LogLevel.Debug, "5120", "SQL: " + cmd.CommandText);

                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                c = rdr.GetString(0);
                            }
                        }
                        abort = false;
                        rdr.Close();
                        cmd.Unprepare();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "5121", "Database exception during get site name " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "5122", "Database Error on get site name.");
                }
                db.CloseDBConnection(conn, _logger);
            }
            return (c);
        }

        // Get username 
        public static async Task<string> getUsername(MyLogger _logger, CancellationTokenSource cts, Int32 uid)
        {
            bool abort = true;
            AdsCommand cmd;
            AdsDataReader rdr;
            String c = "Uknown";

            if (uid <= 0)
            {
                // don't bother
                return c;
            }

            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                do
                {
                    try
                    {

                        cmd = new AdsCommand("SELECT IFNULL(Usrnm,'Unknown') FROM Usrs WHERE userID= " + uid.ToString(), conn);

                        _logger.WriteMessage(NLog.LogLevel.Debug, "5220", "SQL: " + cmd.CommandText);

                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                c = rdr.GetString(0);
                            }
                        }
                        abort = false;
                        rdr.Close();
                        cmd.Unprepare();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "5121", "Database exception during get user name " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "5122", "Database Error on get user name.");
                }
                db.CloseDBConnection(conn, _logger);
            }
            return (c);
        }

        //
        //  insert a job into the IHSFY_jobs table 
        //
        public static async Task<int> AddNewJobIntoIHSFY(CancellationTokenSource cts, UInt32 nid, uint mid)
        {
            MyLogger _logger;
            bool abort = true;
            AdsCommand cmd;

            if (nid == 0)
            {
                // don't bother, no record to match on a network 0
                return (0);
            }

            _logger = new MyLogger("AddNewJobIntoIHSFY", "", nid.ToString(), Constants.CS3000Model);
            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                do
                {
                    try
                    {
                        cmd = new AdsCommand("INSERT INTO IHSFY_jobs(Command_ID, Network_id, User_id, Date_created, Status) "
                                                + " VALUES(" + mid.ToString() + ","
                                                + nid.ToString() + ","
                                                + "-1,'"
                                                + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                                                + "0)", conn);

                        _logger.WriteMessage(NLog.LogLevel.Debug, "5211", "SQL: " + cmd.CommandText);

                        cmd.ExecuteNonQuery();
                        cmd.Unprepare();
                        abort = false;
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "5212", "Database exception during insert new job: " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "5213", "Database Error on Insert new job into IHSFY_jobs table.");
                }
                db.CloseDBConnection(conn, _logger);
            }
            return (0);
        }

        //
        // Check if the controller was a part of swapping process and the process didn't complete 
        // either because the controller was powered down or because a communication failure. 
        // if so send factory reset command 
        //
        public static async Task<int> NeedToDoFactoryReset(MyLogger _logger, CancellationTokenSource cts, uint nid, uint sn)
        {
            bool abort = true;
            AdsCommand cmd;
            AdsDataReader rdr;
            bool isUnderCalsenseRepair = false;
            int reset_required = Constants.FACTORY_RESET_IS_NOT_REQUIRED;

            if (nid == 0)
            {
                // don't bother, no record to match on a network 0
                return (Constants.FACTORY_RESET_IS_NOT_REQUIRED);
            }

            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection( _logger );
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                do
                {
                    try
                    {
						//_logger.WriteMessage(NLog.LogLevel.Debug, "5219", "Checking to see if controller need a factory reset.");

                        // check if the serial number is under calsense repair accounts
                        cmd = new AdsCommand("SELECT 1 FROM ControllerSites s " +
                                             "JOIN CS3000Networks cs ON s.SiteID = cs.NetworkID " +
                                             "WHERE s.deleted = false and MasterSerialNumber=" + sn.ToString() + 
                                             " AND s.CompanyID=" + Constants.CALSENSE_REPAIR_ACCOUNT_ID.ToString(), conn);

                        //_logger.WriteMessage(NLog.LogLevel.Debug, "5220", "SQL: " + cmd.CommandText);

                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            isUnderCalsenseRepair = true; // controller is a replacement 
                        }
                        rdr.Close();
                        cmd.Unprepare();

                        if (isUnderCalsenseRepair)
                        {
                            // check if we have the same network id under a different account than Calsense Repair
                            cmd = new AdsCommand("SELECT 1 FROM ControllerSites s " +
                                                 "JOIN CS3000Networks cs ON s.SiteID = cs.NetworkID " +
                                                 "WHERE s.deleted = false and NetworkID=" + nid.ToString() +
                                                 " AND s.CompanyID<>" + Constants.CALSENSE_REPAIR_ACCOUNT_ID.ToString(), conn);

                            //_logger.WriteMessage(NLog.LogLevel.Debug, "5220", "SQL: " + cmd.CommandText);

                            rdr = cmd.ExecuteReader();
                            if (rdr.HasRows)
                            {
                                _logger.WriteMessage(NLog.LogLevel.Warn, "5213", "Controller is replacement for another controller, sending a factory reset command, nid: " +
                                                                                 nid + " sn: " + sn);

                                reset_required = Constants.FACTORY_RESET_IS_REQIURED;
                            }

                            rdr.Close();
                            cmd.Unprepare();
                        }

                        abort = false;
                        
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "5211", "Database exception during checking for incomplete swapping process: " + e.ToString());
                        reset_required = Constants.FACTORY_RESET_ERROR_WHILE_CHECKING;
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "5212", "Database Error on check for incomplete swapping process.");
                    reset_required = Constants.FACTORY_RESET_ERROR_WHILE_CHECKING;
                }
                db.CloseDBConnection(conn, _logger);
            }
            return (reset_required);
        }

        //
        // Return report name based on MID
        //
        public static string getReportName(uint mid)
        {
            switch (mid)
            {
                case Constants.MID_TO_COMMSERVER_FLOW_RECORDING_init_packet:
                    return "Flow Recording";
                case Constants.MID_TO_COMMSERVER_STATION_HISTORY_init_packet:
                    return "Station History";
                case Constants.MID_TO_COMMSERVER_STATION_REPORT_DATA_init_packet:
                    return "Station";
                case Constants.MID_TO_COMMSERVER_POC_REPORT_DATA_init_packet:
                    return "POC";
                case Constants.MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_init_packet:
                    return "Mainline";
                case Constants.MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_init_packet:
                    return "Lights";
                case Constants.MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_init_packet:
                    return "ET & Rain table";
                case Constants.MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet:
                case Constants.MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet2:
                    return "Budget";
                case Constants.MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_init_packet:
                    return "Moisture Sensor";

                default: return "UNKNOWN";
            }
        }

        //
        // get the newest item datetime field for field in CS3000NetworksSettings for CS3000 only
        //
        public static async Task<DateTime?> GetReportLastDateTime(MyLogger _log, CancellationTokenSource cts, String colname, uint nid)
        {
            DateTime? dt = null;
            bool abort = true;

            // record in database
            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_log);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                do
                {
                    try
                    {
                        AdsCommand cmd = new AdsCommand("SELECT " + colname + " from CS3000NetworkSettings WHERE NetworkID=" + nid.ToString(), conn);
                        _log.WriteMessageWithId(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText, nid.ToString());
                        var result = cmd.ExecuteScalar();
                        cmd.Unprepare();

                        if (result != null)
                        {
                            if (result != DBNull.Value)
                            {
                                dt = Convert.ToDateTime(result);
                            }
                            else
                            {
                                dt = DateTime.Parse("1900/01/01 00:00:00");
                            }
                        }

                        abort = false;
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "14392", "Database exception during query: " + e.ToString(), nid.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "5243", "Database Error on GetReportLastDateTime.");
                }

                db.CloseDBConnection(conn, nid.ToString());
            }

            return (dt);
        }

        // Get username 
        public static async Task<List<int>> ControllersHasPDtoSend(MyLogger _logger, CancellationTokenSource cts)
        {
            bool abort = true;
            AdsCommand cmd;
            AdsDataReader rdr;
            List<int> nids = new List<int>();

            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                do
                {
                    try
                    {

                        cmd = new AdsCommand("SELECT NetworkID FROM Temp_CS3000Networks WHERE STATUS IN (2,3,10)" +
                        " AND NetworkID IN (SELECT SiteID FROM ControllerSites s JOIN Companies m ON s.CompanyID=m.CompanyID WHERE s.deleted = false and CommServerID=" + Constants.CommServerID + ")", conn);

                        //_logger.WriteMessage(NLog.LogLevel.Debug, "5220", "SQL: " + cmd.CommandText);

                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                nids.Add(rdr.GetInt32(0));
                            }
                        }
                        abort = false;
                        rdr.Close();
                        cmd.Unprepare();
                    }
                    catch (Exception e)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "5123", "Database exception during get a list of controllers have pdata to be sent " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                if (abort == true)
                {
                    _logger.WriteMessage(NLog.LogLevel.Error, "5122", "Database Error on get a list of controllers have pdata to be sent.");
                }
                db.CloseDBConnection(conn, _logger);
            }
            return (nids);
        }

        //
        // Build nak response based on init mid "nack mid is (init mid + 1000)"
        //
        public static List<byte[]> buildNakResponse(uint nid, UInt32 sn, UInt32 mid)
        {
            var mem = new MemoryStream(0);

            // build nak response
            if (nid != 0)
            {
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(sn, mem);
                // network id
                Misc.Uint32ToNetworkBytes(nid, mem);
                // mid (init mid + 1000)
                Misc.Uint16ToNetworkBytes((ushort)(mid + Constants.MID_DIFF_FOR_NAK), mem);
            }

            // get the built response to return and be sent
            byte[] data = mem.ToArray();
            List<byte[]> rsplist = new List<byte[]>();
            rsplist.Add(data);

            mem.Dispose();

            return rsplist;
        }

        //
        // Return IHSFY job parameters
        //
        public static Tuple<Int32?, Int32?, Int32?, Int32?> get_IHSFY_Job_Params(MyLogger _log, CancellationTokenSource _cts, uint nid, uint jobnumber)
        {
            AdsCommand cmd;
            AdsDataReader rdr;
            Int32? p1 = null;
            Int32? p2 = null;
            Int32? p3 = null;
            // skip p4, p4 is a binary column we only use it when we received mobile status screen
            Int32? p5 = null;

            ServerDB db = new ServerDB(_cts);

            AdsConnection conn = db.OpenDBConnectionNotAsync(_log);
            if (conn != null)
            {
                do
                {
                    try
                    {
                        cmd = new AdsCommand("SELECT Idx, Command_id, Network_id, User_id, Date_created, Status, Processing_start, Param1, Param2, Param3, Param5  "
                            + " from IHSFY_jobs where Idx = " + jobnumber.ToString() + " and Network_id = " + nid.ToString(), conn);
                        _log.WriteMessage(NLog.LogLevel.Debug, "184", "SQL: " + cmd.CommandText);

                        rdr = cmd.ExecuteReader();
                        if (!rdr.HasRows)
                        {
                            _log.WriteMessage(NLog.LogLevel.Error, "185", "Could not find job in IHSFY_jobs table for nid: " + nid.ToString() + " with job number " + jobnumber.ToString());
                        }
                        else
                        {
                            rdr.Read();
                            if (rdr.IsDBNull(7))
                            {
                                p1 = null;
                            }
                            else
                            {
                                p1 = rdr.GetInt32(7);
                            }
                            if (rdr.IsDBNull(8))
                            {
                                p2 = null;
                            }
                            else
                            {
                                p2 = rdr.GetInt32(8);
                            }
                            if (rdr.IsDBNull(9))
                            {
                                p3 = null;
                            }
                            else
                            {
                                p3 = rdr.GetInt32(9);
                            }
                            if (rdr.IsDBNull(10))
                            {
                                p5 = null;
                            }
                            else
                            {
                                p5 = rdr.GetInt32(10);
                            }
                        }
                        rdr.Close();
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "179", "Database exception during command id " + jobnumber + " checking: " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                db.CloseDBConnection(conn, _log);
            }

            return (Tuple.Create(p1, p2, p3, p5));
        }

        //
        // Return IHSFY job parameters
        //
        public static Tuple<Int32?, Int32?, Int32?, byte[], Int32?> get_IHSFY_Legacy_Job_Params(MyLogger _log, CancellationTokenSource _cts, uint cid, uint jobnumber)
        {
            AdsCommand cmd;
            AdsDataReader rdr;
            Int32? p1 = null;
            Int32? p2 = null;
            Int32? p3 = null;
            byte[] p4 = null;
            Int32? p5 = null;

            ServerDB db = new ServerDB(_cts);

            AdsConnection conn = db.OpenDBConnectionNotAsync(_log);
            if (conn != null)
            {
                do
                {
                    try
                    {
                        cmd = new AdsCommand("SELECT Idx, Command_id, Network_id, User_id, Date_created, Status, Processing_start, Param1, Param2, Param3, Param4, Param5  "
                            + " from IHSFY_jobs where Idx = " + jobnumber.ToString() + " and Controller_id = " + cid.ToString(), conn);
                        _log.WriteMessage(NLog.LogLevel.Debug, "184", "SQL: " + cmd.CommandText);

                        rdr = cmd.ExecuteReader();
                        if (!rdr.HasRows)
                        {
                            _log.WriteMessage(NLog.LogLevel.Error, "185", "Could not find job in IHSFY_jobs table for cid: " + cid.ToString() + " with job number " + jobnumber.ToString());
                        }
                        else
                        {
                            rdr.Read();
                            if (rdr.IsDBNull(7))
                            {
                                p1 = null;
                            }
                            else
                            {
                                p1 = rdr.GetInt32(7);
                            }
                            if (rdr.IsDBNull(8))
                            {
                                p2 = null;
                            }
                            else
                            {
                                p2 = rdr.GetInt32(8);
                            }
                            if (rdr.IsDBNull(9))
                            {
                                p3 = null;
                            }
                            else
                            {
                                p3 = rdr.GetInt32(9);
                            }
                            if (rdr.IsDBNull(10))
                            {
                                p4 = null;
                            }
                            else
                            {
                                p4 = rdr.GetBytes(10);
                            }
                            if (rdr.IsDBNull(11))
                            {
                                p5 = null;
                            }
                            else
                            {
                                p5 = rdr.GetInt32(11);
                            }
                        }
                        rdr.Close();
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "179", "Database exception during command id " + jobnumber + " checking: " + e.ToString());
                    }
                } while (false);    // allows us to break out early if SQL problem

                db.CloseDBConnection(conn, _log);
            }

            return (Tuple.Create(p1, p2, p3, p4, p5));
        }

        //
        // protocol values are sent little endian on the wire
        // if we are also little endian, no need to swap, else
        // swap before combining
        //
        public static uint NetworkBytesToUint32(byte[] buf, int offset)
        {
            byte[] workingarray = new byte[4];

            Buffer.BlockCopy(buf, offset, workingarray, 0, 4);
            if (BitConverter.IsLittleEndian)
            {
            }
            else
            {
                Array.Reverse(workingarray);
            }
            return(BitConverter.ToUInt32(workingarray, 0));
        }
        public static Single NetworkBytesToSingle(byte[] buf, int offset)
        {
            byte[] workingarray = new byte[4];

            Buffer.BlockCopy(buf, offset, workingarray, 0, 4);
            if (BitConverter.IsLittleEndian)
            {
            }
            else
            {
                Array.Reverse(workingarray);
            }
            return (BitConverter.ToSingle(workingarray, 0));
        }
        public static Double NetworkBytesToDouble(byte[] buf, int offset)
        {
            byte[] workingarray = new byte[8];

            Buffer.BlockCopy(buf, offset, workingarray, 0, 8);
            if (BitConverter.IsLittleEndian)
            {
            }
            else
            {
                Array.Reverse(workingarray);
            }
            return (BitConverter.ToDouble(workingarray, 0));
        }
        public static ushort NetworkBytesToUint16(byte[] buf, int offset)
        {
            byte[] workingarray = new byte[2];

            Buffer.BlockCopy(buf, offset, workingarray, 0, 2);
            if (BitConverter.IsLittleEndian)
            {
            }
            else
            {
                Array.Reverse(workingarray);
            }
            return(BitConverter.ToUInt16(workingarray, 0));
        }
        // legacy uses reverse byte ordering
        public static ushort LegacyBytesToUint16(byte[] buf, int offset)
        {
            byte[] workingarray = new byte[2];

            Buffer.BlockCopy(buf, offset, workingarray, 0, 2);

            Array.Reverse(workingarray);
            
            return (BitConverter.ToUInt16(workingarray, 0));
        }
        // legacy uses reverse byte ordering
        public static uint LegacyBytesToUint32(byte[] buf, int offset)
        {
            byte[] workingarray = new byte[4];

            Buffer.BlockCopy(buf, offset, workingarray, 0, 4);

            Array.Reverse(workingarray);

            return (BitConverter.ToUInt32(workingarray, 0));
        }
        public static void Uint16ToNetworkBytes(UInt16 val, MemoryStream mem)
        {
            if (BitConverter.IsLittleEndian)
            {
                mem.WriteByte((byte)val);
                mem.WriteByte((byte)(val >> 8));
            }
            else
            {
                mem.WriteByte((byte)(val >> 8));
                mem.WriteByte((byte)val);
            }
            return;
        }
        public static void Uint32ToNetworkBytes(UInt32 val, MemoryStream mem)
        {

            if (BitConverter.IsLittleEndian)
            {
                mem.WriteByte((byte)val);
                mem.WriteByte((byte)(val >> 8));
                mem.WriteByte((byte)(val >> 16));
                mem.WriteByte((byte)(val >> 24));
            }
            else
            {
                mem.WriteByte((byte)(val >> 24));
                mem.WriteByte((byte)(val >> 16));
                mem.WriteByte((byte)(val >> 8));
                mem.WriteByte((byte)val);
            }
            return;
        }
        public static void Int32ToNetworkBytes(Int32 val, MemoryStream mem)
        {

            if (BitConverter.IsLittleEndian)
            {
                mem.WriteByte((byte)val);
                mem.WriteByte((byte)(val >> 8));
                mem.WriteByte((byte)(val >> 16));
                mem.WriteByte((byte)(val >> 24));
            }
            else
            {
                mem.WriteByte((byte)(val >> 24));
                mem.WriteByte((byte)(val >> 16));
                mem.WriteByte((byte)(val >> 8));
                mem.WriteByte((byte)val);
            }
            return;
        }
        public static void Uint64ToNetworkBytes(UInt64 val, MemoryStream mem)
        {

            if (BitConverter.IsLittleEndian)
            {
                mem.WriteByte((byte)val);
                mem.WriteByte((byte)(val >> 8));
                mem.WriteByte((byte)(val >> 16));
                mem.WriteByte((byte)(val >> 24));
                mem.WriteByte((byte)(val >> 32));
                mem.WriteByte((byte)(val >> 40));
                mem.WriteByte((byte)(val >> 48));
                mem.WriteByte((byte)(val >> 56));
            }
            else
            {
                mem.WriteByte((byte)(val >> 56));
                mem.WriteByte((byte)(val >> 48));
                mem.WriteByte((byte)(val >> 40));
                mem.WriteByte((byte)(val >> 32));
                mem.WriteByte((byte)(val >> 24));
                mem.WriteByte((byte)(val >> 16));
                mem.WriteByte((byte)(val >> 8));
                mem.WriteByte((byte)val);
            }
            return;
        }
        public static void DoubleToNetworkBytes(Double val, MemoryStream mem)
        {
            byte[] workingarray = BitConverter.GetBytes(val);
            int i;

            if (BitConverter.IsLittleEndian)
            {

            }
            else
            {
                Array.Reverse(workingarray);
            }
            for (i = 0; i < 8; i++)
                mem.WriteByte(workingarray[i]);

            return;
        }
        
        public static void SingleToNetworkBytes(Single val, MemoryStream mem)
        {
            byte[] workingarray = BitConverter.GetBytes(val);
            int i;

            if (BitConverter.IsLittleEndian)
            {

            }
            else
            {
                Array.Reverse(workingarray);
            }
            for (i = 0; i < 4; i++)
                mem.WriteByte(workingarray[i]);

            return;
        }
        //
        // for big endian CRC
        //
        public static void Uint32ToBigendianBytes(UInt32 val, MemoryStream mem)
        {

            if (BitConverter.IsLittleEndian)
            {
                mem.WriteByte((byte)(val >> 24));
                mem.WriteByte((byte)(val >> 16));
                mem.WriteByte((byte)(val >> 8));
                mem.WriteByte((byte)val);
            }
            else
            {
                mem.WriteByte((byte)val);
                mem.WriteByte((byte)(val >> 8));
                mem.WriteByte((byte)(val >> 16));
                mem.WriteByte((byte)(val >> 24));
            }
            return;
        }
        public static String NetworkBytesToString(byte[] buf, int offset, int maxlen)
        {
            String x;
            x = System.Text.Encoding.UTF8.GetString(buf, offset, maxlen);
            int zero = x.IndexOf('\0');
            if (zero >= 0) x = x.Remove(zero);
            return (x);
        }

        //
        // Convert a T=seconds, D=Days since epoch into String SQL will like representing Date/Time
        // In this case, the T time has milliseconds encoded in the top 10 bits, with lower 22 bits
        // representing normal seconds.
        //
        public static String PackedTimeMilliDatetoSQLtimestamp(UInt32 T, UInt16 D)
        {
            DateTime b;
            UInt32 milliseconds;
            UInt32 seconds;

            seconds = T & 0x003fffff;           // only bottom 22 bits are seconds
            milliseconds = (T >> 22) & 0x03ff;  // top 10 bits = milliseconds

            b = DateTime.FromOADate((double)D + 2); // excel/VB bug compenstate for 2 day delta
            b = b.AddSeconds((double)seconds);
            b = b.AddMilliseconds((double)milliseconds);
            return ("'" + b.ToString("d") + " " + b.ToString("HH:mm:ss.fff") + "'");
        }
        
        //
        // Convert a T=seconds, D=Days since epoch into Date/Time
        // In this case, the T time has milliseconds encoded in the top 10 bits, with lower 22 bits
        // representing normal seconds.
        //
        public static DateTime PackedTimeMilliDatetoDatetime(UInt32 T, UInt16 D)
        {
            DateTime b;
            UInt32 milliseconds;
            UInt32 seconds;

            seconds = T & 0x003fffff;           // only bottom 22 bits are seconds
            milliseconds = (T >> 22) & 0x03ff;  // top 10 bits = milliseconds

            b = DateTime.FromOADate((double)D + 2); // excel/VB bug compenstate for 2 day delta
            b = b.AddSeconds((double)seconds);
            b = b.AddMilliseconds((double)milliseconds);
            return b;
        }

        //
        // Convert a T=seconds, D=Days since epoch into String SQL will like representing Date/Time
        //
        public static String PackedTimeDatetoSQLtimestamp(UInt32 T, UInt16 D)
        {
            DateTime b;

            b = DateTime.FromOADate((double)D + 2); // excel/VB bug compenstate for 2 day delta
            b = b.AddSeconds((double)T);
            return("'" + b.ToString("d") + " " + b.ToString("HH:mm:ss") + "'");
        }

        public static DateTime PackedTimeDatetoDateTime(UInt32 T, UInt16 D)
        {
            DateTime b;

            b = DateTime.FromOADate((double)D + 2); // excel/VB bug compenstate for 2 day delta
            b = b.AddSeconds((double)T);
            return (b);
        }

        public static DateTime PackedDatetoDate(UInt16 D)
        {
            DateTime b;

            b = DateTime.FromOADate((double)D + 2); // excel/VB bug compenstate for 2 day delta
            return (b);
        }

        public static UInt16 CurrentPackedDate()
        {
            Double b;
            int val;

            b = DateTime.Now.ToOADate();// get current date/time as 2 byte packed value
            val = (UInt16)b;
            val = val - 2;              // subtract two compensates for excel/VB bug

            return ((UInt16)val);
        }

        public static UInt16 PackedDatefromDateTime(DateTime dt)
        {
            Double b;
            int val;

            b = dt.ToOADate();
            val = (UInt16)b;
            val = val - 2;      // excel/VB bug
            return((UInt16)val);
        }

        public static UInt32 FileCRC(String FullPath)
        {
            byte table_index;
            UInt32 crc;
            Int32 c;

            crc = 0xFFFFFFFF;

            using (FileStream fsSource = new FileStream(FullPath, FileMode.Open, FileAccess.Read))
            {
                while((c = fsSource.ReadByte()) != -1)
                {
                    table_index = (byte)(crc ^ c);
                    crc = Crc32Table[table_index] ^ (crc >> 8);
                }

                crc = crc ^ 0xffffffff;
            }

            return (crc);
        }

        public static UInt32 PackedDateCRC(UInt16 D)
        {
            byte table_index;
            UInt32 crc;
            Int32 c;
            byte one, two;

            if (BitConverter.IsLittleEndian)
            {
                one = (byte)D;
                two = (byte)(D >> 8);

            }
            else
            {
                two = (byte)D;
                one = (byte)(D >> 8);
            }


            crc = 0xFFFFFFFF;

            c = one;
            table_index = (byte)(crc ^ c);
            crc = Crc32Table[table_index] ^ (crc >> 8);

            c = two;
            table_index = (byte)(crc ^ c);
            crc = Crc32Table[table_index] ^ (crc >> 8);

            crc = crc ^ 0xffffffff;

            return (crc);
        }

        //
        // when doing a firmware download, we need to packetize the binary firmware file.
        // to do this we create a List of byte arrays. caller passes in the folder where
        // file is located, and the filename (f) of the firmware to read from.
        //
        public static List<byte[]> CrankoutBinFile(MyLogger _logger, String path, UInt32 sn, UInt32 nid, UInt16 mid)
        {
            List<byte[]> ret = new List<byte[]>();
            MemoryStream mem;
            byte[] hdr;
            UInt16 packet_number = 1;


            // create List of byte arrays from file of DESIRED_PACKET_PAYLOAD size
            try
            {
                byte[] bytes;
                byte[] truncated;
                int n;
                int sofar = 0;
                const int OVERHEAD = 16;    // number of upfront header bytes per data packet

                _logger.WriteMessage(NLog.LogLevel.Info, "6080", "Packetizing firmware file for download " + path.ToString());

                using (FileStream fsSource = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    while (true)
                    {
                        // read next packet worth of bytes into array, offset from beginning by header size
                        bytes = new byte[Constants.DESIRED_PACKET_PAYLOAD + OVERHEAD];
                        n = fsSource.Read(bytes, OVERHEAD, Constants.DESIRED_PACKET_PAYLOAD);
                        if (n <= 0)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Info, "6081", "End of file reached after " + packet_number.ToString() + " packets.");

                            // end of file
                            break;

                        }
                        else {
                            // we have a packet of data, so insert header bytes up front
                            mem = new MemoryStream(0);
                            // PID byte
                            mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                            // message class byte
                            mem.WriteByte(Constants.MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION);
                            // to serial number
                            Misc.Uint32ToNetworkBytes(sn, mem);
                            // network id
                            Misc.Uint32ToNetworkBytes(nid, mem);
                            // mid
                            Misc.Uint16ToNetworkBytes(mid, mem);
                            // packet number
                            Misc.Uint16ToNetworkBytes(packet_number, mem);
                            packet_number++;
                            // mid again
                            Misc.Uint16ToNetworkBytes(mid, mem);

                            // convert to array and place at front of packet of data
                            hdr = mem.ToArray();
                            Array.Copy(hdr, bytes, OVERHEAD);

                            if (n < Constants.DESIRED_PACKET_PAYLOAD)
                            {
                                // last or partial packet
                                _logger.WriteMessage(NLog.LogLevel.Info, "6081", "Partial packet of bytes, so far " + (sofar + n).ToString());
                                truncated = new byte[n + OVERHEAD];
                                Array.Copy(bytes, truncated, n + OVERHEAD);
                                ret.Add(truncated);
                                sofar += n;
                            }
                            else
                            {
                                //_logger.WriteMessage(NLog.LogLevel.Info, "6081", "Full packet of bytes, so far " + (sofar+n).ToString());
                                // full packet
                                ret.Add(bytes);
                                sofar += n;
                            }
                        }

                    }

                }
            }
            catch
            {

            }
            return (ret);
        }

        //
        // when doing a firmware update, we need to look in the given "Folder" for any potential firmware
        // files that are newer that what a controller is running. this routine looks in the passed in
        // "Folder" for the newest firmware file present. it uses the internal date stored in a bin
        // file at the MAIN_APP_BUILD_DT_OFFSET offset of the file. by scanning each file in the directory,
        // we keep track of the newest date/time found at that offset and return the filename, and also
        // the timestamp and filesize (these latter two via out parameters).
        //
        public static String FindNewestBinFile(MyLogger _logger, String Folder, int timestamp_offset, out DateTime file_dt, out UInt32 fsize)
        {
            DateTime mostrecent, thisfile;
            String result;

            mostrecent = new DateTime(2000, 1, 1);
            // default return values assume we don't find anything valid
            result = null;
            file_dt = new DateTime(2000, 1, 1);
            fsize = 0;


            List<FileSystemInfo> orderedFiles = new List<FileSystemInfo>();

            DirectoryInfo di = new DirectoryInfo(Folder);
            try
            {
                FileSystemInfo[] files = di.GetFileSystemInfos();
                orderedFiles = files.Where(f => f.Name.EndsWith(".BIN", StringComparison.OrdinalIgnoreCase))
                                        .OrderByDescending(f => f.LastWriteTime)
                                        .ToList();
            }
            catch
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "6099", "Error scanning for firmware .bin files in " + Folder);

            }

            foreach (FileSystemInfo f in orderedFiles)
            {
                _logger.WriteMessage(NLog.LogLevel.Info, "6091", "File: " + f.ToString());
                // read string at MAIN_APP_BUILD_DT_OFFSET to get version of firmware
                try
                {
                    byte[] bytes = new byte[22];
                    String[] components;
                    int n;
                    using (FileStream fsSource = new FileStream(Folder + "\\" + f.ToString(), FileMode.Open, FileAccess.Read))
                    {
                        // 2/6/2014 rmd : We are using the __DATE__ macro in the assembly startup to embed the date
                        // string into the code image at a specific location. Happens to be a different location for
                        // the main code versus the tp micro code. We convert the date string to the number of days
                        // since a starting date. And that becomes the file revision number.

                        // 2/6/2014 rmd : The code revision string in memory has a very specific format. It is as
                        // follows: 0123456789012345678901234567890
                        //          Feb__6_2014_@_10:21:36
                        //          mmm_dd_yyyy_@_hh:mm:ss

                        // Read the source file into a byte array starting at date-time offset
                        fsSource.Seek((long)timestamp_offset, System.IO.SeekOrigin.Begin);
                        n = fsSource.Read(bytes, 0, 22);
                        if (n == 22)
                        {
                            components = System.Text.Encoding.Default.GetString(bytes).Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            if (components.Length != 5)
                            {
                                _logger.WriteMessage(NLog.LogLevel.Error, "6092", "Could not find date-time in .bin file: " + f.ToString());
                            }
                            else
                            {
                                _logger.WriteMessage(NLog.LogLevel.Info, "6050", "Date Time for " + f.ToString() + ":" + components[0] + ' ' + components[1] + ' ' + components[2] + ' ' + components[3] + ' ' + components[4]);
                                // convert the date-time components into a timestamp
                                int month = DateTime.ParseExact(components[0], "MMM", null).Month;
                                int day = Convert.ToInt32(components[1]);
                                int year = Convert.ToInt32(components[2]);
                                int seconds = (int)TimeSpan.Parse(components[4]).TotalSeconds;

                                thisfile = new DateTime(year, month, day);
                                thisfile = thisfile.AddSeconds((double)seconds);
                                if(thisfile > mostrecent)
                                {
                                    // currently, this is the newest .bin file
                                    mostrecent = thisfile;
                                    result = f.ToString();
                                    file_dt = thisfile;
                                    // assume for our purposes that all bin files have a length that
                                    // can fit in 32 bits - so cast to that size from long
                                    fsize = (UInt32) fsSource.Length;
                                    _logger.WriteMessage(NLog.LogLevel.Info, "6050", "Newest .bin file scanned so far " + f.ToString() + " with internal date-time of " + file_dt.ToString());

                                }
                            }

                        }

                    }
                }
                catch
                {

                }
            }
            return (result);
        }

        //
        // when doing a program data request, we need to turn a bunch of bytes into
        // packets of appropriate size.
        //
        public static List<byte[]> MemoryStreamToPackets(MyLogger _logger, MemoryStream m, UInt32 sn, UInt32 nid, UInt16 mid)
        {
            List<byte[]> ret = new List<byte[]>();
            MemoryStream mem;
            byte[] hdr;
            UInt16 packet_number = 1;


            //_logger.WriteMessage(NLog.LogLevel.Info, "8881", "Entry for Pgm Data packetizer");

            // create List of byte arrays from file of DESIRED_PACKET_PAYLOAD size
            try
            {
                byte[] bytes;
                byte[] truncated;
                int n;
                int sofar = 0;
                const int OVERHEAD = 16;    // number of upfront header bytes per data packet

                _logger.WriteMessage(NLog.LogLevel.Info, "8880", "Packetizing program data for transmission, length = " + m.Length.ToString());
                m.Seek(0, SeekOrigin.Begin);    // to beginning of the stream
                while (true)
                {
                    // read next packet worth of bytes into array, offset from beginning by header size
                    bytes = new byte[Constants.DESIRED_PACKET_PAYLOAD + OVERHEAD];
                    n = m.Read(bytes, OVERHEAD, Constants.DESIRED_PACKET_PAYLOAD);
                    if (n <= 0)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Info, "8881", "End of memory stream reached after " + (packet_number-1).ToString() + " packets.");

                        // end of file
                        break;

                    }
                    else
                    {
                        // we have a packet of data, so insert header bytes up front
                        mem = new MemoryStream(0);
                        // PID byte
                        mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                        // message class byte
                        mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                        // to serial number
                        Misc.Uint32ToNetworkBytes(sn, mem);
                        // network id
                        Misc.Uint32ToNetworkBytes(nid, mem);
                        // mid
                        Misc.Uint16ToNetworkBytes(mid, mem);
                        // packet number
                        Misc.Uint16ToNetworkBytes(packet_number, mem);
                        packet_number++;
                        // mid again
                        Misc.Uint16ToNetworkBytes(mid, mem);

                        // convert to array and place at front of packet of data
                        hdr = mem.ToArray();
                        Array.Copy(hdr, bytes, OVERHEAD);

                        if (n < Constants.DESIRED_PACKET_PAYLOAD)
                        {
                            // last or partial packet
                            _logger.WriteMessage(NLog.LogLevel.Info, "8881", "Partial packet of bytes, so far " + (sofar + n).ToString());
                            truncated = new byte[n + OVERHEAD];
                            Array.Copy(bytes, truncated, n + OVERHEAD);
                            ret.Add(truncated);
                            sofar += n;
                        }
                        else
                        {
                            //_logger.WriteMessage(NLog.LogLevel.Info, "6081", "Full packet of bytes, so far " + (sofar+n).ToString());
                            // full packet
                            ret.Add(bytes);
                            sofar += n;
                        }
                    }

                }
            }
            catch(Exception e)
            {
                _logger.WriteMessage(NLog.LogLevel.Warn, "8881", "EXCEPTION in Pgm Data packetizer: " + e.ToString());
            }

            //_logger.WriteMessage(NLog.LogLevel.Info, "8881", "Exit for Pgm Data packetizer");

            return (ret);
        }

        private static readonly UInt32[] Crc32Table = new UInt32[256] { 
            0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,	0xe963a535,
	        0x9e6495a3, 0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988, 0x09b64c2b,	0x7eb17cbd,
	        0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de,	0x1adad47d,
	        0x6ddde4eb, 0xf4d4b551, 0x83d385c7, 0x136c9856, 0x646ba8c0, 0xfd62f97a,	0x8a65c9ec,
	        0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e,	0xd56041e4,
	        0xa2677172, 0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa,	0x42b2986c,
	        0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,	0x26d930ac,
	        0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423, 0xcfba9599,	0xb8bda50f,
	        0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924, 0x2f6f7c87, 0x58684c11,	0xc1611dab,
	        0xb6662d3d, 0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a, 0x71b18589,	0x06b6b51f,
	        0x9fbfe4a5, 0xe8b8d433, 0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818,	0x7f6a0dbb,
	        0x086d3d2d, 0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8,	0xf262004e,
	        0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,	0x8bbeb8ea,
	        0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65, 0x4db26158,	0x3ab551ce,
	        0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb,	0x4369e96a,
	        0x346ed9fc, 0xad678846, 0xda60b8d0, 0x44042d73, 0x33031de5, 0xaa0a4c5f,	0xdd0d7cc9,
	        0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3,	0xb966d409,
	        0xce61e49f, 0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17,	0x2eb40d81,
	        0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,	0xead54739,
	        0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84, 0x0d6d6a3e,	0x7a6a5aa8,
	        0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1, 0xf00f9344, 0x8708a3d2,	0x1e01f268,
	        0x6906c2fe, 0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7, 0xfed41b76,	0x89d32be0,
	        0x10da7a5a, 0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,	0xd6d6a3e8,
	        0xa1d1937e, 0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd,	0x48b2364b,
	        0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,	0x316e8eef,
	        0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236, 0xcc0c7795,	0xbb0b4703,
	        0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04,	0xc2d7ffa7,
	        0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d, 0x9b64c2b0, 0xec63f226, 0x756aa39c,	0x026d930a,
	        0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14,	0x7bb12bae,
	        0x0cb61b38, 0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4,	0xf1d4e242,
	        0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,	0x88085ae6,
	        0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69, 0x616bffd3,	0x166ccf45,
	        0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7,	0x4969474d,
	        0x3e6e77db, 0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0, 0xa9bcae53,	0xdebb9ec5,
	        0x47b2cf7f, 0x30b5ffe9, 0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6,	0xbad03605,
	        0xcdd70693, 0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02,	0x2a6f2b94,
	        0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d };

        public static UInt32 CRC(byte[] data)
        {
            UInt32 i;
            byte table_index;
            UInt32 crc;

            crc = 0xFFFFFFFF;

            for (i = 0; i < data.Length; i++)
            {
                table_index = (byte)(crc ^ data[i]);
                crc = Crc32Table[table_index] ^ (crc >> 8);
            }

            crc = crc ^ 0xffffffff;

            return (crc);
        }

        // similar to above for partially filled in arrays, and does byte order
        public static UInt32 OrderedCRC(byte[] data, UInt32 offset, int len)
        {
            UInt32 i;
            byte table_index;
            UInt32 crc;

            crc = 0xFFFFFFFF;

            for (i = 0; i < len; i++)
            {
                table_index = (byte)(crc ^ data[i+offset]);
                crc = Crc32Table[table_index] ^ (crc >> 8);
            }

            crc = crc ^ 0xffffffff;

            // return the CRC in protocol order
            if (BitConverter.IsLittleEndian)
            {
                return (crc & 0x000000FFU) << 24 | (crc & 0x0000FF00U) << 8 |
                       (crc & 0x00FF0000U) >> 8 | (crc & 0xFF000000U) >> 24;
            }
            else
            {
                return (crc);
            }
        }
        //
        // string used when logging to uniquely identify remote controller
        // (for filtering display)
        //
        public static string ClientLogString(ClientInfoStruct c)
        {
            if (c.nid == 0) return ("");       // unknown controller at this point
            else return (c.nid.ToString());
        }

        //
        // string used when logging into database to uniquely identify remote controller
        // (for filtering display)
        //
        public static string ClientLogDB(ClientInfoStruct c)
        {
            if (c.nid == 0) return ("");       // unknown controller at this point
            else return (c.nid.ToString());
        }

        public static void LogMsg(MyLogger _logger, byte[] finalpacket, int len)
        {
            uint i;
            int j;
            String s;
            for (i = 0, j = len; i < len; i += 8)
            {
                if (j >= 8)
                {
                    s = finalpacket[i].ToString("x2") + " " + finalpacket[i + 1].ToString("x2") + " " + finalpacket[i + 2].ToString("x2") + " " + finalpacket[i + 3].ToString("x2") + " " + finalpacket[i + 4].ToString("x2") + " " + finalpacket[i + 5].ToString("x2") + " " + finalpacket[i + 6].ToString("x2") + " " + finalpacket[i + 7].ToString("x2");
                    j -= 8;
                }
                else
                {
                    s = "";
                    while (j > 0)
                    {
                        s = s + finalpacket[i].ToString("x2") + " ";
                        j--;
                        i++;
                    }
                }
                _logger.WriteMessage(NLog.LogLevel.Debug, "3045", s);
                if (j <= 0) break;
            }
        }
       
    }
}
