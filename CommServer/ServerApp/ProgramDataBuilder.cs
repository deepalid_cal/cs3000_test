﻿using System;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using Advantage.Data.Provider;


namespace Calsense.CommServer
{

    public class ProgramDataBuilder
    {

        //DLLEXPORT INT_32 PDATA_get_change_bit_number( char *ptable_name, char *pfield_name, UNS_32 *psize_of_var_ptr )
        // C routine to get bit number given table name and column name
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int PDATA_get_change_bit_number(
            StringBuilder ptable_name,
            StringBuilder pfield_name,
            ref uint psize_of_var_ptr,
            ref uint pindex);

        private static void ObjectToStream(MemoryStream str, Object o, int fixedlen, MyLogger _logger)
        {
            DateTime b;
            UInt32 d;
            System.TimeSpan t;

            //_logger.WriteMessage(NLog.LogLevel.Warn, "9998", "Stream size before object " + o.GetType().FullName.ToString() + " write is: " + str.Length.ToString());

            switch (Type.GetTypeCode(o.GetType()))
            {
                case TypeCode.Object:
                    switch(o.GetType().FullName.ToString())
                    {
                        case "System.TimeSpan":
                            if (fixedlen == 4)
                            {
                                // 4 bytes of seconds indicating time of day
                                t = (TimeSpan)o;
                                d = (uint)t.Seconds;
                                d += (uint)t.Minutes * 60;
                                d += (uint)t.Hours * 60 * 60;
                                Misc.Uint32ToNetworkBytes(d, str);
                            }
                            else
                            {
                                _logger.WriteMessage(NLog.LogLevel.Error, "499", "Object type is " + o.GetType().FullName.ToString());
                            }
                            break;
                        default:
                            _logger.WriteMessage(NLog.LogLevel.Error, "499", "Object type is " + o.GetType().FullName.ToString());
                            break;
                    }
                    break;

                case TypeCode.Boolean:
                    Misc.Uint32ToNetworkBytes(Convert.ToUInt32(o), str);
                    break;
                case TypeCode.DateTime:
                    if(fixedlen == 4)
                    {
                        // just want the date part not time
                        b = Convert.ToDateTime(o);  // date
                        // remember the 2 day excel/VB bug, compensate by subtracting two days
                        Misc.Uint32ToNetworkBytes(Convert.ToUInt32(b.ToOADate())-2, str); // integer component of double is number of days since epoch
                    }
                    else if(fixedlen == 6)
                    {
                        // want packed date and time (2 byte date, then 4 byte time)
                        b = Convert.ToDateTime(o);  // date
                        // remember the 2 day excel/VB bug, compensate by subtracting two days
                        Misc.Uint16ToNetworkBytes((ushort)(Convert.ToUInt32(b.ToOADate()) - 2), str); // integer component of double is number of days since epoch
                        d = (uint)b.Second;
                        d += (uint)b.Minute * 60;
                        d += (uint)b.Hour * 60 * 60;
                        Misc.Uint32ToNetworkBytes(d, str);
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "499", "Object type is " + o.GetType().FullName.ToString());
                    }
                    break;
                case TypeCode.String:
                    str.Write(ASCIIEncoding.Default.GetBytes(Convert.ToString(o).PadRight(fixedlen, '\0')), 0, fixedlen);
                    //_logger.WriteMessage(NLog.LogLevel.Warn, "999", "ASCII String is " + BitConverter.ToString(ASCIIEncoding.Default.GetBytes(Convert.ToString(o).PadRight(fixedlen, '\0'))));
                    //_logger.WriteMessage(NLog.LogLevel.Debug, "488", "Object string wrote this many: " + Convert.ToString(o).PadRight(fixedlen, '\0').Length.ToString());
                    break;
                case TypeCode.Byte:
                case TypeCode.SByte:
                    _logger.WriteMessage(NLog.LogLevel.Warn, "499", "Object type is " + o.GetType().FullName.ToString());
                    break;

                case TypeCode.Int16:
                case TypeCode.UInt16:
                    // it looks like all the "shortint" values are still transmitted as 4 byte ints, so fall into that
                case TypeCode.Int32:
                    if (fixedlen != 4)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "444", "Program Data Object width different than expected! Expected 4 got : " + fixedlen.ToString());
                    }
                    Misc.Int32ToNetworkBytes(Convert.ToInt32(o), str);
                    break;
                case TypeCode.UInt32:
                    if (fixedlen != 4)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "444", "Program Data Object width different than expected! Expected 4 got : " + fixedlen.ToString());
                    }
                    Misc.Uint32ToNetworkBytes(Convert.ToUInt32(o), str);
                    break;
                case TypeCode.Double:
                    if (fixedlen == 4)
                    {
                        // float values to be transmitted as 4 byte Single 
                        Misc.SingleToNetworkBytes(Convert.ToSingle(o), str);
                    }
                    else
                    {
                        Misc.DoubleToNetworkBytes(Convert.ToDouble(o), str);
                    }
                    break;
                case TypeCode.UInt64:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Single:
                    _logger.WriteMessage(NLog.LogLevel.Error, "499", "Object type is " + o.GetType().FullName.ToString());
                    break;
                default:
                    _logger.WriteMessage(NLog.LogLevel.Error, "499", "Object type is " + o.GetType().FullName.ToString());
                    break;
            }
            //_logger.WriteMessage(NLog.LogLevel.Warn, "9998", "Stream size after object write is: " + str.Length.ToString());
        }

        //
        // Build changes found in the Temp_CS3000Lights table
        //

        public static int Lights(MemoryStream str, StringBuilder tablename, DataTable data, DataTable schema, String id)
        {
            uint varsize;
            int bitnumber;
            uint bitNumberIdx;
            MyLogger _logger;
            uint max;
            UInt32 changebits;

            _logger = new MyLogger(typeof(ProgramDataBuilder).Name, id, id, Constants.CS3000Model);

            // Number of changed Lights is number of data rows we have
            max = (uint)data.Rows.Count;
            _logger.WriteMessage(NLog.LogLevel.Debug, "410", "Number of changed Lights is " + max.ToString());

            if (max == 0) return (0);   // nothing to look at

            Misc.Uint32ToNetworkBytes(max, str);    // number of changed items is the number of rows we have

            foreach (DataRow row in data.Rows)
            {
                List<PdataMessage> pdata = new List<PdataMessage>();

                // construct our change bit mask from column names
                changebits = 0;
                string[] colnames = row["Columns"].ToString().Split(',').Select(cname => cname.Trim()).ToArray();   // string array of column names

                foreach (string column in colnames)
                {
                    varsize = 0;
                    bitNumberIdx = 0;
                    bitnumber = PDATA_get_change_bit_number(tablename, new StringBuilder(column), ref varsize, ref bitNumberIdx);
                    
                    if ((Int32)bitnumber != -1)
                    {
                        if (row[column] is DBNull)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "411", "Ignoring column " + column.ToString() + "* DB val is NULL");
                        }
                        else
                        {
                            changebits = changebits | ((UInt32)1 << (Int32)bitnumber);
                            pdata.Add(new PdataMessage(bitnumber, bitNumberIdx, column, varsize));
                            _logger.WriteMessage(NLog.LogLevel.Debug, "411", "Column name " + column.ToString() +
                                               " has bit number " + bitnumber.ToString() +
                                               " with index " + bitNumberIdx.ToString() +
                                               " and size " + varsize.ToString());
                        }
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "411", "Couldn't find column name " + column.ToString() + " in " + tablename);
                    }
                }

                // Box Index and OutputIndex always present for each light
                ObjectToStream(str, row["BoxIndex"], sizeof(UInt32), _logger);
                ObjectToStream(str, row["OutputIndex"], sizeof(UInt32), _logger);

                // size of change bits
                Misc.Uint32ToNetworkBytes(sizeof(UInt32), str);

                // change bits for this box
                Misc.Uint32ToNetworkBytes(changebits, str);

                // order the list by bit number and bit number index to make sure the message build on the right order
                foreach (PdataMessage p in pdata.OrderBy(v => v.BitNumber).ThenBy(v => v.BitNumberIdx).ToList())
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "413", "Yes *" + p.Column.ToString() + "* is present in colnames...DB val is: " + row[p.Column].ToString());
                    ObjectToStream(str, row[p.Column], (int)p.VarSize, _logger);
                }

            }
            return (0);
        }

        //
        // Build changes found in the Temp_CS3000Stations table
        //

        public static int Stations(MemoryStream str, StringBuilder tablename, DataTable data, DataTable schema, String id)
        {
            uint varsize;
            int bitnumber;
            uint bitNumberIdx;
            MyLogger _logger;
            uint max;
            UInt32 changebits;

            _logger = new MyLogger(typeof(ProgramDataBuilder).Name, id, id, Constants.CS3000Model);

            // Number of changed Stations is number of data rows we have
            max = (uint)data.Rows.Count;
            _logger.WriteMessage(NLog.LogLevel.Debug, "410", "Number of changed Stations is " + max.ToString());

            if (max == 0)return(0);   // nothing to look at

            Misc.Uint32ToNetworkBytes(max, str);    // number of changed items is the number of rows we have
            
            foreach(DataRow row in data.Rows)
            {
                List<PdataMessage> pdata = new List<PdataMessage>();

                // construct our change bit mask from column names
                changebits = 0;
                string[] colnames = row["Columns"].ToString().Split(',').Select(cname=>cname.Trim()).ToArray();   // string array of column names

                foreach (string column in colnames)
                {
                    varsize = 0;
                    bitNumberIdx = 0;
                    bitnumber = PDATA_get_change_bit_number(tablename, new StringBuilder(column), ref varsize, ref bitNumberIdx);
                    
                    if ((Int32)bitnumber != -1)
                    {
                        if (row[column] is DBNull)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "411", "Ignoring column " + column.ToString() + "* DB val is NULL");
                        }
                        else
                        {
                            changebits = changebits | ((UInt32)1 << (Int32)bitnumber);
                            pdata.Add(new PdataMessage(bitnumber, bitNumberIdx, column, varsize));
                            _logger.WriteMessage(NLog.LogLevel.Debug, "411", "Column name " + column.ToString() +
                                               " has bit number " + bitnumber.ToString() +
                                               " with index " + bitNumberIdx.ToString() +
                                               " and size " + varsize.ToString());
                        }
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "411", "Couldn't find column name " + column.ToString() + " in " + tablename);
                    }
                }

                // Box Index and Station Number always present for each station
                ObjectToStream(str, row["BoxIndex"], sizeof(UInt32), _logger);
                ObjectToStream(str, row["StationNumber"], sizeof(UInt32), _logger);

                // size of change bits
                Misc.Uint32ToNetworkBytes(sizeof(UInt32), str);

                // change bits for this box
                Misc.Uint32ToNetworkBytes(changebits, str);

                // order the list by bit number and bit number index to make sure the message build on the right order
                foreach (PdataMessage p in pdata.OrderBy(v => v.BitNumber).ThenBy(v => v.BitNumberIdx).ToList())
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "413", "Yes *" + p.Column.ToString() + "* is present in colnames...DB val is: " + row[p.Column].ToString());
                    ObjectToStream(str, row[p.Column], (int)p.VarSize, _logger);
                }

            }
            return (0);
        }

        //
        // Build changes found in the Temp_CS3000POCs table
        //
 
        public static int POC(MemoryStream str, StringBuilder tablename, DataTable data, DataTable schema, String id)
        {
            uint varsize;
            int bitnumber; 
            uint bitNumberIdx;
            MyLogger _logger;
            uint max;
            UInt32 changebits;

            _logger = new MyLogger(typeof(ProgramDataBuilder).Name, id, id, Constants.CS3000Model);

            // Number of changed POCs is number of data rows we have
            max = (uint)data.Rows.Count;
            _logger.WriteMessage(NLog.LogLevel.Debug, "410", "Number of changed POCs is " + max.ToString());

            if (max == 0)return(0);   // nothing to look at
            
            Misc.Uint32ToNetworkBytes(max, str);    // number of changed items is the number of rows we have
            
            foreach(DataRow row in data.Rows)
            {
                List<PdataMessage> pdata = new List<PdataMessage>();

                // construct our change bit mask from column names
                changebits = 0;
                string[] colnames = row["Columns"].ToString().Split(',').Select(cname=>cname.Trim()).ToArray();   // string array of column names

                foreach (string column in colnames)
                {
                    varsize = 0;
                    bitNumberIdx = 0;
                    bitnumber = PDATA_get_change_bit_number(tablename, new StringBuilder(column), ref varsize, ref bitNumberIdx);

                    if ((Int32)bitnumber != -1)
                    {
                        if (row[column] is DBNull)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "411", "Ignoring column " + column.ToString() + "* DB val is NULL");
                        }
                        else
                        {
                            changebits = changebits | ((UInt32)1 << (Int32)bitnumber);
                            pdata.Add(new PdataMessage(bitnumber, bitNumberIdx, column, varsize));
                            _logger.WriteMessage(NLog.LogLevel.Debug, "411", "Column name " + column.ToString() +
                                                " has bit number " + bitnumber.ToString() +
                                                " with index " + bitNumberIdx.ToString() +
                                                " and size " + varsize.ToString());
                        }
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "411", "Couldn't find column name " + column.ToString() + " in " + tablename);
                    }
                }

                // poc, boxindex, type, serialnumbers
                ObjectToStream(str, row["POC_GID"], sizeof(UInt32), _logger);
                ObjectToStream(str, row["BoxIndex"], sizeof(UInt32), _logger);
                ObjectToStream(str, row["TypeOfPOC"], sizeof(UInt32), _logger);
                ObjectToStream(str, row["DecoderSerialNumber1"], sizeof(UInt32), _logger);
                ObjectToStream(str, row["DecoderSerialNumber2"], sizeof(UInt32), _logger);
                ObjectToStream(str, row["DecoderSerialNumber3"], sizeof(UInt32), _logger);

                // size of change bits
                Misc.Uint32ToNetworkBytes(sizeof(UInt32), str);

                // change bits for this box
                Misc.Uint32ToNetworkBytes(changebits, str);

                // order the list by bit number and bit number index to make sure the message build on the right order
                foreach (PdataMessage p in pdata.OrderBy(v => v.BitNumber).ThenBy(v => v.BitNumberIdx).ToList())
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "413", "Yes *" + p.Column.ToString() + "* is present in colnames...DB val is: " + row[p.Column].ToString());
                    ObjectToStream(str, row[p.Column], (int)p.VarSize, _logger);
                }

            }
            return (0);
        }

        //
        // Build changes found in the Temp_CS3000Weather table
        //

        public static int Weather(MemoryStream str, StringBuilder tablename, DataTable data, DataTable schema, String id)
        {
            uint varsize;
            int bitnumber;
            uint bitNumberIdx;
            MyLogger _logger;
            uint max;
            UInt32 changebits;

            _logger = new MyLogger(typeof(ProgramDataBuilder).Name, id, id, Constants.CS3000Model);

            // Number of changed Weathers is number of data rows we have LIKELY ALWAYS ONE!
            max = (uint)data.Rows.Count;
            _logger.WriteMessage(NLog.LogLevel.Debug, "410", "Number of changed Weathers is " + max.ToString());

            if (max == 0) return (0);   // nothing to look at
            
            foreach (DataRow row in data.Rows)
            {
                List<PdataMessage> pdata = new List<PdataMessage>();

                // construct our change bit mask from column names
                changebits = 0;
                string[] colnames = row["Columns"].ToString().Split(',').Select(cname => cname.Trim()).ToArray();   // string array of column names

                foreach (string column in colnames)
                {
                    varsize = 0;
                    bitNumberIdx = 0;
                    bitnumber = PDATA_get_change_bit_number(tablename, new StringBuilder(column), ref varsize, ref bitNumberIdx);

                    if ((Int32)bitnumber != -1)
                    {
                        if (row[column] is DBNull)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "411", "Ignoring column " + column.ToString() + "* DB val is NULL");
                        }
                        else
                        {
                            changebits = changebits | ((UInt32)1 << (Int32)bitnumber);
                            pdata.Add(new PdataMessage(bitnumber, bitNumberIdx, column, varsize));
                            _logger.WriteMessage(NLog.LogLevel.Debug, "411", "Column name " + column.ToString() +
                                                " has bit number " + bitnumber.ToString() +
                                                " with index " + bitNumberIdx.ToString() +
                                                " and size " + varsize.ToString());
                        }
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "411", "Couldn't find column name " + column.ToString() + " in " + tablename);
                    }
                }

                // size of change bits
                Misc.Uint32ToNetworkBytes(sizeof(UInt32), str);

                // change bits for this box
                Misc.Uint32ToNetworkBytes(changebits, str);

                // order the list by bit number and bit number index to make sure the message build on the right order
                foreach (PdataMessage p in pdata.OrderBy(v => v.BitNumber).ThenBy(v => v.BitNumberIdx).ToList())
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "413", "Yes *" + p.Column.ToString() + "* is present in colnames...DB val is: " + row[p.Column].ToString());
                    ObjectToStream(str, row[p.Column], (int)p.VarSize, _logger);
                }

            }
            return (0);
        }

        //
        // Build changes found in the Temp_CS3000Systems table
        //

        public static int Systems(MemoryStream str, StringBuilder tablename, DataTable data, DataTable schema, String id)
        {
            uint varsize;
            int bitnumber;
            uint bitNumberIdx;
            MyLogger _logger;
            uint max;
            UInt32 changebits;

            _logger = new MyLogger(typeof(ProgramDataBuilder).Name, id, id, Constants.CS3000Model);

            // Number of changed Systems is number of data rows we have
            max = (uint)data.Rows.Count;
            _logger.WriteMessage(NLog.LogLevel.Debug, "410", "Number of changed Systems is " + max.ToString());

            if (max == 0) return (0);   // nothing to look at
            
            Misc.Uint32ToNetworkBytes(max, str);    // number of changed items is the number of rows we have

            foreach (DataRow row in data.Rows)
            {
                List<PdataMessage> pdata = new List<PdataMessage>();

                // construct our change bit mask from column names
                changebits = 0;
                string[] colnames = row["Columns"].ToString().Split(',').Select(cname => cname.Trim()).ToArray();   // string array of column names

                foreach (string column in colnames)
                {
                    varsize = 0;
                    bitNumberIdx = 0;
                    bitnumber = PDATA_get_change_bit_number(tablename, new StringBuilder(column), ref varsize, ref bitNumberIdx);

                    if ((Int32)bitnumber != -1)
                    {
                        if (row[column] is DBNull)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "411", "Ignoring column " + column.ToString() + "* DB val is NULL");
                        }
                        else
                        {
                            changebits = changebits | ((UInt32)1 << (Int32)bitnumber);
                            pdata.Add(new PdataMessage(bitnumber, bitNumberIdx, column, varsize));
                            _logger.WriteMessage(NLog.LogLevel.Debug, "411", "Column name " + column.ToString() +
                                                " has bit number " + bitnumber.ToString() +
                                                " with index " + bitNumberIdx.ToString() +
                                                " and size " + varsize.ToString());
                        }
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "411", "Couldn't find column name " + column.ToString() + " in " + tablename);
                    }
                }

                // poc, boxindex, type, serialnumbers
                ObjectToStream(str, row["SystemGID"], sizeof(UInt32), _logger);

                // size of change bits
                Misc.Uint32ToNetworkBytes(sizeof(UInt32), str);

                // change bits for this box
                Misc.Uint32ToNetworkBytes(changebits, str);

                // order the list by bit number and bit number index to make sure the message build on the right order
                foreach (PdataMessage p in pdata.OrderBy(v => v.BitNumber).ThenBy(v => v.BitNumberIdx).ToList())
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "413", "Yes *" + p.Column.ToString() + "* is present in colnames...DB val is: " + row[p.Column].ToString());
                    ObjectToStream(str, row[p.Column], (int)p.VarSize, _logger);
                }

            }
            return (0);
        }

        //
        // Build changes found in the Temp_CS3000StationGroups table
        //

        public static int StationGroups(MemoryStream str, StringBuilder tablename, DataTable data, DataTable schema, String id)
        {
            uint varsize;
            int bitnumber;
            uint bitNumberIdx;
            MyLogger _logger;
            uint max;
            UInt64 changebits;

            _logger = new MyLogger(typeof(ProgramDataBuilder).Name, id, id, Constants.CS3000Model);

            // Number of changed Station Groups is number of data rows we have
            max = (uint)data.Rows.Count;
            _logger.WriteMessage(NLog.LogLevel.Debug, "410", "Number of changed Station Groups is " + max.ToString());

            if (max == 0) return (0);   // nothing to look at

            Misc.Uint32ToNetworkBytes(max, str);    // number of changed items is the number of rows we have
            
            foreach (DataRow row in data.Rows)
            {
                List<PdataMessage> pdata = new List<PdataMessage>();

                // construct our change bit mask from column names
                changebits = 0;
                string[] colnames = row["Columns"].ToString().Split(',').Select(cname => cname.Trim()).ToArray();   // string array of column names

                foreach (string column in colnames)
                {
                    varsize = 0;
                    bitNumberIdx = 0;
                    bitnumber = PDATA_get_change_bit_number(tablename, new StringBuilder(column), ref varsize, ref bitNumberIdx);

                    if ((Int32)bitnumber != -1)
                    {
                        if (row[column] is DBNull)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "411", "Ignoring column " + column.ToString() + "* DB val is NULL");
                        }
                        else
                        {
                            changebits = changebits | ((UInt64)1 << (Int32)bitnumber);
                            pdata.Add(new PdataMessage(bitnumber, bitNumberIdx, column, varsize));
                            _logger.WriteMessage(NLog.LogLevel.Debug, "411", "Column name " + column.ToString() +
                                                " has bit number " + bitnumber.ToString() +
                                                " with index " + bitNumberIdx.ToString() +
                                                " and size " + varsize.ToString());
                        }
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "411", "Couldn't find column name " + column.ToString() + " in " + tablename);
                    }
                }

                // poc, boxindex, type, serialnumbers
                ObjectToStream(str, row["StationGroupGID"], sizeof(UInt32), _logger);

                // size of change bits
                Misc.Uint32ToNetworkBytes(sizeof(UInt64), str);

                // change bits for this box
                Misc.Uint64ToNetworkBytes(changebits, str);

                // order the list by bit number and bit number index to make sure the message build on the right order
                foreach (PdataMessage p in pdata.OrderBy(v => v.BitNumber).ThenBy(v => v.BitNumberIdx).ToList())
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "413", "Yes *" + p.Column.ToString() + "* is present in colnames...DB val is: " + row[p.Column].ToString());
                    ObjectToStream(str, row[p.Column], (int)p.VarSize, _logger);
                }

            }
            return (0);
        }

        //
        // Build changes found in the Temp_CS3000ManualPrograms table
        //

        public static int ManualPrograms(MemoryStream str, StringBuilder tablename, DataTable data, DataTable schema, String id)
        {
            uint varsize;
            int bitnumber;
            uint bitNumberIdx;
            MyLogger _logger;
            uint max;
            UInt32 changebits;

            _logger = new MyLogger(typeof(ProgramDataBuilder).Name, id, id, Constants.CS3000Model);

            // Number of changed Manual Programs is number of data rows we have
            max = (uint)data.Rows.Count;
            _logger.WriteMessage(NLog.LogLevel.Debug, "410", "Number of changed Manual Programs is " + max.ToString());

            if (max == 0) return (0);   // nothing to look at
            
            Misc.Uint32ToNetworkBytes(max, str);    // number of changed items is the number of rows we have

            foreach (DataRow row in data.Rows)
            {
                List<PdataMessage> pdata = new List<PdataMessage>();

                // construct our change bit mask from column names
                changebits = 0;
                string[] colnames = row["Columns"].ToString().Split(',').Select(cname => cname.Trim()).ToArray();   // string array of column names

                foreach (string column in colnames)
                {
                    varsize = 0;
                    bitNumberIdx = 0;
                    bitnumber = PDATA_get_change_bit_number(tablename, new StringBuilder(column), ref varsize, ref bitNumberIdx);

                    if ((Int32)bitnumber != -1)
                    {
                        if (row[column] is DBNull)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "411", "Ignoring column " + column.ToString() + "* DB val is NULL");
                        }
                        else
                        {
                            changebits = changebits | ((UInt32)1 << (Int32)bitnumber);
                            pdata.Add(new PdataMessage(bitnumber, bitNumberIdx, column, varsize));
                            _logger.WriteMessage(NLog.LogLevel.Debug, "411", "Column name " + column.ToString() +
                                                " has bit number " + bitnumber.ToString() +
                                                " with index " + bitNumberIdx.ToString() +
                                                " and size " + varsize.ToString());
                        }
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "411", "Couldn't find column name " + column.ToString() + " in " + tablename);
                    }
                }

                // group id
                ObjectToStream(str, row["ProgramGID"], sizeof(UInt32), _logger);

                // size of change bits
                Misc.Uint32ToNetworkBytes(sizeof(UInt32), str);

                // change bits for this box
                Misc.Uint32ToNetworkBytes(changebits, str);

                // order the list by bit number and bit number index to make sure the message build on the right order
                foreach (PdataMessage p in pdata.OrderBy(v => v.BitNumber).ThenBy(v => v.BitNumberIdx).ToList())
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "413", "Yes *" + p.Column.ToString() + "* is present in colnames...DB val is: " + row[p.Column].ToString());
                    ObjectToStream(str, row[p.Column], (int)p.VarSize, _logger);
                }

            }                
            return (0);
        }

        //
        // Build changes found in the Temp_CS3000MoistureSensors table
        //

        public static int MoistureSensors(MemoryStream str, StringBuilder tablename, DataTable data, DataTable schema, String id)
        {
            uint varsize;
            int bitnumber;
            uint bitNumberIdx;
            MyLogger _logger;
            uint max;
            UInt32 changebits;

            _logger = new MyLogger(typeof(ProgramDataBuilder).Name, id, id, Constants.CS3000Model);

            // Number of changed Moisture Sensors is number of data rows we have
            max = (uint)data.Rows.Count;
            _logger.WriteMessage(NLog.LogLevel.Debug, "410", "Number of changed Moisture Sensors is " + max.ToString());

            if (max == 0) return (0);   // nothing to look at
            
            Misc.Uint32ToNetworkBytes(max, str);    // number of changed items is the number of rows we have

            foreach (DataRow row in data.Rows)
            {
                List<PdataMessage> pdata = new List<PdataMessage>();

                // construct our change bit mask from column names
                changebits = 0;
                string[] colnames = row["Columns"].ToString().Split(',').Select(cname => cname.Trim()).ToArray();   // string array of column names

                foreach (string column in colnames)
                {
                    varsize = 0;
                    bitNumberIdx = 0;
                    bitnumber = PDATA_get_change_bit_number(tablename, new StringBuilder(column), ref varsize, ref bitNumberIdx);

                    if ((Int32)bitnumber != -1)
                    {
                        if (row[column] is DBNull)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "411", "Ignoring column " + column.ToString() + "* DB val is NULL");
                        }
                        else
                        {
                            changebits = changebits | ((UInt32)1 << (Int32)bitnumber);
                            pdata.Add(new PdataMessage(bitnumber, bitNumberIdx, column, varsize));
                            _logger.WriteMessage(NLog.LogLevel.Debug, "411", "Column name " + column.ToString() +
                                                " has bit number " + bitnumber.ToString() +
                                                " with index " + bitNumberIdx.ToString() +
                                                " and size " + varsize.ToString());
                        }
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "411", "Couldn't find column name " + column.ToString() + " in " + tablename);
                    }
                }

                // DecoderSerialNumber always present for each moisture
                ObjectToStream(str, row["DecoderSerialNumber"], sizeof(UInt32), _logger);

                // size of change bits
                Misc.Uint32ToNetworkBytes(sizeof(UInt32), str);

                // change bits for this box
                Misc.Uint32ToNetworkBytes(changebits, str);

                // order the list by bit number and bit number index to make sure the message build on the right order
                foreach (PdataMessage p in pdata.OrderBy(v => v.BitNumber).ThenBy(v => v.BitNumberIdx).ToList())
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "413", "Yes *" + p.Column.ToString() + "* is present in colnames...DB val is: " + row[p.Column].ToString());
                    ObjectToStream(str, row[p.Column], (int)p.VarSize, _logger);
                }

            }
            return (0);
        }

        //
        // Build changes found in the Temp_CS3000Networks table
        //

        public static int Networks(MemoryStream str, StringBuilder tablename, DataTable data, DataTable schema, String id)
        {
            uint varsize;
            int bitnumber;
            uint bitNumberIdx;
            MyLogger _logger;
            uint max;
            UInt32 changebits;

            _logger = new MyLogger(typeof(ProgramDataBuilder).Name, id, id, Constants.CS3000Model);

            // Number of changed Networks is number of data rows we have LIKELY ALWAYS ONE!
            max = (uint)data.Rows.Count;
            _logger.WriteMessage(NLog.LogLevel.Debug, "410", "Number of changed Networks is " + max.ToString());

            if (max == 0) return (0);   // nothing to look at
            
            foreach (DataRow row in data.Rows)
            {
                List<PdataMessage> pdata = new List<PdataMessage>();

                // construct our change bit mask from column names
                changebits = 0;
                string[] colnames = row["Columns"].ToString().Split(',').Select(cname => cname.Trim()).ToArray();   // string array of column names

                foreach (string column in colnames)
                {
                    varsize = 0;
                    bitNumberIdx = 0;
                    bitnumber = PDATA_get_change_bit_number(tablename, new StringBuilder(column), ref varsize, ref bitNumberIdx);

                    // check if the column is exists
                    if ((Int32)bitnumber != -1)
                    {
                        if (row[column] is DBNull)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "411", "Ignoring column " + column.ToString() + "* DB val is NULL");
                        }
                        else
                        {
                            changebits = changebits | ((UInt32)1 << (Int32)bitnumber);
                            pdata.Add(new PdataMessage(bitnumber, bitNumberIdx, column, varsize));
                            _logger.WriteMessage(NLog.LogLevel.Debug, "411", "Column name " + column.ToString() + 
                                                " has bit number " + bitnumber.ToString() + 
                                                " with index " + bitNumberIdx.ToString() +
                                                " and size " + varsize.ToString());
                        }
                    }
                    else
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "411", "Couldn't find column name " + column.ToString() + " in " + tablename);
                    }
                }

                // size of change bits
                Misc.Uint32ToNetworkBytes(sizeof(UInt32), str);

                // change bits for this box
                Misc.Uint32ToNetworkBytes(changebits, str);

                // order the list by bit number and bit number index to make sure the message build on the right order
                foreach (PdataMessage p in pdata.OrderBy(v => v.BitNumber).ThenBy(v => v.BitNumberIdx).ToList())
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "413", "Yes *" + p.Column.ToString() + "* is present in colnames...DB val is: " + row[p.Column].ToString());
                    ObjectToStream(str, row[p.Column], (int)p.VarSize, _logger);
                }
            }
            return (0);
        }

        private class PdataMessage
        {
            public Int32 BitNumber      { get; private set; }
            public uint BitNumberIdx    { get; private set; }
            public String Column        { get; private set; }
            public uint VarSize         { get; private set; }

            public PdataMessage(Int32 bitNumber, uint bitNumberIdx, String column, uint varSize)
            {
                BitNumber = bitNumber;
                BitNumberIdx = bitNumberIdx;
                Column = column;
                VarSize = varSize;
            }
        }
    }
}
