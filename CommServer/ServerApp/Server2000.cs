﻿//
// Server2000.cs
//
// Starts the Inbound 2000 controller processes
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Calsense.Logging;
using ServerApp;
using System.Collections.Concurrent;
using System.Runtime.ExceptionServices;

namespace Calsense.CommServer
{
    public class Server2000 : IDisposable
    {
        public readonly int Port2000;
        private readonly TcpListener _tcpListener;
        private readonly Task _listenTask;
        public bool Is2000Running { get; private set; }
        Socket _nextSocket;
        private MyLogger _logger;

        public Server2000(int port, CancellationTokenSource cts, ConcurrentBag<Task>tl)
            : this(IPAddress.Any, port, cts, tl)
        {
        }

        public Server2000(IPAddress listeningAddress, int port, CancellationTokenSource cts, ConcurrentBag<Task>tl)
        {
            Port2000 = port;

            _logger = new MyLogger(this.GetType().Name, "", "", Constants.ET2000Model);

            Is2000Running = true;
            _tcpListener = null;
            _listenTask = null;

            //ServerSocketTracker.SetCts(cts);    // tell the static socket tracker what the cancellation token is

            // Start listening
            _logger.WriteMessage(NLog.LogLevel.Debug, "2001", "2000 Inbound Starting...");
            _tcpListener = new TcpListener(listeningAddress, Port2000);
            _tcpListener.Start();
            _logger.WriteMessage(NLog.LogLevel.Debug, "2002", "2000 Inbound Started.");
            // Start a background thread to listen for incoming
            _listenTask = Task.Factory.StartNew(() =>
                {
                    ListenLoop(cts, tl);
                }
                );
        }

        private void ListenLoop(CancellationTokenSource cts, ConcurrentBag<Task>tl)
        {
            // get access to runtime control settings via singleton
            WebFormData wf = WebFormData.Instance;

            if (System.Threading.Thread.CurrentThread.Name == null)
            {
                System.Threading.Thread.CurrentThread.Name = "2000 Connection Lstnr";
            }

            while (_tcpListener.Server.IsBound && !cts.IsCancellationRequested)
            {

                try
                {
                    _logger.WriteMessage(NLog.LogLevel.Debug, "2003", "Awaiting 2000 connection...");
                    // Wait for connection
                    _nextSocket = null;
                    //_nextSocket = await _tcpListener.AcceptSocketAsync().ConfigureAwait(false);
                    _nextSocket = _tcpListener.AcceptSocket();
                    if (_nextSocket == null)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "2020", "NULL Socket returned from AcceptSocket??");
                        break;
                    }

                    if (wf.IgnoreMode)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Warn, "2009", "Ignoring 2000 inbound request from " + IPAddress.Parse(((IPEndPoint)_nextSocket.RemoteEndPoint).Address.ToString()) + " because Ignore Mode enabled.");
                        continue;
                    }
                    
                    CancellationTokenSource _child_cts = CancellationTokenSource.CreateLinkedTokenSource(cts.Token);

                    // Got new connection, create a client handler for it
                    _logger.WriteMessage(NLog.LogLevel.Info, "2004", "2000 Connection made from " + IPAddress.Parse(((IPEndPoint)_nextSocket.RemoteEndPoint).Address.ToString()) +
                        " on remote port: " + ((IPEndPoint)_nextSocket.RemoteEndPoint).Port.ToString());
                    var client = new Inbound2000Internal(this, _nextSocket, _child_cts);

                    // Create a task to handle new connection
                    if(tl != null){     // if someone keeping a list of tasks active, add created task to list
                        tl.Add(Task.Factory.StartNew(client.Do));
                    }
                    else {              // otherwise just create the task
                        Task.Factory.StartNew(client.Do);
                    }
                }
                catch (SocketException e)
                {
                    if (e.SocketErrorCode == SocketError.Interrupted)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Debug, "2005", "2000 Inbound Socket terminating normally.");

                        break;
                    }
                    else throw;
                }
                finally
                {

                }
            }

        }

        public void Dispose()
        {
            if (_tcpListener != null)
            {
                _logger.WriteMessage(NLog.LogLevel.Debug, "2009", "2000 Inbound Stopping...");
                _tcpListener.Stop();
                _logger.WriteMessage(NLog.LogLevel.Debug, "2010", "2000 Inbound Stopped.");
            }
        }
    }
}
