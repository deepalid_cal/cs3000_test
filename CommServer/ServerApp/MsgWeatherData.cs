﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using Advantage.Data.Provider;



namespace Calsense.CommServer
{

    public class MsgWeatherData
    {
        // test call unmanged C code
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint Dummy(ref DUMMY_STRUCT pp,
        uint why,
        [In, Out] byte[] dest,
        uint sz,
        uint idx);


        //
        // Parses Weather Data inbond message from controller
        // returns via Tuple:
        //   List of byte arrays, which are 0 to n messages to send as a response
        //   String that will be passed back in if this connection stays up
        //   Boolean as a flag to terminate connection or not
        //   int as an internal state to track thru session (if needed)
        //
        public static async Task<Tuple<List<byte[]>,String, bool, int>> Parse(ClientInfoStruct client, CancellationTokenSource cts)
        {
            byte[] data;
            MyLogger _logger;
            uint size;
            byte[] Buffer = null;
            AdsCommand cmd;
            bool abort = true;
            WeatherData wd = new WeatherData();
            byte bitfield;


            int offset;

            _logger = new MyLogger(typeof(MsgWeatherData).Name, Misc.ClientLogString(client), Misc.ClientLogDB(client), Constants.CS3000Model);
            _logger.WriteMessage(NLog.LogLevel.Info, "9304", "Processing Weather Data message...");

            size = client.rxlen;
            Buffer = client.rx;

            offset = 0;
            wd.stamp = DateTime.Now;
            bitfield = Buffer[offset++];
            if ((bitfield & (1 << Constants.WEATHER_DATA_HAS_ET)) != 0)
            {
                wd.et_value = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                wd.et_status = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
            }
            else
            {
                wd.et_status = null;
                wd.et_value = null;
            }
            if ((bitfield & (1 << Constants.WEATHER_DATA_HAS_RAIN)) != 0)
            {
                wd.rain_value = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
                wd.rain_status = Misc.NetworkBytesToUint16(Buffer, offset); offset += sizeof(UInt16);
            }
            else
            {
                wd.rain_status = null;
                wd.rain_value = null;
            }

#if DUMPRAWBYTES
            _logger.WriteMessage(NLog.LogLevel.Info, "3004", "Dump of bytes raw:" + size.ToString());
            for (i = 0, j = (int)size; i < size; i += 8)
            {
                if (j >= 8)
                {
                    s = Buffer[i].ToString("x2") + " " + Buffer[i + 1].ToString("x2") + " " + Buffer[i + 2].ToString("x2") + " " + Buffer[i + 3].ToString("x2") + " " + Buffer[i + 4].ToString("x2") + " " + Buffer[i + 5].ToString("x2") + " " + Buffer[i + 6].ToString("x2") + " " + Buffer[i + 7].ToString("x2");
                    j -= 8;
                }
                else
                {
                    s = "";
                    while (j > 0)
                    {
                        s = s + Buffer[i].ToString("x2") + " ";
                        j--;
                        i++;
                    }
                }
                _logger.WriteMessage(NLog.LogLevel.Debug, "8001", s);
                if (j <= 0) break;
                Thread.Sleep(10);       // allow UDP logger to catch up
            }
#endif
            // record record in database
            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                // @WB Check if the Network id in DB before insert records.
                cmd = new AdsCommand("SELECT ISNULL(NetworkID,0) FROM CS3000Networks n join controllersites s on s.siteid=n.networkid  WHERE s.deleted = false and  n.NetworkID=" + client.nid, conn);
                var existsID = cmd.ExecuteScalar();
                cmd.Unprepare();
                if (existsID == null)
                {
                    // we won't insert records
                    _logger.WriteMessage(NLog.LogLevel.Error, "9319", "Network id is not in DB. SN:" + client.sn.ToString() + ", nid:" + client.nid.ToString());
                }
                else
                {
                    _logger.WriteMessage(NLog.LogLevel.Info, "9320", "Inserting Weather Data row into DB...");
                    // NOTE: Transactions not supported on local server, so ignore that exception
                    // insert data into DB and retrieve new network id to assign to controller
                    AdsTransaction txn = null;

                    try
                    {
                        txn = conn.BeginTransaction();
                    }
                    catch (System.NotSupportedException)
                    {

                    }
                    do
                    {
                        try
                        {
                            var etstatstr = wd.et_status == null ? "NULL" : wd.et_status.ToString();
                            var etvalstr = wd.et_value == null ? "NULL" : wd.et_value.ToString();
                            var rainstatstr = wd.rain_status == null ? "NULL" : wd.rain_status.ToString();
                            var rainvalstr = wd.rain_value == null ? "NULL" : wd.rain_value.ToString();
                            cmd = new AdsCommand("MERGE CS3000WeatherData ON " +
                                " (NetworkID=" + client.nid.ToString() +
                                " AND WeatherTimeStamp='" + wd.stamp.ToString("yyyy-MM-dd HH:mm:ss.fff") +
                                "') WHEN NOT MATCHED THEN " +
                                "insert (NetworkID,WeatherTimeStamp," +
                                "ETStatus,ETValue,RainStatus,RainValue)" +
                                " values(" + client.nid.ToString() + ",'" + wd.stamp.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'," +
                                     etstatstr + "," + etvalstr + "," + rainstatstr + "," + rainvalstr + ")", conn, txn);
                            _logger.WriteMessage(NLog.LogLevel.Debug, "9353", "SQL: " + cmd.CommandText);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();
                            // if we made it to here, we are good to commit all our changes
                            abort = false;
                        }
                        catch (Exception e)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "9391", "Database exception during Weather Data insert(s): " + e.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    if (abort == true)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "9391", "Database Error on Weather Data inserts.");
                        txn.Rollback();
                    }
                    else
                    {
                        txn.Commit();
                    }
                }
                db.CloseDBConnection(conn, _logger);
            }

            // if nothing saved to database, do not ACK
            if (abort == true)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "9901", "Not acking, problem with DB update.");

                _logger.WriteMessage(NLog.LogLevel.Info, "14333", "Build Weather Data Nak response.");

                return (Tuple.Create(Misc.buildNakResponse(client.nid, client.sn, client.mid), (String)null, false, Constants.NO_STATE_DATA));
            }

            var mem = new MemoryStream(0);
            // build ack response
            if (client.nid != 0)
            {
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(client.sn, mem);
                // network id
                Misc.Uint32ToNetworkBytes(client.nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_WEATHER_DATA_full_receipt_ack, mem);
                _logger.WriteMessage(NLog.LogLevel.Info, "9371", "Build Weather Ack response.");
            }

            // get the built response to return and be sent
            data = mem.ToArray();
            List<byte[]> rsplist = new List<byte[]>();
            rsplist.Add(data);
            return (Tuple.Create(rsplist, (String)null, false, Constants.NO_STATE_DATA));
        }
    }
}
