﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp
{
    public static class Constants
    {
        // registration message constants
        public const int MAX_CHAIN_LENGTH = 12;
        public const int NUMBER_OF_CHARS_IN_A_NAME = 48;
        public const int REGVER4_SIZE_DELTA_PER_CONTROLLER = 16;    // number of extra bytes at end of version 4 registration per controller
        public const int TEST_STRING_LEN = 48;
        public const int CODE_REV_STR_LEN = 48;
        public const int STATION_CARD_COUNT = 6;
        public const int POC_SLOT_NUMBER = 80;
        public const int LIGHT_SLOT_NUMBER = 90;
        public const uint option_fl_bit = 0x0001;
        public const uint option_SSE_bit = 0x0002;
        public const uint option_SSE_D_bit = 0x0004;
        public const uint option_HUB_bit = 0x0008;
        public const uint card_present_bit = 0x0001;
        public const uint tb_present_bit = 0x0002;
        public const uint MAX_CONVENTIONAL_STATIONS = 48;
        public const uint MAX_STATIONS_PER_CONTROLLER = 128;
        public const uint MAX_STATIONS_PER_NETWORK = ((Constants.MAX_CHAIN_LENGTH * Constants.MAX_STATIONS_PER_CONTROLLER) / 2);
        public const uint STATION_HISTORY_MIN_DAYS_KEPT = 14;
        public const uint STATION_HISTORY_MAX_RECORDS = ((Constants.MAX_STATIONS_PER_NETWORK) * Constants.STATION_HISTORY_MIN_DAYS_KEPT);
        public const uint STATION_REPORT_DATA_MIN_DAYS_KEPT = 14;
        public const uint STATION_REPORT_DATA_MAX_RECORDS = ((Constants.MAX_STATIONS_PER_NETWORK) * Constants.STATION_REPORT_DATA_MIN_DAYS_KEPT);
        public const uint SYSTEM_REPORT_RECORDS_TO_KEEP = 480;
        public const uint POC_REPORT_RECORDS_TO_KEEP = 400;

        public const int NO_STATE_DATA = 299;              // parsing function doesn't maintain state information
        public const int LAST_STATE_COMPLETE = 99;        // parsing function has completed a state cycle

        // station history bit masks for 32 bits of bits
        public const uint pi_flow_data_has_been_stamped_bit = 0x0001;
        public const uint controller_turned_off_bit = 0x0002;
        public const uint hit_stop_time_bit = 0x0004;
        public const uint stop_key_pressed_bit = 0x0008;
        public const uint current_short_bit = 0x0010;
        public const uint current_none_bit = 0x0020;
        public const uint current_low_bit = 0x0040;
        public const uint current_high_bit = 0x0080;
        public const uint tailends_modified_the_cycle_time_bit = 0x0100;
        public const uint tailends_zeroed_the_irrigation_time_bit = 0x0200;
        public const uint flow_low_bit = 0x0400;
        public const uint flow_high_bit = 0x0800;
        public const uint flow_never_checked_bit = 0x1000;
        public const uint no_water_by_manual_prevented_bit = 0x2000;
        public const uint no_water_by_calendar_prevented_bit = 0x4000;
        public const uint mlb_prevented_or_curtailed_bit = 0x08000;
        public const uint mvor_closed_prevented_or_curtailed_bit = 0x0010000;
        public const uint rain_as_negative_time_prevented_irrigation_bit = 0x0020000;
        public const uint rain_as_negative_time_reduced_irrigation_bit = 0x0040000;
        public const uint rain_table_R_M_or_Poll_prevented_or_curtailed_bit = 0x0080000;
        public const uint switch_rain_prevented_or_curtailed_bit = 0x0100000;
        public const uint switch_freeze_prevented_or_curtailed_bit = 0x0200000;
        public const uint wind_conditions_prevented_or_curtailed_bit = 0x0400000;
        public const uint mois_cause_cycle_skip_bit = 0x0800000;
        public const uint mois_max_water_day_bit = 0x1000000;
        public const uint poc_short_cancelled_irrigation_bit = 0x2000000;
        public const uint mow_day_bit = 0x4000000;
        public const uint two_wire_cable_problem_bit = 0x08000000;

        public const uint rip_valid_to_show = 0x010000000;
        public const uint two_wire_station_decoder_inoperative = 0x020000000;
        public const uint two_wire_poc_decoder_inoperative = 0x040000000;


        // flow recording message constants
        public const uint FLOW_RECORDER_MAX_RECORDS = 1600;

        public const UInt16 flow_check_status_bitmask = 0x0007;
        public const int flow_check_status_bitshift = 0;
        public const uint indicies_out_of_range_bit = 0x0008;
        public const uint station_cycles_too_low_bit = 0x0010;
        public const uint cell_iterations_too_low_bit = 0x0020;
        public const uint no_flow_meter_bit = 0x0040;
        public const uint not_enabled_by_user_setting_bit = 0x0080;
        public const uint acquiring_expected_bit = 0x0100;
        public const uint combo_not_allowed_to_check_bit = 0x0200;
        public const uint unstable_flow_bit = 0x0400;
        public const uint cycle_time_too_short_bit = 0x0800;
        public const uint irrigation_restart_bit = 0x01000;
        public const uint not_supposed_to_check_bit = 0x02000;
        public const uint zero_flow_rate_bit = 0x04000;
        public const uint thirty_percent_bit = 0x08000;

        // header bits
        public const byte MSG_CLASS_2000e_CS3000_FLAG = 15;
        public const byte MSG_CLASS_CS3000_FROM_COMMSERVER = 2;
        public const byte MSG_CLASS_CS3000__FROM_COMMSERVER__CODE_DISTRIBUTION = 11;

        // message ID's
        /*
        public const byte MID_FROM_COMMSERVER_REGISTRATION_DATA_ACK = 1;
        public const byte MID_TO_COMMSERVER_REGISTRATION_DATA_init_packet = 0;
        public const byte MID_TO_COMMSERVER_ENGINEERING_ALERTS = 2;
        public const byte MID_FROM_COMMSERVER_ENGINEERING_ALERTS_ACK = 3;
        public const byte MID_TO_COMMSERVER_FLOW_RECORDING = 4;
        public const byte MID_FROM_COMMSERVER_FLOW_RECORDING_ACK = 5;
        public const byte MID_TO_COMMSERVER_STATION_HISTORY = 6;
        public const byte MID_FROM_COMMSERVER_STATION_HISTORY_ACK = 7;
        public const byte MID_TO_COMMSERVER_CHECK_FOR_UPDATES = 8;
        public const byte MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_NO_UPDATES = 9;
        public const byte MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_UPDATE_AVAILABLE = 10;
        public const byte MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_MULTIPLE_UPDATES_AVAILABLE = 11;

        public const byte MID_TO_COMMSERVER_STATION_REPORT_DATA  =  12;
        public const byte MID_FROM_COMMSERVER_STATION_REPORT_DATA_ACK = 13;

        public const byte  MID_TO_COMMSERVER_POC_REPORT_DATA = 14;
        public const byte  MID_FROM_COMMSERVER_POC_REPORT_DATA_ACK = 15;

        public const byte MID_CODE_DISTRIBUTION_kickstart_init_packet = 16;
        public const byte MID_CODE_DISTRIBUTION_kickstart_data_packet = 17;
        public const byte MID_CODE_DISTRIBUTION_kickstart_init_ack = 18;
        public const byte MID_CODE_DISTRIBUTION_kickstart_full_receipt_ack = 19;
        public const byte MID_CODE_DISTRIBUTION_main_init_packet = 20;
        public const byte MID_CODE_DISTRIBUTION_main_data_packet = 21;
        public const byte MID_CODE_DISTRIBUTION_main_init_ack = 22;
        public const byte MID_CODE_DISTRIBUTION_main_full_receipt_ack = 23;

        public const byte   MID_CODE_DISTRIBUTION_tp_init_packet = 30;
        public const byte 	MID_CODE_DISTRIBUTION_tp_data_packet = 31;
        public const byte 	MID_CODE_DISTRIBUTION_tp_init_ack = 32;
        public const byte 	MID_CODE_DISTRIBUTION_tp_full_receipt_ack = 33;

        public const byte MID_TO_COMMSERVER_SYSTEM_REPORT_DATA = 40;
        public const byte MID_FROM_COMMSERVER_SYSTEM_REPORT_DATA_ACK = 41;
         */

        public const byte MID_TO_COMMSERVER_REGISTRATION_DATA_init_packet = (0);
        public const byte MID_TO_COMMSERVER_REGISTRATION_DATA_data_packet = (1);
        public const byte MID_FROM_COMMSERVER_REGISTRATION_DATA_full_receipt_ack = (2);

        public const byte MID_TO_COMMSERVER_ENGINEERING_ALERTS_init_packet = (3);
        public const byte MID_TO_COMMSERVER_ENGINEERING_ALERTS_data_packet = (4);
        public const byte MID_FROM_COMMSERVER_ENGINEERING_ALERTS_full_receipt_ack = (5);

        public const byte MID_TO_COMMSERVER_FLOW_RECORDING_init_packet = (6);
        public const byte MID_TO_COMMSERVER_FLOW_RECORDING_data_packet = (7);
        public const byte MID_FROM_COMMSERVER_FLOW_RECORDING_full_receipt_ack = (8);

        public const byte MID_TO_COMMSERVER_STATION_HISTORY_init_packet = (9);
        public const byte MID_TO_COMMSERVER_STATION_HISTORY_data_packet = (10);
        public const byte MID_FROM_COMMSERVER_STATION_HISTORY_full_receipt_ack = (11);

        public const byte MID_TO_COMMSERVER_STATION_REPORT_DATA_init_packet = (12);
        public const byte MID_TO_COMMSERVER_STATION_REPORT_DATA_data_packet = (13);
        public const byte MID_FROM_COMMSERVER_STATION_REPORT_DATA_full_receipt_ack = (14);

        public const byte MID_CODE_DISTRIBUTION_kickstart_init_packet = (16);
        public const byte MID_CODE_DISTRIBUTION_kickstart_data_packet = (17);

        public const byte MID_CODE_DISTRIBUTION_kickstart_init_ack = (18);
        public const byte MID_CODE_DISTRIBUTION_kickstart_full_receipt_ack = (19);

        public const byte MID_CODE_DISTRIBUTION_main_init_packet = (20);
        public const byte MID_CODE_DISTRIBUTION_main_data_packet = (21);

        public const byte MID_CODE_DISTRIBUTION_main_init_ack = (22);
        public const byte MID_CODE_DISTRIBUTION_main_full_receipt_ack = (23);

        public const byte MID_CODE_DISTRIBUTION_tpmicro_init_packet = (30);
        public const byte MID_CODE_DISTRIBUTION_tpmicro_data_packet = (31);
        public const byte MID_CODE_DISTRIBUTION_tpmicro_init_ack = (32);
        public const byte MID_CODE_DISTRIBUTION_tpmicro_full_receipt_ack = (33);

        public const byte MID_TO_COMMSERVER_POC_REPORT_DATA_init_packet = (34);
        public const byte MID_TO_COMMSERVER_POC_REPORT_DATA_data_packet = (35);
        public const byte MID_FROM_COMMSERVER_POC_REPORT_DATA_full_receipt_ack = (36);

        public const byte MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_init_packet = (37);
        public const byte MID_TO_COMMSERVER_SYSTEM_REPORT_DATA_data_packet = (38);
        public const byte MID_FROM_COMMSERVER_SYSTEM_REPORT_DATA_full_receipt_ack = (39);

        public const byte MID_TO_COMMSERVER_CHECK_FOR_UPDATES_init_packet = (40);
        public const byte MID_TO_COMMSERVER_CHECK_FOR_UPDATES_data_packet = (41);
        public const byte MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_NO_UPDATES = (42);
        public const byte MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_UPDATE_AVAILABLE = (43);
        public const byte MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_MULTIPLE_UPDATES_AVAILABLE = (44);

        public const byte MID_TO_COMMSERVER_DISCONNECT = (45);

        public const byte MID_TO_COMMSERVER_WEATHER_DATA_init_packet = (46);
        public const byte MID_TO_COMMSERVER_WEATHER_DATA_data_packet = (47);
        public const byte MID_FROM_COMMSERVER_WEATHER_DATA_full_receipt_ack = (48);

        public const byte MID_TO_COMMSERVER_CHECK_FOR_WEATHER_init_packet = (49);
        public const byte MID_TO_COMMSERVER_CHECK_FOR_WEATHER_data_packet = (50);
        public const byte MID_FROM_COMMSERVER_CHECK_FOR_WEATHER_ACK_NO_WEATHER = (51);
        public const byte MID_FROM_COMMSERVER_CHECK_FOR_WEATHER_ACK_WEATHER_AVAILABLE = (52);

        // these next two are used in the "blast" method of communications
        public const byte MID_FROM_COMMSERVER_WEATHER_DATA_packet = (53);
        public const byte MID_TO_COMMSERVER_WEATHER_DATA_receipt_ack = (56);

        public const byte MID_FROM_COMMSERVER_WEATHER_DATA_init_packet = (53);
        public const byte MID_FROM_COMMSERVER_WEATHER_DATA_data_packet = (54);
        public const byte MID_TO_COMMSERVER_WEATHER_DATA_init_ack = (55);
        public const byte MID_TO_COMMSERVER_WEATHER_DATA_data_full_receipt_ack = (56);

        public const byte MID_TO_COMMSERVER_RAIN_INDICATION_init_packet = (57);
        public const byte MID_TO_COMMSERVER_RAIN_INDICATION_data_packet = (58);
        public const byte MID_FROM_COMMSERVER_RAIN_INDICATION_receipt_ack = (59);

        //
        // new 'blast' version of above
        //
        public const byte MID_FROM_COMMSERVER_RAIN_SHUTDOWN_packet = (64);
        public const byte MID_TO_COMMSERVER_RAIN_SHUTDOWN_receipt_ack = (67);


        public const byte MID_TO_COMMSERVER_PROGRAM_DATA_init_packet = (68);
        public const byte MID_TO_COMMSERVER_PROGRAM_DATA_data_packet = (69);
        public const byte MID_FROM_COMMSERVER_PROGRAM_DATA_full_receipt_ack = (70);

        public const byte MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_init_packet = (71);
        public const byte MID_TO_COMMSERVER_PROGRAM_DATA_REQUEST_data_packet = (72);
        public const byte MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_ACK_NO_CHANGES = (73);
        public const byte MID_FROM_COMMSERVER_PROGRAM_DATA_REQUEST_ACK_CHANGES_AVAILABLE = (74);

        public const byte MID_FROM_COMMSERVER_PROGRAM_DATA_init_packet = (75);
        public const byte MID_FROM_COMMSERVER_PROGRAM_DATA_data_packet = (76);
        public const byte MID_TO_COMMSERVER_PROGRAM_DATA_init_ack = (77);
        public const byte MID_TO_COMMSERVER_PROGRAM_DATA_data_full_receipt_ack = (78);

        public const byte MID_TO_COMMSERVER_VERIFY_FIRMWARE_VERSION_init_packet = (79);
        public const byte MID_TO_COMMSERVER_VERIFY_FIRMWARE_VERSION_data_packet = (80);
        public const byte MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_UP_TO_DATE = (81);
        public const byte MID_FROM_COMMSERVER_VERIFY_FIRMWARE_VERSION_ACK_FW_OUT_OF_DATE = (82);

        public const byte MID_TO_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_init_packet = (83);
        public const byte MID_TO_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_data_packet = (84);
        public const byte MID_FROM_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_ACK_FW_UP_TO_DATE = (85);
        public const byte MID_FROM_COMMSERVER_BEFORE_PDATA_RQST_CHECK_FIRMWARE_ACK_FW_OUT_OF_DATE = (86);

        // MID from Commserver -> Controller "I Have Something For You", in this case, program data
        public const byte MID_FROM_COMMSERVER_IHSFY_program_data = (90);

        // 5/14/2015 rmd : This is a single packet UNACKNOWLEDGED message from the commserver to the
        // controller. The definition of the message is the FROM_COMM_SERVER_TOP_PACKET_HEADER plus
        // the 6 byte DATE_TIME structure. Of course it has the preamble, CRC, and post amble too.
        // This message is sent at 4PM to all controllers in the tracking table at that time.
        public const byte MID_FROM_COMMSERVER_set_your_time_and_date = (91);

        public const byte MID_FROM_COMMSERVER_mobile_station_ON = (100);
        public const byte MID_TO_COMMSERVER_mobile_station_ON_ack = (101);

        public const byte MID_FROM_COMMSERVER_mobile_station_OFF = (102);
        public const byte MID_TO_COMMSERVER_mobile_station_OFF_ack = (103);

        // 6/11/2015 rmd : A periodic message from the controller to the commserver. Holds dynamic
        // 'state of' information such as flow rate, remaining time for those on, etc. Will be sent
        // in an unsolicited fashion at various times and rates to the commserver. Upon receipt
        // contents put into a holding table and flag set indicating fresh data arrival.
        public const byte MID_TO_COMMSERVER_status_screen_init_packet = (104);
        public const byte MID_TO_COMMSERVER_status_screen_data_packet = (105);
        public const byte MID_FROM_COMMSERVER_status_screen_ack_packet = (106);

        // 6/12/2015 rmd : This is a command that will sort of kick off a mobile session. It ought
        // to be the first command sent when a user with the mobile app chooses a controller. Upon
        // receipt the controller recognizes that there is a user preparing to perhaps send some
        // mobile commands. The controller will create a pseudo 'mobile session' where it will
        // periodically send a status screen at moments and rates defined by the controller. The
        // rate slows as 'mobile inactivity' is sensed by the controller. And eventually the session
        // times out and the status screen updates stop all together. A user at the mobile app can
        // ask on demand for a new status while on the 'mobile app status screen'.
        public const byte MID_FROM_COMMSERVER_send_a_status_screen = (107);
        public const byte MID_TO_COMMSERVER_send_a_status_screen_ack = (108);


        public const byte MID_FROM_COMMSERVER_panel_swap_factory_reset_to_new_panel = (109);
        public const byte MID_TO_COMMSERVER_panel_swap_factory_reset_to_new_panel_ack = (110);

        public const byte MID_FROM_COMMSERVER_panel_swap_factory_reset_to_old_panel = (111);
        public const byte MID_TO_COMMSERVER_panel_swap_factory_reset_to_old_panel_ack = (112);

        public const byte MID_FROM_COMMSERVER_set_all_bits = (113);
        public const byte MID_TO_COMMSERVER_set_all_bits_ack = (114);

        public const byte MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_init_packet = (115);
        public const byte MID_TO_COMMSERVER_LIGHTS_REPORT_DATA_data_packet = (116);
        public const byte MID_FROM_COMMSERVER_LIGHTS_REPORT_DATA_full_receipt_ack = (117);

        // ----------
        public const byte MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_init_packet = (118);
        public const byte MID_TO_COMMSERVER_ET_AND_RAIN_TABLE_data_packet = (119);
        public const byte MID_FROM_COMMSERVER_ET_AND_RAIN_TABLE_full_receipt_ack = (120);

        // 9/30/2015 ajv : We using a single command to determine whether to initiate an MVOR (open
        // or close) or cancel one that's in progress. Once we receive the command, the
        // FOAL_IRRI_initiate_or_cancel_a_master_valve_override function is called with the
        // appropriate parameters to perform the required action.
        public const byte MID_FROM_COMMSERVER_mobile_MVOR = (121);
        public const byte MID_TO_COMMSERVER_mobile_MVOR_ack = (122);

        // 9/30/2015 ajv : We're using a single command to handle lights activities. This command
        // sends what's built up into a LIGHTS_ON_XFER_RECORD and either
        // FOAL_LIGHTS_initiate_a_mobile_light_ON or FOAL_LIGHTS_initiate_a_mobile_light_OFF is
        // called with that record as a parameter to perform the action.
        public const byte MID_FROM_COMMSERVER_mobile_light_cmd = (123);
        public const byte MID_TO_COMMSERVER_mobile_light_cmd_ack = (124);

        // 10/5/2015 ajv : Command to turn Scheduled Irrigation On or Off.
        public const byte MID_FROM_COMMSERVER_mobile_turn_controller_on_off = (125);
        public const byte MID_TO_COMMSERVER_mobile_turn_controller_on_off_ack = (126);

        // ----------

        public const byte MID_FROM_COMMSERVER_mobile_now_days_all_stations = (127);
        public const byte MID_TO_COMMSERVER_mobile_now_days_all_stations_ack = (128);

        public const byte MID_FROM_COMMSERVER_mobile_now_days_by_group = (129);
        public const byte MID_TO_COMMSERVER_mobile_now_days_by_group_ack = (130);

        public const byte MID_FROM_COMMSERVER_mobile_now_days_by_station = (131);
        public const byte MID_TO_COMMSERVER_mobile_now_days_by_station_ack = (132);

        // ----------

        public const byte MID_FROM_COMMSERVER_force_registration = (133);
        public const byte MID_TO_COMMSERVER_force_registration_ack = (134);

        // ----------

        // 12/14/2015 ajv : The enable or disable FLOWSENSE option command takes a TRUE or a FALSE
        // as a parameter to indicate whether to enable or disable the option, respectively.
        public const byte MID_FROM_COMMSERVER_enable_or_disable_FL_option = (135);
        public const byte MID_TO_COMMSERVER_enable_or_disable_FL_option_ack = (136);

        // ----------

        // 12/14/2015 ajv : The clear mainline break command takes the mainline (system) GID as a
        // parameter to clear the appropriate break if more than one exists.
        public const byte MID_FROM_COMMSERVER_clear_MLB = (137);
        public const byte MID_TO_COMMSERVER_clear_MLB_ack = (138);

        // ----------

        // 12/14/2015 ajv : The stop irrigation command emulates the user pressing the STOP key.
        // That is, it stops the highest form of irrigation across all mainlines.
        public const byte MID_FROM_COMMSERVER_stop_all_irrigation = (139);
        public const byte MID_TO_COMMSERVER_stop_all_irrigation_ack = (140);

        // 12/14/2015 ajv : This stop irrigation command emulates the user pressing the STOP key.
        // That is, it stops the highest form of irrigation across all mainlines. Except, in the
        // future, we may opt to enhance this so we'll accept the mainline (system) GID as well.
        // However, we're not going to use it for now.
        public const byte MID_FROM_COMMSERVER_stop_irrigation = (141);
        public const byte MID_TO_COMMSERVER_stop_irrigation_ack = (142);

        // 1/11/2016 ajv : Manually force a check for updates from Command Center Online
        public const byte MID_FROM_COMMSERVER_check_for_updates = (143);
        public const byte MID_TO_COMMSERVER_check_for_updates_ack = (144);

        // ----------
        public const byte MID_FROM_COMMSERVER_mobile_now_days_by_box = (145);
        public const byte MID_TO_COMMSERVER_mobile_now_days_by_box_ack = (146);

        // 02/22/2016 skc : Standard message ids for budget reports
        public const byte MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet = (147);
        public const byte MID_TO_COMMSERVER_BUDGET_REPORT_DATA_data_packet = (148);
        public const byte MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_full_receipt_ack = (149);

        // ----------
        public const byte MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_init_packet = (150);
        public const byte MID_TO_COMMSERVER_MOISTURE_SENSOR_RECORDER_data_packet = (151);
        public const byte MID_FROM_COMMSERVER_MOISTURE_SENSOR_RECORDER_full_receipt_ack = (152);

        // ----------

        // 4/25/2017 ajv : The enable or disable CS3-HUB-OPT command takes a TRUE or a FALSE as a
        // parameter to indicate whether to enable or disable the option, respectively.
        public const byte MID_FROM_COMMSERVER_enable_or_disable_HUB_option = (157);
        public const byte MID_TO_COMMSERVER_enable_or_disable_HUB_option_ack = (158);

        // ----------

        public const byte MID_TO_COMMSERVER_keep_alive = (160);
        public const byte MID_FROM_COMMSERVER_keep_alive_ack = (161);


        public const byte MID_FROM_COMMSERVER_go_ahead_send_your_messages	=						(162);

        public const byte MID_TO_COMMSERVER_no_more_messages_init_packet	=						(163);
        public const byte MID_TO_COMMSERVER_no_more_messages_data_packet	=						(164);
        public const byte MID_FROM_COMMSERVER_no_more_messages_ack		=						(165);

        public const byte MID_TO_COMMSERVER_hub_is_busy_init_packet		=						(166);
        public const byte MID_TO_COMMSERVER_hub_is_busy_data_packet		=						(167);
        public const byte MID_FROM_COMMSERVER_hub_is_busy_ack			=							(168);

        // ----------

        // 3/30/2017 rmd : These two mids are a way for the hub to ask teh commserver to send the
        // hub list. The ack (170) is NOT the hub list. The commserver should queue the hub as a
        // high priority job and instead of the next poll the hub list would be sent. Presently
        // don't have a use for this.
        public const byte MID_TO_COMMSERVER_please_send_me_the_hub_list		=					(169);
        public const byte MID_FROM_COMMSERVER_please_send_me_the_hub_list_ack		=				(170);

        // ----------

        // 2/9/2017 rmd : These may or may not be used. As of this date they do not have a use.
        public const byte MID_FROM_COMMSERVER_pause_code_distribution		=						(171);
        public const byte MID_FROM_COMMSERVER_cancel_code_distribution			=				(172);
        public const byte MID_TO_COMMSERVER_pause_cancel_code_distribution_ack		=			(173);

        // ----------

        // 3/30/2017 rmd : Following the intial connection Jay is going to send the hub a list of
        // the controllers, both 2000 and 3000, that are on the hub. The list will be sent without
        // the hub asking for it. The list is delivered in a single packet and therefore can hold
        // 512 total 2000/3000 controllers.
        public const byte MID_FROM_COMMSERVER_here_is_the_hub_list			=					(174);
        public const byte MID_TO_COMMSERVER_here_is_the_hub_list_ack		=						(175);

        // ----------

        // 2/10/2017 rmd : These are ADDITIONAL result mids from the commserver in response to the
        // MID 40/41 pair to the commserver. The 40/41 pair is a check for updates query from the
        // controller. These particular responses should only be sent to a hub.
        public const byte MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_one_update_but_distribute_both	=	(180);
        public const byte MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_tpmicro	=	(181);
        public const byte MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_main	=	(182);
        public const byte MID_FROM_COMMSERVER_CHECK_FOR_UPDATES_ACK_no_updates_but_distribute_both = (183);

        // 02/28/2017 skc : New format for budget reports
        public const byte MID_TO_COMMSERVER_BUDGET_REPORT_DATA_init_packet2 = (200);
        public const byte MID_TO_COMMSERVER_BUDGET_REPORT_DATA_data_packet2 = (201);
        public const byte MID_FROM_COMMSERVER_BUDGET_REPORT_DATA_full_receipt_ack2 = (202);

        // gets put in IHSFY table when controller on hub has changed (removed/added)
        public const byte MID_TO_COMMSERVER_update_and_send_hub_list = (203);

        // ----------

        // ---------- 

        public const byte MID_FROM_COMMSERVER_clear_rain_all_stations = (210);
        public const byte MID_TO_COMMSERVER_clear_rain_all_stations_ack = (211);

        public const byte MID_FROM_COMMSERVER_clear_rain_by_group = (212);
        public const byte MID_TO_COMMSERVER_clear_rain_by_group_ack = (213);

        public const byte MID_FROM_COMMSERVER_clear_rain_by_station = (214);
        public const byte MID_TO_COMMSERVER_clear_rain_by_station_ack = (215);

        // ---------- 

        // 7/17/2017 ajv : Commands to request data from the controller rather than relying on the 
        // controller to send it automatically. These may be used in the future to provide the user 
        // control over when the data is retrieved. 
        public const byte MID_FROM_COMMSERVER_send_engineering_alerts = (216);
        public const byte MID_TO_COMMSERVER_send_engineering_alerts_ack = (217);

        public const byte MID_FROM_COMMSERVER_send_flow_recording = (217);
        public const byte MID_TO_COMMSERVER_send_flow_recording_ack = (218);

        public const byte MID_FROM_COMMSERVER_send_station_history = (219);
        public const byte MID_TO_COMMSERVER_send_station_history_ack = (220);

        public const byte MID_FROM_COMMSERVER_send_station_report_data = (221);
        public const byte MID_TO_COMMSERVER_send_station_report_data_ack = (222);

        public const byte MID_FROM_COMMSERVER_send_poc_report_data = (223);
        public const byte MID_TO_COMMSERVER_send_poc_report_data_ack = (224);

        public const byte MID_FROM_COMMSERVER_send_system_report_data = (225);
        public const byte MID_TO_COMMSERVER_send_system_report_data_ack = (226);

        public const byte MID_FROM_COMMSERVER_send_lights_report_data = (227);
        public const byte MID_TO_COMMSERVER_send_lights_report_data_ack = (228);

        public const byte MID_FROM_COMMSERVER_send_budget_report_data = (229);
        public const byte MID_TO_COMMSERVER_send_budget_report_data_ack = (230);

        public const byte MID_FROM_COMMSERVER_send_moisture_sensor_recorder_data = (231);
        public const byte MID_TO_COMMSERVER_send_moisture_sensor_recorder_ack = (232);

        // ----------

        // 7/20/2017 ajv : Occassionally, engineering has had the desire to reboot a controller 
        // remotely, whether to force a FLOWSENSE scan, rebuild a socket, or something else. This 
        // command helps acheive that. 
        public const byte MID_FROM_COMMSERVER_reboot_controller = (233);
        public const byte MID_TO_COMMSERVER_reboot_controller_ack = (234);

        // ----------

        // 7/20/2017 ajv : When troubleshooting communication issues remotely, it can be useful to 
        // have the ability to trigger a new FLOWSENSE scan. For example, if there is a large SR 
        // chain and Bill is at a controller some distance from the master, this allows us to force 
        // a rescan after he's made adjustments to the antenna. 
        public const byte MID_FROM_COMMSERVER_perform_FLOWSENSE_scan = (235);
        public const byte MID_TO_COMMSERVER_perform_FLOWSENSE_scan_ack = (236);

        // 7/20/2017 ajv : Similar to the perform FLOWSENSE scan functionality, we've had cases 
        // where engineering was not confident the program data had been synced properly down the 
        // chain. To overcome this, we've had the users turn off FLOWSENSE, leave the screen, wait a 
        // minute, and turn FLOWSENSE back on. This gives us direct control to trigger this behavior 
        // remotely. 
        public const byte MID_FROM_COMMSERVER_perform_FLOWSENSE_data_sync = (237);
        public const byte MID_TO_COMMSERVER_perform_FLOWSENSE_data_sync_ack = (238);

        // ----------

        // 7/20/2017 ajv : In the ET2000 and ET2000e, Field Service occassionally ran into issues 
        // where the flow table had invalid data in it. This resulted in user-level high and low 
        // flow alerts that made little sense. To overcome this, they cleared the flow table using 
        // CC4. City of Davis may have run into a similar issue with the CS3000 and we're therefore 
        // adding this command to provide the same functionality. It's unclear whether Field Service 
        // will be given access to this, though. 
        public const byte MID_FROM_COMMSERVER_reset_flow_table = (239);
        public const byte MID_TO_COMMSERVER_reset_flow_table_ack = (240);

        // ----------

        // 7/20/2017 ajv : In the HUB world, the CommServer doesn't send a new HUB list unless it 
        // either has a new controller added or the HUB establishes a new socket connection. 
        // Although a "send hub list" command exists, Bob has also requested a command to forcibly 
        // disconnect from the CommServer and create a new socket connection. 
        public const byte MID_FROM_COMMSERVER_disconnect_from_cloud = (241);
        public const byte MID_TO_COMMSERVER_disconnect_from_cloud_ack = (242);

        // ----------

        // 7/20/2017 ajv : This command provides a method for a user to activate a software option, 
        // such as FLOWSENSE or HUB, on a controller with a central option attached without having 
        // to go out to the field. 
        public const byte MID_FROM_COMMSERVER_activate_option = (243);
        public const byte MID_TO_COMMSERVER_activate_option_ack = (244);

        // ----------

        // @WB: Bob and I agreed on add (1000) to init mid for NAKs mid's
        public const ushort MID_DIFF_FOR_NAK = (1000);

        // rain stuff
        public const byte WEATHER_DATA_HAS_ET = (0);
        public const byte WEATHER_DATA_HAS_RAIN = (1);
        public const byte RAIN_ROLLOVER_HOUR_DELTA = (4);       // 8PM -> Midnight = 4 hours

        // pre/post ambles
        public static readonly byte[] PREAMBLE = new byte[] { 0x1F, 0x2E, 0x3D, 0x4C };
        public static readonly byte[] POSTAMBLE = new byte[] { 0x5B, 0x6A, 0x79, 0x88 };

        // firmware code revision string offsets in .bin file(s):
        public const int MAIN_APP_BUILD_DT_OFFSET = (0x00000040);
        public const int TPMICRO_BUILD_DT_OFFSET = (0x000000d0);

        public const int DESIRED_PACKET_PAYLOAD = (2048);

        // alerts stuff
        public const int PARSE_FOR_LENGTH_ONLY = (100);
        public const int PARSE_FOR_LENGTH_AND_TEXT = (200);
        public const int MAX_ALERT_STRING = (256);  // ASCII chars in parsed alert string. 256 should be enough

        // program data
        public const int CHANGE_REASON_SYNC_RECEIVING_AT_COMM_SERVER = (20);

        // IHSFY_notification - job completion status values
        public const int JOB_SUCCESSFUL = (0);
        public const int JOB_UNKNOWN_COMMAND_ID = (-1);
        public const int JOB_EXPIRED = (-2);
        public const int JOB_NO_CONNECTION_TO_CONTROLLER = (-3);
        public const int JOB_FLUSH_STALE_MOBILE_AT_STARTUP = (-4);
        public const int JOB_NO_WEATHERSENSE_DATA = (-5);
        public const int JOB_ERROR_WHILE_SENDING = (-6);
        public const int JOB_INVALID_RESPONSE = (-7);

        public const int CALSENSE_REPAIR_ACCOUNT_ID = (169); // calsense repair account "company" id

        public const int FACTORY_RESET_IS_NOT_REQUIRED = 0;
        public const int FACTORY_RESET_IS_REQIURED = 1;
        public const int FACTORY_RESET_ERROR_WHILE_CHECKING = 2;

        // 10/7/2015 wjb: I've Increased the buffer size from 1 MB to 10 MB for now to ensure we have 
        // enough buffer to process reports messages.
        public const int SQL_BUFFER_SIZE = 10 * 1024 * 1024; // 10 MB

        public static readonly String[] WeatherSenseETCodes = new String[]
        {
            "Successful",
            "Invalid request format",
            "Invalid latitude and/or longitude",
            "No stations available",
            "Invalid station",
            "No ET data available",
            "Not enough ET data",
            "Not enough ET data",
            "Unknown"
        };

        public static readonly String[] WeatherSenseRainCodes = new String[]
        {
            "Successful",
            "Invalid request format",
            "Invalid latitude and/or longitude",
            "No stations available",
            "Invalid station",
            "No Rain data available",
            "Not enough Rain data",
            "Not enough Rain data",
            "Unknown"
        };

        public const string CS3000Model = "7";
        public const string ET2000Model = "6";

        public const byte LEGACY_SHARED_BEFORE_ROLL_TIME = 0x01;
        public const byte LEGACY_SHARED_AFTER_MID_NIGHT = 0x02;
        public const byte LEGACY_SHARED_ET_TO_AN_ACTIVE_GAGE = 0x04;
        public const byte LEGACY_SHARED_RAIN_TO_AN_RB = 0x08;

        public static string _CommServerID="";
        public static string CommServerID
        {
            get
            {
                if (String.IsNullOrEmpty(_CommServerID))
                {
                    _CommServerID = System.Configuration.ConfigurationManager.AppSettings["TcpPort"];
                }

                return _CommServerID;
            }
        }

        public class Hubs
        {
            public static readonly int NoHubInvolved = 0;
            public static readonly int CommunicateUsingLegacy_LR_Hub = 1;
            public static readonly int CommunicateUsingLegacy_SR_Hub = 2;
            public static readonly int CommunicateUsingCS3000_LR_Hub = 3;
            public static readonly int CommunicateUsingCS3000_SR_Hub = 4;
            public static readonly int Legacy_LR_Hub = 5;
            public static readonly int Legacy_SR_Hub = 6;
            public static readonly int CS3000_LR_Hub = 7;
            public static readonly int CS3000_SR_Hub = 8;
        }

    }
}
