﻿//
//
// PeriodicTasks class 
//
// Called periodically to check for any CommServer initiated
// tasks.
// Tasks are usually performed for a given set of controllers,
// grouped by their timezone, at a time-of-day for that group.
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using ServerApp;
using System.Collections.Concurrent;
using System.Configuration;
using Advantage.Data.Provider;
using System.Data;




namespace Calsense.CommServer
{
    public static class PeriodicTasks
    {


        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GetTimeZoneStr(uint TimeZoneID);

        //
        // Define actions that happen at a particular time of day - which is
        // picked up from database table CS3000CommServerSettings at runtime.
        //
        struct TimeTriggerStruct
        {
            public TimeSpan? StartTime { get; set; }
            public TimeSpan? RepeatDuration { get; set; }
            public TimeSpan? RepeatFrequency { get; set; }
            public int? DayOfWeek { get; set; }
            public String SqlDayOfWeekColumnName { get; set; }
            public String SqlStartTimeColumnName { get; set; }
            public String SqlStopTimeColumnName { get; set; }
            public String SqlFrequencyColumnName { get; set; }
            public Action<uint, uint> ProcessingFunction { get; set; }
            public String NIDQuery { get; set; }
            public TimeTriggerStruct(TimeSpan? tsStart, TimeSpan? tsDuration, TimeSpan? tsFreq, int? dayOfWeek, String sDayOfWeek, String sStart, String sStop, String sFreq, Action<uint, uint> pf, String q)
                : this()
            {
                StartTime = tsStart;
                RepeatDuration = tsDuration;
                RepeatFrequency = tsFreq;
                SqlDayOfWeekColumnName = sDayOfWeek;
                SqlStartTimeColumnName = sStart;
                SqlStopTimeColumnName = sStop;
                SqlFrequencyColumnName = sFreq;
                ProcessingFunction = pf;
                NIDQuery = q;
            }
        }

        static TimeTriggerStruct[] TimeTriggerData = new TimeTriggerStruct[]
        {
            //
            // Times are "local" times at the controller, so controllers in different
            // time zones will be acted upon at different commserver times
            //
            // time will be filled in via SQL query of CS3000CommServerSettings table
            new TimeTriggerStruct(null, null, null, null, null, "SetControllersClock", null, null, ActionSetControllerClock, "SELECT NetworkID FROM CS3000Networks n "
                                + " JOIN ControllerSites s ON n.NetworkID=s.SiteID WHERE s.Deleted = false " + 
                                " AND s.CompanyID IN (SELECT CompanyID FROM Companies WHERE CommServerID=" + Constants.CommServerID + ") AND n.TimeZone = "),
            new TimeTriggerStruct(null, null, null, null, null, "SendWeatherData", null, null, ActionInitiateWeatherData,  "SELECT NetworkID FROM CS3000Networks n "
                                + " JOIN ControllerSites s ON n.NetworkID=s.SiteID WHERE s.Deleted = false AND "
                                + " (n.RainSharingNetworkID >= 0 OR n.ETSharingNetworkID >= 0 OR n.ETSharingControllerID>-1 OR n.RainSharingControllerID>-1) " + 
                                " AND s.CompanyID IN (SELECT CompanyID FROM Companies WHERE CommServerID=" + Constants.CommServerID + ") AND n.TimeZone = "),
            // for controller involved with a hub we need to send high priority poll to get the weather
            new TimeTriggerStruct(null, null, null, null, null, "HubGetETRain", null, null, ActionHubInitiateGetWeatherData, "SELECT NetworkID FROM Hub_GetWeatherfromControllers " +
                                " WHERE CompanyID IN (SELECT CompanyID FROM Companies WHERE CommServerID=" + Constants.CommServerID + ") AND TimeZone = "),
                                // for controller with rain sensor, deman poll during window
            new TimeTriggerStruct(null, null, null, null, null, "HubRainPollStart", "HubRainPollStop", "HubRainPollIntervalMins", ActionHubInitiateRainPoll, "SELECT NetworkID FROM GetHubRainPolling3000  " +
                                " WHERE CompanyID IN (SELECT CompanyID FROM Companies WHERE CommServerID=" + Constants.CommServerID + ") AND TimeZone = "),
   
                                
        };

        //
        // Define jobs that happen per rows in the IHSFY_jobs table - which include
        // TimeTrigger actions above that generate retriable/ACK'able commands,
        // and mobile commands placed in table by UI
        //
        struct JobListStruct
        {
            public Int32 MessageID { get; set; }            // message ID (MID)
            public TimeSpan RetryPeriod { get; set; }          // seconds between retries
            public TimeSpan Lifetime { get; set; }          // amount of time job can stay alive in table
            public bool FailIfNoConnection { get; set; }    // immediate failure if controller not currently connected
            public bool MobileJobFunction { get; set; }     // true if this is considered a "mobile" job
            public Func<uint, uint, int> JobFunction { get; set; }
            public JobListStruct(Int32 mid, TimeSpan retry, TimeSpan life, bool f, bool m, Func<uint, uint, int> jf)
                : this()
            {
                MessageID = mid;
                RetryPeriod = retry;
                Lifetime = life;
                FailIfNoConnection = f;
                MobileJobFunction = m;
                JobFunction = jf;
            }
        }

        //
        // make sure the lifetime is at least as long as the frequency the job table is
        // checked - or else jobs will expire without getting attempted!
        //
        static JobListStruct[] JobListData = new JobListStruct[]
        {
            new JobListStruct(Constants.MID_FROM_COMMSERVER_WEATHER_DATA_packet, TimeSpan.FromSeconds(300), TimeSpan.FromHours(1.0), false, false, JOB_SendWeatherData),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_RAIN_SHUTDOWN_packet, TimeSpan.FromSeconds(600), TimeSpan.FromHours(23.9), false, false, JOB_SendRainShutdown),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_mobile_station_ON, TimeSpan.FromSeconds(60), TimeSpan.FromMinutes(5), true, true, JOB_SendOnCommand),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_mobile_station_OFF, TimeSpan.FromSeconds(60), TimeSpan.FromMinutes(5), true, true, JOB_SendOffCommand),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_send_a_status_screen, TimeSpan.FromSeconds(60), TimeSpan.FromMinutes(5), true, true, JOB_SendStatusScreenRequest),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_panel_swap_factory_reset_to_new_panel, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(6), true, true, JOB_FactoryResetToNewControllerRequest),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_panel_swap_factory_reset_to_old_panel, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(6), true, true, JOB_FactoryResetToOldControllerRequest),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_set_all_bits, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(4), true, false, JOB_SetAllBitsRequest),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_enable_or_disable_FL_option, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(1), true, false, JOB_EnableFL_OptionCommand),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_force_registration, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(1), true, false, JOB_ForceRegistration),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_mobile_MVOR, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(6), true, true, JOB_MVOR_Command),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_mobile_light_cmd, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(6), true, true, JOB_Light_Command),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_mobile_turn_controller_on_off, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(6), true, true, JOB_ControllerOnOff_Command),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_mobile_now_days_all_stations, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(6), true, true, JOB_SendNoDaysAllStations_Command),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_mobile_now_days_by_group, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(6), true, true, JOB_SendNoDaysByGroup_Command),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_mobile_now_days_by_box, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(6), true, true, JOB_SendNoDaysByBox_Command),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_mobile_now_days_by_station, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(6), true, true, JOB_SendNoDaysByStation_Command),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_clear_MLB, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(6), true, true, JOB_ClearMLB_Command),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_stop_all_irrigation, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(6), true, true, JOB_StopAllIrrigation_Command),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_stop_irrigation, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(6), true, true, JOB_StopIrrigation_Command),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_check_for_updates, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(4), true, false, JOB_CheckForUpdate_Command),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_enable_or_disable_HUB_option, TimeSpan.FromSeconds(120), TimeSpan.FromMinutes(4), true, false, JOB_EnableHub_OptionCommand),
            // following is a dummy job that is created when a status screen comes in so that normal job completion processing happens to get a proper
            // insertion into the notification table.
            new JobListStruct(Constants.MID_TO_COMMSERVER_status_screen_init_packet, TimeSpan.FromHours(24), TimeSpan.FromMinutes(5), false, true, JOB_noaction),
            // this MID shows up when a hub has had a controller added/removed in the database
            new JobListStruct(Constants.MID_TO_COMMSERVER_update_and_send_hub_list, TimeSpan.FromHours(24), TimeSpan.FromMinutes(30), false, false, JOB_hubupdate),
            // demand poll, higher priority and immediate than a normal periodic poll
            new JobListStruct(Constants.MID_FROM_COMMSERVER_go_ahead_send_your_messages, TimeSpan.FromHours(24), TimeSpan.FromMinutes(10), true, false, JOB_DemandPoll),
            new JobListStruct(Constants.MID_FROM_COMMSERVER_send_engineering_alerts, TimeSpan.FromSeconds(60), TimeSpan.FromMinutes(2), true, false, JOB_SendEngineeringAlerts),
        };

        //
        // In a hub, we poll at different rates (faster) when mobile
        // jobs present. This function decides which jobs trigger
        // faster polling and which do not.
        //
        static public bool IsThisA3000MobileJob(Func<uint, uint, int> f)
        {
            if (f == JOB_SendOnCommand) return (true);
            if (f == JOB_SendOffCommand) return (true);
            if (f == JOB_SendStatusScreenRequest) return (true);
            if (f == JOB_MVOR_Command) return (true);
            if (f == JOB_Light_Command) return (true);
            if (f == JOB_ControllerOnOff_Command) return (true);
            if (f == JOB_SendNoDaysAllStations_Command) return (true);
            if (f == JOB_SendNoDaysByGroup_Command) return (true);
            if (f == JOB_SendNoDaysByBox_Command) return (true);
            if (f == JOB_SendNoDaysByStation_Command) return (true);
            if (f == JOB_ClearMLB_Command) return (true);
            if (f == JOB_StopAllIrrigation_Command) return (true);
            if (f == JOB_StopIrrigation_Command) return (true);
            return (false);
        }

        //
        // load trigger times out of CS3000CommServerSettings table, one for each row in TimeTriggerData
        //
        static async void LoadTriggerTimesFromDatabase()
        {
            AdsCommand cmd;

            ServerDB db = new ServerDB(_cts);

            Task<AdsConnection> connTask = db.OpenDBConnection("");
            AdsConnection conn =  await connTask;

            if(conn != null)
            {
                try
                {
                    for (int i = 0; i < TimeTriggerData.Length; i++ )
                    {
                        object dayOfWeek;
                        if (TimeTriggerData[i].SqlDayOfWeekColumnName != null)
                        {
                            cmd = new AdsCommand("SELECT " + TimeTriggerData[i].SqlDayOfWeekColumnName + " from CS3000CommServerSettings WHERE CommServerID=" + Constants.CommServerID, conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            dayOfWeek = cmd.ExecuteScalar();
                            cmd.Unprepare();
                        }
                        else
                        {
                            dayOfWeek = DBNull.Value;
                        }

                        cmd = new AdsCommand("SELECT " + TimeTriggerData[i].SqlStartTimeColumnName + " from CS3000CommServerSettings WHERE CommServerID=" + Constants.CommServerID, conn);
                        //_log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                        object Start = cmd.ExecuteScalar();
                        TimeSpan? trigtime;
                        if(Start == DBNull.Value)
                        {
                            trigtime = null;
                        }
                        else
                        {
                            trigtime = (TimeSpan?)Start;
                        }
                        if (TimeTriggerData[i].StartTime != trigtime)
                        {
                            _log.WriteMessage(NLog.LogLevel.Info, "171", "Loaded daily trigger time for action " + TimeTriggerData[i].SqlStartTimeColumnName + " as " + trigtime.ToString());
                            TimeTriggerData[i].StartTime = trigtime;
                        }
                        cmd.Unprepare();

                        object Stop;
                        if (TimeTriggerData[i].SqlStopTimeColumnName != null)
                        {
                            cmd = new AdsCommand("SELECT " + TimeTriggerData[i].SqlStopTimeColumnName + " from CS3000CommServerSettings WHERE CommServerID=" + Constants.CommServerID, conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            Stop = cmd.ExecuteScalar();
                            cmd.Unprepare();
                        }
                        else
                        {
                            Stop = DBNull.Value;
                        }

                        object Freq;
                        if (TimeTriggerData[i].SqlFrequencyColumnName != null)
                        {
                            cmd = new AdsCommand("SELECT " + TimeTriggerData[i].SqlFrequencyColumnName + " from CS3000CommServerSettings WHERE CommServerID=" + Constants.CommServerID, conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                            Freq = cmd.ExecuteScalar();
                            cmd.Unprepare();
                        }
                        else
                        {
                            Freq = DBNull.Value;
                        }

                        // check for change in duration
                        TimeSpan? duration = null;
                        TimeSpan temp;
                        if (Start != DBNull.Value && Stop != DBNull.Value)
                        {
                            if (TimeSpan.Compare((TimeSpan)Start, (TimeSpan)Stop) < 0)   // Start before Stop
                            {
                                duration = ((TimeSpan)Stop).Subtract((TimeSpan)Start);
                            }
                            else
                            {
                                duration = ((TimeSpan)Start).Subtract((TimeSpan)Stop);
                                temp = new TimeSpan(24, 0, 0);
                                duration = temp.Subtract((TimeSpan)duration);
                            }
                        }

                        if (TimeTriggerData[i].RepeatDuration != duration)
                        {
                            _log.WriteMessage(NLog.LogLevel.Info, "171", "(re)Loaded duration for action " + TimeTriggerData[i].SqlStartTimeColumnName + " as " + duration.ToString());
                            TimeTriggerData[i].RepeatDuration = duration;
                        }

                        if (dayOfWeek != DBNull.Value)
                        {
                            if (TimeTriggerData[i].DayOfWeek != (int)dayOfWeek)
                            {
                                _log.WriteMessage(NLog.LogLevel.Info, "171", "(re)Loaded day of week for action " + TimeTriggerData[i].SqlStartTimeColumnName + " as " + dayOfWeek.ToString());
                                TimeTriggerData[i].DayOfWeek = (int)dayOfWeek;
                            }
                        }

                        // check for change in frequency minutes
                        TimeSpan? f = null;
                        if (Freq != DBNull.Value)
                        {
                            f = (TimeSpan?)new TimeSpan(0, Convert.ToInt32(Freq.ToString()), 0);
                        }
                        if (TimeTriggerData[i].RepeatFrequency != f)
                        {
                            _log.WriteMessage(NLog.LogLevel.Info, "171", "(re)Loaded frequency for action " + TimeTriggerData[i].SqlStartTimeColumnName + " as " + f.ToString());
                            TimeTriggerData[i].RepeatFrequency = f;
                        }
                    }
                }
                catch(Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "169", "Database exception during query of periodic task times: " + e.ToString());
                }

                db.CloseDBConnection(conn, "");
            }

        }
        //
        // constructor
        //
        static PeriodicTasks()
        {
            _log = new MyLogger("PeriodicTasks", "" ,"" , Constants.CS3000Model);
            _cts = null;    // this will get set at runtime
            _jobpollrequest_cts = null;
            _combo_cts = null;
            _lastrun = null;
            // start a task to periodically scan for something to send based
            // on database flags getting set for a controller...
            Task.Factory.StartNew(PollLoop, TaskCreationOptions.LongRunning);
            Task.Factory.StartNew(JobLoop, TaskCreationOptions.LongRunning);
        }
        // internal vars
        private static MyLogger _log;
        private static CancellationTokenSource _cts;
        private static CancellationTokenSource _jobpollrequest_cts;
        private static CancellationTokenSource _combo_cts;
        private static DateTime? _lastrun;
        private static Dictionary<int, String> TimeZoneTable = new Dictionary<int, String>(); 

        //
        // routine to remove job from IHSFY_jobs and place it in IHSFY_notifications with given completion status in 
        // the 'Status' column
        //
        async public static Task CompleteJob(int job_number, int completion_status, byte[] responsedata)
        {
            AdsCommand cmd;

            try
            {
                ServerDB db = new ServerDB(_cts);

                Task<AdsConnection> connTask = db.OpenDBConnection("");
                AdsConnection conn = await connTask;

                if (conn != null)
                {
                    // insert final status into notification table
                    // note that the Param4 column is binary data which
                    // will come into this function as responsedata byte array
                    cmd = new AdsCommand("INSERT INTO IHSFY_notifications(Idx, Command_id, Network_id, User_id, Date_created, Status, Param1, Param2, Param3, Param4, Param5 ) "
                        + " SELECT Idx, Command_id, Network_id, User_id,'"
                        + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                        + completion_status.ToString() + ", Param1, Param2, Param3, :v , Param5"
                        + " FROM IHSFY_jobs WHERE Idx = " + job_number.ToString() , conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);
                    AdsParameter prm = new AdsParameter("v", DbType.Binary);
                    if (responsedata == null)
                    {
                        prm.Value = null;
                    }
                    else
                    {
                        _log.WriteMessage(NLog.LogLevel.Info, "155", "Completed job with binary data containing " + responsedata.Length.ToString() + " bytes of data");
                        prm.Value = responsedata;
                    }
                    cmd.Parameters.Add(prm);
                    cmd.ExecuteNonQuery();
                    cmd.Unprepare();
                    // and then delete the job
                    cmd = new AdsCommand("DELETE from IHSFY_jobs where Idx = " + job_number.ToString(), conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);
                    cmd.ExecuteNonQuery();
                    cmd.Unprepare();

                    //update task status table to notify user with the results
                    cmd = new AdsCommand("UPDATE TaskStatus SET Status=" + (completion_status == Constants.JOB_SUCCESSFUL ? 1 : 2) +
                                         ",HasViewed=false WHERE CommServerJobIdx = " + job_number.ToString(), conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "156", "SQL: " + cmd.CommandText);
                    cmd.ExecuteNonQuery();
                    cmd.Unprepare();

                    db.CloseDBConnection(conn, "");
                }
            }
            catch(Exception e)
            {
                _log.WriteMessage(NLog.LogLevel.Error, "178", "Error setting job status/completion in IHSFY_jobs/IHSFY_notifications table: " + e.ToString());
            }

            return;
        }

        //
        // routine to remove any/all mobile jobs from job table (typically at startup)
        // that are likely stale and complete them into the notification table.
        //
        async static Task FlushMobileJobs()
        {
            AdsCommand cmd;
            AdsDataReader rdr;
            int job_idx;

            try
            {
                ServerDB db = new ServerDB(_cts);

                Task<AdsConnection> connTask = db.OpenDBConnection("");
                AdsConnection conn = await connTask;

                if (conn != null)
                {
                    cmd = new AdsCommand("SELECT Idx, Command_id, Network_id, User_id, Date_created, Status, Processing_start, Param1, Param2, Param3, Param5 "
                            + " from IHSFY_jobs" 
                            + " WHERE ( Network_ID IN (SELECT SiteID FROM ControllerSites s JOIN Companies m ON s.CompanyID=m.CompanyID WHERE CommServerID=" + Constants.CommServerID 
                            + " ) OR Controller_ID IN (SELECT ControllerID FROM Controllers c JOIN ControllerSites s ON s.SiteID=c.SiteID JOIN Companies m ON s.CompanyID=m.CompanyID WHERE CommServerID=" + Constants.CommServerID + "))"
                            , conn);
                        _log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);

                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                for (job_idx = 0; job_idx < JobListData.Length; job_idx++)
                                {
                                    if (JobListData[job_idx].MessageID == rdr.GetInt32(1) && JobListData[job_idx].MobileJobFunction)   // Command_id column index is 1
                                    {
                                        _log.WriteMessage(NLog.LogLevel.Info, "155", "Removing likely stale mobile job at startup, MID: " + rdr.GetInt32(1).ToString());
                                        // found a mobile job, complete it as stale
                                        await CompleteJob(rdr.GetInt32(0), Constants.JOB_FLUSH_STALE_MOBILE_AT_STARTUP, null);
                                        break;
                                    }
                                }
                            }
                        }
                        rdr.Close();
                        cmd.Unprepare();

                        db.CloseDBConnection(conn, "");
                }
            }
            catch (Exception e)
            {
                _log.WriteMessage(NLog.LogLevel.Error, "178", "Error flushing mobile jobs table at startup: " + e.ToString());
            }

            return;
        }

        public static void CheckJobsNow()
        {
            if (_jobpollrequest_cts != null)
            {
                _jobpollrequest_cts.Cancel();
            }
            return;
        }

        async private static void JobLoop()
        {
            int delay;
            AdsCommand cmd;
            AdsDataReader rdr;
            int job_idx;
            ActiveController ac;
            int r;

#if MOBILEJOBSPEEDUP
            bool MobileJobFound;
            int MobilePhaseCounter;
#endif


            while (_cts == null)
            {
                await Task.Delay(50);  // wait for somebody to tell us the cancellation token
            }

            _jobpollrequest_cts = new CancellationTokenSource();
            _combo_cts = CancellationTokenSource.CreateLinkedTokenSource(_jobpollrequest_cts.Token, _cts.Token);

            await Task.Delay(2500);       // let everything spin up...

            _log.WriteMessage(NLog.LogLevel.Info, "150", "Job Loop process starting...");

            // at start up flush any old mobile commands to the finished table, 
            // we don't want to send old ones.
            await FlushMobileJobs();

            while (true)
            {
                //
                // query for job to perform
                //

#if MOBILEJOBSPEEDUP
                MobileJobFound = false;
                MobilePhaseCounter = 0;
#endif

                try
                {
                    ServerDB db = new ServerDB(_cts);

                    Task<AdsConnection> connTask = db.OpenDBConnection("");
                    AdsConnection conn = await connTask;

                    if (conn != null)
                    {

                        cmd = new AdsCommand("SELECT Idx, Command_id, Network_id, User_id, Date_created, Status, Processing_start, Param1, Param2, Param3, Param5 "
                            + " from IHSFY_jobs where isnull(Network_id, -1) > -1 "
                            + " AND Network_ID IN (SELECT SiteID FROM ControllerSites s JOIN CS3000Networks n ON s.SiteID=n.NetworkID JOIN Companies m ON s.CompanyID=m.CompanyID WHERE CommServerID=" + Constants.CommServerID + ")"
                            , conn);

                        //_log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);

                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                // first find the parameters for this type of commands...
                                for(job_idx = 0; job_idx < JobListData.Length; job_idx++)
                                {
                                    if(JobListData[job_idx].MessageID == rdr.GetInt32(1))   // Command_id column index is 1
                                    {
                                        break;
                                    }
                                }
                                if(job_idx >= JobListData.Length)
                                {
                                    _log.WriteMessage(NLog.LogLevel.Error, "155", "Table IHSFY_jobs contains unknown command_id: " + rdr.GetInt32(1).ToString());
                                    // error this job out
                                    await CompleteJob(rdr.GetInt32(0), Constants.JOB_UNKNOWN_COMMAND_ID, null);
                                }
                                // first see if this job has expired - by comparing the Date_created + lifetime to current time
                                else if (rdr.GetDateTime(4) + JobListData[job_idx].Lifetime <= DateTime.Now)
                                {
                                    await CompleteJob(rdr.GetInt32(0), Constants.JOB_EXPIRED, null);
                                }
                                //
                                // special case check for rain shutoff expiring at 8pm controller time - param1 contains minutes til that time based
                                // on time job was created.
                                //
                                else if(JobListData[job_idx].MessageID == Constants.MID_FROM_COMMSERVER_RAIN_SHUTDOWN_packet && rdr.GetDateTime(4).AddMinutes(rdr.GetInt32(7)) <= DateTime.Now)
                                {
                                    await CompleteJob(rdr.GetInt32(0), Constants.JOB_EXPIRED, null);
                                }
                                else
                                {
                                    // at this point this is a command we know about... see if time to run                             
                                    // see if the processing_start datetime column is null, indicating a new command
                                    DateTime? lastrun = null;
                                    Boolean runit = false;
                                    Int32 nid;
                                    nid = rdr.GetInt32(2);
                                    if (!rdr.IsDBNull(6))
                                    {
                                        lastrun = rdr.GetDateTime(6);
                                        if (lastrun + JobListData[job_idx].RetryPeriod <= DateTime.Now)
                                        {
                                            runit = true;       // it's time to try this job again
                                        }
                                    }
#if MOBILEJOBSPEEDUP
                                    if (lastrun == null && JobListData[job_idx].MobileJobFunction)
                                    {
                                        MobileJobFound = true;  // flag that we have seen a new "mobile" job
                                        MobilePhaseCounter = 24;    // poll at faster rate this many times before dropping back to default
                                    }
#endif
                                    if (lastrun == null || runit)
                                    {
                                        // get the tracking table entry for this network id
                                        ac = ServerSocketTracker.GetTrackingEntry((uint)nid);
                                        // see if command requires a connection to controller to activate
                                        if (JobListData[job_idx].FailIfNoConnection && ac == null)
                                        {
                                            // fail if not currently connected
                                            _log.WriteMessage(NLog.LogLevel.Error, "159", "No current connection to NID: " + nid.ToString() + " so job is terminated. ");
                                            await CompleteJob(rdr.GetInt32(0), Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                                        }
                                        // there is a connection, job is good to go - call function (if there is one defined)
                                        else if (JobListData[job_idx].JobFunction != null)
                                        {
                                            _log.WriteMessage(NLog.LogLevel.Info, "167", "Executing job number " + rdr.GetInt32(0).ToString() + " of MID " + rdr.GetInt32(1).ToString() + " on NID " + nid.ToString());
                                            // if this is a hub/hub-connected controller, processing using hub logic
                                            int hubresult = HubWrapper.HubFunction(null, JobListData[job_idx].JobFunction, (uint)nid, 0, (uint)rdr.GetInt32(0), 500, _cts); // priority 500 seems like good guess for IHSFY job
                                            if(hubresult < 0)
                                            {
                                                AdsCommand c2 = new AdsCommand("UPDATE IHSFY_jobs SET Processing_start = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' where Idx = " + rdr.GetInt32(0).ToString(), conn);
                                                await c2.ExecuteNonQueryAsync();
                                                c2.Unprepare();
                                                // error
                                                if (hubresult != -3)    // -3 is the quick return hub change when hub not connected
                                                {
                                                    if (hubresult != -2 || JobListData[job_idx].JobFunction != JOB_hubupdate)
                                                    {
                                                        _log.WriteMessage(NLog.LogLevel.Error, "167", "Error " + hubresult.ToString() + " checking for hub option for IHSFY job " + rdr.GetInt32(0).ToString() + " of MID " + rdr.GetInt32(1).ToString() + " on NID " + nid.ToString());
                                                    }
                                                }
                                                await CompleteJob(rdr.GetInt32(0), Constants.JOB_NO_CONNECTION_TO_CONTROLLER, null);
                                            }
                                            if(hubresult == 0)
                                            {
                                                r = JobListData[job_idx].JobFunction((uint)nid, (uint)rdr.GetInt32(0));
                                                // update the processing time to now
                                                AdsCommand c2 = new AdsCommand("UPDATE IHSFY_jobs SET Processing_start = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' where Idx = " + rdr.GetInt32(0).ToString(), conn);
                                                await c2.ExecuteNonQueryAsync();
                                                c2.Unprepare();
                                                // if the job returns anything other than 0, we will complete the job immediately with that status code
                                                if (r != 0)
                                                {
                                                    await CompleteJob(rdr.GetInt32(0), r, null);
                                                }
                                            }
                                            if(hubresult > 0)
                                            {
                                                // being processed by hub logic
                                                AdsCommand c2 = new AdsCommand("UPDATE IHSFY_jobs SET Processing_start = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' where Idx = " + rdr.GetInt32(0).ToString(), conn);
                                                await c2.ExecuteNonQueryAsync();
                                                c2.Unprepare();
                                                // error
                                                _log.WriteMessage(NLog.LogLevel.Info, "167", "IHSFY Job being handled by hub logic " + rdr.GetInt32(0).ToString() + " of MID " + rdr.GetInt32(1).ToString() + " on NID " + nid.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        rdr.Close();
                        cmd.Unprepare();

                        db.CloseDBConnection(conn, "");
                    }
                }
                catch(Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "178", "Error querying/updating IHSFY_jobs table: " + e.ToString());
                }

#if MOBILEJOBSPEEDUP
                // if any new mobile command job found, look again in 5 seconds
                //
                if (MobileJobFound || MobilePhaseCounter > 0)
                {
                    delay = 5;
                    if (MobilePhaseCounter > 0) MobilePhaseCounter--;   // stay at faster rate until this drops to 0
                }
                else
                {
                    delay = 60;
                }
#endif

                delay = Convert.ToInt32(ConfigurationManager.AppSettings["JobLookLoopSeconds"]);

                try
                {
                    // either a QUIT signal, or a CHECK JOBS NOW signal will get us out of delay early
                    await Task.Delay(Convert.ToInt32(delay * 1001), _combo_cts.Token);        // run every X seconds
                }
                catch (TaskCanceledException ex)
                {
                    if (_cts.IsCancellationRequested)
                    {
                        _log.WriteMessage(NLog.LogLevel.Debug, "190", "Exiting Job Loop task " + ex.ToString());
                    }
                    else
                    {
                        _log.WriteMessage(NLog.LogLevel.Debug, "190", "Web signal to check jobs now.");
                        // recreate the tokens needed to look next time
                        _jobpollrequest_cts.Dispose();
                        _combo_cts.Dispose();
                        _jobpollrequest_cts = new CancellationTokenSource();
                        _combo_cts = CancellationTokenSource.CreateLinkedTokenSource(_jobpollrequest_cts.Token, _cts.Token);
                    }
                }

                if (_cts.IsCancellationRequested) break; // program is exiting
            }
        }

        //
        // method to return the current time at a Controller in the given Calsense timezone number
        //
        //
        static public DateTime CurrentTimeAtControllerInTz(int z)
        {
            foreach (var tz in TimeZoneTable)
            {
                if(tz.Key == z)
                {
                    return (TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(tz.Value)));
                }
            }
            return (DateTime.Now);
        }

        async private static void PollLoop()
        {
            DateTime currentcheck;
            DateTime dtatcontroller;
            DateTime lastatcontroller;
            TimeZoneInfo currentzone;
            AdsCommand cmd;
            AdsDataReader rdr;
            uint i;
            int j;
            IntPtr retptr;
            byte b;

            while (_cts == null)
            {
                await Task.Delay(50);  // wait for somebody to tell us the cancellation token
            }

            await Task.Delay(2500);       // let everything spin up...

            _log.WriteMessage(NLog.LogLevel.Info, "150", "Periodic Task process starting...");

            LoadTriggerTimesFromDatabase();

            // gather the time zone index to name mapping by calling into the shared
            // controller code
            // get timezone name strings using call to shared controller function
            for (i = 0; i <= 300; i++)
            {
                // UKNOWN string is returned for unknown mapping
                retptr = GetTimeZoneStr(i);
                if (retptr != IntPtr.Zero)
                {
                    // get returned bytes which should be an ascii character string
                    j = 0;
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    while ((b = System.Runtime.InteropServices.Marshal.ReadByte(retptr, j)) != 0)
                    {
                        sb.Append((char)b);
                        j++;
                    }
                    if (sb.ToString() == "UNKNOWN")
                    {
                        // not known
                    }
                    else
                    {
                        // verify this computer has information about the given time zone
                        try
                        {
                            TimeZoneInfo.FindSystemTimeZoneById(sb.ToString());
                            TimeZoneTable.Add((int)i, sb.ToString());
                            _log.WriteMessage(NLog.LogLevel.Info, "177", "Retreived time zone name for zone id " + i + " as :" + sb.ToString());
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessage(NLog.LogLevel.Error, "178", "Error looking up time zone " + sb.ToString() + " of " + e.ToString());
                        }

                    }
                }
            }

            while(true)
            {
                //
                // see if a trigger time has been hit since we last looked...
                //
                currentcheck = DateTime.Now;      // timestamp of this check
                currentzone = TimeZoneInfo.Local;           // our timezone
                _log.WriteMessage(NLog.LogLevel.Debug, "151", "Check for task to run at commserver time-of-day: " + currentcheck.ToString());

                if(_lastrun != null)
                {
                    // separately process each timezone we know about
                    foreach (var tz in TimeZoneTable)
                    {
                        //
                        // need to know time of day at controller(s) in the given timezone
                        //
                        dtatcontroller = TimeZoneInfo.ConvertTime(currentcheck, TimeZoneInfo.FindSystemTimeZoneById(tz.Value));
                        lastatcontroller = TimeZoneInfo.ConvertTime((DateTime)_lastrun, TimeZoneInfo.FindSystemTimeZoneById(tz.Value));

                        foreach (TimeTriggerStruct task in TimeTriggerData)
                        {
                            if (task.StartTime != null)
                            {
                                // we have the required start time...
                                // this could be:
                                // a) a once per day non-repeating job
                                // b) a once per month non-repeating job
                                // c) one of the above that repeats
                                // we have to check each repeating possibility for the command

                                long NumChecks = 1;

                                if (task.RepeatDuration != null && task.RepeatFrequency != null)
                                {
                                    long ticks1;
                                    long ticks2;
                                    ticks1 = task.RepeatDuration.Value.Ticks;
                                    ticks2 = task.RepeatFrequency.Value.Ticks;
                                    if (ticks1 > 0 && ticks2 > 0)
                                    {
                                        if (ticks1 / ticks2 > 1)
                                        {
                                            NumChecks = ticks1 / ticks2;
                                        }
                                    }
                                }

                                while (NumChecks-- > 0)
                                {

                                    TimeSpan CheckTime = task.StartTime.Value;
                                    for (i = 0; i < NumChecks; i++)
                                    {
                                        // check each possible frequency of command 
                                        CheckTime += task.RepeatFrequency.Value;
                                    }

                                    // see if the trigger time for given entry falls within current checking window
                                    // for weekly jobs, make sure day of week matches
                                    // or if a repeating function check for a repeat time falling in window

                                    bool TimeToRunJob = false;
                                    if (task.DayOfWeek != null)
                                    {
                                        //_log.WriteMessage(NLog.LogLevel.Debug, "152", "Checking to see if weekly legacy task scheduled for " + Enum.GetName(typeof(DayOfWeek),task.DayOfWeek) + " at " + task.StartTime.ToString() + " is within window of " + lastatcontroller.ToString() + " to " + dtatcontroller.ToString());
                                        // weekly job

                                        // just in case a repeating daily job runs into the next day,
                                        // subtract out the days when comparing time to run
                                        TimeSpan tod;
                                        tod = CheckTime.Subtract(TimeSpan.FromDays(CheckTime.Days));
                                        if (tod > lastatcontroller.TimeOfDay && tod <= dtatcontroller.TimeOfDay && ((int)dtatcontroller.DayOfWeek == (int)task.DayOfWeek))
                                        {
                                            TimeToRunJob = true;
                                        }
                                    }
                                    else
                                    {
                                        //_log.WriteMessage(NLog.LogLevel.Debug, "152", "Checking to see if legacy task scheduled for " + task.StartTime.ToString() + " is within window of " + lastatcontroller.ToString() + " to " + dtatcontroller.ToString());
                                        // just in case a repeating daily job runs into the next day,
                                        // subtract out the days when comparing time to run
                                        TimeSpan tod;
                                        tod = CheckTime.Subtract(TimeSpan.FromDays(CheckTime.Days));
                                        if (tod > lastatcontroller.TimeOfDay && tod <= dtatcontroller.TimeOfDay)
                                        {
                                            TimeToRunJob = true;
                                        }
                                    }

                                    if (TimeToRunJob == true)
                                    {
                                        _log.WriteMessage(NLog.LogLevel.Info, "153", "Controllers in time zone " + tz.Value.ToString() + " have a task to be performed");
                                        //
                                        // make a list of all the Controllers from database in this timezone
                                        //
                                        ServerDB db = new ServerDB(_cts);

                                        Task<AdsConnection> connTask = db.OpenDBConnection("");
                                        AdsConnection conn = await connTask;

                                        // list of the network IDs in database for this timezone
                                        List<int> netids = new List<int>();

                                        if (conn != null)
                                        {
                                            try
                                            {
                                                // use the query specific to this task to get all the applicable NIDs, appending the timezone to query ending in AND Timezone = "
                                                cmd = new AdsCommand(task.NIDQuery + tz.Key.ToString(), conn);
                                                _log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);
                                                rdr = cmd.ExecuteReader();
                                                if (rdr.HasRows)
                                                {

                                                    while (rdr.Read())
                                                    {
                                                        netids.Add(rdr.GetInt32(0));    // put the next network ID into a list
                                                    }
                                                    _log.WriteMessage(NLog.LogLevel.Info, "156", "Number of controllers in DB to process in time zone " + tz.Value.ToString() + " is " + netids.Count.ToString());
                                                }
                                                else
                                                {
                                                    _log.WriteMessage(NLog.LogLevel.Debug, "155", "FYI no controllers present in time zone " + tz.Value.ToString());
                                                }
                                                rdr.Close();
                                                cmd.Unprepare();

                                            }
                                            catch (Exception e)
                                            {
                                                _log.WriteMessage(NLog.LogLevel.Error, "166", "Database exception during query of controllers by periodic task: " + e.ToString());
                                            }

                                            db.CloseDBConnection(conn, "");
                                        }

                                        //
                                        // setup a task to call given function
                                        //
                                        foreach (uint nid in netids)
                                        {
                                            // see if this NID hub connected
                                            int hubresult = HubWrapper.HubFunction(task.ProcessingFunction, null, (uint)nid, (uint)tz.Key, 0, 200, _cts); // priority 200 seems like good guess for periodic jobs
                                            if (hubresult > 0)
                                            {
                                                // being processed by hub logic
                                                _log.WriteMessage(NLog.LogLevel.Debug, "160", "Firing up " + task.StartTime.ToString() + " task via hub logic for network ID " + nid.ToString());
                                            }
                                            else if (hubresult == 0)
                                            {
                                                _log.WriteMessage(NLog.LogLevel.Debug, "160", "Firing up " + task.StartTime.ToString() + " task for network ID " + nid.ToString());
                                                // pragma to remove build warning about not await these "fire and forget tasks"
#pragma warning disable 4014
                                                Task.Run(() => task.ProcessingFunction(nid, (uint)tz.Key), _cts.Token).ConfigureAwait(false);
                                            }
                                            else
                                            {
                                                _log.WriteMessage(NLog.LogLevel.Warn, "160", "Error (possibly not connected?) on " + task.StartTime.ToString() + " task versus hub logic for network ID " + nid.ToString());
                                            }
                                        }
                                    }

                                }
                            }

                        }
                    }
                }

                //
                // save when we ran
                //
                _lastrun = currentcheck;

                try
                {
                    await Task.Delay(Convert.ToInt32(60 * 1000), _cts.Token);        // run every minute
                }
                catch (TaskCanceledException ex)
                {
                    _log.WriteMessage(NLog.LogLevel.Debug, "190", "Exiting Periodic Tasks task " + ex.ToString());
                }

                LoadTriggerTimesFromDatabase();     // check for any changes to run at times...

                if (_cts.IsCancellationRequested) break; // program is exiting
            }
        }

        public static void SetCts(CancellationTokenSource c)
        {
            _cts = c;
        }

        //
        // Action spun up by Task.Run() above to set a given controllers clock.
        // (Controller specified by nid=Network ID argument)
        // build a packet stream containing current time and transmit out
        // on network stream for given controller
        //
        static void ActionSetControllerClock(uint nid, uint timezonekey)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            DateTime timeatcontroller;
            UInt16 d;
            UInt32 s;

            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                var mem = new MemoryStream(0);
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                // network id
                Misc.Uint32ToNetworkBytes(nid, mem);
                // set clock MID
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_set_your_time_and_date, mem);
                // 6 byte date/time for current date/time... 
                // first get current date/time in our local time zone
                // and convert to this controllers time zone
                timeatcontroller = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(TimeZoneTable[(int)timezonekey]));
                // now build 6 byte Calsense date time structure from above c# DateTime object
                // first four bytes are 32 bit time
                // next two bytes are 16 bit date
                d = Misc.PackedDatefromDateTime(timeatcontroller);
                s = (UInt32)timeatcontroller.TimeOfDay.TotalSeconds;
                Misc.Uint32ToNetworkBytes(s, mem);
                Misc.Uint16ToNetworkBytes(d, mem);

                data = mem.ToArray();

                crc = Misc.CRC(data);
                //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                _log.WriteMessage(NLog.LogLevel.Debug, "178", "SetTimeDate to be transmitted to SN:" + ac.serial_number.ToString());

                // combine preamble, data, CRC and then postamble
                byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                byte[] crcbytes;
                crcbytes = System.BitConverter.GetBytes(crc);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(crcbytes, 0, crcbytes.Length);
                }
                System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                try
                {
                    ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                    ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                    // update stats tracking table
                    CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                    CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of SetDateTime to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                    // remove this entry from socket table so we don't keep trying
                    ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                }

                mem.Dispose();
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Debug, "162", "Not setting clock for network ID " + nid.ToString() + " as it is not currently connected.");
            }

            return;
        }


        //
        // Action spun up by Task.Run() above to get rain or ET from the controllers
        // involved with hubs, these controllers requires a poll from the commserver 
        // to send the message
        // (Controller specified by nid=Network ID argument)
        //
        static void ActionHubInitiateGetWeatherData(uint nid, uint timezonekey)
        {
            _log.WriteMessageWithId(NLog.LogLevel.Info, "178", "Polling controller involved with a hub and has weather data to share ", nid.ToString());

            JOB_DemandPoll(nid, 0); // 0 - no job id

            return;
        }

        //
        // Action spun up by Task.Run() above to get rain or ET from the controllers
        // involved with hubs, these controllers requires a poll from the commserver 
        // to send the message
        // (Controller specified by nid=Network ID argument)
        //
        static void ActionHubInitiateRainPoll(uint nid, uint timezonekey)
        {
            _log.WriteMessageWithId(NLog.LogLevel.Info, "178", "Polling controller involved with a hub and has rain sensor to share ", nid.ToString());

            JOB_DemandPoll(nid, 0); // 0 - no job id

            return;
        }

        //
        // called when a status screen comes in ... generate a dummy job and then complete it.
        // (passing along the received data buffer to be placed in the Param4 blob of notification table)
        //
        public static async Task<Tuple<List<byte[]>, String, bool, int>> ReceivedStatusScreen(ClientInfoStruct client, CancellationTokenSource cts)
        {
            byte[] screen = new byte[client.rxlen];
            AdsCommand cmd;
            int? job_number = 0;
            byte[] data;

            _log.WriteMessage(NLog.LogLevel.Info, "422", "Processing Status Screen MID: " + client.mid.ToString() + " with " + client.rxlen.ToString() + " bytes of screen data");

            Array.Copy(client.rx, screen, client.rxlen);

            // database connection
            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(Misc.ClientLogString(client));
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                // make the dummy job, which will give us a row and unique ID to work with
                try
                {
                    cmd = new AdsCommand("INSERT INTO IHSFY_jobs(Command_ID, Network_id, User_id, Date_created, Status) "
                            + " VALUES(" + Constants.MID_TO_COMMSERVER_status_screen_init_packet.ToString() + ","
                            + client.nid.ToString() + ","
                            + "-1,'"
                            + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                            + "0)", conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "483", "SQL: " + cmd.CommandText);
                    cmd.ExecuteNonQuery();
                    cmd.Unprepare();

                    // get the job number that just got generated (via SQL autoinc column)
                    cmd = new AdsCommand("select LASTAUTOINC( CONNECTION ) from system.iota ", conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "483", "SQL: " + cmd.CommandText);
                    job_number = (Int32?)cmd.ExecuteScalar();
                    cmd.ExecuteNonQuery();
                    cmd.Unprepare();
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "223", "Exception during database processing during Status Screen reception, Error: " + e.ToString());
                }

                db.CloseDBConnection(conn, Misc.ClientLogString(client));
            }

            // if we generated a job, mark it complete.
            if (job_number != 0 && job_number != null)
            {
                await CompleteJob((Int32)job_number, Constants.JOB_SUCCESSFUL, screen);

                var mem = new MemoryStream(0);
                // build ack response
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(client.sn, mem);
                // network id
                Misc.Uint32ToNetworkBytes(client.nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_status_screen_ack_packet, mem);
                _log.WriteMessage(NLog.LogLevel.Info, "224", "Build Status Screen Ack response.");

                // get the built response to return and be sent
                data = mem.ToArray();
                List<byte[]> rsplist = new List<byte[]>();
                rsplist.Add(data);
                return (Tuple.Create(rsplist, (String)null, false, Constants.NO_STATE_DATA));
            }
            else
            {
                // no response and leave connection up
                return (Tuple.Create((List<byte[]>)null, (String)null, false, Constants.NO_STATE_DATA));
            }
        }
        //
        // weather data action
        //
        //  UNS_8	whats_included (1)
        //  ET_TABLE_ENTRY (4)
        //	RAIN_TABLE_ENTRY (4)
        static void ActionInitiateWeatherData(uint nid, uint timezonekey)
        {
            AdsCommand cmd;
            // since we require an ACK back from controller for this message, 
            // we will
            // insert a job into the IHSFY_jobs table 
            // and let the job processing take it from there
            _log.WriteMessage(NLog.LogLevel.Debug, "182", "Generating new Weather Data job for NID: " + nid.ToString());
            ServerDB db = new ServerDB(_cts);
            AdsConnection conn = db.OpenDBConnectionNotAsync("");
            if(conn != null)
            {
                try
                {
                    cmd = new AdsCommand("INSERT INTO IHSFY_jobs(Command_ID, Network_id, User_id, Date_created, Status) "
                            + " VALUES(" + Constants.MID_FROM_COMMSERVER_WEATHER_DATA_packet.ToString() + ","
                            + nid.ToString() + ","
                            + "-1,'"
                            + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',"
                            + "0)", conn);
                    _log.WriteMessage(NLog.LogLevel.Debug, "183", "SQL: " + cmd.CommandText);
                    cmd.ExecuteNonQuery();
                    cmd.Unprepare();
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "179", "Database exception during Weather Request job generation: " + e.ToString());
                }

                db.CloseDBConnection(conn, "");
            }
        }
        static int JOB_SendWeatherData(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            Int32? ETID = null;
            Int32? ETCID = null;
            bool ETIsController = false;
            Int32? RainID = null;
            Int32? RainCID = null;
            bool RainIsController = false;
            Int32? ETStatus = null;
            Int32? ETValue = null;
            Int32? RainStatus = null;
            Int32? RainValue = null;
            bool WantsET = true;
            bool WantsRain = true;
            AdsCommand cmd;
            int retcode = 0;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Checking for Weather config for NID " + nid.ToString());
                ServerDB db = new ServerDB(_cts);

                AdsConnection conn = db.OpenDBConnectionNotAsync("");
                if (conn != null)
                {
                    do
                    {
                        try
                        {
                            if (WantsET)
                            {
                                cmd = new AdsCommand("SELECT IFNULL(ETSharingNetworkID,-2) from CS3000Networks n join controllersites s on s.siteid=n.networkid  WHERE s.deleted = false and  n.NetworkID=" + nid.ToString(), conn);
                                _log.WriteMessage(NLog.LogLevel.Debug, "185", "SQL: " + cmd.CommandText);
                                ETID = (Int32?)cmd.ExecuteScalar();
                                cmd.Unprepare();

                                cmd = new AdsCommand("SELECT IFNULL(ETSharingControllerID,-2) from CS3000Networks n join controllersites s on s.siteid=n.networkid  WHERE s.deleted = false and  n.NetworkID=" + nid.ToString(), conn);
                                _log.WriteMessage(NLog.LogLevel.Debug, "185", "SQL: " + cmd.CommandText);
                                ETCID = (Int32?)cmd.ExecuteScalar();
                                cmd.Unprepare();
                                // if we have a legacy controller configured, mark that now.
                                if(ETCID > 0)
                                {
                                    ETIsController = true;
                                    ETID = ETCID;
                                }
                            }

                            if (WantsRain)
                            {
                                cmd = new AdsCommand("SELECT IFNULL(RainSharingNetworkID,-2) from CS3000Networks n join controllersites s on s.siteid=n.networkid  WHERE s.deleted = false and  n.NetworkID=" + nid.ToString(), conn);
                                _log.WriteMessage(NLog.LogLevel.Debug, "185", "SQL: " + cmd.CommandText);
                                RainID = (Int32?)cmd.ExecuteScalar();
                                cmd.Unprepare();

                                cmd = new AdsCommand("SELECT IFNULL(RainSharingControllerID,-2) from CS3000Networks n join controllersites s on s.siteid=n.networkid  WHERE s.deleted = false and  n.NetworkID=" + nid.ToString(), conn);
                                _log.WriteMessage(NLog.LogLevel.Debug, "185", "SQL: " + cmd.CommandText);
                                RainCID = (Int32?)cmd.ExecuteScalar();
                                cmd.Unprepare();
                                // if we have a legacy controller configured, mark that now.
                                if(RainCID > 0)
                                {
                                    RainIsController = true;
                                    RainID = RainCID;
                                }
                            }

                            if ((ETID == null || ETID == -2 || ETID == -1) && (RainID == null || RainID == -2 || RainID == -1))
                            {
                                // no weather info to send.
                                _log.WriteMessage(NLog.LogLevel.Info, "185", "No ET/Rain configured for nid " + nid.ToString());
                            }
                            else
                            {
                                _log.WriteMessage(NLog.LogLevel.Info, "186", "ET/Rain configured for nid " + nid.ToString() + ", gathering...");
                                if (ETID > 0)
                                {
                                    // different network to look at for ET
                                    if (!ETIsController)
                                    {
                                        cmd = new AdsCommand("SELECT ifnull(ETStatus, 0) FROM CS3000WeatherData WHERE " +
                                            " (NetworkID=" + ETID.ToString() +
                                            " AND CAST(WeatherTimeStamp as SQL_DATE) = CURDATE())  ", conn);
                                        _log.WriteMessage(NLog.LogLevel.Debug, "188", "SQL: " + cmd.CommandText);
                                        ETStatus = (Int32?)cmd.ExecuteScalar();

                                        // @WB: we need to modify the ET status in some cases
                                        if (ETStatus != null)
                                        {
                                            // ETStatus = 2 "ET_STATUS_MANUALLY_EDITED_AT_CONTROLLER", we don't think we will ever see this status coming 
                                            // from the controller, but if so we need to changed to 4 "ET_STATUS_SHARED_FROM_THE_CENTRAL"
                                            if (ETStatus == 2) ETStatus = 4;
                                            else if (ETStatus == 0) ETStatus = null; // don't share ET with status = 0 "ET_STATUS_HISTORICAL"
                                        }

                                        cmd.Unprepare();
                                        cmd = new AdsCommand("SELECT ifnull(ETValue, 0) FROM CS3000WeatherData WHERE " +
                                            " (NetworkID=" + ETID.ToString() +
                                            " AND CAST(WeatherTimeStamp as SQL_DATE) = CURDATE())  ", conn);
                                        _log.WriteMessage(NLog.LogLevel.Debug, "188", "SQL: " + cmd.CommandText);
                                        ETValue = (Int32?)cmd.ExecuteScalar();
                                        cmd.Unprepare();
                                    }
                                    else
                                    {
                                        // legacy controller provides ET

                                        cmd = new AdsCommand("SELECT ifnull(ETStatus, 0) FROM ETShare WHERE " +
                                                              " ControllerID=" + ETID.ToString() +
                                                              " AND ETDate=CURDATE()", conn);
                                        _log.WriteMessage(NLog.LogLevel.Debug, "188", "SQL: " + cmd.CommandText);
                                        ETStatus = (Int32?)cmd.ExecuteScalar();
                                        cmd.Unprepare();

                                        // @WB: we need to modify the 2000 ET status to match with the 3000 ET status.
                                        if (ETStatus != null)
                                        {
                                            if (ETStatus == 2) ETStatus = 4; // 4 "ET_STATUS_SHARED_FROM_THE_CENTRAL"
                                            else if (ETStatus == 1) ETStatus = null; // don't share ET with status = 0 "ET_STATUS_HISTORICAL"
                                        }

                                        cmd = new AdsCommand("SELECT ifnull(ET, 0) FROM ETShare WHERE " +
                                                              " ControllerID=" + ETID.ToString() +
                                                              " AND ETDate=CURDATE()", conn);
                                        _log.WriteMessage(NLog.LogLevel.Debug, "188", "SQL: " + cmd.CommandText);
                                        var et = cmd.ExecuteScalar();

                                        if (et !=null)
                                        {
                                            ETValue = (Int32)((Int32)et * 100.0); // @wb: 2000 ET store 100 up, 3000 controller expecting 10000 up.
                                        }
                                        else
                                        {
                                            ETValue = null;
                                        }
                                        cmd.Unprepare();

                                    }

                                }
                                if (RainID > 0)
                                {
                                    // different network to look at for Rain
                                    if (!RainIsController)
                                    {
                                        cmd = new AdsCommand("SELECT ifnull(RainStatus, 0) FROM CS3000WeatherData WHERE " +
                                            " (NetworkID=" + RainID.ToString() +
                                            " AND CAST(WeatherTimeStamp as SQL_DATE) = CURDATE())  ", conn);
                                        _log.WriteMessage(NLog.LogLevel.Debug, "188", "SQL: " + cmd.CommandText);
                                        RainStatus = (Int32?)cmd.ExecuteScalar();
                                        cmd.Unprepare();

                                        // @WB: we need to modify the Rain status in some cases
                                        if (RainStatus != null)
                                        {
                                            // RainStatus = 2 "RAIN_STATUS_MANUALLY_EDITED_AT_CONTROLLER", we don't think we will ever see this status coming 
                                            // from the controller, but if so we need to changed to 4 "RAIN_STATUS_SHARED_FROM_THE_CENTRAL"
                                            // RainStatus = 1 "RAIN_STATUS_FROM_RAIN_BUCKET_R" change to to 4 "RAIN_STATUS_SHARED_FROM_THE_CENTRAL" as well
                                            if (RainStatus == 2 || RainStatus == 1) RainStatus = 4;
                                        }

                                        cmd = new AdsCommand("SELECT ifnull(RainValue, 0) FROM CS3000WeatherData WHERE " +
                                            " (NetworkID=" + RainID.ToString() +
                                            " AND CAST(WeatherTimeStamp as SQL_DATE) = CURDATE())  ", conn);
                                        _log.WriteMessage(NLog.LogLevel.Debug, "188", "SQL: " + cmd.CommandText);
                                        RainValue = (Int32?)cmd.ExecuteScalar();
                                        cmd.Unprepare();
                                    }
                                    else
                                    {
                                        // legacy controller provides Rain
                                        cmd = new AdsCommand("SELECT ifnull(RainStatus, 0) FROM RainShare WHERE " +
                                                              " ControllerID=" + RainID.ToString() + 
                                                              " AND RainDate=CURDATE()", conn);
                                        _log.WriteMessage(NLog.LogLevel.Debug, "188", "SQL: " + cmd.CommandText);
                                        RainStatus = (Int32?)cmd.ExecuteScalar();
                                        cmd.Unprepare();

                                        // @WB: we need to modify the 2000 Rain status to match with the 3000 status.
                                        if (RainStatus != null)
                                        {
                                            // RAIN_STATUS_FROM_RAIN_BUCKET_M in legacy is 7, change it to 0
                                            if (RainStatus == 7) RainStatus = 0;
                                            else if (RainStatus == 6) RainStatus = 4; // 4 "RAIN_STATUS_SHARED_FROM_THE_CENTRAL"
                                        }

                                        cmd = new AdsCommand("SELECT ifnull(Rain, 0) FROM RainShare WHERE " +
                                                              " ControllerID=" + RainID.ToString() +
                                                              " AND RainDate=CURDATE()", conn);
                                        _log.WriteMessage(NLog.LogLevel.Debug, "188", "SQL: " + cmd.CommandText);
                                        RainValue = (Int32?)cmd.ExecuteScalar();
                                        cmd.Unprepare();
                                    }
                                }
                                // look at new weather source if configured 0
                                if(ETID == 0)
                                {
                                    _log.WriteMessage(NLog.LogLevel.Info, "182", "Using new WeatherSenseState for ET on " + nid.ToString());
                                    cmd = new AdsCommand("SELECT ETStatus FROM WeatherSenseData  WHERE " +
                                                        " (ControllerID=" + nid.ToString() +
                                                        " AND ControllerModel > 6 AND WeatherDate=CURDATE()) ", conn);
                                    _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                                    ETStatus = (Int32?)cmd.ExecuteScalar();
                                    cmd.Unprepare();
                                    cmd = new AdsCommand("SELECT ETValue FROM WeatherSenseData  WHERE " +
                                                        " (ControllerID=" + nid.ToString() +
                                                        " AND ControllerModel > 6 AND WeatherDate=CURDATE()) ", conn);
                                    _log.WriteMessage(NLog.LogLevel.Debug, "188", "SQL: " + cmd.CommandText);
                                    var et = cmd.ExecuteScalar();

                                    if (et != null)
                                    {
                                        ETValue = (Int32)((double)et * 10000.0); // @wb: 3000 controller expecting 10000 up.
                                    }
                                    else
                                    {
                                        ETValue = null;
                                        var msg = "No ET data available (WeatherSense)";
                                        Misc.AddAlert(_cts, nid, msg, true, false);
                                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "2888", msg, nid.ToString());
                                    }

                                    cmd.Unprepare();
                                }
                                if (RainID == 0)
                                {
                                    _log.WriteMessage(NLog.LogLevel.Info, "182", "Using new WeatherSenseState for Rain on " + nid.ToString());
                                    cmd = new AdsCommand("SELECT RainStatus FROM WeatherSenseData  WHERE " +
                                                        " (ControllerID=" + nid.ToString() +
                                                        " AND ControllerModel > 6 AND WeatherDate=CURDATE()) ", conn);
                                    _log.WriteMessage(NLog.LogLevel.Debug, "171", "SQL: " + cmd.CommandText);
                                    RainStatus = (Int32?)cmd.ExecuteScalar();
                                    cmd.Unprepare();
                                    cmd = new AdsCommand("SELECT RainValue FROM WeatherSenseData  WHERE " +
                                                        " (ControllerID=" + nid.ToString() +
                                                        " AND ControllerModel > 6 AND WeatherDate=CURDATE()) ", conn);
                                    _log.WriteMessage(NLog.LogLevel.Debug, "188", "SQL: " + cmd.CommandText);

                                    var rain = cmd.ExecuteScalar();

                                    if (rain != null)
                                    {
                                        RainValue = (Int32)((double)rain * 100.0); // @wb: 3000 controller expecting 100 up.
                                    }
                                    else
                                    {
                                        RainValue = null;
                                        var msg = "No Rain data available (WeatherSense)";
                                        Misc.AddAlert(_cts, nid, msg, true, false);
                                        _log.WriteMessageWithId(NLog.LogLevel.Warn, "2888", msg, nid.ToString());
                                    }

                                    cmd.Unprepare();
                                }                                
                            }
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessage(NLog.LogLevel.Error, "179", "Database exception during Weather Request checking: " + e.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    db.CloseDBConnection(conn, "");
                }

                if (RainStatus != null || ETStatus != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // weather data mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_WEATHER_DATA_packet, mem);
                    // weather data with what's included byte first
                    byte versionnumberbyte = 0;
                    if (WantsET && ETStatus != null)
                    {
                        versionnumberbyte |= (1 << Constants.WEATHER_DATA_HAS_ET);
                    }
                    if (WantsRain && RainStatus != null)
                    {
                        versionnumberbyte |= (1 << Constants.WEATHER_DATA_HAS_RAIN);
                    }
                    mem.WriteByte(versionnumberbyte);
                    if (WantsET && ETStatus != null)
                    {
                        UInt16 tempetinches = (UInt16)(ETValue == null ? 0 : ETValue);
                        Int16 tempetstatus = (Int16)(ETStatus == null ? 0 : ETStatus);
                        Misc.Uint16ToNetworkBytes((UInt16)tempetinches, mem);
                        Misc.Uint16ToNetworkBytes((UInt16)tempetstatus, mem);
                    }
                    if (WantsRain && RainStatus != null)
                    {
                        UInt16 tempraininches = (UInt16)(RainValue == null ? 0 : RainValue);
                        Int16 temprainstatus = (Int16)(RainStatus == null ? 0 : RainStatus);
                        Misc.Uint16ToNetworkBytes((UInt16)tempraininches, mem);
                        Misc.Uint16ToNetworkBytes((UInt16)temprainstatus, mem);
                    }

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "Weather Data to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);

                        retcode = Constants.JOB_SUCCESSFUL;
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of Weather Data to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());

                        retcode = Constants.JOB_ERROR_WHILE_SENDING;

                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    retcode = Constants.JOB_NO_WEATHERSENSE_DATA;

                    _log.WriteMessage(NLog.LogLevel.Info, "162", "No weather data available for NID " + nid.ToString());
                }
            }
            else
            {
                retcode = Constants.JOB_ERROR_WHILE_SENDING;

                _log.WriteMessage(NLog.LogLevel.Debug, "162", "Can't send weather data to network ID " + nid.ToString() + " right now as it is not connected.");
            }

            return retcode;     // normally 0, but error code if Weathersense API returned no weather data
        }

        //
        // station turn on message
        //	UNS_32	box_index_0 (4)
        //	UNS_32	station_number_0 (4)
        //  UNS_32	for_how_long_seconds (4)
        //
        static int JOB_SendOnCommand(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            AdsCommand cmd;
            AdsDataReader rdr;
            Int32? p1 = null;
            Int32? p2 = null;
            Int32? p3 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "ON command for NID " + nid.ToString());
                ServerDB db = new ServerDB(_cts);

                AdsConnection conn = db.OpenDBConnectionNotAsync("");
                if (conn != null)
                {
                    do
                    {
                        try
                        {
                            cmd = new AdsCommand("SELECT Idx, Command_id, Network_id, User_id, Date_created, Status, Processing_start, Param1, Param2, Param3 "
                                + " from IHSFY_jobs where Idx = " + jobnumber.ToString() + " and Network_id = " + nid.ToString(), conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);

                            rdr = cmd.ExecuteReader();
                            if (!rdr.HasRows)
                            {
                                // no weather info to send.
                                _log.WriteMessage(NLog.LogLevel.Error, "185", "Could not find ON job in jobs table for nid: " + nid.ToString() + " with job number " + jobnumber.ToString());
                            }
                            else
                            {
                                rdr.Read();
                                if(rdr.IsDBNull(7))
                                {
                                    p1 = null;
                                }
                                else
                                {
                                    p1 = rdr.GetInt32(7);
                                }
                                if(rdr.IsDBNull(8))
                                {
                                    p2 = null;
                                }
                                else
                                {
                                    p2 = rdr.GetInt32(8);
                                }
                                if(rdr.IsDBNull(9))
                                {
                                    p3 = null;
                                }
                                else
                                {
                                    p3 = rdr.GetInt32(9);
                                }
                            }
                            rdr.Close();
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessage(NLog.LogLevel.Error, "179", "Database exception during ON command checking: " + e.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    db.CloseDBConnection(conn, "");
                }

                if (p1 != null && p2 != null & p3 != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // ON mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_mobile_station_ON, mem);
                    // job number which will be returned for us to match up ACK
                    Misc.Uint32ToNetworkBytes(jobnumber, mem);
                    // box_index_0 from param1
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);
                    // station number_0 from param2
                    Misc.Uint32ToNetworkBytes((uint)p2, mem);
                    // duration in seconds from param3
                    Misc.Uint32ToNetworkBytes((uint)p3, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "ON Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of ON command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for ON command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending ON command.");
            }

            return 0;
        }

        //
        //
        // station turn off message
        //	UNS_32	box_index_0 (4)
        //	UNS_32	station_number_0 (4)
        static int JOB_SendOffCommand(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            AdsCommand cmd;
            AdsDataReader rdr;
            Int32? p1 = null;
            Int32? p2 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "OFF command for NID " + nid.ToString());
                ServerDB db = new ServerDB(_cts);

                AdsConnection conn = db.OpenDBConnectionNotAsync("");
                if (conn != null)
                {
                    do
                    {
                        try
                        {
                            cmd = new AdsCommand("SELECT Idx, Command_id, Network_id, User_id, Date_created, Status, Processing_start, Param1, Param2, Param3 "
                                + " from IHSFY_jobs where Idx = " + jobnumber.ToString() + " and Network_id = " + nid.ToString(), conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);

                            rdr = cmd.ExecuteReader();
                            if (!rdr.HasRows)
                            {
                                // no weather info to send.
                                _log.WriteMessage(NLog.LogLevel.Error, "185", "Could not find OFF job in jobs table for nid: " + nid.ToString() + " with job number " + jobnumber.ToString());
                            }
                            else
                            {
                                rdr.Read();
                                if (rdr.IsDBNull(7))
                                {
                                    p1 = null;
                                }
                                else
                                {
                                    p1 = rdr.GetInt32(7);
                                }
                                if (rdr.IsDBNull(8))
                                {
                                    p2 = null;
                                }
                                else
                                {
                                    p2 = rdr.GetInt32(8);
                                }
                            }
                            rdr.Close();
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessage(NLog.LogLevel.Error, "179", "Database exception during OFF Command checking: " + e.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    db.CloseDBConnection(conn, "");
                }

                if (p1 != null && p2 != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // ON mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_mobile_station_OFF, mem);
                    // job number which will be returned for us to match up ACK
                    Misc.Uint32ToNetworkBytes(jobnumber, mem);
                    // box_index_0 from param1
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);
                    // station number_0 from param2
                    Misc.Uint32ToNetworkBytes((uint)p2, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "OFF Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of OFF command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for OFF command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending OFF command.");
            }

            return 0;
        }

        //
        // controlling controller has reported rain shutoff - tell *this*controller
        // the news. keep trying until 8pm controller local time.
        //
        static int JOB_SendRainShutdown(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
 
            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Rain shutoff command for NID " + nid.ToString());

                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // ON mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_RAIN_SHUTDOWN_packet, mem);
                    // job number which will be returned for us to match up ACK
                    // Misc.Uint32ToNetworkBytes(jobnumber, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "Rain shutoff command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of Rain shutoff command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending rain shutoff.");
            }

            return 0;
        }

        //
        // tell controller someone wants a status screen
        //
        static int JOB_SendStatusScreenRequest(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "382", "Status Screen request for NID " + nid.ToString());

                var mem = new MemoryStream(0);
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                // network id
                Misc.Uint32ToNetworkBytes(nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_send_a_status_screen, mem);

                data = mem.ToArray();

                crc = Misc.CRC(data);
                //_log.WriteMessage(NLog.LogLevel.Debug, "370", "Calculated CRC: 0x" + crc.ToString("X"));
                _log.WriteMessage(NLog.LogLevel.Debug, "378", "Status Screen request to be transmitted to SN:" + ac.serial_number.ToString());

                // combine preamble, data, CRC and then postamble
                byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                byte[] crcbytes;
                crcbytes = System.BitConverter.GetBytes(crc);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(crcbytes, 0, crcbytes.Length);
                }
                System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                try
                {
                    ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                    ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                    // update stats tracking table
                    CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                    CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "375", "Exception during transmit of Status Screen request command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                    // remove this entry from socket table so we don't keep trying
                    ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                }

                mem.Dispose();
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "362", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending status screen request.");
            }

            return 0;
        }

        //
        // tell controller someone request a factory reset to the new controller
        //
        static int JOB_FactoryResetToNewControllerRequest(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "382", "Factory reset request for NID " + nid.ToString());

                var mem = new MemoryStream(0);
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                // network id
                Misc.Uint32ToNetworkBytes(nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_panel_swap_factory_reset_to_new_panel, mem);

                data = mem.ToArray();

                crc = Misc.CRC(data);
                //_log.WriteMessage(NLog.LogLevel.Debug, "370", "Calculated CRC: 0x" + crc.ToString("X"));
                _log.WriteMessage(NLog.LogLevel.Debug, "378", "Factory reset request to be transmitted to SN:" + ac.serial_number.ToString());

                // combine preamble, data, CRC and then postamble
                byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                byte[] crcbytes;
                crcbytes = System.BitConverter.GetBytes(crc);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(crcbytes, 0, crcbytes.Length);
                }
                System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                try
                {
                    ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                    ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                    // update stats tracking table
                    CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                    CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "375", "Exception during transmit of Factory reset request command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                    // remove this entry from socket table so we don't keep trying
                    ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                }

                mem.Dispose();
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "362", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for requesting factory reset.");
            }

            return 0;
        }

        //
        // tell controller someone request a factory reset to the new controller
        //
        static int JOB_FactoryResetToOldControllerRequest(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "382", "Factory reset request for NID " + nid.ToString());

                var mem = new MemoryStream(0);
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                // network id
                Misc.Uint32ToNetworkBytes(nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_panel_swap_factory_reset_to_old_panel, mem);

                data = mem.ToArray();

                crc = Misc.CRC(data);
                //_log.WriteMessage(NLog.LogLevel.Debug, "370", "Calculated CRC: 0x" + crc.ToString("X"));
                _log.WriteMessage(NLog.LogLevel.Debug, "378", "Factory reset request to be transmitted to SN:" + ac.serial_number.ToString());

                // combine preamble, data, CRC and then postamble
                byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                byte[] crcbytes;
                crcbytes = System.BitConverter.GetBytes(crc);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(crcbytes, 0, crcbytes.Length);
                }
                System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                try
                {
                    ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                    ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                    // update stats tracking table
                    CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                    CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "375", "Exception during transmit of Factory reset request command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                    // remove this entry from socket table so we don't keep trying
                    ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                }

                mem.Dispose();
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "362", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for requesting factory reset.");
            }

            return 0;
        }

        //
        // tell controller someone request to set all bits in the controller
        //
        static int JOB_SetAllBitsRequest(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "382", "Set all bits request for NID " + nid.ToString());

                var mem = new MemoryStream(0);
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                // network id
                Misc.Uint32ToNetworkBytes(nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_set_all_bits, mem);

                data = mem.ToArray();

                crc = Misc.CRC(data);
                //_log.WriteMessage(NLog.LogLevel.Debug, "370", "Calculated CRC: 0x" + crc.ToString("X"));
                _log.WriteMessage(NLog.LogLevel.Debug, "378", "Set all bits request to be transmitted to SN:" + ac.serial_number.ToString());

                // combine preamble, data, CRC and then postamble
                byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                byte[] crcbytes;
                crcbytes = System.BitConverter.GetBytes(crc);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(crcbytes, 0, crcbytes.Length);
                }
                System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                try
                {
                    ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                    ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                    // update stats tracking table
                    CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                    CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "375", "Exception during transmit of set all bits request command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                    // remove this entry from socket table so we don't keep trying
                    ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                }

                mem.Dispose();
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "362", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for setting all bits.");
            }

            return 0;
        }

        //
        // Force registration packet
        //
        static int JOB_ForceRegistration(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "382", "Force registration request for NID " + nid.ToString());

                var mem = new MemoryStream(0);
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                // network id
                Misc.Uint32ToNetworkBytes(nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_force_registration, mem);

                data = mem.ToArray();

                crc = Misc.CRC(data);
                //_log.WriteMessage(NLog.LogLevel.Debug, "370", "Calculated CRC: 0x" + crc.ToString("X"));
                _log.WriteMessage(NLog.LogLevel.Debug, "378", "Force registration request to be transmitted to SN:" + ac.serial_number.ToString());

                // combine preamble, data, CRC and then postamble
                byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                byte[] crcbytes;
                crcbytes = System.BitConverter.GetBytes(crc);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(crcbytes, 0, crcbytes.Length);
                }
                System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                try
                {
                    ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                    ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                    // update stats tracking table
                    CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                    CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "375", "Exception during transmit of forcing registration command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                    // remove this entry from socket table so we don't keep trying
                    ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                }

                mem.Dispose();
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "362", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for forcing registration.");
            }

            return 0;
        }

        //
        //
        // enable/disable FL option
        //	UNS_32	1 - to enable FL option, 0 - to disable FL option
        static int JOB_EnableFL_OptionCommand(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            AdsCommand cmd;
            AdsDataReader rdr;
            Int32? p1 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Enable/Disable FL option command for NID " + nid.ToString());
                ServerDB db = new ServerDB(_cts);

                AdsConnection conn = db.OpenDBConnectionNotAsync("");
                if (conn != null)
                {
                    do
                    {
                        try
                        {
                            cmd = new AdsCommand("SELECT Idx, Command_id, Network_id, User_id, Date_created, Status, Processing_start, Param1, Param2, Param3 "
                                + " from IHSFY_jobs where Idx = " + jobnumber.ToString() + " and Network_id = " + nid.ToString(), conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);

                            rdr = cmd.ExecuteReader();
                            if (!rdr.HasRows)
                            {
                                // no weather info to send.
                                _log.WriteMessage(NLog.LogLevel.Error, "185", "Could not find Enable/Disable FL option job in jobs table for nid: " + nid.ToString() + " with job number " + jobnumber.ToString());
                            }
                            else
                            {
                                rdr.Read();
                                if (rdr.IsDBNull(7))
                                {
                                    p1 = null;
                                }
                                else
                                {
                                    p1 = rdr.GetInt32(7);
                                }
                            }
                            rdr.Close();
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessage(NLog.LogLevel.Error, "179", "Database exception during Enable/Disable FL option Command checking: " + e.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    db.CloseDBConnection(conn, "");
                }

                if (p1 != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    
                    // enable/disable -FL option mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_enable_or_disable_FL_option, mem);

                    // 1 - to enable FL option, 0 - to disable FL option
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "Enable/Disable FL option Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of Enable/Disable FL option command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for Enable/Disable FL option command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending OFF command.");
            }

            return 0;
        }

        //
        //  MVOR 
        //  UNS_32  job number
        //	UNS_32	system_gid (4)
        //	UNS_32	mvor_action_to_take (4) :
        // 	        MVOR_ACTION_OPEN_MASTER_VALVE							(0)
        //          MVOR_ACTION_CLOSE_MASTER_VALVE							(1)
        //          MVOR_ACTION_CANCEL_MASTER_VALVE_OVERRIDE				(2)
        //  UNS_32	mvor_seconds (4)
        //
        static int JOB_MVOR_Command(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            Int32? p1 = null;
            Int32? p2 = null;
            Int32? p3 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "MVOR command for NID " + nid.ToString());

                Tuple<Int32?, Int32?, Int32?, Int32?> r = Misc.get_IHSFY_Job_Params(_log, _cts, nid, jobnumber);

                p1 = r.Item1;
                p2 = r.Item2;
                p3 = r.Item3;

                if (p1 != null && p2 != null & p3 != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // MVOR mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_mobile_MVOR, mem);
                    // job number which will be returned for us to match up ACK
                    Misc.Uint32ToNetworkBytes(jobnumber, mem);
                    // system_gid from param1
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);
                    // station mvor_action_to_take from param2 
                    Misc.Uint32ToNetworkBytes((uint)p2, mem);
                    // duration in seconds from param3
                    Misc.Uint32ToNetworkBytes((uint)p3, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "MVOR Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of MVOR command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for MVOR command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending MVOR command.");
            }

            return 0;
        }

        //
        //  Turn lights On/Off 
        //  UNS_32  job number
        //	UNS_32	laction_needed:
        //          ACTION_NEEDED_TURN_ON_mobile_light                      (3)            
        //          ACTION_NEEDED_TURN_OFF_AND_REMOVE_light_mobile_off      (8)
        //  UNS_32  box index
        //  UNS_32  light output index
        //	UNS_32	seconds
        //
        static int JOB_Light_Command(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            Int32? p1 = null;
            Int32? p2 = null;
            Int32? p3 = null;
            Int32? p4 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Lights command for NID " + nid.ToString());

                Tuple<Int32?, Int32?, Int32?, Int32?> r = Misc.get_IHSFY_Job_Params(_log, _cts, nid, jobnumber);

                p1 = r.Item1;
                p2 = r.Item2;
                p3 = r.Item3;
                p4 = r.Item4;

                if (p1 != null && p2 != null & p3 != null & p4 != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // ON mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_mobile_light_cmd, mem);
                    // job number which will be returned for us to match up ACK
                    Misc.Uint32ToNetworkBytes(jobnumber, mem);
                    // action needed from param1
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);
                    // box index from param2
                    Misc.Uint32ToNetworkBytes((uint)p2, mem);
                    // light output index from param3
                    Misc.Uint32ToNetworkBytes((uint)p3, mem);
                    // time in seconds from param4
                    Misc.Uint32ToNetworkBytes((uint)p4, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "Light Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of light command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for light command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending light command.");
            }

            return 0;
        }

        //
        //  Turn  Scheduled Irrigation On or Off
        //  UNS_32  job number
        //	UNS_32	lturn_off,  If lturn_off is TRUE, the controller/network is turned off.
        //
        static int JOB_ControllerOnOff_Command(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            Int32? p1 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Controller On/Off command for NID " + nid.ToString());

                Tuple<Int32?, Int32?, Int32?, Int32?> r = Misc.get_IHSFY_Job_Params(_log, _cts, nid, jobnumber);

                p1 = r.Item1;

                if (p1 != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_mobile_turn_controller_on_off, mem);                    
                    // job number which will be returned for us to match up ACK
                    Misc.Uint32ToNetworkBytes(jobnumber, mem);
                    // action needed from param1
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);
                    
                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "Scheduled Irrigation Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of scheduled irrigation On/Off command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for scheduled irrigation on/off command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending scheduled irrigation on/off command.");
            }

            return 0;
        }

        //
        //  Send no water days to all Stations
        //  UNS_32  job number
        //  UNS_32  number of days
        //
        static int JOB_SendNoDaysAllStations_Command(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            Int32? p1 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Send No Water Days to all Stations command for NID " + nid.ToString());

                Tuple<Int32?, Int32?, Int32?, Int32?> r = Misc.get_IHSFY_Job_Params(_log, _cts, nid, jobnumber);

                p1 = r.Item1;

                if (p1 != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_mobile_now_days_all_stations, mem);
                    // job number which will be returned for us to match up ACK
                    Misc.Uint32ToNetworkBytes(jobnumber, mem);
                    // number of days
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "Send No Water to all Stations Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of Send No Water to all Stations command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for sending no water to all Stations command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending no water to all Stations command.");
            }

            return 0;
        }

        //
        //  Send no water days by group
        //  UNS_32  job number
        //  UNS_32  number of days
        //  UNS_32  group gid
        //
        static int JOB_SendNoDaysByGroup_Command(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            Int32? p1 = null;
            Int32? p2 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Send No Water Days by Groups command for NID " + nid.ToString());

                Tuple<Int32?, Int32?, Int32?, Int32?> r = Misc.get_IHSFY_Job_Params(_log, _cts, nid, jobnumber);

                p1 = r.Item1;
                p2 = r.Item2;

                if (p1 != null && p2 != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_mobile_now_days_by_group, mem);
                    // job number which will be returned for us to match up ACK
                    Misc.Uint32ToNetworkBytes(jobnumber, mem);
                    // number of days
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);
                    // group gid
                    Misc.Uint32ToNetworkBytes((uint)p2, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "Send No Water by Group Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of Send No Water by group command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for sending no water by group command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending no water by group command.");
            }

            return 0;
        }

        //
        //  Send no water days by station
        //  UNS_32  job number
        //  UNS_32  number of days
        //  UNS_32  box index
        //  UNS_32  station number
        //
        static int JOB_SendNoDaysByStation_Command(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            Int32? p1 = null;
            Int32? p2 = null;
            Int32? p3 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Send No Water Days by Station command for NID " + nid.ToString());

                Tuple<Int32?, Int32?, Int32?, Int32?> r = Misc.get_IHSFY_Job_Params(_log, _cts, nid, jobnumber);

                p1 = r.Item1;
                p2 = r.Item2;
                p3 = r.Item3;

                if (p1 != null && p2 != null && p3 != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_mobile_now_days_by_station, mem);
                    // job number which will be returned for us to match up ACK
                    Misc.Uint32ToNetworkBytes(jobnumber, mem);
                    // number of days
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);
                    // box index
                    Misc.Uint32ToNetworkBytes((uint)p2, mem);
                    // station number
                    Misc.Uint32ToNetworkBytes((uint)p3, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "Send No Water by Station Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of Send No Water by station command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for sending no water by station command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending no water by station command.");
            }

            return 0;
        }

        //
        //  Send no water days by box
        //  UNS_32  job number
        //  UNS_32  number of days
        //  UNS_32  box index
        //
        static int JOB_SendNoDaysByBox_Command(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            Int32? p1 = null;
            Int32? p2 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Send No Water Days by Box command for NID " + nid.ToString());

                Tuple<Int32?, Int32?, Int32?, Int32?> r = Misc.get_IHSFY_Job_Params(_log, _cts, nid, jobnumber);

                p1 = r.Item1;
                p2 = r.Item2;

                if (p1 != null && p2 != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_mobile_now_days_by_box, mem);
                    // job number which will be returned for us to match up ACK
                    Misc.Uint32ToNetworkBytes(jobnumber, mem);
                    // number of days
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);
                    // box index
                    Misc.Uint32ToNetworkBytes((uint)p2, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "Send No Water by Box Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of Send No Water by box command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for sending no water by box command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending no water by box command.");
            }

            return 0;
        }

        //
        //  Clear mainline break
        //  UNS_32  job number
        //  UNS_32  system gid
        //
        static int JOB_ClearMLB_Command(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            Int32? p1 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Send No Water Days by Station command for NID " + nid.ToString());

                Tuple<Int32?, Int32?, Int32?, Int32?> r = Misc.get_IHSFY_Job_Params(_log, _cts, nid, jobnumber);

                p1 = r.Item1;

                if (p1 != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_clear_MLB, mem);
                    // job number which will be returned for us to match up ACK
                    Misc.Uint32ToNetworkBytes(jobnumber, mem);
                    // system gid
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);
                    
                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "Send Clear MLB Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of clear MLB command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for clearing MLB command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending clear MLB command.");
            }

            return 0;
        }

        //
        //  Stop all irrigation
        //  UNS_32  job number
        //
        static int JOB_StopAllIrrigation_Command(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Send Stop All Irrigation command for NID " + nid.ToString());

                var mem = new MemoryStream(0);
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                // network id
                Misc.Uint32ToNetworkBytes(nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_stop_all_irrigation, mem);
                // job number which will be returned for us to match up ACK
                Misc.Uint32ToNetworkBytes(jobnumber, mem);

                data = mem.ToArray();

                crc = Misc.CRC(data);
                //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                _log.WriteMessage(NLog.LogLevel.Debug, "178", "Send Stop All Irrigation Command to be transmitted to SN:" + ac.serial_number.ToString());

                // combine preamble, data, CRC and then postamble
                byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                byte[] crcbytes;
                crcbytes = System.BitConverter.GetBytes(crc);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(crcbytes, 0, crcbytes.Length);
                }
                System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                try
                {
                    ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                    ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                    // update stats tracking table
                    CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                    CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of stop all irrigation command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                    // remove this entry from socket table so we don't keep trying
                    ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                }

                mem.Dispose();
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending stop all irrigation command.");
            }

            return 0;
        }

        //
        //  Stop irrigation
        //  UNS_32  job number
        //  UNS_32  system gid
        //
        static int JOB_StopIrrigation_Command(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            Int32? p1 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Send Stop Irrigation command for NID " + nid.ToString());

                Tuple<Int32?, Int32?, Int32?, Int32?> r = Misc.get_IHSFY_Job_Params(_log, _cts, nid, jobnumber);

                p1 = r.Item1;

                if (p1 != null)
                {

                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);
                    // mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_stop_irrigation, mem);
                    // job number which will be returned for us to match up ACK
                    Misc.Uint32ToNetworkBytes(jobnumber, mem);
                    // system gid
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "Send Stop Irrigation Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of stop irrigation command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for stopping irrigation command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending stop irrigation command.");
            }

            return 0;
        }

        //
        //
        // enable/disable HUB option
        //	UNS_32	1 - to enable HUB option, 0 - to disable HUB option
        static int JOB_EnableHub_OptionCommand(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            AdsCommand cmd;
            AdsDataReader rdr;
            Int32? p1 = null;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "182", "Enable/Disable HUB option command for NID " + nid.ToString());
                ServerDB db = new ServerDB(_cts);

                AdsConnection conn = db.OpenDBConnectionNotAsync("");
                if (conn != null)
                {
                    do
                    {
                        try
                        {
                            cmd = new AdsCommand("SELECT Idx, Command_id, Network_id, User_id, Date_created, Status, Processing_start, Param1, Param2, Param3 "
                                + " from IHSFY_jobs where Idx = " + jobnumber.ToString() + " and Network_id = " + nid.ToString(), conn);
                            //_log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);

                            rdr = cmd.ExecuteReader();
                            if (!rdr.HasRows)
                            {
                                // no weather info to send.
                                _log.WriteMessage(NLog.LogLevel.Error, "185", "Could not find Enable/Disable HUB option job in jobs table for nid: " + nid.ToString() + " with job number " + jobnumber.ToString());
                            }
                            else
                            {
                                rdr.Read();
                                if (rdr.IsDBNull(7))
                                {
                                    p1 = null;
                                }
                                else
                                {
                                    p1 = rdr.GetInt32(7);
                                }
                            }
                            rdr.Close();
                        }
                        catch (Exception e)
                        {
                            _log.WriteMessage(NLog.LogLevel.Error, "179", "Database exception during Enable/Disable HUB option Command checking: " + e.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    db.CloseDBConnection(conn, "");
                }

                if (p1 != null)
                {
                    var mem = new MemoryStream(0);
                    // PID byte
                    mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                    // message class byte
                    mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                    // to serial number
                    Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                    // network id
                    Misc.Uint32ToNetworkBytes(nid, mem);

                    // enable/disable -HUB option mid
                    Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_enable_or_disable_HUB_option, mem);

                    // 1 - to enable HUB option, 0 - to disable HUB option
                    Misc.Uint32ToNetworkBytes((uint)p1, mem);

                    data = mem.ToArray();

                    crc = Misc.CRC(data);
                    //_log.WriteMessage(NLog.LogLevel.Debug, "170", "Calculated CRC: 0x" + crc.ToString("X"));
                    _log.WriteMessage(NLog.LogLevel.Debug, "178", "Enable/Disable HUB option Command to be transmitted to SN:" + ac.serial_number.ToString());

                    // combine preamble, data, CRC and then postamble
                    byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                    System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                    System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                    byte[] crcbytes;
                    crcbytes = System.BitConverter.GetBytes(crc);
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(crcbytes, 0, crcbytes.Length);
                    }
                    System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                    System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                    try
                    {
                        ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                        ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                        // update stats tracking table
                        CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                        CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    }
                    catch (Exception e)
                    {
                        _log.WriteMessage(NLog.LogLevel.Error, "175", "Exception during transmit of Enable/Disable HUB option command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                        // remove this entry from socket table so we don't keep trying
                        ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                    }

                    mem.Dispose();
                }
                else
                {
                    _log.WriteMessage(NLog.LogLevel.Warn, "162", "Invalid parameters for Enable/Disable HUB option command NID: " + nid.ToString() + " job number: " + jobnumber.ToString());
                }
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "162", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending Enable/Disable HUB command.");
            }

            return 0;
        }

        //
        // Force to check for update
        //
        static int JOB_CheckForUpdate_Command(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "382", "Force to check for update request for NID " + nid.ToString());

                var mem = new MemoryStream(0);
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                // network id
                Misc.Uint32ToNetworkBytes(nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_check_for_updates, mem);

                data = mem.ToArray();

                crc = Misc.CRC(data);
                //_log.WriteMessage(NLog.LogLevel.Debug, "370", "Calculated CRC: 0x" + crc.ToString("X"));
                _log.WriteMessage(NLog.LogLevel.Debug, "378", "Force to check for update request to be transmitted to SN:" + ac.serial_number.ToString());

                // combine preamble, data, CRC and then postamble
                byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                byte[] crcbytes;
                crcbytes = System.BitConverter.GetBytes(crc);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(crcbytes, 0, crcbytes.Length);
                }
                System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                try
                {
                    ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                    ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                    // update stats tracking table
                    CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                    CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "375", "Exception during transmit of forcing to check for update command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                    // remove this entry from socket table so we don't keep trying
                    ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                }

                mem.Dispose();
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "362", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for forcing to check for update.");
            }

            return 0;
        }

        static int JOB_noaction(uint nid, uint jobnumber)
        {
            return 0;
        }

        public static int JOB_hubupdate(uint nid, uint jobnumber)
        {
            int i;

            // tell hub handler about a change in controllers
            i = HubConnectionTracker.ProcessHubChange(nid);
            
            // based on what hub handler returns, complete job with status
            if (i == 0) CompleteJob((int)jobnumber, Constants.JOB_SUCCESSFUL, null);
            else CompleteJob((int)jobnumber, Constants.JOB_INVALID_RESPONSE, null);

            return 0;
        }

        //
        // tell controller someone wants a status screen
        //
        static int JOB_DemandPoll(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;
            ActiveLegacyConnection hubac;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "382", "Demand Poll request for NID " + nid.ToString());

                var mem = new MemoryStream(0);
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                // network id
                Misc.Uint32ToNetworkBytes(nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_go_ahead_send_your_messages, mem);

                data = mem.ToArray();

                crc = Misc.CRC(data);
                //_log.WriteMessage(NLog.LogLevel.Debug, "370", "Calculated CRC: 0x" + crc.ToString("X"));
                _log.WriteMessage(NLog.LogLevel.Debug, "378", "Demand Poll to be transmitted to SN:" + ac.serial_number.ToString());

                // combine preamble, data, CRC and then postamble
                byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                byte[] crcbytes;
                crcbytes = System.BitConverter.GetBytes(crc);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(crcbytes, 0, crcbytes.Length);
                }
                System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                try
                {
                    ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                    ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                    // update stats tracking table
                    CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                    CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                    // save the job id 
                    hubac = HubConnectionTracker.FindACfrom3000NID(nid);
                    if(hubac != null)
                    {
                        if (hubac.controllers3000.Exists(x => x.NID == nid))
                        {
                            hubac.controllers3000.Find(x => x.NID == nid).demandPollJobId = jobnumber;
                        }
                    }
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "375", "Exception during transmit of Demand Poll to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                    // remove this entry from socket table so we don't keep trying
                    ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                }

                mem.Dispose();
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "362", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for sending demand poll.");
            }

            return 0;
        }

        //
        // Send engineering alerts
        //
        static int JOB_SendEngineeringAlerts(uint nid, uint jobnumber)
        {
            ActiveController ac;
            byte[] data;
            UInt32 crc;

            // see if this controller has current connection...and if so get info from DB...
            if ((ac = ServerSocketTracker.GetTrackingEntry(nid)) != null)
            {
                _log.WriteMessage(NLog.LogLevel.Info, "382", "Force to send engineering alerts for NID " + nid.ToString());

                var mem = new MemoryStream(0);
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(ac.serial_number, mem);
                // network id
                Misc.Uint32ToNetworkBytes(nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_send_engineering_alerts, mem);

                data = mem.ToArray();

                crc = Misc.CRC(data);
                //_log.WriteMessage(NLog.LogLevel.Debug, "370", "Calculated CRC: 0x" + crc.ToString("X"));
                _log.WriteMessage(NLog.LogLevel.Debug, "378", "Force to send engineering alerts command to be transmitted to SN:" + ac.serial_number.ToString());

                // combine preamble, data, CRC and then postamble
                byte[] finalpacket = new byte[ServerApp.Constants.PREAMBLE.Length + data.Length + ServerApp.Constants.POSTAMBLE.Length + sizeof(UInt32)];
                System.Buffer.BlockCopy(ServerApp.Constants.PREAMBLE, 0, finalpacket, 0, ServerApp.Constants.PREAMBLE.Length);
                System.Buffer.BlockCopy(data, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length, data.Length);

                byte[] crcbytes;
                crcbytes = System.BitConverter.GetBytes(crc);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(crcbytes, 0, crcbytes.Length);
                }
                System.Buffer.BlockCopy(crcbytes, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length, crcbytes.Length);
                System.Buffer.BlockCopy(ServerApp.Constants.POSTAMBLE, 0, finalpacket, ServerApp.Constants.PREAMBLE.Length + data.Length + sizeof(UInt32), ServerApp.Constants.POSTAMBLE.Length);

                try
                {
                    ac.net_stream.WriteAsync(finalpacket, 0, (int)finalpacket.Length, _cts.Token).ConfigureAwait(false);
                    ac.net_stream.FlushAsync(_cts.Token).ConfigureAwait(false);

                    // update stats tracking table
                    CalsenseControllers.BytesSent((int)ac.serial_number, (int)finalpacket.Length);
                    CalsenseControllers.MsgsSent((int)ac.serial_number, 1);
                }
                catch (Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "375", "Exception during transmit of forcing to send engineering alerts command to controller SN:" + ac.serial_number.ToString() + ", " + e.ToString());
                    // remove this entry from socket table so we don't keep trying
                    ServerSocketTracker.RemoveTrackingEntry(nid, ac.net_stream);
                }

                mem.Dispose();
            }
            else
            {
                _log.WriteMessage(NLog.LogLevel.Warn, "362", "Couldn't find expected network ID " + nid.ToString() + " in Tracking Table for forcing to check for update.");
            }

            return 0;
        }

        //
        // Blast Ack routine - called from ServerInternal when an ACK for periodic task has arrived from controller
        //
        public static async Task<Tuple<List<byte[]>, String, bool, int>> BlastAck(ClientInfoStruct client, CancellationTokenSource cts)
        {
            uint size;
            byte[] Buffer = null;
            AdsCommand cmd;
            AdsDataReader rdr;
            int command_mid;
            int found_job_number = 0;
            int offset;
            uint jobnumber = 0;
            int userId = -1;

            _log.WriteMessage(NLog.LogLevel.Info, "222", "Processing ACK MID: " + client.mid.ToString());

            size = client.rxlen;
            Buffer = client.rx;

            offset = 0;


            // determine which command MID generated this ACK
            // (some ACKs contain a job number we can use to
            // identify the exact command that was sent out
            // for matching up with.)
            switch(client.mid)
            {
                case Constants.MID_TO_COMMSERVER_WEATHER_DATA_receipt_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_WEATHER_DATA_packet;
                    break;
                case Constants.MID_TO_COMMSERVER_RAIN_SHUTDOWN_receipt_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_RAIN_SHUTDOWN_packet;
                    break;
                case Constants.MID_TO_COMMSERVER_send_a_status_screen_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_send_a_status_screen;
                    break;
                case Constants.MID_TO_COMMSERVER_mobile_station_ON_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_mobile_station_ON;
                    jobnumber = Misc.NetworkBytesToUint32(Buffer, offset);
                    break;
                case Constants.MID_TO_COMMSERVER_mobile_station_OFF_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_mobile_station_OFF;
                    jobnumber = Misc.NetworkBytesToUint32(Buffer, offset);
                    break;
                case Constants.MID_TO_COMMSERVER_panel_swap_factory_reset_to_new_panel_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_panel_swap_factory_reset_to_new_panel;
                    break;
                case Constants.MID_TO_COMMSERVER_panel_swap_factory_reset_to_old_panel_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_panel_swap_factory_reset_to_old_panel;
                    break;
                case Constants.MID_TO_COMMSERVER_set_all_bits_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_set_all_bits;
                    break;
                case Constants.MID_TO_COMMSERVER_enable_or_disable_FL_option_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_enable_or_disable_FL_option;
                    break;
                case Constants.MID_TO_COMMSERVER_mobile_MVOR_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_mobile_MVOR;
                    jobnumber = Misc.NetworkBytesToUint32(Buffer, offset);
                    break;
                case Constants.MID_TO_COMMSERVER_mobile_light_cmd_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_mobile_light_cmd;
                    jobnumber = Misc.NetworkBytesToUint32(Buffer, offset);
                    break;
                case Constants.MID_TO_COMMSERVER_mobile_turn_controller_on_off_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_mobile_turn_controller_on_off;
                    jobnumber = Misc.NetworkBytesToUint32(Buffer, offset);
                    // update db after we've received the ACK
                    _log.WriteMessage(NLog.LogLevel.Info, "223", "Updating program data after controller ACK");
                    ServerDB.Execute(_log, cts, "EXECUTE PROCEDURE UpdateCS3000PgmForQuickTask (" + jobnumber + "," +
                        Constants.MID_FROM_COMMSERVER_mobile_turn_controller_on_off + "," + client.nid + ")");
                    break;
                case Constants.MID_TO_COMMSERVER_mobile_now_days_all_stations_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_mobile_now_days_all_stations;
                    jobnumber = Misc.NetworkBytesToUint32(Buffer, offset);
                    // update db after we've received the ACK
                    _log.WriteMessage(NLog.LogLevel.Info, "223", "Updating program data after controller ACK");
                    ServerDB.Execute(_log, cts, "EXECUTE PROCEDURE UpdateCS3000PgmForQuickTask (" + jobnumber + "," +
                        Constants.MID_FROM_COMMSERVER_mobile_now_days_all_stations + "," + client.nid + ")");
                    break;
                case Constants.MID_TO_COMMSERVER_mobile_now_days_by_group_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_mobile_now_days_by_group;
                    jobnumber = Misc.NetworkBytesToUint32(Buffer, offset);
                    // update db after we've received the ACK
                    _log.WriteMessage(NLog.LogLevel.Info, "223", "Updating program data after controller ACK");
                    ServerDB.Execute(_log, cts, "EXECUTE PROCEDURE UpdateCS3000PgmForQuickTask (" + jobnumber + "," +
                        Constants.MID_FROM_COMMSERVER_mobile_now_days_by_group + "," + client.nid + ")");
                    break;
                case Constants.MID_TO_COMMSERVER_mobile_now_days_by_box_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_mobile_now_days_by_box;
                    jobnumber = Misc.NetworkBytesToUint32(Buffer, offset);
                    // update db after we've received the ACK
                    _log.WriteMessage(NLog.LogLevel.Info, "223", "Updating program data after controller ACK");
                    ServerDB.Execute(_log, cts, "EXECUTE PROCEDURE UpdateCS3000PgmForQuickTask (" + jobnumber + "," +
                        Constants.MID_FROM_COMMSERVER_mobile_now_days_by_box + "," + client.nid + ")");
                    break;
                case Constants.MID_TO_COMMSERVER_mobile_now_days_by_station_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_mobile_now_days_by_station;
                    jobnumber = Misc.NetworkBytesToUint32(Buffer, offset);
                    // update db after we've received the ACK
                    _log.WriteMessage(NLog.LogLevel.Info, "223", "Updating program data after controller ACK");
                    ServerDB.Execute(_log, cts, "EXECUTE PROCEDURE UpdateCS3000PgmForQuickTask (" + jobnumber + "," +
                        Constants.MID_FROM_COMMSERVER_mobile_now_days_by_station + "," + client.nid + ")");
                    break;
                case Constants.MID_TO_COMMSERVER_clear_MLB_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_clear_MLB;
                    jobnumber = Misc.NetworkBytesToUint32(Buffer, offset);
                    break;
                case Constants.MID_TO_COMMSERVER_stop_all_irrigation_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_stop_all_irrigation;
                    jobnumber = Misc.NetworkBytesToUint32(Buffer, offset);
                    break;
                case Constants.MID_TO_COMMSERVER_stop_irrigation_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_stop_irrigation;
                    jobnumber = Misc.NetworkBytesToUint32(Buffer, offset);
                    break;
                case Constants.MID_TO_COMMSERVER_check_for_updates_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_check_for_updates;
                    break;
                case Constants.MID_TO_COMMSERVER_enable_or_disable_HUB_option_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_enable_or_disable_HUB_option;
                    break;
                case Constants.MID_TO_COMMSERVER_force_registration_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_force_registration;
                    break;
                case Constants.MID_TO_COMMSERVER_send_engineering_alerts_ack:
                    command_mid = Constants.MID_FROM_COMMSERVER_send_engineering_alerts;
                    break;
                default:
                    command_mid = 0;
                    break;
            }

            // database connection
            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(Misc.ClientLogString(client));
            AdsConnection conn = await connTask;
            if (conn != null)
            {

                // find specific job this ACK is for
                try
                {
                    if (jobnumber != 0)
                    {
                        cmd = new AdsCommand("SELECT TOP 1 Idx, Command_id, Network_id, User_id, Date_created, Status,  Processing_start, Param1, Param2, Param3, Param5 from  IHSFY_jobs "
                            + " WHERE Network_id = " + client.nid.ToString()
                            + " AND Idx = " + jobnumber.ToString()
                            + " AND Command_id = " + command_mid.ToString()
                            + " AND Processing_start IS NOT NULL ORDER BY Processing_start desc "
                            , conn);
                    }
                    else
                    {
                        cmd = new AdsCommand("SELECT TOP 1 Idx, Command_id, Network_id, User_id, Date_created, Status,  Processing_start, Param1, Param2, Param3, Param5 from  IHSFY_jobs "
                            + " WHERE Network_id = " + client.nid.ToString()
                            + "  AND Command_id = " + command_mid.ToString()
                            + " AND Processing_start IS NOT NULL ORDER BY Processing_start desc "
                            , conn);
                    }
                    _log.WriteMessage(NLog.LogLevel.Debug, "154", "SQL: " + cmd.CommandText);

                    rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        rdr.Read();
                        // we found the JOB this ACK is for, generate notification table row 
                        // and then delete the job
                        found_job_number = rdr.GetInt32(0);
                        userId = rdr.GetInt32(3);
                    }
                    else
                    {
                        _log.WriteMessageWithId(NLog.LogLevel.Error, "227", "NO Outstanding Job found for ACK of blast message, MID " + client.mid.ToString(), client.nid.ToString());
                    }
                    rdr.Close();
                    cmd.Unprepare();
                }
                catch(Exception e)
                {
                    _log.WriteMessage(NLog.LogLevel.Error, "223", "Exception during database processing during ACK of blast message, MID " + client.mid.ToString() + " Error: " + e.ToString());
                }

                db.CloseDBConnection(conn, Misc.ClientLogString(client));
            }

            // if we found the job, mark it complete.
            if (found_job_number != 0)
            {
                await CompleteJob(found_job_number, Constants.JOB_SUCCESSFUL, null);

                // user start mobile session, add alert line
                if (command_mid == Constants.MID_FROM_COMMSERVER_send_a_status_screen)
                {
                    String username = await Misc.getUsername(_log, _cts, userId);

                    await Misc.AddAlert(_cts, client.nid, username + " connected using Mobile", true, true);
                }
            }

            // no response and leave connection up
            return (Tuple.Create((List<byte[]>)null, (String)null, false, Constants.NO_STATE_DATA));
        }
    }
}
