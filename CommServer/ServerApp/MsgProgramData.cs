﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using Advantage.Data.Provider;



namespace Calsense.CommServer
{

    public class MsgProgramData
    {
        // common C code that builds SQL statements for given program data buffer, returns
        // number of bytes parsed in message buffer.
        // sqlBuffer - 1MB
        // SQL_search_condition_str - 4 KB
        // SQL_update_str - 4 KB
        // SQL_insert_fields_str - 4 KB
        // SQL_insert_values_str - 4 KB
        // SQL_single_merge_statement_buf - 8 KB
        [DllImport("ParserDLL.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern uint PDATA_extract_and_store_changes(byte[] receiveddata,
            uint reason_for_change,
            uint pset_change_bits,
            uint pchanges_received_from,
            [In, Out] byte[] sqlBuffer,
            [In] byte[] pSQL_search_condition_str,
            [In] byte[] pSQL_update_str,
            [In] byte[] pSQL_insert_fields_str,
            [In] byte[] pSQL_insert_values_str,
            [In] byte[] pSQL_single_merge_statement_buf,
            uint network_ID);

        // wrapper function for above DLL call with attribute to allow exception catching
        // if it throws access exception (not normally catchable in C#)
        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptionsAttribute]
        [System.Security.SecurityCriticalAttribute]
        static uint pdata_wrapper(MyLogger _logger, byte[] receiveddata, uint reason_for_change,
            uint pset_change_bits,
            uint pchanges_received_from,
            byte[] sqlBuffer,
            uint network_ID)
        {
            uint i = 0;
            try
            {
                // allocate the buffers that the parsing function uses to build SQL statements internally
                byte[] a1 = new byte[4 * 1024];
                byte[] a2 = new byte[4 * 1024];
                byte[] a3 = new byte[4 * 1024];
                byte[] a4 = new byte[4 * 1024];
                byte[] a5 = new byte[8 * 1024];
                i = PDATA_extract_and_store_changes(receiveddata, reason_for_change, pset_change_bits, pchanges_received_from, sqlBuffer, a1, a2, a3, a4, a5, network_ID);
            }
            catch(Exception e)
            {
                i = 0;
                _logger.WriteMessage(NLog.LogLevel.Error, "14444", "Program Data parse exception in PDATA_extract_and_store_changes: " + e.ToString());
            }
            return (i);
        }

        //
        // Parses Program Data inbond message from controller
        // returns via Tuple:
        //   List of byte arrays, which are 0 to n messages to send as a response
        //   String that will be passed back in if this connection stays up
        //   Boolean as a flag to terminate connection or not
        //   int as an internal state to track thru session (if needed)
        //
        public static async Task<Tuple<List<byte[]>,String, bool, int>> Parse(ClientInfoStruct client, CancellationTokenSource cts)
        {
            byte[] data;
            MyLogger _logger;
            uint size;
            byte[] Buffer = null;

            byte[] sqlBuffer = new byte[Constants.SQL_BUFFER_SIZE];     
            AdsCommand cmd;
            bool abort = true;
            int offset;
            String sqlcommands = "";

            _logger = new MyLogger(typeof(MsgProgramData).Name, Misc.ClientLogString(client), Misc.ClientLogDB(client), Constants.CS3000Model);
            _logger.WriteMessage(NLog.LogLevel.Info, "14304", "Processing Program Data message...");

            size = client.rxlen;
            Buffer = client.rx;

            offset = 0;

            // call C language parsing routine to get SQL statements necessary for this message
            //sqlcommands = cfunction(Buffer);
            /*
               - ucp is an unsigned char pointer to the data you received
             * 
                - CHANGE_REASON_SYNC_RECEIVING_AT_COMM_SERVER is defined in 
             *    change.h as (20) [not be checked in yet, so you can force 20 for now]
               - pSQL_statement_ptr is a pointer to a string where the string of 
             * SQL statements will be stored by our code. common_includes\sql_merge.h 
             * has a define I’ve created which defines the max size. I’ve used 65,536 
             * chars for now, but we can change it to whatever size you want. We’ll 
             * populate this will all of the SQL statements, semi-colon delimited, 
             * as discussed, which you can execute when the function completes.
             * 
                - pnetwork_ID is the network ID from which the message originated, 
             * as indicated in the header.
             */
            _logger.WriteMessage(NLog.LogLevel.Info, "14327", "Preparing to parse program data...");
            offset = (int) pdata_wrapper( _logger, Buffer, 
                Constants.CHANGE_REASON_SYNC_RECEIVING_AT_COMM_SERVER, 
                0, 
                Constants.CHANGE_REASON_SYNC_RECEIVING_AT_COMM_SERVER,
                sqlBuffer, 
                client.nid);
            // make sure something got looked at
            if(offset == 0)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "14901", "Not acking, problem with parsing of program data.");

                _logger.WriteMessage(NLog.LogLevel.Info, "14333", "Build Program Data Nak response.");

                return (Tuple.Create(Misc.buildNakResponse(client.nid, client.sn, client.mid), (String)null, false, Constants.NO_STATE_DATA));
            }
            sqlcommands = System.Text.Encoding.Default.GetString(sqlBuffer).TrimEnd('\0');  // remove extra nulls
            _logger.WriteMessage(NLog.LogLevel.Info, "14328", "Parse routine looked at " + offset.ToString() + " bytes.");
            // get rid of mysterious non-ascii characters
            if (Regex.Matches(sqlcommands, @"[^\u0000-\u007F]").Count > 0)
            {
                _logger.WriteMessage(NLog.LogLevel.Warn, "14329", "FYI: Replacing non-ASCII characters in SQL with question marks. Probably indicates an error.");
                sqlcommands = Regex.Replace(sqlcommands, @"[^\u0000-\u007F]", "?");
            }
            _logger.WriteMessage(NLog.LogLevel.Debug, "14329", "Resulting SQL is " + sqlcommands.ToString());
#if DUMPRAWBYTES
            _logger.WriteMessage(NLog.LogLevel.Info, "3004", "Dump of bytes raw:" + size.ToString());
            for (i = 0, j = (int)size; i < size; i += 8)
            {
                if (j >= 8)
                {
                    s = Buffer[i].ToString("x2") + " " + Buffer[i + 1].ToString("x2") + " " + Buffer[i + 2].ToString("x2") + " " + Buffer[i + 3].ToString("x2") + " " + Buffer[i + 4].ToString("x2") + " " + Buffer[i + 5].ToString("x2") + " " + Buffer[i + 6].ToString("x2") + " " + Buffer[i + 7].ToString("x2");
                    j -= 8;
                }
                else
                {
                    s = "";
                    while (j > 0)
                    {
                        s = s + Buffer[i].ToString("x2") + " ";
                        j--;
                        i++;
                    }
                }
                _logger.WriteMessage(NLog.LogLevel.Debug, "8001", s);
                if (j <= 0) break;
                Thread.Sleep(10);       // allow UDP logger to catch up
            }
#endif
            // record in database
            ServerDB db = new ServerDB(cts);

            Task<AdsConnection> connTask = db.OpenDBConnection(_logger);
            AdsConnection conn = await connTask;
            if (conn != null)
            {
                    _logger.WriteMessage(NLog.LogLevel.Info, "14320", "Inserting Program Data into DB...");
                    // NOTE: Transactions not supported on local server, so ignore that exception
                    // insert data into DB and retrieve new network id to assign to controller
                    AdsTransaction txn = null;

                    try
                    {
                        txn = conn.BeginTransaction();
                    }
                    catch (System.NotSupportedException)
                    {

                    }
                    do
                    {
                        try
                        {
                            cmd = new AdsCommand(sqlcommands, conn, txn);
                            _logger.WriteMessage(NLog.LogLevel.Debug, "14353", "SQL: " + cmd.CommandText);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();


                            // Execute CompleteCS3000ProgramData:
                            // 1- Update NotifyNewDataArrives field in Temp_CS3000Networks to 2 for the given network id 
                            //    to notify all the users in temp tables that a new data has arrived
                            // 2- Update Box Name(s) from CS3000Network into CS3000Controllers and Controllers tables
                            _logger.WriteMessage(NLog.LogLevel.Info, "14354", "Updating BoxName(s) in CS3000Controllers and Controllers tables.");
                            cmd = new AdsCommand("EXECUTE PROCEDURE CompleteCS3000ProgramData(" + client.nid.ToString() + ");", conn, txn);
                            _logger.WriteMessage(NLog.LogLevel.Debug, "14356", "SQL: " + cmd.CommandText);
                            cmd.ExecuteNonQuery();
                            cmd.Unprepare();
                            // if we made it to here, we are good to commit all our changes
                            abort = false;
                        }
                        catch (Exception e)
                        {
                            _logger.WriteMessage(NLog.LogLevel.Error, "14391", "Database exception during Program Data insert(s): " + e.ToString());
                        }
                    } while (false);    // allows us to break out early if SQL problem

                    if (abort == true)
                    {
                        _logger.WriteMessage(NLog.LogLevel.Error, "14391", "Database Error on Program Data inserts.");
                        txn.Rollback();
                    }
                    else
                    {
                        txn.Commit();
                    }
                    db.CloseDBConnection(conn, _logger);
            }

            // if nothing saved to database, do not ACK
            if (abort == true)
            {
                _logger.WriteMessage(NLog.LogLevel.Error, "14901", "Not acking, problem with DB update.");

                _logger.WriteMessage(NLog.LogLevel.Info, "14333", "Build Program Data Nak response.");

                return (Tuple.Create(Misc.buildNakResponse(client.nid, client.sn, client.mid), (String)null, false, Constants.NO_STATE_DATA));
            }

            var mem = new MemoryStream(0);
            // build ack response
            if (client.nid != 0)
            {
                // PID byte
                mem.WriteByte(Constants.MSG_CLASS_2000e_CS3000_FLAG << 1);
                // message class byte
                mem.WriteByte(Constants.MSG_CLASS_CS3000_FROM_COMMSERVER);
                // to serial number
                Misc.Uint32ToNetworkBytes(client.sn, mem);
                // network id
                Misc.Uint32ToNetworkBytes(client.nid, mem);
                // mid
                Misc.Uint16ToNetworkBytes((UInt16)Constants.MID_FROM_COMMSERVER_PROGRAM_DATA_full_receipt_ack, mem);
                _logger.WriteMessage(NLog.LogLevel.Info, "14371", "Build Program Data Ack response.");
            }

            // get the built response to return and be sent
            data = mem.ToArray();
            List<byte[]> rsplist = new List<byte[]>();
            rsplist.Add(data);
            return (Tuple.Create(rsplist, (String)null, false, Constants.NO_STATE_DATA));
        }
    }
}
