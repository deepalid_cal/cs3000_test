﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using System.Threading;
using System.IO;



//
// our representation of a Controller, as reported via registration message
//
namespace Calsense.CommServer
{

    class I2C_Card_Details
    {
        public Boolean card_present {get; set;}
        public Boolean tb_present {get; set;}
        public UInt32 idx { get; set; }

        public void Initialize(UInt32 i, UInt32 present, UInt32 tb)
        {
            idx = i;
            if(present != 0)
            {
                card_present = true;
                tb_present = (tb != 0) ? true : false;
            }
            else
            {
                card_present = false;
                tb_present = false;
            }
        }
        public I2C_Card_Details(UInt32 i, UInt32 present, UInt32 tb)
        {
            Initialize(i, present, tb);
        }

        public I2C_Card_Details Clone()
        {
            I2C_Card_Details clone = new I2C_Card_Details(0, 0, 0);

            clone.card_present = card_present;
            clone.tb_present = tb_present;
            clone.idx = idx;

            return (clone);
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            I2C_Card_Details d = obj as I2C_Card_Details;
            if ((System.Object)d == null)
            {
                return false;
            }

            return Equals(obj as I2C_Card_Details);
        }

        public bool Equals(I2C_Card_Details d)
        {
            // If parameter is null return false:
            if ((object)d == null)
            {
                return false;
            }

            // Return true if the fields match:
            if(card_present != d.card_present) return(false);
            if(tb_present != d.tb_present) return(false);

            return (true);
        }

        //
        // generate a unique hash for given controller object
        // by hashing a string containing all its relevant items
        //
        public override int GetHashCode()
        {
            String s = "";

            // string for details...
            s = s + card_present.ToString() + tb_present.ToString();

            return (s.GetHashCode());
        }

        public static bool operator ==(I2C_Card_Details lhs, I2C_Card_Details rhs)
        {
            // Check for null on left side. 
            if (Object.ReferenceEquals(lhs, null))
            {
                if (Object.ReferenceEquals(rhs, null))
                {
                    // null == null = true. 
                    return true;
                }

                // Only the left side is null. 
                return false;
            }
            // Equals handles case of null on right side. 
            return lhs.Equals(rhs);
        }

        public static bool operator !=(I2C_Card_Details lhs, I2C_Card_Details rhs)
        {
            return !(lhs == rhs);
        }
    }
    class ControllerDetails
    {
        public Boolean saw_during_scan { get; set; }
        public UInt32 serial_number { get; set; }
        public Boolean option_FL { get; set; }
        public Boolean option_SSE { get; set; }
        public Boolean option_SSE_D { get; set; }
        public Boolean option_HUB { get; set; }
        public IList<I2C_Card_Details> i2c_cards;
        public I2C_Card_Details poc;
        public I2C_Card_Details lights;
        public Boolean weather_card_present { get; set; }
        public Boolean weather_terminal_present { get; set; }
        public Boolean dash_m_card_present { get; set; }
        public Boolean dash_m_terminal_present { get; set; }
        public Boolean dash_m_card_type { get; set; }
        public Boolean two_wire_terminal_present { get; set; }

        public UInt32 port_A_device_index { get; set; }
        public UInt32 port_B_device_index { get; set; }

        public UInt32 box_index { get; set; }   // 0 relative index of this data as reported in registration msg

        public ControllerDetails()
        {
            i2c_cards = new List<I2C_Card_Details>();
            poc = new I2C_Card_Details(0, 0, 0);
            lights = new I2C_Card_Details(0, 0, 0);
        }

        public ControllerDetails Clone()
        {
            ControllerDetails clone = new ControllerDetails();

            clone.serial_number = serial_number;
            clone.option_FL = option_FL;
            clone.option_SSE = option_SSE;
            clone.option_SSE_D = option_SSE_D;
            clone.weather_card_present = weather_card_present;
            clone.weather_terminal_present = weather_terminal_present;
            clone.dash_m_card_present = dash_m_card_present;
            clone.dash_m_terminal_present = dash_m_terminal_present;
            clone.dash_m_card_type = dash_m_card_type;
            clone.two_wire_terminal_present = two_wire_terminal_present;
            clone.port_A_device_index = port_A_device_index;
            clone.port_B_device_index = port_B_device_index;
            clone.box_index = box_index;
            foreach(I2C_Card_Details item in i2c_cards)
            {
                clone.i2c_cards.Add(item.Clone());
            }
            clone.poc = poc.Clone();
            clone.lights = lights.Clone();
            return (clone);
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            ControllerDetails d = obj as ControllerDetails;
            if ((System.Object)d == null)
            {
                return false;
            }

            return Equals(obj as ControllerDetails);
        }

        public bool Equals(ControllerDetails d)
        {
            // If parameter is null return false:
            if ((object)d == null)
            {
                return false;
            }

            // Return true if the fields match:
            if (serial_number != d.serial_number) return (false);
            if (option_FL != d.option_FL) return (false);
            if (option_SSE != d.option_SSE) return (false);
            if (option_SSE_D != d.option_SSE_D) return (false);
            if (!i2c_cards.SequenceEqual(d.i2c_cards)) return (false);
            if (poc != d.poc) return (false);
            if (lights != d.lights) return (false);
            if (weather_card_present != d.weather_card_present) return (false);
            if (weather_terminal_present != d.weather_terminal_present) return (false);
            if (dash_m_card_present != d.dash_m_card_present) return (false);
            if (dash_m_terminal_present != d.dash_m_terminal_present) return (false);
            if (dash_m_card_type != d.dash_m_card_type) return (false);
            if (two_wire_terminal_present != d.two_wire_terminal_present) return (false);
            if (port_A_device_index != d.port_A_device_index) return (false);
            if (port_B_device_index != d.port_B_device_index) return (false);
            if (box_index != d.box_index) return (false);

            return (true);
        }

        //
        // generate a unique hash for given controller object
        // by hashing a string containing all its relevant items
        //
        public override int GetHashCode()
        {
            int i;
            String s = "";

            // string for details...
            s = s + serial_number.ToString() + option_FL.ToString();
            s = s + option_SSE.ToString() + option_SSE_D.ToString();
            s = s + weather_card_present.ToString() + weather_terminal_present.ToString();
            s = s + dash_m_card_present.ToString() + dash_m_terminal_present.ToString();
            s = s + dash_m_card_type.ToString() + two_wire_terminal_present.ToString();
            s = s + port_A_device_index.ToString() + port_B_device_index.ToString();
            s = s + box_index.ToString();
            for (i = 0; i < i2c_cards.Count; i++ )
            {
                s = s + i2c_cards[i].GetHashCode();
            }
            s = s + poc.GetHashCode();
            s = s + lights.GetHashCode();
            return (s.GetHashCode());
        }

        public static bool operator ==(ControllerDetails lhs, ControllerDetails rhs)
        {
            // Check for null on left side. 
            if (Object.ReferenceEquals(lhs, null))
            {
                if (Object.ReferenceEquals(rhs, null))
                {
                    // null == null = true. 
                    return true;
                }

                // Only the left side is null. 
                return false;
            }
            // Equals handles case of null on right side. 
            return lhs.Equals(rhs);
        }

        public static bool operator !=(ControllerDetails lhs, ControllerDetails rhs)
        {
            return !(lhs == rhs);
        }
    }
    class Controller
    {
        public UInt16 structure_version { get; set; }
        public UInt32 time { get; set; }
        public UInt16 date { get; set; }
        public String code_revision_string { get; set; }
        public String TP_code_revision_string { get; set; }
        public IList<ControllerDetails> details;

        // 9/4/2015 rmd : Indication to comm server to clear the pdata associated with this
        // controller. Maybe also clears report data. Added in the transition from revision 4 to
        // 5.
        public bool commserver_to_clear_data { get; set; }

        public Controller()
        {
            details = new List<ControllerDetails>();
        }

        private void LogI2C(String desc, MyLogger l, NLog.LogLevel lev, I2C_Card_Details obj)
        {
            l.WriteMessage(lev, "8001", desc + " Card present:" + obj.card_present.ToString());
            l.WriteMessage(lev, "8001", desc + " TB present:" + obj.tb_present.ToString());
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Controller c = obj as Controller;
            if ((System.Object)c == null)
            {
                return false;
            }

            return Equals(obj as Controller);
        }

        public bool Equals(Controller p)
        {
            // If parameter is null return false:
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            //if (!Enumerable.SequenceEqual(who_is_active, p.who_is_active))
            //{
            //    return (false);
            //}
            //if (NOCIC != p.NOCIC) return (false);
            if (!details.SequenceEqual(p.details))
            {
                return (false);
            }
            return (true);
        }

        //
        // generate a unique hash for given controller object
        // by hashing a string containing all its relevant items
        //
        public override int GetHashCode()
        {
            String s = "";

            s = details.ToString();
            return(s.GetHashCode());
        }

        public static bool operator ==(Controller lhs, Controller rhs)
        {
            // Check for null on left side. 
            if (Object.ReferenceEquals(lhs, null))
            {
                if (Object.ReferenceEquals(rhs, null))
                {
                    // null == null = true. 
                    return true;
                }

                // Only the left side is null. 
                return false;
            }
            // Equals handles case of null on right side. 
            return lhs.Equals(rhs);
        }

        public static bool operator !=(Controller lhs, Controller rhs)
        {
            return !(lhs == rhs);
        }

        public void LogIt(MyLogger l, NLog.LogLevel lev)
        {
            int i;

            l.WriteMessage(lev, "8001", "-----Dumping Structure from Registration Message-----");
            l.WriteMessage(lev, "8001", "Version:" + this.structure_version.ToString());
            l.WriteMessage(lev, "8001", "Time:" + this.time.ToString());
            l.WriteMessage(lev, "8001", "Date:" + this.date.ToString());
            l.WriteMessage(lev, "8001", "Code Revision:" + this.code_revision_string.ToString());
            l.WriteMessage(lev, "8001", "TP Code Revision:" + this.TP_code_revision_string.ToString());

            for (i = 0; i < this.details.Count; i++)
            {
                l.WriteMessage(lev, "8001", " ** Controller " + i.ToString() + " ** ");
                l.WriteMessage(lev, "8001", " Saw during scan:" + this.details[i].saw_during_scan.ToString());
                l.WriteMessage(lev, "8001", " Serial/Number:" + this.details[i].serial_number.ToString());
                l.WriteMessage(lev, "8001", " option_FL:" + this.details[i].option_FL.ToString());
                l.WriteMessage(lev, "8001", " option_SSE:" + this.details[i].option_SSE.ToString());
                l.WriteMessage(lev, "8001", " option_SSE_D:" + this.details[i].option_SSE_D.ToString());
                l.WriteMessage(lev, "8001", " option_HUB:" + this.details[i].option_HUB.ToString());

                for (int ii = 0; ii < this.details[i].i2c_cards.Count; ii++)
                {
                    LogI2C("  I2C #" + ii.ToString() + " ", l, lev, this.details[i].i2c_cards[ii]);
                }
                // dump poc
                LogI2C("  I2C POC ", l, lev, this.details[i].poc);

                // dump lights
                LogI2C("  I2C Lights ", l, lev, this.details[i].lights);

                l.WriteMessage(lev, "8001", " Weather Card present:" + this.details[i].weather_card_present.ToString());
                l.WriteMessage(lev, "8001", " Weather Terminal present:" + this.details[i].weather_terminal_present.ToString());
                l.WriteMessage(lev, "8001", " Dash M Card present:" + this.details[i].dash_m_card_present.ToString());
                l.WriteMessage(lev, "8001", " Dash M Terminal present:" + this.details[i].dash_m_terminal_present.ToString());
                l.WriteMessage(lev, "8001", " Dash M Card type:" + this.details[i].dash_m_card_type.ToString());
                l.WriteMessage(lev, "8001", " Two Wire Terminal present:" + this.details[i].two_wire_terminal_present.ToString());
                l.WriteMessage(lev, "8001", " Port A Device index:" + this.details[i].port_A_device_index.ToString());
                l.WriteMessage(lev, "8001", " Port B Device index:" + this.details[i].port_B_device_index.ToString());
                l.WriteMessage(lev, "8001", " Box index:" + this.details[i].box_index.ToString());
            }
        }

        public Controller Clone()
        {
            Controller clone = new Controller();

            clone.structure_version = structure_version;
            clone.code_revision_string = String.Copy(code_revision_string);
            clone.TP_code_revision_string = String.Copy(TP_code_revision_string);
            //who_is_active.CopyTo(clone.who_is_active, 0);
            //clone.NOCIC = NOCIC;
            //clone.state = state;
            foreach (ControllerDetails item in details)
            {
                clone.details.Add(item.Clone());
            }

            clone.commserver_to_clear_data = commserver_to_clear_data;

            return (clone);
        }
    }
}
