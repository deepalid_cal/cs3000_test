﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Net.NetworkInformation;
using System.Configuration;
using System.Reflection;
using Nancy;
using Nancy.Hosting.Self;       // nuget package for built in web server to support diag pages
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using Nancy.ViewEngines;
using Nancy.Security;
using Nancy.Authentication.Basic;
using Nancy.ErrorHandling;
using Calsense.Logging;


namespace ServerApp
{
    class WebControl
    {
        private NancyHost _server;

        public WebControl()
        {
            _server = null;
        }

        public class CustomBootstrapper : DefaultNancyBootstrapper
        {
            protected override NancyInternalConfiguration InternalConfiguration
            {
                get
                {
                    return NancyInternalConfiguration.WithOverrides(OnConfigurationBuilder);
                }
            }

            void OnConfigurationBuilder(NancyInternalConfiguration x)
            {
                x.ViewLocationProvider = typeof(ResourceViewLocationProvider);
            }

            protected override void ConfigureApplicationContainer(TinyIoCContainer container)
            {
                base.ConfigureApplicationContainer(container);
                ResourceViewLocationProvider.RootNamespaces.Add(
                  Assembly.GetAssembly(typeof(WebserverModule)), "ServerApp.Views");
            }

            protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
            {
                base.ApplicationStartup(container, pipelines);

                pipelines.EnableBasicAuthentication(new BasicAuthenticationConfiguration(
                    container.Resolve<IUserValidator>(),
                    "Calsense"));       // realm name
            }
        }

        public void Start()
        {
            if(_server == null){
                var Nancyconfig = new HostConfiguration();

                // let Nancy web server create Url reservations for us
                Nancyconfig.UrlReservations.CreateAutomatically = true;
                Nancyconfig.RewriteLocalhost = false;   // don't want Nancy replacing localhost with +


                _server = new NancyHost(new CustomBootstrapper(), Nancyconfig, GetBindableUris());
                if (System.Threading.Thread.CurrentThread.Name == null)
                {
                    System.Threading.Thread.CurrentThread.Name = "Web Ctrl";
                }
            }
            _server.Start();
        }



        private static IEnumerable<IPAddress> GetAllUnicastAddresses()
        {
            // This works on both Mono and .NET , but there is a difference: it also
            // includes the LocalLoopBack so we need to filter that one out
            List<IPAddress> addresses = new List<IPAddress>();
            // Obtain a reference to all network interfaces in the machine
            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in adapters)
            {
                if (adapter.OperationalStatus != OperationalStatus.Up) continue;
                IPInterfaceProperties properties = adapter.GetIPProperties();
                addresses.AddRange(from IPAddressInformation uniCast in properties.UnicastAddresses
                                   where uniCast.Address.AddressFamily != AddressFamily.InterNetworkV6
                                   select uniCast.Address);
            }
            return addresses;
        }
        private static Uri[] GetBindableUris()
        {
            string url;
            IEnumerable<IPAddress> addresses = GetAllUnicastAddresses();
            IList<Uri> urls = new List<Uri>();

            //string localurl = "http://localhost:" + ConfigurationManager.AppSettings["ControlPort"] + "/";
            //urls.Add(new Uri(localurl));
            //Console.WriteLine("Diag URL: " + localurl);

            foreach (IPAddress address in addresses)
            {
                IPHostEntry entry = Dns.GetHostEntry(address);
                // listen based on IP address
                if (address.ToString() == "127.0.0.1") continue;    // already listening on localhost above

                url = string.Format("http://{0}:{1}/", address, ConfigurationManager.AppSettings["ControlPort"]);
                urls.Add(new Uri(url));
                //Log.Instance.Info("Diag URL: " + url);
                // listen based on name of computer
                //url = string.Format("http://{0}:{1}/", entry.HostName, ConfigurationManager.AppSettings["ControlPort"]);
                //urls.Add(new Uri(url));
                //Console.WriteLine("Diag URL: " + url);
            }
            return urls.ToArray();
        }
    }

    public class DemoUserIdentity : IUserIdentity
    {
        public string UserName { get; set; }

        public IEnumerable<string> Claims { get; set; }
    }

    public class UserValidator : IUserValidator
    {
        public IUserIdentity Validate(string username, string password)
        {
            if (username == ConfigurationManager.AppSettings["WebUser"] && password == ConfigurationManager.AppSettings["WebPassword"])
            {
                return new DemoUserIdentity { UserName = username };
            }

            // Not recognised => anonymous.
            return null;
        }
    }

    public class PageNotFoundHandler : DefaultViewRenderer, IStatusCodeHandler
    {
        public PageNotFoundHandler(IViewFactory factory)
            : base(factory)
        {
        }

        public bool HandlesStatusCode(Nancy.HttpStatusCode statusCode, NancyContext context)
        {
            return statusCode == Nancy.HttpStatusCode.NotFound;
        }

        public void Handle(Nancy.HttpStatusCode statusCode, NancyContext context)
        {
            var response = RenderView(context, "404.sshtml");
            response.StatusCode = Nancy.HttpStatusCode.NotFound;
            context.Response = response;
        }
    }
}

