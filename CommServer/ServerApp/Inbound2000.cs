﻿//
//
// Inbound 2000 class 
//
// Handles message reception on sockets from 2000 Inbound controllers
//
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Threading;
using System.Runtime.InteropServices;
using Calsense.Logging;
using ServerApp;


namespace Calsense.CommServer
{
    internal class Inbound2000Internal
    {
        public const int THRU_HEADER_SIZE = 16;                // number of bytes to get through header of data or status packet
        public const int STATUS_PKT_SIZE = 24;

        public const int MAX_APP_BUFFER_SIZE = 1024*1024;     // biggest message probably station history 591386
        public const int MAX_APPDATA_IN_PACKET = 494;         // maximum number of application data bytes in data packet (does not include CID)
        public const int DATA_PACKET_OVERHEAD = 26;     // ambles, header, pkt num, and mid (each of those 2 bytes)
        public const int CID_SIZE = 2;                          // command ID size, follows header in all data packets
        public const int MAX_PACKET_SIZE = (MAX_APPDATA_IN_PACKET + DATA_PACKET_OVERHEAD);   // most bytes on line for a data packet

        public const int OFFSET_PRE = 0;                // preamble
        public const int OFFSET_PID = 4;                // first thing after preamble
        public const int OFFSET_FROMTO = 6;                 // offset of from/to addresses (3 characters each)
        public const int OFFSET_SEQ = 12;               // offset of sequence number
        public const int OFFSET_TOTAL_BLOCKS = 14;      // not used in status packet
        public const int OFFSET_THIS_BLOCK = 15;        // acks length if unreceived packets being reported
        public const int OFFSET_MESSAGEID = 16;           // command id for data packets
        public const int OFFSET_APP_DATA = 18;          // where up to 496 bytes of user data start
        public const int OFFSET_CRC = 20;               // assuming no application/acks data
        public const int OFFSET_POSTAMBLE = 24;         // assuming no application/acks data

        private readonly Server2000 _server;
        private readonly Socket _socket;
        private readonly NetworkStream _networkStream;
        private CancellationTokenSource _cts;
        private MyLogger _logger;
        private int _rxBytecount;
        private StringBuilder _from;   // three bytes from
        private StringBuilder _to;     // three bytes to
        private UInt32 _this_block;
        private UInt32 _total_blocks;
        private UInt16 _seq;
        private ActiveLegacyConnection _ac;

        public static UInt32 FakeID = 0;

        public enum ReceiveState
        {
            RX_PREAMBLE1=0,       // preamble bytes
            RX_PREAMBLE2=1,
            RX_PREAMBLE3=2,
            RX_PREAMBLE4=3,
            RX_HEADER_OPEN,     // rest of tpl data header, mid
            RX_REMAINDER,       // minimum 0 data content of message plus crc, post amble
            RX_STATUS_REMAINDER,// crc/postamble of status
            RX_DATA,            // remaining data if any
            RX_STATUS_CHECK,    // checking crc on status
        }

        //
        // create a new receiver on already established socket/networkstream
        //
        public Inbound2000Internal(Server2000 server, Socket socket, CancellationTokenSource cts)
        {
            _server = server;
            _socket = socket;
            _cts = cts;
            _rxBytecount = 0; 
            _logger = new MyLogger(this.GetType().Name, "", "", Constants.ET2000Model);   // unknown controller at this point
            // Set up streams
            _logger.WriteMessage(NLog.LogLevel.Debug, "3001", "Starting 2000 Inbound task for " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) +
                                                      " on remote port: " + ((IPEndPoint)_socket.RemoteEndPoint).Port.ToString());

            _networkStream = new NetworkStream(socket, true);

            // make a holder ActiveLegacyConnection structure to use until we know which controller is connected
            FakeID++;
            _ac = new ActiveLegacyConnection("2E" + FakeID.ToString(), 5, FakeID);
            _ac.key = "2E" + FakeID.ToString();
            _ac.net_stream = _networkStream;
            _ac.sock = socket;
            _ac.activehost = new StringBuilder("?" + FakeID.ToString());
            _ac.activeadr = new StringBuilder("\0\0\0");    // initial address for unknown controller three zeros
            _ac.activecid = 0;
        }

        //
        // Copy of this spun up for each connection we have to legacy controller(s)
        // Note that there can be more than one controller at the end of each connection
        //
        public async void Do()
        {
            Int32 amount;
            Int32 got;
            Int32 rxmax;
            byte[] pkt = new byte[MAX_PACKET_SIZE];     // receive raw init/data/ack packets into this buffer
            Int32 offset;
            ReceiveState _state;
            LegacyClientInfoStruct client;
            int i;
            bool postambleseen;
            bool crcgood;
            int timeout;
            int ignore_count = 0;
            CancellationTokenRegistration registrationThingy = new CancellationTokenRegistration();
            bool identified = false;
            ActiveLegacyConnection matchedac;

            if (System.Threading.Thread.CurrentThread.Name == null)
            {
                System.Threading.Thread.CurrentThread.Name = "LTCP Do()";
            }

            // send something to controller that attached to get its initial response for Identification
            LegacyTransport.AddTxMessage(_ac, _ac.activehost.ToString(), _ac.activeadr.ToString(), LegacyTasks.MID_COMMSERVER_REQUEST_TO_SEND_SERIAL_NUMBER, null, 0, 0);    // no application data, controller id 0 , job id 0

            //
            // continue to listen for packets until someone else cancels us, or connection goes away
            //
            while (true)
            {
                // here when looking for beginning of a packet 
                _state = ReceiveState.RX_PREAMBLE1;
                _from = new StringBuilder("");
                _to = new StringBuilder("");
                offset = 0;
                amount = 0;
                rxmax = 1;          // receive the first byte of preamble

                // set the remote IP as a thread context for logging, should allow easier filtering per controller
                // the nlog,network,chainsaw veiwers don't seem to support the ndc/mdc output, so don't bother
                // NLog.MappedDiagnosticsContext.Set("ip", IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()).ToString());
                // NLog.NestedDiagnosticsContext.Push(IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()).ToString());

                _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3002", "Ready to receive packet from " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());

                // Executed on a separate thread from listener
                for (; ; )
                {
                    DateTime beginstamp = DateTime.Now;

                    try
                    {

                        // receive next chunk of data into our packet buffer
                        Task<Int32> rx = _networkStream.ReadAsync(pkt, offset, rxmax - amount);
                        registrationThingy.Dispose();   // clear old one
                        TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
                        registrationThingy = _cts.Token.Register(() => tcs.TrySetCanceled(),
                            useSynchronizationContext: false);

                        // timeout used below in Delay() call - remote client needs to send
                        // something within this amount of time or we give up on him (90000 = 90 seconds)
                        //
                        // Timeout should be greater than all the transmit retries + receive timeouts
                        //
                        // so we don't time out quicker than transport layer above us which manages retries
                        // and since multiple controllers on one connection could all be timing out,
                        // this really needs to be grossly big, like hours long to never interfere
                        // with normal operations - just catching a hanging listener, if that could ever occur.
                        //
                        if (!identified)
                        {
                            timeout = 5 * 60;  // seconds in 5 minutes
                        }
                        else 
                        {
                            timeout = 8 * 60 * 60;  // seconds in 8 hours
                        }

                        timeout *= 1000;    // convert seconds to milliseconds

                        if (rx == await Task.WhenAny(rx, tcs.Task, Task.Delay(timeout)).ConfigureAwait(false))
                        {
                            got = await rx;         // bytes received this call
                            // since controllers could have changed (when talking to hub) update logging info just in case
                            _logger.UpdateControllerId(_ac.activecid.ToString());
                            if (got == 0)
                            {
                                // dead socket, nothing actually received
                                _logger.WriteMessageWithId(NLog.LogLevel.Info, "3003", "Controller closed socket. IP: " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                // it seems like the .Connected property is not yet false at this
                                // time, as future transmit attempts that check it still proceed.
                                // so we will force socket closed ourselves...
                                try
                                {
                                    // close immediately don't linger, toss pending data
                                    LingerOption lingerOption = new LingerOption(false, 0);
                                    _socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, lingerOption);                                    
                                    _socket.Disconnect(false);
                                }
                                catch
                                {
                                    // don't care if we get an exception closing socket
                                }
                                registrationThingy.Dispose();
                                return;
                            }
                            amount += got;          // total this state
                            _rxBytecount += got;    // total this client
                            if (_from.Length == 3)
                            {
                                // update stats tracking table
                                // TBD CalsenseControllers.BytesReceived((int)_sn, got);
                            }
                        }
                        else if (_cts.IsCancellationRequested)
                        {
                            _logger.WriteMessageWithId(NLog.LogLevel.Info, "3002", "Request to stop listening on " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                            registrationThingy.Dispose();
                            return;
                        }
                        else
                        {
                            _logger.WriteMessageWithId(NLog.LogLevel.Warn, "3003", "Listener timed out waiting for any RX data from " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                            registrationThingy.Dispose();
                            return;
                        }
                    }
                    catch (Exception e)
                    {
                        if (_cts.IsCancellationRequested)
                        {
                            _logger.WriteMessageWithId(NLog.LogLevel.Info, "3002", "Request to stop listening on " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                            registrationThingy.Dispose();
                            return;
                        }
                        if (e is SocketException)
                        {
                            var ex = (SocketException)e;
                            if (ex.SocketErrorCode == SocketError.Interrupted)
                            {
                                _logger.WriteMessageWithId(NLog.LogLevel.Info, "3005", "Socket terminating normally for " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                registrationThingy.Dispose();
                                return;
                            }
                            else if (ex.SocketErrorCode == SocketError.ConnectionReset)
                            {
                                _logger.WriteMessageWithId(NLog.LogLevel.Info, "3005", "Socket closed " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                registrationThingy.Dispose();
                                return;
                            }
                            else
                            {
                                _logger.WriteMessageWithId(NLog.LogLevel.Error, "3005", "Unexpected Socket exception during network read " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) + " " + ex.ToString(), _ac.activecid.ToString());
                                registrationThingy.Dispose();
                                return;
                            }

                        }
                        else if (e is IOException)
                        {
                            if (_state == ReceiveState.RX_PREAMBLE1)
                            {
                                _logger.WriteMessageWithId(NLog.LogLevel.Info, "3005", "Socket closed " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                            }
                            else
                            {
                                _logger.WriteMessageWithId(NLog.LogLevel.Warn, "3005", "Unexpected I/O exception during network read " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) + " " + e.ToString(), _ac.activecid.ToString());
                            }
                            registrationThingy.Dispose();
                            return;
                        }
                        else
                        {
                            _logger.WriteMessageWithId(NLog.LogLevel.Error, "3005", "Unexpected exception during network read " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) + " " + e.ToString(), _ac.activecid.ToString());
                            registrationThingy.Dispose();
                            return;
                        }
                    }

                    if (!_socket.Connected || amount == 0)
                    {
                        _logger.WriteMessageWithId(NLog.LogLevel.Info, "3095", "Socket terminating, not connected anymore.", _ac.activecid.ToString());
                        registrationThingy.Dispose();
                        return;
                    }

                    switch (_state)
                    {
                        case ReceiveState.RX_PREAMBLE1:
                        case ReceiveState.RX_PREAMBLE2:
                        case ReceiveState.RX_PREAMBLE3:
                        case ReceiveState.RX_PREAMBLE4:
                            // if we got here, we received one more byte
                            if(pkt[OFFSET_PRE + (int)_state] != Constants.PREAMBLE[(int)_state])
                            {
                                ignore_count++;
                                if ((ignore_count % 100) == 0 || ignore_count == 1)
                                {
                                    // chatter a little less often than every byte if we are waiting for preamble thru partial messages
                                    _logger.WriteMessageWithId(NLog.LogLevel.Warn, "3095", "While waiting for preamble have ignored " + ignore_count.ToString() + " bytes so far", _ac.activecid.ToString());
                                }
                                break;  // start over, not a good packet opening
                            }
                            offset += amount;    // keep appending
                            if (_state == ReceiveState.RX_PREAMBLE4)
                            {
                                rxmax = THRU_HEADER_SIZE - OFFSET_PID;  // rest of header after preamble
                                _state = ReceiveState.RX_HEADER_OPEN;
                                amount = 0;
                            }
                            else
                            {
                                _state++;   // next preamble byte
                                amount = 0;
                            }
                            continue;

                        case ReceiveState.RX_HEADER_OPEN:
                            // see if we got all the bytes we want up to and including all of header
                            if (amount == rxmax)
                            {
                                // verify message class is 2000 to commserver
                                if(((pkt[OFFSET_PID] >> 1) & 0x0f) != LegacyTransport.MSG_CLASS_2000e_TO_CENTRAL)
                                {
                                    _logger.WriteMessageWithId(NLog.LogLevel.Warn, "3087", "Received non-2000 Message Class?! from IP: " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()) + ", packet dropped", _ac.activecid.ToString());
                                    break;      // break to start new packet receive
                                }

                                // because we gotup thru mid, it is safe to pull data out
                                // from these various offsets before/including mid
                                _from.Clear();
                                _from.Append((char)pkt[OFFSET_FROMTO]);   // 3 byte from, should be controller character address
                                _from.Append((char)pkt[OFFSET_FROMTO + 1]);
                                _from.Append((char)pkt[OFFSET_FROMTO + 2]);

                                _to.Clear();
                                _to.Append((char)pkt[OFFSET_FROMTO + 3]); // 3 byte to, should be 0xff 0xff 0xff
                                _to.Append((char)pkt[OFFSET_FROMTO + 4]);
                                _to.Append((char)pkt[OFFSET_FROMTO + 5]);


                                // @wjb: I comment out this line, we don't need to update the logger ID to the controller address
                                // keep the same id "hostname"
                               // _logger.UpdateId(_from.ToString()); // now we can log with an ID = controller address

#if SWAP_PROCESS_FOR_LEGACY_CONTROLLERS
                            // Check if the controller was a part of swapping process and the process didn't complete 
                            // either because the controller was powered down or because a communication failure. 
                            // if so we need to send a factory reset command 
                            if (_to[0] != 0)
                            {
                                int s = await Misc.NeedToDoFactoryReset(_logger, _cts, _cid, _sn);

                                if (s == Constants.FACTORY_RESET_IS_REQIURED)
                                {
                                    // send a factory reset command
                                    await FactoryResetToOldControllerRequest(_logger, _cid, _sn);

                                    // Close connection
                                    OurExchangeFinish();
                                    return;
                                }
                                else if (s == Constants.FACTORY_RESET_ERROR_WHILE_CHECKING)
                                {
                                    // some error occured, close connection
                                    OurExchangeFinish();
                                    return;
                                }
                            }
#endif

                                // note contact in our stats tracking table
                                // TBD CalsenseControllers.Contacted((int)_sn, ((IPEndPoint)_socket.RemoteEndPoint).Address);
                                // update Last Communicated field in database
                                // Misc.LegacyLastCommunicated(_logger, _cts, _ac.activecid);

                                // check that from address is who we think we are talking to
                                if (_from.ToString() != _ac.activeadr.ToString())
                                {
                                    // some kind of problem, address changing or something...
                                    _logger.WriteMessageWithId(NLog.LogLevel.Warn, "3099", "Mismatch on controller address, expected " + _ac.activeadr.ToString() + " got " + _from.ToString() + ", packet dropped", _ac.activecid.ToString());
                                    break;      // break to start new packet receive
                                }

                                _seq = pkt[OFFSET_SEQ];
                                _seq <<= 8;
                                _seq |= pkt[OFFSET_SEQ + 1];
                                // legacy reverses bytes, so can't do
                                // _seq = Misc.NetworkBytesToUint16(pkt, OFFSET_SEQ);

                                _total_blocks = pkt[OFFSET_TOTAL_BLOCKS];   // for a status packet this byte is not used
                                _this_block = pkt[OFFSET_THIS_BLOCK];       // for a status packet this byte is the AcksLength


                                if ((pkt[OFFSET_PID] & LegacyTransport.LEGACY_STATUS_BIT) != 0)
                                {
                                    // this is a status packet... there will be no MID, only possible application data is AcksLength bytes
                                    rxmax = STATUS_PKT_SIZE - THRU_HEADER_SIZE;   // typically 8 more bytes (CRC and postamble)
                                    rxmax += (int)_this_block;                         // AcksLength tells us how many bytes of retry bits set
                                    offset += got;    // keep appending 
                                    _state = ReceiveState.RX_STATUS_REMAINDER;
                                    amount = 0;
                                    _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3021", "Status Pkt from " + _from.ToString() + " still to go " + rxmax.ToString(), _ac.activecid.ToString());
                                }
                                else
                                {
                                    // not a status packet
                                    // how much to receive in remainder of header
                                    //
                                    if (_total_blocks != _this_block)
                                    {
                                        // non-last packet - will be full 520 bytes
                                        rxmax = (MAX_APPDATA_IN_PACKET + DATA_PACKET_OVERHEAD) - THRU_HEADER_SIZE;
                                    }
                                    else
                                    {
                                        // last packet, could have any amount of application data... at a minimum
                                        // there is a CID, CRC, and postamble
                                        rxmax = DATA_PACKET_OVERHEAD - THRU_HEADER_SIZE;
                                    }
                                    offset = THRU_HEADER_SIZE; 
                                    _state = ReceiveState.RX_REMAINDER;
                                    amount = 0;
                                }
                            }
                            else
                            {
                                _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3021", "Did not get full packet yet, instead got so far:" + amount.ToString(), _ac.activecid.ToString());
                                offset = OFFSET_PID + amount;
                            }
                            continue;   // always keep trying while working on header...

                        case ReceiveState.RX_STATUS_REMAINDER:
                        case ReceiveState.RX_REMAINDER:
                            // see if we got all we are asking for
                            if (amount == rxmax)
                            {
                                //
                                // at this point we may have a full packet...
                                // for a status packet - this should be it as we set rxmax in previous state to exact amount we need
                                // for a non-last data packet - it should be full up
                                //
                                postambleseen = true;
                                crcgood = false;
                                if(_state == ReceiveState.RX_STATUS_REMAINDER || _this_block != _total_blocks)
                                {
                                    // see if post amble correct
                                    // see if CRC correct
                                    for (i = 0; i < Constants.POSTAMBLE.Length; i++)
                                    {
                                        if (pkt[i + got + offset - Constants.POSTAMBLE.Length] != Constants.POSTAMBLE[i])
                                        {
                                            postambleseen = false;  // nope, no end of message yet
                                        }
                                    }
                                    if(postambleseen == true)
                                    {
                                        // data, offset, length
                                        if (Misc.OrderedCRC(pkt, OFFSET_PID, offset+got - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))) == Misc.NetworkBytesToUint32(pkt, got+offset - (Constants.POSTAMBLE.Length + sizeof(UInt32))))   // CRC is right before post amble
                                        {
                                            crcgood = true;
                                        }
                                    }
                                }
                                else
                                {
                                    // since we don't know how many application bytes might be present, we have to look
                                    // for postamble and good CRC byte by byte on the last data packet
                                    for (i = 0; i < Constants.POSTAMBLE.Length; i++)
                                    {
                                        if (pkt[i + got + offset - Constants.POSTAMBLE.Length] != Constants.POSTAMBLE[i])
                                        {
                                            postambleseen = false;  // nope, no end of message yet
                                        }
                                    }
                                    if(postambleseen == true)
                                    {
                                        // check for good CRC
                                        // data, offset, length
                                        if (Misc.OrderedCRC(pkt, OFFSET_PID, offset + got - (Constants.PREAMBLE.Length + Constants.POSTAMBLE.Length + sizeof(UInt32))) == Misc.NetworkBytesToUint32(pkt, got + offset - (Constants.POSTAMBLE.Length + sizeof(UInt32))))   // CRC is right before post amble
                                        {
                                            crcgood = true;
                                        }
                                    }

                                    // if there is still room, and haven't seen end of packet yet, receive another byte
                                    if(!crcgood && ((offset + got) < (MAX_APPDATA_IN_PACKET + DATA_PACKET_OVERHEAD)))
                                    {
                                        offset += got;
                                        rxmax = 1;
                                        amount = 0;
                                        continue;
                                    }
                                }

                                if (!crcgood)
                                {
                                    // messed up packet.
                                    _logger.WriteMessageWithId(NLog.LogLevel.Warn, "3077", "Bad CRC/postamble/length, throwing away packet from " + _from.ToString() + " at IP: " + IPAddress.Parse(((IPEndPoint)_socket.RemoteEndPoint).Address.ToString()), _ac.activecid.ToString());
                                    // break to start a new packet receive
                                    break;
                                }
                                else
                                {
                                    // we have a valid packet.
                                    // good packet (crc, postamble)
                                    // send to transport
                                    _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3099", "Well framed packet " + _this_block.ToString() + " of " + _total_blocks.ToString() + " received from: " + _from.ToString(), _ac.activecid.ToString());
                                    // pkt has byte array
                                    client.rx = pkt;
                                    client.rxlen = (offset + got); // total bytes received
                                    client.sequence = _seq;
                                    client.adr = _from;
                                    LegacyTransport.RxPacketReceived(_ac, client);

                                    //
                                    // see if there has been an identified ActiveLegacyConnection for this connection yet, switch to
                                    // it from our fake setup if so.
                                    //
                                    if (!identified)
                                    {
                                        await Task.Delay(1500);       // allow a little time for processing / matching
                                        if (HubConnectionTracker.HubTrackingTable.Values.Any(x => x.sock == _socket))
                                        {
                                            if (HubConnectionTracker.HubTrackingTable.TryGetValue(HubConnectionTracker.HubTrackingTable.Where(x => x.Value.sock == _socket).Max(x => x.Key), out matchedac))
                                            {
                                                _ac = matchedac;
                                                identified = true;
                                                _logger.WriteMessageWithId(NLog.LogLevel.Info, "3095", "2000 Inbound listener sees it has been identitied.", _ac.activecid.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3021", "Did not get full packet yet, instead got so far:" + (THRU_HEADER_SIZE + amount).ToString(), _ac.activecid.ToString());
                                offset = THRU_HEADER_SIZE + amount; // keep appending after the header we already received 
                                continue;
                            }
                            // hitting this starts a new packet receive
                            break;
                    }

                    _logger.WriteMessageWithId(NLog.LogLevel.Debug, "3020", "Total Rx'd so far on socket is " + _rxBytecount.ToString(), _ac.activecid.ToString());

#if STATSDONE
                if (_sn != 0)
                {
                    // update stats tracking table
                    CalsenseControllers.MsgsReceived((int)_sn, 1);
                }

#endif  
                    // break out of for loop, restart while(true) loop to begin new packet
                    break;
                }
            }
        }
    }
}
