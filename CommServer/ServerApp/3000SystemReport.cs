﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Calsense.Logging;
using Calsense.CommServer;
using ServerApp;
using System.Threading;



//
// our representation of a Station History Record
//
namespace Calsense.CommServer
{
    class SystemReport
    {
        // 9/4/2012 rmd : With the introduction of a pile of SYSTEM report lines we need to include
        // the gid of the system this record 'belongs' to.
        public UInt32 gid { get; set; }

        public UInt32 start_time { get; set; }        // seconds past midnight
        public UInt16 start_date { get; set; }        // Julian day

        public UInt32 end_time { get; set; }        // seconds past midnight
        public UInt16 end_date { get; set; }        // Julian day

        // ---------------------

        // 7/20/2012 rmd : Raw rainfall TOTAL over the accumulation period. Which defaults mid-night
        // to mid-night. But the user can change. We also keep a mid-night to mid-night accumulator
        // whose value is posted into an alert line each mid-night.
        public UInt32 rainfall_raw_total_100u { get; set; }

        // ---------------------

        // When it comes to the seconds accumulators an UNS_16 is only good for 18.2 hours. So the
        // seconds accumulators must be UNS_32 to hold the entire potential 24 hour count.
        public UInt32 rre_seconds { get; set; }						// Radio Remote usage
        public Single rre_gallons_fl { get; set; }

        public UInt32 test_seconds { get; set; }						// Test
        public Single test_gallons_fl { get; set; }

        public UInt32 walk_thru_seconds { get; set; }					// Walk Thru
        public Single walk_thru_gallons_fl { get; set; }

        public UInt32 manual_seconds { get; set; }						// Manual & Manual Sequence
        public Single manual_gallons_fl { get; set; }

        public UInt32 manual_program_seconds { get; set; }				// Manual Programs
        public Single manual_program_gallons_fl { get; set; }

        public UInt32 programmed_irrigation_seconds { get; set; }		// Programmed Irrigation
        public Single programmed_irrigation_gallons_fl { get; set; }

        public UInt32 non_controller_seconds { get; set; }				// Non-Controller
        public Single non_controller_gallons_fl { get; set; }
    }
    class SystemReportList
    {
        public IList<SystemReport> records;
        public SystemReportList()
        {
            records = new List<SystemReport>();
        }
        public void LogIt(MyLogger l, NLog.LogLevel lev)
        {
#if LOGGING_STATION_HISTORY
            int i;

            l.WriteMessage(lev, "8001", "-----Dumping Structure from System Report Message-----");
            l.WriteMessage(lev, "8001", "System GID:" + this.system_gid.ToString());
            for (i = 0; i < this.records.Count; i++ )
            {
                l.WriteMessage(lev, "8001", " ** Record " + i.ToString() + " ** ");
                l.WriteMessage(lev, "8001", "Time:" + this.records[i].time.ToString());
                l.WriteMessage(lev, "8001", "Date:" + this.records[i].date.ToString());
                l.WriteMessage(lev, "8001", " Station Number:" + this.records[i].station_num.ToString());
                l.WriteMessage(lev, "8001", " Box Index0:" + this.records[i].box_index_0.ToString());
                l.WriteMessage(lev, "8001", " Expected Flow:" + this.records[i].expected_flow.ToString());
                l.WriteMessage(lev, "8001", " Derated Expected Flow:" + this.records[i].derated_expected_flow.ToString());
                l.WriteMessage(lev, "8001", " High Limit:" + this.records[i].hi_limit.ToString());
                l.WriteMessage(lev, "8001", " Low Limit:" + this.records[i].lo_limit.ToString());
                l.WriteMessage(lev, "8001", " Portion of actual:" + this.records[i].portion_of_actual.ToString());
                l.WriteMessage(lev, "8001", " Flow check status:" + this.records[i].flow_check_status.ToString());

                l.WriteMessage(lev, "8001", " indicies_out_of_range: " + this.records[i].FRF_NO_CHECK_REASON_indicies_out_of_range.ToString());
                l.WriteMessage(lev, "8001", " station_cycles_too_low: " + this.records[i].FRF_NO_CHECK_REASON_station_cycles_too_low.ToString());
                l.WriteMessage(lev, "8001", " cell_iterations_too_low: " + this.records[i].FRF_NO_CHECK_REASON_cell_iterations_too_low.ToString());
                l.WriteMessage(lev, "8001", " no_flow_meter: " + this.records[i].FRF_NO_CHECK_REASON_no_flow_meter.ToString());
                l.WriteMessage(lev, "8001", " not_enabled_by_user_setting: " + this.records[i].FRF_NO_CHECK_REASON_not_enabled_by_user_setting.ToString());
                l.WriteMessage(lev, "8001", " acquiring_expected: " + this.records[i].FRF_NO_CHECK_REASON_acquiring_expected.ToString());
                l.WriteMessage(lev, "8001", " combo_not_allowed_to_check: " + this.records[i].FRF_NO_CHECK_REASON_combo_not_allowed_to_check.ToString());
                l.WriteMessage(lev, "8001", " unstable_flow: " + this.records[i].FRF_NO_CHECK_REASON_unstable_flow.ToString());
                l.WriteMessage(lev, "8001", " cycle_time_too_short: " + this.records[i].FRF_NO_CHECK_REASON_cycle_time_too_short.ToString());
                l.WriteMessage(lev, "8001", " irrigation_restart: " + this.records[i].FRF_NO_CHECK_REASON_irrigation_restart.ToString());
                l.WriteMessage(lev, "8001", " not_supposed_to_check: " + this.records[i].FRF_NO_CHECK_REASON_not_supposed_to_check.ToString());
                l.WriteMessage(lev, "8001", " zero_flow_rate: " + this.records[i].FRF_NO_UPDATE_REASON_zero_flow_rate.ToString());
                l.WriteMessage(lev, "8001", " thirty_percent: " + this.records[i].FRF_NO_UPDATE_REASON_thirty_percent.ToString());

            }
#endif
        }
    }

}
