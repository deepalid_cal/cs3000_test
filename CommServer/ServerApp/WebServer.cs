﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;
using Nancy;
using Nancy.Hosting.Self;       // nuget package for built in web server to support diag pages
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using Nancy.ViewEngines;
using Nancy.Security;
using Nancy.Authentication.Basic;
using Nancy.ModelBinding;
using Calsense.CommServer;

namespace ServerApp
{
    public class WebserverModule : NancyModule
    {
        public WebserverModule()
        {
            //this.RequiresAuthentication();



            Get["/"] = parameters =>
            {
                return View["index.sshtml"];
            };

            Get["/stats"] = parameters =>
            {
#if TESTOBJECT
                var result = new List<dynamic>();

                dynamic Ctls = new ExpandoObject();

                var dictionary = (IDictionary<string, object>)Ctls;

                dictionary.Add("ID", 1);
                dictionary.Add("ModelID", 3000);
                dictionary.Add("IP", "192.168.1.1");
                dictionary.Add("LastComm", "2/3/2014 10:11:34");
                result.Add(Ctls);
#endif

                return View["stats.sshtml", CalsenseControllers.GetIList()];
            };

            Get["/control/"] = parameters =>
            {
                WebFormData dud = WebFormData.Instance;
                return View["control.sshtml", dud];
            };

            Get["/dojobs/"] = parameters =>
            {
                // user wants to check IHSFY jobs table right now
                PeriodicTasks.CheckJobsNow();
                return Response.AsText("1");
            };

            Get["/et2000/dojobs/"] = parameters =>
            {
                // user wants to check IHSFY jobs table right now
                LegacyTasks.CheckJobsNow();
                return Response.AsText("1");
            };

            Get["/dopdata/{nid:int}"] = parameters =>
            {
                // user wants to check IHSFY program data table right now
                ServerSocketTracker.CheckJobsNow(parameters.nid);
                return Response.AsText("1");
            };

            Post["/control"] = parameters =>
            {
                // connect to form fields by binding
                var config = this.BindTo(WebFormData.Instance);
                return "Values updated.<br> <a href=\"/control\">Continue...</a><script>setTimeout(\"location.href = '/control';\",3000);</script>";
                //return Response.AsRedirect("/control/updated");
            };

            Get["/flushpool"] = parameters =>
            {
                ServerDB.FlushConnectionsPool();
                return Response.AsText("1");
            };

            Get["/about"] = parameters =>
            {
                About a = new About();
                return View["about.sshtml", a];
            };

        }
    }
}
