CREATE FUNCTION SplitColumns 
   ( 
   @Columns VARCHAR ( 5000 )
   )
   RETURNS VARCHAR ( 5000 )
BEGIN

    DECLARE @Pos    Integer;
    DECLARE @OldPos Integer;
      DECLARE @Query VARCHAR (5000);
      DECLARE @Field VARCHAR (30);
     
      SET @Query = '';
     
      IF RIGHT(@Columns, 1) <> ',' THEN
        @Columns = @Columns + ',';
      END IF;
        
    SET  @Pos    = 1;
         @OldPos = 1;
           
      WHILE @Pos < LENGTH(@Columns) DO
       SET @Pos = LOCATE(',', @Columns, @OldPos);
         @Field = LTRIM(RTRIM(SUBSTRING(@Columns, @OldPos, @Pos - @OldPos)));
         @Query = @Query + @Field + '=t.' + @Field + ',';
         SET @OldPos = @Pos + 1;
    END WHILE;
     
      IF RIGHT(@Query, 1) = ',' THEN
        @Query = SUBSTRING(@Query,1 , LENGTH(@Query)-1);
      END IF;
     
      RETURN @Query;

END;
