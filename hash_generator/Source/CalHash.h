#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include "hashids.h" 
using namespace std;
using namespace System::Drawing::Printing;


#define EXIT_FAILURE 1
//#define EXIT_SUCCESS 0


namespace HashBuilder_App {

    using namespace System;
    using namespace System::ComponentModel;
    using namespace System::Collections;
    using namespace System::Windows::Forms;
    using namespace System::Data;
    using namespace System::Drawing;

    /// <summary>
    /// Summary for CalHash
    /// </summary>


    public ref class CalHash : public System::Windows::Forms::Form
    {
    private: System::Windows::Forms::TextBox^  hash_Out;

    public:
        CalHash(void)
        {
            InitializeComponent();
            //
            //TODO: Add the constructor code here
            //
        }

    protected:
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        ~CalHash()
        {
            if (components)
            {
                delete components;
            }

        }
    private: System::Windows::Forms::TextBox^  entry_Srl;

    public: System::Windows::Forms::ComboBox^  feature_List;
    private:

        //    protected:

        //   protected:


    private: System::Windows::Forms::Label^  label1;
    private: System::Windows::Forms::Label^  label2;
    private: System::Windows::Forms::Label^  label3;
    private: System::Windows::Forms::Button^  printCode_B;
    private: System::Drawing::Printing::PrintDocument^  printDocument1;
	private: System::Windows::Forms::Button^  button1;


    private:














        // private:
        /// <summary>
        /// Required designer variable.
        /// </summary>
        System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent(void)
        {
			this->hash_Out = (gcnew System::Windows::Forms::TextBox());
			this->entry_Srl = (gcnew System::Windows::Forms::TextBox());
			this->feature_List = (gcnew System::Windows::Forms::ComboBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->printCode_B = (gcnew System::Windows::Forms::Button());
			this->printDocument1 = (gcnew System::Drawing::Printing::PrintDocument());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// hash_Out
			// 
			this->hash_Out->BackColor = System::Drawing::Color::White;
			this->hash_Out->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 18, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->hash_Out->Location = System::Drawing::Point(12, 135);
			this->hash_Out->Multiline = true;
			this->hash_Out->Name = L"hash_Out";
			this->hash_Out->Size = System::Drawing::Size(261, 288);
			this->hash_Out->TabIndex = 0;
			this->hash_Out->TabStop = false;
			// 
			// entry_Srl
			// 
			this->entry_Srl->Location = System::Drawing::Point(137, 6);
			this->entry_Srl->MaximumSize = System::Drawing::Size(400, 400);
			this->entry_Srl->Name = L"entry_Srl";
			this->entry_Srl->Size = System::Drawing::Size(136, 20);
			this->entry_Srl->TabIndex = 1;
			this->entry_Srl->Text = L"50065";
			// 
			// feature_List
			// 
			this->feature_List->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(2) { L"CS3-FL", L"CS3-HUB-OPT" });
			this->feature_List->ForeColor = System::Drawing::Color::Black;
			this->feature_List->FormattingEnabled = true;
			this->feature_List->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"CS3-FL", L"CS3-HUB-OPT", L"CS3-AQUAPONICS" });
			this->feature_List->Location = System::Drawing::Point(137, 32);
			this->feature_List->Name = L"feature_List";
			this->feature_List->Size = System::Drawing::Size(136, 21);
			this->feature_List->TabIndex = 2;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(32, 35);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(99, 13);
			this->label1->TabIndex = 5;
			this->label1->Text = L"Feature to activate:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 9);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(119, 13);
			this->label2->TabIndex = 6;
			this->label2->Text = L"Controller serial number:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(12, 119);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(84, 13);
			this->label3->TabIndex = 7;
			this->label3->Text = L"Activation code:";
			// 
			// printCode_B
			// 
			this->printCode_B->Location = System::Drawing::Point(95, 429);
			this->printCode_B->Name = L"printCode_B";
			this->printCode_B->Size = System::Drawing::Size(96, 33);
			this->printCode_B->TabIndex = 4;
			this->printCode_B->Text = L"&Print";
			this->printCode_B->UseVisualStyleBackColor = true;
			this->printCode_B->Click += gcnew System::EventHandler(this, &CalHash::Print_Hash);
			// 
			// printDocument1
			// 
			this->printDocument1->PrintPage += gcnew System::Drawing::Printing::PrintPageEventHandler(this, &CalHash::EvHandler1_Print);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(95, 71);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(96, 33);
			this->button1->TabIndex = 3;
			this->button1->Text = L"&Generate Code";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &CalHash::OnGenHashId);
			// 
			// CalHash
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(285, 474);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->printCode_B);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->feature_List);
			this->Controls->Add(this->entry_Srl);
			this->Controls->Add(this->hash_Out);
			this->MaximizeBox = false;
			this->Name = L"CalHash";
			this->Text = L"Option Activation";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

    private: System::Void OnGenHashId(System::Object^  sender, System::EventArgs^  e) {

#ifdef Ctrlr3000
        void GenHashId(char v_buff[30], unsigned long long srlNmr) {
#endif 

			label1->ForeColor = System::Drawing::Color::Black;
			label2->ForeColor = System::Drawing::Color::Black;

			if ((entry_Srl->Text == "") || (entry_Srl->TextLength < 5) || (entry_Srl->TextLength > 6))
			{
				label2->ForeColor = System::Drawing::Color::Red;
				MessageBox::Show("Enter a valid 5-digit serial number and try again.", "Invalid Serial Number", MessageBoxButtons::OK, MessageBoxIcon::Error);
				return;
			}
			else if (feature_List->Text == "")
			{
				label1->ForeColor = System::Drawing::Color::Red;
				MessageBox::Show("Select a feature to activate and try again.", "Invalid Feature", MessageBoxButtons::OK, MessageBoxIcon::Error);
				return;
			}

            //Add local variables to run the computation 
            enum { COMMAND_ENCODE = 0, COMMAND_DECODE = 1 };


            hashids_t *hashids, v_result, *p_result;
            char *salt = HASHIDS_DEFAULT_SALT, *alphabet = HASHIDS_DEFAULT_ALPHABET,
                *buffer;
            unsigned int command = COMMAND_ENCODE, hex = 0;
            size_t min_hash_length = HASHIDS_DEFAULT_MIN_HASH_LENGTH, numbers_count;
            unsigned long long *numbers, v_numbers[20];
            int i;

            //statically allocate memory for strings with arrays
            char v_alphabet[100], v_copy1[100], v_copy2[100], v_salt[20], v_separators[50], v_guards[20];
            //point the character pointers (for C strings) to the arrays
            v_result.alphabet = v_alphabet; v_result.alphabet_copy_1 = v_copy1; v_result.alphabet_copy_2 = v_copy2; v_result.salt = v_salt;
            v_result.separators = v_separators, v_result.guards = v_guards;
            p_result = &v_result;

            //initialize the algorithm for computing the hash
            hashids = hashids_init3(salt, min_hash_length, alphabet, p_result);


#ifdef VS
            /* error checking */
            if (!hashids) {
                switch (hashids_errno) {
                case HASHIDS_ERROR_ALLOC:
                    printf("Hashids: Allocation failed\n");
                    break;
                case HASHIDS_ERROR_ALPHABET_LENGTH:
                    printf("Hashids: Alphabet is too short\n");
                    break;
                case HASHIDS_ERROR_ALPHABET_SPACE:
                    printf("Hashids: Alphabet contains whitespace characters\n");
                    break;
                default:
                    printf("Hashids: Unknown error\n");
                    break;
                }
            }

#endif

            /* encode */
            command = COMMAND_ENCODE; //BE++
            if (command == COMMAND_ENCODE) {

                /* allocate output buffer */
                char v_buffer[100];

                /* collect numbers */
                numbers = v_numbers; //

#ifdef VS
                if (!numbers) {
                    printf("Cannot allocate memory for numbers\n");

                    //BE++ return EXIT_FAILURE;
                }
#endif

                buffer = v_buffer;

                if (!buffer) {
                    printf("Cannot allocate memory for buffer\n");

                }


                numbers_count = 1;
#ifdef VS
                //Read data from entry_Srl, convert to int ulong

                String^ srl_M = CalHash::entry_Srl->Text;

                numbers[0] = Convert::ToInt64(srl_M);

                /* encode, print, cleanup */

                //Produce the correct Hash Id for the controller and the feature 
                // numbers[0] += 1; 
                //   (unsigned long long)config_c.serial_number;

                if (feature_List->SelectedIndex == 0)
                {
                    numbers[0] += 1;
                }
                else if (feature_List->SelectedIndex == 1)
                {
                    numbers[0] += 2;
                }
				else if (feature_List->SelectedIndex == 2)
				{
					numbers[0] += 3;
				}

                hashids_encode(hashids, buffer, numbers_count, numbers);

				// 2018.04.20 ajv - There appears to be some sort of initialization issue which causes the first
				// result to be incorrect. Therefore, we're manually running the encoding scheme a second time to
				// ensure the result is accurate the first time. Obviously, not the way we want to handle this, but
				// a way to get it done quickly without analyzing the entire algorithm.
				hashids_encode(hashids, buffer, numbers_count, numbers);

                size_t buffIndx = 0;

                if (feature_List->SelectedIndex == 0)
                {
                    buffIndx = strlen(buffer);
                    buffer[buffIndx] = '1';
                    buffer[buffIndx + 1] = '\0';
                }
                else if (feature_List->SelectedIndex == 1)
                {

                    buffIndx = strlen(buffer);
                    buffer[buffIndx] = '2';
                    buffer[buffIndx + 1] = '\0';
                }
				else if (feature_List->SelectedIndex == 2)
				{

					buffIndx = strlen(buffer);
					buffer[buffIndx] = '3';
					buffer[buffIndx + 1] = '\0';
				}
				string intBuff = buffer;
                String^ buffer_M = gcnew String(intBuff.c_str());
                CalHash::hash_Out->Text = "Activation code: " + buffer_M + Environment::NewLine;
                //CalHash::hash_Out->Text += Environment::NewLine;
                CalHash::hash_Out->Text += Environment::NewLine;
                CalHash::hash_Out->Text += "Code description:" + Environment::NewLine;

                for (i = 0; i <= buffIndx; i++)
                {
                    if ((buffer_M[i] >= 0x30) && (buffer_M[i] <= 0x39))
                    {
                        CalHash::hash_Out->Text += "  - number " + buffer_M[i] + Environment::NewLine;
                    }
                    else if ((buffer_M[i] >= 0x40) && (buffer_M[i] <= 0x5A))
                    {
                        CalHash::hash_Out->Text += "  - capital " + buffer_M[i] + Environment::NewLine;
                    }
                    else if ((buffer_M[i] >= 0x61) && (buffer_M[i] <= 0x7A))
                    {
						CalHash::hash_Out->Text += "  - lowercase " + System::Char::ToUpper(buffer_M[i]) + Environment::NewLine;
                    }

                }

			}

#endif


        }

             System::Drawing::Font^ font = gcnew System::Drawing::Font("Calibri", 11, FontStyle::Regular);
             System::Drawing::Point  *drawPoint = new Point(100, 100);

    private: System::Void EvHandler1_Print(System::Object^  sender, System::Drawing::Printing::PrintPageEventArgs^  e) {
        e->Graphics->DrawString("We would like to take this opportunity to thank you for choosing Calsense as your resource\n" +
								"management system. We greatly appreciate your business and look forward to the opportunity to\n" +
								"continue serving you.\n\n" +
								"Below, you will find the activation code to enble the " + feature_List->Text + "feature for CS3000\n"
								"controller serial number " + entry_Srl->Text + ".\n\n" + 
								"To activate this feature, navigate to Main Menu > Setup > Activate Options screen and enter the\n" +
								"code in the space provided.\n\n" +
								CalHash::hash_Out->Text + "\n\n" +
								"To learn more about how to take advantage of your valuable benefits and much more, visit\n" +
								"www.calsense.com.\n\n" +
								"Sincerely,\n\nCalsense",
								font, Brushes::Black, *drawPoint);
    }
	private: System::Void Print_Hash(System::Object^  sender, System::EventArgs^  e) {
		//if (printDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			printDocument1->Print();
		}
    }

}; //end of Form class
    }